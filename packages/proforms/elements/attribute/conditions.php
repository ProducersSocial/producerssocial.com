<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

if($ak->isAttributeKeyConditioned()){

	$condition_handle = $ak->getAttributeConditionHandle();

	Loader::model('attribute/categories/proforms_item','proforms');
	$condition_ak = ProformsItemAttributeKey::getByHandle($condition_handle);
	$condition_at_handle = $condition_ak->getAttributeType()->getAttributeTypeHandle();
	//var_dump($condition_at_handle);

	switch($condition_at_handle){
		case 'select':
			if($condition_ak->getController()->getAllowMultipleValues() == 0){
				Loader::packageElement('attribute/types/select','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			}else{
				Loader::packageElement('attribute/types/select_multiple','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			}
			break;
		case 'select_values':
			Loader::packageElement('attribute/types/select_values','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			break;
		case 'boolean':
		case 'agreement':
			Loader::packageElement('attribute/types/boolean','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			break;
		case 'radio':
			Loader::packageElement('attribute/types/radio','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			break;
		default:
			Loader::packageElement('attribute/types/text','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			break;
	}


	$ajax = Loader::helper('concrete/urls')->getBlockTypeToolsURL($bt).'/ajax_attribute.php';

}