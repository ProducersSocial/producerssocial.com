<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

$condition_handle = $ak->getAttributeConditionHandle();
$condition_operator = $ak->getAttributeConditionOpperator();
$condition_value = $ak->getAttributeConditionValue();
$condition_keyID = $ak->getAttributeConditionKeyID();

echo '
<script type="text/javascript"> /*<![CDATA[*/ 

	function check_condition_'.$ak->getAttributeKeyID().'(change_val){ 	
		var condition = "'.$condition_value.'";
		if($.isNumeric(condition)){
			condition = condition * 1;
			change_val = change_val *1;
		}
		
		if(change_val '.$condition_operator.' condition && change_val != "** None"){ 
			console.log("'.$ak->getAttributeKeyHandle().' " + change_val + " " + "'.$condition_operator.'" + " " + condition);
			$.ajax({ 
				type: \'get\', 
				url: \''.Loader::helper('concrete/urls')->getBlockTypeToolsURL($bt).'/ajax_attribute.php\', 
				data:{ akID: '.$ak->getAttributeKeyID().'} 
			}).done(function(html){ 
				$(\'#conditional_'.$ak->getAttributeKeyID().'\').html(html); 
				$(document).trigger(\'rebind\', [\'Custom\', \'Event\']); 
				$(\'.proform_slider\').css(\'height\',$(\'#conditional_'.$ak->getAttributeKeyID().'\').parent().height());
			}); 
		}else{ 
			//console.log("'.$ak->getAttributeKeyHandle().' " + change_val + " " + "'.$condition_operator.'" + " " + condition);
			$(\'#conditional_'.$ak->getAttributeKeyID().'\').empty(); 
			$(\'.proform_slider\').css(\'height\',($(\'#conditional_'.$ak->getAttributeKeyID().'\').parent().height())	); 
		}
	};
	
	function inCondition_'.$ak->getAttributeKeyID().'(){ 
		
		var key_'.$condition_handle.' = $(\'select[name="akID\\\['.$condition_keyID.'\\\]\\\[atSelectOptionID\\\]\\\[\\\]"]\');
		 
		key_'.$condition_handle.'.change(function(){ 
			change_val = $(\'select[name="akID\\\['.$condition_keyID.'\\\]\\\[atSelectOptionID\\\]\\\[\\\]"] option:selected\').text(); 
			check_condition_'.$ak->getAttributeKeyID().'(change_val); 
		}); 

	}
	
	inCondition_'.$ak->getAttributeKeyID().'(); 
	$(document).on(\'rebind\', function() { 
		inCondition_'.$ak->getAttributeKeyID().'(); 
	}); 
	
	var key_'.$condition_handle.' = $(\'select[name="akID\\\['.$condition_keyID.'\\\]\\\[atSelectOptionID\\\]\\\[\\\]"]\');
		
	change_val = $(\'select[name="akID\\\['.$condition_keyID.'\\\]\\\[atSelectOptionID\\\]\\\[\\\]"] option:selected\').text(); 
	check_condition_'.$ak->getAttributeKeyID().'(change_val);
/*]]>*/ 
</script>
';