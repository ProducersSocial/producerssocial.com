<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

//if we have a page path, that means we are deleting
//else, we are adding/updating
if($dpath){

	if($pf->getAttribute('event_multidate')){
		$db = Loader::db();
		$title = $pf->getAttribute('event_title');
		$p = Page::getByPath($dpath.'/'.strtolower(str_replace(' ','-',$title)));
		$db->Execute("DELETE from btProEventDates where eventID = ?",array($p->getCollectionID()));
		$p->delete();
	}

}else{
	
	Loader::model('event_item','proevents');
							
	$dates_array = Loader::helper('form/date_time_time','proevents')->translate_from($p);
	$dates_renumber = array();
	foreach($dates_array as $date_item){
		array_push($dates_renumber, $date_item);
	}
	
	$start_date = date('Y-m-d',strtotime($dates_renumber[1]['date']));
	$description = $pf->getAttribute('event_description');
	$title = $pf->getAttribute('event_title');
	
	$datas = array('cDescription' => $description, 'cName' => $title, 'cDatePublic' => $start_date);
	$p->update($datas);
	
	$blocks = $p->getBlocks('Main');
	foreach($blocks as $b) {
		if($b->getBlockTypeHandle()=='content'){
			$b->deleteBlock();
		}
	}
	
	$bt = BlockType::getByHandle('content');
	$data = array('content' => $description);
	$b = $p->addBlock($bt, 'Main', $data);
	$b->setCustomTemplate('event_post');
	
	$p->reindex();
	
	$event_item = new EventItemDates($p,true);

}