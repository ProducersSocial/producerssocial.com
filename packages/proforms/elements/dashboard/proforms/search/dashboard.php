<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<style type="text/css">
.btn-large{width: 80%;}
.formsets a{color: black;}
.formsets hr{margin: 12px 0;}
.formsets .badge{float:right;}
</style>
    <?php 
    $category = AttributeKeyCategory::getByHandle('proforms_item');
	$sel = array('0' => t('** None'));
	$sets = $category->getAttributeSets();
	foreach($sets as $as) {
	    $ProformsItemList = new ProformsItemList();
	    $ProformsItemList->filterByAttributeSet($as->getAttributeSetID());
	    $items = $ProformsItemList->get();
	    $count = count($items);
		?>
		<div class="span3 well formsets">
            <a href="<?php   echo $this->url('/dashboard/proforms/search/')?>?asID=<?php echo $as->getAttributeSetID()?>">
                <h4><span class="badge badge-inverse"><?php echo $count?></span> <?php echo $as->getAttributeSetName()?></h4>
                <br/>
                <hr/>
                <a href="<?php   echo $this->url('/dashboard/proforms/search/')?>?asID=<?php echo $as->getAttributeSetID()?>"><i class="icon icon-tasks"></i> &nbsp;<?php echo t('View Submissions')?></a>
                <hr/>
                <a href="<?php   echo $this->url('/dashboard/proforms/editor/')?><?php echo $as->getAttributeSetID()?>/" class=""><i class="icon icon-edit"></i> &nbsp;<?php echo t('Edit Form')?></a>
                <hr/>
                <a href="<?php   echo BASE_URL.DIR_REL?>/index.php/tools/packages/proforms/proforms/search_results_export.php?asID=<?php echo $as->getAttributeSetID()?>" class=""><i class="icon icon-share-alt"></i> &nbsp;<?php echo t('Export')?></a>
            </a>
        </div>
        <?php 
	}
    ?>
    <br style="clear: both;"/>
