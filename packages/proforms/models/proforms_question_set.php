<?php      
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/set');
/**
*
* An object that allows a filtered list of events to be returned.
* @package ProEvents
*
**/
class ProformsQuestionSet extends Object {

	public function QuestionSetIsInternal($asID){
		$db = Loader::db();
		return $db->getOne("SELECT is_internal FROM AttributeSets WHERE asID = ?",array($asID));
	}
	
	public function SetQuestionSetIsInternal($asID,$val){
		$db = Loader::db();
		return $db->getOne("UPDATE AttributeSets SET is_internal = ? WHERE asID = ?",array($val,$asID));
	}


}