<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PriceValueEnterAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');

		print '<span class="currency_symbol"></span><input type="text" name="akID['.$this->attributeKey->getAttributeKeyID().'][value]" id="akID['.$this->attributeKey->getAttributeKeyID().'][value]" class="price_value value_enter input-small" value="10" onblur="if(this.value==\'\') this.value=\'10\'; updatePricing();"/>';

	}
	
	public function type_form() {
		$this->load();
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT value FROM atPriceValueEnter WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		if($value){
			$db = Loader::db();
			$db->Execute('delete from atPriceValueEnter where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atPriceValueEnter (avID,value) values (?,?)', array($this->getAttributeValueID(),$value['value']));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Price Value Entry QuestionType allows users to manually enter an amount rather than being predisposed to given amounts.</p>
		');
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;

	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceValueEnterSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}