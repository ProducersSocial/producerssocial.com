<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class AjaxPaymentAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $pattern;
	public $restrictDuplicates = true;
	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		$this->addFooterItem('<script type="text/javascript" src="https://stage.wepay.com/js/iframe.wepay.js"></script>');
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<p style="color: red;margin-left: 160px;margin-top: 22px;margin-bottom: 22px;">'.t('Create a select values question with the names of your payment forms and their handles as options. Then use that question handle below.').'</p>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Payment Select Handle').'</label>';
		print '		<div class="input">';
		print $fm->text('payment_select_handle',$this->payment_select_handle);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atatAjaxPayment where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->payment_select_handle = $row['payment_select_handle'];

		$this->set('akID', $this->akID);
		$this->set('payment_select_handle', $this->payment_select_handle);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atAjaxPayment WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	

	public function validateForm($value) {
		if(is_array($phone)){
			$value = $value['value'];
		}
		$this->load();
		$pattern = $this->pattern;
		if(!@preg_match("$pattern",$value)) {
		  // regex value match is invalid
		  $this->error = t('The input provided is not in the correct format for \''.$this->attributeKey->getAttributeKeyName().'\'!'.$value);
		  return false;
		}
		return true;
	}
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atAjaxPayment where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atAjaxPayment (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$value['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atAjaxPayment where avID = ?', array($this->getAttributeValueID()));
	}
	
	public function help_text(){
		return t('<h4>What is it?</h4><p>Listens for a select_values question_type handle to then match the name of various payment forms by handle, and load that payment form in via AJAX.</p>');
	}
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'payment_select_handle'=>$data['payment_select_handle']
		);
		
		$db=Loader::db();
		$db->Replace('atatAjaxPayment', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atatAjaxPayment where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}