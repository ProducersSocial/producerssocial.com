<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/types/default/controller');

class EmailAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public $restrictDuplicates = true;

	public function form() {
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		print Loader::helper('form')->text($this->field('value'), $value);
	}
	
	public function searchForm($list){
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}
	
	public function validateForm($email) {
		if(is_array($email)){
			$email = $email['value'];
		}
	  if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email)){
	    list($username,$domain)=split('@',$email);
	    if(!checkdnsrr($domain,'MX')) {
	    	$this->error = t('Your email domain appears to be invalid!');
	      return false;
	    }
	    return true;
	  }
	  $this->error = t('Your email is not in the correct format!');
	  return false;
	}


}