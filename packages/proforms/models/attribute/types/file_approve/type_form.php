<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
?>
<div class="clearfix">
	<label><?php    echo t("Add To Fileset")?></label>
	<div class="input">
		<select name="fasID">
		    <option value="0">--Choose Fileset--</option>
		    <?php   foreach ($fileSets as $fs) : ?>
		    <option value='<?php   echo $fs->fsID ?>' <?php   echo $asID == $fs->fsID ? 'selected' : '' ?> >
			<?php   echo htmlspecialchars($fs->fsName, ENT_QUOTES, 'UTF-8') ?>
		    </option>
		    <?php   endforeach ?>
		</select>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Needs Approved By:")?></label>
	<div class="input">
		<select name="approver">
		    <option value="admin" <?php  if($approver == 'admin'){ echo 'SELECTED';}?>><?php echo t('Admin')?></option>
		    <option value="user" <?php  if($approver == 'user'){ echo 'SELECTED';}?>><?php echo t('User')?></option>
		</select>
	</div>
</div>