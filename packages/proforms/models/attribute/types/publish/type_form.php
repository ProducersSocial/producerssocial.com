<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
$ps = Loader::helper('form/page_selector');

Loader::model("collection_types");
$ctArray = CollectionType::getList('');
$pageTypes = array();
foreach($ctArray as $ct) {
	$pageTypes[$ct->getCollectionTypeID()] = $ct->getCollectionTypeName();		
}
?>
<div class="clearfix">
	<label><?php    echo t("Require Review?")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="page_named" value="1" <?php    if($page_named>0){echo 'checked';}?>> <?php    echo t('Yes. Form Entries require review to publish.');?><br/>
		  <i style="color: red;">  <?php    echo t('(Leaving this field unchecked will result in page creation immediatly on form submission!)'); ?> </i>
		</label>
	</div>
</div>
<div class="clearfix">
	<label><?php    echo t("Attribute Matching")?></label>
	<div class="input">
		<div class="row">
			<div class="span3">
				<?php   
				echo $fm->label('from_att',t('From Attribute List'));
				echo $fm->textarea('from_att',$from_att);
				?>
			</div>
			<div class="span3">
				<?php   
				echo $fm->label('to_att',t('To Attribute List'));
				echo $fm->textarea('to_att',$to_att);
				?>
			</div>
		</div>	
		<br/><i><?php   echo t('(Leave these blank to assume 1 to 1 attribute handle matching. Non matching attributes will be skipped.)')?></i>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Publish Location")?></label>
	<div class="input">
		<?php   echo $ps->selectPage('location',$location)?>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Publish Page Type")?></label>
	<div class="input">
		<?php     echo $fm->select('ctID', $pageTypes, $ctID)?>
	</div>
</div>