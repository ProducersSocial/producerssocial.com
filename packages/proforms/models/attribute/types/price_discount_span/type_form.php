<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
function getAttributeOptionHTML($v){ 
	if ($v == 'TEMPLATE') {
		$akPriceDiscountSpanID = 'TEMPLATE_CLEAN';
		$akPriceDiscountSpan = 'TEMPLATE';
		$akPriceDiscountSpanOption = 'TEMP_OPTION';
	} else {
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceDiscountSpanID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceDiscountSpanID = $v->getSelectAttributeOptionID();
		}
		$akPriceDiscountSpan = $v->getSelectAttributeOptionValue();
		$akPriceDiscountSpanOption = $v->getSelectAttributeOptionValueOption();
	}
		?>
		<div id="akPriceDiscountSpanDisplay_<?php    echo $akPriceDiscountSpanID?>" >
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountSpanID)?>')" value="<?php    echo t('Edit')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.deleteValue('<?php    echo addslashes($akPriceDiscountSpanID)?>')" value="<?php    echo t('Delete')?>" />
			</div>			
			<span onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountSpanID)?>')" id="akPriceDiscountSpanStatic_<?php    echo $akPriceDiscountSpanID?>" class="leftCol"><?php    echo $akPriceDiscountSpan ?> <i>(<?php    echo $akPriceDiscountSpanOption ?>)</i></span>
		</div>
		<div id="akPriceDiscountSpanEdit_<?php    echo $akPriceDiscountSpanID?>" style="display:none">
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountSpanID)?>')" value="<?php    echo t('Cancel')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.changeValue('<?php    echo addslashes($akPriceDiscountSpanID)?>')" value="<?php    echo t('Save')?>" />
			</div>		
			<span class="leftCol">
				<input name="akPriceDiscountSpanOriginal_<?php    echo $akPriceDiscountSpanID?>" type="hidden" value="<?php    echo $akPriceDiscountSpan?>" />
				<?php     if (is_object($v) && $v->getSelectAttributeOptionTemporaryID() == false) { ?>
					<input id="akPriceDiscountSpanExistingOption_<?php    echo $akPriceDiscountSpanID?>" name="akPriceDiscountSpanExistingOption_<?php    echo $akPriceDiscountSpanID?>" type="hidden" value="<?php    echo $akPriceDiscountSpanID?>" />
				<?php     } else { ?>
					<input id="akPriceDiscountSpanNewOption_<?php    echo $akPriceDiscountSpanID?>" name="akPriceDiscountSpanNewOption_<?php    echo $akPriceDiscountSpanID?>" type="hidden" value="<?php    echo $akPriceDiscountSpanID?>" />
				<?php     } ?>
				<input id="akPriceDiscountSpanField_<?php    echo $akPriceDiscountSpanID?>" name="akPriceDiscountSpan_<?php    echo $akPriceDiscountSpanID?>" type="text" value="<?php    echo $akPriceDiscountSpan?>" size="20" />
				  
				<input id="akPriceDiscountSpanOptionField_<?php    echo $akPriceDiscountSpanID?>" name="akPriceDiscountSpanOption_<?php    echo $akPriceDiscountSpanID?>" type="text" value="<?php    echo $akPriceDiscountSpanOption?>" size="20" />
			</span>		
		</div>	
		<div class="ccm-spacer">&nbsp;</div>
<?php     } ?>

<fieldset>
<legend><?php    echo t('Select Options')?></legend>

<div class="clearfix">
<label><?php    echo t('Values')?></label>
<div class="input">
	<div id="attributeValuesInterface">
	<div id="attributeValuesWrap">
	<?php    
	Loader::helper('text');
	foreach($akPriceDiscountSpan as $v) { 
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceDiscountSpanID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceDiscountSpanID = $v->getSelectAttributeOptionID();
		}
		?>
		<div id="akPriceDiscountSpanWrap_<?php    echo $akPriceDiscountSpanID?>" class="akPriceDiscountSpanWrap <?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?> akPriceDiscountSpanWrapSortable <?php     } ?>">
			<?php    echo getAttributeOptionHTML( $v )?>
		</div>
	<?php     } ?>
	</div>
	
	<div id="akPriceDiscountSpanWrapTemplate" class="akPriceDiscountSpanWrap" style="display:none">
		<?php    echo getAttributeOptionHTML('TEMPLATE') ?>
	</div>
	
	<div id="addAttributeValueWrap"> 
		<input id="akPriceDiscountSpanFieldNew" name="akPriceDiscountSpanNew" type="text" value="<?php    echo $defaultNewOptionNm ?>" size="40" class="faint input-medium" 
		onfocus="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',0)" 
		onblur="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',1)"
		onkeypress="ccmAttributesHelper.addEnterClick(event,function(){ccmAttributesHelper.saveNewOption()})"
		placeholder="<?php  echo t('% off (no % sign)')?>" /> 
		 <input id="akPriceDiscountSpanOptionFieldNew" name="akPriceDiscountSpanOptionNew" type="text" value="" placeholder="<?php  echo t('Number of Days')?>" class="input-small"/>
		<input class="btn" type="button" onClick="ccmAttributesHelper.saveNewOption(); $('#ccm-attribute-key-form').unbind()" value="<?php    echo t('Add') ?>" />
	</div>
	</div>

</div>
</div>


</fieldset>
<?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?>
<script type="text/javascript">
//<![CDATA[
$(function() {
	ccmAttributesHelper.makeSortable();
});
//]]>
</script>
<?php     } ?>