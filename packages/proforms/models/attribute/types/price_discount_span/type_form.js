var ccmAttributesHelper={   
	valuesBoxDisabled:function(typeSelect){
		var attrValsInterface=document.getElementById('attributeValuesInterface')
		var requiredVals=document.getElementById('reqValues');
		var allowOther=document.getElementById('allowOtherValuesWrap');
		var offMsg=document.getElementById('attributeValuesOffMsg');
		if (typeSelect.value == 'SELECT' || typeSelect.value == 'SELECT_MULTIPLE') {
			attrValsInterface.style.display='block';
			requiredVals.style.display='inline'; 
			if(allowOther) allowOther.style.display='block';
			offMsg.style.display='none';			
		} else {  
			requiredVals.style.display='none'; 
			attrValsInterface.style.display='none';
			if(allowOther) allowOther.style.display='none';
			offMsg.style.display='block'; 
		}
		return false;	
	},  
	
	deleteValue:function(val){
		if(confirm(ccmi18n.deleteAttributeValue)) {
			$('#akPriceDiscountSpanWrap_'+val).remove();				
		}
	},
	
	editValue:function(val){ 
		if($('#akPriceDiscountSpanDisplay_'+val).css('display')!='none'){
			$('#akPriceDiscountSpanDisplay_'+val).css('display','none');
			$('#akPriceDiscountSpanEdit_'+val).css('display','block');		
		}else{
			$('#akPriceDiscountSpanDisplay_'+val).css('display','block');
			$('#akPriceDiscountSpanEdit_'+val).css('display','none');
			//$('#akPriceDiscountSpanField_'+val).val( $('#akPriceDiscountSpanStatic_'+val).html() )
		}
	},
	
	changeValue:function(val){ 
		$('#akPriceDiscountSpanStatic_'+val).html( $('#akPriceDiscountSpanField_'+val).val() + '<i> (' +  $('#akPriceDiscountSpanOptionField_'+val).val() + ')</i>' );
		this.editValue(val);
	},
	
	makeSortable: function() {
		$("div#attributeValuesWrap").sortable({
			cursor: 'move',
			opacity: 0.5
		});
		return false;
	},
	
	saveNewOption:function(){
		var newValF=$('#akPriceDiscountSpanFieldNew');
		var val=newValF.val();
		if(val=='') {
			return;
		}
		var newOptF=$('#akPriceDiscountSpanOptionFieldNew');
		var opt= newOptF.val();
		
		var ts = 't' + new Date().getTime();
		var template=document.getElementById('akPriceDiscountSpanWrapTemplate'); 
		var newRowEl=document.createElement('div');
		newRowEl.innerHTML=template.innerHTML.replace(/template_clean/ig,ts).replace(/template/ig,val).replace(/temp_option/ig,opt);
		newRowEl.id="akPriceDiscountSpanWrap_"+ts;
		newRowEl.className='akPriceDiscountSpanWrap';
		$('#attributeValuesWrap').append(newRowEl);		
		newValF.val(''); 
		newOptF.val(''); 
		return false;
	},
	
	clrInitTxt:function(field,initText,removeClass,blurred){
		if(blurred && field.value==''){
			field.value=initText;
			$(field).addClass(removeClass);
			return;	
		}
		if(field.value==initText) field.value='';
		if($(field).hasClass(removeClass)) $(field).removeClass(removeClass);
		return false;
	},
	
	addEnterClick:function(e,fn){
		var form = $("#ccm-attribute-key-form");
		form.submit(function() {return false;});
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 13 && typeof(fn)=='function' ) {
			fn();
			setTimeout(function() { 
				form.unbind();
			}, 100);
		}
		return false;
	}
}
