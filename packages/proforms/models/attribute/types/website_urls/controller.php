<?php     defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('attribute/types/default/controller');
Loader::library('3rdparty/htmLawed');

class WebsiteUrlsAttributeTypeController extends DefaultAttributeTypeController  {
	
	public $helpers = array('form');
	
	public function saveKey($data) {
		$akTextareaDisplayMode = $data['akTextareaDisplayMode'];
		if (!$akTextareaDisplayMode) {
			$akTextareaDisplayMode = 'text';
		}
		$this->setDisplayMode($akTextareaDisplayMode);
	}

	public function getDisplaySanitizedValue() {

		$this->load();
		if ($this->akTextareaDisplayMode == 'text') {
			return parent::getDisplaySanitizedValue();
		}
		return htmLawed(parent::getValue(), array('safe'=>1, 'deny_attribute'=>'style'));
	}
	
	public function display_review() {

		$this->load();
		
		$value = parent::getValue();

		print Loader::helper('form')->textarea($this->field('value'), $value, array('class' => $additionalClass, 'rows' => 5));
	}
	
	public function display() {

		$this->load();
		$values = explode("\n",parent::getValue());
		
		$fm = Loader::helper('form');
		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][approve_urls]',1);
		
		foreach($values as $val){

			if($val != ''){
				print $fm->checkbox('akID['.$this->attributeKey->getAttributeKeyID().'][url][]',$val).' <a href="'.$val.'" alt="website">'.$val.'</a><br/>';
			}
		}
	}
	
	public function form($additionalClass = false) {
		$this->load();
		if (is_object($this->attributeValue)) {
			$value = $this->getAttributeValue()->getValue();
		}
		// switch display type here
		print Loader::helper('form')->textarea($this->field('value'), $value, array('class' => $additionalClass, 'rows' => 5));
	}

	public function validateForm($data) {
		$value = $data['value'];
		$vals = explode("\n",$value);
		$this->load();

	  	foreach($vals as $val){
		  if(!preg_match("~^(?:f|ht)tps?://~i",trim($val))) {
			  // regex value match is invalid
			  $this->error = t('Your URL\'s must have http:// or https:// for \''.$this->attributeKey->getAttributeKeyName().'\'! - '.$val);
			  return false;
		  }
		}
		return true;

	}
	
	// run when we call setAttribute(), instead of saving through the UI
	public function saveValue($value) {
		if($value['approve_urls']){
			if(is_array($value['url'])){
				$value = implode("\n",$value['url']);
			}
		}else{
			$value = $value['value'];
		}

		$db = Loader::db();
		$db->Replace('atDefault', array('avID' => $this->getAttributeValueID(), 'value' => $value), 'avID', true);
	}
	
	public function saveForm($data) {
		$this->saveValue($data);
	}
	
	public function composer() {
		$this->form('span4');
	}

	public function searchForm($list) {
		$db = Loader::db();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), '%' . $this->request('value') . '%', 'like');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print $f->text($this->field('value'), $this->request('value'));
	}
	

	public function setDisplayMode($akTextareaDisplayMode) {
		$db = Loader::db();
		$ak = $this->getAttributeKey();
		$db->Replace('atTextareaSettings', array(
			'akID' => $ak->getAttributeKeyID(), 
			'akTextareaDisplayMode' => $akTextareaDisplayMode
		), array('akID'), true);
	}
	
	/* 
	public function saveForm($data) {
		$db = Loader::db();
		$this->saveValue($data['value']);
	}
	*/
	
	// should have to delete the at thing
	public function deleteKey() {
		$db = Loader::db();
		$arr = $this->attributeKey->getAttributeValueIDList();
		foreach($arr as $id) {
			$db->Execute('delete from atDefault where avID = ?', array($id));
		}
		
		$db->Execute('delete from atTextareaSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}
	
	public function type_form() {
		$this->load();
	}
	
	protected function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select akTextareaDisplayMode from atTextareaSettings where akID = ?', $ak->getAttributeKeyID());
		$this->akTextareaDisplayMode = $row['akTextareaDisplayMode'];
		$this->set('akTextareaDisplayMode', $this->akTextareaDisplayMode);
	}
	
	public function exportKey($akey) {
		$this->load();
		$akey->addChild('type')->addAttribute('mode', $this->akTextareaDisplayMode);
		return $akey;
	}

	public function importKey($akey) {
		if (isset($akey->type)) {
			$data['akTextareaDisplayMode'] = $akey->type['mode'];
			$this->saveKey($data);
		}
	}
	
	public function duplicateKey($newAK) {
		$this->load();
		$db = Loader::db();
		$db->Replace('atTextareaSettings', array(
			'akID' => $newAK->getAttributeKeyID(), 
			'akTextareaDisplayMode' => $this->akDateDisplayMode
		), array('akID'), true);
	}
}
