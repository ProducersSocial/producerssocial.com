<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class PriceSelectAttributeTypeController extends AttributeTypeController  {

	private $akSelectAllowMultipleValues;
	private $akSelectAllowOtherValues;
	private $akSelectOptionDisplayOrder;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function type_form() {
		$this->load();
		$at = $this->attributeType;
		$path1 = $at->getAttributeTypeFileURL('type_form.js');
		$path2 = $at->getAttributeTypeFileURL('type_form.css');
		$this->addFooterItem(Loader::helper('html')->javascript($path1));
		$this->addFooterItem(Loader::helper('html')->css($path2));
		$this->set('form', Loader::helper('form'));		

		//$akPriceSelect = $this->getPriceSelectFromPost();
		//$this->set('akPriceSelect', $akPriceSelect);
		
		if ($this->isPost()) {
			$akPriceSelect = $this->getPriceSelectFromPost();
			$this->set('akPriceSelect', $akPriceSelect);
		} else if (isset($this->attributeKey)) {
			$options = $this->getOptions();
			$this->set('akPriceSelect', $options);
		} else {
			$this->set('akPriceSelect', array());
		}
	}
	
	protected function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$dbs = Loader::db();
		$row = $dbs->GetRow('select akSelectAllowMultipleValues, akSelectOptionDisplayOrder, akSelectAllowOtherValues from atSelectSettings where akID = ?', $ak->getAttributeKeyID());
		$this->akSelectAllowMultipleValues = $row['akSelectAllowMultipleValues'];
		$this->akSelectAllowOtherValues = $row['akSelectAllowOtherValues'];
		$this->akSelectOptionDisplayOrder = $row['akSelectOptionDisplayOrder'];

		$this->set('akSelectAllowMultipleValues', $this->akSelectAllowMultipleValues);
		$this->set('akSelectAllowOtherValues', $this->akSelectAllowOtherValues);			
		$this->set('akSelectOptionDisplayOrder', $this->akSelectOptionDisplayOrder);			
	}

	public function duplicateKey($newAK) {
		$this->load();
		$dbs = Loader::db();
		$dbs->Execute('insert into atSelectSettings (akID, akSelectAllowMultipleValues, akSelectOptionDisplayOrder, akSelectAllowOtherValues) values (?, ?, ?, ?)', array($newAK->getAttributeKeyID(), $this->akSelectAllowMultipleValues, $this->akSelectOptionDisplayOrder, $this->akSelectAllowOtherValues));	
		$r = $dbs->Execute('select value, displayOrder, isEndUserAdded from atSelectOptions where akID = ?', $this->getAttributeKey()->getAttributeKeyID());
		while ($row = $r->FetchRow()) {
			$dbs->Execute('insert into atSelectOptions (akID, value, displayOrder, isEndUserAdded) values (?, ?, ?, ?)', array(
				$newAK->getAttributeKeyID(),
				$row['value'],
				$row['displayOrder'],
				$row['isEndUserAdded']
			));
		}
	}
	
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Select Values Question Type is a value-text pair designed to separate select values from select options</p><p>For example, you may want to have a select option that choses "department" but is really assigning an email to that form.</p>
		');
	}
	
	
	
	private function getPriceSelectFromPost() {
		$options = new PriceSelectAttributeTypeOptionList();
		$displayOrder = 0;		
		
		foreach($_POST as $key => $value) {
			if( !strstr($key,'akPriceSelect_') || $value=='TEMPLATE' ) continue; 
			$opt = false;
			// strip off the prefix to get the ID
			$id = substr($key, 14);
			// now we determine from the post whether this is a new option
			// or an existing. New ones have this value from in the akPriceSelectNewOption_ post field

			if ($_POST['akPriceSelectNewOption_' . $id] == $id) {
				$valOpt = $_POST['akPriceSelectOption_' . $id];
				$opt = new PriceSelectAttributeTypeOption(0, $value, $valOpt,  $displayOrder);
				$opt->tempID = $id;
			} else if ($_POST['akPriceSelectExistingOption_' . $id] == $id) {
				$valOpt = $_POST['akPriceSelectOption_' . $id];
				$opt = new PriceSelectAttributeTypeOption($id, $value, $valOpt, $displayOrder);
			}
			
			if (is_object($opt)) {
				$options->add($opt);
				$displayOrder++;
			}
		}
		
		return $options;
	}
	
	public function exportKey($akey) {
		$this->load();
		$db = Loader::db();
		$type = $akey->addChild('type');
		$type->addAttribute('allow-multiple-values', $this->akSelectAllowMultipleValues);
		$type->addAttribute('display-order', $this->akSelectOptionDisplayOrder);
		$type->addAttribute('allow-other-values', $this->akSelectAllowOtherValues);
		$r = $db->Execute('select value, displayOrder, isEndUserAdded from atSelectOptions where akID = ? order by displayOrder asc', $this->getAttributeKey()->getAttributeKeyID());
		$options = $type->addChild('options');
		while ($row = $r->FetchRow()) {
			$opt = $options->addChild('option');
			$opt->addAttribute('value', $row['value']);
			$opt->addAttribute('is-end-user-added', $row['isEndUserAdded']);
		}
		return $akey;
	}
	
	public function exportValue($akn) {
		$list = $this->getSelectedOptions();
		if ($list->count() > 0) {
			$av = $akn->addChild('value');
			foreach($list as $l) {
				$av->addChild('option', (string) $l);
			}
		}
	}
	
	public function importValue(SimpleXMLElement $akv) {
		if (isset($akv->value)) {
			$vals = array();
			foreach($akv->value->children() as $ch) {
				$vals[] = (string) $ch;
			}
			return $vals;
		}
	}
	
	public function importKey($akey) {
		if (isset($akey->type)) {
			$akSelectAllowMultipleValues = $akey->type['allow-multiple-values'];
			$akSelectOptionDisplayOrder = $akey->type['display-order'];
			$akSelectAllowOtherValues = $akey->type['allow-other-values'];
			$db = Loader::db();
			$db->Replace('atSelectSettings', array(
				'akID' => $this->attributeKey->getAttributeKeyID(), 
				'akSelectAllowMultipleValues' => $akSelectAllowMultipleValues, 
				'akSelectAllowOtherValues' => $akSelectAllowOtherValues,
				'akSelectOptionDisplayOrder' => $akSelectOptionDisplayOrder
			), array('akID'), true);

			if (isset($akey->type->options)) {
				foreach($akey->type->options->children() as $option) {
					SelectAttributeTypeOption::add($this->attributeKey, $option['value'], $option['is-end-user-added']);
				}
			}
		}
	}
	
	public function form() {
		$this->load();
		$options = $this->getSelectedOptions();
		$selectedOptions = array();
		foreach($options as $opt) {
			$selectedOptions[] = $opt->getSelectAttributeOptionID();
		}
		$this->set('selectedOptions', $selectedOptions);
	}
	
	public function search() {
		$this->load();	
		$selectedOptions = $this->request('atSelectOptionID');
		if (!is_array($selectedOptions)) {
			$selectedOptions = array();
		}
		$this->set('selectedOptions', $selectedOptions);
	}
	
	public function deleteValue() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectOptionsSelected where avID = ?', array($this->getAttributeValueID()));
	}

	public function deleteKey() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		$r = $dbs->Execute('select ID from atSelectOptions where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		while ($row = $r->FetchRow()) {
			$dbs->Execute('delete from atSelectOptionsSelected where atSelectOptionID = ?', array($row['ID']));
		}
		$dbs->Execute('delete from atSelectOptions where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

	public function saveForm($data) {

		$this->load();

		if ($this->akSelectAllowOtherValues && is_array($data['atSelectNewOption'])) {
			foreach($data['atSelectNewOption'] as $newoption) {
				$optobj = PriceSelectAttributeTypeOption::add($this->attributeKey, $newoption, 1);
				$data['atSelectOptionID'][] = $optobj->getSelectAttributeOptionID();
			}
		}

		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectOptionsSelected where avID = ?', array($this->getAttributeValueID()));
		if (is_array($data['atSelectOptionID'])) {
			foreach($data['atSelectOptionID'] as $optID) {
				if ($optID > 0) {
					$dbs->Execute('insert into atSelectOptionsSelected (avID, atSelectOptionID) values (?, ?)', array($this->getAttributeValueID(), $optID));
					if ($this->akSelectAllowMultipleValues == false) {
						break;
					}
				}
			}
		}
	}
	
	// Sets select options for a particular attribute
	// If the $value == string, then 1 item is selected
	// if array, then multiple, but only if the attribute in question is a select multiple
	// Note, items CANNOT be added to the pool (even if the attribute allows it) through this process.
	public function saveValue($value) {
		$this->saveForm($value);
	}

	
	public function getDisplayValue() {
		$list = $this->getSelectedOptions();
		$html = '';
		foreach($list as $l) {
			$html .= $l . ', ';
		}
		return $html;
	}

	
	public function searchForm($list) {
		
		$options = $this->request('atSelectOptionID');
		$optionText = array();
		
		//var_dump($_GET['akID'][$this->attributeKey->getAttributeKeyID()]);
	
		$dbs = Loader::db();
		$tbl = $this->attributeKey->getIndexedSearchTable();
		if (!is_array($options)) {
			return $list;
		}
		foreach($options as $id) {
			if ($id > 0) {
				$opt = PriceSelectAttributeTypeOption::getByID($id);
				$optionText[] = $opt->getSelectAttributeOptionValue();
			}
		}
		if (count($optionText) == 0) {
			return false;
		}
		
		$i = 0;
		foreach($optionText as $val) {
			$val = $dbs->quote('%||' . $val . '||%');
			$multiString .= 'REPLACE(' . $tbl . '.ak_' . $this->attributeKey->getAttributeKeyHandle() . ', "\n", "||") like ' . $val . ' ';
			if (($i + 1) < count($optionText)) {
				$multiString .= 'OR ';
			}
			$i++;
		}
		$list->filter(false, '(' . $multiString . ')');
		return $list;
	}
	
	public function getValue() {
		$list = $this->getSelectedOptions();
		if ($list->count() == 1) {
			return $list->get(0);
		}
		return $list;	
	}
	
	public function getSearchIndexValue() {
		$str = "\n";
		$list = $this->getSelectedOptions();
		foreach($list as $l) {
			$str .= $l . "\n";
		}
		return $str;
	}
	
	public function getSelectedOptions() {
		if (!isset($this->akSelectOptionDisplayOrder)) {
			$this->load();
		}
		$dbs = Loader::db();
		switch($this->akSelectOptionDisplayOrder) {
			case 'popularity_desc':
				$options = $dbs->GetAll("select ID, value, displayOrder, (select count(s2.atSelectOptionID) from atSelectOptionsSelected s2 where s2.atSelectOptionID = ID) as total from atSelectOptionsSelected inner join atSelectOptions on atSelectOptionsSelected.atSelectOptionID = atSelectOptions.ID where avID = ? order by total desc, value asc", array($this->getAttributeValueID()));
				break;
			case 'alpha_asc':
				$options = $dbs->GetAll("select ID, value, displayOrder from atSelectOptionsSelected inner join atSelectOptions on atSelectOptionsSelected.atSelectOptionID = atSelectOptions.ID where avID = ? order by value asc", array($this->getAttributeValueID()));
				break;
			default:
				$options = $dbs->GetAll("select ID, value, displayOrder from atSelectOptionsSelected inner join atSelectOptions on atSelectOptionsSelected.atSelectOptionID = atSelectOptions.ID where avID = ? order by displayOrder asc", array($this->getAttributeValueID()));
				break;
		}
		$dbs = Loader::db();
		$list = new PriceSelectAttributeTypeOptionList();
		foreach($options as $row) {
			$opts = explode(':^:',$row['value']);
			$opt = new PriceSelectAttributeTypeOption($row['ID'], $opts[0],$opts[1], $row['displayOrder']);
			$list->add($opt);
		}
		return $list;
	}
	
	/**
	 * returns a list of available options optionally filtered by an sql $like statement ex: startswith%
	 * @param string $like
	 * @return PriceSelectAttributeTypeOptionList
	 */
	public function getOptions($like = NULL) {
		if (!isset($this->akSelectOptionDisplayOrder)) {
			$this->load();
		}
		$dbs = Loader::db();
		switch($this->akSelectOptionDisplayOrder) {
			case 'popularity_desc':
				if(isset($like) && strlen($like)) {
					$r = $dbs->Execute('select ID, value, displayOrder, count(atSelectOptionsSelected.atSelectOptionID) as total 
						from atSelectOptions left join atSelectOptionsSelected on (atSelectOptions.ID = atSelectOptionsSelected.atSelectOptionID) 
						where akID = ? AND atSelectOptions.value LIKE ? group by ID order by total desc, value asc', array($this->attributeKey->getAttributeKeyID(),$like));
				} else {
					$r = $dbs->Execute('select ID, value, displayOrder, count(atSelectOptionsSelected.atSelectOptionID) as total 
						from atSelectOptions left join atSelectOptionsSelected on (atSelectOptions.ID = atSelectOptionsSelected.atSelectOptionID) 
						where akID = ? group by ID order by total desc, value asc', array($this->attributeKey->getAttributeKeyID()));
				}
				break;
			case 'alpha_asc':
				if(isset($like) && strlen($like)) {
					$r = $dbs->Execute('select ID, value, displayOrder from atSelectOptions where akID = ? AND atSelectOptions.value LIKE ? order by value asc', array($this->attributeKey->getAttributeKeyID(),$like));
				} else {
					$r = $dbs->Execute('select ID, value, displayOrder from atSelectOptions where akID = ? order by value asc', array($this->attributeKey->getAttributeKeyID()));
				}
				break;
			default:
				if(isset($like) && strlen($like)) {
					$r = $dbs->Execute('select ID, value, displayOrder from atSelectOptions where akID = ? AND atSelectOptions.value LIKE ? order by displayOrder asc', array($this->attributeKey->getAttributeKeyID(),$like));
				} else {
					$r = $dbs->Execute('select ID, value, displayOrder from atSelectOptions where akID = ? order by displayOrder asc', array($this->attributeKey->getAttributeKeyID()));
				}
				break;
		}
		$options = new PriceSelectAttributeTypeOptionList();
		while ($row = $r->FetchRow()) {
			$opts = explode(':^:',$row['value']);
			$opt = new PriceSelectAttributeTypeOption($row['ID'],$opts[0],$opts[1], $row['displayOrder']);
			$options->add($opt);
		}
		return $options;
	}
		
	public function validateKey($args) {
		$e = parent::validateKey($args);
		
		// additional validation for select type
		
		$vals = $this->getPriceSelectFromPost();

		if ($vals->count() < 2 ) {
			$e->add(t('A select attribute type must have at least two values, or must allow users to add to it.'));
		}
		
		return $e;
	}

	public function saveKey($data) {

		$ak = $this->getAttributeKey();
		
		$dbs = Loader::db();

		$initialOptionSet = $this->getOptions();
		$selectedPostValues = $this->getPriceSelectFromPost();
		
		$akSelectAllowMultipleValues = $data['akSelectAllowMultipleValues'];
		$akSelectAllowOtherValues = $data['akSelectAllowOtherValues'];
		$akSelectOptionDisplayOrder = $data['akSelectOptionDisplayOrder'];
		
		if ($data['akSelectAllowMultipleValues'] != 1) {
			$akSelectAllowMultipleValues = 0;
		}
		if ($data['akSelectAllowOtherValues'] != 1) {
			$akSelectAllowOtherValues = 0;
		}
		if (!in_array($data['akSelectOptionDisplayOrder'], array('display_asc', 'alpha_asc', 'popularity_desc'))) {
			$akSelectOptionDisplayOrder = 'display_asc';
		}
				
		// now we have a collection attribute key object above.
		$dbs->Replace('atSelectSettings', array(
			'akID' => $ak->getAttributeKeyID(), 
			'akSelectAllowMultipleValues' => $akSelectAllowMultipleValues, 
			'akSelectAllowOtherValues' => $akSelectAllowOtherValues,
			'akSelectOptionDisplayOrder' => $akSelectOptionDisplayOrder
		), array('akID'), true);
		
		// Now we add the options
		$newOptionSet = new PriceSelectAttributeTypeOptionList();
		$displayOrder = 0;
		foreach($selectedPostValues as $option) {
			$opt = $option->saveOrCreate($ak);
			if ($akSelectOptionDisplayOrder == 'display_asc') {
				$opt->setDisplayOrder($displayOrder);
			}
			$newOptionSet->add($opt);
			$displayOrder++;
		}
		
		// Now we remove all options that appear in the 
		// old values list but not in the new
		foreach($initialOptionSet as $iopt) {
			if (!$newOptionSet->contains($iopt)) {
				$iopt->delete();
			}
		}
	}
	
}

class PriceSelectAttributeTypeOption extends Object {

	public function __construct($ID, $value, $valOpt, $displayOrder) {
		$this->ID = $ID;
		$this->value = $value;
		$this->valOpt = $valOpt;
		$this->displayOrder = $displayOrder;	
	}
	
	public function getSelectAttributeOptionID() {return $this->ID;}
	public function getSelectAttributeOptionValue() {return $this->value;}
	public function getSelectAttributeOptionDisplayOrder() {return $this->displayOrder;}
	public function getSelectAttributeOptionTemporaryID() {return $this->tempID;}
	public function getSelectAttributeOptionValueOption() {return $this->valOpt;}
	
	public function __toString() {return $this->value;}
	
	public static function add($ak, $option, $isEndUserAdded = 0) {
		$dbs = Loader::db();
		// this works because displayorder starts at zero. So if there are three items, for example, the display order of the NEXT item will be 3.
		$displayOrder = $dbs->GetOne('select count(ID) from atSelectOptions where akID = ?', array($ak->getAttributeKeyID()));			

		$v = array($ak->getAttributeKeyID(), $displayOrder, $option, $isEndUserAdded);
		$dbs->Execute('insert into atSelectOptions (akID, displayOrder, value, isEndUserAdded) values (?, ?, ?, ?)', $v);
		
		return PriceSelectAttributeTypeOption::getByID($dbs->Insert_ID());
	}
	
	public function setDisplayOrder($num) {
		$dbs = Loader::db();
		$dbs->Execute('update atSelectOptions set displayOrder = ? where ID = ?', array($num, $this->ID));
	}
	
	public static function getByID($id) {
		$dbs = Loader::db();
		$row = $dbs->GetRow("select ID, displayOrder, value from atSelectOptions where ID = ?", array($id));
		if (isset($row['ID'])) {
			$opts = explode(':^:',$row['value']);
			$obj = new PriceSelectAttributeTypeOption($row['ID'], $opts[0], $opts[1], $row['displayOrder']);
			return $obj;
		}
	}
	
	public static function getByValue($value) {
		$dbs = Loader::db();
		$row = $dbs->GetRow("select ID, displayOrder, value from atSelectOptions where value = ?", array($value));
		if (isset($row['ID'])) {
			$opts = explode(':^:',$row['value']);
			$obj = new PriceSelectAttributeTypeOption($row['ID'], $opts[0], $opts[1], $row['displayOrder']);
			return $obj;
		}
	}
	
	public function delete() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectOptions where ID = ?', array($this->ID));
		$dbs->Execute('delete from atSelectOptionsSelected where atSelectOptionID = ?', array($this->ID));
	}
	
	public function saveOrCreate($ak) {
		if ($this->tempID != false || $this->ID==0) {
			return PriceSelectAttributeTypeOption::add($ak, $this->value.':^:'.$this->valOpt);
		} else {
			$dbs = Loader::db();
			$dbs->Execute('update atSelectOptions set value = ? where ID = ?', array($this->value.':^:'.$this->valOpt, $this->ID));
			return PriceSelectAttributeTypeOption::getByID($this->ID);
		}
	}
	
}

class PriceSelectAttributeTypeOptionList extends Object implements Iterator {

	private $options = array();
	
	public function add(PriceSelectAttributeTypeOption $opt) {
		$this->options[] = $opt;
	}
	
	public function rewind() {
		reset($this->options);
	}
	
	public function current() {
		return current($this->options);
	}
	
	public function key() {
		return key($this->options);
	}
	
	public function next() {
		next($this->options);
	}
	
	public function valid() {
		return $this->current() !== false;
	}
	
	public function count() {return count($this->options);}
	
	public function contains(PriceSelectAttributeTypeOption $opt) {
		foreach($this->options as $o) {
			if ($o->getSelectAttributeOptionID() == $opt->getSelectAttributeOptionID()) {
				return true;
			}
		}
		
		return false;
	}
	
	public function get($index) {
		return $this->options[$index];
	}
	
	public function getOptions() {
		return $this->options;
	}
	
	public function __toString() {
		$str = '';
		foreach($this->options as $opt) {
			$str .= $opt->getSelectAttributeOptionValue() . "\n";
		}
		return $str;
	}


}