<?php     defined('C5_EXECUTE') or die("Access Denied.");
$fm = Loader::helper('form');
if($require_review > 0){

	echo $fm->text($this->field('username_field'), $username_field);
	echo t(' Username');
	echo '<br/>';
	echo '<br/>';
	
	echo $fm->email($this->field('email_field'), $email_field);
	echo t(' Email');
	echo '<br/>';
	echo '<br/>';

	echo $fm->password($this->field('password_field'), $password_field);
	echo t(' Password');
	echo '<br/>';
	echo '<br/>';

	echo $fm->checkbox($this->field('value'), 1, $value);
	echo t(' Yes. Register this user.');

}
?>