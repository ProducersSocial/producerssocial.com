<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class AssociateUserAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $response_message;
	public $from_email;
	public $response_subject;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$this->set('associated_user',$this->getValue());
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function search() {
		$f = Loader::helper('form');
		print $f->text($this->field('value'), $this->request('value'));
	}
	
	public function searchForm($list){
		$this->load();
		Loader::model('user_list');
		$ul = new UserList();
		$ul->filterByUsername($this->request('value'));
		$uIDs = $ul->get();
		$user = $uIDs[0];
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $user->uID, '=');
		return $list;
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atAssociateUserSettings where akID = ?', $ak->getAttributeKeyID());
		}
		
		$this->akID = $row['akID'];
		$this->association_type = $row['association_type'];

		$this->set('akID', $this->akID);
		$this->set('association_type', $this->association_type);
	}
	
	
	public function display(){
		$this->load();
		$val = $this->getValue();
		if($val){
			$ui = UserInfo::getByID($val);
			print '<a href="/index.php/dashboard/users/search?uID='.$val.'">'. $ui->getUserName() .'</a>';
		}
	}
	
	public function getDisplayValue(){
		$this->load();
		$val = $this->getValue();
		if($val){
			$ui = UserInfo::getByID($val);
			print '<a href="/index.php/dashboard/users/search?uID='.$val.'">'. $ui->getUserName() .'</a>';
		}
	}
	
	
	public function getDisplayReviewValue(){
		$this->load();
		$val = $this->getValue();
		if($val){
			$ui = UserInfo::getByID($val);
			return '<a href="/index.php/dashboard/users/search?uID='.$val.'">'. $ui->getUserName() .'</a>';
		}
	}
	

	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT uID FROM atAssociateUser WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('<h4>What is it?</h4><p>There are two types of associates.  Automatic & Manual</p><p>Automatic is a hidden form input that collects the submitting users uID (if logged in), and Manual will present a user picker field type for internal use.</p><p>This attribute will only save if no value is present.</p>');
	}
	
	
	public function saveForm($value) {

		$this->load();
		if($value['associated_user']){
			$newval = $value['associated_user'];
			$value = null;
		}elseif($_REQUEST['associated_user']){
			$newval = $_REQUEST['associated_user'];
			$value = null;
		}else{
			$newval = $value['value'];
			$value = null;
		}
		if($newval != $value){
			$db = Loader::db();
			$db->Execute('delete from atAssociateUser where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atAssociateUser (avID,uID) values (?,?)', array($this->getAttributeValueID(),$newval));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atAssociateUser where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'association_type'=>$data['association_type']
		);
		
		$db=Loader::db();
		$db->Replace('atAssociateUserSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atAssociateUserSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}