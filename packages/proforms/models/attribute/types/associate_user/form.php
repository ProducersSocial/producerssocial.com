<?php     defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php   
$fm = Loader::helper('form');
$us = Loader::helper('form/user_selector');
$u = new User();
?>
<style type="text/css">
	.associated_user .ccm-summary-selected-item{width: 250px!important;}
</style>
<?php   
$dashboard = strpos($_SERVER['REQUEST_URI'], 'dashboard');
if($association_type=='automatic' && !$dashboard){
	if($u->isLoggedIn()){
		if(!$associated_user){
			$associated_user = $u->uID;
		}
		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$associated_user);
	}
}else{
	if($u->isLoggedIn()){
		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$associated_user);
		print '<div class="associated_user">'.$us->selectUser('associated_user',$associated_user).'</div>';
	}
}
?>