<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = loader::helper('form');
?>
<fieldset>
	<div class="clearfix">
		<div class="input">
			<div class="well">
			<?php  
			switch($shipping_type){
				case 'user_address':
					$u = new User();
					if($u->isLoggedIn()){
						$ui = UserInfo::getByID($u->uID);
						$user_address = $ui->getAttribute($pull_location_from);
						if(is_array($zones)){
							foreach($zones as $zone){
								if($user_address->state_province == $zone['title']){
									$total = $zone['value'];
									print '<div id="form_tax" data-value="'.$total.'">';
									print '<input type="hidden" name="akID['.$akID.'][value]" class="price_tax" value="'.$total.'" data-value="'.$total.'"/>';
									print '<strong>'.$zone['title'].t(' Tax: ').' + </strong> '.$total.'% ( <span class="currency_symbol"></span><span class="value"></span> )';
									print '<br />';
									print '</div>';
								}
							}
						}
					}
					break;
				case 'address':
					/*
					/ not yet developed
					/ will allow the shiping price to pull by state_province of address question type
					*/
					break;
					
				case 'address':
					/*
					/ not yet developed
					/ will allow the shiping price to pull from selected option
					*/
					break;
			}
			?>
			<br style="clear: both;"/>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){


});
/*]]>*/
</script>