<?php     
defined('C5_EXECUTE') or die("Access Denied.");

class PriceTaxAddressAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPriceTaxAddressSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$zones = $this->getFees();
		
		$this->akID = $row['akID'];
		$this->zones = $zones;
		$this->shipping_type = $row['shipping_type'];
		$this->pull_location_from = $row['pull_location_from'];


		$this->set('akID', $this->akID);
		$this->set('zones',$zones);
		$this->set('shipping_type', $this->shipping_type);
		$this->set('pull_location_from', $this->pull_location_from);

	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT value FROM atPriceTaxAddress WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		if($value){
			$db = Loader::db();
			$db->Execute('delete from atPriceTaxAddress where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atPriceTaxAddress (avID,value) values (?,?)', array($this->getAttributeValueID(),$value['value']));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Price Shipping Address field is designed to pull from shipping cost from a list of sate/provinces .</p><p>Pull from a user attribute, an in-form quesiton, or serve the state/province table as a select option.</p><p>Value is in % - 20 = %20</p>
		');
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'shipping_type'=>$data['shipping_type'],
			'pull_location_from'=>$data['pull_location_from']
		);
		
		$db=Loader::db();
		$db->Replace('atPriceTaxAddressSettings', $vals, 'akID', true);
		
		$this->saveFees();
	}
	
	public function saveFees(){
		$db = Loader::db();
		$db->Execute('delete from atPriceTaxAddressItems where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		if(is_array($this->request('fee_amount'))){
			foreach($this->request('fee_amount') as $key=>$fee_type){
				$fee_title = $_REQUEST['fee_title'][$key];
				$fee_amount = $_REQUEST['fee_amount'][$key];
				$db->Execute('insert into atPriceTaxAddressItems (akID, title, value, type) values (?, ?, ?, ?)', array($this->attributeKey->getAttributeKeyID(),$fee_title,$fee_amount,$fee_type));
			}
		}
	}
	
	public function getFees(){
		$db = Loader::db();
		$values = $db->getAll("SELECT * FROM atPriceTaxAddressItems WHERE akID = ?",array($this->attributeKey->getAttributeKeyID()));
		return $values;
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceTaxAddressSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}