<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = loader::helper('form');
$i = 0;
?>
<div class="clearfix">
	<label><?php     echo t("Type of Address Tax")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <select name="shipping_type">
		  	<option value="select" <?php   if($shipping_type=='select'){echo 'selected';}?>><?php  echo t('Select Option (use table)')?></option>
		  	<option value="user_address" <?php   if($shipping_type=='user_address'){echo 'selected';}?>><?php  echo t('User Address (user attribute required)')?></option>
		  	<option value="address" <?php   if($shipping_type=='address'){echo 'selected';}?>><?php  echo t('Address (question required)')?></option>
		  	<!--<option value="address_autofill"><?php  echo t('Google Autofill Address (question required)')?></option>-->
		  </select>
		</label>
	</div>
</div>
<fieldset>
	<div class="clearfix">
		<label for="pull_location_from" class="control-label"><?php  echo t('Address Question to Use (not required for select option)')?></label>
		<div class="input">
				<?php  echo $fm->text('pull_location_from',$pull_location_from,array('placeholder'=>'address question/attribute handle'));?>
		</div>
	</div>
<fieldset>
<fieldset>
	<div class="clearfix ">
		<a href="javascript:void(0);" class="btn success ccm-button-v2-right add_zone"><i class="icon-white icon-plus-sign"></i> <?php  echo t('Add Tax Zone')?></a>
		<h2><?php  echo t('Tax Zones')?></h2>
		<div id="zones">
			<?php   if($zones){ ?>
			<?php   foreach ($zones as $zone){ $i++; ?>
			<fieldset>
				<div class="clearfix">
					<label for="client_id" class="control-label"><?php  echo t('Fee')?></label>
					<div class="input">
							<?php  echo $fm->text('fee_title[]',$zone['title'],array('placeholder'=>'Title','class'=>'input-medium'));?>
							<?php  echo $fm->text('fee_amount[]',$zone['value'],array('placeholder'=>'value','class'=>'input-small'));?>
							<a href="javascript:void(0);" onClick="$(this).parent().parent().parent().remove();"><i class="icon-trash"></i></a>
					</div>
				</div>
			<fieldset>
			<?php   } ?>
			<?php    }else{ ?>
			<span id="no_zones"><?php  echo t('No Tax Zones added yet.')?></span>
			<?php   } ?>
		</div>
	</div>
<fieldset>
<div style="display: none;" id="zone_template" data-count="<?php  echo $i?>">
	<fieldset>
		<div class="clearfix">
			<label for="client_id" class="control-label"><?php  echo t('Zone')?></label>
			<div class="input">
					<?php  echo $fm->text('title_template',$fee_title,array('placeholder'=>'Title','class'=>'input-medium'));?>
					<?php  echo $fm->text('fee_template',$fee_template,array('placeholder'=>'value','class'=>'input-small'));?>
					<a href="javascript:void(0);" onClick="$(this).parent().parent().parent().remove();"><i class="icon-trash"></i></a>
			</div>
		</div>
	<fieldset>
</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.add_zone').click(function(){
		$('#no_zones').remove();
		var temp = $('#zone_template fieldset').clone();
		temp.find('#fee_template').attr('name','fee_amount[]');
		temp.find('#title_template').attr('name','fee_title[]');
		$('#zones').append(temp);
	});
});
/*]]>*/
</script>