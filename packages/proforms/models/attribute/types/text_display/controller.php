<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class TextDisplayAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $text;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		print $this->text;
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');

		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Text to display').'</label>';
		print '		<div class="input">';
		Loader::element('editor_init');
		Loader::element('editor_config');
		Loader::element('editor_controls', array('mode'=>'full'));
		print $fm->textarea('text', $this->text, array('style' => 'width: 85%; font-family: sans-serif;', 'class' => 'ccm-advanced-editor'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atTextDisplaySettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->text = $row['text'];

		$this->set('akID', $this->akID);
		$this->set('text', $this->text);
	}
	
	public function getValue(){
		$this->load();
		return $this->text;
	}

	
	public function saveForm($value) {
		$this->load();
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'text'=>$data['text']
		);
		
		$db=Loader::db();
		$db->Replace('atTextDisplaySettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atTextDisplaySettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}