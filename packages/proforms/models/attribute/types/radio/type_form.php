<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
function getAttributeOptionHTML($v){ 
	if ($v == 'TEMPLATE') {
		$akRadioID = 'TEMPLATE_CLEAN';
		$akRadio = 'TEMPLATE';
	} else {
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akRadioID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akRadioID = $v->getSelectAttributeOptionID();
		}
		$akRadio = $v->getSelectAttributeOptionValue();
	}
		?>
		<div id="akRadioDisplay_<?php    echo $akRadioID?>" >
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akRadioID)?>')" value="<?php    echo t('Edit')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.deleteValue('<?php    echo addslashes($akRadioID)?>')" value="<?php    echo t('Delete')?>" />
			</div>			
			<span onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akRadioID)?>')" id="akRadioStatic_<?php    echo $akRadioID?>" class="leftCol"><?php    echo $akRadio ?></span>
		</div>
		<div id="akRadioEdit_<?php    echo $akRadioID?>" style="display:none">
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akRadioID)?>')" value="<?php    echo t('Cancel')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.changeValue('<?php    echo addslashes($akRadioID)?>')" value="<?php    echo t('Save')?>" />
			</div>		
			<span class="leftCol">
				<input name="akRadioOriginal_<?php    echo $akRadioID?>" type="hidden" value="<?php    echo $akRadio?>" />
				<?php     if (is_object($v) && $v->getSelectAttributeOptionTemporaryID() == false) { ?>
					<input id="akRadioExistingOption_<?php    echo $akRadioID?>" name="akRadioExistingOption_<?php    echo $akRadioID?>" type="hidden" value="<?php    echo $akRadioID?>" />
				<?php     } else { ?>
					<input id="akRadioNewOption_<?php    echo $akRadioID?>" name="akRadioNewOption_<?php    echo $akRadioID?>" type="hidden" value="<?php    echo $akRadioID?>" />
				<?php     } ?>
				<input id="akRadioField_<?php    echo $akRadioID?>" name="akRadio_<?php    echo $akRadioID?>" type="text" value="<?php    echo $akRadio?>" size="20" />
			</span>		
		</div>	
		<div class="ccm-spacer">&nbsp;</div>
<?php     } ?>

<fieldset>
<legend><?php    echo t('Select Options')?></legend>

<div class="clearfix">
<label for="akRadioOptionDisplayOrder"><?php    echo t("Option Order")?></label>
<div class="input">
	<?php     
	$displayOrderOptions = array(
		'display_asc' => t('Display Order'),
		'alpha_asc' => t('Alphabetical'),
		'popularity_desc' => t('Most Popular First')
	);
	?>

	<?php    echo $form->select('akRadioOptionDisplayOrder', $displayOrderOptions, $akRadioOptionDisplayOrder)?>
</div>
</div>

<div class="clearfix">
<label><?php    echo t('Values')?></label>
<div class="input">
	<div id="attributeValuesInterface">
	<div id="attributeValuesWrap">
	<?php    
	Loader::helper('text');
	foreach($akRadio as $v) { 
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akRadioID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akRadioID = $v->getSelectAttributeOptionID();
		}
		?>
		<div id="akRadioWrap_<?php    echo $akRadioID?>" class="akRadioWrap <?php     if ($akRadioOptionDisplayOrder == 'display_asc') { ?> akRadioWrapSortable<?php     } ?>">
			<?php    echo getAttributeOptionHTML( $v )?>
		</div>
	<?php     } ?>
	</div>
	
	<div id="akRadioWrapTemplate" class="akRadioWrap" style="display:none">
		<?php    echo getAttributeOptionHTML('TEMPLATE') ?>
	</div>
	
	<div id="addAttributeValueWrap"> 
		<input id="akRadioFieldNew" name="akRadioNew" type="text" value="<?php    echo $defaultNewOptionNm ?>" size="40" class="faint" 
		onfocus="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',0)" 
		onblur="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',1)"
		onkeypress="ccmAttributesHelper.addEnterClick(event,function(){ccmAttributesHelper.saveNewOption()})"
		 /> 
		<input class="btn" type="button" onClick="ccmAttributesHelper.saveNewOption(); $('#ccm-attribute-key-form').unbind()" value="<?php    echo t('Add') ?>" />
	</div>
	</div>

</div>
</div>


</fieldset>
<?php     if ($akRadioOptionDisplayOrder == 'display_asc') { ?>
<script type="text/javascript">
//<![CDATA[
$(function() {
	ccmAttributesHelper.makeSortable();
});
//]]>
</script>
<?php     } ?>