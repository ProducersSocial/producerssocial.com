<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class SignatureAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $is_multipart = 1;
	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();

		$html = Loader::helper('html');
		$uh = Loader::helper('concrete/urls');
		$pkg = Package::getByHandle('proforms');
		$this->addFooterItem($html->css('signature/jquery.signaturepad.css','proforms'));
		$this->addFooterItem($html->javascript('bootstrap.js'));
		$this->addFooterItem($html->javascript('json2.min.js','proforms'));
		$this->addFooterItem($html->javascript('jquery.signaturepad.js','proforms'));
		$this->addFooterItem('<!--[if lt IE 7]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addFooterItem('<!--[if lt IE 8]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addFooterItem('<!--[if lt IE 9]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$sg = Loader::helper('signature','proforms');
		print $sg->signature($this->attributeKey->getAttributeKeyID());
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$db = Loader::db();
		$handle = 'ak_'.$this->attributeKey->getAttributeKeyHandle();
		if($this->request('value') != 1){
			$list->filter(false,"$handle <> 1 OR $handle IS NULL");
		}else{
			$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		}
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->checkbox($this->field('value'), 1, $this->request('value')).' '.t('show entries that have already been published');
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		$ak->setTypeMultipart();
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atSignatureSettings where akID = ?', $ak->getAttributeKeyID());
		
		$this->akID = $row['akID'];

		$this->set('akID', $this->akID);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Signature Question Type is a great partner for the Agreement Question Type.  Requires a signature and will save a png file attached to the form.</p>
		');
	}
	
	public function getValue(){
		$db = Loader::db();
		$fID = $db->getOne("SELECT fID FROM atSignature WHERE avID = ?",array($this->getAttributeValueID()));
		return $fID;
	}
	
	public function getDisplayValue() {
		$this->load();
		$fID = $this->getValue();
		if($fID){
			return BASE_URL.File::getRelativePathFromID($fID);
		}else{
			print 'none submitted';
		}
	}
	
	public function display(){
		$this->load();
		$fID = $this->getValue();
		if($fID){
			print '<img src="'.File::getRelativePathFromID($fID).'" alt="signature"/>';
		}else{
			print '<p>'.t('No Signature Available').'</p>';
		}
	}
	
	
	public function saveForm($data) {
		if(!$data['no_import'] && $_REQUEST['output']){
			$this->load();
			
			Loader::library("file_importer","proforms");
			$fi = new FileImporter();
			
			$json = $_REQUEST['output'];
			
			$img = $this->sigJsonToImage($json);
			$ran = rand(0,2000000);
			
			$imageFile = imagepng($img, DIR_FILES_UPLOADED_STANDARD.'/incoming/signature_'.$str_name.'_'.$ran.'.png');
	
			$newFile = $fi->import(DIR_FILES_UPLOADED_STANDARD.'/incoming/signature_'.$str_name.'_'.$ran.'.png');
			
			if(is_file($imageFile)){
				imagedestroy($imageFile);
			}
			
			$fID = $newFile->getFileID();

			$data = true;
		}else{
			$fID = $data['value'];
		}
		
		if($data){		
			$db = Loader::db();
			$db->Execute('delete from atSignature where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atSignature (avID,fID) values (?,?)', array($this->getAttributeValueID(),$fID));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atSignature where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {

		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID()
		);
		
		$db=Loader::db();
		$db->Replace('atSignatureSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atSignatureSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}
	
		/**
		 *	Accepts a signature created by signature pad in Json format
		 *	Converts it to an image resource
		 *	The image resource can then be changed into png, jpg whatever PHP GD supports
		 *
		 *	To create a nicely anti-aliased graphic the signature is drawn 12 times it's original size then shrunken
		 *
		 *	@param	string|array	$json
		 *	@param	array	$options	OPTIONAL; the options for image creation
		 *		imageSize => array(width, height)
		 *		bgColour => array(red, green, blue)
		 *		penWidth => int
		 *		penColour => array(red, green, blue)
		 *
		 *	@return	object
		 */
		public function sigJsonToImage($json, $options = array())
		{
			$defaultOptions = array(
				'imageSize' => array(198, 55)
				,'bgColour' => array(0xff, 0xff, 0xff)
				,'penWidth' => 2
				,'penColour' => array(0x14, 0x53, 0x94)
				,'drawMultiplier'=> 12
			);
			
			$options = array_merge($defaultOptions, $options);
			
			$img = imagecreatetruecolor($options['imageSize'][0] * $options['drawMultiplier'], $options['imageSize'][1] * $options['drawMultiplier']);
			$bg = imagecolorallocate($img, $options['bgColour'][0], $options['bgColour'][1], $options['bgColour'][2]);
			$pen = imagecolorallocate($img, $options['penColour'][0], $options['penColour'][1], $options['penColour'][2]);
			imagefill($img, 0, 0, $bg);
			
			if(is_string($json))
				$jh = Loader::helper('json');
				$json = $jh->decode(stripslashes($json));
			
			foreach($json as $v)
				$this->drawThickLine($img, $v->lx * $options['drawMultiplier'], $v->ly * $options['drawMultiplier'], $v->mx * $options['drawMultiplier'], $v->my * $options['drawMultiplier'], $pen, $options['penWidth'] * ($options['drawMultiplier'] / 2));
			
			$imgDest = imagecreatetruecolor($options['imageSize'][0], $options['imageSize'][1]);
			imagecopyresampled($imgDest, $img, 0, 0, 0, 0, $options['imageSize'][0], $options['imageSize'][0], $options['imageSize'][0] * $options['drawMultiplier'], $options['imageSize'][0] * $options['drawMultiplier']);
			
			imagedestroy($img);
			
			return $imgDest;
		}
		
		/**
		 *	Draws a thick line
		 *	Changing the thickness of a line using imagesetthickness doesn't produce as nice of result
		 *
		 *	@param	object	$img
		 *	@param	int		$startX
		 *	@param	int		$startY
		 *	@param	int		$endX
		 *	@param	int		$endY
		 *	@param	object	$colour
		 *	@param	int		$thickness
		 *
		 *	@return	void
		 */
		public function drawThickLine($img, $startX, $startY, $endX, $endY, $colour, $thickness) 
		{
			$angle = (atan2(($startY - $endY), ($endX - $startX))); 
		
			$dist_x = $thickness * (sin($angle));
			$dist_y = $thickness * (cos($angle));
			
			$p1x = ceil(($startX + $dist_x));
			$p1y = ceil(($startY + $dist_y));
			$p2x = ceil(($endX + $dist_x));
			$p2y = ceil(($endY + $dist_y));
			$p3x = ceil(($endX - $dist_x));
			$p3y = ceil(($endY - $dist_y));
			$p4x = ceil(($startX - $dist_x));
			$p4y = ceil(($startY - $dist_y));
			
			$array = array(0=>$p1x, $p1y, $p2x, $p2y, $p3x, $p3y, $p4x, $p4y);
			imagefilledpolygon($img, $array, (count($array)/2), $colour);
		}

}