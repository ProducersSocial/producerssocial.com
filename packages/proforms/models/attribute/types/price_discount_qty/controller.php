<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class PriceDiscountQtyAttributeTypeController extends AttributeTypeController  {

	private $akSelectAllowMultipleValues;
	private $akSelectAllowOtherValues;
	private $akSelectOptionDisplayOrder;
	
	public $hideLabel = 1; // no lable needed

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function type_form() {
		$this->load();
		$at = $this->attributeType;
		//$path1 = $at->getAttributeTypeFileURL('type_form.js');
		//$path2 = $at->getAttributeTypeFileURL('type_form.css');
		//$this->addFooterItem(Loader::helper('html')->javascript($path1));
		//$this->addFooterItem(Loader::helper('html')->css($path2));
		$this->set('form', Loader::helper('form'));		

		//$akPriceDiscountQty = $this->getPriceDiscountQtyFromPost();
		//$this->set('akPriceDiscountQty', $akPriceDiscountQty);
		
		if ($this->isPost()) {
			$akPriceDiscountQty = $this->getPriceDiscountQtyFromPost();
			$this->set('akPriceDiscountQty', $akPriceDiscountQty);
		} else if (isset($this->attributeKey)) {
			$options = $this->getOptions();
			$this->set('akPriceDiscountQty', $options);
		} else {
			$this->set('akPriceDiscountQty', array());
		}
	}
	
	protected function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$dbs = Loader::db();
		$row = $dbs->GetRow('select akSelectAllowMultipleValues, akSelectOptionDisplayOrder, akSelectAllowOtherValues from atSelectSettings where akID = ?', $ak->getAttributeKeyID());
		$this->akSelectAllowMultipleValues = $row['akSelectAllowMultipleValues'];
		$this->akSelectAllowOtherValues = $row['akSelectAllowOtherValues'];
		$this->akSelectOptionDisplayOrder = $row['akSelectOptionDisplayOrder'];

		$this->set('akSelectAllowMultipleValues', $this->akSelectAllowMultipleValues);
		$this->set('akSelectAllowOtherValues', $this->akSelectAllowOtherValues);			
		$this->set('akSelectOptionDisplayOrder', $this->akSelectOptionDisplayOrder);	
		$this->set('akID',$this->getAttributeKey()->getAttributeKeyID());	
	}

	public function duplicateKey($newAK) {
		$this->load();
		$dbs = Loader::db();
		$dbs->Execute('insert into atSelectSettings (akID, akSelectAllowMultipleValues, akSelectOptionDisplayOrder, akSelectAllowOtherValues) values (?, ?, ?, ?)', array($newAK->getAttributeKeyID(), $this->akSelectAllowMultipleValues, $this->akSelectOptionDisplayOrder, $this->akSelectAllowOtherValues));	
		$r = $dbs->Execute('select value, displayOrder, isEndUserAdded from atSelectOptions where akID = ?', $this->getAttributeKey()->getAttributeKeyID());
		while ($row = $r->FetchRow()) {
			$dbs->Execute('insert into atSelectOptions (akID, value, displayOrder, isEndUserAdded) values (?, ?, ?, ?)', array(
				$newAK->getAttributeKeyID(),
				$row['value'],
				$row['displayOrder'],
				$row['isEndUserAdded']
			));
		}
	}
	
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Discount Qty QuestionType allows you to add price break discounts based on the QTY fields of a payment form.</p><p>Only one of this QuestionType should be used per form</p>
		');
	}
	
	public function checkDiscount($code){
		$opts = $this->getOptions();
		foreach($opts as $opt){
			if($opt->getSelectAttributeOptionValueOption() == $code){
				return $opt->getSelectAttributeOptionValue();
			}
		}
		return false;
	}
	
	
	
	private function getPriceDiscountQtyFromPost() {
		$options = new PriceDiscountQtyAttributeTypeOptionList();
		$displayOrder = 0;		

		foreach($_POST as $key => $value) {
			if( !strstr($key,'akPriceDiscountQty_') || $value=='TEMPLATE' ) continue; 
			$opt = false;
			// strip off the prefix to get the ID
			$temps = explode('_',$key);
			$id = $temps[1];
			// now we determine from the post whether this is a new option
			// or an existing. New ones have this value from in the akPriceDiscountQtyNewOption_ post field
			if ($_POST['akPriceDiscountQtyNewOption_' . $id] == $id) {
				$valOpt = $_POST['akPriceDiscountQtyOption_' . $id];
				$opt = new PriceDiscountQtyAttributeTypeOption(0, $value, $valOpt,  $displayOrder);
				$opt->tempID = $id;
			} else if ($_POST['akPriceDiscountQtyExistingOption_' . $id] == $id) {
				$valOpt = $_POST['akPriceDiscountQtyOption_' . $id];
				$opt = new PriceDiscountQtyAttributeTypeOption($id, $value, $valOpt, $displayOrder);
			}
			
			if (is_object($opt)) {
				$options->add($opt);
				$displayOrder++;
			}
		}
		
		return $options;
	}
	
	
	public function form() {
		$this->load();
		$options = $this->getOptions();
		$this->set('options', $options);
	}
	
	public function search() {
		$this->load();	
		$selectedOptions = $this->request('atSelectOptionID');
		if (!is_array($selectedOptions)) {
			$selectedOptions = array();
		}
		$this->set('selectedOptions', $selectedOptions);
	}
	
	public function deleteValue() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectOptionsSelected where avID = ?', array($this->getAttributeValueID()));
	}

	public function deleteKey() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		$r = $dbs->Execute('select ID from atSelectOptions where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		while ($row = $r->FetchRow()) {
			$dbs->Execute('delete from atSelectOptionsSelected where atSelectOptionID = ?', array($row['ID']));
		}
		$dbs->Execute('delete from atSelectOptions where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

	public function saveForm($data) {

		$this->load();

		if ($this->akSelectAllowOtherValues && is_array($data['atSelectNewOption'])) {
			foreach($data['atSelectNewOption'] as $newoption) {
				$optobj = PriceDiscountQtyAttributeTypeOption::add($this->attributeKey, $newoption, 1);
				$data['atSelectOptionID'][] = $optobj->getSelectAttributeOptionID();
			}
		}

		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectOptionsSelected where avID = ?', array($this->getAttributeValueID()));
		if (is_array($data['atSelectOptionID'])) {
			foreach($data['atSelectOptionID'] as $optID) {
				if ($optID > 0) {
					$dbs->Execute('insert into atSelectOptionsSelected (avID, atSelectOptionID) values (?, ?)', array($this->getAttributeValueID(), $optID));
					if ($this->akSelectAllowMultipleValues == false) {
						break;
					}
				}
			}
		}
	}
	
	// Sets select options for a particular attribute
	// If the $value == string, then 1 item is selected
	// if array, then multiple, but only if the attribute in question is a select multiple
	// Note, items CANNOT be added to the pool (even if the attribute allows it) through this process.
	public function saveValue($value) {
		$this->saveForm($value);
	}

	
	public function getDisplayValue() {
		$list = $this->getSelectedOptions();
		$html = '';
		foreach($list as $l) {
			$html .= $l . ', ';
		}
		return $html;
	}
	
	public function validateForm($p) {
		$this->load();
		$options = $this->request('atSelectOptionID');
		if ($this->akSelectAllowMultipleValues) {
			return count($options) > 0;
		} else {
			if ($options[0] != false) {
				return $options[0] > 0;
			}
		}
		return false;
	}
	
	public function searchForm($list) {
		
		$options = $this->request('atSelectOptionID');
		$optionText = array();
		
		//var_dump($_GET['akID'][$this->attributeKey->getAttributeKeyID()]);
	
		$dbs = Loader::db();
		$tbl = $this->attributeKey->getIndexedSearchTable();
		if (!is_array($options)) {
			return $list;
		}
		foreach($options as $id) {
			if ($id > 0) {
				$opt = PriceDiscountQtyAttributeTypeOption::getByID($id);
				$optionText[] = $opt->getSelectAttributeOptionValue();
			}
		}
		if (count($optionText) == 0) {
			return false;
		}
		
		$i = 0;
		foreach($optionText as $val) {
			$val = $dbs->quote('%||' . $val . '||%');
			$multiString .= 'REPLACE(' . $tbl . '.ak_' . $this->attributeKey->getAttributeKeyHandle() . ', "\n", "||") like ' . $val . ' ';
			if (($i + 1) < count($optionText)) {
				$multiString .= 'OR ';
			}
			$i++;
		}
		$list->filter(false, '(' . $multiString . ')');
		return $list;
	}
	
	public function getValue() {
		$list = $this->getSelectedOptions();
		if ($list->count() == 1) {
			return $list->get(0);
		}
		return $list;	
	}
	
	public function getSearchIndexValue() {
		$str = "\n";
		$list = $this->getSelectedOptions();
		foreach($list as $l) {
			$str .= $l . "\n";
		}
		return $str;
	}
	
	public function getSelectedOptions() {
		if (!isset($this->akSelectOptionDisplayOrder)) {
			$this->load();
		}
		$dbs = Loader::db();
		$options = $dbs->GetAll("select ID, value, displayOrder from atSelectOptionsSelected inner join atSelectOptions on atSelectOptionsSelected.atSelectOptionID = atSelectOptions.ID where avID = ? order by displayOrder asc", array($this->getAttributeValueID()));
		$dbs = Loader::db();
		$list = new PriceDiscountQtyAttributeTypeOptionList();
		foreach($options as $row) {
			$opts = explode(':^:',$row['value']);
			$opt = new PriceDiscountQtyAttributeTypeOption($row['ID'], $opts[0],$opts[1], $row['displayOrder']);
			$list->add($opt);
		}
		return $list;
	}
	
	/**
	 * returns a list of available options optionally filtered by an sql $like statement ex: startswith%
	 * @param string $like
	 * @return PriceDiscountQtyAttributeTypeOptionList
	 */
	public function getOptions($like = NULL) {
		if (!isset($this->akSelectOptionDisplayOrder)) {
			$this->load();
		}
		$dbs = Loader::db();

		if(isset($like) && strlen($like)) {
			$r = $dbs->Execute('select ID, value, displayOrder from atSelectOptions where akID = ? AND atSelectOptions.value LIKE ? order by displayOrder asc', array($this->attributeKey->getAttributeKeyID(),$like));
		} else {
			$r = $dbs->Execute('select ID, value, displayOrder from atSelectOptions where akID = ? order by displayOrder asc', array($this->attributeKey->getAttributeKeyID()));
		}

		$options = new PriceDiscountQtyAttributeTypeOptionList();
		while ($row = $r->FetchRow()) {
			$opts = explode(':^:',$row['value']);
			$opt = new PriceDiscountQtyAttributeTypeOption($row['ID'],$opts[0],$opts[1], $row['displayOrder']);
			$options->add($opt);
		}
		return $options;
	}
		

	public function saveKey($data) {

		$ak = $this->getAttributeKey();
		
		$dbs = Loader::db();

		$initialOptionSet = $this->getOptions();
		$selectedPostValues = $this->getPriceDiscountQtyFromPost();
		
		$akSelectAllowMultipleValues = $data['akSelectAllowMultipleValues'];
		$akSelectAllowOtherValues = $data['akSelectAllowOtherValues'];
		$akSelectOptionDisplayOrder = $data['akSelectOptionDisplayOrder'];
		
		if ($data['akSelectAllowMultipleValues'] != 1) {
			$akSelectAllowMultipleValues = 0;
		}
		if ($data['akSelectAllowOtherValues'] != 1) {
			$akSelectAllowOtherValues = 0;
		}
		if (!in_array($data['akSelectOptionDisplayOrder'], array('display_asc', 'alpha_asc', 'popularity_desc'))) {
			$akSelectOptionDisplayOrder = 'display_asc';
		}
				
		// now we have a collection attribute key object above.
		$dbs->Replace('atSelectSettings', array(
			'akID' => $ak->getAttributeKeyID(), 
			'akSelectAllowMultipleValues' => $akSelectAllowMultipleValues, 
			'akSelectAllowOtherValues' => $akSelectAllowOtherValues,
			'akSelectOptionDisplayOrder' => $akSelectOptionDisplayOrder
		), array('akID'), true);
		
		// Now we add the options
		$newOptionSet = new PriceDiscountQtyAttributeTypeOptionList();
		$displayOrder = 0;
		foreach($selectedPostValues as $option) {
			$opt = $option->saveOrCreate($ak);
			if ($akSelectOptionDisplayOrder == 'display_asc') {
				$opt->setDisplayOrder($displayOrder);
			}
			$newOptionSet->add($opt);
			$displayOrder++;
		}
		
		// Now we remove all options that appear in the 
		// old values list but not in the new
		foreach($initialOptionSet as $iopt) {
			if (!$newOptionSet->contains($iopt)) {
				$iopt->delete();
			}
		}
	}
	
}

class PriceDiscountQtyAttributeTypeOption extends Object {

	public function __construct($ID, $value, $valOpt, $displayOrder) {
		$this->ID = $ID;
		$this->value = $value;
		$this->valOpt = $valOpt;
		$this->displayOrder = $displayOrder;	
	}
	
	public function getSelectAttributeOptionID() {return $this->ID;}
	public function getSelectAttributeOptionValue() {return $this->value;}
	public function getSelectAttributeOptionDisplayOrder() {return $this->displayOrder;}
	public function getSelectAttributeOptionTemporaryID() {return $this->tempID;}
	public function getSelectAttributeOptionValueOption() {return $this->valOpt;}
	
	public function __toString() {return $this->value;}
	
	public static function add($ak, $option, $isEndUserAdded = 0) {
		$dbs = Loader::db();
		// this works because displayorder starts at zero. So if there are three items, for example, the display order of the NEXT item will be 3.
		$displayOrder = $dbs->GetOne('select count(ID) from atSelectOptions where akID = ?', array($ak->getAttributeKeyID()));			

		$v = array($ak->getAttributeKeyID(), $displayOrder, $option, $isEndUserAdded);
		$dbs->Execute('insert into atSelectOptions (akID, displayOrder, value, isEndUserAdded) values (?, ?, ?, ?)', $v);
		
		return PriceDiscountQtyAttributeTypeOption::getByID($dbs->Insert_ID());
	}
	
	public function setDisplayOrder($num) {
		$dbs = Loader::db();
		$dbs->Execute('update atSelectOptions set displayOrder = ? where ID = ?', array($num, $this->ID));
	}
	
	public static function getByID($id) {
		$dbs = Loader::db();
		$row = $dbs->GetRow("select ID, displayOrder, value from atSelectOptions where ID = ?", array($id));
		if (isset($row['ID'])) {
			$opts = explode(':^:',$row['value']);
			$obj = new PriceDiscountQtyAttributeTypeOption($row['ID'], $opts[0], $opts[1], $row['displayOrder']);
			return $obj;
		}
	}
	
	public static function getByValue($value) {
		$dbs = Loader::db();
		$row = $dbs->GetRow("select ID, displayOrder, value from atSelectOptions where value = ?", array($value));
		if (isset($row['ID'])) {
			$opts = explode(':^:',$row['value']);
			$obj = new PriceDiscountQtyAttributeTypeOption($row['ID'], $opts[0], $opts[1], $row['displayOrder']);
			return $obj;
		}
	}
	
	public function delete() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSelectOptions where ID = ?', array($this->ID));
		$dbs->Execute('delete from atSelectOptionsSelected where atSelectOptionID = ?', array($this->ID));
	}
	
	public function saveOrCreate($ak) {
		if ($this->tempID != false || $this->ID==0) {
			return PriceDiscountQtyAttributeTypeOption::add($ak, $this->value.':^:'.$this->valOpt);
		} else {
			$dbs = Loader::db();
			$dbs->Execute('update atSelectOptions set value = ? where ID = ?', array($this->value.':^:'.$this->valOpt, $this->ID));
			return PriceDiscountQtyAttributeTypeOption::getByID($this->ID);
		}
	}
	
}

class PriceDiscountQtyAttributeTypeOptionList extends Object implements Iterator {

	private $options = array();
	
	public function add(PriceDiscountQtyAttributeTypeOption $opt) {
		$this->options[] = $opt;
	}
	
	public function rewind() {
		reset($this->options);
	}
	
	public function current() {
		return current($this->options);
	}
	
	public function key() {
		return key($this->options);
	}
	
	public function next() {
		next($this->options);
	}
	
	public function valid() {
		return $this->current() !== false;
	}
	
	public function count() {return count($this->options);}
	
	public function contains(PriceDiscountQtyAttributeTypeOption $opt) {
		foreach($this->options as $o) {
			if ($o->getSelectAttributeOptionID() == $opt->getSelectAttributeOptionID()) {
				return true;
			}
		}
		
		return false;
	}
	
	public function get($index) {
		return $this->options[$index];
	}
	
	public function getOptions() {
		return $this->options;
	}
	
	public function __toString() {
		$str = '';
		foreach($this->options as $opt) {
			$str .= $opt->getSelectAttributeOptionValue() . "\n";
		}
		return $str;
	}


}