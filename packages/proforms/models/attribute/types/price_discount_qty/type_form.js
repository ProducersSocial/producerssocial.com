var ccmAttributesHelper={   
	valuesBoxDisabled:function(typeSelect){
		var attrValsInterface=document.getElementById('attributeValuesInterface')
		var requiredVals=document.getElementById('reqValues');
		var allowOther=document.getElementById('allowOtherValuesWrap');
		var offMsg=document.getElementById('attributeValuesOffMsg');
		if (typeSelect.value == 'SELECT' || typeSelect.value == 'SELECT_MULTIPLE') {
			attrValsInterface.style.display='block';
			requiredVals.style.display='inline'; 
			if(allowOther) allowOther.style.display='block';
			offMsg.style.display='none';			
		} else {  
			requiredVals.style.display='none'; 
			attrValsInterface.style.display='none';
			if(allowOther) allowOther.style.display='none';
			offMsg.style.display='block'; 
		}
		return false;	
	},  
	
	deleteValue:function(val){
		if(confirm(ccmi18n.deleteAttributeValue)) {
			$('#akPriceDiscountQtyWrap_'+val).remove();				
		}
	},
	
	editValue:function(val){ 
		if($('#akPriceDiscountQtyDisplay_'+val).css('display')!='none'){
			$('#akPriceDiscountQtyDisplay_'+val).css('display','none');
			$('#akPriceDiscountQtyEdit_'+val).css('display','block');		
		}else{
			$('#akPriceDiscountQtyDisplay_'+val).css('display','block');
			$('#akPriceDiscountQtyEdit_'+val).css('display','none');
			//$('#akPriceDiscountQtyField_'+val).val( $('#akPriceDiscountQtyStatic_'+val).html() )
		}
	},
	
	changeValue:function(val){ 
		$('#akPriceDiscountQtyStatic_'+val).html( $('#akPriceDiscountQtyField_'+val).val() + '<i> (' +  $('#akPriceDiscountQtyOptionField_'+val).val() + ')</i>' );
		this.editValue(val);
	},
	
	makeSortable: function() {
		$("div#attributeValuesWrap").sortable({
			cursor: 'move',
			opacity: 0.5
		});
		return false;
	},
	
	saveNewOption:function(){
		var newValF=$('#akPriceDiscountQtyFieldNew');
		var val=newValF.val();
		if(val=='') {
			return;
		}
		var newOptF=$('#akPriceDiscountQtyOptionFieldNew');
		var opt= newOptF.val();
		
		var ts = 't' + new Date().getTime();
		var template=document.getElementById('akPriceDiscountQtyWrapTemplate'); 
		var newRowEl=document.createElement('div');
		newRowEl.innerHTML=template.innerHTML.replace(/template_clean/ig,ts).replace(/template/ig,val).replace(/temp_option/ig,opt);
		newRowEl.id="akPriceDiscountQtyWrap_"+ts;
		newRowEl.className='akPriceDiscountQtyWrap';
		$('#attributeValuesWrap').append(newRowEl);		
		newValF.val(''); 
		newOptF.val(''); 
		return false;
	},
	
	clrInitTxt:function(field,initText,removeClass,blurred){
		if(blurred && field.value==''){
			field.value=initText;
			$(field).addClass(removeClass);
			return;	
		}
		if(field.value==initText) field.value='';
		if($(field).hasClass(removeClass)) $(field).removeClass(removeClass);
		return false;
	},
	
	addEnterClick:function(e,fn){
		var form = $("#ccm-attribute-key-form");
		form.submit(function() {return false;});
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 13 && typeof(fn)=='function' ) {
			fn();
			setTimeout(function() { 
				form.unbind();
			}, 100);
		}
		return false;
	}
}
