<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class HiddenValueAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $hidden_value;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');

		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$this->getValue());
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');

		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Hidden Value').'</label>';
		print '		<div class="input">';
		print $fm->text('hidden_value',$this->hidden_value);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atHiddenValueSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->hidden_value = $row['hidden_value'];

		$this->set('akID', $this->akID);
		$this->set('hidden_value', $this->hidden_value);
	}
	
	public function getValue(){
	    $this->load();
		return $this->hidden_value;
	}

	
	public function saveForm($value) {
		$this->load();
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'hidden_value'=>$data['hidden_value']
		);
		
		$db=Loader::db();
		$db->Replace('atHiddenValueSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atHiddenValueSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}