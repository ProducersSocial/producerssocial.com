<?php    
defined('C5_EXECUTE') or die("Access Denied.");
$fm = Loader::helper('form');
$u = new User();
$fieldFormName = 'akID['.$akID.'][value]';
?>
<br style="clear: both;"/>
<style type="text/css">
.icon {
display: block;
float: left;
height:20px;
width:20px;
background-image:url('<?php      echo ASSETS_URL_IMAGES?>/icons_sprite.png'); /*your location of the image may differ*/
}
.edit {background-position: -22px -2225px;margin-right: 6px!important;}
.copy {background-position: -22px -439px;margin-right: 6px!important;}
.delete {background-position: -22px -635px;}
.qsc_credit_review_comments .clearfix .input{float: right; width: 700px;}
.comments .input{min-width: 360px!important;}
</style>
<b><a href="<?php   echo Loader::helper('concrete/urls')->getToolsURL('proforms/attributes/comments/add_comment.php','proforms')?>?akID=<?php   echo $akID?>&avID=<?php   echo $avID?>" alt="file_path" class="dialog-launch btn success" dialog-width="60%" dialog-height="480" dialog-modal="true" dialog-title="<?php   echo t('Add Comment')?>" dialog-on-close="fetchComments();" onClick="javascript:;" style="flaot: right; margin-bottom: 22px;"><?php   echo t('Add Comment')?></a></b>
<?php   echo $fm->hidden($fieldFormName,true)?>
<table class="ccm-results-list">
	<thead> 
		<tr>
			<th width="90"><?php   echo t('User Comments')?></th>
		</tr>
	</thead>
	<tbody class="comment_results">
	<?php   
	if(is_array($posts)){
		foreach($posts as $post){
			$ui = UserInfo::getByID($post['uID']);
			if($ui){
				$name = $ui->getUserFirstName().' '.$ui->getUserLastName();
			}
			?>
			<tr>
				<td>
					<h6><?php   echo $name?></h6>
					<?php    if($u->uID == $post['uID']){ ?>
					<a href="javascript:removeComment(<?php   echo $post['uID']?>,'<?php   echo $post['date']?>');" style="float:right" class="icon delete"></a>
					<?php    } ?>
					<i style="font-size: 9px;"><?php   echo date('M d, Y @ g:ia',strtotime($post['date']))?></i><br/>
					<?php   echo $post['comment']?>
				</td>
			</tr>
			<?php   
		}
	}
	?>
	</tbody>
</table>

<script type="text/javascript">
/*<![CDATA[*/
	function fetchComments(){
		var url = '<?php   echo BASE_URL.Loader::helper('concrete/urls')->getToolsURL('proforms/attributes/comments/update_comments.php','proforms')?>';
		$.ajax({
			url: url,
			data: {
				avID: '<?php   echo $avID?>'
			}
		}).done( function(html){
			$('.comment_results').html(html);
		});
	}
	
	function removeComment(uID,date){
		var url = '<?php   echo BASE_URL.Loader::helper('concrete/urls')->getToolsURL('proforms/attributes/comments/remove_comment.php','proforms')?>';
		$.ajax({
			url: url,
			data: {
				uID: uID,
				date: date
			}
		}).done( function(html){
			fetchComments();
		});
	}
	
	$(document).ready(function(){
    	$('.dialog-launch').dialog();
	});
/*]]>*/
</script>