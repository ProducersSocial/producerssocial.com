<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class CommentsAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $emails;
	public $include_participants;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->set('akID',$this->attributeKey->getAttributeKeyHandle());
		$this->set('avID',$this->getAttributeValueID());
		$this->set('posts',$this->getValue());
	}

	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		return $list;
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atCommentsSettings where akID = ?',$this->attributeKey->getAttributeKeyID());
		}

		$this->akID = $row['akID'];
		$this->emails = $row['emails'];
		$this->include_participants = $row['include_participants'];
		
		if($_REQUEST['avID']){
			$av = AttributeValue::getByID($_REQUEST['avID']);
			$avID = $av->getAttributeValueID();
		}elseif($_REQUEST['ProformsItemID'] && !$this->getAttributeValueID()){
			$pfo = ProformsItem::getByID($_REQUEST['ProformsItemID']);
			$av = $pfo->getAttributeValueObject($this->attributeKey,true);
			$avID = $av->getAttributeValueID();
		}else{
			$avID = $this->getAttributeValueID();
		}
		
		$this->set('akID', $this->akID);
		$this->set('avID',$avID);
		$this->set('emails', $this->emails);
		$this->set('include_participants', $this->include_participants);
	}
	
	public function getValue(){
		$this->load();
		$db = Loader::db();
		$val = $db->execute("SELECT * FROM atCommentsPost WHERE avID=?",array($this->getAttributeValueID()));
		while($row = $val->fetchrow()){
			$vals[] = $row;
		}
		return $vals;
	}
	
	public function display(){
		$this->set('akID',$this->attributeKey->getAttributeKeyHandle());
		$this->set('avID',$this->getAttributeValueID());
		$this->set('posts',$this->getValue());
		if(!$this->getAttributeValueID()){
			Loader::model('proforms_item','proforms');
			$pf = new ProformsItem($_REQUEST['ProformsItemID']);
			$av = $pf->getAttributeValueObject($this->attributeKey,true);
			$this->set('avID',$av->getAttributeValueID());
		}
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Comments Question Type is primarily for use within internal forms.  It is specifically designed to allow commenting bewtween users regarding any given form during an internal review process.</p> 
		<br /> 
		<h4>Advanced Use</h4>
		<p>Comments Question Type becomes very powerful when combined with custom applications built on top of ProForms specific to an organizations application review process.</p>
		');
	}
	
	public function saveComment($uID,$comment,$avID,$ProformsItemID){
		$this->load();
		$date = date('Y-m-d H:i:s');
		$db = Loader::db();
		//$db->Execute('delete from atCommentsPost');
		//$db->Execute('delete from atCommentsPost where avID = ?', array($avID));
		$db->Execute('insert into atCommentsPost (avID,uID,comment,date) values (?,?,?,?)', array($avID,$uID,$comment,$date));
		
		Loader::model('proforms_item','proforms');
		$ProformsItemID = $db->getOne("SELECT ProformsItemID FROM ProformsItemAttributeValues WHERE avID = ?",array($avID));
		$pfi = ProformsItem::getByID($ProformsItemID);
		$entry_name = $pfi->getAttribute('business_name');
		
		$mh = Loader::helper('mail');
		$mh->from('comments@'.substr(BASE_URL,7),substr(BASE_URL,7).' Form Comments');
		if($this->emails){
			$ui = UserInfo::getByID($uID);
			$uName = $ui->getUserName();
			
			$mail_list = explode("\n",$this->emails);
			$user_list = array();
			
			if($this->include_participants){
				$q = $db->execute("SELECT uID FROM atCommentsPost WHERE avID = ?",array($avID));
				while($row = $q->fetchrow()){
					$u = UserInfo::getByID($row['uID']);
					$user_list[] = $u->getUserEmail();
				}
				$mail_list = array_merge($mail_list,$user_list);
			}
			
			$mail_list = array_unique($mail_list);
			
			foreach($mail_list as $email){
				$mh->to($email);
			}
			
			$body = '
			<b>ATTENTION</b>
			<p>'.$uName.' has posted a comment to a form thread you are following.</p>
			<p>Please review Form Entry #'.$ProformsItemID.' - '.$entry_name.'</p>
			<hr/>
			<b>'.$date.'</b><br /><br />
			'.$comment.'
			<hr/>
			<p>Thank you.</p>
			<p>'.substr(BASE_URL,7).' Web Team.</p>
			';
			
			$mh->setSubject("There has been a new comment added!");
			$mh->setBodyHTML($body);
			$mh->sendMail();
		}
	}
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atComments where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atComments (avID) values (?)', array($this->getAttributeValueID()));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atComments where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'emails'=>$data['emails'],
			'include_participants'=>$data['include_participants']
		);
		
		$db=Loader::db();
		$db->Replace('atCommentsSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atCommentsSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}