<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
?>
<fieldset>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('Notify Emails')?></label>
		<div class="input">
			<div id="ccm-editor-pane">
			  <?php     echo $fm->textarea('emails', $emails)?>
			  <?php    echo t('One email per line');?>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<label><?php    echo t("Include Participants?")?></label>
		<div class="input">
			<label class="checkbox inline">
			  <input type="checkbox" name="include_participants" value="1" <?php    if($include_participants>0){echo 'checked';}?>> <?php    echo t('Yes. Include all commenting users in email notice.');?>
			</label>
		</div>
	</div>
</fieldset>