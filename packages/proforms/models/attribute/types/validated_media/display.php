<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
//var_dump($val);
	if(is_a($val[0],'File')){
		//echo 'FILE';
		$vali = '';
		foreach($val as $f){
			$fID = $f->getFileID();
			$name = $f->getFileName();
			$ext = strtolower($f->getExtension());

			$vali .= '<div class="input_option_'.$akID.' file_wrapper">';

				$allowed_types_video = array('mov','mp4','m4v');
				if(in_array($ext,$allowed_types_video)){
				
					$vali .= '<a href="'.Loader::helper('concrete/urls')->getToolsURL('proforms/show_preview_image.php','proforms').'?vID='.$fID.'&ext='.$ext.'" id="image_'.$key.'_'.$akID.'" alt="file_path" class="dialog-launch" dialog-width="620" dialog-height="340" dialog-modal="true" dialog-title="View Image" dialog-on-close="" onClick="javascript:;">'.$name.'</a>';

				}else{
					$width = $f->getAttributeList()->getAttribute('width');
					if($width > 800){
						$width = 800;
					}
					
					$vali .= '<a href="'.Loader::helper('concrete/urls')->getToolsURL('proforms/show_preview_image.php','proforms').'?fID='.$fID.'" id="image_'.$key.'_'.$akID.'" alt="file_path" class="dialog-launch" dialog-width="'.$width.'" dialog-height="440" dialog-modal="true" dialog-title="View Image" dialog-on-close="" onClick="javascript:;">'.$name.'</a>';
				
				}
			$vali .= '</div>';
		}
		
		print $vali;
		return true;
		
	}