<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class ValidatedMediaAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $type;
	public $qty;
	public $size_limit;
	public $allowed_types_image;
	public $allowed_types_video;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $is_multipart = 1;
	
	public function form(){
		$this->load();
		$this->set('fIDs',$this->getValue());
	}
	
	public function display(){
		$this->load();
		$this->set('val',$this->getValue());
	}
	
	public function closer(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atValidatedMediaSettings where akID = ?', $ak->getAttributeKeyID());
		}
	
		
		$this->akID = $row['akID'];
		$this->type = $row['type'];
		$this->qty = $row['qty'];
		$this->size_limit = $row['size_limit'];
		$this->allowed_types_image = ($row['allowed_types_image']) ? explode(',',$row['allowed_types_image']) : array() ;
		$this->allowed_types_video = ($row['allowed_types_video']) ? explode(',',$row['allowed_types_video']) : array() ;

		$this->set('akID', $this->akID);
		$this->set('type', $this->type);
		$this->set('qty', $this->qty);
		$this->set('size_limit', $this->size_limit);
		$this->set('allowed_types_image', $this->allowed_types_image);
		$this->set('allowed_types_video', $this->allowed_types_video);
	}
	
	public function getValue(){
		$vals = array();
		$db = Loader::db();
		$q = $db->execute("SELECT * FROM atValidatedMediaItems WHERE avID = ?",array($this->getAttributeValueID()));
		while($row = $q->fetchRow()){
			$vals[] = File::getByID($row['fID']);
		}
		return $vals;
	}
	
	
	public function saveForm($data) {
		$this->load();
		$fIDs = $data['existing_values'];
		if($_FILES['akID']['name'][$this->attributeKey->getAttributeKeyID()]){
			foreach($_FILES['akID']['name'][$this->attributeKey->getAttributeKeyID()] as $key=>$sFile){
				$filename = $sFile;
				$path = $_FILES['akID']['tmp_name'][$this->attributeKey->getAttributeKeyID()][$key];
				// creating a new file in the system
				if($path != ''){
					Loader::library("file/importer");
					$fi = new FileImporter();
					$newFile = $fi->import($path, $filename);
		
					$fIDs[] = $newFile->getFileID();
				}
			}
		}elseif(is_array($data)){
			if(is_a($data[0],'File')){
				foreach($data as $f){
					$fIDs[] = $f->getFileID();;
				}
			}
		}
		
		if(!$fIDs){
			$fIDs = array();
		}

		$db = Loader::db();
		$db->Execute('delete from atValidatedMedia where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atValidatedMedia (avID) values (?)', array($this->getAttributeValueID()));
		
		$db->Execute('delete from atValidatedMediaItems where avID = ?', array($this->getAttributeValueID()));
		
		foreach($fIDs as $fID){
			$db->Execute('insert into atValidatedMediaItems (avID,fID) values (?,?)', array($this->getAttributeValueID(),$fID));
		}

	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atValidatedMedia where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$data['akID'],
			'type'=>$data['type'],
			'qty'=>$data['qty'],
			'size_limit'=>$data['size_limit'],
			'allowed_types_image'=> ($data['image_types']) ? implode(',',$data['image_types']) : null,
			'allowed_types_video'=> ($data['video_types']) ? implode(',',$data['video_types']) : null
		);
		
		$db=Loader::db();
		$db->Replace('atValidatedMediaSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atValidatedMediaSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}