<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
?>
<div class="clearfix">
	<label><?php    echo t("Media Type")?></label>
	<div class="input">
		<select name="type">
			<option value="video" <?php    if($type=='video'){echo 'selected';}?>><?php   echo t('Video')?></option>
			<option value="image" <?php    if($type=='image'){echo 'selected';}?>><?php   echo t('Image')?></option>
			<option value="video_image" <?php    if($type=='video_image'){echo 'selected';}?>><?php   echo t('Video & Image')?></option>
		</select>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Max Number of Files")?></label>
	<div class="input">
		<?php   echo $fm->text('qty',$qty, array('class'=>'input-mini'))?>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Max File Size")?></label>
	<div class="input">
		<?php   echo $fm->text('size_limit',$size_limit, array('class'=>'input-mini'))?> <i><?php   echo t('(in killobytes - 3mb = 3000kb)')?></i>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Allowed Video Types")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="video_types[]" value="mp4" <?php    if(in_array('mp4',$allowed_types_video)){echo 'checked';}?>> mp4
		</label>
		<label class="checkbox inline">
		  <input type="checkbox" name="video_types[]" value="mpg" <?php    if(in_array('mpg',$allowed_types_video)){echo 'checked';}?>> mpg
		</label>
		<label class="checkbox inline">
		  <input type="checkbox" name="video_types[]" value="m4v" <?php    if(in_array('m4v',$allowed_types_video)){echo 'checked';}?>> m4v
		</label>
		<label class="checkbox inline">
		  <input type="checkbox" name="video_types[]" value="mov" <?php    if(in_array('mov',$allowed_types_video)){echo 'checked';}?>> mov
		</label>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Allowed Image Types")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="image_types[]" value="jpg" <?php    if(in_array('jpg',$allowed_types_image)){echo 'checked';}?>> jpg
		</label>
		<label class="checkbox inline">
		  <input type="checkbox" name="image_types[]" value="jpeg" <?php    if(in_array('jpeg',$allowed_types_image)){echo 'checked';}?>> jpeg
		</label>
		<label class="checkbox inline">
		  <input type="checkbox" name="image_types[]" value="png" <?php    if(in_array('png',$allowed_types_image)){echo 'checked';}?>> png
		</label>
	</div>
</div>