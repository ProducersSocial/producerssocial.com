<?php     
defined('C5_EXECUTE') or die("Access Denied.");

class PaymentAuthorizeSimAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
	}
	
	public function display_total(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		//API Login ID
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_sim_api_login_id" class="control-label">'.t('Authorize API Login ID').'</label>';
		print '		<div class="input">';
		print $fm->text('authorize_sim_api_login_id',$this->authorize_sim_api_login_id);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		//Transaction Key
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_sim_transaction_key" class="control-label">'.t('Authorize Transaction Key').'</label>';
		print '		<div class="input">';
		print $fm->text('authorize_sim_transaction_key',$this->authorize_sim_transaction_key);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		//Sandbox
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_sim_sandbox" class="control-label">'.t('Sandbox Mode?').'</label>';
		print '		<div class="input">';
		print $fm->checkbox('authorize_sim_sandbox',1,$this->authorize_sim_sandbox);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_sim_price_handle" class="control-label">'.t('Total Handle').'</label>';
		print '		<div class="input">';
		print $fm->text('authorize_sim_price_handle',$this->authorize_sim_price_handle,array('placeholder'=>'total_question_handle'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_sim_return_page" class="control-label">'.t('Return Page').'</label>';
		print '		<div class="input">';
		print Loader::helper('form/page_selector')->selectPage('authorize_sim_return_page',$this->authorize_sim_return_page);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPaymentAuthorizeSimSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->authorize_sim_api_login_id = $row['authorize_sim_api_login_id'];
		$this->authorize_sim_transaction_key = $row['authorize_sim_transaction_key'];
		//$this->authorize_sim_secret_question = $row['authorize_sim_secret_question'];
		$this->authorize_sim_price_handle = $row['authorize_sim_price_handle'];
		$this->authorize_sim_return_page = $row['authorize_sim_return_page'];
		$this->authorize_sim_sandbox = $row['authorize_sim_sandbox'];

		$this->set('akID', $this->akID);
		$this->set('authorize_sim_api_login_id', $this->authorize_sim_api_login_id);
		$this->set('authorize_sim_transaction_key', $this->authorize_sim_transaction_key);
		//$this->set('authorize_sim_secret_question', $this->authorize_sim_secret_question);
		$this->set('authorize_sim_price_handle', $this->authorize_sim_price_handle);
		$this->set('authorize_sim_return_page', $this->authorize_sim_return_page);
		$this->set('authorize_sim_sandbox', $this->authorize_sim_sandbox);
	}
	
	public function getValue(){
		return $this->hidden_value;
	}

	
	public function saveForm($value) {
		$this->load();
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'authorize_sim_api_login_id'=>$data['authorize_sim_api_login_id'],
			'authorize_sim_transaction_key'=>$data['authorize_sim_transaction_key'],
			//'authorize_sim_secret_question'=>$data['authorize_sim_secret_question'],			
			'authorize_sim_price_handle'=>$data['authorize_sim_price_handle'],
			'authorize_sim_return_page'=>$data['authorize_sim_return_page'],
			'authorize_sim_sandbox'=>$data['authorize_sim_sandbox']
		);
		
		$pkg = Package::getByHandle('proforms');
		$pkg->saveConfig('AUTHORIZE_TESTMODE', $data['authorize_sim_sandbox']);
		
		$db=Loader::db();
		$db->Replace('atPaymentAuthorizeSimSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPaymentAuthorizeSimSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}