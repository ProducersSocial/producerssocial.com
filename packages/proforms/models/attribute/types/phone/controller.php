<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/types/default/controller');

class PhoneAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public $restrictDuplicates = true;

	public function form() {
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		print Loader::helper('form')->text($this->field('value'), $value);
		print ' <i>'.t('( format must be in xxx-xxx-xxxx )').'</i>';
	}
	
	public function composer() {
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		print Loader::helper('form')->text($this->field('value'), $value, array('class' => 'span5'));
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}

	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	public function validateForm($phone) {
		if(is_array($phone)){
			$phone = $phone['value'];
		}
		if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $phone)) {
			 // $phone is valid
			 $this->error = t('Your phone number is not in the correct format!');
			 return true;
		}
		return false;
	}

}