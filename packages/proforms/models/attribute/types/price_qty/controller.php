<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/types/default/controller');

class PriceQtyAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';

	public function form() {
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		if(!$value){
			$value = 1;
		}
		print Loader::helper('form')->number($this->field('value'), $value,array('class'=>'input-small','placeholder'=>'1'));
	}
	
	public function searchForm($list){
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}


}