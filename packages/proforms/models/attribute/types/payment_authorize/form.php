<?php     
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('proforms_item','proforms');
$fm = Loader::helper('form');
$bt = BlockType::getByHandle('proforms_display');

$aim = Loader::helper('concrete/urls')->getToolsURL('proforms/payment/authorize/aim.php','proforms');

if($authorize_sandbox == 1){
	$set_sandbox = 'true';
}else{
	$set_sandbox = 'false';
}

if($_REQUEST['payment_form']){
	$ProformsItemID =  $_REQUEST['payment_form'];
}

if($_REQUEST['entryID']){
	$ProformsItemID = str_replace(' ','+',$_REQUEST['entryID']);
	$eh = Loader::helper('encryption');
	$ProformsItemID = $eh->decrypt( $ProformsItemID );
}

if($ProformsItemID){
   echo $fm->hidden('ProformsItemID', $ProformsItemID);     
}

echo '<div class="payment_form">';
echo $fm->hidden('set_sandbox', $set_sandbox);
echo $fm->hidden('authorize_price_handle',$authorize_price_handle,array('readonly'=>'readonly'));
//echo $fm->hidden($field_value,'');
//Billing Info
/*
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Fist Name</label></strong>
	<div class="input">'.$fm->text('x_first_name','',array('placeholder'=>'x_first_name', 'size'=>'15')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Last Name</label></strong>
	<div class="input">'.$fm->text('x_last_name','',array('placeholder'=>'x_last_name', 'size'=>'14')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Address</label></strong>
	<div class="input">'.$fm->text('x_address','',array('placeholder'=>'x_address', 'size'=>'26')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">City</label></strong>
	<div class="input">'.$fm->text('x_city','',array('placeholder'=>'x_city', 'size'=>'15')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">State</label></strong>
	<div class="input">'.$fm->text('x_state','',array('placeholder'=>'x_state', 'size'=>'4')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Zip</label></strong>
	<div class="input">'.$fm->text('x_zip','',array('placeholder'=>'x_zip', 'size'=>'9')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Country</label></strong>
	<div class="input">'.$fm->text('x_country','',array('placeholder'=>'x_country', 'size'=>'22')).'</div>
	</div>';
*/
//Credit Card Info
echo '<div id="authorize_errors"></div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Credit Card Number</label></strong>
	<div class="input">'.$fm->text('x_card_num','',array('placeholder'=>'Credit Card Number', 'autocomplete' => 'off', 'size'=>'15')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_code">CCV</label></strong>
	<div >'.$fm->text('x_card_code','',array('placeholder'=>'CCV', 'autocomplete' => 'off', 'size'=>'4', 'style'=>'width:75px;')).'</div>
	</div>';
	$months = array(''=>'MM','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12');
	$currentYear = date('y');
	$lastYear = $currentYear + 20;
	$_years = range($currentYear, $lastYear, 1);
	$years = array(''=>'YY');
	foreach($_years as $k => $v){
		$years[$v] = $v;
	}
echo '<div class="clearfix text">
	<strong><label for="x_card_date">Exp. Date</label></strong>
	<div  class="select">'.$fm->select('x_exp_month',$months).$fm->select('x_exp_year',$years).'</div>
	</div>';

//Get's Done in Tool
echo $fm->hidden('authorize_transaction_id',$authorize_transaction_id);

echo '</div>';
?>
<script type="text/javascript">
$(document).ready(function(){
	$('form.proform_slider').submit(function(e) { 
     e.preventDefault();
     e.returnValue = false;
     form = $(this);
     // some validation code here: if valid, add podkres1 class
     //if ($('input.podkres1').length > 0) { 
        // do nothing
     //} else {
        var $form = $(this);
        var postStr = $("form.proform_slider").serialize();
         $.ajax({ 
			type: 'post',
			url: '<?php  echo $aim; ?>', 
			data: postStr,
			dataType: 'json',
			success: function(data){
	            if(data.approved === true){
	            	$( "input[name=authorize_transaction_id]" ).val(data.transaction_id);//Set Transaction ID
	            	// submit the form
	                form.off('submit');
	               	form.submit();
	            }else{
	            	$('#authorize_errors').html('<span class="alert alert-danger">'+data.error+'</span>');
	            }
			},
             error: function(){
             	console.log('Something went wrong with the ajax');
             	$('#authorize_errors').html('<span class="alert alert-danger">Something went wrong, please contact the web developer.</span>');
             }
         });
    // }
	});
});
</script>