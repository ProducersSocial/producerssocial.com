<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class AutoResponderAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $response_message;
	public $from_email;
	public $response_subject;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]','autorespond');
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		return $list;
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atatAutoResponderSettings where akID = ?', $ak->getAttributeKeyID());
		}
		
		$this->akID = $row['akID'];
		$this->response_message = $row['response_message'];
		$this->response_subject = $row['response_subject'];
		$this->from_mail = $row['from_email'];
		$this->notify = $row['notify'];
		$this->notifyEmailsOnSubmission = $row['notifyEmailsOnSubmission'];
		
		$this->set('akID', $this->akID);
		$this->set('response_message', $this->response_message);
		$this->set('response_subject', $this->response_subject);
		$this->set('from_email', $this->from_mail);
		$this->set('notify', $this->notify);
		$this->set('notifyEmailsOnSubmission', $this->notifyEmailsOnSubmission);
	}
	
	public function getAgreementText(){
		$this->load();
		return $this->agreement_text;
	}
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atAutoResponder WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('<h4>What is it?</h4><p>The below message content will be sent to <u>any</u> submitted "email" attribute type\'s value contained within it\'s respective form.</p> <br /> <h4>Advanced Use</h4><p>Any Question Type ordered BEFORE this Question may be referenced within this email by using that question\'s respective Question Handle placed between double square brackets:  [[question_handle]].</p><br/><p>This Question Type uses the "$reviewStatus=0;" and will never be printed out for Form review.  It is merely a front-end Question Type for functional use.</p><p>Bracket Handles can be used to dynamically populate the To Email, the Subject, and within the Body of the message.</p>');
	}
	
	
	public function saveForm($value) {
			
		$this->load();
		
		if(!$this->getValue()){
			
			/*
			/ This question type is set to $reviewStatus=0;, meaning it will not
			/ show up in the dashboard review tabs, and thus, never send except 
			/ on original submission.
			*/
			$this->load();
			$mh = Loader::helper('mail');
			if($this->from_mail){
				$mh->from($this->from_mail);
			}else{
				$mh->from('proforms@'.substr(BASE_URL,7));
			}
			
			//$mh->replyto($preset_mail,$preset_name);
			
			$set = AttributeSet::getByID($_REQUEST['question_set']);
			$keys = $set->getAttributeKeys();
			$emails = '';
			
			$db = Loader::db();
			$pfiID = $db->getOne("SELECT ProformsItemID FROM ProformsItemAttributeValues WHERE akID = ? AND avID =?",array($this->attributeKey->getAttributeKeyID(),$this->getAttributeValueID()));
			$pfi = ProformsItem::getByID($pfiID);
			
			foreach($keys as $ak){
				if($ak->getAttributeType()->getAttributeTypeHandle() == 'email'){
					if($i){$emails .= ',';}
					
					$emails .= $_REQUEST['akID'][$ak->getAttributeKeyID()]['value'];
					$i++;
				}
				if($ak->getAttributeType()->getAttributeTypeHandle() == 'associate_user'){
					if($i){$emails .= ',';}
					$uID = $pfi->getAttribute($ak);
					Loader::model('userinfo');
					$ui = UserInfo::getByID($uID);
					$emails .= $ui->getUserEmail();
					$i++;
				}
			}
			
			/*
			* parse email dynamic vals
			*/
			if($this->notifyEmailsOnSubmission){
				$emails = '';
				if($i){$emails .= ',';}
				if(strpos($this->notify, '@') > 1){
					$emails .= $this->notify;
				}else{
					preg_match_all('/\[\[(.*?)\]\]/', $this->notify, $matches);
					if(is_array($matches[1])){
						foreach($matches[1] as $handle){
							$val = $pfi->getAttribute($handle);
							$at = ProformsItemAttributeKey::getByHandle($handle);
							$thandle = $at->getAttributeType()->getAttributeTypeHandle();
							if($thandle == 'select_values'){
								$value = $val->getSelectAttributeOptionValueOption();
							}else{
								$value = $val;
							}
							$emails .= $value;
						}
					}
				}
			}
			
			
			/*
			* parse body dynamic vals
			*/
			if($emails != ''){
				$body = str_replace('="/','="'.BASE_URL.'/',$this->response_message);
				preg_match_all('/\[\[(.*?)\]\]/', $body, $matches);
				if(is_array($matches[1])){
					foreach($matches[1] as $handle){
						$ak = ProformsItemAttributeKey::getByHandle($handle);
						if(is_object($ak)){
							$thandle = $ak->getAttributeType()->getAttributeTypeHandle();
						}
						$match = '[['.$handle.']]';
						
						$vo = $pfi->getAttributeValueObject($ak);
						
						if($handle == 'ProformsItemID'){
							$newval = $_REQUEST['ProformsItemID'];
							$body = str_replace($match,$newval,$body); 
						}elseif($thandle=='price_event'){
							if(is_object($vo)){
								$newval = $vo->getDisplayValue('getDisplayReviewValue');
								$body = str_replace($match,$newval,$body);
							}
						}else{
							if(is_object($vo)){
								$cnt = $ak->getController();
						
								if(method_exists($cnt,'getDisplayValue')){
									$val = $vo->getDisplayValue('getDisplayValue');
								}elseif(method_exists($cnt,'getDisplayReviewValue')){
									$val = $vo->getDisplayValue('getDisplayReviewValue');
								}else{
									$val = $vo->getDisplayValue();
								}
								
								if(is_array($val)){
									foreach($val as $v){
										$newval = $v.'<br/>';
										$body = str_replace($match,$newval,$body);
									}
								}else{
									if(is_a($val,'File')){
										Loader::model('file');
										$name = BASE_URL.File::getRelativePathFromID($val->fID);
										$body = str_replace($match,$name,$body);
									}else{
										$body = str_replace($match,$val,$body);
									}
								}
							}
						}
					}
				}
				
				
				/*
				* parse subject dynamic vals
				*/
				$subject = $this->response_subject;
				preg_match_all('/\[\[(.*?)\]\]/', $subject, $matches);
				if(is_array($matches[1])){
					foreach($matches[1] as $handle){
						$ak = ProformsItemAttributeKey::getByHandle($handle);
						if(is_object($ak)){
							$thandle = $ak->getAttributeType()->getAttributeTypeHandle();
						}
						$match = '[['.$handle.']]';
						
						if($handle == 'ProformsItemID'){
							$newval = $_REQUEST['ProformsItemID'];
							$subject = str_replace($match,$newval,$subject); 
						}elseif($thandle=='price_event'){
							$vo = $pfi->getAttributeValueObject($ak);
							if(is_object($vo)){
								$newval = $vo->getDisplayValue('getDisplayReviewValue');
								$subject = str_replace($match,$newval,$subject);
							}
						}else{
							$val = $pfi->getAttribute($handle);
							if(is_array($val)){
								foreach($val as $v){
									$newval = $v.'<br/>';
									$subject = str_replace($match,$newval,$subject);
								}
							}else{
								if(is_a($val,'File')){
									Loader::model('file');
									$name = BASE_URL.File::getRelativePathFromID($val->fID);
									$subject = str_replace($match,$name,$subject);
								}else{
									$subject = str_replace($match,$val,$subject);
								}
							}
						}
					}
				}
				
				$emails = trim($emails, ','); 
				
				//Log::addEntry($body);
				$mh->to($emails);
				$mh->setBodyHTML($body);
				$mh->setBody(strip_tags($body));
				$mh->setSubject($subject);
				$mh->sendMail();
			}
			$db = Loader::db();
			$db->Execute('delete from atAutoResponder where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atAutoResponder (avID,dummy) values (?,?)', array($this->getAttributeValueID(),1));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atAutoResponder where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'response_message'=>$data['response_message'],
			'response_subject'=>$data['response_subject'],
			'from_email'=>$data['from_email'],
			'notifyEmailsOnSubmission'=>($data['notifyEmailsOnSubmission'])?1:0,
			'notify'=>$data['notify']
		);
		
		$db=Loader::db();
		$db->Replace('atatAutoResponderSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atatAutoResponderSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}