<?php     defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php   
$fm = Loader::helper('form');
Loader::model('attribute/categories/user');
$attribs = UserAttributeKey::getList();
?>
<div class="clearfix">
	<label><?php    echo t("User Attribute")?></label>
	<div class="input">
		<?php 
		$atts_aray = array(
			'uName' => 'User Name',
			'uEmail' => 'User Email'
		);
		
		foreach($attribs as $ak){
			$atts_aray[$ak->getAttributeKeyHandle()] = $ak->getAttributeKeyName();
		}
		?>
		<?php    print $fm->select('akHandle',$atts_aray,$akHandle);?>
	</div>
</div>