<?php     
defined('C5_EXECUTE') or die("Access Denied.");

class MultiplexAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $allow_multiple;
	public $questions;
	public $restrictDuplicates = true;
	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$at = $this->attributeType;

		$path = $at->getAttributeTypeFileURL('js/jQuery.geoselector.js');
		$multiplex_js = $at->getAttributeTypeFileURL('multiplex.js');
		$json_path = $at->getAttributeTypeFileURL('js/divisions.json');
		$json_path = BASE_URL.DIR_REL.'/packages/proforms/models/attribute/types/multiplex/js/divisions.json';
		$this->set('json_path',$json_path);
		$this->addFooterItem(Loader::helper('html')->javascript($multiplex_js));
		$this->addFooterItem(Loader::helper('html')->javascript($path));
		
		$this->set('answers',$this->getValue());
		$this->set('answerCount',$this->getAnswerCount());
	}

	public function getDisplayValue(){
		$this->load();
		$answers = $this->getValue();
		$questions = $this->questions;
		$valstring = '';
		foreach($answers as $answer){
			$mpq = 0;
			//var_dump($answer);
			if(is_array($questions)){
				foreach($questions as $key=>$question){
					$mpq++;
					//var_dump($question);
					$ci = 0;
					if($question['mpqtID']==6){
						$street = $answer[$key]['mpqValue'];
						$city = $answer[($key+1)]['mpqValue'];
						$country = $answer[($key+2)]['mpqValue'];
						$states = $answer[($key+3)]['mpqValue'];
						$postal = $answer[($key+4)]['mpqValue'];
						$valstring .= $street.' '.$city.' '.$states.', '.$postal.' - '.$country;
					}elseif($question['mpqtID']==4){
						if(is_array($answer)){
					  	    $valKey = $answer[$key]['mpqID'];
						  	foreach($answer as $an){
						  		if($valKey == $an['mpqID']){
							  		if($ci){$valstring .= ', ';}
							  		$valstring .= $an['mpqValue'];
							  		$ci++;
							  	}
						  	}
						  }
					}elseif($question['mpqtID']==3){
					    $valstring .= $this->getQuestionSelectValue($answer[$key]['mpqValue']);
					}else{
						$valstring .= $answer[$key]['mpqValue'];
					}
					$valstring .= ' | ';
				}
			}
			$valstring .= "<br/>";
			$i++;
		}

		return $valstring;
	}
	
	public function display(){
		$this->load();
		$this->set('answers',$this->getValue());
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atMultiPlexSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$qow = $db->query("SELECT * FROM atMultiPlexQuestions WHERE akID = ? ORDER BY mpqID ASC", $this->attributeKey->getAttributeKeyID());
		while($qq = $qow->fetchrow()){
			$questions[] = $qq;
		}
		
		$this->akID = $row['akID'];
		$this->allow_multiple = $row['allow_multiple'];
		$this->questions = $questions;

		$this->set('akID', $this->akID);
		$this->set('allow_multiple', $this->allow_multiple);
		$this->set('questions', $this->questions);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->Query("SELECT * FROM atMultiPlexAnswers WHERE avID = ? ORDER BY mpvID",array($this->getAttributeValueID()));
		while($row = $val->fetchrow()){
			$answers[$row['mpsID']][] = $row;
		}
		return $answers;
	}
	
	public function getQuestionSelectValue($valueID){
	    $db = Loader::db();
    	$options = $db->getOne("SELECT mpqOptions FROM atMultiPlexQuestions WHERE akID = ?",array($this->attributeKey->getAttributeKeyID()));
    	$opts = explode("\r\n",$options);
    	foreach($opts as $key=>$opt){
        	if($key == $valueID){
            	return $opt;
        	}
    	}
	}
	
	public function getAnswerCount(){
		$db = Loader::db();
		$val = $db->getOne("SELECT MAX(mpsID) FROM atMultiPlexAnswers WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Multiplex Question Type could be thought of as a "form" within your form!</p><br /><p>For more complex data collection, it is sometimes necessary to have duplicatable form inputs.  Multiplex allows you to meet this need as well as more simple custom "groupings" of questions that really need to be contained as one data set.</p>
		<br /> 
		<h4>Advanced Use</h4>
		<p>This Question Type uses the "$restrictDuplicates = true;" and has the option to validate duplicate entries.</p>
		');
	}
	

	public function validateForm($value) {
		  $this->load();

		  $pattern = $this->pattern;
		  if(!preg_match($pattern,$value)) {
			  // $phone is valid
			  $this->error = t('The input provided is not in the correct format for \''.$this->attributeKey->getAttributeKeyName().'\'!');
			  return false;
		  }
		  return true;

	}
	
	public function saveForm($value) {
		//var_dump($_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()]);exit;
		$this->load();
		$db = Loader::db();
		
		//var_dump($_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()][2]);exit;
		
		$db->Execute('delete from atMultiPlex where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('delete from atMultiPlexAnswers where avID = ?', array($this->getAttributeValueID()));
		
		$db->Execute('insert into atMultiPlex (avID,dummy) values (?,?)', array($this->getAttributeValueID(),null));
		
		if($_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()]){
			foreach($_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()] as $qs){
				$mpsID++;
				foreach($qs as $mpqID=>$val){
					if(is_array($val)){
						//var_dump($qs);exit;
						foreach($val as $val_sting){
							$db->Execute('insert into atMultiPlexAnswers (avID,mpsID,mpqID,mpqValue) values (?,?,?,?)', array($this->getAttributeValueID(),$mpsID,$mpqID,$val_sting));
						}
					}else{
						$db->Execute('insert into atMultiPlexAnswers (avID,mpsID,mpqID,mpqValue) values (?,?,?,?)', array($this->getAttributeValueID(),$mpsID,$mpqID,$val));
					}
				}
			}
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atMultiPlex where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'allow_multiple'=>$data['allow_multiple']
		);
		
		
		$db=Loader::db();
		$db->Replace('atMultiPlexSettings', $vals, 'akID', true);
		
		$db->Execute('delete from atMultiPlexQuestions where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		
		if(is_array($data['input_types'])){
			foreach($data['input_types'] as $id=>$vals){
				$values = array(
					'akID'=>$this->attributeKey->getAttributeKeyID(),
					'mpqtID'=>$data['input_type_id'][$id],
					'mpqTitle'=>$data['input_types'][$id],
					'mpqOptions'=>$data['input_options'][$id]
				);
				$db->Execute("INSERT INTO atMultiPlexQuestions (akID,mpqtID,mpqTitle,mpqOptions) VALUES (?,?,?,?)",$values);
			}
		}
			
	}
	
	public function duplicateKey($newAK) {
		$this->load();
		$db = Loader::db();
		$db->Execute('insert into atMultiPlexSettings (akID, allow_multiple) values (?, ?)', array($newAK->getAttributeKeyID(), $this->allow_multiple));	
		$r = $db->Execute('select * from atMultiPlexQuestions where akID = ?', $this->getAttributeKey()->getAttributeKeyID());
		while ($row = $r->FetchRow()) {
			$db->Execute('insert into atMultiPlexQuestions (akID,mpqtID,mpqTitle,mpqOptions) VALUES (?,?,?,?)', array(
				$newAK->getAttributeKeyID(),
				$row['mpqtID'],
				$row['mpqTitle'],
				$row['mpqOptions']
			));
		}
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atMultiPlexSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}