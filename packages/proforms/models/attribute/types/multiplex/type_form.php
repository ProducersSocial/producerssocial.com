<?php   
defined('C5_EXECUTE') or die("Access Denied.");
$fm = Loader::helper('form');
$qTypes = array(
	'1'=>'Text',
	'2'=>'Textarea',
	'3'=>'Select',
	'4'=>'Checkbox',
	'5'=>'Radio',
	'6'=>'Address'
);
//var_dump($questions);
?>
<div class="clearfix">
	<label><?php    echo t("Allow Multiple?")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="allow_multiple" value="1" <?php    if($allow_multiple>0){echo 'checked';}?>> <?php    echo t('Yes. Input group can be multiplied for multiple entries.');?>
		</label>
	</div>
</div>
<div class="clearfix">
	<label><?php    echo t("Add an input")?></label>
	<div class="input">
		<select name="mpqtID" id="mpqtID">
			<?php   
			foreach($qTypes as $id=>$qt){
				echo '<option value="'.$id.'">'.$qt.'</option>';
			}
			?>
		</select>	
		
		<a href="javascript:;" id="add_inputs" class="btn btn-mini info">&nbsp;+&nbsp;</a>
	</div>
</div>

<div class="clearfix" id="input_set">
	<label><?php    echo t("Inputs")?></label>
	<div class="input" id="input_set_inputs">
	<?php    
	if(is_array($questions)){
		foreach($questions as $question){
			$i++;
			?>
			<div class="clearfix">
				<label class="qtLabel"><?php   echo $qTypes[$question['mpqtID']]?></label>
				<div class="input input_label">
					<?php   
					echo $fm->hidden('input_type_id['.$i.']',$question['mpqtID']);
					if($question['mpqtID'] != 6){
						echo $fm->text('input_types['.$i.']',$question['mpqTitle']);
						echo '<a href="javascript:;" onClick="$(this).parent().parent().remove();">[X]</a>';
						echo t(' Input Label');
					}else{
						echo $fm->hidden('input_types['.$i.']','address');
						echo t('(Address Fields will be shown)');
						echo '<a href="javascript:;" onClick="$(this).parent().parent().remove();">[X]</a>';
					}
					?>
				</div>
				<div class="input input_options">
					<?php   
					if($question['mpqtID'] > 2 && $question['mpqtID'] != 6){
						echo '<br/>';
						echo $fm->textarea('input_options['.$i.']',$question['mpqOptions']);
						echo t(' Input Options (each option on new line)');
					}
					?>
				</div>
			</div>			
			<?php   
		}
	}
	?>
	</div>
	<input type="hidden" name="input_count" id="input_count" value="<?php   echo $i?>"/>
</div>


<div class="input" style="display: none;">
	<div class="clearfix" id="input_template">
		<label class="qtLabel"></label>
		<div class="input input_label">
		</div>
		<div class="input input_options">
		</div>
	</div>
</div>


<script type="text/javascript">
/*<![CDATA[*/
	$(document).ready(function(){
		$('#add_inputs').bind('click tap',function(e){
			e.preventDefault();
			var type = $('#mpqtID option:selected').val();
			var label = $('#mpqtID option:selected').text();
			var id = ($('#input_count').val() * 1) + 1;
			$('#input_count').val(id);
			if(type < 3){
				var temp = $('#input_template').clone();
				temp.attr('id','');
				temp.find('.qtLabel').html(label);
				temp.find('.input_label').append($('<input />').attr({'type':'text', 'name': 'input_types['+id+']'}));
				temp.find('.input_label').append($('<input />').attr({'type':'hidden', 'name': 'input_type_id['+id+']', 'value':type}));
				temp.find('.input_label').append('<a href="javascript:;" onClick="$(this).parent().parent().remove();">[X]</a>');
				temp.find('.input_label').append('<?php   echo t(' Input Label')?>');
				$('#input_set_inputs').append(temp);
			}else if(type==6){
				var temp = $('#input_template').clone();
				temp.attr('id','');
				temp.find('.qtLabel').html(label);
				temp.find('.input_label').append('<?php   echo t('(Address Fields will be shown)')?>');
				temp.find('.input_label').append($('<input />').attr({'type':'hidden', 'name': 'input_types['+id+']', 'value':'address'}));
				temp.find('.input_label').append($('<input />').attr({'type':'hidden', 'name': 'input_type_id['+id+']', 'value':type}));
				temp.find('.input_label').append('<a href="javascript:;" onClick="$(this).parent().parent().remove();">[X]</a>');
				$('#input_set_inputs').append(temp);	
			}else{
				var temp = $('#input_template').clone();
				temp.attr('id','');
				temp.find('.qtLabel').html(label);
				temp.find('.input_label').append($('<input />').attr({'type':'text', 'name': 'input_types['+id+']'}));
				temp.find('.input_label').append($('<input />').attr({'type':'hidden', 'name': 'input_type_id['+id+']', 'value':type}));
				temp.find('.input_label').append('<a href="javascript:;" onClick="$(this).parent().parent().remove();">[X]</a>');
				temp.find('.input_label').append('<?php   echo t(' Input Label')?>');
				temp.find('.input_options').append('<br/>');
				temp.find('.input_options').append($('<textarea />').attr({'name': 'input_options['+id+']'}));
				temp.find('.input_options').append('<?php   echo t(' Input Options (each option on new line)')?>');
				$('#input_set_inputs').append(temp);
			}
			return false;
		});	
	});
/*]]>*/
</script>

