<?php   
defined('C5_EXECUTE') or die("Access Denied.");
$i = 1;
$fm = Loader::helper('form');
if(!is_array($answers)){
	$answers = array(1);
}
?>
<style type="text/css">
.address_form {
	margin-left: 90px;
}
.address_form label select,.address_form label input{
	float: left;
	display: inline!important;
	margin-left: 55px;
	margin-right: 5px;
}
.multiplex_set{
	border: 1px solid gainsboro;
	padding-top: 20px;
}
.add_more{float: right;margin-top: -35px!important;color: white!important;z-index: 300;}
.ccm-ui label.control-label{
	width: 80%!important;
}
.event-attributes{width: 400px;}
</style>
<div id="mpqs_<?php   echo $akID?>" class="multiplex_set">
	<div id="set_<?php   echo $akID?>_<?php   echo $i?>">
		<?php   
		foreach($answers as $answer){
			$mpq = 0;
			if($i > 1){
				echo '<hr/>';
			}
			//var_dump($answer);
			if(is_array($questions)){
				foreach($questions as $key=>$question){
					//var_dump($question);
					$mpq++;
					$ci = 0;
					?>
					<div class="clearfix">
						<label style="margin-right: 5px;"><strong><?php    echo $question['mpqTitle']?></strong></label>
						<div class="input" data-set="<?php   echo $i?>">
						  <?php   
						  if($question['mpqtID']==6){
						  	$street = $answer[$key]['mpqValue'];
							$city = $answer[($key+1)]['mpqValue'];
							$country = $answer[($key+2)]['mpqValue'];
							$states = $answer[($key+3)]['mpqValue'];
							$postal = $answer[($key+4)]['mpqValue'];
							echo $street.' '.$city.' '.$states.', '.$postal.' - '.$country;
						  }elseif($question['mpqtID']==4){
						  	if(is_array($answer)){
						  	    $valKey = $answer[$key]['mpqID'];
							  	foreach($answer as $an){
							  		if($valKey == $an['mpqID']){
								  		if($ci){echo ', ';}
								  		echo $an['mpqValue'];
								  		$ci++;
								  	}
							  	}
							  }
						  }elseif($question['mpqtID']==3){
						    echo $this->controller->getQuestionSelectValue($answer[$key]['mpqValue']);
						  }else{
						  	echo $answer[$key]['mpqValue'];
						  }
						  ?>
						</div>
					</div>
					<?php   
				}
			}
			$i++;
		}
		?>
	</div>
</div>


