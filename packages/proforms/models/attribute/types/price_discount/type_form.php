<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
function getAttributeOptionHTML($v){ 
	if ($v == 'TEMPLATE') {
		$akPriceDiscountID = 'TEMPLATE_CLEAN';
		$akPriceDiscount = 'TEMPLATE';
		$akPriceDiscountOption = 'TEMP_OPTION';
	} else {
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceDiscountID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceDiscountID = $v->getSelectAttributeOptionID();
		}
		$akPriceDiscount = $v->getSelectAttributeOptionValue();
		$akPriceDiscountOption = $v->getSelectAttributeOptionValueOption();
	}
		?>
		<div id="akPriceDiscountDisplay_<?php    echo $akPriceDiscountID?>" >
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountID)?>')" value="<?php    echo t('Edit')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.deleteValue('<?php    echo addslashes($akPriceDiscountID)?>')" value="<?php    echo t('Delete')?>" />
			</div>			
			<span onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountID)?>')" id="akPriceDiscountStatic_<?php    echo $akPriceDiscountID?>" class="leftCol"><?php    echo $akPriceDiscount ?> <i>(<?php    echo $akPriceDiscountOption ?>)</i></span>
		</div>
		<div id="akPriceDiscountEdit_<?php    echo $akPriceDiscountID?>" style="display:none">
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountID)?>')" value="<?php    echo t('Cancel')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.changeValue('<?php    echo addslashes($akPriceDiscountID)?>')" value="<?php    echo t('Save')?>" />
			</div>		
			<span class="leftCol">
				<input name="akPriceDiscountOriginal_<?php    echo $akPriceDiscountID?>" type="hidden" value="<?php    echo $akPriceDiscount?>" />
				<?php     if (is_object($v) && $v->getSelectAttributeOptionTemporaryID() == false) { ?>
					<input id="akPriceDiscountExistingOption_<?php    echo $akPriceDiscountID?>" name="akPriceDiscountExistingOption_<?php    echo $akPriceDiscountID?>" type="hidden" value="<?php    echo $akPriceDiscountID?>" />
				<?php     } else { ?>
					<input id="akPriceDiscountNewOption_<?php    echo $akPriceDiscountID?>" name="akPriceDiscountNewOption_<?php    echo $akPriceDiscountID?>" type="hidden" value="<?php    echo $akPriceDiscountID?>" />
				<?php     } ?>
				<input id="akPriceDiscountField_<?php    echo $akPriceDiscountID?>" name="akPriceDiscount_<?php    echo $akPriceDiscountID?>" type="text" value="<?php    echo $akPriceDiscount?>" size="20" />
				  
				<input id="akPriceDiscountOptionField_<?php    echo $akPriceDiscountID?>" name="akPriceDiscountOption_<?php    echo $akPriceDiscountID?>" type="text" value="<?php    echo $akPriceDiscountOption?>" size="20" />
			</span>		
		</div>	
		<div class="ccm-spacer">&nbsp;</div>
<?php     } ?>

<fieldset>
<legend><?php    echo t('Select Options')?></legend>

<div class="clearfix">
<label><?php    echo t('Values')?></label>
<div class="input">
	<div id="attributeValuesInterface">
	<div id="attributeValuesWrap">
	<?php    
	Loader::helper('text');
	foreach($akPriceDiscount as $v) { 
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceDiscountID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceDiscountID = $v->getSelectAttributeOptionID();
		}
		?>
		<div id="akPriceDiscountWrap_<?php    echo $akPriceDiscountID?>" class="akPriceDiscountWrap <?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?> akPriceDiscountWrapSortable <?php     } ?>">
			<?php    echo getAttributeOptionHTML( $v )?>
		</div>
	<?php     } ?>
	</div>
	
	<div id="akPriceDiscountWrapTemplate" class="akPriceDiscountWrap" style="display:none">
		<?php    echo getAttributeOptionHTML('TEMPLATE') ?>
	</div>
	
	<div id="addAttributeValueWrap"> 
		<input id="akPriceDiscountFieldNew" name="akPriceDiscountNew" type="text" value="<?php    echo $defaultNewOptionNm ?>" size="40" class="faint" 
		onfocus="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',0)" 
		onblur="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',1)"
		onkeypress="ccmAttributesHelper.addEnterClick(event,function(){ccmAttributesHelper.saveNewOption()})"
		placeholder="<?php  echo t('Amount (no currency symbol)')?>" /> 
		 <input id="akPriceDiscountOptionFieldNew" name="akPriceDiscountOptionNew" type="text" value="" placeholder="<?php  echo t('Discount Code')?>"/>
		<input class="btn" type="button" onClick="ccmAttributesHelper.saveNewOption(); $('#ccm-attribute-key-form').unbind()" value="<?php    echo t('Add') ?>" />
	</div>
	</div>

</div>
</div>


</fieldset>
<?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?>
<script type="text/javascript">
//<![CDATA[
$(function() {
	ccmAttributesHelper.makeSortable();
});
//]]>
</script>
<?php     } ?>