<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = loader::helper('form');
?>
<fieldset>
	<div class="clearfix">
		<div class="input">
			<div class="well">
			<?php  
			switch($shipping_type){
				case 'user_address':
					$u = new User();
					if($u->isLoggedIn()){
						$ui = UserInfo::getByID($u->uID);
						$user_address = $ui->getAttribute($pull_location_from);
						if(is_array($zones)){
							foreach($zones as $zone){
								if($user_address->state_province == $zone['title']){
									$total = money_format('%(#4n',$zone['value'] * 1);
									print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$total);
									print '<strong>'.$zone['title'].t(' Shipping: ').' + <span class="currency_symbol"></span></strong> '.$total;
								}
							}
						}
					}
					break;
				case 'address':
					/*
					/ not yet developed
					/ will allow the shiping price to pull by state_province of address question type
					*/
					break;
					
				case 'address':
					/*
					/ not yet developed
					/ will allow the shiping price to pull from selected option
					*/
					break;
			}
			?>
			<br style="clear: both;"/>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){


});
/*]]>*/
</script>