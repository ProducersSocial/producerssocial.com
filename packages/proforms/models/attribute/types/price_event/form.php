<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));

?>
<style type="text/css">
.ui-datepicker-calendar .booked{background-color: #ffb6b6;}
.ui-datepicker-calendar .available{background-color: #c3ffb5;}
.ui-datepicker-calendar .ui-state-disabled{background-color: #e8e8e8!important;}
.time_booked{color: red;text-decoration: line-through;}
.price_time_options{margin-top: 22px;}
</style>
<?php     
switch($type){
	case 'from_to':
	?>
	<div class="text">
		<div class="input">
			<div class="well">
				<div class="input-append date">
					<input name="<?php    echo $field?>[dt1]" class="span2" id="dpd1" size="16" date-data-format="<?php    echo DATE_APP_DATE_PICKER?>" type="text" value="<?php    echo date(DATE_APP_GENERIC_MDY)?>" data-price="0">
					<span class="add-on"><i class="icon-calendar"></i> <?php    echo t(' Start')?></span>
				</div>
				<div class="input-append date">
					<input name="<?php    echo $field?>[dt2]" class="span2" id="dpd2" size="16" date-data-format="<?php    echo DATE_APP_DATE_PICKER?>" type="text" value="<?php    echo date(DATE_APP_GENERIC_MDY)?>" data-at="<?php    echo $akID?>" data-price="0">
					<span class="add-on"><i class="icon-calendar"></i> <?php    echo t(' End')?></span>
				</div>
          </div>
		</div>
	</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var disabledDays = <?php    echo $event_list?>;
	var enabledDays = <?php    echo $events_available?>;
	var bookedDays = <?php    echo $events_booked?>;
	var checkin = $('#dpd1').datepicker({
	  dateFormat: '<?php    echo DATE_APP_DATE_PICKER?>',
	  numberOfMonths: 1,
	  beforeShowDay: function(date){
	  	var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
        if(jQuery.inArray(string,bookedDays) >= 0){
	        return [true,'booked'];
        }else if(jQuery.inArray(string,enabledDays) >= 0){
	        return [true,'available'];
        }else{
	        return [false];
        }
      }
	});
	var checkout = $('#dpd2').datepicker({
	  dateFormat: '<?php    echo DATE_APP_DATE_PICKER?>',
	  numberOfMonths: 1,
	  beforeShowDay: function(date){
	  	var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
        if(jQuery.inArray(string,bookedDays) >= 0){
	        return [true,'booked'];
        }else if(jQuery.inArray(string,enabledDays) >= 0){
	        return [true,'available'];
        }else{
	        return [false];
        }
      },
      onSelect: function(date,obj){
      	
        $('#dpd2').attr('data-price',0);
  
	     var s = $("#dpd1").datepicker("getDate");
	     var sd  = s.getDate();
         var sm = s.getMonth() + 1;         
         var sy =  s.getFullYear();
         var start = sy + '-' + sm + '-' + sd;
         
	     var ed = $("#dpd2").datepicker("getDate");
	     var edd  = ed.getDate();
         var edm = ed.getMonth() + 1;         
         var edy =  ed.getFullYear();
         var end = edy + '-' + edm + '-' + edd;
		 
			 var input = $('#dpd2');
			 var event_price_check = '<?php     echo BASE_URL.DIR_REL;?>/index.php/tools/packages/proforms/proforms/attributes/price_event/price_check.php';
			 var akID = $('#dpd2').attr('data-at');
			 $.ajax({
				url: event_price_check,
				dataType: 'json',
				type: "POST",
				data:{
					date1: start,
					date2: end,
					akID: akID
				},
				success: function(response){
					//will only ever use the first instance of price.
					//DOES NOT SUPPORT MULTIPLE EVENT RETURNS
					var checkset = null;
					$('.price_time_options').remove();
					if(response.length > 1){
						$.each(response, function() {
						  $.each(this, function(f, event) {
						  	input.attr( 'data-price',( parseInt(input.attr('data-price')) + parseInt(event.event_price)));
						  	var status = '';
						  	if(event.status == 'booked'){
							   status = ' disabled=true';
						  	}
						  	var checked = '';
						  	if(!checkset){
							  	checked = 'checked';
							  	checkset = 1;
						  	}
						  });
						});
					}
				}
			}).done(function(){
				<?php   if(!$event_require_payment){ ?>
				updatePricing();
				$.event.trigger({
				  type:    "eventInfoLoaded",
				  message: "eventInfoLoaded fired.",
				  time:    new Date()
				});
				<?php   } ?>
			});
	
      }
	});
});
/*]]>*/
</script>
	<?php    
		break;
		
		
	case 'single_date':
	?>
	<div class="text">
		<div class="input">
			<div class="well">
				<div class="input-append date">
					<?php   
					$id = $field.'[dt2]';
					?>
					<input name="<?php  echo $id?>" class="span2" id="dpd2" size="16" date-data-format="<?php    echo DATE_APP_DATE_PICKER?>" type="text" value="<?php    echo date(DATE_APP_GENERIC_MDY)?>" data-at="<?php    echo $akID?>" data-price="0">

					<span class="add-on"><i class="icon-calendar"></i> <?php    echo t(' Date')?></span>
				</div>
          </div>
		</div>
	</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var disabledDays = <?php    echo $event_list?>;
	var enabledDays = <?php    echo $events_available?>;
	var bookedDays = <?php    echo $events_booked?>;
	var checkout = $('#dpd2').datepicker({
	  dateFormat: '<?php    echo DATE_APP_DATE_PICKER?>',
	  numberOfMonths: 1,
	  beforeShowDay: function(date){
	  	var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
        if(jQuery.inArray(string,bookedDays) >= 0){
	        return [true,'booked'];
        }else if(jQuery.inArray(string,enabledDays) >= 0){
	        return [true,'available'];
        }else{
	        return [false];
        }
      },
      onSelect: function(date,obj){
      	
		$(this).attr('data-price',0);	 

	     var ed = $("#dpd2").datepicker("getDate");
	     var edd  = ed.getDate();
         var edm = ed.getMonth() + 1;         
         var edy =  ed.getFullYear();
         var set_date = edy + '-' + edm + '-' + edd;
         
         //console.log(set_date);
         
		var input = $(this);
		var event_price_check = '<?php     echo BASE_URL.DIR_REL;?>/index.php/tools/packages/proforms/proforms/attributes/price_event/price_check.php';
		var akID = $(this).attr('data-at');
		$.ajax({
			url: event_price_check,
			type: "POST",
			dataType: 'json',
			data:{
				date1: set_date,
				akID: akID
			},
			success: function(data){
				var checkset = null;
				$('.price_time_options').remove();
				$.each(data, function() {
				  $.each(this, function(f, event) {
				  	var status = '';
				  	if(event.status == 'booked'){
					   status = ' disabled=true';
				  	}
				  	var checked = '';
				  	if(!checkset){
					  	checked = 'checked';
					  	checkset = 1;
					}
				    $("#dpd2").parent().append('<div class="price_time_options time_'+event.status+'"><input type="radio" name="akID['+$("#dpd2").attr('data-at')+'][value][eID]" class="price_option_time" data-value="'+event.event_price+'" value="'+event.eID+'" '+status+' '+checked+'/> '+event.title+': '+event.start+' - '+event.end+'</div>');
				  });
				});
			}
		}).done(function(){
			<?php   if(!$event_require_payment){ ?>
			updatePricing();
			$('.price_option_time').click(function(){
				updatePricing();
				$.event.trigger({
				  type:    "eventInfoLoaded",
				  message: "eventInfoLoaded fired.",
				  time:    new Date()
				});
			});
			$.event.trigger({
				  type:    "eventInfoLoaded",
				  message: "eventInfoLoaded fired.",
				  time:    new Date()
				});
			<?php   } ?>
		});
	  }
	});
});
/*]]>*/
</script>
	<?php    
		break;
		
	case 'specific_date':
	?>
	<div class="text">
		<div class="input">
			<div class="well">
				<div class="input-append date">
					<input name="<?php    echo $field?>[dt2]" class="span2" id="dpd2" size="16" date-data-format="<?php    echo DATE_APP_DATE_PICKER?>" type="hidden" value="<?php    echo $search_date?>" data-at="<?php    echo $akID?>" data-price="0" disabled='true'/>
					<span><?php    echo $search_date?></span>
					<span class="add-on"><i class="icon-calendar"></i> <?php    echo t(' Date')?></span>
				</div>
          </div>
		</div>
	</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	$(this).attr('data-price',0);	 
	var set_date = $("#dpd2").val();

	var event_price_check = '<?php     echo BASE_URL.DIR_REL;?>/index.php/tools/packages/proforms/proforms/attributes/price_event/price_check.php';
	var akID = $("#dpd2").attr('data-at');
	$.ajax({
		url: event_price_check,
		type: "POST",
		dataType: 'json',
		data:{
			date1: set_date,
			akID: akID
		},
		success: function(data){
			$('.price_time_options').remove();
			$.each(data, function() {
				var checkset = null;
				$.each(this, function(f, event) {
					var status = '';
					if(event.status == 'booked'){
						status = ' disabled=true';
					}
					var checked = '';
					if(!checkset){
					  	checked = 'checked';
					  	checkset = 1;
					}
					$("#dpd2").parent().append('<div class="price_time_options time_'+event.status+'"><input type="radio" name="akID['+$("#dpd2").attr('data-at')+'][value][eID]" class="price_option_time" data-value="'+event.event_price+'" value="'+event.eID+'" '+status+' '+checked+'/> '+event.title+': '+event.start+' - '+event.end+'</div>');
				});
			});
		}
	}).done(function(){
		<?php   if(!$event_require_payment){ ?>
		updatePricing();
		$.event.trigger({
				  type:    "eventInfoLoaded",
				  message: "eventInfoLoaded fired.",
				  time:    new Date()
				});
		<?php   } ?>
	});
});
/*]]>*/
</script>
	<?php    
		break;
		
		case 'listen':
		?>
		<script type="text/javascript">
		/*<![CDATA[*/
		$(document).ready(function(){
			$.event.trigger({
			  type:    "eventInfoLoaded",
			  message: "eventInfoLoaded fired.",
			  time:    new Date()
			});
		});
		/*]]>*/
		</script>
		<?php 
		break;
}
?>
<input name="<?php    echo $field?>[qty]" type="hidden" value="1" id="event_qty"/>
<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){

			$('.price_qty input').change(function(){
				var qty = 0;
				$('.price_qty input').each(function(){
					qty += parseFloat($(this).val());
				});
				if(qty==0){qty = 1;}
				$('#event_qty').val(qty);
			});

	});
	/*]]>*/
</script>