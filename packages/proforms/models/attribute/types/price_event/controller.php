<?php 
defined('C5_EXECUTE') or die("Access Denied.");

class PriceEventAttributeTypeController extends AttributeTypeController  {

	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';

	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		$at = $this->attributeType;
		//$jspath = $at->getAttributeTypeFileURL('js/bootstrap-datepicker.js');
		//$this->addFooterItem(Loader::helper('html')->javascript($jspath));
		//$cssfpath = $at->getAttributeTypeFileURL('css/datepicker.css');
		//$this->addHeaderItem(Loader::helper('html')->css($cssfpath));

		$field = $this->field('value');
		$this->set('field',$field);

		$this->getEvents();

		switch ($this->type){
			case 'of_page':

				break;
			case 'listen':
				$fm = Loader::helper('form');

				//print $fm->text($field.'[dt2]',$this->checkPrice($this->search_date),array('disabled'=>'disabled'));
				//$this->set('event_list',$this->event_list);
				$db = Loader::db();
				if($_REQUEST['eID']){

					global $c;

					Loader::model('event_item','proevents');
					$eventify = Loader::helper('eventify','proevents');

					$eID = $_REQUEST['eID'];
					$event = $db->getRow("SELECT * FROM btProEventDates WHERE eID =?",array($eID));
					$value = t('Date: ').date('D M d Y',strtotime($event['date']));

					if($event['allday']<1){
						$value .= ' '.date('g:ia',strtotime($event['sttime'])).'-'.date('g:ia',strtotime($event['entime']));
					}

					extract($eventify->getEventVars($c,$eID));

					echo '<h2>'.$eventTitle.'</h2>';
					echo '<div class="date-times">';

					$value .= ' ('.$event['event_price'].')';
					print $value;
					print '<input type="hidden" name="akID['.$this->attributeKey->getAttributeKeyID().'][value][dt2]" value="'.$event['date'].'" data-price="'.$event['event_price'].'" />';

					print '<input type="hidden" name="akID['.$this->attributeKey->getAttributeKeyID().'][value][eID]" id="akID['.$this->attributeKey->getAttributeKeyID().'][value][eID]" class="price_static_option_time" value="'.$eID.'" />';

					echo '</div>';
					echo '<br />';

				}else{
					global $c;
					//var_dump($c);
					Loader::model('event_item','proevents');
					$eventify = Loader::helper('eventify','proevents');

					extract($eventify->getEventVars($c));

					echo '<h2>'.$eventTitle.'</h2>';
					echo '<div class="date-times">';

					print '<input type="hidden" name="akID['.$this->attributeKey->getAttributeKeyID().'][value][dt2]" value="'.date(DATE_APP_GENERIC_MDY,strtotime($next_dates_array[0]->date)).'" data-price="'.$next_dates_array[0]->event_price.'" />';

					print '<input type="hidden" name="akID['.$this->attributeKey->getAttributeKeyID().'][value][eID]" value="'.$next_dates_array[0]->eID.'" id="akID['.$this->attributeKey->getAttributeKeyID().'][value][eID]" value="'.$next_dates_array[0]->eID.'" class="price_static_option_time"/>';

					if(is_array($next_dates_array)){
						foreach($next_dates_array as $next_date){

							echo date('M dS ',strtotime($next_date->date));

							if ($event_allday !=1){
							 	echo date(t('g:i a'),strtotime($next_date->start)).' - '.date(t('g:i a'),strtotime($next_date->end));
							}else{
								echo ' - '.t('All Day');
							}

							if($next_date->event_price > 0){ echo ' : <span class="currency_symbol"></span>'.money_format('%(#4n',$next_date->event_price * 1);}
							echo '<br/>';
						}
					}
					
					echo '</div>';
					echo '<br />';
				}
				break;
			case 'specific_date':
				$fm = Loader::helper('form');

				//print $fm->text($field.'[dt2]',$this->checkPrice($this->search_date),array('disabled'=>'disabled'));
				$this->set('event_list',$this->event_list);
				break;
			case 'single_date':
				$this->parse_events();

				$this->set('event_list',$this->event_list);
				$this->set('events_booked',$this->events_booked);
				$this->set('events_available',$this->events_available);
				$this->set('events_options',$this->events_options);

				break;
			case 'from_to':
				$this->parse_events();

				$this->set('event_list',$this->event_list);
				$this->set('events_booked',$this->events_booked);
				$this->set('events_available',$this->events_available);
				$this->set('events_options',$this->events_options);

				break;
		}
	}

	public function getEvents($date=null){
		if(!$date){
			$date = date('Y-m-1');
		}

		Loader::model('event_list','proevents');
		$el = new EventList();
		$el->filterByBookable();
		$el->filterByAllDates(null,null,0);

		if ($this->category != 'All Categories') {
			$selected_cat = explode(', ',$this->category);
			$el->filterByCategories($selected_cat);
		}

		if ($this->section != 'All Sections') {
			$el->filterByParentID($this->section);
		}
		//$el->debug();
		$this->events = $el->get();

		return true;
	}


	public function parse_events($date=null){
		if(!$date){
			$date = date('Y-m-1');
		}

		$events = $this->events;

		$db = Loader::db();
		$dth = Loader::helper('form/date_time_time','proevents');

		$events_options = array();
		$events_array = array();
		$booked_array = array();
		$include_string = '[';
		$events_booked = '[';
		$event_pricing = '';
		foreach($events as $date_string => $ep){
			$i++;

	  		$date_array = $dth->translate_from_string($date_string);

	  		Loader::model('event_item','proevents');
			$event_item = new EventItem($ep,$date_string);

	  		$eID = $date_array['eID'];
	  		$edate = date('Y-m-d',strtotime($date_array['date']));
	  		$stime = $date_array['start'];
	  		$etime = $date_array['end'];

			$events_array[] = $edate;

			$eventID = $ep->getCollectionID();
			$eventName = $ep->getCollectionName();

			if(!in_array($eventName,$events_options)){
				$events_options[$eventID] = $eventName;
			}

			$r = $db->Query("SELECT * FROM btProEventDates WHERE eventID = ? AND date = ? AND status = 'available'",array($eventID,$edate));
			$available = $r->RecordCount();

			if($available == 0 && !in_array($edate,$booked_array)){
  				if($eb){ $events_booked .= ',';}
  				$booked_array[] = $edate;
	  			$events_booked .= "'".$edate."'";
	  			$eb++;
  			}else{
  				if($ab){ $include_string .= ',';}
	  			$include_string .= "'".$edate."'";
	  			$ab++;
  			}
		}
		$events_booked .= ']';
		$include_string .= ']';

		$exclude_string = '[';

		for($d=0;$d<=365;$d++){
			if(!in_array(date('Y-m-d',strtotime('+'.$d.' day',strtotime($date))),$events_array)){
				if($t){ $exclude_string .= ',';}
				$exclude_string .= "'".date('Y-m-d',strtotime('+'.$d.' day',strtotime($date)))."'";
				$t++;
			}
		}

		$exclude_string .= ']';

		$this->event_list = $exclude_string;
		$this->events_booked = $events_booked;
		$this->events_available = $include_string;
		$this->events_options = $events_options;
	}


	public function validateForm($data) {
		$this->load();
		$db = Loader::db();
		$dates = array();
		//$this->error = $data['value']['qty'];
		if($data['value']['dt1']){
			$date1 = $this->setDatePickerFormat($data['value']['dt1']);
			$date2 = $this->setDatePickerFormat($data['value']['dt2']);

			$d1m = date('n',strtotime($date2));
			$d1d = date('j',strtotime($date2));
			$d1y = date('Y',strtotime($date2));

			$d2m = date('n',strtotime($date1));
			$d2d = date('j',strtotime($date1));
			$d2y = date('Y',strtotime($date1));

			$datetime1 = mktime(0,0,0,$d1m,$d1d,$d1y);
			$datetime2 = mktime(0,0,0,$d2m,$d2d,$d2y);

			$interval = floor(($datetime1-$datetime2)/86400) ;

			$dayspan = $interval;

			for($d=0;$d<=$dayspan;$d++){
				$dates[] = date('Y-m-d',strtotime('+'.$d.' day',strtotime($date1)));
			}
		}else{
			$dates[] = $this->setDatePickerFormat($data['value']['dt2']);
		}

		if($data['value']['eID']){
			$seID = $data['value']['eID'];
			$selected_event_time = $db->getRow("SELECT * FROM btProEventDates WHERE eID = ?",array($seID));
		}


		$this->getEvents();

		$events = $this->events;

		$dth = Loader::helper('form/date_time_time','proevents');

		foreach($events as $date_string => $ep){
	  		$date_array = $dth->translate_from_string($date_string);

	  		$eID = $date_array['eID'];

		  	$date = $date_array['date'];
		  	
	  		if(in_array($date,$dates)){
	  			if($data['value']['eID']){
	  				$event = $db->getRow("SELECT * FROM btProEventDates WHERE eID = ? AND sttime = ? ANd entime = ?",array($eID,$selected_event_time['sttime'],$selected_event_time['entime']));
	  			}else{
	  				$event = $db->getRow("SELECT * FROM btProEventDates WHERE eID = ?",array($eID));
	  			}

	  			if($event){
	  				if($this->error){$this->error .= '<br/>';}
	  		
		  			if($event['event_qty'] < 1 || $event['status'] == 'booked'){
			  			$this->error .= t('Date '.$date.' is no longer available!');
		  			}
		  			if($data['value']['qty'] > $event['event_qty']){
						$this->error .= t('Date '.$date.' can not accommodate your desired Quantity!');
					}
				}
	  		}

	  	}
	  	//$this->error = $data['value']['eID'];
	  	if($this->error){
		  		return false;
	  	}
		return true;
	}

	public function type_form() {
		$this->load();
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}

		$db = Loader::db();
		$row = $db->GetRow('select * from atPriceEventSettings where akID = ?', $this->attributeKey->getAttributeKeyID());

		$this->akID = $row['akID'];
		$this->type = $row['type'];
		$this->category = $row['category'];
		$this->section = $row['section'];
		$this->search_date = $row['search_date'];
		$this->event_require_payment = $row['event_require_payment'];

		$this->set('akID', $this->akID);
		$this->set('type', $this->type);
		$this->set('category', $this->category);
		$this->set('section', $this->section);
		$this->set('search_date', $this->search_date);
		$this->set('event_require_payment', $this->event_require_payment);

		$this->set('akID',$this->getAttributeKey()->getAttributeKeyID());

	}

	public function checkPrice($date1=null,$date2=null){
		$this->load();
		$db = Loader::db();
		$this->getEvents();
		$events = $this->events;
		$event_array = array();
		$dth = Loader::helper('form/date_time_time','proevents');
		foreach($events as $date_string => $ep){
	  		$date_array = $dth->translate_from_string($date_string);

	  		$eID = $date_array['eID'];
	  		$ldate = $date_array['date'];

	  		if(!$date2){
		  		if(date('Y-m-d',strtotime($ldate)) == date('Y-m-d',strtotime($date1))){
		  			$event_array[] = $db->getAll("SELECT eID,title,TIME_FORMAT(sttime,'%l:%i %p') as start,TIME_FORMAT(entime,'%l:%i %p') as end,status,event_price FROM btProEventDates WHERE eID = ?",array($eID));
		  		}
		  	}else{
		  		if(date('Ymd',strtotime($ldate)) >= date('Ymd',strtotime($date1)) && date('Ymd',strtotime($ldate)) <= date('Ymd',strtotime($date2))){
		  			$event_array[] = $db->getAll("SELECT eID,title,TIME_FORMAT(sttime,'%l:%i %p') as start,TIME_FORMAT(entime,'%l:%i %p') as end,status,event_price FROM btProEventDates WHERE eID = ?",array($eID));
		  		}
		  	}
	  	}
	  	$jh = Loader::helper('json');
	  	return $jh->encode($event_array);
	}

	public function getValue(){
		$this->load();
		$db = Loader::db();
		$event_string = $db->GetOne('select value from atPriceEvent where avID = ?', $this->getAttributeValueID());
		return $event_string;
	}


	public function getDisplayValue(){
		$this->load();
		$db = Loader::db();
		$event_string = $db->GetOne('select value from atPriceEvent where avID = ?', $this->getAttributeValueID());

		$eventIDs = explode(',',$event_string);

		$date1 = $db->GetOne('SELECT date FROM btProEventDates WHERE eID = ?',array($eventIDs[0]));
		$date2 = $db->GetRow('SELECT * FROM btProEventDates WHERE eID = ?',array($eventIDs[count($eventIDs) - 1]));
		
		$value = '<strong>'.$date2['title'].'</strong>';

		switch ($this->type){
			case 'from_to':
				$value .= t('From: ').date('D Md Y',strtotime($date1));
				$value .= t(' / To: ').date('D Md Y',strtotime($date2['date']));
				break;
			case  'single_date':
			case  'specific_date':
				$value = t('Date: ').date('D Md Y',strtotime($date2['date']));
				if($date2['allday']<1){
					$value .= ' '.date('g:ia',strtotime($date2['sttime'])).'-'.date('g:ia',strtotime($date2['entime']));
				}
				break;
		}

		return $value;
	}

	public function getDisplayReviewValue(){
		$this->load();
		$db = Loader::db();
		$event_string = $db->GetOne('select value from atPriceEvent where avID = ?', $this->getAttributeValueID());

		$eventIDs = explode(',',$event_string);

		$date1 = $db->GetOne('SELECT date FROM btProEventDates WHERE eID = ?',array($eventIDs[0]));
		$date2 = $db->GetRow('SELECT * FROM btProEventDates WHERE eID = ?',array($eventIDs[count($eventIDs) - 1]));

		$value = '<strong>'.$date2['title'].'</strong>';

		switch ($this->type){
			case 'from_to':
				$value .= t('From: ').date('D Md Y',strtotime($date1));
				$value .= t(' / To: ').date('D Md Y',strtotime($date2['date']));
				break;
			case  'single_date':
			case  'specific_date':
				$value = t('Date: ').date('D Md Y',strtotime($date2['date']));
				if($date2['allday']<1){
					$value .= ' '.date('g:ia',strtotime($date2['sttime'])).'-'.date('g:ia',strtotime($date2['entime']));
				}
				break;
		}

		return $value;
	}

	public function display_review(){

		$this->load();
		$db = Loader::db();
		$event_string = $db->GetOne('select value from atPriceEvent where avID = ?', $this->getAttributeValueID());

		$eventIDs = explode(',',$event_string);

		$date1 = $db->GetOne('SELECT date FROM btProEventDates WHERE eID = ?',array($eventIDs[0]));
		$date2 = $db->GetRow('SELECT * FROM btProEventDates WHERE eID = ?',array($eventIDs[count($eventIDs) - 1]));

		$value = '<strong>'.$date2['title'].'</strong> - ';

		switch ($this->type){
			case 'from_to':
				$value .= t('From: ').date('D Md Y',strtotime($date1));
				$value .= t(' / To: ').date('D Md Y',strtotime($date2['date']));
				break;
			case  'single_date':
			case  'specific_date':
				$value .= t('Date: ').date('D Md Y',strtotime($date2['date']));
				if($date2['allday']<1){
					$value .= ' '.date('g:ia',strtotime($date2['sttime'])).'-'.date('g:ia',strtotime($date2['entime']));
				}
				break;
			case  'listen':
				$value .= t('Date: ').date('D Md Y',strtotime($date2['date']));
				if($date2['allday']<1){
					$value .= ' '.date('g:ia',strtotime($date2['sttime'])).'-'.date('g:ia',strtotime($date2['entime']));
				}
				break;
		}

		print $value;
	}
	
	public function getRequirePayment(){
		return $this->event_require_payment;
	}

	public function setDatePickerFormat($date){

		//$date = date(DATE_APP_GENERIC_MDY,strtotime($date));

		switch(DATE_APP_DATE_PICKER){

			case 'dd/mm/yy':
			case 'd/m/yy':
				list($day, $month, $year) = explode('/', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'dd-mm-yy':
			case 'd-m-yy':
				list($day, $month, $year) = explode('-', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'mm/dd/yy':
			case 'm/d/yy':
				list($month, $day, $year) = explode('/', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'yy/mm/dd':
			case 'yy/m/d':
				list($year, $month, $day) = explode('/', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'yy/dd/mm':
			case 'yy/d/m':
				list($year, $day, $month) = explode('/', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'mm-dd-yy':
			case 'm-d-yy':
				list($month, $day, $year) = explode('-', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'yy-mm-dd':
			case 'yy-m-d':
				list($year, $month, $day) = explode('-', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;

			case 'yy-dd-mm':
			case 'y-d-mm':
				list($year, $day, $month) = explode('-', $date);
				return $year.'-'.sprintf("%02s", $month).'-'.sprintf("%02s", $day);

				break;
		}

	}


	public function saveForm($data) {
		$this->load();
		$db = Loader::db();
		$dates = array();
		if($data['value']['dt1']){

			$date1 = $this->setDatePickerFormat($data['value']['dt1']);
			$date2 = $this->setDatePickerFormat($data['value']['dt2']);

			$d1m = date('n',strtotime($date2));
			$d1d = date('j',strtotime($date2));
			$d1y = date('Y',strtotime($date2));

			$d2m = date('n',strtotime($date1));
			$d2d = date('j',strtotime($date1));
			$d2y = date('Y',strtotime($date1));

			$datetime1 = mktime(0,0,0,$d1m,$d1d,$d1y);
			$datetime2 = mktime(0,0,0,$d2m,$d2d,$d2y);

			$interval = floor(($datetime1-$datetime2)/86400) ;

			$dayspan = $interval;

			for($d=0;$d<=$dayspan;$d++){
				$dates[] = date('Y-m-d',strtotime('+'.$d.' day',strtotime($date1)));
			}
		}else{
			$dates[] = $this->setDatePickerFormat($data['value']['dt2']);
		}

		if($data['value']['eID']){
			$seID = $data['value']['eID'];
			$booked_events[] = $seID;
			
		}else{
    		$this->getEvents();
    
    		$events = $this->events;
    
    		$dth = Loader::helper('form/date_time_time','proevents');
    		$booked_events = array();
    
    		foreach($events as $date_string => $ep){
    	  		$date_array = $dth->translate_from_string($date_string);
    
    	  		$eID = $date_array['eID'];
    	  		$date = $date_array['date'];
    
    	  		if(in_array($date,$dates)){
    	  		
                    $event = $db->getRow("SELECT * FROM btProEventDates WHERE eID = ?",array($eID));
                      
    	  			if($event){
    	  				$booked_events[] = $eID;
    	  			}
    	  		}
    	  	}
	  	}

	  	$events_string = implode(',',$booked_events);

	  	$db = Loader::db();
		$db->Replace('atPriceEvent', array('avID' => $this->getAttributeValueID(),'value' => $events_string), 'avID', true);

	}

	public function saveValue($value) {
		$this->saveForm($value);
	}

	public function deleteValue(){

	}

	public function validateKey($args){
		$e = parent::validateKey($args);
		$pkg = Package::getByHandle('proevents');
		if(!$pkg){
			$e->add(t('You do not have ProEvents installed.  You must purchase ProEvents to use this Attribute/Question Type.'));
		}
		return $e;
	}


	public function saveKey($data) {

        $search_date = $this->setDatePickerFormat($data['search_date']);

		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'type'=>$data['type'],
			'category'=>($data['category']) ? implode(',',$data['category']) : 'All Categories',
			'section'=>$data['section'],
			'search_date'=>$search_date,
			'event_require_payment'=> ($data['event_require_payment']) ? 1:0,
		);

		$db=Loader::db();
		$db->Replace('atPriceEventSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceEventSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}