<?php     
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('proforms_item','proforms');
$fm = Loader::helper('form');
$bt = BlockType::getByHandle('proforms_display');
//$ProformsItemID = $_REQUEST['entryID'];
$aim = BASE_URL.DIR_REL.'/tools/packages/proforms_extend_authorize/proforms/payment/authorize/aim.php';

if($authorize_sandbox == 1){
	$set_sandbox = 'false';
}else{
	$set_sandbox = 'true';
}

echo $ProformsItemID;
echo '<div class="payment_form">';
echo $fm->hidden('set_sandbox', $set_sandbox);
echo $fm->hidden('authorize_price_handle',$authorize_price_handle,array('readonly'=>'readonly'));
//echo $fm->hidden($field_value,'');
//Billing Info
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Fist Name</label></strong>
	<div class="input">'.$fm->text('x_first_name','',array('placeholder'=>'x_first_name', 'size'=>'15')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Last Name</label></strong>
	<div class="input">'.$fm->text('x_last_name','',array('placeholder'=>'x_last_name', 'size'=>'14')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Address</label></strong>
	<div class="input">'.$fm->text('x_address','',array('placeholder'=>'x_address', 'size'=>'26')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">City</label></strong>
	<div class="input">'.$fm->text('x_city','',array('placeholder'=>'x_city', 'size'=>'15')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">State</label></strong>
	<div class="input">'.$fm->text('x_state','',array('placeholder'=>'x_state', 'size'=>'4')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Zip</label></strong>
	<div class="input">'.$fm->text('x_zip','',array('placeholder'=>'x_zip', 'size'=>'9')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Country</label></strong>
	<div class="input">'.$fm->text('x_country','',array('placeholder'=>'x_country', 'size'=>'22')).'</div>
	</div>';
//Credit Card Info
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Credit Card Number</label></strong>
	<div class="input">'.$fm->text('x_card_num','',array('placeholder'=>'Credit Card Number', 'size'=>'15')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_num">Exp. Date</label></strong>
	<div class="input">'.$fm->text('x_exp_date','',array('placeholder'=>'Exp', 'size'=>'4')).'</div>
	</div>';
echo '<div class="clearfix text">
	<strong><label for="x_card_code">CCV</label></strong>
	<div class="input">'.$fm->text('x_card_code','',array('placeholder'=>'CCV', 'size'=>'4')).'</div>
	</div>';

//Get's Done in Tool
echo $fm->hidden('authorize_transaction_id',$authorize_transaction_id);

echo '</div>';
?>
<script type="text/javascript">
$(document).ready(function(){
	$('form.proform_slider').submit(function(e) { 
     e.preventDefault();
     e.returnValue = false;
     form = $(this);
     // some validation code here: if valid, add podkres1 class
     //if ($('input.podkres1').length > 0) { 
        // do nothing
     //} else {
        var $form = $(this);
        var postStr = $("form.proform_slider").serialize();
         $.ajax({ 
			type: 'post',
			url: '<?php  echo $aim; ?>', 
			data: postStr,
			//dataType: 'json',
			success: function(data){
				console.log(data);
	           /* if(data.transaction_id != ''){
	            	$( "input[name=authorize_transaction_id]" ).val(data.transaction_id);//Set Transaction ID
	            	console.log(data.transaction_id);
	            	console.log('Sandbox = '+ data.sandbox);
	            	// submit the form
	                form.off('submit');
	               	form.submit();
	            }else{
	            	console.log(data.error);
	            	console.log('Sandbox = '+ data.sandbox);
	            }*/
			},
             error: function(){
             	console.log('Something went wrong with the ajax');
             	console.log(data);
             }
         });
    // }
	});
});
</script>