<?php     
defined('C5_EXECUTE') or die("Access Denied.");

class PaymentAuthorizeAttributeTypeController extends AttributeTypeController  {
	public $hideLabel=1;//Need To Figure That Out
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		//$this->set('field_value', $this->field('value'));
	}
	
	public function display_total(){
		$this->load();
	}

	public function display(){
		$this->load();
		$db = Loader::db();
		$avID = $this->getAttributeValueID();
		$row = $db->GetRow('select * from atPaymentAuthorize where avID = ?', $avID);
		print $row['transaction_id'];
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<p>This uses the Authorize.Net AIM method to submit payments. You must use HTTPS in oder for the to be secure.';
		//API Login ID
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_api_login_id" class="control-label">'.t('Authorize API Login ID').'</label>';
		print '		<div class="input">';
		print $fm->text('authorize_api_login_id',$this->authorize_api_login_id,array('placeholder'=>'authorize_api_login_id'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		//Transaction Key
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_transaction_key" class="control-label">'.t('Authorize Transaction Key').'</label>';
		print '		<div class="input">';
		print $fm->text('authorize_transaction_key',$this->authorize_transaction_key,array('placeholder'=>'authorize_transaction_key'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		//Sandbox Mode
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_sandbox" class="control-label">'.t('Sandbox Mode?').'</label>';
		print '		<div class="input">';
		print $fm->checkbox('authorize_sandbox',1,$this->authorize_sandbox);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		//Total Price Handle
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="authorize_price_handle" class="control-label">'.t('Total Handle').'</label>';
		print '		<div class="input">';
		print $fm->text('authorize_price_handle',$this->authorize_price_handle,array('placeholder'=>'total_question_handle'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPaymentAuthorizeSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		//$this->authorize_api_login_id = $row['authorize_api_login_id'];
		//$this->authorize_transaction_key = $row['authorize_transaction_key'];
		//$this->authorize_secret_question = $row['authorize_secret_question'];
		$this->authorize_price_handle = $row['authorize_price_handle'];
		$this->authorize_return_page = $row['authorize_return_page'];
		$this->authorize_sandbox = $row['authorize_sandbox'];

		$this->set('akID', $this->akID);
		//$this->set('authorize_api_login_id', $this->authorize_api_login_id);
		//$this->set('authorize_transaction_key', $this->authorize_transaction_key);
		$this->set('authorize_price_handle', $this->authorize_price_handle);
		$this->set('authorize_sandbox', $this->authorize_sandbox);
	}

	public function getAuthorizeCredentials(){
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		$db = Loader::db();
		$row = $db->GetRow('select * from atPaymentAuthorizeSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		$this->authorize_api_login_id = $row['authorize_api_login_id'];
		$this->authorize_transaction_key = $row['authorize_transaction_key'];
		$this->set('authorize_api_login_id', $this->authorize_api_login_id);
		$this->set('authorize_transaction_key', $this->authorize_transaction_key);
		return $this;
	}

	public function getValue(){
		return $this->hidden_value;
	}

	
	public function saveForm($value){
		$this->load();
		//print_r($_REQUEST); //This will give an error but at least you can see what's happening
		//Authorize Variables
		$price_handle = $_REQUEST['authorize_price_handle'];//Authorize Price Handle
		$transID = $_REQUEST['authorize_transaction_id'];//Authorize Transaction ID
		//Form Variables
		$question_set =  $_REQUEST['question_set']; //Question Set ID
		$attribute_keys = $_REQUEST['akID']; //Atribute Keys
		$ProformsItemID = $_REQUEST['ProformsItemID'];
		$pfo = ProFormsItem::getByID($ProformsItemID);
		$question_set = $pfo->asID;
		$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
		$db = Loader::db();	
		//Update the Price Total with Transaction ID so that it is PAID
		foreach ($setAttribs as $ak) {
			if($ak->getAttributeType()->getAttributeTypeHandle() == 'price_total'){
				$value = $pfo->getAttributeValueObject($ak);
				$amount = $ak->getController()->getRawValue($value->getAttributeValueID());
				$amount = money_format('%(#4n',$amount);
				$db->Execute("DELETE FROM atPriceTotalPayments WHERE avID = ?",array($value->getAttributeValueID()));
				$db->Execute("INSERT INTO atPriceTotalPayments (avID,amount,payment_date,transactionID) VALUES(?,?,?,?)",array($value->getAttributeValueID(),$amount,date('Y-m-d'),$transID));
				Events::fire('proforms_item_payment',$pfo);
			}
		}
		//Save Transaction ID to atPaymentAuthorize
		$avID = $this->getAttributeValueID();
		//$db->Execute('delete from atPaymentAuthorize where avID = ?', array($avID));//Dont Need?
		$db->Execute('insert into atPaymentAuthorize (avID,total,paid,transaction_id) values (?,?,?,?)', array($avID,$amount,0,$transID));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'authorize_api_login_id'=>$data['authorize_api_login_id'],
			'authorize_transaction_key'=>$data['authorize_transaction_key'],	
			'authorize_price_handle'=>$data['authorize_price_handle'],
			'authorize_sandbox'=>$data['authorize_sandbox']
		);
		
		$pkg = Package::getByHandle('proforms');
		$pkg->saveConfig('AUTHORIZE_TESTMODE', $data['authorize_sandbox']);
		
		$db=Loader::db();
		$db->Replace('atPaymentAuthorizeSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPaymentAuthorizeSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}