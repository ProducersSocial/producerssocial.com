<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('attribute/types/default/controller');

class ColorAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function getDisplaySanitizedValue() {
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('ccm.colorpicker.css'));
   		$this->addHeaderItem($html->javascript('jquery.colorpicker.js'));
   		
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		$html = '<div class="ccm-color-swatch-wrapper" style="background: none;"><div class="ccm-color-swatch" style="background: none;"><div hex-color="' . $value . '" style="background: none; background-color:' . $value . '"></div></div></div>';

		return $html;
	}
	
	public function form() {
		$html = Loader::helper('html');
		$form = Loader::helper('form');
		$this->addHeaderItem($html->css('ccm.app.css'));
   		$this->addHeaderItem($html->javascript('ccm.app.js'));

		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		$fieldFormName = 'akID['.$this->attributeKey->getAttributeKeyID().'][value]';
		$html = '';
		$html .= '
		<style type="text/css">
		.colorpicker .colorpicker_none{padding: 3px 8px!important;}
		.colorpicker .colorpicker_submit{padding: 3px 8px!important;}
		.ccm-ui .colorpicker input[type="text"]{width: auto!important;height: 18px!important;margin-top: 0px;}
		</style>';
		$html .= '<div id="f' . $this->attributeKey->getAttributeKeyHandle() . '" hex-color="' . $value . '" ></div>';
		$html .= $form->hidden($fieldFormName, $value);

		$html .= "<script type=\"text/javascript\">
	$(function() {
		var f" .$this->attributeKey->getAttributeKeyHandle(). "Div =$('div#f" .$this->attributeKey->getAttributeKeyHandle(). "');
		var c" .$this->attributeKey->getAttributeKeyHandle(). " = f" .$this->attributeKey->getAttributeKeyHandle(). "Div.attr('hex-color'); 
		f" .$this->attributeKey->getAttributeKeyHandle(). "Div.ColorPicker({
			flat: true,
			color: c" .$this->attributeKey->getAttributeKeyHandle(). ",
			clickNone: function() {
				return false;
			},
			onSubmit: function(hsb, hex, rgb, cal) { 
				$('input[name=" . '"akID['.$this->attributeKey->getAttributeKeyID().'][value]"' . "]').val('#' + hex);
			},  
			onNone: function(cal) {  
				$('input[name=" . $fieldFormName . "]').val('');		
				$('div#f" . $this->attributeKey->getAttributeKeyHandle(). "').css('backgroundColor',''); 
				cal.css('display','block');
			}
		});
		$('input.colorpicker_none').remove();
		//$('input.colorpicker_submit').attr('value', '');
	});
</script>";
		print $html;
	}

}
?>