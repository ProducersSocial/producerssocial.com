<?php     
defined('C5_EXECUTE') or die("Access Denied.");

class StatusAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$this->set('value',$this->getValue());
	}
	
	public function basic_form(){
		$this->load();
		$this->set('value',$this->getValue());
	}

	
	public function type_form() {
		$this->load();
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atValidatedMediaSettings where akID = ?', $ak->getAttributeKeyID());
			
			$this->akID = $row['akID'];

			$this->set('akID', $ak->getAttributeKeyID());
		}
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The powerful Status Question Type allows you establish one or several status measures for tracking the progress of any given form. </p>
		<br /> 
		<h4>Advanced Use</h4>
		<p>The Status Question comes with it\'s own Sitewide Event Handlers that fire on a per-status basis. proforms_item_approve, proforms_item_pending, & proforms_item_deny all fire independently and can be "hooked" into from third party packages.  Want to do XYZ with W form once G department has approved it?  there you go!</p>
		');
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT status FROM atStatus WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function getSearchIndexValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT status FROM atStatus WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function searchForm($list){
		$this->load();
		$handle = 'ak_'.$this->attributeKey->getAttributeKeyHandle();

		if($this->request('value') == 'pending'){
			$list->filter(false,"$handle LIKE '%pending%' OR $handle IS NULL");
		}else{
			$val = $this->request('value');
			$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), "%$val%", 'LIKE');
		}

		return $list;
	}
	

	public function search() {
		$f = Loader::helper('form');
		$options = array(
			'pending'=>'Filter Pending',
			'notified'=>'Filter Notified',
			'approved'=>'Filter Approved',
			'denied'=>'Filter Denied'
		);
		print ' &nbsp;&nbsp;'.$f->select($this->field('value'), $options, $this->request('value'));
	}
	
	public function saveForm($data) {

		if($this->getAttributeValueID()){
			$ovalue = $this->getValue();
		}
		$value = $data['value'];
		$this->load();
		$db = Loader::db();
		$db->Replace('atStatus', array('avID' => $this->getAttributeValueID(),'status' => $value), 'avID', true);
		
		if($_REQUEST['ProformsItemID']){
			$pf = ProformsItem::getByID($_REQUEST['ProformsItemID']);
			if($value=='approved' && $value != $ovalue){
				Events::fire('proforms_item_approve', $pf,$this->attributeKey->getAttributeKeyHandle());
			}elseif($value=='denied' && $value != $ovalue){
				Events::fire('proforms_item_deny', $pf,$this->attributeKey->getAttributeKeyHandle());
			}elseif($value=='pending' && $value != $ovalue){
				Events::fire('proforms_item_pending', $pf,$this->attributeKey->getAttributeKeyHandle());
			}
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atStatus where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID()
		);
		
		$db=Loader::db();
		$db->Replace('atStatusSettings', $vals, 'akID', true);
		
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atStatusSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}