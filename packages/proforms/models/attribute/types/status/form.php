<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
$options = array(''=>t('-- Select One --'),'pending'=>t('Pending'),'approved'=>t('Approved'),'denied'=>t('Denied'));
?>

<select name="akID[<?php    echo $akID?>][value]" id="status_<?php     echo $akID;?>" class="msdrops">
	<?php    
	foreach($options as $key=>$option){
		if($value == $key){
			$selected = 'selected';
		}else{
			$selected = '';
		}
		echo '<option value="'.$key.'" data-image="'.$this->getAttributeKey()->getAttributeType()->getAttributeTypeFileURL('images/'.$key.'.png').'" '.$selected.'>'.$option.'</option>';
	}
	?>
</select>


<script type="text/javascript">
$(function() {
	try {
	$("#status_<?php     echo $akID;?>").msDropDown();
	} catch(e) {
	alert(e.message);
	}
});
</script>