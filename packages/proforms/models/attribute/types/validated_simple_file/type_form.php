<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
?>
<div class="clearfix">
	<label><?php    echo t("Min File Size")?></label>
	<div class="input">
		<?php   echo $fm->text('size_limit',$size_limit, array('class'=>'input-mini'))?> <i><?php   echo t('(in killobytes - 3mb = 3000kb)')?></i>
	</div>
</div>
<div class="clearfix">
	<label><?php    echo t("Allowed File Types")?></label>
	<div class="input">
		<?php   echo $fm->textarea('allowed_file_types',$allowed_file_types, array('class'=>'input-mini'))?><br/>
		<i><?php   echo t('(each type on new line)')?></i>
		<i><p><?php echo t('See this online article for javascript type names: <a href="http://webdesign.about.com/od/multimedia/a/mime-types-by-file-extension.htm" target="_blank">http://webdesign.about.com/od/multimedia/a/mime-types-by-file-extension.htm</a>')?></p></i>
	</div>
</div>
<div class="clearfix">
	<label><?php    echo t("Add To Fileset")?></label>
	<div class="input">
		<select name="fasID">
		    <option value="0">--Choose Fileset--</option>
		    <?php   foreach ($fileSets as $fs) : ?>
		    <option value='<?php   echo $fs->fsID ?>' <?php   echo $asID == $fs->fsID ? 'selected' : '' ?> >
			<?php   echo htmlspecialchars($fs->fsName, ENT_QUOTES, 'UTF-8') ?>
		    </option>
		    <?php   endforeach ?>
		</select>
	</div>
</div>