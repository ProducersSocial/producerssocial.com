<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');

if($type=='optionset_begin'){
echo $fm->checkbox('optional_expand_'.$akID, 1).' '.t('Show Optional Questions');
?>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('#optional_expand_<?php   echo $akID?>').click(function(){
		if($(this).is(':checked')){
			$('#additional_options_<?php   echo $akID?>').slideDown('fast',function(){
					var slide_height = $('#additional_options_<?php   echo $akID?>').height() + ( $('.proform_slider').height() );
					$('.proform_slider').css('height',slide_height);
				}
			);
		}else{
			$('#additional_options_<?php   echo $akID?>').slideUp('fast',function(){
					var slide_height = $('.proform_slider').height() - ( $('#additional_options_<?php   echo $akID?>').height() );
					$('.proform_slider').css('height',slide_height);
				}
			);
		}
	});
});
/*]]>*/
</script>
<div id="additional_options_<?php   echo $akID?>" style="display: none;">
<?php   
}else{
?>
</div>
<?php   
}
?>