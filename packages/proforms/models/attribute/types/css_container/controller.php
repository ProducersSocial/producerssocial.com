<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class CssContainerAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $type;
	public $css_class;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
	}
	
	public function closer(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		$options = array('css_container_begin'=>t('Container Open'),'css_container_end'=>t('Container Close'));
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Container State').'</label>';
		print '		<div class="input">';
		print $fm->select('type',$options,$this->type);
		print $fm->hidden('force_close',1);
		print '		</div>';
		print '	</div>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('CSS Class Name').'</label>';
		print '		<div class="input">';
		print $fm->text('css_class',$this->css_class);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atCssContainerSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->type = $row['type'];
		$this->css_class = $row['css_class'];

		$this->set('akID', $this->akID);
		$this->set('type', $this->type);
		$this->set('css_class', $this->css_class);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Css Container Question Type is designed to allow greater flexibility for forms containing more advanced styling such as two column forms.</p><p>The question will "wrap" a div around any group of questions. When added, this attribute will add <u>two</u> Questions.  An opening and a closing question.  Simply drag the two around any set of questions to wrap them.</p> 
		<br /> 
		<h4>Advanced Use</h4>
		<p>This Question Type uses the "$reviewStatus=0;" and will never be printed out for Form review.  It is merely a front-end esthetic container.</p>
		');
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atCssContainer WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atCssContainer where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atCssContainer (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$value));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atCssContainer where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'type'=>$data['type'],
			'css_class'=>$data['css_class']
		);
		
		$db=Loader::db();
		$db->Replace('atCssContainerSettings', $vals, 'akID', true);
		if($data['force_close'] == 1 && substr($this->attributeKey->getAttributeKeyHandle(),-16) != '_close_container'){
			$sets = $this->attributeKey->getAttributeSets();
			foreach($sets as $set){
				$asIDs[] = $set->getAttributeSetID();
			}
			$at = AttributeType::getByHandle('css_container');

			$closer = ProformsItemAttributeKey::getByHandle($this->attributeKey->getAttributeKeyHandle().'_close_container'); 
			if(!is_object($closer) || !intval($closer->getAttributeKeyID())){
				$data['akHandle'] = $this->attributeKey->getAttributeKeyHandle().'_close_container';
				$data['akName'] = $this->attributeKey->getAttributeKeyName().t(' (close container)');
				$data['type'] = 'css_container_end';
				$data['css_class'] = $this->css_class;
				$data['force_close'] =  0;
				$closer = ProformsItemAttributeKey::add($at,$data);
				foreach($sets as $set){
					$closer->setAttributeSet($set); 
				}
			}
		}
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atCssContainerSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}