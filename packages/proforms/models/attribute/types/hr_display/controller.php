<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/types/default/controller');

class HrDisplayAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	public $hideLabel = 1; // no lable needed

	public function form() {
		print '<hr/>';
	}
	
	public function composer() {
		print '<hr/>';
	}
	
}