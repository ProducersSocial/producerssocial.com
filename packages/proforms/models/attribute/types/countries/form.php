<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
?>

							<div class="input date_country dateform" data-set="<?php   echo $i?>">
								  	<div class=""  data-id="<?php   echo $question['mpqID']?>">
										<input name="akID[<?php   echo $question['akID']?>][<?php   echo $i?>][<?php   echo $question['mpqID']?>][]" class="country_date country" value="<?php   echo $value?>" placeholder="<?php   echo t('Country')?>"/>
										<div class="state_wipe"><input name="blank_state" class="state_date state" type="hidden" value=""/></div>
								  	</div>
							</div>
							<script type="text/javascript">
								/*<![CDATA[*/
								$(document).ready(function(){
									var dc = $('.country_date').val();
									
									if(!dc){
										dc = 'United States';
									}
									$(".date_country").geoSelector({ 
											data: "<?php   echo $json_path?>" ,
											countrySelector: '.country_date',
											stateSelector: '.state_date',
											defaultCountry: dc,
											defaultState: 'Ohio'
									});
									$('.state_wipe').remove();
								});
								/*]]>*/
							</script>
