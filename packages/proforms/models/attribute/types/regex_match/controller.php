<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class RegexMatchAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $pattern;
	public $restrictDuplicates = true;
	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		print $fm->text('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$this->getValue());
		print '&nbsp;<i>('.t('Format must be ').$this->format_example.')</i>';
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<p style="color: red;margin-left: 160px;margin-top: 22px;margin-bottom: 22px;">'.t('When searching the internet for expression string examples, be sure to search php preg_match() strings, not regex.').'</p>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">preg_match() '.t('Pattern').'</label>';
		print '		<div class="input">';
		print $fm->text('pattern',$this->pattern);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">preg_match() '.t('Example').'</label>';
		print '		<div class="input">';
		print $fm->text('format_example',$this->format_example);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atRegexMatchSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->pattern = $row['pattern'];
		$this->format_example = $row['format_example'];

		$this->set('akID', $this->akID);
		$this->set('pattern', $this->pattern);
		$this->set('format_example', $this->format_example);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atRegexMatch WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	

	public function validateForm($value) {
		if(is_array($value)){
			$value = $value['value'];
		}
		$this->load();
		$pattern = $this->pattern;
		if(!@preg_match("$pattern",$value)) {
		  // regex value match is invalid
		  $this->error = t('The input provided is not in the correct format for \''.$this->attributeKey->getAttributeKeyName().'\'!'.$value);
		  return false;
		}
		return true;
	}
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atRegexMatch where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atRegexMatch (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$value['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atRegexMatch where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'pattern'=>$data['pattern'],
			'format_example'=>$data['format_example']
		);
		
		$db=Loader::db();
		$db->Replace('atRegexMatchSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atRegexMatchSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}