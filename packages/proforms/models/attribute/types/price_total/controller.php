<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PriceTotalAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		
		$total = $this->getValue();
		if(!$total){$total = 0.00;}

		$total = money_format('%(#4n',$total*1);
		
		if($this->show_payments){
		    echo '<hr/>';
    		$this->displayForm();
		}else{
    		echo '<div id="form_total">'.$this->getCurrencyType().'<span class="value">'.$total.'</span>'.$fm->hidden($this->field('value'),$total,array('class'=>'form_total_value')).'</div>';
		}
		
		$at = $this->attributeType;
		$path = $at->getAttributeTypeFileURL('js/form.js');
		$this->addFooterItem(Loader::helper('html')->javascript($path));
	}
	
	public function getCurrencyType(){
		$types = array(
			'usd'=>'$',
			'cad'=> '$ CAD',
			'aud' => '$ AUD',
			'GBP'=> '£',
		);
		return $types[$this->currency];
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		$types = array(
			'usd'=>'$ US',
			'cad'=> '$ CAD',
			'aud' => '$ AUD',
			'GBP'=> '£',
		);
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Currency').'</label>';
		print '		<div class="input">';
		print $fm->select('currency',$types,$this->currency);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Display Payments').'</label>';
		print '		<div class="input">';
		print $fm->checkbox('show_payments',1,$this->show_payments);
		print ' <i>('.t('Yes, display any payments or credits attributed to a given entryID').')</i>';
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPriceTotalSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->currency = $row['currency'];
		$this->show_payments = $row['show_payments'];

		$this->set('akID', $this->akID);
		$this->set('currency', $this->currency);
		$this->set('show_payments',$this->show_payments);
	}
	
	public function getCurrency(){
		$this->load();
		return $this->currency;
	}
	
	public function getDisplayValue(){
		$this->load();
		$db = Loader::db();
		$sum_paid = $this->getPayments();
		$total = $this->getOriginalTotal();
		$balance = $total - $sum_paid;
		
		return $balance;
	}
	
	public function getDisplayReviewValue(){
		$this->load();
		
		$total = $this->getOriginalTotal();
		$sum_paid = $this->getPayments();
		
		$balance = $total - $sum_paid;
		$paid_balance = $sum_paid;

		$val = $this->getCurrencyType();
		$_SESSION['currency_symbol'] = $val;
		if($balance > 1){
			$val .= $balance;
			$val .= ' <b><I style="color: red;">(unpaid)</I></b>';
		}else{
			$val .= $paid_balance;
			$val .= ' <b><I style="color: green;">(paid)</I></b>';
		}
		return $val;
	}
	
	public function display(){
		$this->load();
		$db = Loader::db();
		
		$sum_paid = $this->getPayments();
		
		$total = $db->getOne("SELECT total FROM atPriceTotal WHERE avID = ?",array($this->getAttributeValueID()));
		$balance = $total - $sum_paid;
		
		print '<b>'.$this->getCurrencyType().$this->getOriginalTotal().'</b><br />';
		
		$this->getPaymentHistory();
		
		print '<hr />';
		print '<b>';
		print t('Balance: ');
		
		$val = $this->getCurrencyType();
		$val .= money_format('%(#4n',$balance);
		if($balance > 1){$val .= ' <b><I style="color: red;">(unpaid)</I></b>';}
		print $val;
		print '</b>';
	}
	
	public function displayForm(){
		$this->load();
		$db = Loader::db();
		$fm = Loader::helper('form');
		
		$sum_paid = $this->getPayments();
		
		$total = $this->getOriginalTotal();
		
		$balance = $total - $sum_paid;
		
		$this->getPaymentHistory();
		
		print '<hr />';

        print '<b>';
		print t('Balance: ');
		
		$val = $this->getCurrencyType();
		$val .= '<div id="paid_total"><span class="value">'.money_format('%(#4n',$balance).'</span></div>';
		if($balance > 1){$val .= ' <b><I style="color: red;">(unpaid)</I></b>';}
		print $val;
		print '</b>';
	}
	
	public function getValue(){
		$this->load();
		
		$sum_paid = $this->getPayments();
		
		$total = $this->getOriginalTotal();
		
		//$balance = $this->getCurrencyType();
		$balance .= $total - $sum_paid;
		return $balance;
	}
	
	public function getRawValue($avID=null){
		if(!$avID){$avID = $this->getAttributeValueID();}
		
		$this->load();
		
		$sum_paid = $this->getPayments($avID);

		$total = $this->getOriginalTotal($avID);

		if($total > $sum_paid && $total > 0){
			return $total - $sum_paid;
		}else{
			return 0;
		}
	}
	
	public function getRawImpendingValue($avID=null){
		if(!$avID){$avID = $this->getAttributeValueID();}
		
		$this->load();
		
		$sum_paid = $this->getPayments($avID);

		$total = $this->getOriginalTotal($avID);

		return $total - $sum_paid;
	}
	
	public function getOriginalTotal($avID=null){
		if(!$avID){$avID = $this->getAttributeValueID();}
		$db = Loader::db();
		return $db->getOne("SELECT total FROM atPriceTotal WHERE avID = ?",array($avID));
	}
	
	public function getRawCreditHistory($avID=null){
		
		if(!$avID){
			$avID = $this->getAttributeValueID();
		}

		$db = Loader::db();
		$sum_paid = 0;
		$credits = $db->getAll("SELECT amount FROM atPriceTotalCredits WHERE avID = ?",array($avID));

		return $credits;
	}
	
	public function getRawPaymentHistory($avID=null){
		
		if(!$avID){
			$avID = $this->getAttributeValueID();
		}

		$db = Loader::db();
		$sum_paid = 0;
		$payments = $db->getAll("SELECT amount FROM atPriceTotalPayments WHERE avID = ?",array($avID));

		return $payments;
	}
	
	public function getPaymentHistory($avID=null){
		
		if(!$avID){
			$avID = $this->getAttributeValueID();
		}

		$db = Loader::db();
		$sum_paid = 0;
		$payments = $db->getAll("SELECT amount,credit FROM atPriceTotalPayments WHERE avID = ?",array($avID));

		if(count($payments)>0){
			foreach($payments as $amount){
				if(!$amount['credit']){
					print '<I>('.t('Payment').') '.$this->getCurrencyType().'<span class="payment_amount">'.money_format('%(#4n',$amount['amount']).'</span></i><br />';
				}elseif($amount['credit']==1){
					print '<I style="color: #33CCCC;">('.t('Credit').') -'.$this->getCurrencyType().'<span class="payment_amount">'.money_format('%(#4n',$amount['amount']).'</span></i><br />';
				}else{
					print '<I style="color: green;">('.t('Refund').') -'.$this->getCurrencyType().'<span class="payment_amount">'.money_format('%(#4n',$amount['amount']).'</span></i><br />';
				}
			}
		}

		return $sum_paid;
	}
	
	public function getPayments($avID=null){
		if(!$avID){
			$avID = $this->getAttributeValueID();
		}
		$db = Loader::db();
		$sum_paid = 0;
		$payments = $db->getAll("SELECT amount,credit FROM atPriceTotalPayments WHERE avID = ?",array($avID));
		if(count($payments)>0){
			foreach($payments as $amount){
				if(!$amount['credit']){
					
					$sum_paid += money_format('%(#4n',$amount['amount']);
				}else{
					$sum_paid -= money_format('%(#4n',$amount['amount']);
				}
			}
		}

		return $sum_paid;
	}
	
	
	public function getPaid(){
		$db = Loader::db();
		$val = $db->getOne("SELECT paid FROM atPriceTotal WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atPriceTotal where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atPriceTotal (avID,total,paid) values (?,?,?)', array($this->getAttributeValueID(),$value['value'],0));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atPriceTotal where avID = ?', array($this->getAttributeValueID()));
	}
	
	public function savePayment($avID=null,$amount,$credit=null,$transID='internal'){
		$this->load();
		$db = Loader::db();
		if($this->getAttributeValueID()){
			$avID = $this->getAttributeValueID();
		}
		$db->Execute("INSERT INTO atPriceTotalPayments (avID,amount,payment_date,credit,transactionID) VALUES(?,?,?,?,?)",array($avID,$amount,date('Y-m-d'),$credit,$transID));
		
		$this->updatePaidAmount($avID);
	}
	
	public function removePayment($id){
		$db = Loader::db();
		$avID = $db->getOne("SELECT avID FROM atPriceTotalPayments WHERE ptpID = ?",array($id));
		$db->Execute("DELETE FROM atPriceTotalPayments WHERE ptpID = ?",array($id));
		$this->updatePaidAmount($avID);
	}
	
	
	public function updatePaidAmount($avID){
	    $db = Loader::db();
    	$paid = $this->getPaymentHistory($avID);
        $db->execute("UPDATE atPriceTotal SET paid = ? WHERE avID = ?",array($paid,$avID));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'currency'=>$data['currency'],
			'show_payments'=>$data['payments']?$data['show_payments']:0
		);
		
		$db=Loader::db();
		$db->Replace('atPriceTotalSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceTotalSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}