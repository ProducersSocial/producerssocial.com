<?php    
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('proforms_item','proforms');
$fm = Loader::helper('form');
if($_REQUEST['entryID']){
	$ProformsItemID = str_replace(' ','+',$_REQUEST['entryID']);
	$salt = preg_replace('/[^A-Za-z0-9\-]/', '1', Config::get('SECURITY_TOKEN_ENCRYPTION') );
	$ProformsItemID = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($ProformsItemID), MCRYPT_MODE_CBC, md5(md5($salt))), "\0");
}else{
	$proformsItemID = $_REQUEST['payment_form'];
}
$payment = ProformsItem::GetByID($proformsItemID);
$total = $payment->getAttribute($wepay_price_handle);

Loader::model('attribute/set');
$as = AttributeSet::getByID($payment->getAttributeSetID());
$title = $as->getAttributeSetName();
/** 
 * Initialize the WePay SDK object 
 */

Loader::library('gateways/wepay','proforms');

Wepay::useStaging($client_id, $client_secret);
$wepay = new WePay($access_token);

/**
 * Make the API request to get the checkout_uri
 * 
 */

try {
	//$app_fee = $total * 0.045;
	$checkout = $wepay->request('/checkout/create', array(
			'account_id' => $account_id, // ID of the account that you want the money to go to
			'amount' => $total, // dollar amount you want to charge the user
			'short_description' => $title, // a short description of what the payment is for
			'reference_id' => $proformsItemID.'.'.$akID,
			'type' => "Donation", // the type of the payment - choose from GOODS SERVICE DONATION or PERSONAL
			'mode' => "iframe", // put iframe here if you want the checkout to be in an iframe, regular if you want the user to be sent to WePay
			'fee_payer' => 'payee',
			//'app_fee' => $app_fee,
			'callback_uri' => BASE_URL.DIR_REL.'/tools/packages/proforms/proforms/payment/wepay_ipn.php'
			)
	);
} catch (WePayException $e) { // if the API call returns an error, get the error message for display later
	$error = $e->getMessage();
}
//var_dump($error);
?>
<style type="text/css">
.ui-dialog{max-width:625px!important;}
</style>
		<h1>Make A Donation!</h1>
		<p><?php echo $title?></p>
		<?php  if (isset($error)): ?>
			<h2 style="color:red">ERROR: <?php  echo $error ?></h2>
		<?php  else: ?>
			<div id="checkout_div"></div>
			<script type="text/javascript" src="https://stage.wepay.com/js/iframe.wepay.js"></script>
			<script type="text/javascript">
			WePay.iframe_checkout("checkout_div", "<?php  echo $checkout->checkout_uri ?>");
			$('.proforms_submit').hide();
			$(document).ready(function(){
				$('.proforms_submit_group').remove();
				$('.ajax_payment label').remove();
			});
			</script>
		<?php  endif; ?>