<?php     
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('proforms_item','proforms');
$fm = Loader::helper('form');
$bt = BlockType::getByHandle('proforms_display');
if($_REQUEST['payment_form']){
	$ProformsItemID =  $_REQUEST['payment_form'];
}else{
	$ProformsItemID = str_replace(' ','+',$_REQUEST['entryID']);
	$salt = preg_replace('/[^A-Za-z0-9\-]/', '1', Config::get('SECURITY_TOKEN_ENCRYPTION') );
	$ProformsItemID = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($ProformsItemID), MCRYPT_MODE_CBC, md5(md5($salt))), "\0");
}

//$IPN = BASE_URL.DIR_REL.'/tools/packages/proforms/proforms/payment/ipn.php';
$IPN = BASE_URL.DIR_REL.'/ipn';

if($paypal_sandbox==1){
	$paypal = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
}else{
	$paypal = 'https://www.paypal.com/cgi-bin/webscr';
}
echo '<div class="payment_form">';
if($ProformsItemID){
	$pfo = ProFormsItem::getByID($ProformsItemID);
	if($pfo->asID > 0){
		$question_set = AttributeSet::getByID($pfo->asID);
		$setAttribs = $question_set->getAttributeKeys();
		echo '<h3>'.t('Payment Confirmation: ').$question_set->getAttributeSetName().'</h3><br/>';
		foreach ($setAttribs as $ak) {
			$value = $pfo->getAttributeValueObject($ak);
			Loader::packageElement('attribute/attribute_form_review','proforms',array('ak'=>$ak,'bt'=>$bt,'aValue'=>$value,'review'=>1));
			
			if($ak->getAttributeKeyHandle() == $paypal_price_handle){
				$ctl = $ak->getController();
				$curency = $ctl->getCurrency();
				$amount = $ak->getController()->getRawValue($value->getAttributeValueID());
			}

			if($ak->getAttributeType()->getAttributeTypeHandle() == 'price_event'){
				ob_start();
				$ak->render('display_review',$value);
				$event_title = ob_get_contents();
				ob_end_clean();
			}
		}
		if($event_title){
			$payment_title = strip_tags($event_title);
		}else{
			$payment_title = $question_set->getAttributeSetName();
		}
		
		$u = new User();
		if($u->isLoggedIn()) {
			echo $fm->hidden('custom', $u->uID);
			//$ui = UserInfo::getByID($u->uID);
			//echo $fm->hidden('custom', $ui->getUserEmail());
		}
		
		echo $fm->hidden($this->field('value'),$ProformsItemID);
		echo $fm->hidden('cmd','_xclick');
		echo $fm->hidden('currency_code',strtoupper($curency));
		echo $fm->hidden('amount',$amount);
		echo $fm->hidden('business',$paypal_id);
		echo $fm->hidden('item_name',$payment_title);
		echo $fm->hidden('notify_url',$IPN.'?entryID='.$ProformsItemID);
		echo $fm->hidden('entryID',$ProformsItemID);
		echo $fm->hidden('return',BASE_URL.Loader::helper('navigation')->getLinkToCollection(Page::getByID($paypal_return_page)));
	}
}
echo '</div>';
?>
<script type="text/javascript">
$(document).ready(function(){
	$('form.proform_slider').attr('action','<?php   echo $paypal?>');
});
</script>
<style type="text/css">
.payment_form label{text-align: right;width: 100px;float: left;clear: left;line-height: 24px;font-weight: bold;}
.payment_form .input{line-height: 24px;float:left;margin-left: 22px;}
.payment_form hr{width: 400px;}
</style>