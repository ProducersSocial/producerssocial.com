<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class ProformsItemAttributeKey extends AttributeKey {
	
	protected $searchIndexFieldDefinition = 'ProformsItemID I(11) UNSIGNED NOTNULL DEFAULT 0 PRIMARY';
	protected $multipart;
	protected $reviewStatus = 1;
	
	public function get($akID){
		return ProformsItemAttributeKey::getByID($akID);
	}
	
	public function getAttributeKeyID(){
		return parent::getAttributeKeyID();
	}
	
	public function getIndexedSearchTable() {
		return 'ProformsItemSearchIndexAttributes';
	}
		
	public function getSearchIndexFieldDefinition() {
		return $this->searchIndexFieldDefinition;
	}
	
	public function setTypeMultipart(){
		$this->multipart = 1;
	}
	
	public function getTypeMultipart(){
		return $this->multipart;
	}
	
	public function setReviewStatusFalse(){
		$this->reviewStatus = 0;
	}
	
	public function getReviewStatus(){
		return $this->reviewStatus;
	}
	
	public function setHideLabel()
	{
    	$this->hide_label = 1;
	}
	
	public function isAttributeKeyLabelHidden(){
		return $this->hide_label;
	}
	

	public function getAttributes($ObjectID, $method = 'getValue') {
		$dbs = Loader::db();
		$values = $dbs->GetAll("select akID, avID from ProformsItemAttributeValues where ProformsItemID = ?", array($ObjectID));
		//$avl = new AttributeValueList();
		$atts_array = array();
		foreach($values as $val) {
			$ak = ProformsItemAttributeKey::getByID($val['akID']);
			if (is_object($ak)) {
				$cnt = $ak->getController();
				if($cnt->is_multipart){
					$ak->setTypeMultipart();
				}

				if($cnt->reviewStatus<1 && !is_null($cnt->reviewStatus)){
					$ak->setReviewStatusFalse();
				}
				
				if($cnt->hideLabel > 0){
					$ak->setHideLabel();
				}
				
				$value = $ak->getAttributeValue($val['avID'], $method);
				//$avl->addAttributeValue($ak, $value);
				$atts_array[$ak->getAttributeKeyHandle()] = $value;
			}
		}
		return $atts_array;
	}
	
	public function isAttributeKeyMonetary(){
		return $this->is_monetary;
	}
	
	public function isAttributeKeyHeader(){
		return $this->is_header;
	}
	
	public function isAttributeKeyRequired(){
		return $this->orakIsRequired;
	}
	
	public function isAttributeKeyNoDuplicates(){
		return $this->no_duplicates;
	}
	
	public function isAttributeKeyConditioned(){
		return $this->is_conditioned;
	}

	public function getAttributeKeyTooltip(){
		return $this->tooltip;
	}
	
	public function getAttributeConditionHandle(){return $this->condition_handle;}
	public function getAttributeConditionOpperator(){return $this->condition_operator;}
	public function getAttributeConditionValue(){return $this->condition_value;}
	public function getAttributeConditionKeyID(){
		$tak = ProformsItemAttributeKey::getByHandle($this->condition_handle);
		return $tak->getAttributeKeyID();
	}
	
	public static function getColumnHeaderList() {
		return parent::getList('proforms_item', array('akIsColumnHeader' => 1));	
	}
	
	public static function getColumnQuestionHeaderList($asID){
		$headers = array();
		$as = AttributeSet::getByID($asID);
		$atts = $as->getAttributeKeys();
		foreach($atts as $ak){
			if($ak->isAttributeKeyHeader()){
				$headers[] = $ak;
			}
		}
		
		if(count($headers)<1){
			$headers = ProformsItemAttributeKey::getSortedColumnHeaderList();
		}
		
		return $headers;
	}
	
	
	public static function getSortedColumnHeaderList() {
		$akCategoryHandle = 'proforms_item';
		$filters = array('akIsColumnHeader' => 1);	
		$db = Loader::db();
		$pkgHandle = $db->GetOne('select pkgHandle from AttributeKeyCategories inner join Packages on Packages.pkgID = AttributeKeyCategories.pkgID where akCategoryHandle = ?', array($akCategoryHandle));
		$q = 'select AttributeKeys.akID,  AttributeSetKeys.displayOrder from AttributeKeys 
			inner join AttributeSetKeys on AttributeSetKeys.akID  = AttributeKeys.akID
			inner join AttributeKeyCategories on AttributeKeys.akCategoryID = AttributeKeyCategories.akCategoryID 
			where AttributeKeyCategories.akCategoryHandle = ?
			';
		foreach($filters as $key => $value) {
			$q .= ' and ' . $key . ' = ' . $value . ' ';
		}
		$q .= 'order by AttributeSetKeys.displayOrder asc ';
		$r = $db->Execute($q, array($akCategoryHandle));
		$list = array();
		$txt = Loader::helper('text');
		if ($pkgHandle) { 
			Loader::model('attribute/categories/' . $akCategoryHandle, $pkgHandle);
		} else {
			Loader::model('attribute/categories/' . $akCategoryHandle);
		}
		$className = $txt->camelcase($akCategoryHandle);
		while ($row = $r->FetchRow()) {
			$c1 = $className . 'AttributeKey';
			$c1a = call_user_func(array($c1, 'getByID'), $row['akID']);
			if (is_object($c1a) && !in_array($c1a,$list)) {
				$list[] = $c1a;
			}
		}
		$r->Close();
		return $list;
	}
	
	public function getAttributeKeys() {
		$akc = AttributeKeyCategory::getByHandle('proforms_item');
		$akcID = $akc->getAttributeKeyCategoryID();
		$db = Loader::db();
		$r = $db->Execute('select AttributeKeys.akID from AttributeKeys left join AttributeSetKeys on AttributeKeys.akID = AttributeSetKeys.akID where akIsInternal = 0 and akCategoryID = ?', $akcID);
		$keys = array();
		$cat = AttributeKeyCategory::getByID($akcID);
		while ($row = $r->FetchRow()) {
			$keys[] = $cat->getAttributeKeyByID($row['akID']);
		}
		return $keys;		
	}	
	
	public function getSearchableIndexedList() {
		return parent::getList('proforms_item', array('akIsSearchableIndexed' => 1));	
	}
	
	public static function getSearchableList() {
		return parent::getList('proforms_item', array('akIsSearchable' => 1));	
	}
	
	public function getAttributeValue($avID, $method = 'getValue') {
		$av = ProformsItemAttributeValue::getByID($avID);
		$av->setAttributeKey($this);
		return call_user_func_array(array($av, $method), array());
	}
	
	/** 
	 * Loads the required attribute fields for this instantiated attribute
	 */
	protected function load($akIdentifier, $loadBy = 'akID') {
		$db = Loader::db();
		$akunhandle = Loader::helper('text')->uncamelcase(get_class($this));
		$akCategoryHandle = substr($akunhandle, 0, strpos($akunhandle, '_attribute_key'));
		if ($akCategoryHandle != '') {
			$row = $db->GetRow('select akID, akHandle, akName, AttributeKeys.akCategoryID, akIsInternal, akIsEditable, akIsSearchable, akIsSearchableIndexed, akIsAutoCreated, akIsColumnHeader, AttributeKeys.atID, atHandle, AttributeKeys.pkgID from AttributeKeys inner join AttributeKeyCategories on AttributeKeys.akCategoryID = AttributeKeyCategories.akCategoryID inner join AttributeTypes on AttributeKeys.atID = AttributeTypes.atID where ' . $loadBy . ' = ? and akCategoryHandle = ?', array($akIdentifier, $akCategoryHandle));
		} else {
			$row = $db->GetRow('select akID, akHandle, akName, akCategoryID, akIsEditable, akIsInternal, akIsSearchable, akIsSearchableIndexed, akIsAutoCreated, akIsColumnHeader, AttributeKeys.atID, atHandle, AttributeKeys.pkgID from AttributeKeys inner join AttributeTypes on AttributeKeys.atID = AttributeTypes.atID where ' . $loadBy . ' = ?', array($akIdentifier));		
		}
		if(count($row)>0){
			$this->setPropertiesFromArray($row);
			
			$additional_vars = $db->GetRow('SELECT * FROM ProformsItemAttributeKeys WHERE akID = ?', array($akIdentifier));
			$this->setPropertiesFromArray($additional_vars);
		}
	}

	/** 
	 * Renders a view for this attribute key. If no view is default we display it's "view"
	 * Valid views are "view", "form" or a custom view (if the attribute has one in its directory)
	 * Additionally, an attribute does not have to have its own interface. If it doesn't, then whatever
	 * is printed in the corresponding $view function in the attribute's controller is printed out.
	 */
	public function render($view = 'view', $value = false, $return = false) {
		$at = $this->getAttributeType();
		$resp = $at->render($view, $this, $value, $return);
		if ($return) {
			return $resp;
		}
	}
	
	public static function getByID($akID){
		$ak = new ProformsItemAttributeKey();
		$ak->load($akID);
		if (is_object($ak)) {
			$at = $ak->getAttributeType();
			$cnt = $at->getController();
			$cnt->setAttributeKey($this);
			if($cnt->is_multipart){
				$ak->setTypeMultipart();
			}
			if($cnt->reviewStatus<1 && !is_null($cnt->reviewStatus) ){
				$ak->setReviewStatusFalse();
			}
			if($cnt->hideLabel > 0){
				$ak->setHideLabel();
			}
			return $ak;	
		}
	
	}
	
	public static function getByHandle($akHandle) {
		$dbs = Loader::db();
		$akID = $dbs->GetOne('select akID from AttributeKeys inner join AttributeKeyCategories on AttributeKeys.akCategoryID = AttributeKeyCategories.akCategoryID where akHandle = ? and akCategoryHandle = \'proforms_item\'', array($akHandle));
		$ak = new ProformsItemAttributeKey();
		$ak->load($akID);
		if ($ak->getAttributeKeyID() > 0) {
			return $ak;	
		}
	}
	
	public static function getList($filters = array()) {
		$akCategoryHandle = 'proforms_item';
		$db = Loader::db();
		$pkgHandle = $db->GetOne('select pkgHandle from AttributeKeyCategories inner join Packages on Packages.pkgID = AttributeKeyCategories.pkgID where akCategoryHandle = ?', array($akCategoryHandle));
		$q = 'select AttributeSetKeys.akID from AttributeKeys inner join AttributeKeyCategories on AttributeKeys.akCategoryID = AttributeKeyCategories.akCategoryID inner join AttributeSetKeys on AttributeKeys.akID = AttributeSetKeys.akID where akCategoryHandle = ? ORDER BY AttributeSetKeys.asID,AttributeSetKeys.displayOrder';
		foreach($filters as $key => $value) {
			$q .= ' and ' . $key . ' = ' . $value . ' ';
		}
		$r = $db->Execute($q, array($akCategoryHandle));
		$list = array();
		$txt = Loader::helper('text');
		if ($pkgHandle) { 
			Loader::model('attribute/categories/' . $akCategoryHandle, $pkgHandle);
		} else {
			Loader::model('attribute/categories/' . $akCategoryHandle);
		}
		$className = $txt->camelcase($akCategoryHandle);
		while ($row = $r->FetchRow()) {
			$c1 = $className . 'AttributeKey';
			$c1a = call_user_func(array($c1, 'getByID'), $row['akID']);
			if (is_object($c1a)) {
				$list[] = $c1a;
			}
		}
		$r->Close();
		return $list;
	}
	

	public function saveAttribute($Object, $value = false) {
		$av = $Object->getAttributeValueObject($this, true);
		parent::saveAttribute($av, $value);
		$dbs = Loader::db();
		$v = array($Object->getProformsItemID(), $this->getAttributeKeyID(), $av->getAttributeValueID());
		$dbs->Replace('ProformsItemAttributeValues', array(
			'ProformsItemID' => $Object->getProformsItemID(), 
			'akID' => $this->getAttributeKeyID(), 
			'avID' => $av->getAttributeValueID()
		), array('ProformsItemID', 'akID'));
		
		unset($av);
	}
	
	public function add($type=null,$args, $pkg = false) {
		
		extract($args);
		
		if(!is_object($type)){
			$type = AttributeType::getByID($args['type']);
		}

		//we have to remove sets momentarily as C5.6.2 cannot handle multi-sets
		$sargs = $args;
		$sargs['asID'] = $args['asID'][0];
		$ak = parent::add('proforms_item', $type, $sargs, $pkg);
		
		if($is_header != 1){
			$is_header = 0;
		}
		if ($orakIsRequired != 1) {
			$orakIsRequired = 0;
		}
		if ($hide_label != 1) {
			$hide_label = 0;
		}
		if($no_duplicates != 1){
			$no_duplicates = 0;
		}
		
		if($is_monetary != 1){
			$is_monetary = 0;
		}

		if($is_conditioned != 1){
			$is_conditioned = 0;
		}
		
		$v = array($ak->getAttributeKeyID(),$is_header,$orakIsRequired,$hide_label,$is_monetary,$no_duplicates,$is_conditioned,$condition_handle,$condition_operator,$condition_value,$tooltip);
		$dbs = Loader::db();
		$dbs->Execute('REPLACE INTO ProformsItemAttributeKeys (akID,is_header,orakIsRequired,hide_label,is_monetary,no_duplicates,is_conditioned,condition_handle,condition_operator,condition_value,tooltip) VALUES (?,?,?,?,?,?,?,?,?,?,?)', $v);

		if($asID && is_array($asID)){
			foreach($asID as $as_id){
				if($as_id>0){
					$as = AttributeSet::getByID($as_id);
					$as->addKey($ak);
				}
			}
		}
		
		if($questionSetID > 0 && $order){
			ProformsItemAttributeKey::updateProformsItemSpecificOrder($ak->getAttributeKeyID(),$questionSetID,$order);
		}
		
		$nak = new ProformsItemAttributeKey();
		$nak->load($ak->getAttributeKeyID());
		return $ak;
	}
	
	public function update($args) {

		//we have to remove sets momentarily as C5.6.2 cannot handle multi-sets
		$sargs = $args;
		$sargs['asID'] = $args['asID'][0];
		$ak = parent::update($sargs);

		extract($args);
		if($is_header != 1){
			$is_header = 0;
		}
		if ($orakIsRequired != 1) {
			$orakIsRequired = 0;
		}
		if ($hide_label != 1) {
			$hide_label = 0;
		}
		if($no_duplicates != 1){
			$no_duplicates = 0;
		}
		if($is_monetary != 1){
			$is_monetary = 0;
		}
		if($is_conditioned != 1){
			$is_conditioned = 0;
		}

		$v = array($ak->getAttributeKeyID(),$is_header,$orakIsRequired,$hide_label,$is_monetary,$no_duplicates,$is_conditioned,$condition_handle,$condition_operator,$condition_value,$tooltip);
		$dbs = Loader::db();
		$dbs->Execute('REPLACE INTO ProformsItemAttributeKeys (akID,is_header,orakIsRequired,hide_label,is_monetary,no_duplicates,is_conditioned,condition_handle,condition_operator,condition_value,tooltip) VALUES (?,?,?,?,?,?,?,?,?,?,?)', $v);
		if($asID){
			foreach($asID as $as_id){
				if($as_id>0){
					$as = AttributeSet::getByID($as_id);
					$as->addKey($this);
					$s .= ' AND asID <> '.$as_id;
				}
			}
		}

		//parent::clearAttributeSets();
		$db = Loader::db();
		$db->Execute('delete from AttributeSetKeys where akID = ?'.$s, $this->getAttributeKeyID());
	}

	public function delete() {
		parent::delete();
		$dbs = Loader::db();
		$r = $dbs->Execute('select avID from ProformsItemAttributeValues where akID = ?', array($this->getAttributeKeyID()));
		while ($row = $r->FetchRow()) {
			$dbs->Execute('delete from AttributeValues where avID = ?', array($row['avID']));
		}
		$dbs->Execute('delete from ProformsItemAttributeValues where akID = ?', array($this->getAttributeKeyID()));
	}
	
	public function clearAttribute($ak){
		//var_dump($ak);exit;
	}
	

	
	public function updateProformsItemSpecificOrder($akID,$questionSetID,$order) {
		$order_set = 0;
		$haspassed = 0;
		$as = AttributeSet::getByID($questionSetID);
		$pats = $as->getAttributeKeys();
		$db = Loader::db();
		for ($i = 0; $i < count($pats); $i++) {		
			if($i==$order){
				$order_set = 1;
				$db->query("update AttributeSetKeys set displayOrder = {$i} where akID = ? AND asID = ?", array($akID,$questionSetID));
				$key = $pats[$i];
				if($haspassed){
					//if the order number HAS passed alread AND
					//akID has passed alread, 
					//we are bumping the current one down
					$t = $i-1;
				}else{
					//if the order number HAS passed alread AND
					//if the akID has NOT passed alread, 
					//we are bumping the current one up
					$t = $i+1;
				}
				$db->query("update AttributeSetKeys set displayOrder = {$t} where akID = ? AND asID = ?", array($key->getAttributeKeyID(),$questionSetID));
			}else{

				$t = $i;
				$p = $i;

				if($order_set > 0){
					//if the ordered number HAS passed alread, 
					//we are bumping the current one up
					$t = $i+1;
				}
				$key = $pats[$p];
				if($key->getAttributeKeyID() != $akID){
					$db->query("update AttributeSetKeys set displayOrder = {$t} where akID = ? AND asID = ?", array($key->getAttributeKeyID(),$questionSetID));	
				}else{
					$haspassed = 1;
				}
			}
		}

	}
	
}

class ProformsItemAttributeValue extends AttributeValue {

	public function setProformsItem($Object) {
		$this->ProformsItem = $Object;
	}
	
	public static function getByID($avID) {
		$cav = new ProformsItemAttributeValue();
		$cav->load($avID);
		if ($cav->getAttributeValueID() == $avID) {
			return $cav;
		}
	}
	
	public function getDisplayValue($mode = 'display') {
		if ($mode != false) {
			$th = Loader::helper('text');
			$method = 'get' . $th->camelcase($mode) . 'Value';
			if (method_exists($this->attributeType->controller, $method)) {
				return $this->attributeType->controller->{$method}();
			}elseif (method_exists($this->attributeType->controller, $mode)) {
				return $this->attributeType->controller->{$mode}();
			}
		}		
		return $this->attributeType->controller->getValue();		
	}

	public function delete() {
		$dbs = Loader::db();
		$dbs->Execute('delete from ProformsItemAttributeValues where ProformsItemID = ? and akID = ? and avID = ?', array(
			$this->ProformsItem->getProformsItemID(), 
			$this->attributeKey->getAttributeKeyID(),
			$this->getAttributeValueID()
		));

		// Before we run delete() on the parent object, we make sure that attribute value isn't being referenced in the table anywhere else
		$num = $dbs->GetOne('select count(avID) from ProformsItemAttributeValues where avID = ?', array($this->getAttributeValueID()));
		if ($num < 1) {
			parent::delete();
		}
	}
}