

	var innitializeForm = function(FORM_ID,data){
		//we must str strip string inputs from number question types.
		//lame the core doesn't already do this.
		$('.number .input input').each(function() {
			$(this).bind('keyup',function(){
				if(!$.isNumeric($(this).val())){
					var str = $(this).val();
					$(this).val(str.substring(0, str.length - 1));
				}
			});
		});
		
		$('.proforms_submit_' + FORM_ID ).click(function(){
			if(typeof tinyMCE != 'undefined'){
				tinyMCE.triggerSave();
			}
			$('.input_error').removeClass('input_error');
			var form = $('#proforms_form_' + FORM_ID ).serialize();
			$('.ajax_error_' + FORM_ID ).hide();
			$('.ajax_loader_' + FORM_ID ).css('display','inline-block');
			$('#proforms_form_' + FORM_ID + ' input[type!=checkbox]').addClass('formGrayOut').attr('disabled','disabled');
			$('#proforms_form_' + FORM_ID + ' input[type!=checkbox]').removeAttr('disabled');		

			var urlr = data.toolsAjax;
			
			$.ajax({
				url: urlr,
				type: "POST",
				dataType: 'json',
				data: form,
				success: function(responder){
					$('.ajax_loader_' + FORM_ID ).hide();
					if(responder.success > 0){
						//we validate twice. via ajax & again on submit
						$('#proforms_form_' + FORM_ID ).submit();
					}else{
						$('.ajax_response_' + FORM_ID ).remove();
						var message = '';
						$('.ajax_loader_' + FORM_ID ).hide();
						$('#proforms_form_' + FORM_ID + ' input[type!=checkbox]').removeClass('formGrayOut').removeAttr('disabled');
						$.each(responder.error,function(key,r){
							message += '<li class="ajax_response_' + FORM_ID + '">'+r+'</li>';
						});
						$('.ajax_error_' + FORM_ID + ' ul').append(message).parent().parent().show();
						
						$.each(responder.fields,function(key,r){

							$.ajax({
								url: data.captcha,
								success: function(recaptcha){
									$('#captcha_input').replaceWith(recaptcha);
								}
							});

							if($('#'+r).hasClass('ccm-input-select')){
								$('#'+r).parent().addClass('input_error');
							}else if($('#'+r).hasClass('ccm-input-checkbox')){
								if(!$('#'+r).parent().hasClass('checkbox_wrapper')){
									$('#'+r).wrap('<div class="checkbox_wrapper" />');
								}
								$('#'+r).parent().addClass('input_error');
							}else{
								// beginning
								if($('#'+r).size() > 0) {
									$('#'+r).addClass('input_error');
								}else{
									$('#'+r.replace(/[\\]/g,'').replace(/[^0-9A-Za-z-]/g,'_')).addClass('input_error');
								}
								//end
							}
							var position = $('.ajax_error_' + FORM_ID).position();
							scroll(0,position.top);
						});
						return false;
					}
				},
				error: function(){

				}
			});
		
		});
		
		$('.ajax_error .alert').bind('close', function (e) {
		  e.preventDefault();
		  $('.ajax_error').hide();
		});
	}

$(document).ready(function(){
	$(".dialog-launch").dialog();
	$("div.form_tooltip").removeClass('form_tooltip');
	$(".form_tooltip").tooltip({
 
      // place tooltip on the right edge
      position: "center right",
 
      // a little tweaking of the position
      offset: [-2, 10],
 
      // use the built-in fadeIn/fadeOut effect
      effect: "fade",
 
      // custom opacity setting
      opacity: 0.7
 
      });
});