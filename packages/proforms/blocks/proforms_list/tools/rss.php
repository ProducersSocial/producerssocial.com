<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

//Permissions Check
//if($_GET['bID']) {
	$c = Page::getByID($_GET['cID']);
	$a = Area::get($c, $_GET['arHandle']);
		
	//edit survey mode
	$b = Block::getByID($_GET['bID'],$c, $a);
	
	$controller = new ProformsListBlockController($b);
	$rssUrl = $controller->getRssUrl($b);
	
	
	if($controller->headers){
		$atts = explode(',',$controller->headers);
		foreach($atts as $akID){
			$slist[] = ProformsItemAttributeKey::getByID($akID);
		}
	}else{
		$slist = ProformsItemAttributeKey::getColumnQuestionHeaderList($controller->asID);
	}
	//$bp = new Permissions($b);
	//if( $bp->canRead() && $controller->rss) {

		$cArray = $controller->getEntries();
		$nh = Loader::helper('navigation');

		header('Content-type: text/xml');
		echo "<" . "?" . "xml version=\"1.0\"?>\n";

?>
		<rss version="2.0">
		  <channel>
			<title><?php    echo $controller->rssTitle?></title>
			<link><?php    echo BASE_URL.DIR_REL.htmlspecialchars($rssUrl)?></link>
			<description><?php    echo $controller->rssDescription?></description> 
<?php    
		for ($i = 0; $i < count($cArray); $i++ ) {
			$cobj = $cArray[$i]; 
			$title = t('Form Entry ID#').$cobj->getProformsItemID(); 
			
			foreach($slist as $ak){
				$vo = $cobj->getAttributeValueObject($ak);
				if (is_object($vo)) {
					$collective_description .= $ak->getAttributeKeyName().': '. $vo->getValue('display')."\r\n";
				}
			}
			?>
			<item>
			  <title><?php    echo htmlspecialchars($title);?></title>
			  <description><?php    echo htmlspecialchars(strip_tags($collective_description))."....";?></description>
			</item>
<?php    } ?>
     		 </channel>
		</rss>
		
<?php    	//} else {  	
		//$v = View::getInstance();
		//$v->renderError('Permission Denied',"You don't have permission to access this RSS feed");
		//exit;
	//}
			
//} else {
//	echo "You don't have permission to access this RSS feed";
//}
exit;






