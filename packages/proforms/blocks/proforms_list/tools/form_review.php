<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('proforms_item','proforms');
Loader::model('attribute/categories/proforms_item', 'proforms');

$update_url = urldecode($_REQUEST['update_url']);
$ProformsItemID = $_REQUEST['ProformsItemID'];
$bID = $_REQUEST['bID'];
$b = Block::getByID($bID);
$bt = $b->getBlockTypeObject();
$cnt = $b->getController();

$pfo = ProFormsItem::getByID($_REQUEST['ProformsItemID']);

$atIDs = explode(',',$cnt->review);
?>
<style type="text/css">
.entry_review label {min-width: 80px;float:left;font-weight: bold;}
.entry_review .required{float: left;}
.entry_review .input {margin-left: 22px; float:left;}
</style>
<form class="ccm-ui entry_review proform_slider" name="proforms_form_<?php   echo $bID?>" id="proforms_form_<?php   echo $bID?>" method="post" action="<?php   echo $update_url?>" enctype="multipart/form-data">
<input type="hidden" name="ProformsItemID" id="ProformsItemID" value="<?php   echo $ProformsItemID?>"/>
<?php   
foreach($atIDs as $akID){
	$ak = ProformsItemAttributeKey::getByID($akID);
	if($ak->akID > 0){
		if($pfo){$value = $pfo->getAttributeValueObject($ak);}
		echo '<div class="clearfix text">';
		if($ak->getAttributeType()->getAttributeTypeHandle() == 'comments'){
			Loader::packageElement('attribute/attribute_form','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value,'review'=>0));
		}else{
			Loader::packageElement('attribute/attribute_form_review','proforms',array('ak'=>$ak,'bt'=>$bt,'aValue'=>$value,'review'=>1));
		}
		echo $ak->render('closer');
		echo '</div>';
	}
}
?>
<?php    if($review > 0){ ?>
<input type="submit" name="submit" value="<?php   echo t('Update Entry')?>" class="btn primary"/>
<?php    } ?>
</form>

<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){
		$('.dialog-launch').dialog();
	});
	/*]]>*/
</script>