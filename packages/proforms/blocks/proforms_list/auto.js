var proformsList = {
	servicesDir: $("input[name=formsListToolsDir]").val(),
	init: function(){
		this.blockForm=document.forms['ccm-block-form'];
		
		this.rss=this.blockForm.rss;
		for(var i=0;i<this.rss.length;i++){
			this.rss[i].onclick  = function(){ proformsList.rssInfoShown(); }
			this.rss[i].onchange = function(){ proformsList.rssInfoShown(); }			
		}

		this.rssInfoShown();
		this.tabSetup();
	},
	tabSetup: function(){
		$('ul#ccm-blockEditPane-tabs li a').each( function(num,el){
			el.onclick=function(){
				var pane=this.id.replace('ccm-blockEditPane-tab-','');
				proformsList.showPane(pane);
			}
		});		
	}, 
	showPane:function(pane){
		$('ul#ccm-blockEditPane-tabs li').each(function(num,el){ $(el).removeClass('ccm-nav-active') });
		$(document.getElementById('ccm-blockEditPane-tab-'+pane).parentNode).addClass('ccm-nav-active');
		$('div.ccm-blockEditPane').each(function(num,el){ el.style.display='none'; });
		$('#ccm-blockEditPane-'+pane).css('display','block');
		//if(pane=='preview') this.loadPreview();
	}, 
	rssInfoShown:function(){	
		for(var i=0;i<this.rss.length;i++){
			if( this.rss[i].checked && this.rss[i].value=='1' ){
				$('#ccm-pagelist-rssDetails').css('display','block');
				return; 
			}				
		}
		$('#ccm-pagelist-rssDetails').css('display','none');
	},
	validate:function(){
			var failed=0;
			
			var rssOn=$('#ccm-proformsList-rssSelectorOn');
			var rssTitle=$('#ccm-proformsList-rssTitle');
			if( rssOn && rssOn.attr('checked') && rssTitle && rssTitle.val().length==0 ){
				alert(ccm_t('feed-name'));
				rssTitle.focus();
				failed=1;
			}
			
			if(failed){
				ccm_isBlockError=1;
				return false;
			}
			return true;
	}	
}

$(function(){ proformsList.init(); });

$(function() {
	$('#radiantweb-search-add-option').click(
		function () {
			$('#ccm-search-field-base').show();
		}
	);
	$('#searchField').change(
		function () {
			var val = $(this).val();
			//$('#radiantweb-search-field-set'+val).show();
			//$('#radiantweb-search-field-set'+val+' .radiantweb-selected-field').val(val);
			$(this).val('');
		
			var copy = $('#radiantweb-search-field-set'+val).clone();
			
			$('#ccm-search-saved-fields-wrapper').append(copy);
			
			$('#ccm-search-saved-fields-wrapper .ccm-search-field').show();
			
			$('#ccm-search-field-base').hide();
			
			$('.ccm-search-remove-option').click(
				function () {
					$(this).closest('.ccm-search-field').remove();
				}
			);
		}
	);
	$('.ccm-search-remove-option').click(
		function () {
			$(this).closest('.ccm-search-field').remove();
		}
	);
	
	$('form').submit(function(){
		$('#ccm-search-fields-wrapper').remove();
	});
	
	$.each($('.status select'), function(){
		var name = $(this).attr('name');
		$(this).attr('name',name + '[]');	
	});

});

//ccmValidateBlockForm = function() { return locationsList.validate(); }