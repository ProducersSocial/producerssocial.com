<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));
	class ProformsListBlockController extends BlockController {
	

		protected $btTable = 'btProformsList';
		protected $btInterfaceWidth = "580";
		protected $btInterfaceHeight = "350";
		
		public $stored_search = array();
		public $excluded_list = array();
		
		/** 
		* Used for localization. If we want to localize the name/description we have to include this
		**/
		
		public function getBlockTypeDescription() {
			return t("List Form Entries based on advanced attribute filters.");
		}
		
		public function getBlockTypeName() {
			return t("ProForms List");
		}
		
		public function getJavaScriptStrings() {
			return array(
				'feed-name' => t('Please give your RSS Feed a name.')
			);
		}
		
		public function __construct($obj = null) {		
			parent::__construct($obj);
			//Loader::model('contact_directory_contact','contact_directory');
			//$this->baseobj = new ContactDirectoryContact();
			$html = Loader::helper('html');
			$form = Loader::helper('form');
			$uh = Loader::helper('concrete/urls');
			Loader::model('attribute/categories/proforms_item', 'proforms');
			$this->set('form', $form);

			if (isset($this->stored_search) && !is_array($this->stored_search)) {
				$args = @unserialize($this->stored_search);		
				if (!is_array($args)) $args = array();
				$this->stored_search = $args;
				$this->set('stored_search',$this->stored_search);
			}
			
			if (isset($this->excluded_list) && !is_array($this->excluded_list)) {
				$args = @unserialize($this->excluded_list);		
				if (!is_array($args)) $args = array();
				$this->excluded_list = $args;
				$this->set('excluded_list',$this->excluded_list);
			}
			
			
		}
		
		public function on_page_view(){
		
			$html = Loader::helper('html');
			$this->addHeaderItem($html->css('ccm.app.css'));
			$this->addHeaderItem($html->css('jquery.ui.css'));
			$this->addHeaderItem($html->javascript('jquery.js'));
			$this->addFooterItem($html->javascript('jquery.ui.js'));
			$this->addHeaderItem($html->javascript('bootstrap.js'));
			$this->addFooterItem($html->javascript('ccm.app.js'));
			$this->addHeaderItem($html->javascript('jquery.dd.min.js','proforms'));
			$this->addHeaderItem($html->javascript('tiny_mce/tiny_mce.js'));
			$this->addHeaderItem($html->css('dd.css','proforms'));
			$at = AttributeType::getByHandle('multiplex');
			$jspath = $at->getAttributeTypeFileURL('multiplex.js');
			$path = $at->getAttributeTypeFileURL('js/jQuery.geoselector.js');
			$json_path = $at->getAttributeTypeFileURL('js/divisions.json');
			
			$h = Loader::helper('lists/states_provinces');
			$provinceJS = "<script type=\"text/javascript\"> var ccm_attributeTypeAddressStatesTextList = '\\\n";
			$all = $h->getAll();
			foreach($all as $country => $countries) {
				foreach($countries as $value => $text) {
					$provinceJS .= str_replace("'","\'", $country . ':' . $value . ':' . $text . "|\\\n");
				}
			}
			$provinceJS .= "'</script>";
			$this->addHeaderItem($provinceJS);
			$this->addHeaderItem($html->javascript('country_state.js','proforms'));
			
			$this->addFooterItem($html->javascript($path));
		}
		
		function getEntries($query = null) {
			Loader::model('proforms_item_list','proforms');
			$db = Loader::db();
			$bID = $this->bID;
			if ($this->bID) {
				$q = "select * from btProformsList where bID = ?";
				$r = $db->query($q,array($bID));
				if ($r) {
					$row = $r->fetchRow();
				}
			}
			
			$pl = new ProformsItemlist();
			$pl->setNameSpace('b' . $this->bID);
			
			$cArray = array();

			switch($row['orderBy']) {
				case 'display_asc':
					$pl->sortByDisplayOrder();
					break;
				case 'display_desc':
					$pl->sortByDisplayOrderDescending();
					break;
				case 'chrono_asc':
					$pl->sortByPublicDate();
					break;
				case 'alpha_asc':
					$pl->sortByName();
					break;
				case 'alpha_desc':
					$pl->sortByNameDescending();
					break;
				default:
					$pl->sortByPublicDateDescending();
					break;
			}

			$num = (int) $row['num'];
			
			if ($num > 0) {
				$pl->setItemsPerPage($num);
			}
			
			$pl->filterByReviewed($this->filter_reviewed);
			
			if($this->filter_user){
				$u = new User();
				if($u->isLoggedIn()){
					$pl->filterByAssociatedUser($u->uID);
				}else{
					$pl->filterByAssociatedUser(0);
				}
			}
			
			$excluded = array();
			if($this->excluded_list){
				$excluded = $this->excluded_list;
			}

			if ($this->asID) {
				$pl->filterByAttributeSet($this->asID);
			}

			//uncomment to reset sessions vars
			//$_SESSION['search_keys'] = null;
			
			if($_SESSION['search_keys'] || $this->stored_search){
				$keys = ($_SESSION['search_keys']) ? $_SESSION['search_keys'] : $this->stored_search;
				$pl->filterByAttributeKeys($keys,$excluded);
			}
			
			if($_SESSION['keywords'] || $this->keywords){
				$keywords = ($_SESSION['keywords']) ? $_SESSION['keywords'] : $this->keywords;
				$pl->filterBySearchKeywords($keywords);
			}
	
			if($_REQUEST['ccm_order_by_b'.$this->bID]){
				$pl->sortBy($_REQUEST['ccm_order_by_b'.$this->bID],$_REQUEST['ccm_order_dir_b'.$this->bID]);
			}
			
			//$pl->debug();
			
			if ($num > 0) {
				$entries = $pl->getPage();
			} else {
				$entries = $pl->get();
			}
			$this->set('pl', $pl);
			return $entries;
		}
		
		public function view() {

			//var_dump($_REQUEST['akID']);
			//$_SESSION['search_keys'] = $_REQUEST['akID'];
			if($this->request('remove_entry') == 'success'){
				$this->set('message',t('Your Entry has been removed!'));
			}elseif($this->request('updated_entry') == 'success'){
				$this->set('message',t('Your Entry has been updated!'));
			}
			
			$entries = $this->getEntries();
			
			$nh = Loader::helper('navigation');
			$this->set('nh', $nh);
			$this->set('uh',Loader::helper('concrete/urls'));
			//$this->set('cArray', $cArray);
			$this->set('entries', $entries);
			
			if($this->headers){
				$atts = explode(',',$this->headers);
				foreach($atts as $akID){
					$key = ProformsItemAttributeKey::getByID($akID);
					if(is_object($key)){
						$slist[] = $key;
					}
				}
			}else{
				$slist = ProformsItemAttributeKey::getColumnQuestionHeaderList($this->asID);
			}
			$this->set('slist',$slist);
	
		}
		
		
		public function add() {

			$c = Page::getCurrentPage();
			$uh = Loader::helper('concrete/urls');
			//	echo $rssUrl;
			$this->set('c', $c);
			$this->set('uh', $uh);
			$this->set('bt', BlockType::getByHandle('proforms_list'));
			$this->set('displayAliases', true);
		}
	
		public function edit() {
			$b = $this->getBlockObject();
			$bCID = $b->getBlockCollectionID();
			$bID=$b->getBlockID();
			$this->set('bID', $bID);
			$c = Page::getCurrentPage();
			$uh = Loader::helper('concrete/urls');
			$this->set('uh', $uh);
			$this->set('bt', BlockType::getByHandle('proforms_list'));
		}
		
		public function action_update_entry(){
			global $c;
			
			$this->validate_post();
			if (!$this->error_post->has()) {
				$nh = Loader::helper('navigation');
				
				Loader::model('proforms_item', 'proforms');
				
				if($this->request('ProformsItemID')){
					$Object = ProformsItem::getByID($this->request('proformsItemID'));
				
					Loader::model('attribute/set');
					Loader::model('attribute/categories/proforms_item', 'proforms');
	
					foreach ($_REQUEST['akID'] as $akID=>$value) {
						$ak = ProformsItemAttributeKey::getByID($akID);
						if($ak->getAttributeType()->getAttributeTypeHandle() != 'comments'){
							$ak->saveAttributeForm($Object);
							$i++;
						}
					}
					if($i){
						$Object->reindex();
					}
					
					$this->externalRedirect($nh->getLinkToCollection($c).'?update_entry=success');
				}
			}else{
				$this->set('error',$this->error_post->getList());
				$this->view();
			}
		}
		
		public function validate_post() {
			$this->error_post = Loader::helper('validation/error');
			$jn = Loader::helper('json');
			$vt = Loader::helper('validation/strings');
			$vn = Loader::Helper('validation/numbers');
			$dt = Loader::helper("form/date_time");
			//$er = Loader::helper('validation/error');
			Loader::model('attribute/categories/proforms_item','proforms');
			

			foreach ($_REQUEST['akID'] as $akID=>$value) {

				$ak = ProformsItemAttributeKey::getByID($akID);	
				$cnt = $ak->getController();
				$type = $ak->getAttributeType();
				if($ak->isAttributeKeyRequired()){
					if(
						!$_REQUEST['akID'][$ak->getAttributeKeyID()]['value'] && 
						!$_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID'] && 
						!$_REQUEST['akID'][$ak->getAttributeKeyID()][1]['value_st_dt'] &&
						!$ak->getTypeMultipart() && 
						$type->getAttributeTypeHandle() != 'address'
					){
						$this->error_post->add("'".$ak->getAttributeKeyName()."'".t(' is required!'));
					}
				}
				
				if(method_exists($cnt,'validateForm')){
					if($_REQUEST['akID'][$ak->getAttributeKeyID()]['value']){
						$cnt->validateForm(array("value"=>$_REQUEST['akID'][$ak->getAttributeKeyID()]['value']));
					}elseif($_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID']){
						$cnt->validateForm($_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID']);
					}
	
					if($cnt->error != ''){
						$this->error_post->add($cnt->error);
					}
				}
				
			}
	
		}
		
		public function action_remove_entry(){
			global $c;
			Loader::model('proforms_item', 'proforms');
			$nh = Loader::helper('navigation');
			if($this->request('ProformsItemID')){
				$Object = ProformsItem::getByID($this->request('ProformsItemID'));
				$Object->delete();
			}
			$this->externalRedirect($nh->getLinkToCollection($c).'?remove_entry=success');
		}
		
		
		function save($args) {
			$db = Loader::db();
			$bID = $this->bID;

			if (!is_array($args['selectedSearchField'])) $args['selectedSearchField'] = array();

			if (is_array($args['akID'])) {
				foreach ($args['akID'] as $key => $vals) {

					if (!in_array($key,$args['selectedSearchField'])) { 
						unset($args['akID'][$key]);
					}else{
						$settings[$key] = $vals;
					}
				}
				foreach($args['selectedSearchField'] as $field){
					if($field != ''){
						$settings['selectedSearchField'][] = $field;
					}
				}
			}
			
			//var_dump($args['headerOptionsOrdering']);exit;
			
			$jh = Loader::helper('json');

			$args['stored_search'] = serialize($settings);
			$args['excluded_list'] = serialize($args['excluded_list']);
			$args['num'] = ($args['num'] > 0) ? $args['num'] : 0;
			$args['truncateSummaries'] = ($args['truncateSummaries']) ? '1' : '0';
			$args['allow_edit'] = ($args['allow_edit']) ? '1' : '0';
			$args['allow_delete'] = ($args['allow_delete']) ? '1' : '0';
			$args['displayFeaturedOnly'] = ($args['displayFeaturedOnly']) ? '1' : '0';
			$args['truncateChars'] = intval($args['truncateChars']); 
			$args['paginate'] = intval($args['paginate']); 
			$args['asID'] = isset($args['asID']) ? $args['asID'] : '';
			$args['headers'] = ($args['headerOptions']) ? implode(',',$args['headerOptions']) : '';
			$args['headers_ordering'] = $jh->encode($args['headerOptionsOrdering']);
			$args['review'] = ($args['reviewOptions']) ? implode(',',$args['reviewOptions']) : '';
			$args['review_ordering'] = $jh->encode($args['reviewOptionsOrdering']);
			$args['edits'] = ($args['editsOptions']) ? implode(',',$args['editsOptions']) : '';
			$args['edits_ordering'] = $jh->encode($args['editsOptionsOrdering']);
			$args['title'] = isset($args['title']) ? $args['title'] : '';

			parent::save($args);
		}
		
		function getBlockPath($path){
			$db = Loader::db();
			$r = $db->Execute("SELECT cPath FROM PagePaths WHERE cID = ? ",array($path));
			while ($row = $r->FetchRow()) {
				$pIDp = $row['cPath'];
				return $pIDp ;
			}
		}
		

		public function getRssUrl($b){
			$uh = Loader::helper('concrete/urls');
			if(!$b) return '';
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$c = $b->getBlockCollectionObject();
			$a = $b->getBlockAreaObject();
			$rssUrl = $uh->getBlockTypeToolsURL($bt)."/rss?bID=".$b->getBlockID()."&cID=".$c->getCollectionID()."&arHandle=" . $a->getAreaHandle();
			return $rssUrl;
		}
	}

?>