<?php      
defined('C5_EXECUTE') or die(_("Access Denied.")); 
$jh = Loader::helper('json');
$form = Loader::helper('form');
//search options
$selected_options = explode(',',$controller->headers);

$headers_ordering = ($controller->headers_ordering && $controller->headers_ordering != 'null') ? $jh->decode($controller->headers_ordering) : array();


Loader::model("attribute/set");		
Loader::model('attribute/categories/proforms_item','proforms');
$searchFieldAttributes = ProformsItemAttributeKey::getList();

$atts_list = array();
?>
<style type="text/css">
#proforms_header tr:hover{cursor: move;}
</style>
<div id="ccm-search-fields-wrapper">
	<table id="proforms_header" class="table table-striped">
		<tbody>
		<?php       
			if(is_array($headers_ordering)){
				foreach ($headers_ordering as $akID) { 
					$ak = ProformsItemAttributeKey::getByID($akID);
					if($ak->akID > 0){
						$atts_list[] = $ak->getAttributeKeyHandle();
						if(in_array($ak->getAttributeKeyID(),$selected_options)){
							$selected = 1;
						}else{
							$selected = 0;
						}
						echo '<tr><td>';
						echo $form->checkbox('headerOptions[]',$ak->getAttributeKeyID(),$selected);
						echo $form->hidden('headerOptionsOrdering[]',$ak->getAttributeKeyID());
						echo ' '.$ak->getAttributeKeyName();
						echo '<i class="icon icon-move" style="float:right;"></i>';
						echo '</td></tr>';
					}
				}
			}
			foreach ($searchFieldAttributes as $ak) { 
				if(is_object($ak) && $ak->getReviewStatus() > 0 && !in_array($ak->getAttributeKeyHandle(),$atts_list) && !in_array($ak->getAttributeKeyID(),$headers_ordering)){
					$atts_list[] = $ak->getAttributeKeyHandle();
					if(in_array($ak->getAttributeKeyID(),$selected_options)){
						$selected = 1;
					}else{
						$selected = 0;
					}
					echo '<tr><td>';
					echo $form->checkbox('headerOptions[]',$ak->getAttributeKeyID(),$selected);
					echo $form->hidden('headerOptionsOrdering[]',$ak->getAttributeKeyID());
					echo ' '.$ak->getAttributeKeyName();
					echo '<i class="icon icon-move" style="float:right;"></i>';
					echo '</td></tr>';
				}
			} 
		?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){
		$('#proforms_header tbody').sortable();
	});
	/*]]>*/
</script>