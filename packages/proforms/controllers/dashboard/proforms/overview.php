<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('attribute/categories/proforms_item', 'proforms');
Loader::model('proforms_item_list', 'proforms');

class DashboardProformsOverviewController extends Controller {
	
	public $helpers = array('html','form');
	
	public function on_start() {
		$this->error = Loader::helper('validation/error');
		$this->addHeaderItem(Loader::helper('html')->javascript('jquery.dd.min.js','proforms'));
		$this->addHeaderItem(Loader::helper('html')->css('dd.css','proforms'));
		$this->addHeaderItem(Loader::helper('html')->javascript('jquery.js'));
		
		$html = Loader::helper('html');
		$uh = Loader::helper('concrete/urls');
		$pkg = Package::getByHandle('proforms');
		$this->addFooterItem($html->css('signature/jquery.signaturepad.css','proforms'));
		$this->addFooterItem($html->javascript('bootstrap.js'));
		$this->addFooterItem($html->javascript('json2.min.js','proforms'));
		$this->addFooterItem($html->javascript('jquery.signaturepad.js','proforms'));
		$this->addFooterItem('<!--[if lt IE 7]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addFooterItem('<!--[if lt IE 8]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addFooterItem('<!--[if lt IE 9]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addHeaderItem('<script type="text/javascript">$(function() { ccm_setupAdvancedSearch(\'proforms-item\'); });</script>');
		$this->addHeaderItem('<script type="text/javascript" src="'.$uh->getPackageURL($pkg).'/js/proforms_item.ui.js"></script>');
	}
	
	public function view(){
	
	}
}