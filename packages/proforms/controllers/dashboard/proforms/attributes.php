<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('attribute/categories/proforms_item', 'proforms');

class DashboardProformsAttributesController extends Controller {
	
	public $helpers = array('form');
	
	public function __construct() {
		parent::__construct();
		$otypes = AttributeType::getList('proforms_item');
		$types = array();
		foreach($otypes as $at) {
			$types[$at->getAttributeTypeID()] = $at->getAttributeTypeName();
		}
		$this->set('types', $types);
	}
	
	public function delete($akID, $token = null){
		try {
			$ak = ProformsItemAttributeKey::getByID($akID); 
				
			if(!($ak instanceof ProformsItemAttributeKey)) {
				throw new Exception(t('Invalid attribute ID.'));
			}
	
			$valt = Loader::helper('validation/token');
			if (!$valt->validate('delete_attribute', $token)) {
				throw new Exception($valt->getErrorMessage());
			}
			
			$ak->delete();
			
			if($this->post('ajax')){
				print 'success';
				exit;
			}else{
				$this->redirect("/dashboard/proforms/attributes", 'attribute_deleted');
			}
		} catch (Exception $e) {
			$this->set('error', $e);
		}
	}
	
	public function duplicate($akID, $token = null){
		try {
			$ak = ProformsItemAttributeKey::getByID($akID); 
				
			if(!($ak instanceof ProformsItemAttributeKey)) {
				throw new Exception(t('Invalid attribute ID.'));
			}
	
			$valt = Loader::helper('validation/token');
			if (!$valt->validate('copy_attribute', $token)) {
				throw new Exception($valt->getErrorMessage());
			}
			
			$handle = $ak->getAttributeKeyHandle().'_2';
			$name = $ak->getAttributeKeyName().' 2';
			$ak->duplicate(array('akHandle'=>$handle,'akName'=>$name));
			
			$this->redirect("/dashboard/proforms/attributes", 'attribute_duplicated');
		} catch (Exception $e) {
			$this->set('error', $e);
		}
	}
	
	public function on_start() {
		$this->set('category', AttributeKeyCategory::getByHandle('proforms_item'));
		
        $jh = Loader::helper('json');
		$html = Loader::helper('html');
		$this->addHeaderItem($html->javascript('jquery.dd.min.js','proforms'));
		$this->addHeaderItem($html->css('dd.css','proforms'));
		$at = AttributeType::getByHandle('multiplex');
		$jspath = $at->getAttributeTypeFileURL('multiplex.js');
		$path = $at->getAttributeTypeFileURL('js/jQuery.geoselector.js');
		$json_path = $at->getAttributeTypeFileURL('js/divisions.json');
		$this->addFooterItem($html->javascript($path));
		$this->addHeaderItem($html->css('ccm.app.css'));
		$this->addHeaderItem($html->css('jquery.ui.css'));
		$this->addHeaderItem($html->javascript('jquery.js'));
		$this->addHeaderItem($html->javascript('jquery.ui.js'));
		$this->addHeaderItem($html->javascript('jquery.form.js'));
		$this->addHeaderItem($html->javascript('ccm.app.js'));
		$this->addFooterItem($html->javascript('bootstrap.js'));		
		$this->addFooterItem($html->javascript('jquery.signaturepad.js','proforms'));	
		$this->addHeaderItem($html->javascript($jspath));
		$h = Loader::helper('lists/states_provinces');
		$provinceJS = "<script type=\"text/javascript\"> var ccm_attributeTypeAddressStatesTextList = '\\\n";
		$all = $h->getAll();
		foreach($all as $country => $countries) {
			foreach($countries as $value => $text) {
				$provinceJS .= str_replace("'","\'", $country . ':' . $value . ':' . $text . "|\\\n");
			}
		}
		$provinceJS .= "'</script>";
		$this->addHeaderItem($provinceJS);
		$this->addHeaderItem($html->javascript('country_state.js','proforms'));
	}
	
	public function select_type() {
		$atID = $this->request('atID');
		$at = AttributeType::getByID($atID);
		$this->set('type', $at);
	}
	
	public function view($set=null) {
		Loader::model('attribute/categories/proforms_item', 'proforms');
		$attribs = ProformsItemAttributeKey::getList();
		$this->set('attribs', $attribs);
		if($set){
			$this->set('set',$set);
		}
	}
	
	public function add() {
	    
	    $jh = Loader::helper('json');
	    
		$this->select_type();
		$type = $this->get('type');
		$cnt = $type->getController();

		if(!$_POST['akHandle']){
			$name = $this->post('akName');
			$name = str_replace(' ','_',$name);
			$name = str_replace(',','_',$name);
			$name = str_replace('-','_',$name);
			$name = str_replace('/','_',$name);
			$name = str_replace('(','',$name);
			$name = str_replace(')','',$name);
			$name = strtolower($name);
			$_POST['akHandle'] = $name;
		}
		
		$e = $cnt->validateKey($this->post());
		if ($e->has()) {
			if($this->post('questionSetID')){
				print $jh->encode($e->getList());
				exit;
			}else{
				$this->set('error', $e);
			}
		} else {
			$ak = ProformsItemAttributeKey::add($type,$this->post());
			$set = $_POST['asID'][0];
			if($this->post('questionSetID')){
				print $jh->encode(array('success'=>true));
				exit;
			}else{
				$this->redirect('/dashboard/proforms/attributes/', 'attribute_created',$set);
			}
		}
	}

	public function attribute_deleted() {
		$this->set('message', t('Question has been Deleted.'));
	}
	
	public function attribute_duplicated() {
		$this->set('message', t('Question has been Duplicated.'));
	}
	
	public function attribute_created($set=null) {
		$this->set('message', t('Question has been Created.'));
		$this->view($set);
	}

	public function attribute_updated($set=null) {
		$this->set('message', t('Question has been Updated.'));
		$this->view($set);
	}
	
	public function edit($akID = 0) {
	
	    $jh = Loader::helper('json');
	    
		if ($this->post('akID')) {
			$akID = $this->post('akID');
		}
		if($akID){
			$key = ProformsItemAttributeKey::getByID($akID);
			$type = $key->getAttributeType();
			$this->set('key', $key);
			$this->set('type', $type);
			
			if ($this->isPost()) {
				$cnt = $type->getController();
				$cnt->setAttributeKey($key);
	
				if(!$_POST['akHandle']){
					$name = $this->post('akName');
					$name = str_replace(' ','_',$name);
					$name = str_replace(',','_',$name);
					$name = str_replace('-','_',$name);
					$name = str_replace('/','_',$name);
					$name = str_replace('(','',$name);
					$name = str_replace(')','',$name);
					$name = strtolower($name);
					$_POST['akHandle'] = $name;
				}
				
				$e = $cnt->validateKey($this->post());
				if ($e->has()) {
					if($this->post('questionSetID')){
						print $jh->encode($e->getList());
						exit;
					}else{
						$this->set('error', $e);
					}
				} else {
					$key->update($this->post());
					$set = $_POST['asID'][0];
					if($this->post('questionSetID')){
						print $jh->encode(array('success'=>true));
						exit;
					}else{
						$this->redirect('/dashboard/proforms/attributes/', 'attribute_updated',$set);
					}
				}
			}
		}
	}
	
}