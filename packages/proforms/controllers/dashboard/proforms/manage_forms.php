<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardProformsManageFormsController extends Controller {

	public $category;
	public $token;
	public $error;
	
	public function on_start(){
		$this->token = Loader::helper('validation/token');
		$this->error = Loader::helper('validation/error');
	}
	
	public function view() {
		$this->set('categories', AttributeKeyCategory::getList());
		$akc = AttributeKeyCategory::getByHandle('proforms_item');
		$akcID = $akc->getAttributeKeyCategoryID();
		$this->redirect('/dashboard/proforms/manage_forms/category/'.$akcID.'/');
		

	}
	
	public function category($categoryID = false, $mode = false) {
		$this->addFooterItem('<script type="text/javascript">
		$("div.ccm-attribute-sortable-set-list").sortable({
			handle: \'img.ccm-group-sort\',
			cursor: \'move\',
			opacity: 0.5,
			stop: function() {
				var ualist = $(this).sortable(\'serialize\');
				ualist += \'&categoryID='.$categoryID.'\';
				$.post(\''.REL_DIR_FILES_TOOLS_REQUIRED.'/dashboard/attribute_set_order_update\', ualist, function(r) {
	
				});
			}
		});
	</script>');
		if(!intval($categoryID)) {
			$this->redirect('/dashboard/proforms/manage_forms');
		}
		$this->category = AttributeKeyCategory::getByID($categoryID);
		if (is_object($this->category)) {
			$sets = $this->category->getAttributeSets();
			$this->set('sets', $sets);
		} else {
			$this->redirect('/dashboard/proforms/manage_forms');
		}
		$this->set('categoryID', $categoryID);
		switch($mode) {
			case 'set_added':
				$this->set('message', t('Form added.'));
				break;
			case 'set_deleted':
				$this->set('message', t('Form deleted.'));
				break;
			case 'set_updated':
				$this->set('message', t('Form updated.'));
				break;
		}
		

		
	}

	public function add_set() {
		$this->category($this->post('categoryID'));
		if ($this->token->validate('add_set')) { 
			if (!trim($this->post('asHandle'))) { 
				$this->error->add(t("Specify a handle for your Form."));
			} else {
				$as = AttributeSet::getByHandle($this->post('asHandle'));
				if (is_object($as)) {
					$this->error->add(t('That handle is in use.'));
				}
			}
			if (!trim($this->post('asName'))) { 
				$this->error->add(t("Specify a name for your Form."));
			}
			if (!$this->error->has()) {
				if (!$this->category->allowAttributeSets()) {
					$this->category->setAllowAttributeSets(AttributeKeyCategory::ASET_ALLOW_SINGLE);
				}
				
				$this->category->addSet($this->post('asHandle'), $this->post('asName'), false, 0);
				
				$as = AttributeSet::getByHandle($this->post('asHandle'));
				Loader::model('proforms_question_set','proforms');
				$qs = new ProformsQuestionSet();
				$qs->SetQuestionSetIsInternal($as->getAttributeSetID(),$this->post('is_internal'));
				
				$this->redirect('dashboard/proforms/manage_forms', 'category', $this->category->getAttributeKeyCategoryID(), 'set_added');
			}
			
		} else {
			$this->error->add($this->token->getErrorMessage());
		}
	}
	
	public function update_set() {
		$this->edit($this->post('asID'));
		if ($this->token->validate('update_set')) { 
			$as = AttributeSet::getByID($this->post('asID'));
			if (!is_object($as)) {
				$this->error->add(t('Invalid Form.'));
			} else {
				if (!trim($this->post('asHandle')) && (!$as->isAttributeSetLocked())) { 
					$this->error->add(t("Specify a handle for your Form."));
				} else {
					$asx = AttributeSet::getByHandle($this->post('asHandle'));
					if (is_object($asx) && $asx->getAttributeSetID() != $as->getAttributeSetID()) {
						$this->error->add(t('That handle is in use.'));
					}
				}
				if (!trim($this->post('asName'))) { 
					$this->error->add(t("Specify a name for your Form."));
				}
			}
			
			if (!$this->error->has()) {
				if (!$as->isAttributeSetLocked()) {
					$as->updateAttributeSetHandle($this->post('asHandle'));
				}
				$as->updateAttributeSetName($this->post('asName'));
				
				Loader::model('proforms_question_set','proforms');
				$qs = new ProformsQuestionSet();
				$qs->SetQuestionSetIsInternal($this->post('asID'),$this->post('is_internal'));
				$this->redirect('dashboard/proforms/manage_forms', 'category', $as->getAttributeSetKeyCategoryID(), 'set_updated');
			}
			
		} else {
			$this->error->add($this->token->getErrorMessage());
		}
	}
	
	public function update_set_attributes() {
		$cat = AttributeKeyCategory::getByHandle('proforms_item');
		Loader::model('attribute/categories/proforms_item','proforms');
		if ($this->token->validate('update_set_attributes')) { 
			$as = AttributeSet::getByID($this->post('asID'));
			if (!is_object($as)) {
				$this->error->add(t('Invalid Form.'));
			}

			if (!$this->error->has()) {
				// go through and add all the attributes that aren't in another set
				$as->clearAttributeKeys();
				$keys = ProformsItemAttributeKey::getAttributeKeys();
					
				//$unassigned = $cat->getUnassignedAttributeKeys();			
				if (is_array($this->post('akID'))) {
					foreach($keys as $ak) { 
						if (in_array($ak->getAttributeKeyID(), $this->post('akID'))) {
							$as->addKey($ak);
						}
					}
				}
				$this->redirect('dashboard/proforms/manage_forms', 'category', $cat->getAttributeKeyCategoryID(), 'set_updated');
			}	
			
		} else {
			$this->error->add($this->token->getErrorMessage());
		}
		$this->edit($this->post('asID'));
	}
	
	public function getBlockTypeUsedList($blocktype='proforms_display',$asID=null){
		$bt = BlockType::getByHandle($blocktype);
		$btID = $bt->getBlockTypeID();
		$db = Loader::db();
		$q = "SELECT MAX(cvb.cvID),MAX(cvb.bID),cvb.cID FROM CollectionVersionBlocks cvb LEFT JOIN Blocks b ON b.bID = cvb.bID WHERE b.btID = ? GROUP BY cvb.cID ORDER BY cvb.cvID DESC";
		$bIDs = $db->getAll($q,array($btID));
		//var_dump($bIDs);
		$block_exists = false;
		if(is_array($bIDs)){
			foreach($bIDs as $bi){
				$b = Block::getByID($bi['MAX(cvb.bID)']);
				$cnt = $b->getController();
				
				if($cnt->question_set == $asID || $cnt->asID == $asID){
					$p = Page::getByID($bi['cID']);
					$pv = CollectionVersion::get($p,$bi['MAX(cvb.cvID)']);
					if($pv->isApproved()){
						$block_exists = true;
					}
				}
			}
		}
		//var_dump($block_exists);exit;
		return $block_exists;
	}
	
	public function delete_set() {
		
		$bs = $this->getBlockTypeUsedList('proforms_display',$this->post('asID'));

		if($bs){
			$this->error->add(t('There is a proforms_display block that is using this form. Please remove or change that first.'));
		}
		
		$bs = $this->getBlockTypeUsedList('proforms_list',$this->post('asID'));

		if($bs){
			$this->error->add(t('There is a proforms_list block that is using this form. Please remove or change that first.'));
		}
		
		if ($this->token->validate('delete_set')) { 
		
			$as = AttributeSet::getByID($this->post('asID'));
			
			/*get all form blocks to test if in use*/
			
			
			if (!is_object($as)) {
				$this->error->add(t('Invalid Form.'));
			} else if ($as->isAttributeSetLocked()) { 
				$this->error->add(t('This form is locked. That means it was added by a package and cannot be manually removed.'));
				$this->edit($as->getAttributeSetID());
			}
			if (!$this->error->has()) {
				$categoryID = $as->getAttributeSetKeyCategoryID();
				$as->delete();
				$this->redirect('dashboard/proforms/manage_forms', 'category', $categoryID, 'set_deleted');
			}			
		} else {
			$this->error->add($this->token->getErrorMessage());
		}
/* 		/$this->view(); */
		$this->edit($as->getAttributeSetID());
		
		if($this->error){
			$this->set('error',$this->error->getList());
		}
	}
	

	
	public function edit($asID = false) {
		$as = AttributeSet::getByID($asID);
		if (is_object($as)) {
			$this->set('set', $as);
		} else {
			$this->redirect('/dashboard/proforms/manage_forms');
		}
	}
	
}

?>