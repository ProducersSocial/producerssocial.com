<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('attribute/categories/proforms_item', 'proforms');
Loader::model('proforms_item_list', 'proforms');

class DashboardProformsSearchController extends Controller {
	
	public $helpers = array('html','form');
	
	public function on_start() {
		$this->error = Loader::helper('validation/error');
		$this->addHeaderItem(Loader::helper('html')->javascript('jquery.dd.min.js','proforms'));
		$this->addHeaderItem(Loader::helper('html')->css('dd.css','proforms'));
		$this->addHeaderItem(Loader::helper('html')->javascript('jquery.js'));
		
		$html = Loader::helper('html');
		$uh = Loader::helper('concrete/urls');
		$pkg = Package::getByHandle('proforms');
		$this->addFooterItem($html->css('signature/jquery.signaturepad.css','proforms'));
		$this->addFooterItem($html->javascript('bootstrap.js'));
		$this->addFooterItem($html->javascript('json2.min.js','proforms'));
		$this->addFooterItem($html->javascript('jquery.signaturepad.js','proforms'));
		$this->addFooterItem('<!--[if lt IE 7]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addFooterItem('<!--[if lt IE 8]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addFooterItem('<!--[if lt IE 9]><script src="'.$uh->getPackageURL($pkg).'js/flashcanvas.js"></script><![endif]-->');
		$this->addHeaderItem('<script type="text/javascript">$(function() { ccm_setupAdvancedSearch(\'proforms-item\'); });</script>');
		$this->addHeaderItem('<script type="text/javascript" src="'.$uh->getPackageURL($pkg).'/js/proforms_item.ui.js"></script>');
	}
	
	public function view(){
		
		Loader::model('attribute/categories/proforms_item', 'proforms');
		$html = Loader::helper('html');
		$form = Loader::helper('form');
		$this->set('form', $form);

		$ProformsItemList = $this->getRequestedSearchResults();
		$proforms_entry = $ProformsItemList->getPage();
			
		$this->set('ProformsItemList', $ProformsItemList);		
		$this->set('proforms_entry', $proforms_entry);		
		$this->set('pagination', $ProformsItemList->getPagination());
	}
	
	public function getRequestedSearchResults() {

		$ProformsItemList = new ProformsItemList();
		
		if($_REQUEST['keywords']){
			$_SESSION['keywords'] = $_REQUEST['keywords'];
		}
		if($_REQUEST['numResults']){
			$_SESSION['numResults'] = $_REQUEST['numResults'];
		}

		if($_REQUEST['asID']!=null){
			if($_REQUEST['asID']===0){
				$_SESSION['asID'] = null;
			}else{
				$_SESSION['asID'] = $_REQUEST['asID'];
			}
		}
		
		
		if($_REQUEST['selectedSearchField']){
			$_SESSION['selectedSearchField'] = $_REQUEST['selectedSearchField'];
		}
		if($_REQUEST['date_from']){
			$_SESSION['date_from'] = $_REQUEST['date_from'];
		}
		if($_REQUEST['date_to']){
			$_SESSION['date_to'] = $_REQUEST['date_to'];
		}
		
		$req = $ProformsItemList->getSearchRequest();
		$this->set('searchRequest', $req);
		
		$pkg = Package::getByHandle('proforms');
		
		if ($_SESSION['keywords'] != '') {
			$ProformsItemList->filterByKeywords($_SESSION['keywords']);
		}	
		
		if ($_SESSION['numResults']) {
			$ProformsItemList->setItemsPerPage($_SESSION['numResults']);
		} else {
			$ProformsItemList->setItemsPerPage(10);
		}

		if($_SESSION['asID'] > 0){
			$set = AttributeSet::getByID($_SESSION['asID']);
			if($set->asID > 0){
				$ProformsItemList->filterByAttributeSet($_SESSION['asID']);
			}
		}
		
		
		if($_REQUEST['ccm_order_by']){
			$ProformsItemList->sortBy($_REQUEST['ccm_order_by'],$_REQUEST['ccm_order_dir']);
		}else{
			$ProformsItemList->sortBy('ProformsItemID','DESC');
		}
		
		//$ProformsItemList->debug();
		
		if (is_array($_SESSION['selectedSearchField'])) {
			foreach($_SESSION['selectedSearchField'] as $i => $item) {
				// due to the way the form is setup, index will always be one more than the arrays
				if ($item != '') {
					switch($item) {
						case "date_added":
							$dateFrom = $_SESSION['date_from'];
							$dateTo = $_SESSION['date_to'];
							if ($dateFrom != '') {
								$dateFrom = date('Y-m-d', strtotime($dateFrom));
								$ProformsItemList->filter(false, "ci.ProformsItemDate >= '$dateFrom'");
								$dateFrom .= ' 00:00:00';
							}
							if ($dateTo != '') {
								$dateTo = date('Y-m-d', strtotime($dateTo));
								$dateTo .= ' 23:59:59';
								$ProformsItemList->filter(false, "ci.ProformsItemDate <= '$dateTo'");
							}
							break;

						default:
							Loader::model('attribute/categories/proforms_item', 'proforms');
							$akID = $item;
							$ak = ProformsItemAttributeKey::getByID($akID);
							$type = $ak->getAttributeType();
							$cnt = $type->getController();
							$cnt->setRequestArray($req);
							$cnt->setAttributeKey($ak);
							$cnt->searchForm($ProformsItemList);
							break;
					}
				}
			}
		}

		$this->set('searchRequest', $req);
		return $ProformsItemList;
	}
	
	private function saveData($Object) {
		
		Loader::model('attribute/categories/proforms_item', 'proforms');
		if(is_array($this->post('akID'))){
			foreach($this->post('akID') as $akID=>$value) {
				$ak = ProformsItemAttributeKey::getByID($akID);
				$ak->saveAttributeForm($Object);
			}
			$Object->reindex();
		}
	}
	
	public function add() {
		Loader::model('proforms_item', 'proforms');
		if ($this->isPost()) {
			$this->validate();
			if (!$this->error->has()) {
				$Object = ProformsItem::add();
				$this->saveData($Object);
				if($this->post('front_side')){
					$this->redirect('/index.php/ProformsItems/ProformsItem/'.$Object->ProformsItemID.'/');
				}else{
					$this->redirect('/dashboard/proforms/search/', 'object_added');
				}
			}
		}

	}
	
	public function edit($ObjectID) {
		
		Loader::model('proforms_item', 'proforms');
		$Object = ProformsItem::getByID($ObjectID);
		
		if (!empty($Object)) {
			$this->set('Object', $Object);	
		} else {
			$this->redirect('/dashboard/proforms/search/');
		}
	}
	
	public function update() {
		if ($this->isPost()) {
			$this->validate();
			if (!$this->error->has()) {
				Loader::model('proforms_item', 'proforms');
				$Object = ProformsItem::getByID($this->post('ProformsItemID'));
				$this->saveData($Object);
				$this->redirect('/dashboard/proforms/search/', 'object_updated');
			}else{
				$this->edit($this->post('ProformsItemID'));
			}
		}
	}
	
	public function delete($ObjectID=null) {
		Loader::model('proforms_item', 'proforms');
		foreach($_GET['ProformsItemID'] as $ObjectID){
			$Object = ProformsItem::getByID($ObjectID);
			if(is_object($Object)) {
				$Object->delete();
			}
		}
		$this->redirect('/dashboard/proforms/search/', 'object_deleted');
	}

	public function duplicate($ObjectID=null){

		Loader::model('proforms_item', 'proforms');
		if(isset($_GET['ProformsItemID'])){
			foreach($_GET['ProformsItemID'] as $ObjectID){
				$Object = ProformsItem::getByID($ObjectID);
				if(is_object($Object)) {
					$dupedProformsItem = $Object->duplicate($Object);
				}
				$dupedProformsItem->reindex();
			}
			$this->redirect('/dashboard/proforms/search/', 'object_duplicated');
		}
	}
	
	
	protected function validate() {
	


	}
	
	public function object_added() {
		$this->set('message', t('Entry added.'));
		$this->view();
	}
	public function object_updated() {
		$this->set('message', t('Entry updated.'));
		$this->view();
	}
	public function object_deleted() {
		$this->set('message', t('Entry deleted.'));
		$this->view();
	}
	public function object_duplicated() {
		$this->set('message', t('Entry Copied.'));
		$this->view();
	}
	
	public function on_before_render() {
		$this->set('error', $this->error);
	}
}

?>