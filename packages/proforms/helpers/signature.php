<?php    
/**
 * @package Helpers
 * @category Proforms
 * @author Chad Cantrell <chad@goradiantweb.com>
 * @copyright  Copyright (c) 2013 RadiantWeb. (http://www.goradiantweb.com)
 * @license    http://www.concrete5.org/license/     MIT License
 */

/**
 * Helpful functions for working with form signatures. Includes HTML input tags and the like
 */

defined('C5_EXECUTE') or die("Access Denied.");
class SignatureHelper {

	public function __construct() {

	}
	
	/** 
	 * Creates a button
	 * @param string $name
	 * @param string value
	 * @param array $fields Additional fields appended at the end of the submit button
	 * return string $html
	 */	 
	public function signature($name){
		
		$html = '<div id="sigPad_'.$name.'" style="max-width: 350px;">';
		$html .= '<div class="sig sigWrapper">';
		$html .= '	<!--    <div class="typed"></div>-->';
		$html .= '	<canvas class="pad" width="240" height="70"></canvas>';
		$html .= '	<input type="hidden" name="output" class="output">';
		$html .= '</div>';
		$html .= '<div class="clearButton"><a href="#clear" class="clear_sig">'.t('Clear Signature').'</a></div>';
		$html .= '</div>';
		
		$html .= "  
<script type=\"text/javascript\">
	$(document).ready(function() {
	  $('#sigPad_".$name."').signaturePad({
	  	drawOnly: true, 
	  	validateFields: true, 
	  	onFormError: function(e){
	  		if(e.drawInvalid){
		  		$('.loader_ajax').hide();
		  		var message = '<li class=\"ajax_response\">".t('A Signature Is Required!')."</li>';
		  		$('.ajax_error ul').find('ul').remove();
		  		$('.ajax_error ul').empty();
		  		$('.ajax_error ul').append(message).parent().parent().show();
		  		$('.close').bind('click tap', function(){
		  			$('.ajax_error').hide();
		  		});
		  		return false;
		  	}
	  	}
	  });
	});
</script>";

		return $html;
	}

}