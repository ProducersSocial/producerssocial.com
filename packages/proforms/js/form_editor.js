var liveDraggable = function (OPTS) {
		$(".sortable").sortable({
		    placeholder: "ui-state-highlight",
			cursor: 'move',
			opacity: 0.5,
			stop: function(ev,ui) {
				var dropped = ui.item;
				var akID = dropped.attr('data-key');
				dropped.addClass('added');
				var placed = $('.sortable li').index($('.added'));
				$('.added').removeClass('added');
				$.ajax({
					url: OPTS.EDITOR_DROP,
					data:{
						questionSetID: OPTS.QSID,
						akID: akID,
						order: placed
					}
				}).done(function(response){
				});
			}
		});
		$(".draggable").draggable({ 
		    connectToSortable: ".sortable",
			opacity: 0.65,
			appendTo: "body",
			helper: 'clone',
			revert: true,
			revertDuration: 2
		});
        $('.droppable').droppable({
			hoverClass: "hover",
		    accept: ".draggable",
		    drop: function(ev,ui) {
				var dropped = ui.draggable;
				dropped.addClass('added');
				var placed = $('.sortable li').index($('.ui-state-highlight'));
				var droppedOn = $(this);
				var atID = dropped.attr('data-id');
				$.ajax({
					url: OPTS.EDITOR_FORM,
					data:{
						questionSetID: OPTS.QSID,
						atID: atID,
						order: placed
					}
				}).done(function(response){
					$('#diolog_content').html(response);
					$('#diolog_content').dialog({ 
						title: OPTS.MESSAGES.TITLE,
						width: 680,
						close: function(){
							$('.ajax_loader').show();
							$('#form_preview_items').empty();
							$.ajax({
								url: OPTS.EDITOR_LIST,
								data: {
									questionSetID: OPTS.QSID
								}
							}).done(function(input_list){
								$('.ajax_loader').hide();
								$('#form_preview_items').html(input_list).sortable();
								liveDraggable(OPTS);
							});
						}
					});
				});
				
				liveDraggable(OPTS);
			}
	    });
	    $('.clearfix .delete').bind('click',function(){
	    	var parent = $(this).parent().parent().parent().parent();
	    	var key = parent.attr('data-key');
	    	var Vault = OPTS.TOKEN;
	    	var dURL = OPTS.BASE_PAGE + 'attributes/delete/' + key + '/' + Vault + '/';	
			if (confirm(OPTS.MESSAGES.CONFIRM)) { 
				$.ajax({
					url: dURL,
					type: 'post',
					data:{
						ajax: true
					}
				}).done(function(){
					parent.remove();
				});
			}
	    });
	    $('.clearfix label').bind('click',function(){
	    	$('.clearfix').removeClass('current');
			$(this).parent().addClass('current'); 
	    });
	    $('.clearfix .edit').bind('click',function(){
	        var parent = $(this).parent().parent().parent().parent();
			var key = parent.attr('data-key');
			var type = parent.attr('data-type');
			$.ajax({
				url: OPTS.EDITOR_FORM,
				data:{
					questionSetID: OPTS.QSID,
					atID: type,
					akID: key
				}
			}).done(function(responsed){
				$('#diolog_content').html(responsed);
				$('#diolog_content').dialog({ 
					title: OPTS.MESSAGES.EDIT,
					width: 680,
					close: function(){
						$('.ajax_loader').show();
						$('#form_preview_items').empty();
						$.ajax({
							url: OPTS.EDITOR_LIST,
							data: {
								questionSetID: OPTS.QSID
							}
						}).done(function(input_list){
							$('.ajax_loader').hide();
							$('#form_preview_items').html(input_list).sortable();
							liveDraggable(OPTS);
						});
					}
				});
			});
		});
    };
    