<?php     defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('attribute/set');

$set = AttributeSet::getByID($setID);

$bodyHTML = t('
<p>Hey there!</p>

<p>Thank you for your ').$set->getAttributeSetName().t(' form submission!</p>

<p>Our team will review this as soon as possible!</p>

<p>Thanks so much!</p>

'.substr(BASE_URL, 7).' Admin Team');

$body = strip_tags($bodyHTML);