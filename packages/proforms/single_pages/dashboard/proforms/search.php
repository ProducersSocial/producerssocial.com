	<?php     defined('C5_EXECUTE') or die("Access Denied."); 

	if (($this->controller->getTask() == 'update' || $this->controller->getTask() == 'edit' || $this->controller->getTask() == 'add')) { 
	
	$searchType = 'proforms-item';
	
	Loader::model('proforms_item', 'proforms');
	$title = $this->controller->getTask() == 'add' ? t('Add') : t('Update');
	$df = Loader::helper('form/date_time');
	$u = new User();
	$ui = UserInfo::getByID($u->uID);
	if (is_object($Object)) { 
		$ObjectID = $Object->getProformsItemID();
		$set = $Object->getAttributeSetID();
		$task = 'update';
		$titletext = t('Review Form Entry');
		$buttonText = t('Save Entry');
	} else {
		$task = 'add';
		$titletext = t('Add New Entry');
		$buttonText = t('Add New Entry');
	}
	?>	
	<style type="text/css">
		.ccm-ui label{padding-right: 12px!important; font-weight: bold;}
		#dates_wrap div{padding-top: 7px;}
		.ccm-input-textarea{width: 470px!important; height: 160px!important;}
	</style>
		<?php    echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t($titletext).'<span class="label" style="position:relative;top:-3px;left:12px;">'.t('* required field').'</span>', false, false, false);?>
		<div class="ccm-pane-body">
			<form method="post" enctype="multipart/form-data" action="<?php    echo $this->action($task)?>" id="<?php   echo $searchType?>-form">
			<?php    echo $form->hidden('ProformsItemID', $ObjectID); ?>
			<?php    echo $form->hidden('asID', $set); ?>
			<?php    echo $form->hidden('question_set', $set); ?>
			<?php   
			$category = AttributeKeyCategory::getByHandle('proforms_item');
			$attribs = ProformsItemAttributeKey::getList();
			$sets = $category->getAttributeSets();
			if (count($attribs) > 0) {
				?>
				<?php     $ih = Loader::helper('concrete/interface'); ?>
		        <?php     print $ih->submit($buttonText, $searchType.'-form', 'right', 'primary'); ?>
		        <?php   
		        if($task == 'update') {
					echo "<div style='display:inline; width:10px; height:10px;'></div>";
					print $ih->button(t('Delete'), $this->url('/dashboard/proforms/search/delete/?ProformsItemID[]='.$ObjectID), 'right', 'danger', array('style' => 'margin-right:5px;'));
				}
				?>
				<?php   
				if($_REQUEST['edit_entry']){
				?>
					<a href="<?php   echo $this->action('edit/'.$ObjectID.'/')?>" class="btn info ccm-button-v2-right" style="margin-right:5px;"><?php   echo t('exit edit mode')?></a>
				<?php   
				}else{
				?>
					<a href="<?php   echo $this->action('edit/'.$ObjectID.'/?edit_entry=true')?>" class="btn info ccm-button-v2-right" style="margin-right:5px;"><?php   echo t('enter edit mode')?></a>
				<?php   
				}
				?>
				<ul class="tabs">
				<?php   
				foreach ($sets as $as) {
					Loader::model('proforms_question_set','proforms');
					$qs = new ProformsQuestionSet();
					$internal = $qs->QuestionSetIsInternal($as->getAttributeSetID());
					if(!$set || ($as->getAttributeSetID() == $set || $internal)){
						$set_reset[] = $as;
						?>
						<li <?php    if(!$t){echo 'class="active"';}?>><a href="javascript:void(0)" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.<?php   echo strtolower(str_replace(' ','_',$as->getAttributeSetName()))?>').show();"><?php    echo t($as->getAttributeSetName())?></a>
						</li>				
						<?php   
						$t++;
					}
				}
				?>
				</ul>	
				<br style="clear: both;"/>
				<?php   
									
										foreach ($set_reset as $as) {
											// and now the panes
											$qs = new ProformsQuestionSet();
											$internal = $qs->QuestionSetIsInternal($as->getAttributeSetID());
											$pi++;
											echo "<div class='pane ".strtolower(str_replace(' ','_',$as->getAttributeSetName()))."'";
											if($pi==1){
												echo " style=\"display: block;\">";
											}else{
												echo " style=\"display: none;\">";
											}
											echo ("<table class=\"entry-form table table-striped\" border=\"0\" cellspacing=\"0\" cellpadding=\"8\">");
											$setAttribs = $as->getAttributeKeys();
											foreach ($setAttribs as $ak) {
												if (is_object($Object)) {
													$aValue = $Object->getAttributeValueObject($ak);
												}
												if($ak->getReviewStatus()>0){
													?>
													<tr>
								
														<?php   
														if($_REQUEST['edit_entry'] || $internal || $this->controller->getTask() == 'add'){
														?>
															<td><strong><?php    echo $ak->render('label');?></strong></td>
															<td>
																<?php   
																if($ak->getAttributeType()->getAttributeTypeHandle() != 'signature' || $aValue->getValue() < 1){
																	echo $ak->render('form',$aValue);
																}else{
																	echo $ak->render('display',$aValue);
																}
																?>
															</td>
														
														<?php   
														}else{
														?>
															<td><strong><?php    echo $ak->render('label');?></strong></td>
															<td>
																<?php   
																$cnt = $ak->getController();
																if(method_exists($cnt,'display_review')){
																	$ak->render('display_review',$aValue);
																}elseif(method_exists($cnt,'display')){
																	$ak->render('display',$aValue);
																}else{
																	if(is_object($aValue)){
																		$akID = $ak->getAttributeKeyID();

																		$val = $aValue->getValue();
																
																		if(is_a($val,'File')){
                                                    				        echo $val->getFileName();
                                                				        }elseif(!is_array($val) && !$ak->getTypeMultipart()){
                                                							echo $val;
                                                						}
																	}
																}
																?>
															</td>
														<?php   
														}
														?>
														</td>
													</tr>
												<?php   
												}
											}
											echo ("</table>");
											echo ("</div>");
										}
										?>
							<?php   
							}
							?>
			<script type="text/javascript">
			/*<![CDATA[*/
			$(document).ready(function(){
				$('.dialog-launch').dialog();
			});
			/*]]>*/
			</script>
			<div class="ccm-spacer"></div>
		</div>
	    <div class="ccm-pane-footer">
	    	<?php     $ih = Loader::helper('concrete/interface'); ?>
	        <?php     print $ih->submit($buttonText, $searchType.'-form', 'right', 'primary'); ?>
	        <?php   
	        if($task == 'update') {
				echo "<div style='display:inline; width:10px; height:10px;'></div>";
				print $ih->button(t('Delete'), $this->url('/dashboard/proforms/search/delete/?ProformsItemID[]='.$ObjectID), 'right', 'danger', array('style' => 'margin-right:5px;'));
			}
			?>
			<?php   
			if($_REQUEST['edit_entry']){
			?>
				<a href="<?php   echo $this->action('edit/'.$ObjectID.'/')?>" class="btn info ccm-button-v2-right" style="margin-right:5px;"><?php   echo t('exit edit mode')?></a>
			<?php   
			}else{
			?>
				<a href="<?php   echo $this->action('edit/'.$ObjectID.'/?edit_entry=true')?>" class="btn info ccm-button-v2-right" style="margin-right:5px;"><?php   echo t('enter edit mode')?></a>
			<?php   
			}
			?>
	        <?php     print $ih->button(t('Cancel'), $this->url('/dashboard/proforms/search/'), 'left'); ?>
	    </div>
	    </form>
	<?php    }else{ ?>
	<style type="text/css">
		.ccm-ui td,.ccm-ui th{padding: 8px!important;}
	</style>
	<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForm Submissions'), t('Search Proforms Item\'s on your site and perform bulk actions on them.'), false, false);?>
	
		<div class="ccm-pane-options" id="ccm-<?php   echo $searchInstance?>-pane-options">
		<?php    Loader::packageElement('dashboard/proforms/search/search_form_advanced', 'proforms'); ?>
		</div>
		<div class="ccm-pane-body">
		<?php    Loader::packageElement('dashboard/proforms/search/search_results', 'proforms', array('proforms_entry' => $proforms_entry, 'ProformsItemList' => $ProformsItemList, 'pagination' => $pagination)); ?>
		</div>

		
	<div class="ccm-pane-footer">

	</div>
	<?php   
	}
	?>
<script type="text/javascript">
/*<![CDATA[*/

/*]]>*/
</script>