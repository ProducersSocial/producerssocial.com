<?php     defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php   
$valt = Loader::helper('validation/token');
$bt = BlockType::getByHandle('proforms_display');
$uh = Loader::helper('concrete/urls');
$editor_form = $uh->getToolsURL('proforms/editor_field','proforms');
if($expanded_mode){ 
	$editor_list = $uh->getToolsURL('proforms/editor_list','proforms');
}else{
	$editor_list = $uh->getToolsURL('proforms/editor_list_simple','proforms');
}
$editor_drop = $uh->getToolsURL('proforms/editor_drop','proforms');
$base_url = $this->url('/dashboard/proforms/');
?>
<style type="text/css">
.icon_form {background-image:url('<?php     echo ASSETS_URL_IMAGES?>/icons_sprite.png')!important;}
</style>
<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForms Editor'), false, false, false)?>
	<div class="ccm-pane-options">
		<form method="get" class="form-horizontal inline-form-fix" action="<?php   echo $this->url('/dashboard/proforms/attributes/select_type/')?>" id="ccm-attribute-build-form">
			<div class="ccm-pane-options-permanent-search">
				<div class="span5">
					<a href="<?php    echo $this->url('/dashboard/proforms/manage_forms', 'category', $category->getAttributeKeyCategoryID())?>" id="" class="btn warning " style="float:left;margin-right: 12px;"><i class="icon-list-alt icon-white"></i> <?php    echo t('Manage Forms')?></a>
					<a href="<?php    echo $this->url('/dashboard/proforms/attributes')?>" id="" class="btn info "><i class="icon-list-alt icon-white"></i>  <?php    echo t('Manage Questions')?></a>
				</div>
				<div class="span6">
					<?php    if($question_set){ ?>
						<a href="<?php    echo Loader::helper('concrete/urls')->getToolsURL('proforms/preview_form.php','proforms');?>?qsID=<?php   echo $question_set?>" dialog-width="650" dialog-height="440" dialog-modal="true" dialog-title="<?php    echo t('Preview Form'); ?>" dialog-on-close="" onClick="javascript:;" class="dialog-launch btn success" style="float:right;"><i class="icon-eye-open icon-white"></i> <?php   echo t('Preview Form')?></a>
						<?php    } ?>
					<label><?php   echo t('Chose a Form')?></label>
					<div class="input">
						<select name="chose_form" id="chose_form">
						<option value=""><?php   echo t('-- none selected --')?></option>
						<?php    
						if(is_array($sets)){
							foreach($sets as $set){
								echo '<option value="'.$set->getAttributeSetID().'"';
								if($question_set == $set->getAttributeSetID()){echo ' selected';}
								echo '>'.$set->getAttributeSetName().'</option>';
							}
						}
						?>
						</select>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#chose_form').change(function(){
									var selected = $('#chose_form option:selected').val();
									window.location.href = '<?php   echo $this->url('/dashboard/proforms/editor/')?>' + selected + '/';
								});
								$('.dialog-launch').dialog();
							});
						</script>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="ccm-pane-body">
		<?php    if($question_set){ ?>
		<div class="span8 form_preview">
			<div class="form_build_containter">
				<?php    if($expanded_mode){ ?>
				<a href="<?php   echo $this->action('view',$question_set)?>" class="btn" style="float:right;"><?php   echo t('Simple Mode')?></a>
				<?php    }else{ ?>
				<a href="<?php   echo $this->action('expanded_mode',$question_set)?>" class="btn" style="float:right;"><?php   echo t('Expanded Mode')?></a>
				<?php    } ?>
				<h4><?php   echo t('Form Preview')?></h4>
				<hr/>
				<div class="ajax_loader loader_ajax" style="display: none;"><br/><img src="<?php    echo Loader::helper('concrete/urls')->getBlockTypeAssetsURL($bt, 'images/ajax-loader.gif')?>" alt="loading"/><br/></div>
				<form id="form_preview_items">

				</form>
			</div>
		</div>
		<div class="span3 attributes last">
			<div id="form_question_types_container">
				<div class="form_build_containter">
					<h4><?php   echo t('Question Types')?></h4>
					<hr/>
					<style>
					.loader {display: block;float: right;margin-right: 32px;height:16px;width:16px;background-image:url('<?php     echo ASSETS_URL_IMAGES?>/icons/icon_header_loading.gif');}
					</style>
					<form id="live-search" action="" class="styled" method="post">
					    <input type="text" class="text-input" id="csfilter" value="" placeholder="Search..."/>
					    <span id="filter-count"></span>
						<div class="loader" style="display: none;"></div>
					</form>
					<ul id="question_types">
						<?php    foreach($otypes as $at){ ?>
						<li class="draggable" data-id="<?php   echo $at->getAttributeTypeID()?>"><img src="<?php   echo $at->getAttributeTypeIconSRC()?>" alt="icon"> &nbsp;<?php   echo $at->getAttributeTypeName()?></li>
						<?php    } ?>
					</ul>
				</div>
			</div>
			<div style="text-align: right;">
			<h4><?php   echo t('Need more QuestionTypes?')?></h4>
			<p><a href="http://goradiantweb.com/contact/free-quote/">http://goradiantweb.com</a></p>
			</div>
		</div>
		<br style="clear:both;" />
		<div id="input_diolog" style="display: none;">
			<div id="diolog_content">
			
			</div>
		</div>
		<script type="text/javascript">
		$(function() {
			var elist = '<?php   echo $editor_list?>';
			var qsID  = <?php   echo $question_set?>;
		
			var data = {
				TOKEN: '<?php   echo $valt->generate('delete_attribute')?>',
				QSID: <?php   echo $question_set?>,
				EDITOR_LIST: '<?php   echo $editor_list?>',
				EDITOR_FORM: '<?php   echo $editor_form?>',
				EDITOR_DROP: '<?php   echo $editor_drop?>',
				BASE_PAGE: '<?php   echo $base_url?>',
				MESSAGES: {
					TITLE: '<?php   echo t('Add Question')?>',
					EDIT: '<?php   echo t('Edit Question')?>',
					CONFIRM: '<?php    echo t('Are you sure you want to remove this Question?')?>'
				}
			};
		    $.ajax({
				url: elist,
				data: {
					questionSetID: qsID
				}
			}).done(function(input_list){
				$('#form_preview_items').html(input_list);
				$('#form_preview_items ul').sortable();
				liveDraggable(data);
			});
			$(document).scroll(function(){
				if($(this).scrollTop() > 258){
					$('#question_types').addClass('questions_fixed');
				}else{
					$('#question_types').removeClass('questions_fixed');
				}
			});
			$('#csfilter').attr("autocomplete","off");
		    $("#csfilter").keyup(function(){
		    	if($(this).val().length > 3){
		    		$('.loader').show();
		 			var keyword = $(this).val();
		 			$(".draggable").each(function(){
			            if ($(this).text().search(new RegExp(keyword, "i","g")) > 1) {
			                $(this).show();
			            }else{
				            $(this).fadeOut();
			            }
			        });
			        $('.loader').hide();
		    	}else{
			    	$('.draggable').show();
		    	}
		    	return true;
		    });
	
		});
		</script>
		<?php    }else{ ?>
		<div class="span12 form_preview">
			<div class="ajax_error" style="width: 400px; margin: 45px auto;">
				<div class="alert alert-danger">
				  	<button type="button" class="close" data-dismiss="alert">&times;</button>
				  	<h4><?php    echo t('Hmmmm:'); ?></h4>
					<div class="error_message"><ul><li><?php   echo t('No Form Selected')?></li></ul></div>
				</div>
			</div>
		</div>
		<?php    } ?>
	</div>
<div class="ccm-pane-footer">
	
</div>
