<?php     defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php   
if($type){
	$cnt = $type->getController();
	if(method_exists($cnt,'help_text')){
		$help = array($cnt->help_text(),'http://www.concrete5.org/marketplace/addons/proforms/documentation/');
	}else{
		$help = false;
	}
}
?>
<?php    if (isset($key)) { ?>
<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Edit Question'), $help, false, false)?>
<form method="post" action="<?php   echo $this->action('edit')?>" id="ccm-attribute-key-form">



<?php    Loader::packageElement("attribute/type_form_required","proforms", array('category' => $category, 'type' => $type, 'key' => $key)); ?>

</form>

<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);?>



<?php    } else if ($this->controller->getTask() == 'select_type' || $this->controller->getTask() == 'add' || $this->controller->getTask() == 'edit') { ?>

	<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForms Forms'), $help, false, false)?>

	<?php    if (isset($type)) { ?>
		<form method="post" action="<?php   echo $this->action('add')?>" id="ccm-attribute-key-form">
	
		<?php    Loader::packageElement("attribute/type_form_required", "proforms",array('category' => $category, 'type' => $type)); ?>
	
		</form>	
	<?php    } ?>
	
	<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);?>



<?php    } else { ?>

	<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForms Questions List'), false, false, false)?>

	<?php   
	$attribs = ProformsItemAttributeKey::getList();
	Loader::packageElement('dashboard/attributes_table', 'proforms', array('category' => $category, 'attribs'=> $attribs, 'set'=>$set, 'editURL' => '/dashboard/proforms/attributes')); ?>



	<div class="ccm-pane-footer">
		
    </div>

<?php    } ?>
<script type="text/javascript">
$(function() {

	$(".ccm-attribute-list-wrapper").sortable({
		handle: 'img.ccm-attribute-icon',
		cursor: 'move',
		opacity: 0.5,
		stop: function(ev,ui) {
			var dropped = ui.item;
			var akID = dropped.attr('data-key');
			var questionSetID = dropped.parent().attr('attribute-set-id');
			dropped.addClass('added');
			var placed = dropped.parent().find('.ccm-attribute').index($('.added'));
			$('.added').removeClass('added');
			$.ajax({
				url: '<?php     echo REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/editor_drop'; ?>',
				data:{
					questionSetID: questionSetID,
					akID: akID,
					order: placed
				}
			}).done(function(r){

			});
		}
	});

});

</script>
<style type="text/css">
div.ccm-attributes-list img.ccm-attribute-icon:hover {cursor: move}
</style>
