<?php     defined('C5_EXECUTE') or die("Access Denied.");
$form = Loader::helper('form');
$txt = Loader::helper('text');?>
<?php     if (in_array($this->controller->getTask(), array('update_set', 'update_set_attributes', 'edit', 'delete_set'))) { 

	echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Edit Form'), false, 'span8 offset2');?>
		
		<div class="clearfix">
		<div class="row">
		<div class="span-pane-half">
		<h3><?php    echo t('Update Form Details')?></h3>
	
		<?php     if ($set->isAttributeSetLocked() && $set->getAttributeSetHandle() != 'example_form') { ?>
			<div class="info block-message alert-message">
				<p><?php     echo t('This Form is locked. It cannot be deleted, and its handle cannot be changed.')?></p>
			</div>	
		<?php     } ?>

		<form class="" method="post" action="<?php     echo $this->action('update_set')?>">
			<input type="hidden" name="asID" value="<?php     echo $set->getAttributeSetID()?>" />
			<?php     echo Loader::helper('validation/token')->output('update_set')?>
			<div class="clearfix">
				<?php     echo $form->label('asHandle', t('Form Handle'))?>
				<div class="input">
					<?php     if ($set->isAttributeSetLocked() && $set->getAttributeSetHandle() != 'example_form') { ?>
						<?php     echo $form->text('asHandle', $set->getAttributeSetHandle(), array('disabled' => 'disabled'))?>
					<?php     } else { ?>
						<?php     echo $form->text('asHandle', $set->getAttributeSetHandle())?>
					<?php     } ?>
				</div>
			</div>
	
			<div class="clearfix">
				<?php     echo $form->label('asName', t('Form Name'))?>
				<div class="input">
					<?php     echo $form->text('asName', $set->getAttributeSetName())?>
				</div>
			</div>
			
			<div class="clearfix">
				<?php     echo $form->label('is_internal', t('Questions in this Form are used for internal use only'))?>
				<div class="input">
					<?php   
					Loader::model('proforms_question_set','proforms');
					$qs = new ProformsQuestionSet();
					?>
					<?php     echo $form->checkbox('is_internal', 1, $qs->QuestionSetIsInternal($set->getAttributeSetID()))?>
				</div>
			</div>

			<div class="clearfix">
				<label></label>
				<div class="input">
					<?php     echo $form->submit('submit', t('Update Form'), array('class' => ''))?>
				</div>
			</div>
		</form>

		<?php     if (!$set->isAttributeSetLocked() || $set->getAttributeSetHandle() == 'example_form') { ?>	
			<h3><?php    echo t('Delete Set')?></h3>
			<p><?php     echo t('Warning, this cannot be undone. No questions will be deleted but they will no longer be grouped together.')?></p>
			<form method="post" action="<?php     echo $this->action('delete_set')?>" class="">
				<input type="hidden" name="asID" value="<?php     echo $set->getAttributeSetID()?>" />
				<?php     echo Loader::helper('validation/token')->output('delete_set')?>
			
				<div class="clearfix">
					<?php     echo $form->submit('submit', t('Delete Form'), array('class' => 'danger'))?>
				</div>
			</form>
		<?php     } ?>
		</div>

		<div class="span-pane-half">
		<h3><?php    echo t('Add Questions to Form')?></h3>
	
		<form class="" method="post" action="<?php     echo $this->action('update_set_attributes')?>">
			<input type="hidden" name="asID" value="<?php     echo $set->getAttributeSetID()?>" />
			<?php     echo Loader::helper('validation/token')->output('update_set_attributes')?>
	
			<?php     
			$cat = AttributeKeyCategory::getByID($set->getAttributeSetKeyCategoryID());
			$list = AttributeKey::getList($cat->getAttributeKeyCategoryHandle());
			//$unassigned = $cat->getUnassignedAttributeKeys();
			if (count($list) > 0) { ?>
	
				<div class="clearfix">
					<ul class="inputs-list">
	
						<?php     foreach($list as $ak) { 
	
						//$disabled = '';
						//if (!in_array($ak, $unassigned) && (!$ak->inAttributeSet($set))) { 
						//	$disabled = array('disabled' => 'disabled');
						//}
		
						?>
							<li>
								<label>
									<?php     echo $form->checkbox('akID[]', $ak->getAttributeKeyID(), $ak->inAttributeSet($set), $disabled)?>
									<span><?php     echo $ak->getAttributeKeyName()?></span>
									<span class="help-inline"><?php     echo $ak->getAttributeKeyHandle()?></span>
								</label>
							</li>	
						<?php     } ?>
					</ul>
				</div>
		
				<div class="clearfix">
					<?php     echo $form->submit('submit', t('Update Questions'), array('class' => ''))?>
				</div>
			<?php     } else { ?>
				<p><?php     echo t('No questions found.')?></p>
			<?php     } ?>
	
		</form>
		</div>
		</div>
		</div>


	<?php     echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);?>




<?php     } else if($this->controller->getTask() == 'category' || $this->controller->getTask() == 'add_set'){ ?>

	<?php     echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForms Forms List'), false, 'span8 offset2',false);?>
	<div class="ccm-pane-options">
		<div class="ccm-pane-options-permanent-search">

				<a href="<?php   echo $this->url('/dashboard/proforms/editor/')?>" class="btn primary"><i class="icon-edit icon-white"></i>  <?php   echo t('Go To Editor')?></a>
				<a href="<?php    echo $this->url('/dashboard/proforms/attributes')?>" id="" class="btn info "><i class="icon-list-alt icon-white"></i>  <?php    echo t('Manage Questions')?></a>

		</div>
	</div>
	<div class="ccm-pane-body">
		<style type="text/css">
		.ccm-group{max-width: 400px!important;}
		</style>
		<form method="post" action="<?php     echo $this->action('add_set')?>">
	
	
		<?php     if (count($sets) > 0) { ?>
			<style type="text/css">
			div.ccm-group a.ccm-group-inner{display:inline!important;line-height: 35px;}
			</style>
			<div class="ccm-attribute-sortable-set-list">
			
				<?php     foreach($sets as $asl) { ?>
					<div class="ccm-group" id="asID_<?php     echo $asl->getAttributeSetID()?>">
					
						<a href="<?php    echo Loader::helper('concrete/urls')->getToolsURL('proforms/preview_form.php','proforms');?>?qsID=<?php   echo $asl->getAttributeSetID()?>" dialog-width="650" dialog-height="440" dialog-modal="true" dialog-title="<?php    echo t('Preview Form'); ?>" dialog-on-close="" onClick="javascript:;" class="dialog-launch btn btn-mini"><i class="icon-eye-open"></i></a> 
								
						<a href="<?php    echo BASE_URL.DIR_REL?>/index.php/dashboard/proforms/editor/<?php   echo $asl->getAttributeSetID()?>/"  class="btn btn-mini"><i class="icon-edit"></i></a>
					
						<img class="ccm-group-sort" src="<?php     echo ASSETS_URL_IMAGES?>/icons/up_down.png" width="14" height="14" />
						<a class="ccm-group-inner" href="<?php     echo $this->url('/dashboard/proforms/manage_forms/', 'edit', $asl->getAttributeSetID())?>" style="background-image: url(<?php     echo ASSETS_URL_IMAGES?>/icons/group.png)"><?php     echo $asl->getAttributeSetName()?></a>
					</div>
				<?php     } ?>
			</div>
			<script type="text/javascript">
			/*<![CDATA[*/
			$(document).ready(function(){
				$('.dialog-launch').dialog();
			});
			/*]]>*/
			</script>
	<?php     } else { ?>
		<?php     echo t('No Forms currently defined.')?>
	<?php     } ?>

	<br/>
	
	<h3><?php    echo t('Add Form')?></h3>

	<input type="hidden" name="categoryID" value="<?php     echo $categoryID?>" />
	<?php     echo Loader::helper('validation/token')->output('add_set')?>
	<div class="clearfix">
		<?php     echo $form->label('asHandle', t('Form Handle'))?>
		<div class="input">
			<?php     echo $form->text('asHandle')?>
		</div>
	</div>
	
	<div class="clearfix">
		<?php     echo $form->label('asName', t('Form Name'))?>
		<div class="input">
			<?php     echo $form->text('asName')?>
		</div>
	</div>
	
	<div class="clearfix">
		<?php     echo $form->label('is_internal', t('Questions in this Form are used for internal use only'))?>
		<div class="input">
			<?php     echo $form->checkbox('is_internal', 1 )?>
		</div>
	</div>
	
	<div class="clearfix">
		<label></label>
		<div class="input">
			<?php     echo $form->submit('submit', t('Add Form'), array('class' => 'btn success'))?>
		</div>
	</div>

	</form>

	</div>
	<div class="ccm-pane-footer">
		
    </div>

<?php     } else { ?>
	<?php     echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Questions Categories'), false, 'span8 offset3');?>
		<p><?php     echo t('Question Categories are used to group different types of Forms.')?></p>
		<div class="">
			<?php     
			if(count($categories) > 0) {
				foreach($categories as $cat) { ?>
					<div class="ccm-group" id="acID_<?php     echo $cat->getAttributeKeyCategoryID()?>">
						<a class="ccm-group-inner" href="<?php     echo $this->url('/dashboard/proforms/manage_forms/', 'category', $cat->getAttributeKeyCategoryID())?>" style="background-image: url(<?php     echo ASSETS_URL_IMAGES?>/icons/group.png)"><?php     echo $txt->unhandle($cat->getAttributeKeyCategoryHandle())?></a>
					</div>
				<?php     } 
			} else {
				echo t('No question categories currently defined.');
			} ?>
		</div>
	<?php     echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);?>	
<?php     } ?>

