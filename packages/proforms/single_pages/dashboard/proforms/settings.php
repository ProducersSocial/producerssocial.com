<?php     defined('C5_EXECUTE') or die("Access Denied."); ?>
	<?php    echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForms  Settings'), false, false, false);?>
	<div class="ccm-pane-body">
	<?php   
	$fm = Loader::helper('form');
	$pkg = Package::getByHandle('proforms_item');
	?>
	<form name="settings" action="<?php   echo $this->action('set_settings')?>" method="post">
		<div class="clearfix">
			<strong><?php    echo t('Exporting');?></strong>
			<div class="input">
				<div class="event-attributes">
					<div>
					<?php   
					echo $fm->checkbox('export_all',1,$export_all);
					?>
					<?php    echo t('Allow exporting of all form fields rather than only form headers?');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ccm-pane-footer">
    	<?php     $ih = Loader::helper('concrete/interface'); ?>
        <?php     print $ih->submit(t('Save Settings'), 'settings', 'right', 'primary'); ?>
    </div>
    </form>
<?php   
$db = Loader::db();
?>