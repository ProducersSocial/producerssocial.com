<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
$form = Loader::helper('form');
Loader::model('proforms_item', 'proforms');
Loader::model('attribute/categories/proforms_item', 'proforms');
$attribs = ProformsItemAttributeKey::getList();

$proforms_entry = array();
if (is_array($_REQUEST['ProformsItemID'])) {
	foreach($_REQUEST['ProformsItemID'] as $ObjectID) {
		$noO = ProformsItem::getByID($ObjectID);
		$proforms_entry[] = $noO;
	}
}

if ($_POST['task'] == 'update_extended_attribute') {
	$fakID = $_REQUEST['fakID'];
	$value = ''; 
	
	$ak = ProformsItemAttributeKey::get($fakID);
	foreach($proforms_entry as $noO) {
		$ak->saveAttributeForm($noO);
		$noO->reindex();
	}
	$val = $noO->getAttributeValueObject($ak);
	print $val->getValue('display');	
	exit;
} 

if ($_POST['task'] == 'clear_extended_attribute') {

	$fakID = $_REQUEST['fakID'];
	$value = ''; 

	$ak = ProformsItemAttributeKey::get($fakID);
	foreach($proforms_entry as $noO) {
		$noO->clearAttribute($ak);
	}

	print '<div class="ccm-attribute-field-none">' . t('None') . '</div>';
	exit;
}


function printAttributeRow($ak) {
	global $proforms_entry, $form;
	
	$value = '';
	for ($i = 0; $i < count($proforms_entry); $i++) {
		$lastValue = $value;
		$noO = $proforms_entry[$i];
		$vo = $noO->getAttributeValueObject($ak);
		if (is_object($vo)) {
			$value = $vo->getValue('display');
			if ($i > 0 ) {
				if ($lastValue != $value) {
					$value = '<div class="ccm-attribute-field-none">' . t('Multiple Values') . '</div>';
					break;
				}
			}
		}
	}	

	if ($value == '') {
		$text = '<div class="ccm-attribute-field-none">' . t('None') . '</div>';
	} else {
		$text = $value;
	}
	if ($ak->isAttributeKeyEditable()) { 
	$type = $ak->getAttributeType();
	$hiddenFIDfields='';
	foreach($proforms_entry as $noO) {
		$hiddenfields.=' '.$form->hidden('ProformsItemID[]' , $noO->getProformsItemID()).' ';
	}	
	
	$html = '
	<tr class="ccm-attribute-editable-field">
		<th><a href="javascript:void(0)">' . $ak->getAttributeKeyName() . '</a></th>
		<td width="100%" class="ccm-attribute-editable-field-central"><div class="ccm-attribute-editable-field-text">' . $text . '</div>
		<form method="post" action="' . REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/bulk_properties">
			<input type="hidden" name="fakID" value="' . $ak->getAttributeKeyID() . '" />
			'.$hiddenfields.'
			<input type="hidden" name="task" value="update_extended_attribute" />
			<div class="ccm-attribute-editable-field-form ccm-attribute-editable-field-type-' . strtolower($type->getAttributeTypeHandle()) . '">
			' . $ak->render('form', $vo, true) . '
			</div>
		</form>
		</td>
		<td class="ccm-attribute-editable-field-save"><a href="javascript:void(0)"><img src="' . ASSETS_URL_IMAGES . '/icons/edit_small.png" width="16" height="16" class="ccm-attribute-editable-field-save-button" /></a>
		<a href="javascript:void(0)"><img src="' . ASSETS_URL_IMAGES . '/icons/close.png" width="16" height="16" class="ccm-attribute-editable-field-clear-button" /></a>
		<img src="' . ASSETS_URL_IMAGES . '/throbber_white_16.gif" width="16" height="16" class="ccm-attribute-editable-field-loading" />
		</td>
	</tr>';
	
	} else {

	$html = '
	<tr>
		<th>' . $ak->getAttributeKeyName() . '</th>
		<td width="100%" colspan="2">' . $text . '</td>
	</tr>';	
	}
	print $html;
}

if (!isset($_REQUEST['reload'])) { ?>
	<div id="ccm-proforms-item-proforms_item-wrapper">
<?php    } ?>

<h1><?php    echo t('Object Details')?></h1>


<div id="ccm-proforms-item-proforms_item">

<table border="0" cellspacing="0" cellpadding="0" class="ccm-grid">
<?php    

foreach($attribs as $at) {

	printAttributeRow($at);

}

?>
</table>

<br/>  

</div>

<script type="text/javascript">
$(function() { 
	ccm_activateEditablePropertiesGrid();  
});
</script>

<?php    
if (!isset($_REQUEST['reload'])) { ?>
</div>
<?php    }
