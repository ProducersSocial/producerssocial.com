<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
// Get the Authorize SDK
Loader::library('gateways/authorize/autoload', 'proforms');
Loader::model('proforms_item','proforms');
$jh = Loader::helper('json');
//Get Amount
$question_set =  $_REQUEST['question_set']; //Question Set ID
$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
$price_handle = $_REQUEST['authorize_price_handle'];//Authorize Price Handle
//Update the Price Total with Transaction ID so that it is PAID
foreach ($setAttribs as $ak) {
	if($ak->akHandle == $price_handle){
		$akID = $ak->akID;
		$amount = $_REQUEST['akID'][$akID]['value'];
	}
	if($ak->getAttributeType()->getAttributeTypeHandle() == 'payment_authorize'){
		//print_r($ak);
		$authorize = $ak->getController()->getAuthorizeCredentials();
		$authorize_api_login_id = $authorize->authorize_api_login_id;
		$authorize_transaction_key = $authorize->authorize_transaction_key;
	}
	//print_r($ak);
}

if(!$amount)
{
    $pf = ProformsItem::getByID($_REQUEST['ProformsItemID']);
    $amount = $pf->getAttribute($price_handle);
}

$transaction = new AuthorizeNetAIM($authorize_api_login_id, $authorize_transaction_key);
//$transaction->amount = '9.99';
//$transaction->card_num = '4007000000027';
//$transaction->exp_date = '10/16';
//$customer = (object)array();
//$customer->first_name = $_POST['x_first_name'];
//$customer->last_name = $_POST['x_last_name'];
//$customer->company = $_POST['x_company'];
//$customer->address = $_POST['x_address'];
//$customer->city = $_POST['x_city'];
//$customer->state = $_POST['x_state'];
//$customer->zip = $_POST['x_zip'];
//$customer->country = $_POST['x_country'];
//$customer->phone = $_POST['x_phone'];
//$customer->fax = $_POST['x_fax'];
//$customer->email = $_POST['x_email'];
//$customer->cust_id = $_POST['x_cust_id];
//$customer->customer_ip = $_POST['x_customer_ip'];
//$transaction->setFields($customer);
$exp_date = $_POST['x_exp_month'].'/'.$_POST['x_exp_year'];
$transaction->amount = $amount;
$transaction->card_num = $_POST['x_card_num'];
$transaction->exp_date = $exp_date;
$transaction->card_code = $_POST['x_card_code'];
$transaction->setSandbox($_POST['set_sandbox']);


$response = $transaction->authorizeAndCapture();

Log::addEntry($jh->encode($response));
$arr = array('transaction_id' => '','error' => '','sandbox'=>$_POST['set_sandbox'], 'amount'=>$amount);
if ($response->approved) {
	//echo $response->transaction_id;
	$arr['transaction_id'] = $response->transaction_id;
} else {
	//Build the error code.
	$arr['error'] = $response->response_reason_text;
}
//Print out the JSON
echo $jh->encode($arr);
?>