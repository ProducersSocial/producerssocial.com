<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

$cnt = Loader::controller('/dashboard/proforms/search');
$ProformsItemList = $cnt->getRequestedSearchResults();
//$_SESSION['numResults'] = 1000;
$proforms_entry = $ProformsItemList->get();


Loader::packageElement('dashboard/proforms/search/search_results_export', 'proforms', array('proforms_entry' => $proforms_entry, 'ProformsItemList' => $ProformsItemList));