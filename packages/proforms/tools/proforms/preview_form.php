<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

$question_set = $_REQUEST['qsID'];
$bt = BlockType::getByHandle('proforms_display');
$th = Loader::helper('concrete/urls'); 
$fm = Loader::helper('form');
$bID = 1;
?>
<style type="text/css">
.ProformsItem_item {padding-bottom: 12px; padding-top: 12px; border-top-color: #e3e3e3; border-top-width: 1px; border-top-style: dashed; float: left;clear: left; width: 100%;}
.ProformsItem_item .map_img{border-color: #cbcbcb; border-width: 2px; border-style: solid; float: left; margin-right: 12px;}
.ProformsItem_item .ProformsItem_price{float: left;}
.ProformsItem_item h2{ margin-top: 3px; }
.ProformsItem_item h3{margin-top: 0px;}
.proform-attributes input,.proform-attributes select{margin:0!important;}
.formGrayOut{ background: #ccc;}
.ajax_loader {padding-left: 22px; width: 80px!important;margin-right: 0px!important;}
.ccm-ui label{display: inline-block!important; width:100%;}
.required {color: red!important; display: inline-block!important;}
a.next_step,a.prev_step,a.submitit{color: white!important;margin-top: 32px!important;}
.form_step{float: left; position: absolute;}
.proforms_submit{display: block; clear: both;}
.proform_slider{margin-bottom: 35px;position: relative;}
.bar_item{float: left; display: block; min-width: 155px; height: 40px;padding-top: 6px;line-height: 32px;border: 1px solid #d2d2d2;background: #f2f2f2; /* Old browsers */ background: -moz-linear-gradient(top,  #f2f2f2 0%, #d8d8d8 100%); /* FF3.6+ */ background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f2f2), color-stop(100%,#d8d8d8)); /* Chrome,Safari4+ */ background: -webkit-linear-gradient(top,  #f2f2f2 0%,#d8d8d8 100%); /* Chrome10+,Safari5.1+ */ background: -o-linear-gradient(top,  #f2f2f2 0%,#d8d8d8 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #f2f2f2 0%,#d8d8d8 100%); /* IE10+ */ background: linear-gradient(to bottom,  #f2f2f2 0%,#d8d8d8 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f2f2', endColorstr='#d8d8d8',GradientType=0 ); /* IE6-9 */ }
.current_slide{background: #e2e2e2; /* Old browsers */background: -moz-linear-gradient(top,  #e2e2e2 0%, #c4c4c4 100%); /* FF3.6+ */background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e2e2e2), color-stop(100%,#c4c4c4)); /* Chrome,Safari4+ */background: -webkit-linear-gradient(top,  #e2e2e2 0%,#c4c4c4 100%); /* Chrome10+,Safari5.1+ */background: -o-linear-gradient(top,  #e2e2e2 0%,#c4c4c4 100%); /* Opera 11.10+ */background: -ms-linear-gradient(top,  #e2e2e2 0%,#c4c4c4 100%); /* IE10+ */background: linear-gradient(to bottom,  #e2e2e2 0%,#c4c4c4 100%); /* W3C */filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#c4c4c4',GradientType=0 ); /* IE6-9 */}
.bar_item .number{float: left; font-size: 20px; margin-left: 22px; color: #828282; -webkit-border-radius: 32px;border-radius: 32px; background-color:#fbfafa; display: block; width: 30px; height: 30px; text-align: center;}
.bar_item .step_name{ margin-left: 68px;font-weight: bold;color: #000000; }
.bar_item {min-width: 150px;}
.bar_item:first-child{-webkit-border-radius: 5px 0px 0px 5px;border-radius: 5px 0px 0px 5px;}
.bar_item:last-child{-webkit-border-radius: 0px 5px 5px 0px;border-radius: 0px 5px 5px 0px;}
.bar_item :hover{cursor: pointer;}
.form_step .multiplex_set .control-label {width: 120px!important;}
input.input_error,select.input_error,textarea.input_error,.input_error{
-webkit-box-shadow: 0px 0px 6px 1px rgba(256, 0, 0, .5)!important;
box-shadow: 0px 0px 6px 1px rgba(256, 0, 0, .5)!important;
-moz-box-shadow: 0px 0px 6px 1px rgba(256, 0, 0, .5)!important;
-webkit-border-radius: 5px;
border-radius: 5px;
display:inline-block;}
.response{margin-top: 42px;}
.address_form{margin-left: 170px!important;}
.address_form input,.address_form select{margin: 8px 0!important;}
	.address_form input,
	.ccm-ui.proform_slider input[type=text],
	.ccm-ui.proform_slider input.hasDatepicker,
	.ccm-ui.proform_slider textarea,
	.ccm-ui.proform_slider select,
	.ccm-ui.proform_slider .uneditable-input{
	height: 28px!important;
	width: 410px;
}

.ccm-ui.proform_slider  input[type=checkbox]{height: auto!important;width: auto!important;}

.ccm-ui.proform_slider  textarea{
	height: 150px!important;
}

.ccm-ui.proform_slider  select{
	height: 36px!important;
}

@media (max-width: 979px) {
	.bar_item{min-width: 125px;}
}

@media (max-width: 767px) {
	.address_form input,
	.ccm-ui.proform_slider input[type=text],
	.ccm-ui.proform_slider input.hasDatepicker,
	.ccm-ui.proform_slider textarea,
	.ccm-ui.proform_slider select,
	.ccm-ui.proform_slider .uneditable-input{
		height: 28px!important;
		width: 280px;
	}
	.bar_item{min-width: 75px;}
	.bar_item .step_name{display: none;}
}
</style>
<div class="ccm-ui proform_slider" id="proforms_form">
	<div id="form_progress"></div>
	<div class="clearfix"></div>
	<input type="hidden" name="question_set" value="<?php   echo $question_set?>"/>
	<?php    if($ProformsItemID){ ?>
		<input type="hidden" name="ProformsItemID" value="<?php   echo $ProformsItemID?>"/>
		<?php    $pfo = ProFormsItem::getByID($ProformsItemID);?>
	<?php    } ?>
	<?php   
	$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
	foreach ($setAttribs as $ak) {
		if($pfo){$value = $pfo->getAttributeValueObject($ak);}
		$count += 1;
		if(($ak->getReviewStatus() > 0 || strpos($ak->getAttributeType()->getAttributeTypeHandle(), '_display') > 0 )){
	
			//load up conditions code
			Loader::packageElement('attribute/conditions','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			
			//if this is multipart, add hidden input
			if($ak->getTypeMultipart()){
				$multipart = true;
				echo $fm->hidden('multipart',1);
			}
			
			if(!$ak->isAttributeKeyConditioned()){ 
				//load up form input
				Loader::packageElement('attribute/attribute_form','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			}else{ 
				//or load up blank for conditional
				echo '<div id="conditional_'.$ak->getAttributeKeyID().'"></div>';
			} 

		}
		if($count == count($setAttribs)){
			if($displayCaptcha){
				Loader::packageElement('attribute/captcha','proforms');
			}else{
				echo $fm->hidden('no_captcha',true);
			}
		}
		echo $ak->render('closer');
	}	
	
	
	if(!$button_text){
		$button_text = t('Submit');
	}
	
	if($multipart){
		echo $fm->hidden('multipart',true);
	}
	?>
		<div class="clearfix"></div>
		<div class="clearfix"></div>
		<input type="button" class="btn primary proforms_submit proforms_submit_<?php   echo $bID?>" name="proforms_submit_<?php   echo $bID?>" value="<?php   echo $button_text?>" style="float: right;"/>
		
	</div>
	
	<div class="clearfix"></div>
	<br/><br/>
	<div class="ajax_loader_<?php   echo $bID?> loader_ajax" style="display: none;"><img src="<?php    echo Loader::helper('concrete/urls')->getBlockTypeAssetsURL($bt, 'images/ajax-loader.gif')?>" alt="loading"/> </div>
	<div id="debug"></div>
	<div class="ajax_error_<?php   echo $bID?> ajax_error ccm-ui" style="display: none; clear:both;">
		<div class="alert alert-danger">
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		  	<h4><?php    echo t('There was a problem with your form submission:'); ?></h4>
			<ul>
			
			</ul>
		</div>
	</div>
	<div class="ajax_success_<?php   echo $bID?> ccm-ui"<?php    if(!$success){ echo 'style="display: none;"';}?>>
		<div class="alert alert-success">
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		  	<h4><?php    echo t('Success!'); ?></h4>
		<?php   
		if($thankyouMsg){
			echo $thankyouMsg;
		}else{
			echo t('Your entry has been submitted.  Our team will review your submission!  Thanks Again!');
		}
		?>
		</div>
	</div>
</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){

	$('.proforms_submit_<?php   echo $bID?>').click(function(){


	});
	
	$('.submitit').bind('click tap',function(){
		//$(this).parent().submit();
		$('.proforms_submit_<?php   echo $bID?>').click();
	});
	
	var t = 0;
	var i = 0;
	$('.form_step').each(function(){
		t +=1;
		if(!$('.bar_'+t).attr('rel')){
			var name = $(this).attr('data-name');
			$('#form_progress').append('<div class="bar_item bar_'+t+'" rel="'+t+'"><span class="number">'+t+'</span><div class="step_name">'+name+'</div></div>');
		}
		if(!$(this).attr('alt')){
			i+=1;
			$(this).attr('alt',i);
			$(this).addClass('stepform_'+i);
			if(i==1){
				$(this).show();
				$('.bar_1').addClass('current_slide');
				$('.proform_slider').css('height',($(this).height() + 35));
				$('.form_step').css('width',$('.proform_slider').css('width'));
			}
		}
	});	
	
	$('.submitit').html( $('.proforms_submit').val() );
	$('.proforms_submit').hide();

	$('.bar_item').bind('click tap',function(){
		var current = false;
		var goto_slide = $(this).attr('rel');
		$('.bar_item').each(function(){
			if($(this).hasClass('current_slide')){
				current = true;
			}
			if($(this).attr('rel') == goto_slide && !current){
				var prev_slide = goto_slide;
				var current_slide = $('.current_slide').attr('rel');
				var o_height = $('.stepform_'+current_slide).height();
				var n_height = $('.stepform_'+prev_slide).height();
				var prev = $('.stepform_'+prev_slide);
		
				$('.bar_item').removeClass('current_slide');
				$('.bar_'+prev_slide).addClass('current_slide');
				$('.submitit').hide();
		
				if(o_height > n_height){
					$('.stepform_'+current_slide).hide('slide', {direction: 'right'}, 400);
					prev.show('slide', {
						direction: 'left',
						complete: function(){
							$('.proform_slider').css('height',(n_height + 35));
							$('html, body').animate({
						         
						     }, 500);
						}
					}, 400);
				}else{
					$('.stepform_'+current_slide).hide('slide', {direction: 'right'}, 400);
					$('.proform_slider').css('height',(n_height + 35));
					prev.show('slide', {
						direction: 'left',
						complete: function(){
							$('html, body').animate({
						         
						     }, 500);
						}
					}, 400);
				}
		
				if(prev_slide == 1){
					$('.prev_step').hide();
					$('.next_step').show();
				}else{
					$('.next_step').show();
				}
			}else if($(this).attr('rel') == goto_slide){
				var next_slide = goto_slide;
		
				var current_slide = $('.current_slide').attr('rel');
				var o_height = $('.stepform_'+current_slide).height();
				var n_height = $('.stepform_'+next_slide).height();
				var next = $('.stepform_'+next_slide);
				
				$('.bar_item').removeClass('current_slide');
				$('.bar_'+next_slide).addClass('current_slide');
		
				if(o_height > n_height){
					$('.stepform_'+current_slide).hide('slide', {direction: 'left'}, 400);
					next.show('slide', {
						direction: 'right',
						complete: function(){
							$('.proform_slider').css('height',(n_height + 35));
							$('html, body').animate({
						         
						     }, 500);
						}
					}, 400);
				}else{
					$('.stepform_'+current_slide).hide('slide', {direction: 'left'}, 400);
					$('.proform_slider').css('height',(n_height + 35));
					next.show('slide', {
						direction: 'right',
						complete: function(){
							$('html, body').animate({
						         
						     }, 500);
						}
					}, 400);
				}
		
				if($('.form_step').length == next_slide){
					$('.next_step').hide();
					$('.submitit').show();
				}
				if($('.form_step').length > 1){
					$('.prev_step').show();
				}
			}
		});
	});

	$('a.next_step').bind('click tap',function(e){

		var next_slide = ($(this).parent().attr('alt') * 1) + 1;

		var o_height = $(this).parent().height();
		var n_height = $('.stepform_'+next_slide).height();
		var next = $('.stepform_'+next_slide);
		
		$('.bar_item').removeClass('current_slide');
		$('.bar_'+next_slide).addClass('current_slide');

		if(o_height > n_height){
			$(this).parent().hide('slide', {direction: 'left'}, 400);
			next.show('slide', {
				direction: 'right',
				complete: function(){
					$('.proform_slider').css('height',(n_height + 35));
					$('html, body').animate({
				         
				     }, 500);
				}
			}, 400);
		}else{
			$(this).parent().hide('slide', {direction: 'left'}, 400);
			$('.proform_slider').css('height',(n_height + 35));
			next.show('slide', {
				direction: 'right',
				complete: function(){
					$('html, body').animate({
				         
				     }, 500);
				}
			}, 400);
		}

		if($('.form_step').length == next_slide){
			$('.next_step').hide();
			$('.submitit').show();
		}
		if($('.form_step').length > 1){
			$('.prev_step').show();
		}

	});

	$('a.prev_step').bind('click tap',function(e){
		var prev_slide = ($(this).parent().attr('alt') * 1) - 1;

		var o_height = $(this).parent().height();
		var n_height = $('.stepform_'+prev_slide).height();
		var prev = $('.stepform_'+prev_slide);

		$('.bar_item').removeClass('current_slide');
		$('.bar_'+prev_slide).addClass('current_slide');
		$('.submitit').hide();

		if(o_height > n_height){
			$(this).parent().hide('slide', {direction: 'right'}, 400);
			prev.show('slide', {
				direction: 'left',
				complete: function(){
					$('.proform_slider').css('height',(n_height + 35));
					$('html, body').animate({
				         
				     }, 500);
				}
			}, 400);
		}else{
			$(this).parent().hide('slide', {direction: 'right'}, 400);
			$('.proform_slider').css('height',(n_height + 35));
			prev.show('slide', {
				direction: 'left',
				complete: function(){
					$('html, body').animate({
				         
				     }, 500);
				}
			}, 400);
		}

		if(prev_slide == 1){
			$('.prev_step').hide();
			$('.next_step').show();
		}else{
			$('.next_step').show();
		}

	});
	
		
});
/*]]>*/
</script>