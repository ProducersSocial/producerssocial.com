<?php    
defined('C5_EXECUTE') or die("Access Denied.");
$u = new User();
$fm = Loader::helper('form');

//var_dump($ak);
?>
<form id="comment" class="ccm-ui">
<?php   echo $fm->hidden('user_id',$u->uID)?>
<?php   echo $fm->hidden('avID',$_REQUEST['avID'])?>
<?php      Loader::element('editor_init'); ?>
<?php      Loader::element('editor_config'); ?>
<?php      Loader::element('editor_controls', array('mode'=>'basic')); ?>
<?php     
echo '<div class="comment_text">'.$fm->textarea('user_comment', $user_comment, array('style' => 'width: 85%; font-family: sans-serif;', 'class' => 'ccm-advanced-editor')).'</div>';
?>
<br/>
<?php     $ih = Loader::helper('concrete/interface'); ?>
<?php     print $ih->button(t('Post'), null,'right', 'primary add_post'); ?>
</form>
<script type="text/javascript">
/*<![CDATA[*/
	$('.add_post').bind('click',function(e){
		e.preventDefault();
		var content = tinyMCE.activeEditor.getContent();
		var ProformsItemID = $('#ProformsItemID').val();
		var uID = $('#user_id').val();
		var url = '<?php   echo BASE_URL.Loader::helper('concrete/urls')->getToolsURL('proforms/attributes/comments/save_comment.php','proforms')?>';
		$.ajax({
			url: url,
			data: {
				user_comment: content,
				uID: uID,
				akID: '<?php   echo $_REQUEST['akID']?>',
				avID: '<?php   echo $_REQUEST['avID']?>',
				ProformsItemID: ProformsItemID
			}
		}).done( function(html){
			//console.log(html);
			$.fn.dialog.closeTop();
		});
		return false;
	});
/*]]>*/
</script>
