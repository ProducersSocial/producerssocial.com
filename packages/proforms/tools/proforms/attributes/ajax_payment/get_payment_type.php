<?php 
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/categories/proforms_item','proforms');
Loader::model('proforms_item','proforms');
$payment = ProformsItem::GetByID($_REQUEST['payment_form']);

$asHandle = $payment->getAttribute($_REQUEST['payment_handle'])->valOpt;

$setAttribs = AttributeSet::getByHandle($asHandle)->getAttributeKeys();

	foreach ($setAttribs as $ak) {
		Loader::packageElement('attribute/attribute_form','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value,'review'=>$review));
	}
exit;