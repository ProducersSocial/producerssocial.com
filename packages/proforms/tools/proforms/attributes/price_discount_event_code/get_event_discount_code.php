<?php      
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('attribute/categories/collection');
$jh = Loader::helper('json');
$ak = CollectionAttributeKey::getByID($_REQUEST['akID']);

$db = Loader::db();
$cID = $db->getOne("SELECT eventID FROM btProEventDates WHERE eID = ?",array($_REQUEST['eID']));

$event = Page::getByID($cID);

$vo = $event->getAttributeValueObject($ak);
//var_dump($vo->getAttributeKey()->getController()->getFees());
if(is_object($vo)){
	$codes_array = $vo->getAttributeKey()->getController()->getFees();
	if(is_array($codes_array)){
		foreach($codes_array as $code_vars){
			if($code_vars['code'] == $_REQUEST['code']){
				print $jh->encode(array('type'=>$code_vars['type'],'value'=>$code_vars['value']));
			}
		}
	}
}
