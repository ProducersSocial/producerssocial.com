<?php      
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('attribute/categories/collection');

$ak = CollectionAttributeKey::getByID($_REQUEST['akID']);

$db = Loader::db();
$cID = $db->getOne("SELECT eventID FROM btProEventDates WHERE eID = ?",array($_REQUEST['eID']));

$event = Page::getByID($cID);

$vo = $event->getAttributeValueObject($ak);

if(is_object($vo)){
	$code_vars = $vo->getValue();
	$jh = Loader::helper('json');
	print $jh->encode(array('type'=>$code_vars['type'],'value'=>$code_vars['value']));
}
