<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$db = Loader::db();
$db->execute("update atFileApprove set status='approved' where avID = ?",array($_REQUEST['avID']));

Loader::model('proforms_item','proforms');
$pf = ProformsItem::getByID($_REQUEST['proformsItemID']);
Events::fire('proforms_file_approve', $pf);

print 'success';
exit;