<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
$valt = Loader::helper('validation/token');
Loader::model('proforms_item','proforms');

if($_REQUEST['token']){
	if (!$valt->validate('refund_request', $_REQUEST['token'])) {
		throw new Exception($valt->getErrorMessage());
	}
	
    $Object = ProformsItem::getByID($_REQUEST['ProformsItemID']);
    Events::fire('PROFORMS_REFUND_FORM', $Object);
    
    echo '<div class="alert-message block-message success">'.t('Your refund request has been successfully sent!').'</div>';
    exit;
}else{
    $token = $valt->generate('refund_request');
}
?>
<div class="ccm-ui">
	<form action="/index.php/tools/packages/proforms/proforms/request_refund.php" method="post" id="refundRequest">
		<div class="alert-message block-message info">
		  <a class="close" href="javascript:;" onClick="$(this).parent().hide()">×</a>
		  <p><?php echo t('Please tell us your reason for refund.')?></p>
		  <div class="alert-actions">
		
		  </div>
		</div>
		<div id="refund_response">
    		<textarea name="cancle_order_reason" cols="28" rows="8"></textarea>
    		<br/><br/>
    		<button type="submit" class="btn btn-info" id="refund_submit"><i class="icon icon-white icon-ok"></i> <?php echo t('Yes, Request Refund')?></button>
    		<?php echo $fm->hidden('ProformsItemID',$_REQUEST['ProformsItemID']);?>
    		<?php echo $fm->hidden('token',$token);?>
		</div>
		<br /><br />
		<div id="found_order">
		
		</div>
	</form>
</div>
<script>

   $('#refundRequest').submit(function(e){
       e.preventDefault();
       
       var url = $('#refundRequest').attr('action');
       $.ajax({
          url: url,
          data: $('#refundRequest').serialize(),
          success: function(response){
              $('#refund_response').html(response);
          }
       });
       
       return false;
   }) 

</script>