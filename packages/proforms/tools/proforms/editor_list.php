<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$question_set = $_REQUEST['questionSetID'];
$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
?>
<ul class="sortable droppable">
<?php   
foreach ($setAttribs as $ak) {
	$cnt = $ak->getController();
	$count += 1;
	$multiplex = strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'ultiplex');
	
	if((!method_exists($cnt,'closer') || 
	strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'ultiplex') > 0
	&& strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'idden_') < 1
	&& strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'sociate_') < 1
	)
	&& $ak->getReviewStatus() > 0
	){
	?>
	<li data-key="<?php   echo $ak->getAttributeKeyID()?>" data-type="<?php   echo $ak->getAttributeType()->getAttributeTypeID()?>">
		<?php    if((!$ak->isAttributeKeyLabelHidden() && $ak->getReviewStatus() > 0) || $multiplex > 0){ ?>
		<label><?php    echo $ak->render('label');?></label>
		<?php    } ?>
		<div class="input">
			<div class="proform-attributes <?php   echo $ak->getAttributeType()->getAttributeTypeHandle()?>">
				<i class="icon-trash delete pull-right"></i>
				<i class="icon-edit edit pull-right"></i>
				<i class="icon-move move pull-right"></i>
				<div class="editable_form">
				<?php   
				echo $ak->render('form');
				?>
				</div>
			</div>
		</div>
		<div class="dropable"></div>
	</li>
	<?php   
	}elseif(strpos($ak->getAttributeType()->getAttributeTypeHandle(), '_display') > 0 || strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'idden_') > 0){
	?>
	<li data-key="<?php   echo $ak->getAttributeKeyID()?>" data-type="<?php   echo $ak->getAttributeType()->getAttributeTypeID()?>">
		<div class="input">
			<div class="proform-attributes">
				<i class="icon-trash delete pull-right"></i>
				<i class="icon-edit edit pull-right"></i>
				<i class="icon-move move pull-right"></i>
				<div class="hidden_method"><?php   echo $ak->getAttributeKeyName()?></div>
			</div>
		</div>
		<div class="dropable"></div>
	</li>
	<?php   
	}else{
	?>
	<li data-key="<?php   echo $ak->getAttributeKeyID()?>" data-type="<?php   echo $ak->getAttributeType()->getAttributeTypeID()?>">
		<div class="input">
			<div class="proform-attributes">
				<i class="icon-trash delete pull-right"></i>
				<i class="icon-edit edit pull-right"></i>
				<i class="icon-move move pull-right"></i>
				<div class="closer_method"><?php   echo $ak->getAttributeKeyName()?></div>
			</div>
		</div>
		<div class="dropable"></div>
	</li>
	<?php   
	}
}
?>
</ul>