<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
/**
 * AuthorizeNetException.php
 *
 * @package AuthorizeNet
 */

/**
 * Exception class for AuthorizeNet PHP SDK.
 *
 * @package AuthorizeNet
 */
class AuthorizeNetException extends Exception
{
}
