<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 
class DashboardPronewsAddNewsController extends Controller {
	

	public $helpers = array('html','form');
	
	public function on_start() {
		Loader::model('page_list');
		$this->error = Loader::helper('validation/error');
	}
	
	public function view() {
		$this->setupForm();
		$this->loadnewsSections();
		$newsList = new PageList();
		$newsList->sortBy('cDateAdded', 'desc');
		if (isset($_GET['cParentID']) && $_GET['cParentID'] > 0) {
			$newsList->filterByParentID($_GET['cParentID']);
		} else {
			$sections = $this->get('sections');
			$keys = array_keys($sections);
			$keys[] = -1;
			$newsList->filterByParentID($keys);
		}
	}
	

	protected function loadnewsSections() {
		$newsSectionList = new PageList();
		$newsSectionList->filterByNewsSection(1);
		$newsSectionList->sortBy('cvName', 'asc');
		$tmpSections = $newsSectionList->get();
		$sections = array();
		foreach($tmpSections as $_c) {
			$p = new Permissions($_c);
			if($p->canAddSubContent()){
				$this->pt = $p;
				$sections[$_c->getCollectionID()] = $_c->getCollectionName();
			}
		}
		$this->set('sections', $sections);
	}


	public function edit($cID) {
		$this->setupForm();
		$news = Page::getByID($cID);
		
		if ($this->isPost()) {
			$this->validate();
			if (!$this->error->has()) {
				$p = Page::getByID($this->post('newsID'));
				$parent = Page::getByID($this->post('cParentID'));
				$ct = CollectionType::getByID($this->post('ctID'));				
				$data = array('ctID' =>$ct->getCollectionTypeID(), 'cDescription' => $this->post('newsDescription'), 'cName' => $this->post('newsTitle'), 'cDatePublic' => Loader::helper('form/date_time')->translate('newsDate'));
				$p->update($data);
				if ($p->getCollectionParentID() != $parent->getCollectionID()) {
					$p->move($parent);
				}
				$this->saveData($p);
				$this->redirect('/dashboard/pronews/list/', 'news_updated');
			}
		}
		
		$sections = $this->get('sections');
		if (in_array($news->getCollectionParentID(), array_keys($sections))) {
			$this->set('news', $news);	
		} else {
			$this->redirect('/dashboard/pronews/add_news/');
		}

	}

	protected function setupForm() {
		$this->loadnewsSections();
		Loader::model("collection_types");
		$ctArray = CollectionType::getList('');
		$pageTypes = array();
		foreach($ctArray as $ct) {
			if($this->pt && $this->pt->canAddSubCollection($ct) != 0){
				$pageTypes[$ct->getCollectionTypeID()] = $ct->getCollectionTypeName();		
			}
		}
		$this->set('pageTypes', $pageTypes);
		$this->addHeaderItem(Loader::helper('html')->javascript('tiny_mce/tiny_mce.js'));
	}

	public function add() {
		$this->setupForm();
		if ($this->isPost()) {
			$this->validate();
			if (!$this->error->has()) {
				$parent = Page::getByID($this->post('cParentID'));
				$ct = CollectionType::getByID($this->post('ctID'));				
				$data = array('cName' => $this->post('newsTitle'), 'cDescription' => $this->post('newsDescription'), 'cDatePublic' => Loader::helper('form/date_time')->translate('newsDate'));
				$p = $parent->add($ct, $data);	
				$this->saveData($p);
				$this->redirect('/dashboard/pronews/list/', 'news_added');
			}
		}
	}

	protected function validate() {
		$vt = Loader::helper('validation/strings');
		$vn = Loader::Helper('validation/numbers');
		$dt = Loader::helper("form/date_time");
		if (!$vn->integer($this->post('cParentID'))) {
			$this->error->add(t('You must choose a parent page for this news entry.'));
		}			

		if (!$vn->integer($this->post('ctID'))) {
			$this->error->add(t('You must choose a page type for this news entry.'));
		}			
		
		if (!$vt->notempty($this->post('newsTitle'))) {
			$this->error->add(t('Title is required'));
		}

		Loader::model("attribute/categories/collection");

		
		$akct = CollectionAttributeKey::getByHandle('news_category');
		$ctKey = $akct->getAttributeKeyID();
		foreach($this->post(akID) as $key => $value){
			if($key==$ctKey){
				foreach($value as $type => $values){
					if($type=='atSelectNewOption'){
						foreach($values as $cat => $valued){
							if($valued==''){
								$this->error->add(t('Categories must have a value'));	
							}
						}
					}
				}
			}
		}

		if (!$this->error->has()) {
			Loader::model('collection_types');
			$ct = CollectionType::getByID($this->post('ctID'));				
			$parent = Page::getByID($this->post('cParentID'));				
			$parentPermissions = new Permissions($parent);
			if (!$parentPermissions->canAddSubCollection($ct)) {
				$this->error->add(t('You do not have permission to add a page of that type to that area of the site.'));
			}
		}
		
		if($this->error->has()){
			if($this->post('newsID')!=''){
				$mode = 'edit/'.$this->post('newsID').'/';
			}else{
				$mode = '';
			}
			$this->set('message', t('<a href="'.BASE_URL.'/index.php/dashboard/pronews/add_news/'.$mode.'"><input type="button" value="continue"/></a>'));
		}
	}
	private function saveData($p) {
	
	
		$blocks = $p->getBlocks('Main');
		foreach($blocks as $b) {
			$b->deleteBlock();
		}
		
		Loader::model("attribute/categories/collection");
		$cak = CollectionAttributeKey::getByHandle('news_tag');
		$cak->saveAttributeForm($p);	
		
		$cck = CollectionAttributeKey::getByHandle('meta_title');
		$cck->saveAttributeForm($p);
		
		$cck = CollectionAttributeKey::getByHandle('meta_description');
		$cck->saveAttributeForm($p);
		
		$cck = CollectionAttributeKey::getByHandle('meta_keywords');
		$cck->saveAttributeForm($p);
		
		$cck = CollectionAttributeKey::getByHandle('news_category');
		$cck->saveAttributeForm($p);
		
		$cnv = CollectionAttributeKey::getByHandle('exclude_nav');
		$cnv->saveAttributeForm($p);
		
		$ct = CollectionAttributeKey::getByHandle('thumbnail');
		$ct->saveAttributeForm($p);
		
		$cur = CollectionAttributeKey::getByHandle('news_url');
		$cur->saveAttributeForm($p);
			
		
		$bt = BlockType::getByHandle('content');
		
		$data = array('content' => $this->post('newsBody'));			
					
		$b = $p->addBlock($bt, 'Main', $data);
		$b->setCustomTemplate('news_post');

		$p->reindex();
			
	}

	
	public function on_before_render() {
		$this->set('error', $this->error);
	}
	
}