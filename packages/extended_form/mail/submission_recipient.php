<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

$submittedData='';
foreach($questionAnswerPairs as $questionAnswerPair) {
	if ($questionAnswerPair['type'] == 'line') {
		$submittedData .= $questionAnswerPair['question']."\r\n\r\n";
	}elseif ($questionAnswerPair['type'] == 'text') {
		$submittedData .= $questionAnswerPair['question']."\r\n\r\n";
	}elseif ($questionAnswerPair['type'] == 'space' || $questionAnswerPair['type'] == 'hr') {
		$submittedData .= "\r\n";	
	}else{
		$submittedData .= $questionAnswerPair['question'].": ".$questionAnswerPair['answer']."\r\n\r\n";
	}
} 

$body = t("You succesfully sent our extendedform %s on our Concrete5 website.
The following information was sent to us.

%s

Thank you!

", $formName, $submittedData);