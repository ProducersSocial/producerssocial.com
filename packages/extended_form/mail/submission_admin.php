<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

$submittedData='';
foreach($questionAnswerPairs as $questionAnswerPair) {
	if ($questionAnswerPair['type'] == 'line') {
		$submittedData .= $questionAnswerPair['question']."\r\n\r\n";
	}elseif ($questionAnswerPair['type'] == 'text') {
		$submittedData .= $questionAnswerPair['question']."\r\n\r\n";
	}elseif ($questionAnswerPair['type'] == 'space' || $questionAnswerPair['type'] == 'hr') {
		$submittedData .= "\r\n";	
	}else{
		$submittedData .= $questionAnswerPair['question'].": ".$questionAnswerPair['answer']."\r\n\r\n";
	}
}  
$formDisplayUrl=BASE_URL.DIR_REL.'/index.php/dashboard/reports/extended_forms/?qsid='.$questionSetId;

$body = t("There has been a submission of the extendedform %s through your Concrete5 website.

%s

To view all of this form's submissions, visit %s 

", $formName, $submittedData, $formDisplayUrl);