<?php   defined('C5_EXECUTE') or die("Access Denied.");
/* @var $h ConcreteDashboardHelper */
$h = Loader::helper('concrete/dashboard');
/* @var $ih ConcreteInterfaceHelper */
$ih = Loader::helper('concrete/interface');
/* @var $nh NavigationHelper */
$nh = Loader::helper('navigation');
/* @var $text TextHelper */
$text = Loader::helper('text');
/* @var $dh DateHelper*/
$dh = Loader::helper('date');
/* @var $urlhelper UrlHelper */
$urlhelper = Loader::helper('url');
/* @var $json JsonHelper */
$json = Loader::helper('json');
/* @var $db DataBase */
$db = Loader::db();
?>
<script>
jQuery(function($) {
	var deleteResponse = (<?php  echo $json->encode(t('Are you sure you want to delete this form submission?'))?>),
		deleteForm = (<?php  echo $json->encode(t('Are you sure you want to delete this form and its form submissions?'))?>),
		deleteFormAnswers = (<?php  echo $json->encode(t('Are you sure you want to delete this form submissions?'))?>);
	$('.delete-response').live('click', function(e) {
		if (!confirm(deleteResponse)) {
			e.preventDefault();
		}
	});
	$('.delete-form').live('click', function(e) {
		if (!confirm(deleteForm)) {
			e.preventDefault();
		}
	});
	$('.delete-form-answers').live('click', function(e) {
		if (!confirm(deleteFormAnswers)) {
			e.preventDefault();
		}
	});
});
</script>
<style>
	::-webkit-scrollbar {
	-webkit-appearance: none;
	width: 7px;
	height: 6px;
}
::-webkit-scrollbar-thumb {
	border-radius: 4px;
	background-color: rgba(0,0,0,.5);
	-webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
}

#wide-content-notification {
	margin-left: 5px;
	display: none;
	color: #aaa;
}

.form-results-container {
	width: 100%;
	overflow: auto;
}

</style>
<?php  if(!isset($questionSet)):?>
<?php  echo $h->getDashboardPaneHeaderWrapper(t('Form Results'));?>
<?php  
$showTable = false;
foreach ($surveys as $qsid => $survey) {
	$block = Block::getByID((int) $survey['bID']);
	if (is_object($block)) {
		$showTable = true;
		break;
	}
}

if ($showTable) { ?>

<table class="table table-striped">
	<thead>
		<tr>
			<th><?php   echo t('Form')?></th>
			<th><?php   echo t('Submissions')?></th>
            <th><?php   echo t('Rating')?></td>
			<th><?php   echo t('Options')?></th>
		</tr>
	</thead>
	<tbody>
		<?php   foreach ($surveys as $qsid => $survey):
		$block = Block::getByID((int) $survey['bID']);
		if (!is_object($block)) {
			continue;
		}
		$in_use = (int) $db->getOne(
			'SELECT count(*)
			FROM CollectionVersionBlocks
			INNER JOIN Pages
			ON CollectionVersionBlocks.cID = Pages.cID
			INNER JOIN CollectionVersions
			ON CollectionVersions.cID = Pages.cID
			WHERE CollectionVersions.cvIsApproved = 1
			AND CollectionVersionBlocks.cvID = CollectionVersions.cvID
			AND CollectionVersionBlocks.bID = ?',
			array($block->bID)
		);
		$url = $nh->getLinkToCollection($block->getBlockCollectionObject());
?>
		<tr>
			<td><?php  echo $text->entities($survey['surveyName'])?></td>
			<td><?php  echo $text->entities($survey['answerSetCount'])?></td>
            <td><?php  echo $survey['ratingTotal']; ?></td>
			<td>
				<?php  echo $ih->button(t('View Responses'), DIR_REL . '/index.php?cID=' . $c->getCollectionID().'&qsid='.$qsid, 'left', 'small')?>
				<?php  echo $ih->button(t('Open Page'), $url, 'left', 'small')?>
				<?php  echo $ih->button(t('Delete Submissions'), $this->action('').'?qsID='.$qsid.'&action=deleteFormAnswers', 'left', 'small error delete-form-answers')?>
				<?php  if(!$in_use):?>
				<?php  echo $ih->button(t('Delete'), $this->action('').'?bID='.$survey['bID'].'&qsID='.$qsid.'&action=deleteForm', 'left', 'small error delete-form')?>
				<?php  endif?>
			</td>
		</tr>
		<?php  endforeach?>
	</tbody>
</table>
<?php   } else { ?>
	<p><?php  echo t('There are no available forms in your site.')?></p>
<?php   } ?>
<?php  echo $h->getDashboardPaneFooterWrapper();?>
<?php  else:?>
<?php  echo $h->getDashboardPaneHeaderWrapper(t('Responses to %s', $surveys[$questionSet]['surveyName']), false, false, false);?>
<div class="ccm-pane-body <?php   if(!$paginator || !strlen($paginator->getPages())>0){ ?> ccm-pane-body-footer <?php   } ?>">
<?php  if(count($answerSets) == 0):?>
	<div><?php  echo t('No one has yet submitted this form.')?></div>
	<?php  else:?>

	<div class="ccm-list-action-row">
		<a id="ccm-export-results" href="<?php  echo $this->action('excel', '?qsid=' . $questionSet)?>"><span></span><?php  echo t('Export to Excel')?></a>
	</div>

	<div class="form-results-container">
		<script>
		$(document).ready(function(){
			if($('.form-results-container')[0].scrollWidth > $('.ccm-pane-body').width()) {
				$('#wide-content-notification').show();
			}
		});
		</script>
		<p id="wide-content-notification"><?php   echo t('* Scroll right to view full results'); ?></p>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php   if($_REQUEST['sortBy']=='chrono') { ?>
					<th class="header headerSortDown">
						<a href="<?php  echo $text->entities($urlhelper->unsetVariable('sortBy'))?>">
					<?php   } else { ?>
					<th class="header headerSortUp">
						<a href="<?php  echo $text->entities($urlhelper->setVariable('sortBy', 'chrono'))?>">
					<?php   } ?>
						<?php  echo t('Date')?>
						</a>
					</th>
					<?php  foreach($questions as $question):?>
                    	<?php  if ($question['inputType'] != 'line' && $question['inputType'] != 'text' && $question['inputType'] != 'hr' && $question['inputType'] != 'space' && $question['inputType'] != 'fieldset-start' && $question['inputType'] != 'fieldset-end') { ?>
                    	<th><?php  echo $question['question']?></th>
                        <?php  } ?>
                    <?php  endforeach?>
        			
					 <?php  if($surveys[$questionSet]['capturePageID'] != '') { ?>			
                     	<th><?php  echo t('Page'); ?></th>
                     <?php  } ?>
                     
                     <?php  if($surveys[$questionSet]['captureIP'] != '') { ?>			
                     	<th><?php  echo t('IP'); ?></th>
                     <?php  } ?>
                     
                     
                     <?php  if($surveys[$questionSet]['captureUser'] != '') { ?>			
                     	<th><?php  echo t('User'); ?></th>
                     <?php  } ?>
                     
					<th><?php  echo t('Actions')?></th>
				</tr>
			</thead>
			<tbody>
		<?php  foreach($answerSets as $answerSetId => $answerSet):?>
				<tr>
					<td>
		<?php  echo $dh->getSystemDateTime($answerSet['created'])?></td>
		<?php  foreach($questions as $questionId => $question):
					if ($question['inputType'] == 'fileupload') {
						$fID = (int) $answerSet['answers'][$questionId]['answer'];
						$file = File::getByID($fID);
						if ($fID && $file) {
							$fileVersion = $file->getApprovedVersion();
							echo '<td><a href="' . $fileVersion->getRelativePath() .'">'.
								$text->entities($fileVersion->getFileName()).'</a></td>';
						} else {
							echo '<td>'.t('File not found').'</td>';
						}
					}
					elseif ($question['inputType'] != 'line' && $question['inputType'] != 'text' && $question['inputType'] != 'hr' && $question['inputType'] != 'space' && $question['inputType'] != 'fieldset-start' && $question['inputType'] != 'fieldset-end') 
					{	
						if($question['inputType']=='textarea'){
							echo '<td>'.$text->entities($answerSet['answers'][$questionId]['answerLong']).'</td>';
						} else {
							echo '<td>'.$text->entities($answerSet['answers'][$questionId]['answer']).'</td>';
						}
					}

		endforeach?>
        
				<?php  if($surveys[$questionSet]['capturePageID'] != '') { 
                    $page = Page::getByID($answerSet['answers'][9996]['answer']); if (is_object($page)) { ?>
                        <td><a href="<?php    echo DIR_REL . $page->getCollectionPath() ?>" target="_blank"><?php    echo $page->getCollectionName() ?></a></td>
  				<?php  } else { ?>
                <td>&nbsp;</td>
                <?php  } ?>                 
                <?php  } ?>
                        
                <?php  if($surveys[$questionSet]['captureIP'] != '') { ?>
                        <td><?php  echo $answerSet['answers'][9997]['answer']; ?></td>
                <?php  } else { ?>
                	<td><?php  echo t('Unknown'); ?></td>
				<?php  } ?>
                       
                <?php  if($surveys[$questionSet]['captureUser'] != '') { ?>
                        <?php  $user = User::getByUserID($answerSet['answers'][9999]['answer']); 
						if (is_object($user)) { ?><td><a href="<?php    echo $this->url('/dashboard/users/search?uID=' . $answerSet['answers'][9998]['answer']) ?>"><?php    echo $answerSet['answers'][9999]['answer']; ?></a></td>
                <?php  } else { ?>
                	<td><?php  echo t('Guest'); ?></td>
				<?php  } } ?>
                
					<td>
						<?php  echo $ih->button(
							t("Delete"),
							$this->action('').'?qsid='.$answerSet['questionSetId'].'&asid='.$answerSet['asID'].'&action=deleteResponse',
							'left',
							'danger delete-response small'
						)?>
					</td>
				</tr>
		<?php  endforeach?>
			</tbody>
		</table>
	</div>
</div>
<?php   if($paginator && strlen($paginator->getPages())>0){ ?>	 
<div class="ccm-pane-footer">
	<div class="pagination">
	  <ul>
		  <li class="prev"><?php  echo $paginator->getPrevious()?></li>
		  
		  <?php   // Call to pagination helper's 'getPages' method with new $wrapper var ?>
		  <?php  echo $paginator->getPages('li')?>
		  
		  <li class="next"><?php  echo $paginator->getNext()?></li>
	  </ul>
	</div>
</div>
<?php   } ?>		
<?php  endif?>
<?php  echo $h->getDashboardPaneFooterWrapper(false);?>
<?php  endif?>
