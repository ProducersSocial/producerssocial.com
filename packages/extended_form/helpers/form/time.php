<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
class FormTimeHelper {

	/** 
	 * Creates form fields
	 * <code>
	 *     $dh->time('yourStartDate', '2008-07-12 3:00:00');
	 * </code>
	 * @param string $prefix
	 * @param string $value
	 */
	public function time($prefix, $value = null) {
		if (substr($prefix, -1) == ']') {
			$prefix = substr($prefix, 0, strlen($prefix) -1);
			$_activate = $prefix . '_activate]';
			$_h = $prefix . '_h]';
			$_m = $prefix . '_m]';
			$_a = $prefix . '_a]';
		} else {
			$_activate = $prefix . '_activate';
			$_h = $prefix . '_h';
			$_m = $prefix . '_m';
			$_a = $prefix . '_a';
		}
		
		$dfh = (DATE_FORM_HELPER_FORMAT_HOUR == '12') ? 'h' : 'H';
		$dfhe = (DATE_FORM_HELPER_FORMAT_HOUR == '12') ? '12' : '23';
		$dfhs = (DATE_FORM_HELPER_FORMAT_HOUR == '12') ? '1' : '0';
		if ($value != null) {
			$h = date($dfh, strtotime($value));
			$m = date('i', strtotime($value));
			$a = date('A', strtotime($value));
		} else {
			$h = date($dfh);
			$m = date('i');
			$a = date('A');
		}
		$id = preg_replace("/[^0-9A-Za-z-]/", "_", $prefix);
		$html = '';
		$html .= '<span class="ccm-input-time-wrapper" id="' . $id . '_tw">';
		$html .= '<select id="' . $id . '_h" name="' . $_h . '" ' . $disabled . '>';
		for ($i = $dfhs; $i <= $dfhe; $i++) {
			if ($h == $i) {
				$selected = 'selected';
			} else {
				$selected = '';
			}
			$html .= '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
		}
		$html .= '</select>:';
		$html .= '<select id="' . $id . '_m" name="' . $_m . '" ' . $disabled . '>';
		for ($i = 0; $i <= 59; $i++) {
			if ($m == sprintf('%02d', $i)) {
				$selected = 'selected';
			} else {
				$selected = '';
			}
			$html .= '<option value="' . sprintf('%02d', $i) . '"' . $selected . '>' . sprintf('%02d', $i) . '</option>';
		}
		$html .= '</select>';
		if (DATE_FORM_HELPER_FORMAT_HOUR == '12') {
			$html .= '<select id="' . $id . '_a" name="' . $_a . '" ' . $disabled . '>';
			$html .= '<option value="AM" ';
			if ($a == 'AM') {
				$html .= 'selected';
			}
			$html .= '>AM</option>';
			$html .= '<option value="PM" ';
			if ($a == 'PM') {
				$html .= 'selected';
			}
			$html .= '>PM</option>';
			$html .= '</select>';
		}
		$html .= '</span>';
		
		return $html;
	
	}
}
