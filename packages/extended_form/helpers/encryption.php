<?php    
/**
 * @package Helpers
 * @category Concrete
 * @author Andrew Embler <andrew@concrete5.org>
 * @copyright  Copyright (c) 2003-2008 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 */

/**
 * Functions used to send mail in Concrete.
 * @package Helpers
 * @category Concrete
 * @author Andrew Embler <andrew@concrete5.org>
 * @copyright  Copyright (c) 2003-2008 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 */
 
defined('C5_EXECUTE') or die(_("Access Denied."));
class encryptionHelper {
	
	private $password = NULL;
 
	public function set_key($password) {
		$this->password = $password;
	}
 
	function encrypt($string) {
        return @base64_encode(@gzcompress($string.$this->password, 9));
	}
	 
	function decrypt($string) {
		return eregi_replace($this->password, '' ,@gzuncompress(@base64_decode($string)));
	}
}

?>