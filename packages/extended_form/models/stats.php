<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));


/**
 * Namespace for statistics-related functions used by the form block.
 *
 * @package Blocks
 * @subpackage BlockTypes
 * @author Tony Trupp <tony@concrete5.org>
 * @copyright  Copyright (c) 2003-2008 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 *
 */
class ExtendedFormBlockStatistics {
	
	public static function getTotalSubmissions($date = null) {
		$db = Loader::db();
		if ($date != null) {
			return $db->GetOne("select count(asID) from btExtendedFormAnswerSet where DATE_FORMAT(created, '%Y-%m-%d') = ?", array($date));
		} else {
			return $db->GetOne("select count(asID) from btExtendedFormAnswerSet");
		}

	}
	
	public static function loadSurveys($ExtendedFormSurvey){  
		$db = Loader::db();
		return $db->query('SELECT s.* FROM '.$ExtendedFormSurvey->btTable.' AS s, Blocks AS b, BlockTypes AS bt '.
						  'WHERE s.bID=b.bID AND b.btID=bt.btID AND bt.btHandle="extended_form" ' );
	}
	
	public static function loadSurvey($ExtendedFormSurvey, $fID){  
		$db = Loader::db();
		return $db->query('SELECT s.* FROM '.$ExtendedFormSurvey->btTable.' AS s, Blocks AS b, BlockTypes AS bt '.
						  'WHERE s.bID=' . $fID . ' AND s.bID=b.bID AND b.btID=bt.btID AND bt.btHandle="extended_form" ' );
	}
	
	public static $sortChoices=array('newest'=>'created DESC','chrono'=>'created');
	
	public static function buildAnswerSetsArray( $questionSet, $orderBy='', $limit='' ){
		$db = Loader::db();
		
		if( strlen(trim($limit))>0 && !strstr(strtolower($limit),'limit')  )
			$limit=' LIMIT '.$limit;
			
		if( strlen(trim($orderBy))>0 && array_key_exists($orderBy, self::$sortChoices) ){
			 $orderBySQL=self::$sortChoices[$orderBy];
		}else $orderBySQL=self::$sortChoices['newest'];
		
		//get answers sets
		$sql='SELECT * FROM btExtendedFormAnswerSet AS aSet '.
			 'WHERE aSet.questionSetId='.$questionSet.' ORDER BY '.$orderBySQL.' '.$limit;
		$answerSetsRS=$db->query($sql);
		//load answers into a nicer multi-dimensional array
		$answerSets=array();
		$answerSetIds=array(0);
		while( $answer = $answerSetsRS->fetchRow() ){
			//answer set id - question id
			$answerSets[$answer['asID']]=$answer;
			$answerSetIds[]=$answer['asID'];
		}		
		
		//get answers
		$sql='SELECT * FROM btExtendedFormAnswers AS a WHERE a.asID IN ('.join(',',$answerSetIds).') ORDER BY aID';
				
		$answersRS=$db->query($sql);
		
		//load answers into a nicer multi-dimensional array 
		while( $answer = $answersRS->fetchRow() ){
			//answer set id - question id
			$answerSets[$answer['asID']]['answers'][$answer['msqID']] = $answer;
		}
	
		return $answerSets;
	}
}