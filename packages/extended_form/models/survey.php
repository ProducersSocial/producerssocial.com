<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * Namespace for other functions used by the form block.
 *
 * @package Blocks
 * @subpackage BlockTypes
 * @author Tony Trupp <tony@concrete5.org>
 * @copyright  Copyright (c) 2003-2008 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 *
 */
class ExtendedFormSurvey{

		public $btTable = 'btExtendedForm';
		public $btQuestionsTablename = 'btExtendedFormQuestions';
		public $btAnswerSetTablename = 'btExtendedFormAnswerSet';
		public $btAnswersTablename = 'btExtendedFormAnswers'; 	
		
		public $lastSavedMsqID=0;
		public $lastSavedqID=0;
		
		public $bID = 0;
		
		function __construct(){
			$db = Loader::db();
			$this->db=$db;
		}

		function addEditQuestion($values,$withOutput=1){
			$jsonVals=array();
			$values['options']=str_replace(array("\r","\n"),'%%',$values['options']); 
			if(strtolower($values['inputType'])=='undefined')  
				$values['inputType']='field';
			
			//set question set id, or create a new one if none exists
			if(intval($values['qsID'])==0) 
				$values['qsID']=time(); 
			
			//validation
			if( strlen($values['question'])==0 || strlen($values['inputType'])==0  || $values['inputType']=='null' ){
				//complete required fields
				$jsonVals['success']=0;
				$jsonVals['noRequired']=1;
			}else{
				
				if( intval($values['msqID']) ){
					$jsonVals['mode']='"Edit"';
					
					//questions that are edited are given a placeholder row in btExtendedFormQuestions with bID=0, until a bID is assign on block update
					$pendingEditExists = $this->db->getOne( "select count(*) as total from btExtendedFormQuestions where bID=0 AND msqID=".intval($values['msqID']) );
					
					//hideQID tells the interface to hide the old version of the question in the meantime
					$vals=array( intval($values['msqID'])); 		
					$jsonVals['hideQID']=intval($this->db->GetOne("SELECT MAX(qID) FROM btExtendedFormQuestions WHERE bID!=0 AND msqID=?",$vals));	
				}else{
					$jsonVals['mode']='"Add"';
				}
				
				$width = $height = 0;
				if ($values['inputType'] == 'textarea'){
					$width  = $this->limitRange(intval($values['width']), 20, 500);
					$height = $this->limitRange(intval($values['height']), 1, 100); 
				} elseif ($values['inputType'] == 'tinymce'){
                    $width  = $this->limitRange(intval($values['width']), 20, 500);
                    $height = $this->limitRange(intval($values['height']), 1, 100);
				} elseif ($values['inputType'] == 'email'){
					//$values['other'] = $values['emailcopy'];
					$values['other'] = $values['emailcopy'] + 2 * $values['emailvalidate'];
				} elseif ($values['inputType'] == 'datetime'){
					$values['options'] = $values['answerOptionsDate'];
				} elseif ($values['inputType'] == 'text'){
					$values['options'] = $values['textarea'];
				} elseif ($values['inputType'] == 'hidden'){
					$values['options'] = $values['hidden'];
				} elseif ($values['inputType'] == 'fieldset-start'){
					$values['options'] = $values['legend'];
				} elseif ($values['inputType'] == 'rating'){
					$values['options'] = $values['stars'];
				} elseif ($values['inputType'] == 'integer'){
					$values['options'] = $values['integerOptions'];	
				} elseif ($values['inputType'] == 'recipient'){
					$values['options']=str_replace(array("\r","\n"),'%%',$values['recipientOptions']); 
				}
					
				if( $pendingEditExists ){ 
								
					$dataValues=array(intval($values['qsID']), trim($values['question']), $values['inputType'],
								      $values['options'], intval($values['position']), $width, $height, intval($values['required']), 
									  intval($values['other']), $values['otheroptions'], intval($values['tooltip']), $values['tooltipText'], $values['customClass'], intval($values['msqID']) );			
					$sql='UPDATE btExtendedFormQuestions SET questionSetId=?, question=?, inputType=?, options=?, position=?, width=?, height=?, required=?, other=?, otherTitle=?, tooltip=?, tooltipText=?, customClass=? WHERE msqID=? AND bID=0';					
				}else{ 
					if( !isset($values['position']) || strlen($values['position'])==0) {
					$values['position'] = intval($this->db->GetOne("SELECT MAX(position) FROM btExtendedFormQuestions WHERE questionSetId=?", intval($values['qsID'])));
					}
					if(!intval($values['msqID']))
						$values['msqID']=intval($this->db->GetOne("SELECT MAX(msqID) FROM btExtendedFormQuestions")+1); 
					$dataValues=array($values['msqID'],intval($values['qsID']), trim($values['question']), $values['inputType'],
								     $values['options'], intval($values['position']), intval($values['width']), intval($values['height']), intval($values['required']), 
									 intval($values['other']), $values['otheroptions'], intval($values['tooltip']), $values['tooltipText'], $values['customClass']);			
					$sql='INSERT INTO btExtendedFormQuestions (msqID,questionSetId,question,inputType,options,position,width,height,required,other,otherTitle,tooltip,tooltipText,customClass) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)'; 
				}
				$result=$this->db->query($sql,$dataValues);  
				$this->lastSavedMsqID=intval($values['msqID']);	
				$this->lastSavedqID=intval($this->db->GetOne("SELECT MAX(qID) FROM btExtendedFormQuestions WHERE bID=0 AND msqID=?", array($values['msqID']) ));
				$jsonVals['qID']=$this->lastSavedqID;
				$jsonVals['success']=1;
			}
			
			$jsonVals['qsID']=$values['qsID'];
			$jsonVals['msqID']=intval($values['msqID']);
			//create json response object
			$jsonPairs=array();
			foreach($jsonVals as $key=>$val) $jsonPairs[]=$key.':'.$val;
			if($withOutput) echo '{'.join(',',$jsonPairs).'}';
		}
		
		function getQuestionInfo($qsID,$qID){
			$questionRS=$this->db->query('SELECT * FROM btExtendedFormQuestions WHERE questionSetId='.intval($qsID).' AND qID='.intval($qID).' LIMIT 1' );
			$questionRow=$questionRS->fetchRow();
			$jsonPairs=array();
			if ($questionRow['inputType'] == 'email') {
				//$questionRow['emailcopy'] = $questionRow['other']; 
				$otherval = $questionRow['other'];
				$isEmailCopy = $otherval % 2;
				$isEmailValidate = (int)($otherval / 2);
				$questionRow['emailcopy'] = $isEmailCopy;
				$questionRow['emailvalidate'] = $isEmailValidate;

				$questionRow['other'] = 0;
			}
			elseif ($questionRow['inputType'] == 'datetime') 
			{
				$questionRow['answerOptionsDate'] = $questionRow['options']; 
				$questionRow['options'] = '';
			}
			elseif ($questionRow['inputType'] == 'text') 
			{
				$questionRow['textarea'] = $questionRow['options']; 
				$questionRow['options'] = '';
			}
			elseif ($questionRow['inputType'] == 'hidden') 
			{
				$questionRow['hidden'] = $questionRow['options']; 
				$questionRow['options'] = '';
			}
			elseif ($questionRow['inputType'] == 'rating') 
			{
				$questionRow['stars'] = $questionRow['options']; 
				$questionRow['options'] = '';
			}
			elseif ($questionRow['inputType'] == 'integer') 
			{
				$questionRow['integer'] = $questionRow['options']; 
				$questionRow['options'] = '';
			}
			elseif ($questionRow['inputType'] == 'fieldset-start') 
			{
				$questionRow['legend'] = $questionRow['options']; 
				$questionRow['options'] = '';
			}
			
			foreach($questionRow as $key=>$val){
				if($key=='options') $key='optionVals';
				$jsonPairs[]=$key.':"'.str_replace(array("\r","\n"),'%%',addslashes($val)).'"';
			}
			echo '{'.join(',',$jsonPairs).'}';
		}

		function deleteQuestion($qsID,$msqID){
			$sql='DELETE FROM btExtendedFormQuestions WHERE questionSetId='.intval($qsID).' AND msqID='.intval($msqID);
			$this->db->query($sql,$dataValues);
		} 
		
		function loadQuestions($qsID, $bID=0, $showPending=0 ){
			$db = Loader::db();
			if( intval($bID) ){
				$bIDClause=' AND ( bID='.intval($bID).' ';			
				if( $showPending ) 
					 $bIDClause.=' OR bID=0) ';	
				else $bIDClause.=' ) ';	
			}
			return $db->query('SELECT * FROM btExtendedFormQuestions WHERE questionSetId='.intval($qsID).' '.$bIDClause.' ORDER BY position ASC, msqID ASC');
		}
		
		static function getAnswerCount($qsID){
			$db = Loader::db();
			return $db->getOne( 'SELECT count(*) FROM btExtendedFormAnswerSet WHERE questionSetId='.intval($qsID) );
		}
		
		static function getRatingTotal($qsID, $bID=0){
			$db = Loader::db();
			if( intval($bID) != 0)
				$bIDClause=' AND efq.bID = '.intval($bID).' ';			

			return $db->getOne( 'SELECT ROUND(AVG(efa.answer)) AS total FROM btExtendedFormAnswerSet AS efas, btExtendedFormAnswers AS efa, btExtendedFormQuestions AS efq WHERE efa.asID = efas.asID AND efq.msqID = efa.msqID '.$bIDClause.' AND efas.questionSetId = '.$qsID.' AND  efq.inputType = \'rating\'' );
		}			
		
		function loadSurvey( $qsID, $showEdit=false, $bID=0, $hideQIDs=array(), $showPending=0 ){
			
			$this->bID = $bID;
			
			$questionsRS=$this->loadQuestions( $qsID, $bID, $showPending);
					
			if(!$showEdit){
				
				echo '<div class="formBlockSurveyTable">'."\n";	
								
				while( $questionRow=$questionsRS->fetchRow() ){	
					
					$label = '';
										
					if( in_array($questionRow['qID'], $hideQIDs) ) 
						continue;

					if ($questionRow['inputType'] == 'hidden') {
						$val=($_REQUEST['Question'.intval($questionRow['msqID'])])?$_REQUEST['Question'.intval($questionRow['msqID'])]:trim($questionRow['options']);
						echo '<input name="Question'.intval($questionRow['msqID']).'" type="hidden" value="'.$val.'" />'."\n";
					}
					elseif ($questionRow['inputType'] == 'fieldset-start')
					{
						echo '<fieldset class="fieldset '.$questionRow['customClass'].'">'."\n";							
						if (trim($questionRow['options']) != '')
						{
							echo '<legend class="legend">'.stripslashes($questionRow['options']).'</legend>'."\n";	
							echo '<br class="clearfloat" />'."\n";
						}
					}
					elseif ($questionRow['inputType'] == 'fieldset-end')
						echo '</fieldset>'."\n";
					else
					{
						echo '<div class="formBlockSurveyRow">'."\n";
											
						if ($questionRow['inputType'] == 'space')
							echo '<div class="formBlockSurveyCell space '.$questionRow['customClass'].'">&nbsp;</div>'."\n";
						elseif ($questionRow['inputType'] == 'text')
							echo '<div class="formBlockSurveyCell text '.$questionRow['customClass'].'">'.stripslashes($questionRow['options']).'</div>'."\n";					
						else if ($questionRow['inputType'] == 'hr')
							echo '<div class="formBlockSurveyCell hr"><hr class="'.$questionRow['customClass'].'" /></div>'."\n";
						elseif ($questionRow['inputType'] == 'line')
							echo '<div class="formBlockSurveyCell line"><h4 class="'.$questionRow['customClass'].'">'.stripslashes($questionRow['question']).'</h4></div>'."\n";						
						else
						{
							$requiredSymbol=($questionRow['required'])?'&nbsp;<span class="required">*</span>':'';
							
							if ($questionRow['inputType'] != 'checkboxlist' && $questionRow['inputType'] != 'radios')
								$label = '<label for="Question'.intval($questionRow['msqID']).'">'.$questionRow['question'].$requiredSymbol.'</label>';
							else
								$label = '<label>'.$questionRow['question'].$requiredSymbol.'</label>';
								
							echo '<div class="formBlockSurveyCell question">'.$label.'</div>'."\n";
							echo '<div class="formBlockSurveyCell answer ';
							if ($questionRow['required']) echo 'req';
							//echo '">'.$this->loadInputType($questionRow,showEdit).'</div>'."\n";
							echo '">'.$this->loadInputType($questionRow, $showEdit).'</div>'."\n";
						}
						
						echo '</div>'."\n"; 
						//echo '<br class="clearfloat" />'."\n";
						$questionRow['isSecondEmail'] = false;  // a flag for the loadInputType() method
						if ($questionRow['inputType'] == 'email' and (int)(intval($questionRow['other']) / 2) == 1) {
							$questionRow['isSecondEmail'] = true;
							echo '<div class="formBlockSurveyRow">'."\n";
												
							$requiredSymbol=($questionRow['required'])?'&nbsp;<span class="required">*</span>':'';
							$label = '<label>'.'Confirm '.$questionRow['question'].$requiredSymbol.'</label>';
								
							echo '<div class="formBlockSurveyCell question">'.$label.'</div>'."\n";
							echo '<div class="formBlockSurveyCell answer ';
							if ($questionRow['required']) echo 'req';
							echo '">'.$this->loadInputType($questionRow, $showEdit).'</div>'."\n";
							
							echo '</div>'."\n";							
						}
					}
				}			
				$surveyBlockInfo = $this->getExtendedFormSurveyBlockInfoByQuestionId($qsID,intval($bID));
				
				if($surveyBlockInfo['displayCaptcha']) {				 
					 echo '<div class="formBlockSurveyRow">'."\n";
					 echo '<div class="formBlockSurveyCell question">';
					 echo '<label for="ccmCaptchaCode">'.stripslashes($surveyBlockInfo['titleCaptcha']).':&nbsp;<span class="required">*</span></label>';
					 echo '</div>'."\n";
					 echo '<div class="formBlockSurveyCell answer">';
					 if($surveyBlockInfo['useReCaptcha']) {	
						$recaptcha = Loader::helper('validation/recaptcha', 'extended_form');
						echo $recaptcha->recaptcha_get_html($surveyBlockInfo['publickeyReCaptcha']);
					 } else {
						 $captcha = Loader::helper('validation/captcha');	 
						 $captcha->display();				  
						 $captcha->showInput();
					 }
					 echo stripslashes($surveyBlockInfo['commentCaptcha']);
					 echo '</div>'."\n";  
					 //echo '<br class="clearfloat" />'."\n\r";
					 echo '</div>'."\n\r";   				
   				}
				
				if($surveyBlockInfo['displayRequired']) {					
					echo '<div class="formBlockSurveyRow">'."\n";
					echo '<div class="formBlockSurveyCell question">&nbsp;</div>'."\n";
					echo '<div class="formBlockSurveyCell answer"><span class="required">'.stripslashes($surveyBlockInfo['titleRequired']).'</span></div>'."\n";
					//echo '<br class="clearfloat" />'."\n\r";
					echo '</div>'."\n\r"; 
				}
				
				echo '<div class="formBlockSurveyRow SubmitButton">'."\n";
				echo '<div class="formBlockSurveyCell question">&nbsp;</div>'."\n";
				echo '<div class="formBlockSurveyCell answer">';
				echo '<input class="formBlockSubmitButton" name="Submit" type="submit" value="'.stripslashes($surveyBlockInfo['titleSubmit']).'" /></div>'."\n\r"; 
				echo '</div>'."\n\r"; 
				
				echo '</div>'."\n\r"; 
				
			}else{
			
			
				echo '<div id="extendedFormSurveyTableWrap">';
				echo '<div id="extendedFormSurveyPreviewTable" class="extendedFormSurveyTable">';	
								
				while( $questionRow=$questionsRS->fetchRow() ){	 
				
					if( in_array($questionRow['qID'], $hideQIDs) ) continue;
				
					$requiredSymbol=($questionRow['required'])?'<span class="required">*</span>':'';				
					?>
					<div id="extendedFormSurveyQuestionRow_<?php  echo $questionRow['msqID']?>" class="extendedFormSurveyQuestionRow" style="margin-bottom:2px;">
						<div class="extendedFormSurveyMover"></div>
                        <div class="extendedFormSurveyQuestion preview"><?php  echo $questionRow['question'].' '.$requiredSymbol?></div>
						<div class="extendedFormSurveyOptions">
							<!--
                            <div style="float:right">
								<a href="#" onclick="extendedFormSurvey.moveUp(this,<?php  echo $questionRow['msqID']?>);return false" class="moveUpLink"></a> 
								<a href="#" onclick="extendedFormSurvey.moveDown(this,<?php  echo $questionRow['msqID']?>);return false" class="moveDownLink"></a>						  
							</div>						
							-->
                            <a href="#" class="btn" onclick="extendedFormSurvey.reloadQuestion(<?php    echo intval($questionRow['qID']) ?>);return false"><?php     echo t('Edit')?></a> &nbsp;&nbsp; 
							<a href="#" class="btn error" onclick="extendedFormSurvey.deleteQuestion(this,<?php    echo intval($questionRow['msqID']) ?>,<?php    echo intval($questionRow['qID'])?>);return false"><?php  echo  t('Delete')?></a>
						</div>
						<div class="extendedFormSurveySpacer"></div>
					</div>
				<?php    }			 
				echo '</div></div>';
			}
		}
		
		
		function loadInputType($questionData,$showEdit){
			$options=explode('%%',$questionData['options']);
			$msqID=intval($questionData['msqID']);
			switch($questionData['inputType']){				
				
				case 'checkboxlist': 
					$html.= "\r".'<div class="checkboxList">'."\r";
					
					if ($questionData['other'] && trim($questionData['otherTitle']) != '')
					{
						$checked=($_REQUEST['Question'.$msqID.'_'.(count($options)+1)]=='other_QWOP')?'checked="checked"':'';
						$display=($_REQUEST['Question'.$msqID.'_'.(count($options)+1)]=='other_QWOP')?'display:block;':'display:none;';
						
						$other_checkbox = '<div class="checkboxPair"><input name="Question'.$msqID.'_'.(count($options)+1).'" id="Question'.$msqID.'_'.(count($options)+1).'" type="checkbox" value="other_QWOP" '.$checked.' onclick="extendedForm.showOtherCheckbox(\'Question'.$msqID.'_'.(count($options)+1).'\');" class="'.$questionData['customClass'].'" />&nbsp;<label for="Question'.$msqID.'_'.(count($options)+1).'" class="'.$questionData['customClass'].'_label">'.trim(stripslashes($questionData['otherTitle'])).'</label></div>';												
						
						$val=($_REQUEST['Question'.$msqID.'_other'])?$_REQUEST['Question'.$msqID.'_other']:'';
						$other = '<br class="clearfloat" />
								  <div id="Question'.$msqID.'_'.(count($options)+1).'_other" class="otherDiv" style="'.$display.'">
								  <input name="Question'.$msqID.'_other" type="text" value="'.stripslashes(htmlspecialchars($val)).'" class="'.$questionData['customClass'].'_other" />
								  </div>';
					}
					
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					
					for ($i = 0; $i < count($options); $i++) {
						if(strlen(trim($options[$i]))==0) continue;
						$checked=($_REQUEST['Question'.$msqID.'_'.$i]==trim($options[$i]))?'checked="checked"':'';
						$html.= '<div class="checkboxPair"><input name="Question'.$msqID.'_'.$i.'" id="Question'.$msqID.'_'.$i.'" type="checkbox" value="'.trim($options[$i]).'" '.$checked.' />&nbsp;<label for="Question'.$msqID.'_'.$i.'">'.$options[$i].'</label></div>'."\r\n";
					}
					$html.= $other_checkbox.'</div>'.$tooltip.$other;
					return $html;

				case 'select':
					if($this->frontEndMode){
						$selected=(!$_REQUEST['Question'.$msqID])?'selected="selected"':'';
						$html.= '<option value="" '.$selected.'></option>';					
					}
					foreach($options as $option){
						$selected=($_REQUEST['Question'.$msqID]==trim($option))?'selected="selected"':'';
						$html.= '<option '.$selected.'>'.trim($option).'</option>';
					}
					
					if ($questionData['other'] && trim($questionData['otherTitle']) != '')
					{
						$selected=($_REQUEST['Question'.$msqID]=='other_QWOP')?'selected="selected"':'';
						$display=($_REQUEST['Question'.$msqID]=='other_QWOP')?'display:block;':'display:none;';
						
						$html .= '<option value="other_QWOP" '.$selected.'>'.trim(stripslashes($questionData['otherTitle'])).'</option>';												
						
						$val=($_REQUEST['Question'.$msqID.'_other'])?$_REQUEST['Question'.$msqID.'_other']:'';
						$other = '<br class="clearfloat" />
								  <div id="Question'.$msqID.'_other" class="otherDiv" style="'.$display.'">
								  <input name="Question'.$msqID.'_other" type="text" value="'.stripslashes(htmlspecialchars($val)).'" class="'.$questionData['customClass'].'_other" />
								  </div>';
						$selectscript = 'onchange="extendedForm.showOtherSelect(\'Question'.$msqID.'\')"';
					}
					
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
											
					return '<select name="Question'.$msqID.'" id="Question'.$msqID.'" '.$selectscript.' class="'.$questionData['customClass'].'">'.$html.'</select>'.$tooltip.$other;
				
				case 'recipient':
					
					$crypt = Loader::helper('encryption', 'extended_form');
					$crypt->set_key(PASSWORD_SALT);
					 
					if($this->frontEndMode){
						$selected=(!$_REQUEST['Question'.$msqID])?'selected="selected"':'';
						$html.= '<option value="" '.$selected.'></option>';					
					}
					foreach($options as $option){
						
						if (eregi(';', $option))
							list($recipient_email, $recipient_name) = @split(';', $option);
						else
							$recipient_email = $recipient_name = $option;
						
						$option = $crypt->encrypt($option);
						
						$selected=($_REQUEST['Question'.$msqID]==trim($option))?'selected="selected"':'';
						$html.= '<option value="'.$option.'" '.$selected.'>'.trim($recipient_name).'</option>';
					}
										
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
											
					return '<select name="Question'.$msqID.'" id="Question'.$msqID.'" class="'.$questionData['customClass'].'">'.$html.'</select>'.$tooltip;
							
				case 'radios':
					$html.= '<div class="radiobuttonsList">'."\r\n";
					
					if ($questionData['other'] && trim($questionData['otherTitle']) != '')
					{
						$checked=($_REQUEST['Question'.$msqID]=='other_QWOP')?'checked="checked"':'';
						$display=($_REQUEST['Question'.$msqID]=='other_QWOP')?'display:block;':'display:none;';
						$radioscript = 'onclick="extendedForm.showOtherRadio(\'Question'.$msqID.'\')"';
						
						$other_checkbox = '<div class="radioPair"><input name="Question'.$msqID.'" id="Question'.$msqID.'_radio_other" type="radio" value="other_QWOP" '.$checked.' '.$radioscript.' class="'.$questionData['customClass'].'" />&nbsp;<label for="Question'.$msqID.'_radio_other" class="'.$questionData['customClass'].'_label">'.trim(stripslashes($questionData['otherTitle'])).'</label></div>';												
						
						$val=($_REQUEST['Question'.$msqID.'_other'])?$_REQUEST['Question'.$msqID.'_other']:'';
						$other = '<br class="clearfloat" />
								  <div id="Question'.$msqID.'_other" class="otherDiv" style="'.$display.'">						  
								  <input name="Question'.$msqID.'_other" type="text" value="'.stripslashes(htmlspecialchars($val)).'" class="'.$questionData['customClass'].'_other" />
								  </div>';
					}
					
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					
					for ($i = 0; $i < count($options); $i++) {
						if(strlen(trim($options[$i]))==0) continue;
						$checked=($_REQUEST['Question'.$msqID]==trim($options[$i]))?'checked="checked"':'';
						$html.= '<div class="radioPair"><input name="Question'.$msqID.'" id="Question'.$msqID.'_'.$i.'" type="radio" value="'.trim($options[$i]).'" '.$checked.' '.$radioscript .' class="'.$questionData['customClass'].'" />&nbsp;<label for="Question'.$msqID.'_'.$i.'" class="'.$questionData['customClass'].'_label">'.$options[$i].'</label></div>';
					}
					
					$html.= $other_checkbox.'</div>'.$tooltip.$other;
					return $html;
					
				case 'fileupload': 
					$html='<input type="file" name="Question'.$msqID.'" id="Question'.$msqID.'" />'; 				
					
					$tooltip=($questionData['tooltip'])?'<div class="tooltip_holder" title="'.stripslashes($questionData['tooltipText']).'">?</div>':'';
					return $html.$tooltip;
				
				case 'datetime':															
					$val=($_REQUEST['Question'.$msqID])?$_REQUEST['Question'.$msqID]:'';
					if ($questionData['options'] == 'datetime' || $questionData['options'] == '') {
						$dt = Loader::helper('form/date_time');
						$html = $dt->datetime('Question'.$msqID, $val);
					} elseif ($questionData['options'] == 'date') {
						$dt = Loader::helper('form/date_time');
						$html = $dt->date('Question'.$msqID, $val);
					} elseif ($questionData['options'] == 'time') {
						$dt = Loader::helper('form/time', 'extended_form');
						$html = $dt->time('Question'.$msqID, $val);
					}
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					return $html.$tooltip;
					
				case 'textarea':
					$val=($_REQUEST['Question'.$msqID])?$_REQUEST['Question'.$msqID]:trim($questionData['options']);
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					return '<textarea name="Question'.$msqID.'" id="Question'.$msqID.'" cols="'.$questionData['width'].'" rows="'.$questionData['height'].'" class="'.$questionData['customClass'].'">'.$val.'</textarea>'.$tooltip;
			   
			    case 'tinymce':
                    $val=($_REQUEST['Question'.$msqID])?$_REQUEST['Question'.$msqID]:trim($questionData['options']);
                    if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
                    return '<textarea class="advancedEditor" name="Question'.$msqID.'" id="Question'.$msqID.'" cols="'.$questionData['width'].'" rows="'.$questionData['height'].'" class="'.$questionData['customClass'].'">'.$val.'</textarea>'.$tooltip;
				
				case 'rating':
					$val=($_REQUEST['Question'.$msqID])?$_REQUEST['Question'.$msqID]:'';
					$rating.= '<div class="ratinglist" id="stars-'.$msqID.'">'."\r\n";					
					for ($i=1; $i<=intval($questionData['options']); $i++)
					{
						$checked=($_REQUEST['Question'.$msqID]==$i)?'checked':'';
						$rating.= '<input name="Question'.$msqID.'" id="Question'.$msqID.'" type="radio" value="'.$i.'" title="'.$i.'" class="star '.$questionData['customClass'].'" '.$checked.' />';
					}
					$rating.= '<div id="stars-cap-'.$msqID.'"></div></div>';
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					
					return $rating.$tooltip.$script;
				
				case 'integer':
					$val=($_REQUEST['Question'.$msqID])?$_REQUEST['Question'.$msqID]:'';
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					return '<input name="Question'.$msqID.'" id="Question'.$msqID.'" type="text" value="'.stripslashes(htmlspecialchars($val)).'" class="'.$questionData['customClass'].'" />'.$tooltip;
				
				case 'email':
					$str2 = ($questionData['isSecondEmail']) ? '_2' : '';
					
					$val=($_REQUEST['Question'.$msqID.$str2])?$_REQUEST['Question'.$msqID.$str2]:trim($questionData['options']);
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					return '<input name="Question'.$msqID.$str2.'" id="Question'.$msqID.$str2.'" type="text" value="'.stripslashes(htmlspecialchars($val)).'" class="'.$questionData['customClass'].'" />'.$tooltip;
						
				case 'field':
				default:
					$val=($_REQUEST['Question'.$msqID])?$_REQUEST['Question'.$msqID]:trim($questionData['options']);
					if ($questionData['tooltip']) {
						if ($questionData['customClass'])
							$class = ' '.$questionData['customClass'].'_tooltip';
						else 
							$class =  '';							
						$tooltip = '<div class="tooltip_holder'.$class.'" title="'.stripslashes($questionData['tooltipText']).'">?</div>';
					} else 
						$tooltip = '';
					return '<input name="Question'.$msqID.'" id="Question'.$msqID.'" type="text" value="'.stripslashes(htmlspecialchars($val)).'" class="'.$questionData['customClass'].'" />'.$tooltip;
			}
		}
		
		function getExtendedFormSurveyBlockInfo($bID){
			$rs=$this->db->query('SELECT * FROM btExtendedForm WHERE bID='.intval($bID).' LIMIT 1' );
			return $rs->fetchRow();
		}
		
		function getExtendedFormSurveyBlockInfoByQuestionId($qsID,$bID=0){
			$sql='SELECT * FROM btExtendedForm WHERE questionSetId='.intval($qsID);
			if(intval($bID)>0) $sql.=' AND bID='.$bID;
			$sql.=' LIMIT 1'; 
			$rs=$this->db->query( $sql );
			return $rs->fetchRow();
		}		
		
		function reorderQuestions($qsID=0,$qIDs){
			$qIDs=explode(',',$qIDs);
			if(!is_array($qIDs)) $qIDs=array($qIDs);
			$positionNum=0;
			foreach($qIDs as $qID){
				$vals=array( $positionNum,intval($qID), intval($qsID) );
				$sql='UPDATE btExtendedFormQuestions SET position=? WHERE msqID=? AND questionSetId=?';
				$rs=$this->db->query($sql,$vals);
				$positionNum++;
			}
		}
		
		function reorderNewQuestions($qsID=0, $qIDs){
			$positionNum=0;
			foreach($qIDs as $qID){
				$vals=array( $positionNum, intval($qID), intval($qsID) );
				$sql='UPDATE btExtendedFormQuestions SET position=? WHERE msqID=? AND questionSetId=?';
				$rs=$this->db->query($sql,$vals);
				$positionNum++;
			}
		}			

		function limitRange($val, $min, $max){
			$val = ($val < $min) ? $min : $val;
			$val = ($val > $max) ? $max : $val;
			return $val;
		}
				
		//Run on Form block edit
		static function questionCleanup( $qsID=0, $bID=0 ){
			$db = Loader::db();
		
			//First make sure that the bID column has been set for this questionSetId (for backwards compatibility)
			$vals=array( intval($qsID) ); 
			$questionsWithBIDs=$db->getOne('SELECT count(*) FROM btExtendedFormQuestions WHERE bID!=0 AND questionSetId=?',$vals);
			
			//form block was just upgraded, so set the bID column
			if(!$questionsWithBIDs){ 
				$vals=array( intval($bID), intval($qsID) );  
				$rs=$db->query('UPDATE btExtendedFormQuestions SET bID=? WHERE bID=0 AND questionSetId=?',$vals);
				return; 
			} 			
			
			//Then remove all temp/placeholder questions for this questionSetId that haven't been assigned to a block
			$vals=array( intval($qsID) );  
			$rs=$db->query('DELETE FROM btExtendedFormQuestions WHERE bID=0 AND questionSetId=?',$vals);			
		}
}
?>