<?php      

     /*
	 * Tell A Friend From for Concrete 5
	 * @author Wim Bouter <wim@dewebmakers.nl>
	 */
	 
defined('C5_EXECUTE') or die(_("Access Denied."));

class ExtendedFormPackage extends Package {

	protected $pkgHandle = 'extended_form';
	protected $appVersionRequired = '5.5';
	protected $pkgVersion = '2.7.2';
	
	public function getPackageDescription() {
		return t("This addon gives you just a little bit more playroom with your forms.");
	}
	
	public function getPackageName() {
		return t("Extended Form");
	}
	
	public function install() {
		$pkg = parent::install();
		BlockType::installBlockTypeFromPackage('extended_form', $pkg);
		BlockType::installBlockTypeFromPackage('form_results', $pkg);
		
		Loader::model('single_page');
		$sp = SinglePage::add('/dashboard/reports/extended_form', $pkg);
		$sp->setAttribute('icon_dashboard', 'icon-briefcase');
	}
	
	public function upgrade() {
		parent::upgrade();
		
		$p = Page::getByPath('/dashboard/reports/extended_form');
		$p->setAttribute('icon_dashboard', 'icon-briefcase');
	}

	
	public function uninstall() {
		parent::uninstall();
		$db = Loader::db();
		$db->Execute('drop table btExtendedForm');
		$db->Execute('drop table btExtendedFormAnswers');
		$db->Execute('drop table btExtendedFormAnswerSet');
		$db->Execute('drop table btExtendedFormQuestions');
		$db->Execute('drop table btExtendedFormResults');
		
	}
}