<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::block('extended_form');

Loader::model('stats', 'extended_form');
Loader::model('survey', 'extended_form');
		
class DashboardReportsExtendedFormController extends Controller {

	private $pageSize=5; 

	public function view(){	
		if($_REQUEST['all']){
			$this->pageSize=100000; 
			$_REQUEST['page']=1;
		}
		$this->loadSurveyResponses();
	}
	
	public function excel(){ 
		$dateHelper = Loader::helper('date');
		
		$this->pageSize=0;
		$this->loadSurveyResponses();
		$textHelper = Loader::helper('text');
		
		$questionSet=$this->get('questionSet');
		$answerSets=$this->get('answerSets');
		$questions=$this->get('questions');	
		$surveys=$this->get('surveys');	 
		 
		$fileName=$textHelper->filterNonAlphaNum($surveys[$questionSet]['surveyName']);
		
		header("Content-Type: application/vnd.ms-excel");
		header("Cache-control: private");
		header("Pragma: public");
		$date = date('Ymd');
		header("Content-Disposition: inline; filename=".$fileName."_form_data_{$date}.xls"); 
		header("Content-Title: ".$surveys[$questionSet]['surveyName']." Form Data Output - Run on {$date}");		echo "<html>\r\n";
		echo "<head><META http-equiv=Content-Type content=\"text/html; charset=".APP_CHARSET."\"></head>\r\n";
		echo "<body>\r\n";
		echo "<table>\r\n";
		$hasCBRow = false;
		foreach($questions as $questionId=>$question){ 
            if ($question['inputType'] == 'checkboxlist') {
				$hasCBRow = true;
			}
		}

		echo "<tr>";
		echo "\t\t<td";
		if ($hasCBRow) {
			echo " rowspan=\"2\" valign='bottom'";
		}
		echo "><b>Submitted Date</b></td>\r\n";
		
		foreach($questions as $questionId=>$question) { 
            if ($question['inputType'] == 'checkboxlist')
            {
                $options = explode('%%', $question['options']);
				
				if ($question['other'] == 1 && trim($question['otherTitle']) != '')
						$options[] = $question['otherTitle']; 
					
			    echo "\t\t".'<td colspan="'.count($options).'"><b>'."\r\n";
				echo "\t\t\t".$questions[$questionId]['question']."\r\n";
				echo "\t\t</b></td>\r\n";		
            }
			elseif ($question['inputType'] != 'line' && $question['inputType'] != 'text' && $question['inputType'] != 'hr' && $question['inputType'] != 'space' && $question['inputType'] != 'fieldset-start' && $question['inputType'] != 'fieldset-end') {
			    echo "\t\t<td ";
			    if ($hasCBRow) {
			    	echo " rowspan=\"2\" valign='bottom'";
			    }
			    echo "><b>\r\n";
				echo "\t\t\t".$questions[$questionId]['question']."\r\n";
				echo "\t\t</b></td>\r\n";		
            }
		}
		
		if($surveys[$questionSet]['capturePageID'] == 1) {
			echo "\t\t<td ";
			if ($hasCBRow) {
				echo "rowspan=\"2\" valign='bottom'";
			}
			echo "><b>\r\n";
			echo "\t\t\t".t('Page')."\r\n";
			echo "\t\t</b></td>\r\n";
		} 
		
		if($surveys[$questionSet]['captureIP'] == 1) {
			echo "\t\t<td ";
			if ($hasCBRow) {
				echo "rowspan=\"2\" valign='bottom'";
			}
			echo "><b>\r\n";
			echo "\t\t\t".t('IP')."\r\n";
			echo "\t\t</b></td>\r\n";
		} 
		
		if($surveys[$questionSet]['captureUser'] == 1) {
			echo "\t\t<td ";
			if ($hasCBRow) {
				echo "rowspan=\"2\" valign='bottom'";
			}
			echo "><b>\r\n";
			echo "\t\t\t".t('Username')."\r\n";
			echo "\t\t</b></td>\r\n";
		} 
		
		echo "</tr>";

		// checkbox row
		if ($hasCBRow) {
			echo "<tr>";
			foreach($questions as $questionId=>$question){ 
				if ($question['inputType'] == 'checkboxlist')
				{
					$options = explode('%%', $question['options']);
					
					if ($question['other'] == 1 && trim($question['otherTitle']) != '')
						$options[] = $question['otherTitle']; 
						
					foreach($options as $opt) {
						echo "<td><b>{$opt}</b></td>";
					}
				}
			}
			echo "</tr>";
		}
		
		foreach($answerSets as $answerSetId=>$answerSet){ 
			$questionNumber=0;
			$numQuestionsToShow=2;
			echo "\t<tr>\r\n";
			echo "\t\t<td>". $dateHelper->getSystemDateTime($answerSet['created'])."</td>\r\n";
			foreach($questions as $questionId=>$question){ 
				$questionNumber++;
                if ($question['inputType'] == 'checkboxlist'){
                    $options = explode('%%', $question['options']);
                    if ($question['other'] == 1 && trim($question['otherTitle']) != '')
						$options[] = $question['otherTitle']; 
					$subanswers = explode(',', $answerSet['answers'][$questionId]['answer']);
                    for ($i = 1; $i <= count($options); $i++)
                    {
				        echo "\t\t<td align='center'>\r\n";
                        if (in_array(trim($options[$i-1]), $subanswers)) {
				           // echo "\t\t\t".$options[$i-1]."\r\n";
				           echo "x";
				        } elseif (preg_match('/'.$options[$i-1].'/', end($subanswers))) {
							echo end($subanswers);
						} else {
						    echo "\t\t\t&nbsp;\r\n";
				        }
				        echo "\t\t</td>\r\n";
                    }					
               }elseif($question['inputType']=='fileupload'){ 
					echo "\t\t<td>\r\n";
					$fID=$answerSet['answers'][$questionId]['answer'];
					$file=File::getByID($fID);
					if($fID && $file){
						$fileVersion=$file->getApprovedVersion();
						echo "\t\t\t".'<a href="'. BASE_URL . DIR_REL . $fileVersion->getRelativePath() .'">'.$fileVersion->getFileName().'</a>'."\r\n";
					}else{									
						echo "\t\t\t".t('File not found')."\r\n";
					} 	
					echo "\t\t</td>\r\n";		
				}elseif ($question['inputType'] != 'line' && $question['inputType'] != 'text' && $question['inputType'] != 'hr' && $question['inputType'] != 'space' && $question['inputType'] != 'fieldset-start' && $question['inputType'] != 'fieldset-end') {				
				    echo "\t\t<td>\r\n";
				    echo "\t\t\t".$answerSet['answers'][$questionId]['answer'].$answerSet['answers'][$questionId]['answerLong']."\r\n";					
				    echo "\t\t</td>\r\n";
                }
			}
			if($surveys[$questionSet]['capturePageID'] == 1) {
				$page = Page::getByID($answerSet['answers'][9996]['answer']); 
				if (is_object($page)) {
                        echo "\t\t<td>\r\n";
                        echo "\t\t\t<a href='".View::url($page->getCollectionPath())."' target='_blank'>";
                        echo $page->getCollectionName();
                        echo "</a></a>\r\n";
                  		echo "\t\t</td>\r\n";     
            	} else {
					echo "\t\t<td>&nbsp;\r\n</td>\r\n";
				}
			}
			
			if($surveys[$questionSet]['captureIP'] == 1) {
				echo "\t\t<td>\r\n";
				echo "\t\t\t".$answerSet['answers'][9997]['answer']."</a>\r\n";					
				echo "\t\t</td>\r\n";
			} 
			
			if($surveys[$questionSet]['captureUser'] == 1) {
				$user = User::getByUserID($answerSet['answers'][9999]['answer']); 
				if (is_object($user)) {
					echo "\t\t<td>\r\n";
					echo "\t\t\t<a href=\"".BASE_URL."/index.php/dashboard/users/search?uID=" . $answerSet['answers'][9998]['answer']."\">".$answerSet['answers'][9999]['answer']."</a>\r\n";					
					echo "\t\t</td>\r\n";
				} else {
					echo "\t\t<td>\r\n";
					echo t('Guest');
				    echo "\t\t</td>\r\n";
				}	
			} 
			echo "\t</tr>\r\n";
		}
		echo "</table>\r\n";
		echo "</body>\r\n";
		echo "</html>\r\n";		
		die;
	}	

	private function loadSurveyResponses(){
		$c=$this->getCollectionObject();
		$db = Loader::db();
		$tempMiniSurvey = new ExtendedFormSurvey();
		$pageBase = DIR_REL . '/' . DISPATCHER_FILENAME . '?cID=' . $c->getCollectionID();
		
		if( $_REQUEST['action'] == 'deleteForm' ){
			$this->deleteForm($_REQUEST['bID'], $_REQUEST['qsID']);
		}	
		
		if( $_REQUEST['action'] == 'deleteFormAnswers' ){
			$this->deleteFormAnswers($_REQUEST['qsID']);
            $this->redirect('/dashboard/reports/extended_form');
		}	
		
		if( $_REQUEST['action'] == 'deleteResponse' ){
			$this->deleteAnswers($_REQUEST['asid']);
		}		
		
		//load surveys
		$surveysRS=ExtendedFormBlockStatistics::loadSurveys($tempMiniSurvey);
		
		//index surveys by question set id
		$surveys=array();
		while($survey=$surveysRS->fetchRow()){
			//get Survey Answers
			$survey['answerSetCount'] = ExtendedFormSurvey::getAnswerCount( $survey['questionSetId'] );
			$survey['ratingTotal'] = ExtendedFormSurvey::getRatingTotal( $survey['questionSetId'], $survey['bID'] );
			$surveys[ $survey['questionSetId'] ] = $survey;			
		}		
	
			
		//load requested survey response
		if (!empty($_REQUEST['qsid'])) {
			$questionSet = intval(preg_replace('/[^[:alnum:]]/','',$_REQUEST['qsid']));
			
			//get Survey Questions
			$questionsRS = ExtendedFormSurvey::loadQuestions($questionSet);
			$questions = array();
			while( $question = $questionsRS->fetchRow() ){
				$questions[$question['msqID']]=$question;
			}
			
			//get Survey Answers
			$answerSetCount = ExtendedFormSurvey::getAnswerCount($questionSet);
			
			//pagination 
			$pageBaseSurvey = $pageBase.'&qsid='.$questionSet;
			$paginator = Loader::helper('pagination');
			$sortBy = $_REQUEST['sortBy'];
			$paginator->init(
				(int) $_REQUEST['page'],
				$answerSetCount,
				$pageBaseSurvey.'&page=%pageNum%&sortBy='.$sortBy,
				$this->pageSize
			);
			
			if ($this->pageSize>0) {
				$limit = $paginator->getLIMIT();
			} else {
				$limit = '';
			}
			$answerSets = ExtendedFormBlockStatistics::buildAnswerSetsArray( $questionSet, $sortBy, $limit ); 
		}
		$this->set('questions',$questions);		
		$this->set('answerSets',$answerSets);
		$this->set('paginator',$paginator);	
		$this->set('questionSet',$questionSet);
		$this->set('surveys',$surveys);  			
	}
	// SET UP DELETE FUNCTIONS HERE
	// DELETE SUBMISSIONS
	private function deleteAnswers($asID){
		$db = Loader::db();
		$v = array(intval($asID));
		$q = 'DELETE FROM btExtendedFormAnswers WHERE asID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM btExtendedFormAnswerSet WHERE asID = ?';		
		$r = $db->query($q, $v);
	}
	//DELETE A FORM ANSWERS
	private function deleteFormAnswers($qsID){
		$db = Loader::db();
		$v = array(intval($qsID));
		$q = 'SELECT asID FROM btExtendedFormAnswerSet WHERE questionSetId = ?';
				
		$r = $db->query($q, $v);
		while ($row = $r->fetchRow()) {
			$asID = $row['asID'];
			$this->deleteAnswers($asID);
		}
	}
	//DELETE FORMS AND ALL SUBMISSIONS
	private function deleteForm($bID, $qsID){
		$this->deleteFormAnswers($qsID);
		
		$v = array(intval($bID));
		$q = 'DELETE FROM btExtendedFormQuestions WHERE bID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM btExtendedForm WHERE bID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM Blocks WHERE bID = ?';		
		$r = $db->query($q, $v);
		
	}	
}
