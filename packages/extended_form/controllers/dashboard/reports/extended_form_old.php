<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('stats', 'extended_form');
Loader::model('survey', 'extended_form');
		
class DashboardReportsExtendedFormController extends Controller {

	private $pageSize=3; 

	public function view(){	
		if($_REQUEST['all']){
			$this->pageSize=100000; 
			$_REQUEST['page']=1;
		}
		$this->loadSurveyResponses();
	}

	public function excel(){ 
		$this->pageSize=0;
		$this->loadSurveyResponses();
		$textHelper = Loader::helper('text');
		
		$questionSet=$this->get('questionSet');
		$answerSets=$this->get('answerSets');
		$questions=$this->get('questions');	
		$surveys=$this->get('surveys');	 
		 
		$fileName=$textHelper->filterNonAlphaNum($surveys[$questionSet]['surveyName']);
				
		header("Content-Type: application/vnd.ms-excel");
		header("Cache-control: private");
		header("Pragma: public");
		$date = date('Ymd');
		header("Content-Disposition: inline; filename=".$fileName."_form_data_{$date}.xls"); 
		header("Content-Title: ".$surveys[$questionSet]['surveyName']." Form Data Output - Run on {$date}");		
		echo "<table>\r\n";
		$hasCBRow = false;
		foreach($questions as $questionId=>$question){ 
            if ($question['inputType'] == 'checkboxlist') {
				$hasCBRow = true;
			}
		}

		echo "<tr>";
		echo "\t\t<td ";
		if ($hasCBRow) {
			echo "rowspan=\"2\" valign='bottom'";
		}
		echo "><b>Submitted Date</b></td>\r\n";
		
		foreach($questions as $questionId=>$question){ 
            if ($question['inputType'] == 'checkboxlist')
            {
			    $options = explode('%%', $question['options']);
				
				if ($question['other'] == 1 && trim($question['otherTitle']) != '')
					$options[] = $question['otherTitle']; 
			   
			   echo "\t\t".'<td colspan="'.count($options).'"><b>'."\r\n";
			   echo "\t\t\t".$questions[$questionId]['question']."\r\n";
			   echo "\t\t</b></td>\r\n";
            }
            elseif ($question['inputType'] != 'line' && $question['inputType'] != 'text' && $question['inputType'] != 'hr' && $question['inputType'] != 'space' && $question['inputType'] != 'fieldset-start' && $question['inputType'] != 'fieldset-end')            
            {
			    echo "\t\t<td ";
			    if ($hasCBRow) {
			    	echo "rowspan=\"2\" valign='bottom'>";
			    }
			    echo "<b>\r\n";
				echo "\t\t\t".$questions[$questionId]['question']."\r\n";
			    echo "\t\t</b></td>\r\n";
            }
		}
			
		if($surveys[$questionSet]['captureUser'] == 1) {
			echo "\t\t<td ";
			if ($hasCBRow) {
				echo "rowspan=\"2\" valign='bottom'>";
			}
			echo "<b>\r\n";
			echo "\t\t\tUsername\r\n";
			echo "\t\t</b></td>\r\n";
		} 
			
		echo "</tr>";

		// checkbox row
		if ($hasCBRow) {
			echo "<tr>";
			foreach($questions as $questionId=>$question){ 
				if ($question['inputType'] == 'checkboxlist')
				{
					$options = explode('%%', $question['options']);
					
					if ($question['other'] == 1 && trim($question['otherTitle']) != '')
						$options[] = $question['otherTitle']; 
					
					foreach($options as $opt) {
						echo "<td><b>{$opt}</b></td>";
					}
				}
			}
			echo "</tr>";
		}
		
		foreach($answerSets as $answerSetId=>$answerSet){ 
			$questionNumber=0;
			$numQuestionsToShow=2;
			echo "\t<tr>\r\n";
			echo "\t\t<td>".$answerSet['created']."</td>\r\n";
			foreach($questions as $questionId=>$question){ 
				$questionNumber++;
                if ($question['inputType'] == 'checkboxlist'){
                    $options = explode('%%', $question['options']);
					if ($question['other'] == 1 && trim($question['otherTitle']) != '')
						$options[] = $question['otherTitle']; 
						
                    $subanswers = explode(',', $answerSet['answers'][$questionId]['answer']);                    					
					for ($i = 1; $i <= count($options); $i++)
                    {
				        echo "\t\t<td>\r\n";
                        foreach ($subanswers as $subanswer)
						{
							if ($subanswer == $options[$i-1])
								echo "&#10004;";
							elseif ($i == count($options)) {
								echo $subanswers[count($subanswers)-1];
								break;
							}
						}
										       
					    echo "\t\t</td>\r\n";
                    }					
                }elseif($question['inputType']=='fileupload'){ 
					echo "\t\t<td>\r\n";
					$fID=$answerSet['answers'][$questionId]['answer'];
					if($fID) {
					//$file=File::getByID($fID);
					//if($fID && $file){
					//	$fileVersion=$file->getApprovedVersion();
					//	echo "\t\t\t".'<a href="'. BASE_URL . DIR_REL . $fileVersion->getRelativePath() .'">'.$fileVersion->getFileName().'</a>'."\r\n";
						echo '<a href="'. $fID .'">Download file</a>';
					}else{									
						echo "\t\t\t".t('File not found')."\r\n";
					} 	
					echo "\t\t</td>\r\n";		
				}elseif ($question['inputType'] != 'line' && $question['inputType'] != 'text' && $question['inputType'] != 'hr' && $question['inputType'] != 'space' && $question['inputType'] != 'fieldset-start' && $question['inputType'] != 'fieldset-end') {					
				    echo "\t\t<td>\r\n";
				    echo "\t\t\t".$answerSet['answers'][$questionId]['answer'].$answerSet['answers'][$questionId]['answerLong']."\r\n";					
				    echo "\t\t</td>\r\n";
                }
			}
			
			if($surveys[$questionSet]['captureUser'] == 1) {
				echo "\t\t<td>\r\n";
				echo "\t\t\t<a href=\"".BASE_URL."/index.php/dashboard/users/search?uID=" . $answerSet['answers'][9998]['answer']."\">".$answerSet['answers'][9999]['answer']."</a>\r\n";					
				echo "\t\t</td>\r\n";
			} 


			echo "\t</tr>\r\n";
		}
		echo "</table>\r\n";		
		die;
	}	

	private function loadSurveyResponses(){
		$c=$this->getCollectionObject();
		$db = Loader::db();
			
		$extendedFormSurvey = new ExtendedFormSurvey();		
		$extendedFormBlockStatistics = new ExtendedFormBlockStatistics();
		
		$pageBase=DIR_REL.'/index.php?cID='.$c->getCollectionID();	
		
		if( $_REQUEST['action'] == 'deleteForm' ){
			$this->deleteForm($_REQUEST['bID'], $_REQUEST['qsID']);
		}	
		
		if( $_REQUEST['action'] == 'deleteResponse' ){
			$this->deleteAnswers($_REQUEST['asid']);
		}		
				
		//load surveys
		$surveysRS=$extendedFormBlockStatistics->loadSurveys($extendedFormSurvey);
		
		//index surveys by question set id
		$surveys=array();
		while($survey=$surveysRS->fetchRow()){
			//get Survey Answers
			$survey['answerSetCount'] = $extendedFormSurvey->getAnswerCount( $survey['questionSetId'] );
			$survey['ratingTotal'] = $extendedFormSurvey->getRatingTotal( $survey['questionSetId'], $survey['bID'] );
			$surveys[ $survey['questionSetId'] ] = $survey;			
		}		
		
		//load requested survey response
		if( strlen($_REQUEST['qsid'])>0 ){
			$questionSet=preg_replace('/[^[:alnum:]]/','',$_REQUEST['qsid']);
			
			//get Survey Questions
			$questionsRS=$extendedFormSurvey->loadQuestions($questionSet);
			$questions=array();
			while( $question = $questionsRS->fetchRow() ){
				$questions[$question['msqID']]=$question;
			}
						
			//get Survey Answers
			$answerSetCount = $extendedFormSurvey->getAnswerCount($questionSet);
			
			//pagination 
			$pageBaseSurvey=$pageBase.'&qsid='.$questionSet;
			$paginator=Loader::helper('pagination');
			$sortBy=$_REQUEST['sortBy'];
			$paginator->init( intval($_REQUEST['page']) ,$answerSetCount,$pageBaseSurvey.'&page=%pageNum%&sortBy='.$sortBy,$this->pageSize);
			
			if($this->pageSize>0)
				$limit=$paginator->getLIMIT();
			else $limit='';
			$answerSets = $extendedFormBlockStatistics->buildAnswerSetsArray( $questionSet, $sortBy, $limit ); 
		}
		$this->set('questions',$questions);		
		$this->set('answerSets',$answerSets);
		$this->set('paginator',$paginator);	
		$this->set('questionSet',$questionSet);
		$this->set('surveys',$surveys);  			
	}
	// SET UP DELETE FUNCTIONS HERE
	// DELETE SUBMISSIONS
	private function deleteAnswers($asID){
		$db = Loader::db();
		$v = array(intval($asID));
		$q = 'DELETE FROM btExtendedFormAnswers WHERE asID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM btExtendedFormAnswerSet WHERE asID = ?';		
		$r = $db->query($q, $v);
	}
	//DELETE FORMS AND ALL SUBMISSIONS
	private function deleteForm($bID, $qsID){
		$db = Loader::db();
		$v = array(intval($qsID));
		$q = 'SELECT asID FROM btExtendedFormAnswerSet WHERE questionSetId = ?';
				
		$r = $db->query($q, $v);
		while ($row = $r->fetchRow()) {
			$asID = $row['asID'];
			$this->deleteAnswers($asID);
		}
		
		$v = array(intval($bID));
		$q = 'DELETE FROM btExtendedFormQuestions WHERE bID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM btExtendedForm WHERE bID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM Blocks WHERE bID = ?';		
		$r = $db->query($q, $v);
		
	}	
}

?>