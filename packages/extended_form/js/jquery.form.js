// JavaScript Document
var extendedForm ={
	showOtherSelect:function(input){ 
		if ($('#'+input+' option:last').attr('selected')) 
			$('#'+input+'_other').slideDown('slow');
		else 
			$('#'+input+'_other').slideUp('slow');
	},
	showOtherCheckbox:function(input){ 
		if ($('#'+input).attr('checked')) 
			$('#'+input+'_other').slideDown('slow');
		else 
			$('#'+input+'_other').slideUp('slow');
	},
	showOtherRadio:function(input){ 
		if ($('#'+input+'_radio_other').attr('checked')) 
			$('#'+input+'_other').slideDown('slow');
		else 
			$('#'+input+'_other').slideUp('slow');
	}
}