var extendedFormSurvey ={
	bid:0,
	serviceURL: $("input[name=extendedFormSurveyServices]").val() + '?block=extended_form&',
	init: function(){ 
		this.tabSetup();
		this.answerTypes=document.forms['ccm-block-form'].answerType;
		this.answerTypesEdit=document.forms['ccm-block-form'].answerTypeEdit; 
		this.answerOptionsDate=document.forms['ccm-block-form'].answerOptionsDate;
		this.answerOptionsDateEdit=document.forms['ccm-block-form'].answerOptionsDateEdit; 
		
		for(var i=0;i<this.answerTypes.length;i++){
			this.answerTypes[i].onclick=function(){extendedFormSurvey.optionsCheck(this);extendedFormSurvey.settingsCheck(this);}
			this.answerTypes[i].onchange=function(){extendedFormSurvey.optionsCheck(this);extendedFormSurvey.settingsCheck(this);}
		} 
		for(var i=0;i<this.answerTypesEdit.length;i++){
			this.answerTypesEdit[i].onclick=function(){extendedFormSurvey.optionsCheck(this,'Edit');extendedFormSurvey.settingsCheck(this,'Edit');}
			this.answerTypesEdit[i].onchange=function(){extendedFormSurvey.optionsCheck(this,'Edit');extendedFormSurvey.settingsCheck(this,'Edit');}
		} 			
		$('#refreshButton').click( function(){ extendedFormSurvey.refreshSurvey() } );
		$('#addQuestion').click( function(){ extendedFormSurvey.addQuestion() } );
		$('#editQuestion').click( function(){ extendedFormSurvey.addQuestion('Edit') } );
		$('#cancelEditQuestion').click( function(){ $('#editQuestionForm').css('display','none') } );			
		
		this.serviceURL+='cID='+this.cID+'&arHandle='+this.arHandle+'&bID='+this.bID+'&btID='+this.btID+'&';
		
		if (this.bID > 0)
			extendedFormSurvey.showPane('edit');
		else
			extendedFormSurvey.showPane('add');
			
		extendedFormSurvey.refreshSurvey();
	},	
		
	tabSetup: function(){
		$('ul#ccm-formblock-tabs li a').each( function(num,el){ 
			el.onclick=function(){
				var pane=this.id.replace('ccm-formblock-tab-','');
				extendedFormSurvey.showPane(pane);
			}
		});		
	},
	
	showPane:function(pane){
		$('ul#ccm-formblock-tabs li').each(function(num,el){ $(el).removeClass('ccm-nav-active') });
		$(document.getElementById('ccm-formblock-tab-'+pane).parentNode).addClass('ccm-nav-active');
		$('div.ccm-formBlockPane').each(function(num,el){ el.style.display='none'; });
		$('#ccm-formBlockPane-'+pane).css('display','block');
	},
	
	refreshSurvey : function(){
		
		var serviceURL =  this.serviceURL;
		var qsID = parseInt(this.qsID);
		
		$.ajax({ 
			url: this.serviceURL+'mode=refreshSurvey&qsID='+parseInt(this.qsID)+'&hide='+extendedFormSurvey.hideQuestions.join(','),
			success: function(msg){ $('#extendedFormSurveyPreviewWrap').html(msg); }
		});
		$.ajax({ 			
			url: this.serviceURL+'mode=refreshSurvey&qsID='+parseInt(this.qsID)+'&showEdit=1&hide='+extendedFormSurvey.hideQuestions.join(','),
			success: function(msg){	
				$('#extendedFormSurveyWrap').html(msg); 
				
				$("#extendedFormSurveyPreviewTable .extendedFormSurveyQuestionRow").hover(
					function () { $(this).css("background-color", '#EEE'); },
					function () { $(this).css("background-color", 'transparent'); }
				);
				
				$("#extendedFormSurveyPreviewTable").sortable({
					handle: '.extendedFormSurveyMover',
					cursor: 'move',
					opacity: 0.5,
					stop: function() {
						var ualist = $(this).sortable('serialize');
						$.post(serviceURL + 'mode=reorderNewQuestions&qsID='+qsID, ualist, function(r) {});
					}
				});						
			}
		});
	},
	
	optionsCheck : function(radioButton,mode){
			if(mode!='Edit') mode='';
			$('#emailcopyCheck'+mode).css('display','none');
			$('#emailvalidateCheck'+mode).css('display','none');
			$('#datetimeOptions'+mode).css('display','none');
			$('#answerSettings'+mode).css('display','none');
			$('#answerOptionsArea'+mode).css('display','none');
			$('#textAreaOption'+mode).css('display','none');
			$('#otherOptions'+mode).css('display','none');
			$('#toolTips'+mode).css('display','none');
			$('#questionRequired'+mode).css('display','none');
			$('#hiddenOption'+mode).css('display','none');
			$('#ratingOption'+mode).css('display','none');
			$('#legendOption'+mode).css('display','none');
			$('#recipientOption'+mode).css('display','none');
			$('#integerOption'+mode).css('display','none');
			$('#customClassDiv'+mode).css('display','none');
			
			if(radioButton.value=='select' || radioButton.value=='radios' || radioButton.value=='checkboxlist'){
				$('#answerOptionsArea'+mode).css('display','block');
				$('#otherOptions'+mode).css('display','block');
				$('#toolTips'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');	
				$('#customClassDiv'+mode).css('display','block');		
			} else if(radioButton.value=='text') {
				$('#textAreaOption'+mode).css('display','block');
				$('#customClassDiv'+mode).css('display','block');
			} else if(radioButton.value=='hr' || radioButton.value=='space' || radioButton.value=='line' || radioButton.value=='fieldset-end'){
				$('#customClassDiv'+mode).css('display','block');
			} else if(radioButton.value=='textarea') {
				$('#answerSettings'+mode).css('display','block');
				$('#answerOptionsArea'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');
				$('#toolTips'+mode).css('display','block');
				$('#customClassDiv'+mode).css('display','block');
			} else if(radioButton.value=='datetime') {
				$('#datetimeOptions'+mode).css('display','block');
				$('#toolTips'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');
				$('#customClassDiv'+mode).css('display','block');	
			} else if(radioButton.value=='email') {
				$('#emailcopyCheck'+mode).css('display','block');
				$('#emailvalidateCheck'+mode).css('display','block');
				$('#toolTips'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');
				$('#customClassDiv'+mode).css('display','block');
			} else if(radioButton.value=='hidden') {
				$('#hiddenOption'+mode).css('display','block');
			} else if(radioButton.value=='rating') {
				$('#ratingOption'+mode).css('display','block');	
				$('#toolTips'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');
				$('#customClassDiv'+mode).css('display','block');	
			} else if(radioButton.value=='fieldset-start') {
				$('#legendOption'+mode).css('display','block');	
				$('#customClassDiv'+mode).css('display','block');
			} else if(radioButton.value=='recipient') {
				$('#recipientOption'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');	
				$('#customClassDiv'+mode).css('display','block');
			} else if(radioButton.value=='integer') {
				$('#toolTips'+mode).css('display','block');
				$('#integerOption'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');	
				$('#customClassDiv'+mode).css('display','block');
			} else {
				$('#toolTips'+mode).css('display','block');
				$('#questionRequired'+mode).css('display','block');
				$('#customClassDiv'+mode).css('display','block');
			}
			
			if ($('#tooltip_check'+mode).attr('checked'))
				$('#tooltipWrap'+mode).css('display','block');	
		
			if ($('#otherOption'+mode).attr('checked'))
				$('#otherOptionWrap'+mode).css('display','block');	
		},
		
	settingsCheck : function(radioButton,mode){
		return true;
		},
		
	addQuestion : function(mode){ 
		var msqID=0;
		if(mode!='Edit') mode='';
		else msqID=parseInt($('#msqID').val());		
		var postStr='question='+encodeURIComponent($('#question'+mode).val())+'&options='+encodeURIComponent($('#answerOptions'+mode).val())
		postStr+= '&textarea='+encodeURIComponent($('#textarea'+mode).val())
		var other=($('#otherOption'+mode).attr('checked'))?1:0;
		postStr+='&other='+other;
		postStr+='&otheroptions='+encodeURIComponent($('#otherOptionTitle'+mode).val());
		var tooltip=($('#tooltip_check'+mode).attr('checked'))?1:0;
		postStr+='&tooltip='+tooltip;
		postStr+='&tooltipText='+encodeURIComponent($('#tooltipText'+mode).val());
		postStr+='&width='+escape($('#width'+mode).val());
		postStr+='&height='+escape($('#height'+mode).val());
		var req=($('#required'+mode).attr('checked'))?1:0;
		postStr+='&required='+req;
		postStr+='&position='+escape($('#position'+mode).val());
				
		var form=document.getElementById('ccm-block-form'); 
		var opts=form['answerType'+mode];
		var answerType='';
		for(var i=0;i<opts.length;i++){
			if(opts[i].checked){
				answerType=opts[i].value;
				break;
			}
		} 
		postStr+='&inputType='+answerType;
		
		var opts=form['answerOptionsDate'+mode];
		var answerOptionsDate='';
		for(var i=0;i<opts.length;i++){
			if(opts[i].checked){
				answerOptionsDate=opts[i].value;
				break;
			}
		} 
		postStr+='&answerOptionsDate='+answerOptionsDate;
		var emailcopy=($('#emailcopy'+mode).attr('checked'))?1:0;
		postStr+='&emailcopy='+emailcopy;
		var emailvalidate=($('#emailvalidate'+mode).attr('checked'))?1:0;
		postStr+='&emailvalidate='+emailvalidate;
		
		postStr+='&hidden='+encodeURIComponent($('#hidden'+mode).val());
		postStr+='&stars='+encodeURIComponent($('#stars'+mode).val());
		postStr+='&legend='+encodeURIComponent($('#legend'+mode).val());
		postStr+='&recipientOptions='+encodeURIComponent($('#recipientOptions'+mode).val());
		postStr+='&integerOptions='+encodeURIComponent($('#integerOptions'+mode).val());
		
		postStr+='&customClass='+encodeURIComponent($('#customClassField'+mode).val());
					
		postStr+='&msqID='+msqID+'&qsID='+parseInt(this.qsID);	
		$.ajax({ 
			type: "POST",
			data: postStr,
			url: this.serviceURL+'mode=addQuestion&qsID='+parseInt(this.qsID),
			success: function(msg){ 
				eval('var jsonObj='+msg);
				if(!jsonObj){
				   alert(ccm_t('ajax-error'));
				}else if(jsonObj.noRequired){
				   alert(ccm_t('complete-required'));
				}else{
				   if(jsonObj.mode=='Edit'){
					   $('#questionEditedMsg').slideDown('slow');
					   setTimeout("$('#questionEditedMsg').slideUp('slow');",5000);
					   if(jsonObj.hideQID){
						   extendedFormSurvey.hideQuestions.push( extendedFormSurvey.edit_qID ); //jsonObj.hideQID); 
						   extendedFormSurvey.edit_qID=0;
					   }
				   }else{
					   $('#questionAddedMsg').slideDown('slow');
					   setTimeout("$('#questionAddedMsg').slideUp('slow');",5000);
					   //extendedFormSurvey.saveOrder();
				   }
				   $('#editQuestionForm').css('display','none');
				   extendedFormSurvey.qsID=jsonObj.qsID;
				   extendedFormSurvey.ignoreQuestionId(jsonObj.msqID);
				   $('#qsID').val(jsonObj.qsID);
				   extendedFormSurvey.resetQuestion();
				   extendedFormSurvey.refreshSurvey();						  
				   //extendedFormSurvey.showPane('preview');
				}
			}
		});
	},
	
	ignoreQuestionId:function(msqID){
		var msqID, ignoreEl=$('#ccm-ignoreQuestionIDs');
		if(ignoreEl.val()) msqIDs=ignoreEl.val().split(',');
		else msqIDs=[];
		msqIDs.push( parseInt(msqID) );
		ignoreEl.val( msqIDs.join(',') );
	},
	
	reloadQuestion : function(qID){			
			$.ajax({ 
				url: this.serviceURL+"mode=getQuestion&qsID="+parseInt(this.qsID)+'&qID='+parseInt(qID),
				success: function(msg){				
						eval('var jsonObj='+msg);
						$('#editQuestionForm').css('display','block')
						$('#questionEdit').val(jsonObj.question);
						$('#answerOptionsEdit').val(jsonObj.optionVals.replace(/%%/g,"\r\n") );
						$('#textareaEdit').val(jsonObj.textarea);
						$('#hiddenEdit').val(jsonObj.hidden);
						$('#legendEdit').val(jsonObj.legend);
						$('#starsEdit').val(jsonObj.stars);
						$('#widthEdit').val(jsonObj.width);
						$('#heightEdit').val(jsonObj.height); 
						$('#positionEdit').val(jsonObj.position); 
						if( parseInt(jsonObj.other)==1 ) 
							 $('#otherOptionEdit').attr('checked', true);
						else $('#otherOptionEdit').attr('checked', false);
						$('#otherOptionTitleEdit').val(jsonObj.otherTitle); 
						if( parseInt(jsonObj.tooltip)==1 ) 
							 $('#tooltip_checkEdit').attr('checked', true);
						else $('#tooltip_checkEdit').attr('checked', false);
						$('#tooltipTextEdit').val(jsonObj.tooltipText); 
						if( parseInt(jsonObj.required)==1 ) 
							 $('#requiredEdit').attr('checked', true);
						else $('#requiredEdit').attr('checked', false);
						if( parseInt(jsonObj.emailcopy)==1 ) 
							 $('#emailcopyEdit').attr('checked', true);
						else $('#emailcopyEdit').attr('checked', false);
						if( parseInt(jsonObj.emailvalidate)==1 ) 
							 $('#emailvalidateEdit').attr('checked', true);
						else $('#emailvalidateEdit').attr('checked', false);
						$('#msqID').val(jsonObj.msqID);    
						$('#recipientOptionsEdit').val(jsonObj.optionVals.replace(/%%/g,"\r\n") );
						$('#integerOptionsEdit').val(jsonObj.integer);
						
						$('#customClassFieldEdit').val(jsonObj.customClass);
						
						for(var i=0;i<extendedFormSurvey.answerOptionsDateEdit.length;i++){							
							if(extendedFormSurvey.answerOptionsDateEdit[i].value==jsonObj.answerOptionsDate){
								extendedFormSurvey.answerOptionsDateEdit[i].checked=true; 
							}
						}
						
						for(var i=0;i<extendedFormSurvey.answerTypesEdit.length;i++){							
							if(extendedFormSurvey.answerTypesEdit[i].value==jsonObj.inputType){
								extendedFormSurvey.answerTypesEdit[i].checked=true; 
								extendedFormSurvey.optionsCheck(extendedFormSurvey.answerTypesEdit[i],'Edit');
								//extendedFormSurvey.settingsCheck(extendedFormSurvey.answerTypesEdit[i],'Edit');
							}
						}
						if(parseInt(jsonObj.bID)>0) 
							extendedFormSurvey.edit_qID = parseInt(qID) ;
						//scroll(0,165);
					}
			});
	},	
	//prevent duplication of these questions, for block question versioning
	pendingDeleteQuestionId:function(msqID){
		var msqID, deleteEl=$('#ccm-pendingDeleteIDs');
		if(deleteEl.val()) msqIDs=deleteEl.val().split(',');
		else msqIDs=[];
		msqIDs.push( parseInt(msqID) );
		deleteEl.val( msqIDs.join(',') );
	},	
	hideQuestions : [], 
	deleteQuestion : function(el,msqID,qID){
			if(confirm(ccm_t('delete-question'))) { 
				$.ajax({ 
					url: this.serviceURL+"mode=delQuestion&qsID="+parseInt(this.qsID)+'&msqID='+parseInt(msqID),
					success: function(msg){	extendedFormSurvey.resetQuestion(); extendedFormSurvey.refreshSurvey();  }			
				});
				
				extendedFormSurvey.ignoreQuestionId(msqID);
				extendedFormSurvey.hideQuestions.push(qID); 
				extendedFormSurvey.pendingDeleteQuestionId(msqID)
			}
	},
	resetQuestion : function(){
			$('#question').val('');
			$('#answerOptions').val('');
			$('#width').val('50');
			$('#height').val('3');
			$('#msqID').val('');
			for(var i=0;i<this.answerTypes.length;i++){
				this.answerTypes[i].checked=false;
			}
			$('#answerOptionsArea').hide();
			$('#datetimeOptions').hide();			
			$('#answerSettings').hide();
			$('#required').attr('checked',false)
			$('#otherOptionsWrap').hide();
			$('#otherOption').attr('checked',false);
			$('#otherOptionTitle').val('');
			$('#toolTipsWrap').hide();
			$('#tooltip_check').attr('checked',false);
			$('#tooltipText').val('');
			$('#emailcopy').attr('checked',false);
			$('#emailcopyCheck').hide();
			$('#emailvalidate').attr('checked',false);
			$('#emailvalidateCheck').hide();
			$('#textarea').val('');
			$('#textAreaOption').hide();
			$('#hidden').val('');
			$('#hiddenOption').hide();
			$('#stars').val('');
			$('#ratingOption').hide();
			$('#legend').val('');
			$('#legendOption').hide();
			$('#recipientOptions').val('');
			$('#recipientOption').hide();
			$('#integerOptions').val('');
			$('#integerOption').hide();
			$('#customClassField').val('');
			$('#customClassDiv').hide();
	},
	
	validate:function(){
			var failed=0;
			
			var n=$('#ccmSurveyName');
			if( !n || parseInt(n.val().length)==0 ){
				alert(ccm_t('form-name'));
				this.showPane('options');
				n.focus();
				failed=1;
			}
			
			var Qs=$('.extendedFormSurveyQuestionRow'); 
			if( !Qs || parseInt(Qs.length)<1 ){
				alert(ccm_t('form-min-1'));
				failed=1;
			}
			
			if(failed){
				ccm_isBlockError=1;
				return false;
			}
			return true;
	},
	
	moveUp:function(el,thisQID){
		var qIDs=this.serialize();
		var previousQID=0;
		for(var i=0;i<qIDs.length;i++){
			if(qIDs[i]==thisQID){
				if(previousQID==0) break; 
				$('#extendedFormSurveyQuestionRow'+thisQID).after($('#extendedFormSurveyQuestionRow'+previousQID));
				break;
			}
			previousQID=qIDs[i];
		}	
		this.saveOrder();
	},
	
	moveDown:function(el,thisQID){
		var qIDs=this.serialize();
		var thisQIDfound=0;
		for(var i=0;i<qIDs.length;i++){
			if(qIDs[i]==thisQID){
				thisQIDfound=1;
				continue;
			}
			if(thisQIDfound){
				$('#extendedFormSurveyQuestionRow'+qIDs[i]).after($('#extendedFormSurveyQuestionRow'+thisQID));
				break;
			}
		}
		this.saveOrder();
	},
	
	serialize:function(){
		var t = document.getElementById("extendedFormSurveyPreviewTable");
		var qIDs=[];
		for(var i=0;i<t.childNodes.length;i++){ 
			if( t.childNodes[i].className && t.childNodes[i].className.indexOf('extendedFormSurveyQuestionRow')>=0 ){ 
				var qID=t.childNodes[i].id.substr('extendedFormSurveyQuestionRow'.length);
				qIDs.push(qID);
			}
		}
		return qIDs;
	},
	
	saveOrder:function(){ 
		var postStr='qIDs='+this.serialize().join(',')+'&qsID='+parseInt(this.qsID);
		$.ajax({ 
			type: "POST",
			data: postStr,
			url: this.serviceURL+"mode=reorderQuestions",			
			success: function(msg){	
				extendedFormSurvey.refreshSurvey();
			}			
		});
	},
	
	showRecipient:function(cb){ 
		if(cb.checked) $('#recipientEmailWrap').css('display','block');
		else $('#recipientEmailWrap').css('display','none');
	},
	showSender:function(cb){ 
		if(cb.checked) $('#senderWrap').css('display','block');
		else $('#senderWrap').css('display','none');
	},
	showOtherOption:function(cb, mode){ 
		if(mode!='Edit') mode='';
		if(cb.checked) $('#otherOptionWrap'+mode).css('display','block');
		else $('#otherOptionWrap'+mode).css('display','none');
	},
	showTooltip:function(cb, mode){ 
		if(mode!='Edit') mode=''; 
		if(cb.checked) $('#tooltipWrap'+mode).css('display','block');
		else $('#tooltipWrap'+mode).css('display','none');
	},
	showCaptcha:function(cb){ 
		if(cb.checked) $('#captchaWrap').css('display','block');
		else $('#captchaWrap').css('display','none');
	},
	showReCaptcha:function(cb){ 
		if(cb.checked) {
			$('#recaptchaWrap').css('display','block');
			$('#normalcaptchaWrap').css('display','none');
		} else {
			$('#recaptchaWrap').css('display','none');
			$('#normalcaptchaWrap').css('display','block');
		}
	},
	showAttachment:function(cb){ 
		if(cb.checked) $('#attachmentWrap').css('display','block');
		else $('#attachmentWrap').css('display','none');
	},
	showRequired:function(cb){ 
		if(cb.checked) $('#requiredWrap').css('display','block');
		else $('#requiredWrap').css('display','none');
	}
}
ccmValidateBlockForm = function() { return extendedFormSurvey.validate(); }

//$(document).ready(function(){
	//extendedFormSurvey.init();
	$('#ccm-form-redirect').change(function() {
		if($(this).is(':checked')) {
			$('#ccm-form-redirect-page').show();
		} else {
			$('#ccm-form-redirect-page').hide();
		}
	});
	$('.formBlockSurveyTable .tooltip_holder').tooltip({ 
		track: true, 
		delay: 0, 
		showURL: false, 
		fade: 250 
	});	
//});
