<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));
//$extendedFormSurveyInfo['surveyName']= $bs->surveyName;
Loader::model('survey', 'extended_form');

$extendedFormSurvey=new ExtendedFormSurvey($b);
$extendedFormSurveyInfo=$extendedFormSurvey->getExtendedFormSurveyBlockInfo( $b->getBlockID() );
ExtendedFormSurvey::questionCleanup( intval($extendedFormSurveyInfo['questionSetId']), $b->getBlockID() );

$u=new User();
$ui=UserInfo::getByID($u->uID);
if( strlen(trim($extendedFormSurveyInfo['recipientEmail']))==0 )
	$extendedFormSurveyInfo['recipientEmail']=$ui->uEmail;
?>

<script>
var thisbID=parseInt(<?php     echo $b->getBlockID()?>); 
var thisbtID=parseInt(<?php     echo $b->getBlockTypeID()?>); 
</script>

<?php      include('styles_include.php'); ?>
<?php      include('form_setup_html.php'); ?>
