<?php      defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<style type="text/css">
div.fieldRow{ border-top: 1px solid #ccc; padding:8px 4px ; margin:8px; clear:left}
div.fieldRow div.fieldLabel{float: left; width:30%; text-align:right; padding-top: 6px; padding-right:8px}
div.fieldRow div.fieldValues{float: left; width:65%}

#ccm-formBlockPane-options div.fieldLabel{float: left; width:30%; text-align:right; padding-top: 6px; padding-right:8px}
#ccm-formBlockPane-options div.fieldValues{float: left; width:65%}
div#newQuestionBox{ border:1px solid #ccc; padding:8px; margin-top:24px }

.spacer{ clear:both; font-size:1px } 
#answerOptionsArea, #answerOptionsAreaEdit{display:none}
#answerSettings, #answerSettingsEdit{display:none}
#editQuestionForm .formBlockSubmitButton{display:none}
.formBlockQuestionMsg{ background:#FFFF99; padding:2px; margin:16px 0px; border:1px solid #ddd; display:none; }

#recipientEmailWrap{margin-top:8px}

#extendedFormSurveyTableWrap { position:inherit; }
#extendedFormSurveyTableWrap .extendedFormSurveyTable{ width:100%; position:inherit; }
.extendedFormSurveyQuestionRow{ position:inherit; clear:both; width:100%; margin-bottom:16px; font-family:Arial, Helvetica, sans-serif; font-size:11px; padding:0px }
.extendedFormSurveyQuestionRowActive{}
.extendedFormSurveyQuestionRowHelper{ z-index:500; background:#fafafa; border:1px dashed #999; width:auto }
.extendedFormSurveyQuestionRow .extendedFormSurveyQuestion{ float:left; width:66%}
.extendedFormSurveyQuestionRow .extendedFormSurveyResponse{ float:left; width:55%}
.extendedFormSurveyQuestionRow .extendedFormSurveyOptions{ float:left; width:28%; font-size:11px}
.extendedFormSurveyQuestionRow .extendedFormSurveyOptions a.moveUpLink{ display:block; background:url(<?php     echo DIR_REL?>/concrete/images/icons/arrow_up.png) no-repeat center; height:10px; width:16px; }
.extendedFormSurveyQuestionRow .extendedFormSurveyOptions a.moveDownLink{ display:block; background:url(<?php     echo DIR_REL?>/concrete/images/icons/arrow_down.png) no-repeat center; height:10px; width:16px; }
.extendedFormSurveyQuestionRow .extendedFormSurveyOptions a.moveUpLink:hover{background:url(<?php     echo DIR_REL?>/concrete/images/icons/arrow_up_black.png) no-repeat center;}
.extendedFormSurveyQuestionRow .extendedFormSurveyOptions a.moveDownLink:hover{background:url(<?php     echo DIR_REL?>/concrete/images/icons/arrow_down_black.png) no-repeat center;}
.extendedFormSurveyQuestionRow .extendedFormSurveySpacer{font-size:1px; line-height:1px; clear:both; }

.noborder { border:0px !important; }
.extendedFormSurveyQuestionRow .extendedFormSurveyMover {cursor:move;background-image:url(/concrete/images/icons/up_down.png);background-repeat:no-repeat;background-position: center left;height: 29px;width: 26px;float:left}
</style>
