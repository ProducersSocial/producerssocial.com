<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('survey', 'extended_form');
$extendedFormSurvey = new ExtendedFormSurvey();

//Permissions Check
if($_GET['cID'] && $_GET['arHandle']) {
	$c = Page::getByID($_GET['cID'], 'RECENT');
	$a = Area::get($c, $_GET['arHandle']);  
	if(intval($_GET['bID'])==0){ 
		$ap = new Permissions($a);	
		$bt = BlockType::getByID($_GET['btID']);	
		if(!$ap->canAddBlock($bt)) $badPermissions=true;
	}else{
		$b = Block::getByID($_GET['bID'], $c, $a);
		$bp = new Permissions($b);
		if( !$bp->canWrite() ) $badPermissions=true;
	}
}else $badPermissions=true;
if($badPermissions){
	echo t('Invalid Permissions');
	die;
} 
switch ($_GET['mode']){

	case 'addQuestion':
		$extendedFormSurvey->addEditQuestion($_POST);
		break;
		
	case 'getQuestion':
		$extendedFormSurvey->getQuestionInfo( intval($_GET['qsID']), intval($_GET['qID']) );
		break;			
		
	case 'delQuestion':
		$extendedFormSurvey->deleteQuestion(intval($_GET['qsID']),intval($_GET['msqID']));
		break;			
		
	case 'reorderQuestions':
		$extendedFormSurvey->reorderQuestions(intval($_POST['qsID']),$_POST['qIDs']);
		break;
	
	case 'reorderNewQuestions':
		$extendedFormSurvey->reorderNewQuestions(intval($_GET['qsID']), $_POST['extendedFormSurveyQuestionRow']);
		break;
					
	case 'refreshSurvey':
	default: 
		$showEdit=(intval($_REQUEST['showEdit'])==1)?true:false; 
		$extendedFormSurvey->loadSurvey( intval($_GET['qsID']), $showEdit, intval($_GET['bID']), explode(',',$_GET['hide']), 1 ); 
}

?>
