<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));
$uh = Loader::helper('concrete/urls'); ?>

<ul class="ccm-dialog-tabs" id="ccm-formblock-tabs">
	<li class="<?php  echo (intval($extendedFormSurveyInfo['bID']) == 0)?'ccm-nav-active':''?>"><a href="javascript:void(0)" id="ccm-formblock-tab-add"><?php  echo t('Add')?></a></li>
	<li class="<?php  echo (intval($extendedFormSurveyInfo['bID']) > 0)?'ccm-nav-active':''?>"><a href="javascript:void(0)" id="ccm-formblock-tab-edit"><?php  echo t('Edit')?></a></li>
	<li><a href="javascript:void(0)" id="ccm-formblock-tab-preview"><?php  echo t('Preview')?></a></li>
	<li><a href="javascript:void(0)" id="ccm-formblock-tab-options"><?php  echo t('Options')?></a></li>
    <!-- <li><a href="javascript:void(0)" id="ccm-formblock-tab-mailing"><?php  echo t('Messages')?></a></li>-->
    <li><a href="javascript:void(0)" id="ccm-formblock-tab-remarks"><?php  echo t('Help')?></a></li>
</ul>

<input type="hidden" id="rcID" name="rcID" value="1" />

<input type="hidden" name="extendedFormSurveyServices" value="<?php     echo $uh->getBlockTypeToolsURL($bt)?>/services.php" />

<input type="hidden" id="ccm-ignoreQuestionIDs" name="ignoreQuestionIDs" value="" />
<input type="hidden" id="ccm-pendingDeleteIDs" name="pendingDeleteIDs" value="" />

<div id="ccm-formBlockPane-options" class="ccm-formBlockPane ccm-ui">

	<?php     
	$c = Page::getCurrentPage();
	if(strlen($extendedFormSurveyInfo['surveyName'])==0)
		$extendedFormSurveyInfo['surveyName']=$c->getCollectionName();
	?>
	
    <strong><?php    echo t('Form options and behavior') ?></strong>
    <div class="ccm-note"><?php    echo t('Change the following settings to let the form perform as you wish.') ?></div>
	
	<div class="fieldRow">
		<div class="fieldLabel"><?php     echo t('Formname')?>:</div>
		<div class="fieldValues">
			<input id="ccmSurveyName" name="surveyName" style="width: 95%" type="text" class="ccm-input-text" value="<?php     echo $extendedFormSurveyInfo['surveyName']?>" />
		</div>
		<div class="ccm-spacer"></div>
	</div>	
    
    <div class="fieldRow">
		<div class="fieldLabel" style=""><?php    echo t("Use captcha")?>:</div>
        <div class="fieldValues">
        <input name="displayCaptcha" value="1" <?php     echo (intval($extendedFormSurveyInfo['displayCaptcha'])>=1)?'checked="checked"':''?> type="checkbox" onchange="extendedFormSurvey.showCaptcha(this)" onclick="extendedFormSurvey.showCaptcha(this)" />
        </div>
        <div id="captchaWrap" class="fieldRow noborder" style=" <?php     echo (intval($extendedFormSurveyInfo['displayCaptcha'])==0)?'display:none':''?>">
			
            <div class="fieldLabel" style=""><?php    echo t("Use Google ReCaptcha")?>:</div>
            <div class="fieldValues">
            <input name="useReCaptcha" value="1" <?php     echo (intval($extendedFormSurveyInfo['useReCaptcha'])>=1)?'checked="checked"':''?> type="checkbox" onchange="extendedFormSurvey.showReCaptcha(this)" onclick="extendedFormSurvey.showReCaptcha(this)" />
            </div>
            
            <div id="recaptchaWrap" class="fieldRow noborder" style=" <?php     echo (intval($extendedFormSurveyInfo['useReCaptcha'])==0)?'display:none':''?>">
           		<div class="fieldLabel">&nbsp;</div>
                <div class="fieldValues">
                	<div class="ccm-note"><?php    echo t('<a href="https://www.google.com/recaptcha/admin/create" target="_blank">Sign Up for reCaptcha from Google</a>. Use the information from your registration to add the following details to the Extended Form.') ?></div>
                </div>
            
                <div class="fieldLabel" style=""><?php    echo t("Public Key")?>:</div>
                <div class="fieldValues"> 
                     <input type="text" style="width: 95%" name="publickeyReCaptcha" class="ccm-input-text" value="<?php    echo $this->controller->publickeyReCaptcha ?>"/>
               	     <div class="ccm-note">Add your public key here.</div>
                </div>
                
                <div class="fieldLabel" style=""><?php    echo t("Private Key")?>:</div>
                <div class="fieldValues"> 
                     <input type="text" style="width: 95%" name="privatekeyReCaptcha" class="ccm-input-text" value="<?php    echo $this->controller->privatekeyReCaptcha ?>"/>
               	     <div class="ccm-note">Add your private key here.</div>
                </div>
            
            </div>
            
            <div id="normalcaptchaWrap" class="fieldRow noborder" style=" <?php     echo (intval($extendedFormSurveyInfo['useReCaptcha'])!=0)?'display:none':''?>">
            
                <div class="fieldLabel" style=""><?php    echo t("Title captcha")?>:</div>
                <div class="fieldValues"> 
                     <input type="text" style="width: 95%" name="titleCaptcha" class="ccm-input-text" value="<?php    echo $this->controller->titleCaptcha ?>"/>
                </div>
                <div class="ccm-spacer"></div>
                <div class="fieldLabel" style=""><?php    echo t("Comment captcha")?>:</div>
                <div class="fieldValues"> 
                    <input type="text" style="width: 95%" name="commentCaptcha" class="ccm-input-text" value="<?php    echo $this->controller->commentCaptcha ?>"/>
                </div>
            
            </div>
            
        </div>
		<div class="ccm-spacer"></div>
	</div>
    
    <div class="fieldRow">
    <div class="fieldLabel" style=""><?php    echo t("Show 'Required fields'-text")?>:</div>
    <div class="fieldValues">
        <input name="displayRequired" id="displayRequired"  value="1" type="checkbox" <?php     echo (intval($extendedFormSurveyInfo['displayRequired'])>=1)?'checked="checked"':''?> onchange="extendedFormSurvey.showRequired(this)" onclick="extendedFormSurvey.showRequired(this)" />
    </div>
    <div id="requiredWrap" class="fieldRow noborder" style=" <?php     echo (intval($extendedFormSurveyInfo['displayRequired'])==0)?'display:none':''?>">
        <div class="fieldLabel" style=""><?php    echo t("'Required fields'-text")?>:</div>
        <div class="fieldValues"> 
             <input type="text" style="width: 95%" name="titleRequired" id="titleRequired" value="<?php    echo $this->controller->titleRequired ?>"  class="ccm-input-text" />
        </div>
    </div>
    <div class="ccm-spacer"></div>
    </div>
        
    <div class="fieldRow">
		<div class="fieldLabel"><?php     echo t('Title submit-button')?>:</div>
		<div class="fieldValues">
			<input id="titleSubmit" name="titleSubmit" style="width: 95%" type="text" class="ccm-input-text" value="<?php    echo $this->controller->titleSubmit ?>" />
		</div>
		<div class="ccm-spacer"></div>
	</div>
	
    <div class="ccm-spacer"></div>
    <br /><br />
    <strong><?php    echo t('Custom sender') ?></strong>
    <div class="ccm-note"><?php    echo t('Instead of sending the formfiller a mail with the admin as sender, you can add your own custom sender here. Just add name and e-mailaddress.') ?></div>
    
    <div class="fieldRow" style="margin-top:16px">
		<div class="fieldLabel" style=""><?php     echo t('Custom sender')?>: </div>
        <div class="fieldValues">
		<input name="displaySender" type="checkbox" value="1" <?php     echo (intval($extendedFormSurveyInfo['displaySender'])>=1)?'checked="checked"':''?> onchange="extendedFormSurvey.showSender(this)" onclick="extendedFormSurvey.showSender(this)" />
        </div>
		<div id="senderWrap" class="fieldRow noborder" style=" <?php     echo (intval($extendedFormSurveyInfo['displaySender'])==0)?'display:none':''?>">
			<div class="fieldLabel"><?php     echo t('Name')?>:</div>
			<div class="fieldValues">
		     <input name="sendFromName" value="<?php  echo $extendedFormSurveyInfo['sendFromName']?>" type="text" class="ccm-input-text" maxlength="128" style="width: 95%" />
			</div>
			<div class="ccm-spacer"></div>
            
            <div class="fieldLabel"><?php     echo t('E-mailaddress')?>:</div>
			<div class="fieldValues">
		     <input name="sendFromEmail" value="<?php  echo $extendedFormSurveyInfo['sendFromEmail']?>" type="text" class="ccm-input-text" maxlength="128" style="width: 95%" />
			</div>
			<div class="ccm-spacer"></div>
		</div>
	</div>	
	
    <div class="ccm-spacer"></div>
    <br /><br />
    <strong><?php    echo t('Notify someone') ?></strong>
    <div class="ccm-note"><?php    echo t('Send an e-mail when the form is submitted. The data of the form will always be saved into the dashboard, this just sends a notification that the form is filled out.') ?></div>
    
    <div class="fieldRow" style="margin-top:16px">
		<div class="fieldLabel" style=""><?php     echo t('Notify me')?>: </div>
        <div class="fieldValues">
		<input name="notifyMeOnSubmission" type="checkbox" value="1" <?php     echo (intval($extendedFormSurveyInfo['notifyMeOnSubmission'])>=1)?'checked="checked"':''?> onchange="extendedFormSurvey.showRecipient(this)" onclick="extendedFormSurvey.showRecipient(this)" />
        </div>
		<div id="recipientEmailWrap" class="fieldRow noborder" style=" <?php     echo (intval($extendedFormSurveyInfo['notifyMeOnSubmission'])==0)?'display:none':''?>">
			<div class="fieldLabel"><?php     echo t('E-mailaddress')?>:</div>
			<div class="fieldValues">
			 <input name="recipientEmail" value="<?php    echo $extendedFormSurveyInfo['recipientEmail']?>" type="text" class="ccm-input-text" maxlength="128" style="width: 95%" />
			<div class="ccm-note"><?php    echo  t('(Seperate with a comma)')?></div>
			</div>
			<div class="ccm-spacer"></div>
		</div>
	</div> 
	
    <div class="ccm-spacer"></div>
    <br /><br />
    <strong><?php    echo t('Capture some more....') ?></strong>
    <div class="ccm-note"><?php    echo t('When the form is filled out by a loggedin user you can let the form grab the user and save it along with the form fields. This will capture userID and Username. You can also capture the remote IP address of the form filler. When you want to know which page the form was filled from, you just have to capture the PageID.') ?></div>
    
    <div class="fieldRow">
		<div class="fieldLabel" style=""><?php    echo t("Capture User")?>:</div>
        <div class="fieldValues">
        <input name="captureUser" value="1" <?php     echo (intval($extendedFormSurveyInfo['captureUser'])>=1)?'checked="checked"':''?> type="checkbox" />
        </div>
    </div>
    <div class="ccm-spacer"></div>
    <div class="fieldRow">
		<div class="fieldLabel" style=""><?php    echo t("Capture Remote IP")?>:</div>
        <div class="fieldValues">
        <input name="captureIP" value="1" <?php     echo (intval($extendedFormSurveyInfo['captureIP'])>=1)?'checked="checked"':''?> type="checkbox" />
        </div>
    </div>
    <div class="ccm-spacer"></div>
    <div class="fieldRow">
		<div class="fieldLabel" style=""><?php    echo t("Capture PageID")?>:</div>
        <div class="fieldValues">
        <input name="capturePageID" value="1" <?php     echo (intval($extendedFormSurveyInfo['capturePageID'])>=1)?'checked="checked"':''?> type="checkbox" />
        </div>
    </div>
    
    <div class="ccm-spacer"></div>
    <br /><br />
    <strong><?php    echo t('After submission') ?></strong>
    <div class="ccm-note"><?php    echo t('What to do when the form is submitted...') ?></div>    
    
    <div class="fieldRow">
    <div class="fieldLabel" style="white-space:nowrap;"><?php     echo t('Subject e-mail')?>:</div>
    <div class="fieldValues">
     <input name="emailSubject" style="width: 95%" type="text" class="ccm-input-text" value="<?php    echo $this->controller->emailSubject ?>" />
    <div class="ccm-note"><?php    echo t('Use %s to add formname to the subject.') ?></div>
    </div>
    </div>
    
    <div class="fieldRow">
		<div class="fieldLabel" style=""><?php     echo t('Add attachment to mail');?>: </div>
        <div class="fieldValues">
			<input id="attachment" name="attachment" value="1" <?php     echo (intval($extendedFormSurveyInfo['attachment'])>=1)?'checked="checked"':''?> type="checkbox" onchange="extendedFormSurvey.showAttachment(this)" onclick="extendedFormSurvey.showAttachment(this)" />
        </div>
        <div class="ccm-note"><?php    echo  t('(Sends an attachment along with the notification mail')?></div>
		<div id="attachmentWrap" class="fieldRow noborder" <?php     echo (intval($extendedFormSurveyInfo['attachment'])>=1)?'':'style="display:none"'; ?>>
        <div class="fieldLabel" style=""><?php    echo t("Select file")?>:</div>
        <div class="fieldValues"> 
		<?php    
		$form = Loader::helper('concrete/asset_library');
		if ($extendedFormSurveyInfo['attachmentCID'])
			$_POST['attachmentCID'] = $extendedFormSurveyInfo['attachmentCID'];
		print $form->file('attachmentCID', 'attachmentCID', 'Choose File');
		?>
		</div>
   		</div>
    	<div class="ccm-spacer"></div>
    </div>
    
    <div class="fieldRow">
		<div class="fieldLabel" style=""><?php     echo t('Message after submission')?>:</div>
		<div class="fieldValues"> 
			<textarea name="thankyouMsg" cols="50" rows="4" style="width: 95%" class="ccm-input-text" ><?php    echo $this->controller->thankyouMsg ?></textarea>
		</div>
		<div class="ccm-spacer"></div>
	</div>   
       	
	<div class="fieldRow">
		<div class="fieldLabel" style=""><?php     echo t('Redirect to page after submission?');?></div>
		<div class="fieldValues"> 
        	<input id="ccm-form-redirect" name="redirect" value="1" <?php     echo (intval($extendedFormSurveyInfo['redirectCID'])>=1)?'checked="checked"':''?> type="checkbox" />
        </div>
		<div id="ccm-form-redirect-page" class="fieldRow noborder" <?php     echo (intval($extendedFormSurveyInfo['redirectCID'])>=1)?'':'style="display:none"'; ?>>
		<div class="fieldLabel" style=""><?php    echo t("Select page")?>:</div>
        <div class="fieldValues"> 
		<?php    
		$form = Loader::helper('form/page_selector');
		if ($extendedFormSurveyInfo['redirectCID']) {
			print $form->selectPage('redirectCID', $extendedFormSurveyInfo['redirectCID']);
		} else {
			print $form->selectPage('redirectCID');
		}
		?>
		</div>
   		</div>
    	<div class="ccm-spacer"></div>
    </div>
    
    <div class="ccm-spacer"></div>
    <br /><br />
    <strong><?php    echo t('Custom Class for the form') ?></strong>
    <div class="ccm-note"><?php    echo t('This is only for advanced users. Advanced users can add a class to the form. This enables the user to add there own custom settings to there stylesheet. Enter the name of the class you wish to use.') ?></div>
    
    <div class="fieldRow">
    <div class="fieldLabel" style="white-space:nowrap;"><?php     echo t('Custom Class')?>:</div>
    <div class="fieldValues">
     <input name="customClass" style="width: 95%" type="text" class="ccm-input-text" value="<?php    echo $this->controller->customClass ?>" />
    <div class="ccm-note"><?php    echo t('Insert the name of the class you want to use. Without the dot (.)') ?></div>
    </div>
    </div>
	
</div> 

<input type="hidden" id="qsID" name="qsID" value="<?php     echo intval($extendedFormSurveyInfo['questionSetId'])?>" />
<input type="hidden" id="oldQsID" name="oldQsID" value="<?php     echo intval($extendedFormSurveyInfo['questionSetId'])?>" />
<input type="hidden" id="bID" name="bID" value="<?php     echo intval($extendedFormSurveyInfo['bID'])?>" />
<input type="hidden" id="msqID" name="msqID" value="<?php     echo intval($msqID)?>" />

<div id="ccm-formBlockPane-add" class="ccm-formBlockPane  ccm-ui" style=" <?php     echo (intval($extendedFormSurveyInfo['bID'])==0)?'display:block':''?> ">
	
    <strong><?php    echo t('Add new item') ?></strong>
    <div class="ccm-note"><?php    echo t('Add a new item to the form.') ?></div>		
		
	<div id="questionAddedMsg" class="formBlockQuestionMsg"><?php     echo t('Your item has been added. To view it click the preview tab.')?></div>
		
    <div class="fieldRow">
        <div class="fieldLabel"><?php     echo t('Title')?>:</div>
        <div class="fieldValues">
            <input id="question" name="question" type="text" style="width: 95%" class="ccm-input-text" />
        </div>
        <div class="ccm-spacer"></div>
    </div>	
    <div class="fieldRow">
   		<div class="fieldLabel"><?php     echo t('Type')?>:</div>
        <div class="fieldValues" style="width: 30%; margin-top: 6px; margin-left: 4px;">
            <strong>Formfields</strong> &nbsp; <br>
            <input name="answerType" type="radio" value="field" /> <?php     echo t('Text Field')?> &nbsp; <br>
            <input name="answerType" type="radio" value="email" /> <?php     echo t('E-mailaddress Field')?> &nbsp; <br>
            <input name="answerType" type="radio" value="textarea" /> <?php     echo t('Textarea ')?> &nbsp; <br>
            <input name="answerType" type="radio" value="tinymce" /> <?php      echo t('TinyMCE Textarea ')?> &nbsp; <br>
            <input name="answerType" type="radio" value="integer" /> <?php     echo t('Integer')?> &nbsp; <br>
            <input name="answerType" type="radio" value="radios" /> <?php     echo t('Radiobuttons')?> &nbsp; <br>
            <input name="answerType" type="radio" value="select" /> <?php     echo t('Selectbox' )?> &nbsp; <br>
            <input name="answerType" type="radio" value="checkboxlist" /> <?php     echo t('Checkboxes')?> &nbsp; <br>
            <input name="answerType" type="radio" value="datetime" /> <?php     echo t('Date/time')?> &nbsp; <br>
            <input name="answerType" type="radio" value="hidden" /> <?php     echo t('Hidden Field')?> &nbsp; <br>
            <input name="answerType" type="radio" value="fileupload" /> <?php     echo t('Fileupload')?> &nbsp; <br>
            <input name="answerType" type="radio" value="rating" /> <?php     echo t('Rating')?> &nbsp; <br>
         </div> 
         <div class="fieldValues" style="width: 30%; margin-top: 6px;">  
            <strong>Content items</strong> &nbsp; <br>
            <input name="answerType" type="radio" value="line" /> <?php     echo t('Title (H4)')?> &nbsp; <br>
            <input name="answerType" type="radio" value="text" /> <?php     echo t('Text (P)')?> &nbsp; <br>
            <input name="answerType" type="radio" value="hr" /> <?php     echo t('Horizontal rule')?> &nbsp; <br>
            <input name="answerType" type="radio" value="space" /> <?php     echo t('Empty line')?> &nbsp; <br>
            <input name="answerType" type="radio" value="fieldset-start" /> <?php     echo t('Field set (start)')?> &nbsp; <br>
            <input name="answerType" type="radio" value="fieldset-end" /> <?php     echo t('Field set (end)')?> &nbsp; <br>
            <br /><br />
            <strong>Others</strong> &nbsp; <br>
            <input name="answerType" type="radio" value="recipient" /> <?php     echo t('Recipient selector')?> &nbsp; <br>
        </div>
        <div class="spacer"></div>
    </div>
		
        <div class="fieldRow" id="recipientOption" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Recipients")?>:</div>
        <div class="fieldValues">
        	<textarea id="recipientOptions" name="recipientOptions" cols="50" rows="4" style="width:235px" class="ccm-input-text"></textarea><br />
            <div class="ccm-note"><?php     echo t('Put each recipient (email;name) on a new line. Example: yourname@domain.com;Firstname Lastname')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="integerOption" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Length")?>:</div>
        <div class="fieldValues">
        <input type="text" style="width: 95%" name="integerOptions" id="integerOptions" value="" class="ccm-input-text" />
        <div class="ccm-note"><?php     echo t('Add the length of the numbers. Add 99 to unlimited length.')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="legendOption" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Legend")?>:</div>
        <div class="fieldValues">
        	<input type="text" style="width: 95%" name="legend" id="legend" value="" class="ccm-input-text" />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="ratingOption" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Stars")?>:</div>
        <div class="fieldValues">
        	<input type="text" style="width: 50px" name="stars" id="stars" value="" class="ccm-input-text" />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="hiddenOption" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Value")?>:</div>
        <div class="fieldValues">
        	<input type="text" style="width: 95%" name="hidden" id="hidden" value="" class="ccm-input-text" />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="textAreaOption" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Text")?>:</div>
        <div class="fieldValues">
        	<textarea id="textarea" name="textarea" cols="50" rows="4" style="width:235px" class="ccm-input-text"></textarea><br />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="emailcopyCheck" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Send copy")?>:</div>
        <div class="fieldValues">
        	<input name="emailcopy" id="emailcopy"  value="1" type="checkbox" />
            <div class="ccm-note"><?php    echo  t('(Sends a copy of the submission to this e-mailaddress)')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="emailvalidateCheck" style="display:none;">
		<div class="fieldLabel" style=""><?php   echo t("Require validation")?>:</div>
        <div class="fieldValues">
        	<input name="emailvalidate" id="emailvalidate"  value="1" type="checkbox" />
            <div class="ccm-note"><?php   echo  t('(Requires a user to enter this e-mailaddress twice)')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="datetimeOptions" style="display:none;">
			<div class="fieldLabel"><?php     echo t('Date/time options')?>: </div>
			<div class="fieldValues">
				<input name="answerOptionsDate" type="radio" value="datetime" /> <?php     echo t('Date and time')?> &nbsp; <br>
                <input name="answerOptionsDate" type="radio" value="date" /> <?php     echo t('Date only')?> &nbsp; <br>
                <input name="answerOptionsDate" type="radio" value="time" /> <?php     echo t('Time only')?> &nbsp; <br>
			</div>
			<div class="ccm-spacer"></div>
		</div>
        
		<div class="fieldRow" id="answerOptionsArea" style="display:none;">
			<div class="fieldLabel"><?php     echo t('Content options')?>: </div>
			<div class="fieldValues">
				<textarea id="answerOptions" name="answerOptions" cols="50" rows="4" style="width:235px" class="ccm-input-text"></textarea><br />
				<?php     echo t('Put each answer on a new line')?>
			</div>
			<div class="ccm-spacer"></div>
		</div>
		
		
		<div class="fieldRow" id="answerSettings" style="display:none;">
			<div class="fieldLabel"><?php     echo t('Settings')?>: </div>
			<div class="fieldValues">
				<?php     echo t('Textarea width')?>: <input id="width" name="width" type="text" value="50" size="3" class="ccm-input-text" /> <br />
				<?php     echo t('Textarea height')?>: <input id="height" name="height" type="text" value="3" size="2" class="ccm-input-text" />
			</div>
			<div class="ccm-spacer"></div>
		</div>
		
        <div class="fieldRow" id="otherOptions" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Other option")?>:</div>
        <div class="fieldValues"> 
       		<input name="otherOption" id="otherOption" value="1" type="checkbox" onchange="extendedFormSurvey.showOtherOption(this)" onclick="extendedFormSurvey.showOtherOption(this)" />
        </div>
        <div id="otherOptionWrap" class="fieldRow noborder" style="display:none;">
			<div class="fieldLabel" style=""><?php    echo t("Title other option")?>:</div>
            <div class="fieldValues"> 
                 <input type="text" style="width: 95%" name="otherOptionTitle" id="otherOptionTitle" value="" class="ccm-input-text" />
            </div>
        </div>
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="toolTips">
		<div class="fieldLabel" style=""><?php    echo t("Tooltip")?>:</div>
        <div class="fieldValues">
        	<input name="tooltip_check" id="tooltip_check"  value="1" type="checkbox" onchange="extendedFormSurvey.showTooltip(this)" onclick="extendedFormSurvey.showTooltip(this)" />
        </div>
        <div id="tooltipWrap" class="fieldRow noborder" style="display:none;">
			<div class="fieldLabel" style=""><?php    echo t("Tooltip text")?>:</div>
            <div class="fieldValues"> 
                 <input type="text" style="width: 95%" name="tooltipText" id="tooltipText" value=""  class="ccm-input-text" />
            </div>
        </div>
		<div class="ccm-spacer"></div>
		</div>
        
		<div class="fieldRow" id="questionRequired">
			<div class="fieldLabel">&nbsp;</div>
			<div class="fieldValues"> 
				<input id="required" name="required" type="checkbox" value="1" />
				<?php     echo t('This item is required.')?> 
			</div>
			<div class="ccm-spacer"></div>
		</div>		
		
        <div id="customClassDiv">
        <strong><?php    echo t('Custom Class for this field/item') ?>:</strong>
        <div class="ccm-note"><?php    echo t('This is only for advanced users. Advanced users can add a class to the field/item. This enables the user to add there own custom settings to there stylesheet. Enter the name of the class you wish to use.') ?></div>
        
        <div class="fieldRow">
        <div class="fieldLabel" style="white-space:nowrap;"><?php     echo t('Custom Class')?>:</div>
        <div class="fieldValues">
         <input name="customClassField" id="customClassField" style="width: 95%" type="text" class="ccm-input-text" value="" />
        <div class="ccm-note"><?php    echo t('Insert the name of the class you want to use. Without the dot (.)') ?></div>
        </div>
        </div>
        <div class="ccm-spacer"></div>
		</div>
        
		<div class="fieldRow">
			<div class="fieldLabel">&nbsp; </div>
			<div class="fieldValues" >
				<input type="hidden" id="position" name="position" value="" />
				<input id="refreshButton" name="refresh" type="button" value="Refresh" style="display:none" class="btn" /> 
				<input id="addQuestion" name="add" type="button" value="<?php     echo t('Add item')?> &raquo;" class="btn" />
			</div>
		</div>
		
		
		<div class="ccm-spacer"></div>
		
</div> 
	
<div id="ccm-formBlockPane-edit" class="ccm-formBlockPane ccm-ui" style=" <?php     echo (intval($extendedFormSurveyInfo['bID'])>0)?'display:block':''?> ">
	
	<div id="questionEditedMsg" class="formBlockQuestionMsg"><?php     echo t('Your item has been edited.')?></div>
	
	<div id="editQuestionForm" style="display:none">
    
    <strong><?php    echo t('Edit item') ?></strong>
    <div class="ccm-note"><?php    echo t('Edit this item of the form.') ?></div>		

		<div class="fieldRow">
			<div class="fieldLabel"><?php     echo t('Title')?>:</div>
			<div class="fieldValues">
				<input id="questionEdit" name="question" type="text" style="width: 265px" class="ccm-input-text" />
			</div>
			<div class="ccm-spacer"></div>
		</div>	
		
		<div class="fieldRow">
			<div class="fieldLabel"><?php     echo t('Item')?>: </div>
			<div class="fieldValues" style="width: 30%; margin-top: 6px; margin-left: 4px;">
				<strong>Formfields</strong> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="field" /> <?php     echo t('Text Field')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="email" /> <?php     echo t('E-mailaddress Field')?> &nbsp; <br>
				<input name="answerTypeEdit" type="radio" value="textarea" /> <?php     echo t('Textarea ')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="tinymce" /> <?php      echo t('TinyMCE Textarea ')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="integer" /> <?php     echo t('Integer')?> &nbsp; <br>
				<input name="answerTypeEdit" type="radio" value="radios" /> <?php     echo t('Radiobuttons')?> &nbsp; <br>
				<input name="answerTypeEdit" type="radio" value="select" /> <?php     echo t('Selectbox' )?> &nbsp; <br>
				<input name="answerTypeEdit" type="radio" value="checkboxlist" /> <?php     echo t('Checkboxes')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="datetime" /> <?php     echo t('Date/time')?> &nbsp; <br>
				<input name="answerTypeEdit" type="radio" value="hidden" /> <?php     echo t('Hidden Field')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="fileupload" /> <?php     echo t('Fileupload')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="rating" /> <?php     echo t('Rating')?> &nbsp; <br>
             </div>
             <div class="fieldValues" style="width: 30%; margin-top: 6px;">
                <strong>Content items</strong> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="line" /> <?php     echo t('Title (H4)')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="text" /> <?php     echo t('Text (P)')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="hr" /> <?php     echo t('Horizontal rule')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="space" /> <?php     echo t('Space')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="fieldset-start" /> <?php     echo t('Field set (start)')?> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="fieldset-end" /> <?php     echo t('Field set (end)')?> &nbsp; <br>
				<br /><br />
                <strong>Other</strong> &nbsp; <br>
                <input name="answerTypeEdit" type="radio" value="recipient" /> <?php     echo t('Recipient selector')?> &nbsp; <br>
			</div>
			<div class="spacer"></div>
		</div>
		
        <div class="fieldRow" id="recipientOptionEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Recipients")?>:</div>
        <div class="fieldValues">
        <textarea id="recipientOptionsEdit" name="recipientOptionsEdit" cols="50" rows="4" style="width:235px" class="ccm-input-text"></textarea><br />
        <div class="ccm-note"><?php     echo t('Put each recipient (email;name) on a new line. Example: yourname@domain.com;Firstname Lastname')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="integerOptionEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Length")?>:</div>
        <div class="fieldValues">
        <input type="text" style="width: 95%" name="integerOptionsEdit" id="integerOptionsEdit" value="" class="ccm-input-text" />
        <div class="ccm-note"><?php     echo t('Add the length of the numbers. Add 99 to unlimited length.')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
		<div class="fieldRow" id="legendOptionEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Legend")?>:</div>
        <div class="fieldValues">
        	<input type="text" style="width: 95%" name="legendEdit" id="legendEdit" value="" class="ccm-input-text" />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="ratingOptionEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Stars")?>:</div>
        <div class="fieldValues">
        	<input type="text" style="width: 50px" name="starsEdit" id="starsEdit" value="" class="ccm-input-text" />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="hiddenOptionEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Value")?>:</div>
        <div class="fieldValues">
        	<input type="text" style="width: 95%" name="hiddenEdit" id="hiddenEdit" value="" class="ccm-input-text" />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="textAreaOptionEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Text")?>:</div>
        <div class="fieldValues">
        	<textarea id="textareaEdit" name="textareaEdit" cols="50" rows="4" style="width:235px" class="ccm-input-text"></textarea><br />
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="emailcopyCheckEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php    echo t("Send copy")?>:</div>
        <div class="fieldValues">
        	<input name="emailcopyEdit" id="emailcopyEdit"  value="1" type="checkbox" />
            <div class="ccm-note"><?php    echo  t('(Sends a copy of the submission to this e-mailaddress)')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="emailvalidateCheckEdit" style="display:none;">
		<div class="fieldLabel" style=""><?php   echo t("Validate")?>:</div>
        <div class="fieldValues">
        	<input name="emailvalidateEdit" id="emailvalidateEdit"  value="1" type="checkbox" />
            <div class="ccm-note"><?php   echo  t('(Requires the user to enter this e-mailaddress twice)')?></div>
        </div>        
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="datetimeOptionsEdit">
			<div class="fieldLabel"><?php     echo t('Date/time options')?>: </div>
			<div class="fieldValues">
				<input name="answerOptionsDateEdit" type="radio" value="datetime" /> <?php     echo t('Date and time')?> &nbsp; <br>
                <input name="answerOptionsDateEdit" type="radio" value="date" /> <?php     echo t('Date only')?> &nbsp; <br>
                <input name="answerOptionsDateEdit" type="radio" value="time" /> <?php     echo t('Time only')?> &nbsp; <br>
			</div>
			<div class="ccm-spacer"></div>
		</div>
        
		<div class="fieldRow" id="answerOptionsAreaEdit">
			<div class="fieldLabel"><?php     echo t('Answer options')?>: </div>
			<div class="fieldValues">
				<textarea id="answerOptionsEdit" name="answerOptionsEdit" cols="50" rows="4" style="width:90%" class="ccm-input-text"></textarea><br />
				<?php     echo t('Put each answer on a new line')?>			
			</div>
			<div class="ccm-spacer"></div>
		</div>
			
		<div class="fieldRow" id="answerSettingsEdit">
			<div class="fieldLabel"><?php     echo t('Settings')?>: </div>
			<div class="fieldValues">
				<?php     echo t('Textarea width')?>: <input id="widthEdit" name="width" type="text" value="50" size="3"  class="ccm-input-text" /> <br />
				<?php     echo t('Textarea height')?>: <input id="heightEdit" name="height" type="text" value="3" size="2" class="ccm-input-text" />
			</div>
			<div class="ccm-spacer"></div>
		</div>
		        
        <div class="fieldRow" id="otherOptionsEdit">
		<div class="fieldLabel" style=""><?php    echo t("Other option")?>:</div>
        <div class="fieldValues"> 
       		<input name="otherOptionEdit" id="otherOptionEdit" value="1" type="checkbox" onchange="extendedFormSurvey.showOtherOption(this, 'Edit')" onclick="extendedFormSurvey.showOtherOption(this, 'Edit')" />
        </div>
        <div id="otherOptionWrapEdit" class="fieldRow noborder" style="display:none;">
			<div class="fieldLabel" style=""><?php    echo t("Title other option")?>:</div>
            <div class="fieldValues"> 
                 <input type="text" style="width: 95%" name="otherOptionTitleEdit" id="otherOptionTitleEdit" value="" class="ccm-input-text" />
            </div>
        </div>
		<div class="ccm-spacer"></div>
		</div>
        
        <div class="fieldRow" id="toolTipsEdit">
		<div class="fieldLabel" style=""><?php    echo t("Tooltip")?>:</div>
        <div class="fieldValues">
        	<input name="tooltip_checkEdit" id="tooltip_checkEdit"  value="1" type="checkbox" onchange="extendedFormSurvey.showTooltip(this, 'Edit')" onclick="extendedFormSurvey.showTooltip(this, 'Edit')" />
        </div>
        <div id="tooltipWrapEdit" class="fieldRow noborder" style="display:none;">
			<div class="fieldLabel" style=""><?php    echo t("Tooltip text")?>:</div>
            <div class="fieldValues"> 
                 <input type="text" style="width: 95%" name="tooltipTextEdit" id="tooltipTextEdit" value=""  class="ccm-input-text" />
            </div>
        </div>
		<div class="ccm-spacer"></div>
		</div>
        
		<div class="fieldRow" id="questionRequiredEdit">
			<div class="fieldLabel">&nbsp;</div>
			<div class="fieldValues"> 
				<input id="requiredEdit" name="required" type="checkbox" value="1" />
				<?php     echo t('This item is required.')?> 
			</div>
			<div class="ccm-spacer"></div>
		</div>		
		
        <div id="customClassDivEdit">
        <strong><?php    echo t('Custom Class for this field/item') ?>:</strong>
        <div class="ccm-note"><?php    echo t('This is only for advanced users. Advanced users can add a class to the field/item. This enables the user to add there own custom settings to there stylesheet. Enter the name of the class you wish to use.') ?></div>
        
        <div class="fieldRow">
        <div class="fieldLabel" style="white-space:nowrap;"><?php     echo t('Custom Class')?>:</div>
        <div class="fieldValues">
         <input name="customClassFieldEdit" id="customClassFieldEdit" style="width: 95%" type="text" class="ccm-input-text" value="" />
        <div class="ccm-note"><?php    echo t('Insert the name of the class you want to use. Without the dot (.)') ?></div>
        </div>
        </div>
        <div class="ccm-spacer"></div>
        </div>
        
        <div class="fieldRow">
			<div class="fieldLabel">&nbsp; </div>
			<div class="fieldValues" >
				<input type="hidden" id="positionEdit" name="position" value="1000" />
            <input id="cancelEditQuestion" name="cancelEdit" type="button" value="Cancel" class="btn" />
            <input id="editQuestion" name="edit" type="button" class="btn" value="Save Changes &raquo;" />
			</div>
		</div>
		
		
		<div class="ccm-spacer"></div>
        
		
	</div>

	<div id="extendedFormSurvey">
		<div style="margin-bottom:16px"><strong><?php  echo t('Edit')?>:</strong></div>
		<div id="extendedFormSurveyWrap"></div>            
	</div>
</div>	
	
<div id="ccm-formBlockPane-preview" class="ccm-formBlockPane">
	<div id="extendedFormSurvey">
		<div style="margin-bottom:16px"><strong><?php  echo t('Preview')?>:</strong></div>	
		<div id="extendedFormSurveyPreviewWrap"></div>
	</div>
</div>

<div id="ccm-formBlockPane-mailing" class="ccm-formBlockPane">
	<div id="extendedFormSurvey">
		<strong><?php    echo t('Mail messages') ?></strong>
    	<div class="ccm-note"><?php  echo t('Create custom messages for the form notification e-mails. There are two, one for the admin and one for the recipient. Note that the recipient only receives mail if there is an e-mailaddress formfield and the "send copy" checkbox is checked.') ?></div>

        <div class="fieldRow">
			<div class="fieldLabel" style="white-space:nowrap"><?php  echo t('Notification message to admin')?>:</div>
			<div class="ccm-spacer"></div>
			<div class="fieldValues"><textarea name="notification_message" rows="15" class="ccm-input-text" id="notification_message" style="width: 600px;"></textarea></div>
			<div class="ccm-spacer"></div>
	    </div>
        <div class="fieldRow">
			<div class="fieldLabel" style="white-space:nowrap"><?php  echo t('Notification message to recipient')?>:</div>
			<div class="ccm-spacer"></div>
			<div class="fieldValues"><textarea name="recipient_message" rows="15" class="ccm-input-text" id="recipient_message" style="width: 600px;"></textarea></div>
			<div class="ccm-spacer"></div>
	    </div>
	</div>
</div>

<div id="ccm-formBlockPane-remarks" class="ccm-formBlockPane">
  <p><strong>Editing</strong> </p>
  <p>Thank you for buying Extended Form!</p>
  <p>You can change certain things about this script.</p>
  <p> /blocks/controller.php - language and form functions.</p>
 
  <p>/js/jquery.form.js - edit the behavior of the form</p>
  <p>/js/jquery.tooltip.js - edit the behavior of the tooltip</p>
  <p>/js/jquery.rating.js - edit the behavior of the rating stars</p>
  
  <p>/mail/submission_admin.php - outgoing mail to the admin / webmaster<br />
     /mail/submission_recepient.php - outgoing mail to the recipient</p>

  <p>Keep in mind that the files in /mail/ are overwritten when updating this addon!</p> 
  
  <p>You can work with templates, example template is available in this addon.<br />
  	 Check the files /blocks/extended_form/templates/custom</p>    
   
  <p><strong>CSS</strong></p>
  
   
     
  <p>Here some explanation about using the custom classes for each field / item.<br />
    The custom class is alwasy assigned to the input.<br />
    When the input has more &quot;children&quot; like label or other (when checked), you can use this<br />
  also with your custom class...</p>
  <p>Example:</p>
  <p>.mycustomclassname { // This is the inputfield <br />
    }</p>
  <p>.mycustomclassname_label { // This is the label for the inputfield (available for radio, checkbox, select)<br />
    }</p>
  <p>.mycustomclassname_other { // This is the label for the &quot;other&quot; inputfield, when check (available for radio, checkbox, select) <br />
    }</p>
  <p>.mycustomclassname_tooltip { // This is the tooltip for the inputfield<br />
    }  </p>
  <p>Some predefined class in the form...</p>
  <p>.formBlockSurveyTable 		 -	Represents the complete form<br />
    .formBlockSurveyRow			 -	Each row within the form.<br />
    .formBlockSurveyCell 		 -	Each cell within the form. Title and inputfield are each in different cells but have the same class.</p>
  <p>.question					- Assigned to the cell that holds the question or name of the input field<br />
    .answer						 - Assigned to the cell that holds the inputfield</p>
  <p>.checkboxList				 - A div around the checkboxes<br />
    .checkboxPair				 - A div around the checkbox and its label</p>
  <p>.radiobuttonsList			 - A div around the radios<br />
    .radiobuttonsPair			 - A div around the radios and its label</p>
  <p>.otherDiv					 - A div around the &quot;other&quot; inputfield (available for radio, checkbox, select) <br />
    .tooltip_holder				 - This holds the text in the tooltip.</p>
  <p>.space						 - Assigned to the cell that holds the space item<br />
    .text						 - Assigned to the cell that holds the text item<br />
    .hr							 - Assigned to the cell that holds the hr item<br />
    .line						 - Assigned to the cell that holds the line item<br />
    .fieldset					 - Assigned to the cell that holds the fieldset item<br />
    .legend						 - Assigned to the cell that holds the legend item<br />
  .required					 - Astrix and &quot;required&quot;-text in the form</p>
<p>Good Luck!</p>
  <p>More info at: <a href="http://www.concrete5-addons.nl/" target="_blank">http://www.concrete5-addons.nl/</a></p>
</div>


<script>
//safari was loading the auto.js too late. This ensures it's initialized
function initExtendedFormBlockWhenReady(){
	if(extendedFormSurvey && typeof(extendedFormSurvey.init)=='function'){
		extendedFormSurvey.cID=parseInt(<?php     echo $c->getCollectionID()?>);
		extendedFormSurvey.arHandle="<?php     echo $a->getAreaHandle()?>";
		extendedFormSurvey.bID=thisbID;
		extendedFormSurvey.btID=thisbtID;
		extendedFormSurvey.qsID=parseInt(<?php     echo $extendedFormSurveyInfo['questionSetId']?>);	
		extendedFormSurvey.init();
		extendedFormSurvey.refreshSurvey();
	}
	else 
		setTimeout('initExtendedFormBlockWhenReady()',100);
}
initExtendedFormBlockWhenReady();
</script>