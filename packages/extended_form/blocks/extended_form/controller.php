<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));
class ExtendedFormBlockController extends BlockController {
		
	public $btTable = 'btExtendedForm';
	public $btQuestionsTablename = 'btExtendedFormQuestions';
	public $btAnswerSetTablename = 'btExtendedFormAnswerSet';
	public $btAnswersTablename = 'btExtendedFormAnswers'; 	
	public $btInterfaceWidth = '620';
	public $btInterfaceHeight = '530';
	public $thankyouMsg=''; 
	public $titleSubmit=''; 
	public $titleCaptcha=''; 
	public $commentCaptcha=''; 
	public $titleRequired = '';
	public $emailSubject = '';
	
	protected $noSubmitFormRedirect=0;
	protected $lastAnswerSetId=0;
		
	/** 
	 * Used for localization. If we want to localize the name/description we have to include this
	 */
	public function getBlockTypeDescription() {
		return t("Build extended forms and surveys.");
	}
	
	public function getBlockTypeName() {
		return t("Extended Form");
	}
	
	public function getJavaScriptStrings() {
		return array(
			'delete-question' => t('Are you sure you want to delete this question?'),
			'form-name' => t('Your form must have a name.'),
			'complete-required' => t('Please complete all required fields.'),
			'ajax-error' => t('AJAX Error.'),
			'form-min-1' => t('Please add at least one question to your form.')			
		);
	}
	
	public function __construct($b = null){ 
		parent::__construct($b);
		//$this->bID = intval($this->_bID);
		if(is_string($this->thankyouMsg) && !strlen($this->thankyouMsg)){ 
			$this->thankyouMsg = 'Thank you!';
		}
		if(is_string($this->titleSubmit) && !strlen($this->titleSubmit)){ 
			$this->titleSubmit = 'Submit';
		}		
		if(is_string($this->titleCaptcha) && !strlen($this->titleCaptcha)){ 
			$this->titleCaptcha = 'Captcha';
		}
		if(is_string($this->commentCaptcha) && !strlen($this->commentCaptcha)){ 
			$this->commentCaptcha = 'Please insert the letters and numbers shown in the image';
		}
		if(is_string($this->emailSubject) && !strlen($this->emailSubject)){ 
			$this->emailSubject = '%s Form Submission';
		}
		if(is_string($this->titleRequired) && !strlen($this->titleRequired)){ 
			$this->titleRequired = 'Fields are equired';
		}
	}
	
	public function on_page_view() {
		
		$db = Loader::db();
		$html = Loader::helper('html');
		
		$this->addFooterItem($html->javascript('jquery.form.js', 'extended_form'));
		//$this->addHeaderItem($html->css('jquery.form.css', 'extended_form'));
				
		Loader::model('survey', 'extended_form');
		$extendedFormSurvey = new ExtendedFormSurvey();
		$questions = $extendedFormSurvey->loadQuestions( intval($this->questionSetId), $this->bID, 0);
		
		while( $questionRow = $questions->fetchRow() ){	
			if ($questionRow['inputType'] == 'tinymce') 
				$tinyMCE = true;	
			if ($questionRow['tooltip'] == 1)
				$tooltip = true;	
			if ($questionRow['inputType'] == 'rating') 
				$rating = true;
			if ($questionRow['inputType'] == 'datetime') 
				$datetime = true;
		}
		if ($tinyMCE) {	
			$this->addFooterItem($html->javascript('tiny_mce/tiny_mce.js'));
			$this->addFooterItem('<script type="text/javascript"> tinyMCE.init({ mode : "textareas", width: "100%", height: "150px", inlinepopups_skin : "concreteMCE", theme_concrete_buttons2_add : "spellchecker", relative_urls : false, convert_urls: false, theme : "advanced",  theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,image,cleanup,code,charmap",   theme_advanced_buttons2 : \'\', theme_advanced_buttons3 : \'\', theme_advanced_toolbar_location : "top", theme_advanced_toolbar_align : "left", plugins: "inlinepopups,spellchecker,safari,advlink", editor_selector : "advancedEditor", spellchecker_languages : "+English=en"  });  </script>');
		}
		if ($tooltip) {
			$this->addFooterItem($html->javascript('jquery.tooltip.js', 'extended_form'));
			//$this->addHeaderItem($html->css('jquery.tooltip.css', 'extended_form'));
			$this->addFooterItem('<script type="text/javascript">$(function() {$(\'.formBlockSurveyTable .tooltip_holder\').tooltip({ track: true, delay: 0, showURL: false, fade: 250 });});</script>');
		}
		if ($rating) {	
			$this->addFooterItem($html->javascript('jquery.rating.js', 'extended_form'));
			//$this->addHeaderItem($html->css('jquery.rating.css', 'extended_form'));
		}
		if ($datetime) {	
			$this->addFooterItem($html->javascript('jquery.ui.js'));
			$this->addFooterItem($html->css('jquery.ui.css'));
			$this->addFooterItem($html->css('ccm.calendar.css'));
		}
	}
	
	public function view(){ 
		$pURI = ($_REQUEST['pURI']) ? $_REQUEST['pURI'] : str_replace(array('&ccm_token='.$_REQUEST['ccm_token'],'&btask=passthru','&method=submit_form'),'',$_SERVER['REQUEST_URI']);
		$this->set('pURI',  htmlentities( $pURI, ENT_COMPAT));
	}
		

	function save( $data=array() ) { 
		
		if( !$data || count($data)==0 ) 
			$data=$_POST;  
		
		if (intval($data['rcID']) == 0)
			$data['rcID'] = 1;
		
		$b=$this->getBlockObject(); 
		$c=$b->getBlockCollectionObject();
		
		$db = Loader::db();
		if(intval($this->bID)>0)	 
			$total = $db->getOne("select count(*) as total from {$this->btTable} where bID = ".intval($this->bID));
		else 
			$total = 0; 
			
		if($_POST['qsID']) $data['qsID']=$_POST['qsID'];
		if(!$data['qsID']) $data['qsID']=time(); 	
		if(!$data['oldQsID']) $data['oldQsID']=$data['qsID']; 
		$data['bID']=intval($this->bID); 
		
		if(!isset($data['redirect']) || $data['redirect'] <= 0)
			$data['redirectCID'] = 0;		
				
		$v = array( $data['qsID'], $data['surveyName'], intval($data['notifyMeOnSubmission']), $data['recipientEmail'], $data['thankyouMsg'], $data['titleSubmit'], 
					intval($data['displayCaptcha']), $data['titleCaptcha'], $data['commentCaptcha'], intval($data['useReCaptcha']), $data['publickeyReCaptcha'], $data['privatekeyReCaptcha'], 
					intval($data['captureUser']), intval($data['captureIP']), intval($data['capturePageID']),
					intval($data['attachment']), intval($data['attachmentCID']), intval($data['redirectCID']), 
					intval($data['displaySender']), $data['sendFromName'], $data['sendFromEmail'], intval($data['displayRequired']), $data['titleRequired'], 
					$data['customClass'], $data['emailSubject'], intval($this->bID));
 		
		//is it new? 
		if( intval($total)==0 )
		{ 
			$q = "insert into {$this->btTable} (questionSetId, surveyName, notifyMeOnSubmission, recipientEmail, thankyouMsg, titleSubmit, displayCaptcha, titleCaptcha, commentCaptcha, useReCaptcha, publickeyReCaptcha, privatekeyReCaptcha, captureUser, captureIP, capturePageID, attachment, attachmentCID, redirectCID, displaySender, sendFromName, sendFromEmail, displayRequired, titleRequired, customClass, emailSubject, bID) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";		
		}
		else
		{
			$q = "update {$this->btTable} set questionSetId=?, surveyName=?, notifyMeOnSubmission=?, recipientEmail=?, thankyouMsg=?, titleSubmit=?, displayCaptcha=?, titleCaptcha=?, commentCaptcha=?, useReCaptcha=?, publickeyReCaptcha=?, privatekeyReCaptcha=?, captureUser=?, captureIP=?, capturePageID=?, attachment=?, attachmentCID=?, redirectCID=?, displaySender=?, sendFromName=?, sendFromEmail=?, displayRequired=?, titleRequired=?, customClass=?, emailSubject=? where bID=? AND questionSetId=".$data['qsID'];
		}		
		
		//Add Questions (for programmatically creating forms, such as during the site install)
		if( count($data['questions'])>0 ){
			Loader::model('survey', 'extended_form');
			$extendedFormSurvey = new ExtendedFormSurvey();
			foreach( $data['questions'] as $questionData )
				$extendedFormSurvey->addEditQuestion($questionData,0);
		}
 
 		$this->questionVersioning($data);

		$rs = $db->query($q, $v);  
		
		return true;
	}
	
	//Ties the new or edited questions to the new block number
	//New and edited questions are temporarily given bID=0, until the block is saved... painfully complicated
	protected function questionVersioning( $data=array() ){
		$db = Loader::db();
		$oldBID = intval($data['bID']);
		
		//if this block is being edited a second time, remove edited questions with the current bID that are pending replacement
		//if( intval($oldBID) == intval($this->bID) ){  
			$vals=array( intval($data['oldQsID']) );  
			$pendingQuestions=$db->getAll('SELECT msqID FROM btExtendedFormQuestions WHERE bID=0 && questionSetId=?',$vals); 
			foreach($pendingQuestions as $pendingQuestion){  
				$vals=array( intval($this->bID), intval($pendingQuestion['msqID']) );  
				$db->query('DELETE FROM btExtendedFormQuestions WHERE bID=? AND msqID=?',$vals);
			}
		//} 
	
		//assign any new questions the new block id 
		$vals=array( intval($data['bID']), intval($data['qsID']), intval($data['oldQsID']) );  
		$rs=$db->query('UPDATE btExtendedFormQuestions SET bID=?, questionSetId=? WHERE bID=0 && questionSetId=?',$vals);
 
 		//These are deleted or edited questions.  (edited questions have already been created with the new bID).
 		$ignoreQuestionIDsDirty=explode( ',', $data['ignoreQuestionIDs'] );
		$ignoreQuestionIDs=array(0);
		foreach($ignoreQuestionIDsDirty as $msqID)
			$ignoreQuestionIDs[]=intval($msqID);	
		$ignoreQuestionIDstr=join(',',$ignoreQuestionIDs); 
		
		//remove any questions that are pending deletion, that already have this current bID 
 		$pendingDeleteQIDsDirty=explode( ',', $data['pendingDeleteIDs'] );
		$pendingDeleteQIDs=array();
		foreach($pendingDeleteQIDsDirty as $msqID)
			$pendingDeleteQIDs[]=intval($msqID);		
		$vals=array( $this->bID, intval($data['qsID']), join(',',$pendingDeleteQIDs) );  
		$unchangedQuestions=$db->query('DELETE FROM btExtendedFormQuestions WHERE bID=? AND questionSetId=? AND msqID IN (?)',$vals);			
	} 
	
	//Duplicate will run when copying a page with a block, or editing a block for the first time within a page version (before the save).
	function duplicate($newBID) { 
	
		$b=$this->getBlockObject(); 
		$c=$b->getBlockCollectionObject();	 
		 
		$db = Loader::db();
		$v = array($this->bID);
		$q = "select * from {$this->btTable} where bID = ? LIMIT 1";
		$r = $db->query($q, $v);
		$row = $r->fetchRow();
		
		//if the same block exists in multiple collections with the same questionSetID
		if(count($row)>0){ 
			$oldQuestionSetId=$row['questionSetId']; 
			
			//It should only generate a new question set id if the block is copied to a new page,
			//otherwise it will loose all of its answer sets (from all the people who've used the form on this page)
			$questionSetCIDs=$db->getCol("SELECT distinct cID FROM {$this->btTable} AS f, CollectionVersionBlocks AS cvb ".
										 "WHERE f.bID=cvb.bID AND questionSetId=".intval($row['questionSetId']) );
			
			//this question set id is used on other pages, so make a new one for this page block 
			if( count( $questionSetCIDs ) >1 || !in_array( $c->cID, $questionSetCIDs ) ){ 
				$newQuestionSetId=time(); 
				$_POST['qsID']=$newQuestionSetId; 
			}else{
				//otherwise the question set id stays the same
				$newQuestionSetId=$row['questionSetId']; 
			}
			
			//duplicate survey block record 
			//with a new Block ID and a new Question 
			$v = array($newQuestionSetId,$row['surveyName'],$newBID,$row['thankyouMsg'],intval($row['notifyMeOnSubmission']),$row['recipientEmail'],$row['displayCaptcha'],$row['titleCaptcha'],$row['commentCaptcha'], $row['useReCaptcha'], $row['publickeyReCaptcha'], $row['privatekeyReCaptcha'], $row['titleSubmit'], intval($data['captureUser']), intval($data['captureIP']), intval($data['capturePageID']), intval($data['attachment']), intval($data['attachmentCID']), $data['displaySender'], $data['sendFromName'], $data['sendFromEmail'],$row['displayRequired'],$row['titleRequired'],$row['customClass'],$row['emailSubject']);
			
			$q = "insert into {$this->btTable} ( questionSetId, surveyName, bID, thankyouMsg, notifyMeOnSubmission, recipientEmail, displayCaptcha, titleCaptcha, commentCaptcha, useReCaptcha, publickeyReCaptcha, privatekeyReCaptcha, titleSubmit, captureUser, captureIP, capturePageID, attachment, attachmentCID, displaySender, sendFromName, sendFromEmail, displayRequired, titleRequired, customClass, emailSubject) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			$result=$db->Execute($q, $v); 
			
			$rs=$db->query("SELECT * FROM {$this->btQuestionsTablename} WHERE questionSetId=$oldQuestionSetId AND bID=".intval($this->bID) );
			while( $row=$rs->fetchRow() ){
				$v=array($newQuestionSetId,intval($row['msqID']), intval($newBID), $row['question'],$row['inputType'],$row['options'],$row['position'],$row['width'],$row['height'],$row['required'],$row['other'],$row['otherTitle'],$row['tooltip'],$row['tooltipText'],$row['customClass']);
				$sql= "INSERT INTO {$this->btQuestionsTablename} (questionSetId,msqID,bID,question,inputType,options,position,width,height,required,other,otherTitle,tooltip,tooltipText,customClass) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				$db->Execute($sql, $v);
			}
			
			return $newQuestionSetId;
		}
		return 0;	
	}
	

	//users submits the completed survey
	function action_submit_form() { 
		
		global $c;
		
		$ip = Loader::helper('validation/ip');
		Loader::library("file/importer");
		
		if (!$ip->check()) {
			$this->set('invalidIP', $ip->getErrorMessage());			
			return;
		}	

		$txt = Loader::helper('text');
		$db = Loader::db();
				
		//question set id
		$qsID=intval($_POST['qsID']); 
		if($qsID==0)
			die (t("Oops, something is wrong with the form you posted (it doesn't have a question set id)."));
		
		//get all questions for this question set
		$rows=$db->GetArray("SELECT * 
							 FROM {$this->btQuestionsTablename} 
							 WHERE questionSetId=? 
							 AND bID=? 
							 ORDER BY position ASC, msqID ASC", array( $qsID, intval($this->bID)));			
						
		//checked required fields
		foreach($rows as $row){
			if( intval($row['required'])==1 ){
				$notCompleted=0;
				$answerFound=0;
				
				if($row['inputType']=='checkboxlist'){
					foreach($_POST as $key=>$val){
						if( strstr($key, 'Question'.$row['msqID'].'_') && strlen($val)) {
							if ($val=='other_QWOP' && trim($_POST['Question'.$row['msqID'].'_other']) == '') {
								$answerFound=0;
								$errors[$row['msqID']] = $row['question']." ".t("isn't selected or empty");
							} else {							
								$answerFound=1;
							} 
						}
					}
					if(!$answerFound) {
						$errors[$row['msqID']] = $row['question']." ".t("isn't selected or empty");
						$notCompleted=1;
					}
					
				}elseif($row['inputType']=='radios'){
					if (strlen(trim($_POST['Question'.$row['msqID']])) && trim($_POST['Question'.$row['msqID']]) != 'other_QWOP')
						$answerFound=1;
					elseif (trim($_POST['Question'.$row['msqID']]) == 'other_QWOP' && strlen(trim($_POST['Question'.$row['msqID'].'_other'])) )
						$answerFound=1;
					if(!$answerFound) {
						$errors[$row['msqID']] = $row['question']." ".t("isn't selected or empty");
						$notCompleted=1;	
					}
				}elseif($row['inputType']=='select'){
					if (strlen(trim($_POST['Question'.$row['msqID']])) && trim($_POST['Question'.$row['msqID']]) != 'other_QWOP')
						$answerFound=1;
					elseif (trim($_POST['Question'.$row['msqID']]) == 'other_QWOP' && strlen(trim($_POST['Question'.$row['msqID'].'_other'])) )
						$answerFound=1;
					if(!$answerFound) {
						$errors[$row['msqID']] = $row['question']." ".t("isn't selected or empty");
						$notCompleted=1;	
					}			
				
				}elseif($row['inputType']=='fileupload'){		
					if( !isset($_FILES['Question'.$row['msqID']]) || !is_uploaded_file($_FILES['Question'.$row['msqID']]['tmp_name']) )					
					{
						$errors[$row['msqID']] = $row['question']." ".t("couldn't be uploaded");
						$notCompleted=1;	
					}
				
				}elseif($row['inputType']=='integer'){	
					$form_validator = Loader::helper('validation/numbers');
					if (!$form_validator->integer(trim($_POST['Question'.$row['msqID']]))) 
					{
						$errors[$row['msqID']] = $row['question']." ".t("is empty or isn't a number");
						$notCompleted=1;	
					}
					elseif (strlen(eregi_replace(' ','',$_POST['Question'.$row['msqID']])) != $row['options'] && $row['options'] != '99')
					{
						$errors[$row['msqID']] = $row['question']." ".t("should be a ".$row['options']." letter number");
						$notCompleted=1;	
					}
					
				}elseif($row['inputType']=='datetime'){	
					if ($row['options'] == 'datetime' || $row['options'] == '') {				
						if (trim($_POST['Question'.$row['msqID'].'_dt']) == '' || 
							trim($_POST['Question'.$row['msqID'].'_h']) == '' || trim($_POST['Question'.$row['msqID'].'_m']) == '' || trim($_POST['Question'.$row['msqID'].'_a']) == '') {
							$errors[$row['msqID']] = $row['question']." ".t("is empty or isn't a correct date and time");
							$notCompleted=1;
						}	
					} elseif ($row['options'] == 'date') {				
						if (trim($_POST['Question'.$row['msqID']]) == '') {
							$errors[$row['msqID']] = $row['question']." ".t("is empty or isn't a correct date");
							$notCompleted=1;
						}	
					} elseif ($row['options'] == 'time') {				
						if (trim($_POST['Question'.$row['msqID'].'_h']) == '' || trim($_POST['Question'.$row['msqID'].'_m']) == '' || trim($_POST['Question'.$row['msqID'].'_a']) == '') {
							$errors[$row['msqID']] = $row['question']." ".t("is empty or isn't a correct time");
							$notCompleted=1;
						}	
					}
				}elseif($row['inputType']=='recipient'){
					if (strlen(trim($_POST['Question'.$row['msqID']])))
						$answerFound=1;					
					if(!$answerFound) {
						$errors[$row['msqID']] = $row['question']." ".t("isn't selected or empty");
						$notCompleted=1;	
					}			
				}elseif( !strlen(trim($_POST['Question'.$row['msqID']])) ){
					$errors[$row['msqID']] = $row['question']." ".t("is empty or incorrect");
					$notCompleted=1;
				}				
			}
			// This validation should be performed even if email address is not a required field.
			if ($row['inputType']=='email') {
				$form_validator = Loader::helper('validation/strings');				
				if( intval($row['required'])==1 )
				{									
					if (!$form_validator->email(trim($_POST['Question'.$row['msqID']]))) 
					{
						$errors[$row['msqID']] = $row['question']." ".t("is empty or isn't an emailaddress");
						$notCompleted=1;	
					}				
					if ((int)(intval($row['other']) / 2) == 1) {
						// This is a special validated email question (with two actual fields)
						if ($_POST['Question'.$row['msqID']] != $_POST['Question'.$row['msqID'].'_2'])
						{
							$errors[$row['msqID']] = $row['question']." ".t("doesn't match the confirmation");
							$notCompleted=1;	
						}
					}
				}
				elseif (strlen(trim($_POST['Question'.$row['msqID']])) > 0)
				{
					if (!$form_validator->email(trim($_POST['Question'.$row['msqID']]))) 
					{
						$errors[$row['msqID']] = $row['question']." ".t("isn't an emailaddress");
						$notCompleted=1;	
					}	
					if ((int)(intval($row['other']) / 2) == 1) {
						// This is a special validated email question (with two actual fields)
						if ($_POST['Question'.$row['msqID']] != $_POST['Question'.$row['msqID'].'_2'])
						{
							$errors[$row['msqID']] = $row['question']." ".t("doesn't match the confirmation");
							$notCompleted=1;	
						}
					}
				}
			}
		}
		// check captcha if activated
		if ($this->displayCaptcha) {
			if ($this->useReCaptcha) {         
				$recaptcha = Loader::helper('validation/recaptcha', 'extended_form');
				$resp = $recaptcha->recaptcha_check_answer($this->privatekeyReCaptcha, $_SERVER["REMOTE_ADDR"], $_REQUEST["recaptcha_challenge_field"], $_REQUEST["recaptcha_response_field"]);
				if (!$resp->is_valid) {
					$errors['captcha'] = t("Incorrect captcha code");
				}
			} else {						
				$captcha = Loader::helper('validation/captcha');
				if (!$captcha->check()) {
					$errors['captcha'] = t("Incorrect captcha code");
					$_REQUEST['ccmCaptchaCode']='';
				}
			}
		}
		
		$tmpFileIds=array();	
		if(count($errors) > 0){			
			$this->set('formResponse', t('The following fields are empty or incorrect:') );
			$this->set('errors', $errors);
			$this->set('Entry', $E);	
		}
		else	
		{	
			foreach($rows as $row){
				if ($row['inputType']!='fileupload') continue;
				$questionName='Question'.$row['msqID']; 			
				if (!intval($row['required']) && (!isset($_FILES[$questionName]['tmp_name']) || !is_uploaded_file($_FILES[$questionName]['tmp_name']))) continue;
				$fi = new FileImporter();
				$resp = $fi->import($_FILES[$questionName]['tmp_name'], $_FILES[$questionName]['name']);
				if (!($resp instanceof FileVersion)) {
					switch($resp) {
						case FileImporter::E_FILE_INVALID_EXTENSION:
							$errors['fileupload'] = t('Invalid file extension.');
							break;
						case FileImporter::E_FILE_INVALID:
							$errors['fileupload'] = t('Invalid file.');
							break;
						
					}
				}else{
					//$tmpFileIds[intval($row['msqID'])] = $resp->getFileID();
					$tmpFileIds[intval($row['msqID'])] = $resp->getDownloadURL();
				}	
			}	
			
			if(count($errors) != 0){			
				$this->set('formResponse', t('The following fields are empty or incorrect:') );
				$this->set('errors', $errors);
				$this->set('Entry',$E);			
			}else{ 
				$q="insert into {$this->btAnswerSetTablename} (questionSetId) values (?)";
				$db->query($q,array($qsID));
				$answerSetID=$db->Insert_ID();
				$this->lastAnswerSetId=$answerSetID;
			
				$questionAnswerPairs=array();
						
				//loop through each question and get the answers 
				$counter = 0;
				foreach( $rows as $row ){					
					$answer="";
					$answerLong="";								
					//save each answer
					if($row['inputType']=='checkboxlist'){
						$keys = array_keys($_POST);
						foreach ($keys as $key){
							if (strpos($key, 'Question'.$row['msqID'].'_') === 0 && $key != 'Question'.$row['msqID'].'_other'){
								if ($_POST[$key] == 'other_QWOP') {
									$answer[]=$txt->sanitize($row['otherTitle'].' '.$_POST['Question'.$row['msqID'].'_other']);
								}else{
									$answer[]=$txt->sanitize($_POST[$key]);
								}
							}
						}
					}elseif($row['inputType']=='radios'){
						if ($_POST['Question'.$row['msqID']] == 'other_QWOP'){
							$answer=$txt->sanitize($row['otherTitle'].' '.$_POST['Question'.$row['msqID'].'_other']);
						}else{
							$answer=$txt->sanitize($_POST['Question'.$row['msqID']]);
						}
					}elseif($row['inputType']=='select'){
						if ($_POST['Question'.$row['msqID']] == 'other_QWOP'){
							$answer=$txt->sanitize($row['otherTitle'].' '.$_POST['Question'.$row['msqID'].'_other']);
						}else{
							$answer=$txt->sanitize($_POST['Question'.$row['msqID']]);
						}
					}elseif($row['inputType']=='textarea'){
						$answerLong=$_POST['Question'.$row['msqID']];
						$answer='';
					}elseif($row['inputType']=='fileupload'){
						 $answer = $tmpFileIds[intval($row['msqID'])];
					}elseif($row['inputType']=='datetime'){
						if ($row['options'] == 'datetime') {				
							$answer = $_POST['Question'.$row['msqID'].'_dt'].' '.$_POST['Question'.$row['msqID'].'_h'].':'.$_POST['Question'.$row['msqID'].'_m'].' '.$_POST['Question'.$row['msqID'].'_a'];
						}
						elseif ($row['options'] == 'date') {				
							$answer = $_POST['Question'.$row['msqID']];
						}
						elseif ($row['options'] == 'time') {				
							$answer = $_POST['Question'.$row['msqID'].'_h'].':'.$_POST['Question'.$row['msqID'].'_m'].' '.$_POST['Question'.$row['msqID'].'_a'];
						}
					}elseif($row['inputType']=='email'){
						$form_validator = Loader::helper('validation/strings');
						// if(intval($row['other']) != 0 && $form_validator->email(trim($_POST['Question'.$row['msqID']]))){
						if(intval($row['other']) % 2 == 1 && $form_validator->email(trim($_POST['Question'.$row['msqID']]))){
							$sendCopy = true;
							$sendCopyMail = $txt->sanitize($_POST['Question'.$row['msqID']]);
						}	
						$answer=$txt->sanitize($_POST['Question'.$row['msqID']]);			
					}elseif($row['inputType']=='integer'){						
						$answer=$txt->sanitize($_POST['Question'.$row['msqID']]);			
					}elseif($row['inputType']=='text'){
						$row['question'] = $row['options'];
					}elseif($row['inputType']=='recipient'){
												
						$crypt = Loader::helper('encryption', 'extended_form');
						$crypt->set_key(PASSWORD_SALT);
					
						$recipient_dec = $crypt->decrypt($_POST['Question'.$row['msqID']]);
						
						if (eregi(';', $recipient_dec)) {
							list($recipient_email, $recipient_name) = @split(';', $recipient_dec);
							$answer = $recipient_name.' ('.$recipient_email.')';	
						} else {
							$recipient_email = $recipient_name = $recipient_dec;
							$answer = $recipient_name;
						}
							
					
					}else{
						$answerLong="";
						$answer=$txt->sanitize($_POST['Question'.$row['msqID']]);
					}
					
					if( is_array($answer) ) 
						$answer=join(',',$answer);
					
					if ($row['inputType'] != 'hr' && $row['inputType'] != 'fieldset-start' && $row['inputType'] != 'fieldset-end') 
					{			
						$questionAnswerPairs[$row['msqID']]['question'] = $row['question'];
						$questionAnswerPairs[$row['msqID']]['type'] = $row['inputType'];
						$questionAnswerPairs[$row['msqID']]['answer'] = $txt->sanitize( $answer.$answerLong );
					
						$v=array($row['msqID'],$answerSetID,$answer,$answerLong);
						$q="insert into {$this->btAnswersTablename} (msqID,asID,answer,answerLong) values (?,?,?,?)";
						$db->query($q,$v);
						
						$counter++;	
					}
				}
			}
			
			if (intval($this->capturePageID) != 0) 
			{
				//Add pageID to database
				$v=array('9996', $answerSetID, $c->getCollectionId(), '');
				$q="insert into {$this->btAnswersTablename} (msqID, asID, answer, answerLong) values (?, ?, ?, ?)";
				$db->query($q,$v);
				//Add pageID to mailing
				$questionAnswerPairs[9996]['question'] = 'PageID';
				$questionAnswerPairs[9996]['type'] = 'field';
				$questionAnswerPairs[9996]['answer'] = $c->getCollectionId();            
				$counter++;			
			}
		
			if (intval($this->captureIP) != 0) 
			{
				$v=array('9997', $answerSetID, $_SERVER['REMOTE_ADDR'], '');
				$q="insert into {$this->btAnswersTablename} (msqID, asID, answer, answerLong) values (?, ?, ?, ?)";
				$db->query($q,$v);
				
				$questionAnswerPairs[9997]['question'] = 'IP';
				$questionAnswerPairs[9997]['type'] = 'field';
				$questionAnswerPairs[9997]['answer'] = $_SERVER['REMOTE_ADDR'];				
				$counter++;	
			
			}
			
			if (intval($this->captureUser) != 0 && intval($_SESSION['uID']) != 0) 
			{
				$v=array('9998', $answerSetID, $_SESSION['uID'], $answerLong);
				$q="insert into {$this->btAnswersTablename} (msqID, asID, answer, answerLong) values (?, ?, ?, ?)";
				$db->query($q,$v);

				$v=array('9999', $answerSetID, $_SESSION['uName'], $answerLong);
				$q="insert into {$this->btAnswersTablename} (msqID, asID, answer, answerLong) values (?, ?, ?, ?)";
				$db->query($q,$v);
				
				$questionAnswerPairs[9999]['question'] = 'User';
				$questionAnswerPairs[9999]['type'] = 'field';
				$questionAnswerPairs[9999]['answer'] = $_SESSION['uName'].' ('.$_SESSION['uID'].')';				
				$counter++;	
			} 
			
			$refer_uri=$_POST['pURI'];
			if(!strstr($refer_uri,'?')) $refer_uri.='?';			
			
			if(intval($this->displaySender)>0){	
				$formFormEmailAddress = $this->sendFromEmail;
				$formFormName = $this->sendFromName;		
			}elseif( strlen(FORM_BLOCK_SENDER_EMAIL)>1 && strstr(FORM_BLOCK_SENDER_EMAIL,'@') ){
				$formFormEmailAddress = FORM_BLOCK_SENDER_EMAIL; 
				$formFormName = null; 
			}else{ 
				$adminUserInfo=UserInfo::getByID(USER_SUPER_ID);
				$formFormEmailAddress = $adminUserInfo->getUserEmail(); 
				$formFormName = null;
			}
			
			if ($sendCopy && $sendCopyMail)
				$from = $sendCopyMail;
			else
				$from = $formFormEmailAddress;
			
			if (intval($this->attachment) != 0 && intval($this->attachmentCID) != 0) {
				Loader::model('file_version');
				$f = File::getByID($this->attachmentCID);
				$fp = new Permissions($f);					
				$fv = $f->getApprovedVersion();					
			}
			
			if(intval($this->notifyMeOnSubmission)>0)
			{	
				if( strlen(FORM_BLOCK_SENDER_EMAIL)>1 && strstr(FORM_BLOCK_SENDER_EMAIL,'@') ){
					$formFormEmailAddress = FORM_BLOCK_SENDER_EMAIL; 
				}else{ 
					$adminUserInfo=UserInfo::getByID(USER_SUPER_ID);
					$formFormEmailAddress = $adminUserInfo->getUserEmail(); 
				} 					
				
				$mh = Loader::helper('mail', 'extended_form');
				$mh->clearAll();
				$mh->to( $this->recipientEmail ); 
				$mh->from( $from ); 
				$mh->addParameter('formName', $this->surveyName);
				$mh->addParameter('questionSetId', $this->questionSetId);
				$mh->addParameter('questionAnswerPairs', $questionAnswerPairs); 
				$mh->load('submission_admin', 'extended_form');
				$mh->setSubject(t($this->emailSubject, $this->surveyName));
				//echo $mh->body.'<br>';
				
				if (intval($this->attachment) != 0 && intval($this->attachmentCID) != 0) {			
					$mh->addAttachment($fv->getPath(), $fv->getMimeType(), $fv->getFileName());	
				}
				
				@$mh->sendMail(); 
			}
			
			if ($recipient_email != '') {					
				$mh = Loader::helper('mail', 'extended_form');				
				$mh->clearAll();
				$mh->to( $recipient_email ); 
				$mh->from( $from ); 
				$mh->addParameter('formName', $this->surveyName);
				$mh->addParameter('questionSetId', $this->questionSetId);
				$mh->addParameter('questionAnswerPairs', $questionAnswerPairs); 
				$mh->load('submission_admin', 'extended_form');
				$mh->setSubject(t($this->emailSubject, $this->surveyName));
				//echo $mh->body.'<br>';
				
				if (intval($this->attachment) != 0 && intval($this->attachmentCID) != 0) {				
					$mh->addAttachment($fv->getPath(), $fv->getMimeType(), $fv->getFileName());	
				}
				
				@$mh->sendMail(); 
			}
			
			if ($sendCopy) {				
				$mh = Loader::helper('mail', 'extended_form');
				$mh->clearAll();
				$mh->to( $sendCopyMail ); 
				$mh->from( $formFormEmailAddress, $formFormName ); 
				$mh->addParameter('formName', $this->surveyName);
				$mh->addParameter('questionSetId', $this->questionSetId);
				$mh->addParameter('questionAnswerPairs', $questionAnswerPairs); 
				$mh->load('submission_recipient', 'extended_form');
				$mh->setSubject(t($this->emailSubject, $this->surveyName));
				//echo $mh->body.'<br>';
				
				if (intval($this->attachment) != 0 && intval($this->attachmentCID) != 0) {				
					$mh->addAttachment($fv->getPath(), $fv->getMimeType(), $fv->getFileName());
				}
				
				@$mh->sendMail(); 			 
			}
							 
			//$_REQUEST=array();	
			
			if($this->redirectCID > 0) {
				$pg = Page::getByID($this->redirectCID);
				if(is_object($pg)) {
					$this->redirect($pg->getCollectionPath());
				} else { // page didn't exist, we'll just do the default action
					header("Location: ".$refer_uri."&surveySuccess=1&qsid=".$this->questionSetId);
					exit;
				}
			} else {		
				header("Location: ".$refer_uri."&surveySuccess=1&qsid=".$this->questionSetId);
				die;
			}
		}
	}		
		
	function delete() { 
		
		Loader::model('survey', 'extended_form');
		$db = Loader::db();
		
		$deleteData['questionsIDs']=array();
		$deleteData['strandedAnswerSetIDs']=array();

		$extendedFormSurvey = new ExtendedFormSurvey();
		$info = $extendedFormSurvey->getExtendedFormSurveyBlockInfo($this->bID);
		
		//get all answer sets
		$q = "SELECT asID FROM {$this->btAnswerSetTablename} WHERE questionSetId = ".intval($info['questionSetId']);
		$answerSetsRS = $db->query($q); 
 
		//delete the questions
		$deleteData['questionsIDs']=$db->getAll( "SELECT qID FROM {$this->btQuestionsTablename} WHERE questionSetId = ".intval($info['questionSetId']).' AND bID='.intval($this->bID) );
		foreach($deleteData['questionsIDs'] as $questionData)
			$db->query("DELETE FROM {$this->btQuestionsTablename} WHERE qID=".intval($questionData['qID']));			
		
		//delete left over answers
		$strandedAnswerIDs = $db->getAll('SELECT fa.aID FROM `btExtendedFormAnswers` AS fa LEFT JOIN btExtendedFormQuestions as fq ON fq.msqID=fa.msqID WHERE fq.msqID IS NULL');
		foreach($strandedAnswerIDs as $strandedAnswerIDs)
			$db->query('DELETE FROM `btExtendedFormAnswers` WHERE aID='.intval($strandedAnswer['aID']));
			
		//delete the left over answer sets
		$deleteData['strandedAnswerSetIDs'] = $db->getAll('SELECT aset.asID FROM btExtendedFormAnswerSet AS aset LEFT JOIN btExtendedFormAnswers AS fa ON aset.asID=fa.asID WHERE fa.asID IS NULL');
		foreach($deleteData['strandedAnswerSetIDs'] as $strandedAnswerSetIDs)
			$db->query('DELETE FROM btExtendedFormAnswerSet WHERE asID='.intval($strandedAnswerSetIDs['asID']));		
		
		//delete the form block		
		$q = "delete from {$this->btTable} where bID = '{$this->bID}'";
		$r = $db->query($q);		
		
		parent::delete();
		
		return $deleteData;
	}
}	