<?php       
defined('C5_EXECUTE') or die(_("Access Denied."));
$survey=$controller;  
Loader::model('survey', 'extended_form');
$extendedFormSurvey=new ExtendedFormSurvey($b);
$extendedFormSurvey->frontEndMode=true;
?>
<?php    if ($invalidIP) { ?>
<div class="ccm-error"><p><?php    echo $invalidIP?></p></div>
<?php    } ?>

<form enctype="multipart/form-data" id="extendedFormSurveyView<?php    echo intval($bID)?>" class="extendedFormSurveyView <?php  echo $survey->customClass ?>" method="post" action="<?php    echo htmlspecialchars(htmlspecialchars_decode($this->action('submit_form')))?>">
<?php    if( $_GET['surveySuccess'] && $_GET['qsid']==intval($survey->questionSetId) ){ ?>
		<div id="msg"><?php     echo $survey->thankyouMsg ?></div> 
<?php    }elseif(strlen($formResponse)){ ?>
		<div id="msg">
<?php    echo $formResponse ?>
<?php    if(is_array($errors) && count($errors)) foreach($errors as $error){ ?>
		<div class="error"><?php    echo $error ?></div>
<?php    } ?>
</div>
<?php    } ?>
<div>
<input name="qsID" type="hidden" value="<?php    echo  intval($survey->questionSetId)?>" />
<input name="pURI" type="hidden" value="<?php    echo  $pURI ?>" />
</div>
<?php    $extendedFormSurvey->loadSurvey( $survey->questionSetId, 0, intval($bID) );  ?> 
</form>