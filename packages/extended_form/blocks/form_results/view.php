<?php  
if( strlen($questionSet)>0 ){ 
	$c = Page::getCurrentPage();
	$pageURL = DIR_REL. "/" . DISPATCHER_FILENAME . "?cID=" . $c->getCollectionID();  ?>
	<div class="form-responses-wrapper">
	
	
	<?php   if( count($answerSets)==0 ){ ?>
		<div><?php  echo t('No one has yet submitted this form.')?></div>
	<?php   }else{ ?>
	
		<div class="frTopNav">
			<?php   if($_REQUEST['all']!=1){ 
				echo "<a href='" . $pageURL . "&all=1&sortBy=" . $_REQUEST['sortBy'] . "&qsid=" . $questionSet . "'>" . t('Show All') . "</a>";
			}else{ 
				echo "<a href='" . $pageURL . "&all=0&sortBy=" . $_REQUEST['sortBy'] . "&qsid=" . $questionSet . "'>" . t('Show Paging') . "</a>";			
			} ?>
			
			&nbsp;|&nbsp;
			 
			<?php   if($_REQUEST['sortBy']=='chrono'){ 
				echo "<a href='" . $pageURL . "&all=1&sortBy=newest&qsid=" . $questionSet . "\'>" . t('Sort by Newest') . "</a>";
			}else{ 
				echo "<a href='" . $pageURL . "&all=0&sortBy=chrono&qsid=" . $questionSet . "\'>" . t('Sort Chronologically') . "</a>";
			} ?>			
			<div class="spacer"></div>
		</div>
	
		<?php   
		$dh = Loader::helper('date');
		foreach($answerSets as $answerSetId=>$answerSet){ ?>
			
			<div style="margin:0px; padding:0px; width:100%; height:auto" >
			<table class="entry-form">
				<tr>
					<td class="header"><?php  echo t('Submitted Date')?></td>
					<td class="header"><?php  echo $dh->getLocalDateTime($answerSet['created'], "F j, Y, g:i:s a") ?></td>
				</tr>
				<?php   
				$questionNumber=0;
				$numQuestionsToShow=2;
				foreach($questions as $questionId=>$question){ 
				
					//if this row doesn't have an answer, don't show it.
					if(!strlen(trim($answerSet['answers'][$questionId]['answerLong'])) && 
					   !strlen(trim($answerSet['answers'][$questionId]['answer'])))
					   		continue;
					   
					$questionNumber++; 
					?>
					<tr class="<?php  echo ($questionNumber>$numQuestionsToShow)?'extra':''?>QuestionRow<?php  echo $answerSetId?> <?php  echo ($questionNumber>$numQuestionsToShow)?'noDisplay':'' ?>">
						<td width="33%">
							<?php  echo  $questions[$questionId]['question'] ?>
						</td>
						<td>
							<?php  
							if( $question['inputType']=='fileupload' ){
								$fID=intval($answerSet['answers'][$questionId]['answer']);
								$file=File::getByID($fID);
								if($fID && $file){
									$fileVersion=$file->getApprovedVersion();
									echo '<a href="' . $fileVersion->getRelativePath() .'">'.$fileVersion->getFileName().'</a>';
								}else{
									echo t('File not found');
								}
							}elseif($question['inputType']=='text'){
								echo $answerSet['answers'][$questionId]['answerLong'];
							}else{
								echo $answerSet['answers'][$questionId]['answer'];
							}
							?>							
						</td>
					</tr>
				<?php   } ?>
			</table>
			</div>
			
			<div style="text-align:right; margin-bottom:16px">
			<a onclick="return deleteResponse()" href="<?php   echo $pageURL; ?>&qsid=<?php   echo $answerSet['questionSetId']?>&asid=<?php   echo $answerSet['asID']?>&action=deleteResponse"><?php  echo t("Delete Response")?></a>
			&nbsp;
			<?php    if( count($questions)>$numQuestionsToShow ){ ?>
				|&nbsp;<a style="cursor:pointer" onclick="toggleQuestions(<?php   echo $answerSetId?>,this)"><?php   echo t('Toggle fields') ?></a>
			<?php    } ?>
			</div>	
		<?php    } ?>
		
	<?php   } ?> 	

	<?php   if($paginator && strlen($paginator->getPages())>0){ ?>	 
		 <div  class="pagination">
			 <div class="pageLeft"><?php  echo $paginator->getPrevious()?></div>
			 <div class="pageRight"><?php  echo $paginator->getNext()?></div>
			 <?php  echo $paginator->getPages()?>
		 </div>		
	<?php   } ?>		
	
	</div>

<?php   } else {
	echo t('This form no longer exists.');
}?>