// JavaScript Document
function toggleQuestions(qsID,trigger){
	$('.extraQuestionRow'+qsID).toggleClass('noDisplay');
	if(trigger.state=='open') {
		 trigger.state='closed';
	}else{
		trigger.state='open';
	}
}
//SET UP FORM RESPONSE CONFIRM DELETE
function deleteResponse(dLink){
	return confirm("Are you sure you want to delete this form submission?");
}
//SET UP FORM CONFIRM DELETE
function deleteForm(dLink){
	return confirm("Are you sure you want to delete this form and its form submissions?");
}
