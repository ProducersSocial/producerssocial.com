<?php  
defined('C5_EXECUTE') or die(_("Access Denied.")); 
?>
<style type="text/css">
div.fieldRow{ border-top: 1px solid #ccc; padding:8px 4px ; margin:8px; clear:left}
div.fieldRow div.fieldLabel{float: left; width:30%; text-align:right; padding-top: 6px; padding-right:8px}
div.fieldRow div.fieldValues{float: left; width:65%}
</style>
<strong><?php    echo t('Select form') ?></strong>
<div class="ccm-note"><?php    echo t('Select the form from which you want to show the results') ?></div>

<div class="fieldRow">
    <div class="fieldLabel"><?php  echo t('Formname')?>:</div>
    <div class="fieldValues">
      <?php  if (count($surveys) == 0) { ?>
       echo t('You have not created any forms.');
      <?php  } else { ?> 
       <select name="formID">
		<?php 
        foreach($surveys as $thisQuestionSetId=>$survey){
            $b=Block::getByID( intval($survey['bID']) );
            $db = Loader::db();            
            if (is_object($b)) 
			{ 
				if ( $survey['bID'] == $formID ) { 
					$selected = "selected='selected'";
				} else {
					$selected = "";	
				}?>
            <option value="<?php  echo $survey['bID'] ?>" <?php  echo $selected?>><?php  echo $survey['surveyName']; ?></option>
            <?php  }
        } ?></select>
      <?php  } ?> 
    </div>
    <div class="ccm-spacer"></div>
</div>	