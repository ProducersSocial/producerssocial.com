<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
class FormResultsBlockController extends BlockController 
{	
	public $btTable = 'btExtendedFormResults';
	public $btInterfaceWidth = '420';
	public $btInterfaceHeight = '230';
	private $pageSize=3; 
	
	public function getBlockTypeDescription() {
		return t("Display the results for a particular form located somewhere else on the website.");
	}
	
	public function getBlockTypeName() {
		return t("Form Results");
	}
	
	public function add() {
		$this->set('surveys', $this->getSurveyList());	
	}
	
	public function edit() {
		$this->set('surveys', $this->getSurveyList());	
	}
	
	public function getSurveyList() {
		
		$db = Loader::db();
		
		Loader::model('survey', 'extended_form');
		Loader::model('stats', 'extended_form');
		
		$tempMySurvey = new ExtendedFormSurvey();
		//load surveys
		$surveysRS = ExtendedFormBlockStatistics::loadSurveys($tempMySurvey);
		
		//index surveys by question set id
		$surveys=array();
		while($survey=$surveysRS->fetchRow()){
			//get Survey Answers
			$survey['answerSetCount'] = $tempMySurvey->getAnswerCount( $survey['questionSetId'] );
			$surveys[ $survey['questionSetId'] ] = $survey;			
		}	
		return $surveys;
	}
	
	public function view(){
		
		if($_REQUEST['all']){
			$this->pageSize=100000; 
			$_REQUEST['page']=1;
		}
			
		$pageBase=$this->curPageURL();
		
		$db = Loader::db();
		Loader::model('survey', 'extended_form');
		Loader::model('stats', 'extended_form');
		
		$tempMySurvey = new ExtendedFormSurvey();
		$fID = $this->formID;
					
		if( $_REQUEST['action'] == 'deleteResponse' ){
			$this->deleteAnswers($_REQUEST['asid']);
		}		
		//load surveys
		$surveysRS=ExtendedFormBlockStatistics::loadSurvey($tempMySurvey, $fID);
		
		//index surveys by question set id
		$survey=array();
		$survey=$surveysRS->fetchRow();
		//get Survey Answers
		$questionSet = $survey['questionSetId'];
		$survey['answerSetCount'] = $tempMySurvey->getAnswerCount($questionSet);
		
		$questionsRS = $tempMySurvey->loadQuestions($questionSet);
		$questions=array();
		while( $question = $questionsRS->fetchRow() ){
			$questions[$question['msqID']]=$question;
		}
		
		$answerSetCount = $survey['answerSetCount'];
		
		$pageBaseSurvey=$pageBase.'&qsid='.$questionSet;
		$paginator=Loader::helper('pagination');
		$sortBy=$_REQUEST['sortBy'];
		$paginator->init( intval($_REQUEST['page']) ,$answerSetCount,$pageBaseSurvey.'&page=%pageNum%&sortBy='.$sortBy,$this->pageSize);
		
		if($this->pageSize>0)
			$limit=$paginator->getLIMIT();
		else $limit='';
		$answerSets = ExtendedFormBlockStatistics::buildAnswerSetsArray( $questionSet, $sortBy, $limit ); 
		$this->set('questions',$questions);		
		$this->set('answerSets',$answerSets);
		$this->set('paginator',$paginator);	
		$this->set('questionSet',$questionSet);
		$this->set('survey',$survey);  		
	}

	private function deleteAnswers($asID){
		$db = Loader::db();
		$v = array(intval($asID));
		$q = 'DELETE FROM btExtendedFormAnswers WHERE asID = ?';		
		$r = $db->query($q, $v);
		
		$q = 'DELETE FROM btExtendedFormAnswerSet WHERE asID = ?';		
		$r = $db->query($q, $v);
	}

	public function curPageURL() {
		//if (strlen($this->cID)>0) {
			$c = Page::getCurrentPage();
			$pageURL = DIR_REL. "/" . DISPATCHER_FILENAME . "?cID=" . $c->getCollectionID();
		//} else {
//			$pageURL = 'http';
//			if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//			$pageURL .= "://";
//			if ($_SERVER["SERVER_PORT"] != "80") {
//				$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
//			} else {
//				$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
//			}
//		}
		return $pageURL;
	}
}
?>