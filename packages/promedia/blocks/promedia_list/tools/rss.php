<?php   
//Permissions Check
//if($_GET['bID']) {
	//edit survey mode
	$b = Block::getByID($_GET['bID']);
	
	$controller = new PromediaListBlockController($b);
	$rssUrl = $controller->getRssUrl($b);
	
	//$bp = new Permissions($b);
	//if( $bp->canRead() && $controller->rss) {
		$strip = array("&nbsp;","&");
		$cArray = $controller->getPages();
		$nh = Loader::helper('navigation');
		$uh = Loader::helper('concrete/urls');
		$mh = Loader::helper('media_type','promedia');

		header('Content-type: text/xml');
		echo "<" . "?" . "xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";

?>
		<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
		
		  <channel>
			<title><?php   echo $controller->rssTitle?></title>
			<language>en-us</language>
			<link><?php   echo BASE_URL.DIR_REL.$rssUrl?></link>
			<copyright>Copyright <?php  echo date('Y')?></copyright>
			<?php 
			$d = date( 'D, d M Y H:i:s T');
			$mysqldate = gmdate(DATE_RFC822, strtotime($d));
			?>
    		<lastBuildDate><?php   echo $mysqldate; ?></lastBuildDate>
			<description><?php   echo $controller->rssDescription?></description> 
			<itunes:summary><?php   echo $controller->rssDescription?></itunes:summary>
			<itunes:author><?php  echo SITE?></itunes:author>
			<itunes:category text="Technology"/>
			<itunes:category text="Podcasting"/>
			
<?php   
		for ($i = 0; $i < count($cArray); $i++ ) {
			$file = $cArray[$i];
			$name = $file->getTitle();
			$file_src = $file->getDownloadURL();
			
			$audio = null;
			$video = null;

			$ak_d = FileAttributeKey::getByHandle('media_date'); 
			$date = $file->getAttribute($ak_d);
			if($date==null){
				$date = $file->getDateAdded();
			}
			$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
			$image = $file->getAttribute($ak_a);
			
			if($image==null){
				$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
			}else{
				$image = File::getByID($image->fID);
				$image_path=$image->getURL();
			}
			

			$ak_s = FileAttributeKey::getByHandle('media_audio'); 
			$audio = $file->getAttribute($ak_s);
			if($audio){
				$audio_array = $mh->getMediaTypeOutput($audio);
				$file_src = $audio_array['path'];
			}

			$ak_s = FileAttributeKey::getByHandle('media_video'); 
			$video = $file->getAttribute($ak_s);
			if($video){
				$video_array = $mh->getMediaTypeOutput($video);
				$file_src = $video_array['path'];
				$image_path = $mh->getMediaThumbnail($video_array['id'],$video_array['type']);
			}
			?>
			<item>
			  <title><?php   echo htmlspecialchars($name);?></title>
			  <link>
				<?php  echo $file_src?>		  
			  </link>
			  <guid>
				<?php  echo $file_src?>
			  </guid>
			  <itunes:explicit>No</itunes:explicit>
			  <itunes:author><?php  echo substr(BASE_URL,7)?></itunes:author>
			  <itunes:owner>
			      <itunes:name><?php  echo substr(BASE_URL,7)?></itunes:name>
			      <itunes:email>mail@<?php  echo substr(BASE_URL,7)?></itunes:email>
			  </itunes:owner>
			  <enclosure url="<?php  echo $file_src?>" length="<?php  echo $controller->getFileSize($file->fID)?>" 
			  <?php  
			  if(substr($file->getFileName(),-3)=='mp3'){ echo 'type="audio/mpeg"/>';}
			  elseif(substr($file->getFileName(),-3)=='mp4' || substr($file->getFileName(),-3)=='m4v' || substr($file->getFileName(),-3)=='mpg'){ echo 'type="video/mpeg"/>';}
			  elseif(substr($file->getFileName(),-3)=='flv' || substr($file->getFileName(),-3)=='f4v'){ echo 'type="video/flash"/>';}
			  elseif(substr($file->getFileName(),-3)=='wmv'){ echo 'type="video/wmv"/>';}
			  elseif(substr($file->getFileName(),-3)=='avi'){ echo 'type="video/avi"/>';}
			  else{echo 'type="application/atom+xml"/>';}
			  ?>
			  <itunes:image href="<?php  echo $image_path?>" />
			  
			  <description><?php   echo htmlspecialchars(strip_tags($file->getDescription()))."....";?></description>
			  <itunes:summary><?php   echo htmlspecialchars(strip_tags($file->getDescription()))."....";?></itunes:summary>
			  <?php 
			  $d = date( 'D, d M Y H:i:s T',strtotime($date));
			  $mysqldate = gmdate(DATE_RFC822, strtotime($d));
			  ?>
			  <pubDate><?php   echo $mysqldate; ?></pubDate>
			  <!--
			  <itunes:keywords><?php  echo $file->getAttribute('tags')?></itunes:keywords>
			  -->
			</item>
<?php   } ?>
     	  </channel>
		</rss>
		
<?php   	//} else {  	
		//$v = View::getInstance();
		//$v->renderError('Permission Denied',"You don't have permission to access this RSS feed");
		//exit;
	//}
			
//} else {
//	echo "You don't have permission to access this RSS feed";
//}
exit;


