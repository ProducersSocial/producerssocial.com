<?php   defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<?php   $c = Page::getCurrentPage(); ?>

<input type="hidden" name="pageListToolsDir" value="<?php   echo $uh->getBlockTypeToolsURL($bt)?>/" />
<div class="deals-attributes">
	 <h2><?php   echo t('Media Title')?></h2>
	 <input id="ccm-pagelist-rssTitle" type="text" name="title" style="width:250px" value="<?php   echo $title?>" /><br /><br />
</div> 
<div id="ccm-pagelistPane-add" class="ccm-pagelistPane">
	<div class="ccm-block-field-group">
	  <h2><?php   echo t('Number of Pages')?></h2>
	  <?php   echo t('Display')?>
	  <input type="text" name="num" value="<?php   echo $num?>" style="width: 30px">
	  <?php   
	  Loader::model('file_set');
	  $fs = FileSet::getMySets();	
	  ?>
	  &nbsp;
	  of
	  &nbsp;
	  <select name="setID">
	  	<?php   
	  	foreach($fs as $s){
	  		if($setID==$s->fsID){
	  			$selected = 'selected';
	  		}else{
	  			$selected = '';
	  		}
	  		echo '<option value="'.$s->fsID.'" '.$selected.'>'.$s->fsName.'</option>';
	  	}
	  	if (!$setID || $setID==0){ 
				echo '<option value="0" selected>Every</option>';
		}else{
				echo '<option value="0">Every</option>';
		}
	  	?>
	  </select>
	  &nbsp;
	  File Set
	  <h2><?php   echo t('Filter')?></h2>
	  
	<div class="media-attributes">
		<div>
			<select name="type">
			<?php   
			$db = Loader::db();
			$k=$db->query("SELECT akID FROM AttributeKeys WHERE akHandle = 'media_type'");
			while($row=$k->fetchrow()){
				$key=$row['akID'];
			}
			$r=$db->query("SELECT value FROM atSelectOptions WHERE akID = $key");
			while($row=$r->fetchrow()){
				$cats[]=$row['value'];
			}
			foreach($cats as $cat){
				if ($cat==$type){ $select = 'selected';}else{ $select = '';}
				echo '<option value="'.$cat.'" '.$select.'>'.$cat.'</option>';
			}
			if (!isset($type) || $type=='All Types'){ 
				echo '<option value="All Types" selected>All Types</option>';
			}else{
				echo '<option value="All Types">All Types</option>';
			}
			?>
			</select>
		</div>
	</div>
	<br/>
		
	</div>
	<div class="ccm-block-field-group">
		<h2><?php   echo t('Pagination')?></h2>
		<input type="checkbox" name="paginate" value="1" <?php   if ($paginate == 1) { ?> checked <?php   } ?> />
		<?php   echo t('Display pagination interface if more items are available than are displayed.')?>
	</div>
	<div class="ccm-block-field-group">
	  <h2><?php   echo t('Sort Pages')?></h2>
	  <?php   echo t('Pages should appear')?>
	  <select name="orderBy">
		<option value="date_desc" <?php   if ($orderBy == 'date_desc') { ?> selected <?php   } ?>><?php   echo t('with the most recent first')?></option>
		<option value="date_asc" <?php   if ($orderBy == 'date_asc') { ?> selected <?php   } ?>><?php   echo t('with the earlist first')?></option>
		<option value="alpha_asc" <?php   if ($orderBy == 'alpha_asc') { ?> selected <?php   } ?>><?php   echo t('in alphabetical order')?></option>
		<option value="alpha_desc" <?php   if ($orderBy == 'alpha_desc') { ?> selected <?php   } ?>><?php   echo t('in reverse alphabetical order')?></option>
	  </select>
	</div>
	
	<div class="ccm-block-field-group">
	  <h2><?php   echo t('Provide RSS Feed')?></h2>
	   <input id="ccm-pagelist-rssSelectorOn" type="radio" name="rss" class="rssSelector" value="1" <?php   echo ($rss?"checked=\"checked\"":"")?>/> <?php   echo t('Yes')?>   
	   &nbsp;&nbsp;
	   <input type="radio" name="rss" class="rssSelector" value="0" <?php   echo ($rss?"":"checked=\"checked\"")?>/> <?php   echo t('No')?>   
	   <br /><br />
	   <div id="ccm-pagelist-rssDetails" <?php   echo ($rss?"":"style=\"display:none;\"")?>>
		   <strong><?php   echo t('RSS Feed Title')?></strong><br />
		   <input id="ccm-pagelist-rssTitle" type="text" name="rssTitle" style="width:250px" value="<?php   echo $rssTitle?>" /><br /><br />
		   <strong><?php   echo t('RSS Feed Description')?></strong><br />
		   <textarea name="rssDescription" style="width:250px" ><?php   echo $rssDescription?></textarea>
	   </div>
	</div>
	
	<style type="text/css">
	#ccm-pagelist-truncateTxt.faintText{ color:#999; }
	<?php   if(truncateChars==0 && !$truncateSummaries) $truncateChars=128; ?>
	</style>
	<div class="ccm-block-field-group">
	   <h2><?php   echo t('Truncate Summaries')?></h2>	  
	   <input id="ccm-pagelist-truncateSummariesOn" name="truncateSummaries" type="checkbox" value="1" <?php   echo ($truncateSummaries?"checked=\"checked\"":"")?> /> 
	   <span id="ccm-pagelist-truncateTxt" <?php   echo ($truncateSummaries?"":"class=\"faintText\"")?>>
	   		<?php   echo t('Truncate descriptions after')?> 
			<input id="ccm-pagelist-truncateChars" <?php   echo ($truncateSummaries?"":"disabled=\"disabled\"")?> type="text" name="truncateChars" size="3" value="<?php   echo intval($truncateChars)?>" /> 
			<?php   echo t('characters')?>
	   </span>
	</div>
	
</div>