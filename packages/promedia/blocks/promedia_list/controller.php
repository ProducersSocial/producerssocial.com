<?php   

	defined('C5_EXECUTE') or die(_("Access Denied."));
	class PromediaListBlockController extends BlockController {

		protected $btTable = 'btProMediaList';
		protected $btInterfaceWidth = "500";
		protected $btInterfaceHeight = "350";
		
		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("List media Items based on type, area, or category.");
		}
		
		public function getBlockTypeName() {
			return t("Promedia List");
		}
		
		public function getJavaScriptStrings() {
			return array(
				'feed-name' => t('Please give your RSS Feed a name.')
			);
		}
		
		function getPages($query = null) {
			Loader::model('promedia_list','promedia');
			$db = Loader::db();
			$bID = $this->bID;
			if ($this->bID) {
				$q = "select * from btProMediaList where bID = '$bID'";
				$r = $db->query($q);
				if ($r) {
					$row = $r->fetchRow();
				}
			} else {
				$row['num'] = $this->num;
				$row['orderBy'] = $this->orderBy;
				$row['rss'] = $this->rss;
				$row['type'] = $this->type;
				$row['title'] = $this->title;
				$row['setID'] = $this->setID;
			}
			

			$fl = new PromediaList();
			$fl->setNameSpace('b' . $this->bID);
			
			$cArray = array();
			
			$sortBy = $row['orderBy']; 
			if($sortBy=='date_asc'){
				//$fl->sortBy('f.fDateAdded', 'asc'); 
				$fl->sortByAttributeKey('media_date','asc'); 
			}elseif($sortBy=='date_desc'){
				//$fl->sortBy('f.fDateAdded', 'desc');  
				$fl->sortByAttributeKey('media_date','desc'); 
			}elseif($sortBy=='alpha_asc'){
				$fl->sortBy('fvFilename', 'asc');
			}elseif($sortBy=='alpha_desc'){
				$fl->sortBy('fvFilename', 'desc');
			}else{ // "title"
				$fl->sortBy('fvTitle', 'asc' ); 
			}	


			$num = (int) $row['num'];
			
			if ($num > 0) {
				$fl->setItemsPerPage($num);
			}
			
			
			if ($this->setID > 0) {
				Loader::model('file_set');
				$fs = FileSet::getByID($this->setID);
				$fl->filterBySet($fs);
			}

			$c = Page::getCurrentPage();
			if (is_object($c)) {
				$this->cID = $c->getCollectionID();
			}
			
			if($_REQUEST['media-search']){
				$fl->filterByKeyword($_REQUEST['media-search']);
			}

			if ($this->type != 'All Types' && !$_REQUEST['media-search']) {
				$type = $this->type;
				$type = "%\n$type\n%";
				$fl->filter(false,"ak_media_type LIKE '$type'");
			}
			
			$fl->filter('fv.fvExtension', 'png', '!=');
			$fl->filter('fv.fvExtension', 'jpg', '!=');
			$fl->filter('fv.fvExtension', 'bmp', '!=');
			$fl->filter('fv.fvExtension', 'gif', '!=');
			$fl->filter('fv.fvExtension', 'pdf', '!=');
			$fl->filter('fv.fvExtension', 'ico', '!=');
			$fl->filter('fv.fvExtension', 'zip', '!=');
			
			//$fl->debug();

			if ($num > 0) {
				$pages = $fl->getPage();
			} else {
				$pages = $fl->get();
			}
			$this->set('_fl', $fl);
			return $pages;
		}
		
		public function view() {
			$cArray = $this->getPages();
			$nh = Loader::helper('navigation');
			$this->set('nh', $nh);
			//$this->set('cArray', $cArray);
			$this->set('cArray', $cArray);
		}

		public function on_page_view() {
			$html = Loader::helper('html');
			$bt = BlockType::getByHandle('promedia_list');
			$this->addFooterItem($html->javascript('jquery.js'));
			$this->addFooterItem($html->javascript('jquery.ui.js'));
			
			$this->addHeaderItem($html->javascript(BASE_URL.DIR_REL.'/packages/promedia/blocks/promedia_list/tools/fancybox/jquery.fancybox-1.3.4.pack.js'));
			$this->addHeaderItem($html->css(BASE_URL.DIR_REL.'/packages/promedia/blocks/promedia_list/tools/fancybox/jquery.fancybox-1.3.4.css'));
			
			$this->addHeaderItem($html->css('jquery.ui.css'));
		}

		public function add() {
			Loader::model("collection_types");
			$c = Page::getCurrentPage();
			$uh = Loader::helper('concrete/urls');
			//	echo $rssUrl;
			$this->set('c', $c);
			$this->set('uh', $uh);
			$this->set('bt', BlockType::getByHandle('promedia_list'));
			$this->set('displayAliases', true);
		}
	
		public function edit() {
			$b = $this->getBlockObject();
			$bCID = $b->getBlockCollectionID();
			$bID=$b->getBlockID();
			$this->set('bID', $bID);
			$c = Page::getCurrentPage();
			if ($c->getCollectionID() != $this->cParentID && (!$this->cThis) && ($this->cParentID != 0)) { 
				$isOtherPage = true;
				$this->set('isOtherPage', true);
			}
			$uh = Loader::helper('concrete/urls');
			$this->set('uh', $uh);
			$this->set('bt', BlockType::getByHandle('promedia_list'));
		}
		
		function save($args) {
			// If we've gotten to the process() function for this class, we assume that we're in
			// the clear, as far as permissions are concerned (since we check permissions at several
			// points within the dispatcher)
			$db = Loader::db();

			$bID = $this->bID;
			$c = $this->getCollectionObject();
			if (is_object($c)) {
				$this->cID = $c->getCollectionID();
			}
			
			$args['num'] = ($args['num'] > 0) ? $args['num'] : 0;
			$args['cThis'] = ($args['cParentID'] == $this->cID) ? '1' : '0';
			$args['cParentID'] = ($args['cParentID'] == 'OTHER') ? $args['cParentIDValue'] : $args['cParentID'];
			$args['truncateSummaries'] = ($args['truncateSummaries']) ? '1' : '0';
			$args['displayFeaturedOnly'] = ($args['displayFeaturedOnly']) ? '1' : '0';
			$args['displayAliases'] = ($args['displayAliases']) ? '1' : '0';
			$args['truncateChars'] = intval($args['truncateChars']); 
			$args['paginate'] = intval($args['paginate']); 
			$args['category'] = isset($args['category']) ? $args['category'] : '';
			$args['setID'] = isset($args['setID']) ? $args['setID'] : '';
			$args['title'] = isset($args['title']) ? $args['title'] : '';

			parent::save($args);
		
		}
		
		function getBlockPath($path){
			$db = Loader::db();
			$r = $db->Execute("SELECT cPath FROM PagePaths WHERE cID = '$path' ");
			while ($row = $r->FetchRow()) {
				$pIDp = $row['cPath'];
				return $pIDp ;
			}
		}

		public function getRssUrl($b){
			$uh = Loader::helper('concrete/urls');
			if(!$b) return '';
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$c = $b->getBlockCollectionObject();
			//$a = $b->getBlockAreaObject();
			$rssUrl = $uh->getBlockTypeToolsURL($bt)."/rss?bID=".$b->getBlockID()."&amp;cID=".$c->getCollectionID();
			return $rssUrl;
		}
		
		
		function fileGrab($ID, $name=NULL){
			$db = Loader::db();
			$r = $db->Execute("select fvFilename from FileVersions where fID= '$ID'");
			while ($row = $r->FetchRow()) {
				$filename = $this->sanitizeTitle($row['fvFilename']);
				$path = View::url('/download_file', $ID);
				if (isset($name)){
				return $filename ;
				} else {
				return $path.$filename ;
				}
			}
		}
		
		function getFileSize($fID) {
			$db = Loader::db();
			$r = $db->Execute("select fvSize from FileVersions where fID= '$fID'");
			while ($row = $r->FetchRow()) {
				$fileSize = $row['fvSize'];
				return $fileSize;
			}
		}
		
		function sanitizeTitle($filename, $maxLength = 60) {
			// remove all numbers from the front of a file
			$st = preg_replace("/^[0-9]+/", "", $filename);
			if (strlen($st) > $maxLength) {
				$st = substr($st, 0, $maxLength) . '...';
			}
			return $st;
		}
		
		public function URLfix($link) {
			if (substr($link,-1)!='/'){
		    	return $link.'&';
			}else{
				return $link.'?';
			}
		}
		
		
		
	}

?>