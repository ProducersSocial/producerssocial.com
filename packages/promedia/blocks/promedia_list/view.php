<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '400';
    $width = '600';
    
	if (count($cArray) > 0) { ?>
	<div class="scroller">
  	<?php  
			for ($i = 0; $i < count($cArray); $i++ ) {
				$t++;
				$file = $cArray[$i];
				$remote = null;
				$name = $file->getFileName();
				$file_path = BASE_URL.DIR_REL.$file->getDownloadURL();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);
				if($image==null){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}else{
					$image = File::getByID($image->fID);
					$image_path=$image->getURL();
				}
				
				
				$file_src = BASE_URL.DIR_REL.$file->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $file->getAttribute($ak_s);
				if($audio){
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				
				$video = null;
				$ak_s = FileAttributeKey::getByHandle('media_video'); 
				$video = $file->getAttribute($ak_s);
	
				if($video){
					$video_array = $mh->getMediaTypeOutput($video);
					$video_path = $video_array['path'];
					$image_path = $mh->getMediaThumbnail($video_array['id'],$video_array['type']);
					//$video_src = $video_path.'?width='.$width.'&height='.$height;
					//$file_src = $video_src;
					if($video_array['type'] == 'vimeo'){
						$file_src = 'http://player.vimeo.com/video/'.$video_array['id'];
					}else{
						$file_src = $video_path;
					}
				}

				$ak_s = FileAttributeKey::getByHandle('media_download'); 
				$download = $file->getAttribute($ak_s);

				//var_dump($file->getRelativePath());
				?>
			<div class="scroll_item">
				<div class="preview pre<?php  echo $i?>">
					<?php  
					if($image_f){
					?>
					<img src="<?php  echo $image_path?>" alt="video_img" class="thumb_img"/>
					<?php  
					}
					?>
						<a id="player_get<?php  echo $i?>" href="<?php  echo $file_src?>"><?php  echo $file->getTitle()?></a>
					
						<a id="player_get_play<?php  echo $i?>" alt="<?php  echo $file_src?>"  href="#popup_prep" class="play_button"></a>
				</div>
				<br/>
				<?php   
					if(!$controller->truncateSummaries){
						echo $file->getDescription();
					}else{
						echo $textHelper->shorten($file->getDescription(),$controller->truncateChars);
					}
				?>
			</div>
	</div>
	<div style="display: none;">
		<div id="popup_prep">
		
		</div>
	</div>
	<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function() {
		$('.play_button').fancybox({
			width: 'auto',
			height: 'auto',
			onClosed: function(){
		    	$('video, audio').each(function() {
		          $(this)[0].player.pause();		  
		        });
			}
		});
	});
	/*]]>*/
	</script>
	<script type="text/javascript">
	/*<![CDATA[*/
	$('.play_button').click(function(){
		$('#popup_prep').html('');
		var html = '';
		var path = $(this).attr('alt');
		var extension = path.substr( (path.lastIndexOf('.') +1) );
		
		if(extension){
		    switch(extension) {
		        case 'm4v':  
		        case 'mpeg':
		        case 'mpg':
		        case 'wmv':
		        case 'avi':
		        case 'mov':
		        case 'flv':
		        case 'f4v':
		        case 'mp4':
		          html = '<video class="player_box" id="play" src="'+path+'" type="video/'+extension+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video><br style="clear: both;"/>';
		          break;    
		        case 'mp3':
		        case 'm4a':
		          html = '<audio class="audioplayer" id="play" src="'+path+'" controls="controls" type="audio/mp3"></audio><br style="clear: both;"/>';
		          break;   
		        default:
		          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+path+'" frameborder="0" allowfullscreen></iframe>'; 
		    }
	    }
		$('#popup_prep').append(innerShiv(html));

		$('video,audio').mediaelementplayer();
	});
	/*]]>*/
	</script>
<?php  } ?>
	<?php   
	} 
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php   echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /><a href="<?php   echo BASE_URL.$rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php   echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php   echo $controller->rssTitle?>" />
			<br/>
		<?php   
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>