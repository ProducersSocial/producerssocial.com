<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '400';
    $width = '600';
    
	if (count($cArray) > 0) { ?>
	<div id="skin-wrapper" data-skin-name="premium-pixels">
			<script type="text/javascript">
			$(document).ready(function(){
			
				new jPlayerPlaylist({
					jPlayer: "#jquery_jplayer_1",
					cssSelectorAncestor: "#jp_container_1"
				}, [

  	<?php  
			for ($i = 0; $i < count($cArray); $i++ ) {
				$t++;
				$file = $cArray[$i];
				$remote = null;
				$name = $file->getFileName();
				$file_path = BASE_URL.DIR_REL.$file->getDownloadURL();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);
				if($image==null){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}else{
					$image = File::getByID($image->fID);
					$image_path=$image->getURL();
				}
				
				
				$file_src = BASE_URL.DIR_REL.$file->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $file->getAttribute($ak_s);
				if($audio){
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				if ($t > 1 ){ echo ',';}
				?>
				{
					title:"<?php  echo $name?>",
					<?php  echo substr($file_src, -3, 3)?>: "<?php  echo $file_src?>"
				}
				<?php  
				}
				?>
			], {
				swfPath: "<?php  echo $uh->getBlockTypeAssetsURL($bt)?>/tools",
				supplied: "<?php  echo substr($file_src, -3, 3)?>",
				wmode: "window"
			});

		});
	</script>
	
		<div id="jquery_jplayer_1" class="jp-jplayer"></div>

		<div id="jp_container_1" class="jp-audio">
				<div class="jp-type-playlist">
					<div class="jp-gui jp-interface">
						<ul class="jp-controls">
							<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
							<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
							<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
							<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
							<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
							<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
							<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
							<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
						</ul>
						<div class="jp-progress">
							<div class="jp-seek-bar">
								<div class="jp-play-bar"></div>
							</div>
						</div>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
						<div class="jp-time-holder">
							<div class="jp-current-time"></div>
							<div class="jp-duration"></div>
						</div>
						<ul class="jp-toggles">
							<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
							<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
							<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
							<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
						</ul>
					</div>
					<div class="jp-playlist">
						<ul>
							<li></li>
						</ul>
					</div>
					<div class="jp-no-solution">
						<span>Update Required</span>
						To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
					</div>
				</div>
		</div>

	<script type="text/javascript">
	/*<![CDATA[*/

	/*]]>*/
	</script>
	<?php   
	} 
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php   echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /><a href="<?php   echo BASE_URL.$rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php   echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php   echo $controller->rssTitle?>" />
			<br/>
		<?php   
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>