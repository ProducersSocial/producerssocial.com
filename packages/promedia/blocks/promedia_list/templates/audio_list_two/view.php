<?php    
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '400';
    $width = '600';
    
	if (count($cArray) > 0) { ?>
	<div class="scroller">
  	<?php   
			for ($i = 0; $i < count($cArray); $i++ ) {
				$t++;
				$file = $cArray[$i];
				$remote = null;
				$name = $file->getFileName();
				$download = null;
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);
				if($image==null){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}else{
					$image = File::getByID($image->fID);
					$image_path=$image->getURL();
				}
							
				$file_src = BASE_URL.DIR_REL.$file->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $file->getAttribute($ak_s);
				if($audio){
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				
				// CRN the following code populates the download variable with the download URL specified in the ProMedia Manager
				if(!$download){
					$ak_s = FileAttributeKey::getByHandle('media_download'); 
					$download = $file->getAttribute($ak_s);
					if($download){
						$download_array = $mh->getMediaTypeOutput($download);
						$download_path = $download_array['path'];
						$download= $download_path;
						//$download= $download_path.'?width='.$width.'&height='.$height;
						// CRN the width and height settings are neccisary for chrome otherwise the file doesn't play in the browser when you click on it
					}else{
						$download = null;
					}
				}
				
				?>
			<div class="scroll_item">
				<h2><?php    echo $file->getTitle() ?></h2>
				<div class="preview pre<?php   echo $i?>">
					<?php   
					if($image_f){
					?>
					<img src="<?php   echo $image_path?>" alt="video_img" class="thumb_img"/>
					<?php   
					}
					?>
					<audio class="audioplayer" id="play" src="<?php echo $file_src?>" controls="controls" type="audio/mp3"></audio><br style="clear: both;"/>
				</div>
				<br/>
				<div class="audio-desc">
					<?php     
					if(!$controller->truncateSummaries){
						echo '<p>' . $file->getDescription() . '</p>';
					}else{
						echo $textHelper->shorten($file->getDescription(),$controller->truncateChars);
					} 
					?>
					<p><a href="<?php   echo $download?>" title="Download file <?php    echo $file->getTitle() ?>">Download this File</a></p>
	            </div>
			</div>
			<?php   
		
	}
	?>
	</div>
	<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){
		$('video,audio').mediaelementplayer();
	});
	/*]]>*/
	</script>
	<?php    
	} 
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php    echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /><a href="<?php    echo BASE_URL.$rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php    echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php    echo $controller->rssTitle?>" />
			<br/>
		<?php    
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>