<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	Loader::model('file');
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '340';
    $width = '500';
 
    
	//if (count($cArray) > 0) { ?>
	<div id="highlights_body">
		<div class="scroller">
  		<?php   
		for ($i = 0; $i < count($cArray); $i++ ) {
			$file = $cArray[$i];
			$name = $file->getFileName();
			$file_path = $file->getDownloadURL();
			$ak_d = FileAttributeKey::getByHandle('media_date'); 
			$date = $file->getAttribute($ak_d);
			
			$image = null;
			$image_path = null;
			$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
			$image = $file->getAttribute($ak_a);
			
			
			$ak_s = FileAttributeKey::getByHandle('media_audio'); 
			$audio = $file->getAttribute($ak_s);
			if($audio){
				$audio_array = $mh->getMediaTypeOutput($audio);
				$audio_path = $audio_array['path'];
				$file_src = $audio_path;
			}
			
			$ak_s = FileAttributeKey::getByHandle('media_video'); 
			$video = $file->getAttribute($ak_s);
			
			if($video){
				$remote_array = $mh->getMediaTypeOutput($video);
				$remote = $remote_array['path'];
				$image_path = $mh->getMediaThumbnail($remote_array['id'],$remote_array['type']);
			}
			
			if(is_object($image)){
				$image_path = $image->getURL();
			}
			
			if(!$image && !$image_path){
				$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
			}
			
			$ak_n = FileAttributeKey::getByHandle('media_notes'); 
			$notes = $file->getAttribute($ak_n);
			?>
			<div class="scroll_item pre<?php   echo $i?>">
				<div class="preview">
					<img src="<?php   echo $image_path?>" alt="video_img" class="thumb_img"/>
					<a id="player_get<?php   echo $i?>" href="javascript:;" onClick="getMedia('<?php   echo $i?>');"><?php   echo $file->getTitle()?></a>
					
					<br/>
					<?php   
					if(!$controller->truncateSummaries){
						echo $file->getDescription();
					}else{
						echo $textHelper->shorten($file->getDescription(),$controller->truncateChars);
					}
				?>
				</div>
			</div>
			<?php   
			}
			?>
		</div>
		
		<div class="main_display">
			<div id="display_player" class="display_player">

			</div>
			
			<?php   
			for ($i = 0; $i < count($cArray); $i++ ) {
				$file = $cArray[$i];
				$remote = null;
				$audio = null;
				$video = null;
				$download = null;
				$name = $file->getFileName();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$image = null;
				$image_path = null;
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);

				
				$ftype = $file->getExtension();
				$vtype_array = array('m4v','mpeg','mp4','mpg','wmv','avi','mov','flv');
				$atype_array = array('mp3');
				$dtype_array = array('pdf','doc');
				if(in_array($ftype,$vtype_array)){
					$video = $file->getRelativePath();
				}elseif(in_array($ftype,$atype_array)){
					$audio = $file->getRelativePath();
				}elseif(in_array($ftype,$dtype_array)){
					$download = $file->getRelativePath();
				}

	
				if(!$audio){
					$ak_s = FileAttributeKey::getByHandle('media_audio'); 
					$audio = $file->getAttribute($ak_s);
					if($audio){
						$audio_array = $mh->getMediaTypeOutput($audio);
						$audio_path = $audio_array['path'];
						$audio = $audio_path.'?width='.$width.'&height='.$height;
					}else{
						$audio = null;
					}
				}

				if(!$video){
					$ak_s = FileAttributeKey::getByHandle('media_video'); 
					$video = $file->getAttribute($ak_s);
					if($video){
						$video_array = $mh->getMediaTypeOutput($video);
						$video_path = $video_array['path'];
						$image_path = $mh->getMediaThumbnail($video_array['id'],$video_array['type']);
						$video = $video_path.'?width='.$width.'&height='.$height;
					}else{
						$video = null;
					}
				}
					
				if(!$download){
					$ak_s = FileAttributeKey::getByHandle('media_download'); 
					$download = $file->getAttribute($ak_s);
					if($download){
						$download_array = $mh->getMediaTypeOutput($download);
						$download_path = $download_array['path'];
						$download= $download_path.'?width='.$width.'&height='.$height;
					}else{
						$download = null;
					}
				}
				
				if(is_object($image)){
					$image_path = $image->getURL();
				}
				
				if(!$image && !$image_path){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}

				$description = $file->getDescription();
				//var_dump($file->getRelativePath());
				?>
				<div id="player_<?php   echo $i?>" style="display: none;" class="media_contain player_<?php   echo $i?>">
					<div class="audio_embed_<?php   echo $i?>" alt="<?php  echo $audio?>"></div>
					<div class="video_embed_<?php   echo $i?>" alt="<?php  echo $video?>"></div>
					<div class="download_embed_<?php   echo $i?>" alt="<?php  echo $download?>"></div>
					<div class="description_<?php   echo $i?>"><?php  echo $description?></div>
				</div>
				<?php   
			} 
		?>
		</div>
	</div>
	<div id="player_info">
			
	</div>
	
	
	
	<br style="clear:both;">
	<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){
		var html = '';
		var ihtml = '';
		var video = $('.video_embed_0').attr('alt');
		var audio = $('.audio_embed_0').attr('alt');
		var download = $('.download_embed_0').attr('alt');
		var description = $('.description_0').html();
		
		var extension = video.substr( (video.lastIndexOf('.') +1) );

	    if(extension){
		    switch(extension) {
		        case 'm4v':  
		        case 'mpeg':
		        case 'mpg':
		        case 'wmv':
		        case 'avi':
		        case 'mov':
		        case 'flv':
		        case 'f4v':
		        case 'mp4':
		          html = '<video id="play_0" controls="controls" src="'+video+'" type="video/'+extension+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video>';
		          break;     
		        default:
		          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+video+'" frameborder="0" allowfullscreen></iframe>';  
		    }
		}
		
		$('#display_player').append(innerShiv(html));
		
		if(download.length > 0){
			ihtml += '<a href="'+download+'" class="ui-state-default ui-corner-all custom_style header_menu download"><span class="ui-icon ui-icon-document"></span><?php  echo t(' Download File ')?></a>';
		}
		
		if(audio.length > 0){
			ihtml += '<a href="#audio_playme" class="ui-state-default ui-corner-all custom_style header_menu audio_get_play" id="audio_play"><span class="ui-icon ui-icon-volume-on"></span><?php  echo t(' Play Audio ')?></a><div style="display: none;"><div id="audio_playme"><audio id="player"  src="'+audio+'" controls="controls" type="audio/mp3"></audio><br style="clear: both;"/></div></div>';
		}
		
		if(description.length > 0){
			//ihtml += '<span class="file_description">'+description+'</span>';
		}
		
		$('#player_info').append(innerShiv(ihtml));
		
		$('#audio_play').fancybox({
					onClosed: function(){
				    	$('video, audio').each(function() {
				          $(this)[0].player.pause();		  
				        });
					}
				});
		
		$('video,audio').mediaelementplayer();
	});
	

	function getMedia(i) {
		$('#display_player').empty();
		$('#player_info').empty();
		var html = '';
		var ihtml = '';
		var video = $('.video_embed_'+i).attr('alt');
		var audio = $('.audio_embed_'+i).attr('alt');
		var download = $('.download_embed_'+i).attr('alt');
		var description = $('.description_'+i).html();
		
		if(video){
			var extension = video.substr( (video.lastIndexOf('.') +1) );
			
			if(extension){
			    switch(extension) {
			        case 'm4v':  
			        case 'mpeg':
			        case 'mpg':
			        case 'wmv':
			        case 'avi':
			        case 'mov':
			        case 'flv':
			        case 'f4v':
			        case 'mp4':
			          html = '<video id="play_0" controls="controls" src="'+video+'" type="video/'+extension+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video>';
			          break;     
			        default:
			          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+video+'" frameborder="0" allowfullscreen></iframe>';  
			    }
			}
	
			$('#display_player').append(innerShiv(html));
		}
		
		if(download.length > 0){
			ihtml += '<a href="'+download+'" class="ui-state-default ui-corner-all custom_style header_menu download"><span class="ui-icon ui-icon-document"></span><?php  echo t(' Download File ')?></a>';
		}
		
		//alert(aextension)
		if(audio && audio.length > 0){
			var aextension = audio.substr( (audio.lastIndexOf('.') +1) , 3);
			ihtml += '<a href="#audio_playme" class="ui-state-default ui-corner-all custom_style header_menu audio_get_play" id="audio_play"><span class="ui-icon ui-icon-volume-on"></span><?php  echo t(' Play Audio ')?></a><div style="display: none;"><div id="audio_playme"><audio id="player"  src="'+audio+'" controls="controls" type="audio/'+aextension+'"></audio><br style="clear: both;"/></div></div>';
		}
		
		if(description.length > 0){
			//ihtml += '<span class="file_description">'+description+'</span>';
		}
		
		$('#player_info').append(innerShiv(ihtml));
		
		$('#audio_play').fancybox({
					onClosed: function(){
				    	$('video, audio').each(function() {
				          $(this)[0].player.pause();		  
				        });
					}
				});
		
		$('video,audio').mediaelementplayer({
			/*@cc_on
			@if (@_jscript_version == 9)
			            mode: 'shim'
			@end
			@*/
		});
	}
	/*]]>*/
	</script>
	<?php  
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php   echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /> <a href="<?php   echo $rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php   echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php   echo $controller->rssTitle?>" />
			<br/>
	<?php    
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>