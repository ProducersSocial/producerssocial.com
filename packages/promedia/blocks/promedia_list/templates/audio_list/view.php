<?php    
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '400';
    $width = '600';
    
	if (count($cArray) > 0) { ?>
	<div class="scroller">
  	<?php   
			for ($i = 0; $i < count($cArray); $i++ ) {
				$t++;
				$file = $cArray[$i];
				$remote = null;
				$name = $file->getFileName();
				$file_path = BASE_URL.DIR_REL.$file->getDownloadURL();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);
				if($image==null){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}else{
					$image = File::getByID($image->fID);
					$image_path=$image->getURL();
				}
				
				
				$file_src = BASE_URL.DIR_REL.$file->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $file->getAttribute($ak_s);
				if($audio){
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				
				// CRN the following code populates the download variable with the download URL specified in the ProMedia Manager
				$download_path = null;
				if(!$download){
					$ak_s = FileAttributeKey::getByHandle('media_download'); 
					$download = $file->getAttribute($ak_s);
					if($download){
						$download_array = $mh->getMediaTypeOutput($download);
						$download_path = $download_array['path'];
						//$download= $download_path.'?width='.$width.'&height='.$height;
						// CRN the width and height settings are neccisary for chrome otherwise the file doesn't play in the browser when you click on it
					}
				}
				?>
                <h2><?php   echo $file->getTitle() ?></h2>
			<div class="skin-wrapper" data-skin-name="premium-pixels">
					<script type="text/javascript">
					 $(document).ready(function(){
					  $("#jquery_jplayer_<?php   echo $i?>").jPlayer({
					   ready: function () {
					    $(this).jPlayer("setMedia", {
					     <?php   echo substr($file_src, -3, 3)?>: "<?php   echo $file_src?>"
					    });
					   },
					   play: function() { // To avoid both jPlayers playing together.
							$(this).jPlayer("pauseOthers");
						},
						repeat: function(event) { // Override the default jPlayer repeat event handler
							if(event.jPlayer.options.loop) {
								$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
								$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
									$(this).jPlayer("play");
								});
							} else {
								$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
								$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
									$("#jquery_jplayer_2").jPlayer("play", 0);
								});
							}
						},
					   swfPath: "<?php   echo $uh->getBlockTypeAssetsURL($bt)?>/tools",
					   supplied: "<?php   echo substr($file_src, -3, 3)?>",
					   cssSelectorAncestor: "#jp_container_<?php   echo $i?>",
					   wmode: "window",
					   preload: "none",
					   solution: "html,flash"
					  });
					 });
					</script>
					<div id="jquery_jplayer_<?php   echo $i?>" class="jp-jplayer"></div>
					
							<div id="jp_container_<?php   echo $i?>" class="jp-audio">
								<div class="jp-type-single">
									<div class="jp-gui jp-interface">
										<ul class="jp-controls">
											<li class="jp-play"><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
											<li class="jp-pause"><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
											<li class="jp-stop"><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
											<li class="jp-mute"><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
											<li class="jp-unmute"><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
											<li class="jp-volume-max"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
										</ul>
										<div class="jp-progress">
											<div class="jp-seek-bar">
												<div class="jp-play-bar"></div>
											</div>
										</div>
										<div class="jp-volume-bar">
											<div class="jp-volume-bar-value"></div>
										</div>
										<div class="jp-time-holder">
											<div class="jp-current-time"></div>
											<div class="jp-duration"></div>
				
											<ul class="jp-toggles">
												<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
												<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
												<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
												<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
											</ul>
										</div>
									</div>

									<div class="jp-no-solution">
										<span>Update Required</span>
										To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
									</div>
								</div>
							</div>		
					</div>
                    <div class="audio-desc">
					<?php    
					if(!$controller->truncateSummaries){
						echo '<p>' . $file->getDescription() . '</p>';
					}else{
						echo $textHelper->shorten($file->getDescription(),$controller->truncateChars);
					} 
					?>
            <?php  if($download_path){ ?>
            <p>
                <a href="<?php   echo $download_path; ?>" target="_blank" title="Download the <?php   echo $file->getTitle() ?>">Download this File</a>
            </p>
            <?php  } ?>
            </div>
			 <?php   } ?>
	</div>
	<script type="text/javascript">
	/*<![CDATA[*/

	/*]]>*/
	</script>
	<?php    
	} 
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php    echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /><a href="<?php    echo BASE_URL.$rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php    echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php    echo $controller->rssTitle?>" />
			<br/>
		<?php    
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>