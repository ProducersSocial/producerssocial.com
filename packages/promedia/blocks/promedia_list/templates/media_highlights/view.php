<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	Loader::model('file');
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '340';
    $width = '500';
    
	//if (count($cArray) > 0) { ?>
	<div id="highlights_body">
		<div class="scroller">
  		<?php   
		for ($i = 0; $i < count($cArray); $i++ ) {
			$file = $cArray[$i];
			$name = $file->getFileName();
			$file_path = $file->getDownloadURL();
			$ak_d = FileAttributeKey::getByHandle('media_date'); 
			$date = $file->getAttribute($ak_d);
			
			$image = null;
			$image_path = null;
			$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
			$image = $file->getAttribute($ak_a);
			
			$ak_s = FileAttributeKey::getByHandle('media_video'); 
			$video = $file->getAttribute($ak_s);
			
			if($video){
				$remote_array = $mh->getMediaTypeOutput($video);
				$remote = $remote_array['path'];
				$image_path = $mh->getMediaThumbnail($remote_array['id'],$remote_array['type']);
			}

			if(is_object($image)){
				$image_path = $image->getURL();
			}
			
			if(!$image && !$image_path){
				$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
			}
			
			$ak_n = FileAttributeKey::getByHandle('media_notes'); 
			$notes = $file->getAttribute($ak_n);
			?>
			<div class="scroll_item pre<?php   echo $i?>" onClick="getMedia('<?php   echo $i?>');">
				<div class="preview">
					<img src="<?php   echo $image_path?>" alt="video_img" class="thumb_img"/>
					<a id="player_get<?php   echo $i?>" href="javascript:;" ><?php   echo $file->getTitle()?></a>
					<br/>
					<?php   
					if(!$controller->truncateSummaries){
						echo $file->getDescription();
					}else{
						echo $textHelper->shorten($file->getDescription(),$controller->truncateChars);
					}
				?>
				</div>
			</div>
			<?php   
			}
			?>
		</div>
		
		<div class="main_display">
			<div id="display_player" class="display_player">
			
			</div>
			<?php   
			for ($i = 0; $i < count($cArray); $i++ ) {
				$file = $cArray[$i];
				$remote = null;
				$name = $file->getFileName();
				$file_path = BASE_URL.DIR_REL.$file->getDownloadURL();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);
				if($image==null){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}else{
					$image = File::getByID($image->fID);
					$image_path=$image->getURL();
				}
				
				
				$file_src = $file->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $file->getAttribute($ak_s);
				if($audio){
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				
				$video = null;
				$ak_s = FileAttributeKey::getByHandle('media_video'); 
				$video = $file->getAttribute($ak_s);
	
				if($video){
					$video_array = $mh->getMediaTypeOutput($video);
					$video_path = $video_array['path'];
					$image_path = $mh->getMediaThumbnail($video_array['id'],$video_array['type']);
					//$video_src = $video_path.'?width='.$width.'&height='.$height;
					//$file_src = $video_src;
					if($video_array['type'] == 'vimeo'){
						$file_src = 'http://player.vimeo.com/video/'.$video_array['id'];
					}else{
						$file_src = $video_path.'?width='.$width.'&height='.$height;
					}
				}

				$ak_s = FileAttributeKey::getByHandle('media_download'); 
				$download = $file->getAttribute($ak_s);

				//var_dump($file->getRelativePath());
				?>
				<div id="player_<?php   echo $i?>" style="display: none;" class="media_contain player_<?php   echo $i?>">
					<div class="player_embed_<?php   echo $i?>" alt="<?php  echo $file_src?>"></div>
				</div>
				<?php   
				
			unset($remote);
			} 
		?>
		</div>
	</div>
	<br style="clear:both;">
	<script type="text/javascript">
	/*<![CDATA[*/
	//load up the player 0 for initial page load
	$(document).ready(function(){
		var html = '';
		var path = $('.player_embed_0').attr('alt');
		var extension = path.substr( (path.lastIndexOf('.') +1) );
	    switch(extension) {
	        case 'm4v':  
	        case 'mpeg':
	        case 'mpg':
	        case 'wmv':
	        case 'avi':
	        case 'mov':
	        case 'flv':
	        case 'f4v':
	        case 'mp4':
	          html = '<video id="play_0" controls="controls" src="'+path+'" type="video/'+extension+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video>';
	          break;    
	        case 'mp3':
	        case 'm4a':
	          html = '<audio class="audioplayer" id="play_0"  src="'+path+'" controls="controls" type="audio/mp3"></audio>';
	          break;   
	        default:
	          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+path+'" frameborder="0" allowfullscreen></iframe>';  
	    }
		var load_check = $('#display_player').html();
		$('#display_player').append(innerShiv(html));
		$('video,audio').mediaelementplayer();
	});
	

	function getMedia(i) {
		//grab player instance, add to player box
		$('#display_player').empty();
		var html = '';
		var path = $('.player_embed_'+i+'').attr('alt');
		var extension = path.substr( (path.lastIndexOf('.') +1) );
		
		if(extension){
		    switch(extension) {
		        case 'm4v':  
		        case 'f4v': 
		        case 'mpeg':
		        case 'mpg':
		        case 'wmv':
		        case 'avi':
		        case 'mov':
		        case 'flv':
		        case 'mp4':
		          html = '<video class="player_box" id="play_'+i+'" controls="controls" src="'+path+'" type="video/'+extension+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video>';
		          break;    
		        case 'mp3':
		          html = '<audio class="audioplayer" id="play_'+i+'"  src="'+path+'" controls="controls" type="audio/mp3"></audio>';
		          break;   
		        default:
		          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+path+'" frameborder="0" allowfullscreen></iframe>';
		          break;    
		    }
	    }
	    //alert(html);
		$('#display_player').append(innerShiv(html));
		$('video,audio').mediaelementplayer();
	}
	/*]]>*/
	</script>
	<?php  
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php   echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /> <a href="<?php   echo $rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php   echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php   echo $controller->rssTitle?>" />
			<br/>
		<?php    
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>