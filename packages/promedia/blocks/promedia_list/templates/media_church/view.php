<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '400';
    $width = '600';
    
	if (count($cArray) > 0) { ?>
	<script type="text/javascript">
	/*<![CDATA[*/

	function getMedia(t,i) {
		$('#popup_prep').empty();
		var html = '';
		var path = $(t).attr('alt');
		var extension = path.substr( (path.lastIndexOf('.') +1) );
		
		if(extension){
		    switch(extension) {
		        case 'm4v':  
		        case 'mpeg':
		        case 'mpg':
		        case 'wmv':
		        case 'avi':
		        case 'mov':
		        case 'flv':
		        case 'f4v':
		        case 'mp4':
		          html = '<video class="player_box" id="play_'+i+'" type="video/'+extension+'" controls="controls" src="'+path+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video><br style="clear:both;"/>';
		          break;    
		          var player = new MediaElementPlayer('video,audio');
		        case 'mp3':
		        case 'm4a':
		          html = '<audio class="audioplayer" id="play_'+i+'"  src="'+path+'" controls="controls" type="audio/mp3"></audio><br style="clear:both;"/>';
		          var player = new MediaElementPlayer('video,audio');
		          break;   
		        default:
		          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+path+'" frameborder="0" allowfullscreen></iframe>'; 
		    }
	    }
		$('#popup_prep').append(innerShiv(html));
	}
	/*]]>*/
	</script>
	<div class="scroller">
  	<?php  
			for ($i = 0; $i < count($cArray); $i++ ) {
				$file = $cArray[$i];
				$remote = null;
				$audio = null;
				$video = null;
				$download = null;
				$name = $file->getFileName();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$image = null;
				$image_path = null;
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);

				
				$ftype = substr($file->getFileName(),-3);
				$vtype_array = array('m4v','mpeg','mp4','mpg','wmv','avi','mov','flv');
				$atype_array = array('mp3');
				$dtype_array = array('pdf','doc');
				if(in_array($ftype,$vtype_array)){
					$video = $file->getRelativePath();
				}elseif(in_array($ftype,$atype_array)){
					$audio = $file->getRelativePath();
				}elseif(in_array($ftype,$dtype_array)){
					$download = $file->getRelativePath();
				}

	
				if(!$audio){
					$ak_s = FileAttributeKey::getByHandle('media_audio'); 
					$audio = $file->getAttribute($ak_s);
					if($audio){
						$audio_array = $mh->getMediaTypeOutput($audio);
						$audio_path = $audio_array['path'];
						$audio = $audio_path;
					}else{
						$audio = null;
					}
				}

				if(!$video){
					$ak_s = FileAttributeKey::getByHandle('media_video'); 
					$video = $file->getAttribute($ak_s);
					if($video){
						$video_array = $mh->getMediaTypeOutput($video);
						$video_path = $video_array['path'];
						$image_path = $mh->getMediaThumbnail($video_array['id'],$video_array['type']);
						$video = $video_path.'?width='.$width.'&height='.$height;
					}else{
						$video = null;
					}
				}
					
				if(!$download){
					$ak_s = FileAttributeKey::getByHandle('media_download'); 
					$download = $file->getAttribute($ak_s);
					if($download){
						$download_array = $mh->getMediaTypeOutput($download);
						$download_path = $download_array['path'];
						$download= $download_path.'?width='.$width.'&height='.$height;
					}else{
						$download = null;
					}
				}
				
				if(is_object($image)){
					$image_path = $image->getURL();
				}
				
				if(!$image && !$image_path){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}

				$description = $file->getDescription();

				?>
			<div class="scroll_item">
				<div class="preview pre<?php  echo $i?>">
					<?php  
					if($image_path){
					?>
					<img src="<?php  echo $image_path?>" alt="video_img" class="thumb_img"/>
					<?php  
					}
					?>
						<div class="button_float">
							<?php   if($download){  ?>
							<a id="download_get_play<?php  echo $i?>" href="<?php  echo $download?>" target="_blank" class="download_button"></a>
							<?php   } ?>
							
							<?php   if($video){  ?>
							<a id="video_get_play<?php  echo $i?>" alt="<?php  echo $video?>" href="#popup_prep" onClick="getMedia( this , '<?php  echo $i?>')" class="video_button popup"></a>
							<?php   } ?>
							
							<?php   if($audio){  ?>
							<a id="audio_get_play<?php  echo $i?>" alt="<?php  echo $audio?>" href="#popup_prep" onClick="getMedia( this ,'<?php  echo $i?>')" class="play_button popup"></a>
							<?php   } ?>
						</div>
						
						<?php  echo wordwrap($file->getTitle(), 23, '<br/>')?>
				</div>
				<br style="clear: both;"/>
			</div>
			<div style="display: none;">
				<div id="popup_prep">
				
				</div>
			</div>
			<script type="text/javascript">
			/*<![CDATA[*/
			$(document).ready(function() {
				$('.popup').fancybox({
					onClosed: function(){
				    	$('video, audio').each(function() {
				          $(this)[0].player.pause();		  
				        });
					}
				});
			});
			/*]]>*/
			</script>
			<?php  
		
	}
	?>
	</div>
	<?php   

	} 
	if(!$previewMode && $controller->rss) { 
			$btID = $b->getBlockTypeID();
			$bt = BlockType::getByID($btID);
			$uh = Loader::helper('concrete/urls');
			$rssUrl = $controller->getRssUrl($b);
			?>
			<div class="rssIcon">
				<img src="<?php   echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /><a href="<?php   echo $rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php   echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php   echo $controller->rssTitle?>" />
			<br/>
		<?php   
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>