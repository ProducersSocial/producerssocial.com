<?php   
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$btID = $b->getBlockTypeID();
	$bt = BlockType::getByID($btID);
	$uh = Loader::helper('concrete/urls');
	$rssUrl = $controller->getRssUrl($b);
	$textHelper = Loader::helper("text"); 
	$fl = Loader::model('file_version');
	$uh = Loader::helper('concrete/urls');
	$mh = Loader::helper('media_type','promedia');
    $bt = BlockType::getByHandle('promedia_list');
    $height = '400';
    $width = '600';
    $spread = 4;
    $playerID = rand(0,777777);
    
	if (count($cArray) > 0) { ?>
	<div class="scroller">
	<table id="media_galery">
  	<?php  
  	$t = 0;

			for ($i = 0; $i < count($cArray); $i++ ) {
				$t++;
				$file = $cArray[$i];
				$remote = null;
				$name = $file->getFileName();
				$file_path = BASE_URL.DIR_REL.$file->getDownloadURL();
				$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $file->getAttribute($ak_d);
				
				$ak_a = FileAttributeKey::getByHandle('media_artwork'); 
				$image = $file->getAttribute($ak_a);
				if($image==null){
					$image_path=$uh->getBlockTypeAssetsURL($bt).'/tools/mp3.png';
				}else{
					$image = File::getByID($image->fID);
					$image_path=$image->getURL();
				}
				
				
				$file_src = BASE_URL.DIR_REL.$file->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $file->getAttribute($ak_s);
				if($audio){
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				
				$video = null;
				$ak_s = FileAttributeKey::getByHandle('media_video'); 
				$video = $file->getAttribute($ak_s);
	
				if($video){
					$video_array = $mh->getMediaTypeOutput($video);
					$video_path = $video_array['path'];
					$image_path = $mh->getMediaThumbnail($video_array['id'],$video_array['type']);
					//$video_src = $video_path.'?width='.$width.'&height='.$height;
					//$file_src = $video_src;
					if($video_array['type'] == 'vimeo'){
						$file_src = 'http://player.vimeo.com/video/'.$video_array['id'];
					}else{
						$file_src = $video_path;
					}
				}
				?>
				<td valign="top">

					</a><img src="<?php   echo $image_path?>" href="#popup_prep" alt="<?php  echo $file_src?>" class="<?php  echo $playerID?>player_get<?php  echo $i?> gallery_thumb" href="<?php  echo $file_src?>"/>
				
				</td>
			<?php  
			if($t == $spread){
				$t=0;
				echo '</tr>';
			}
	}

	for($d=$t;$d<$spread;$d++){
		echo '<td></td>';
		if($t == $spread){
			echo '</tr>';
		}
	}
	?>
	</table>
	</div>
	<div style="display: none;">
		<div id="popup_prep">
		
		</div>
	</div>
	<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function() {
		$('.gallery_thumb').fancybox({
			width: 'auto',
			height: 'auto',
			onClosed: function(){
		    	$('video, audio').each(function() {
		          $(this)[0].player.pause();		  
		        });
			}
		});
	});
	/*]]>*/
	</script>
	<script type="text/javascript">
	/*<![CDATA[*/
	$('.gallery_thumb').click(function(){
		$('#popup_prep').html('');
		var html = '';
		var path = $(this).attr('alt');
		var extension = path.substr( (path.lastIndexOf('.') +1) );

		if(extension){
		    switch(extension) {
		        case 'm4v':  
		        case 'mpeg':
		        case 'mpg':
		        case 'wmv':
		        case 'avi':
		        case 'mov':
		        case 'flv':
		        case 'f4v':
		        case 'mp4':
		          html = '<video class="player_box" id="play" controls="controls" src="'+path+'" width="<?php  echo $width?>" height="<?php  echo $height?>"></video><br style="clear: both;"/>';
		          var player = new MediaElementPlayer('video,audio');
		          break;    
		        case 'mp3':
		        case 'm4a':
		          html = '<audio class="audioplayer" id="play"  src="'+path+'" controls="controls" type="audio/m4a"></audio><br style="clear: both;"/>';
		          var player = new MediaElementPlayer('video,audio');
		          break;   
		        default:
		          html = '<iframe title="video player" width="<?php  echo $width?>px" height="<?php  echo $height?>px" src="'+path+'" frameborder="0" allowfullscreen></iframe>'; 
		    }
	    }
		$('#popup_prep').append(innerShiv(html));
	});
	/*]]>*/
	</script>
	<?php   

	} 
	if(!$previewMode && $controller->rss) { 
			?>
			<div class="rssIcon">
				<img src="<?php   echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" /><a href="<?php   echo $rssUrl?>" target="_blank">get this feed</a>
				
			</div>
			<link href="<?php   echo $rssUrl?>" rel="alternate" type="application/rss+xml" title="<?php   echo $controller->rssTitle?>" />
			<br/>
		<?php   
	} 

	
	if ($paginate && $num > 0 && is_object($_fl)) {
		$_fl->displayPaging();
	}
	
?>