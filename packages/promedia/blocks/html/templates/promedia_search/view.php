<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
global $c;
$keyword = $_REQUEST['media-search'];
?>
<style type="text/css">
.loader {display: block;float: left;margin-right: 32px;height:16px;width:16px;background-image:url('<?php  echo ASSETS_URL_IMAGES?>/icons/icon_header_loading.gif');}
</style>
<form id="media-search" class="styled" method="post">
	<div class="loader" style="display: none;"></div>
    <input type="text" class="text-input" name="media-search" id="media-search-filter" value="<?php echo $keyword?>" placeholder="Search..." rel="popover"/>
    <span id="filter-count"></span>
</form>

<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function() {
	$('#media-search').submit(function(e){
		e.preventDefault();
		var pathname = '<?php echo $c->getCollectionPath()?>';
		var keyword = $('#media-search-filter').val();
		window.location.href = pathname+'?media-search='+keyword;
	});
});
/*]]>*/
</script>