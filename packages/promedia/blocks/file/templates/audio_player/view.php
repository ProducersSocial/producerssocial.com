<?php     defined('C5_EXECUTE') or die("Access Denied.");
	$f = $controller->getFileObject();
	$fp = new Permissions($f);
	if ($fp->canRead()) { 
		$c = Page::getCurrentPage();
		if($c instanceof Page) {
			$cID = $c->getCollectionID();
		}
?>
<div class="audio-wrapper" data-skin-name="premium-pixels">
  	<?php   
				$name = $f->getFileName();
				$file_path = $f->getDownloadURL();
				//$ak_d = FileAttributeKey::getByHandle('media_date'); 
				$date = $f->getAttribute('media_date');
										
				$file_src = BASE_URL.DIR_REL.$f->getRelativePath();
				
				$ak_s = FileAttributeKey::getByHandle('media_audio'); 
				$audio = $f->getAttribute($ak_s);
				if($audio){
					$mh = Loader::helper('media_type','promedia');
					$audio_array = $mh->getMediaTypeOutput($audio);
					$audio_path = $audio_array['path'];
					$file_src = $audio_path;
				}
				?>
                <h2><?php   echo $f->getTitle() ?></h2>
					<script type="text/javascript">
					 $(document).ready(function(){
					  $("#jquery_jplayer_<?php   echo $f->getFileID(); ?>").jPlayer({
					   ready: function () {
					    $(this).jPlayer("setMedia", {
					     <?php   echo substr($file_src, -3, 3)?>: "<?php   echo $file_src?>"
					    });
					   },
					   play: function() { // To avoid both jPlayers playing together.
							$(this).jPlayer("pauseOthers");
						},
						repeat: function(event) { // Override the default jPlayer repeat event handler
							if(event.jPlayer.options.loop) {
								$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
								$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
									$(this).jPlayer("play");
								});
							} else {
								$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
								$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
									$("#jquery_jplayer_2").jPlayer("play", 0);
								});
							}
						},
					   swfPath: "packages/promedia/blocks/promedia_list/tools",
					   supplied: "<?php   echo substr($file_src, -3, 3)?>",
					   cssSelectorAncestor: "#jp_container_<?php   echo $f->getFileID();?>",
					   wmode: "window",
					   preload: "none",
					   solution: "html,flash"
					  });
					 });
					</script>
					<div id="jquery_jplayer_<?php   echo $f->getFileID();?>" class="jp-jplayer"></div>
					
							<div id="jp_container_<?php   echo $f->getFileID();?>" class="jp-audio">
								<div class="jp-type-single">
									<div class="jp-gui jp-interface">
										<ul class="jp-controls">
											<li class="jp-play"><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
											<li class="jp-pause"><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
											<li class="jp-stop"><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
											<li class="jp-mute"><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
											<li class="jp-unmute"><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
											<li class="jp-volume-max"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
										</ul>
										<div class="jp-progress">
											<div class="jp-seek-bar">
												<div class="jp-play-bar"></div>
											</div>
										</div>
										<div class="jp-volume-bar">
											<div class="jp-volume-bar-value"></div>
										</div>
										<div class="jp-time-holder">
											<div class="jp-current-time"></div>
											<div class="jp-duration"></div>
				
											<ul class="jp-toggles">
												<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
												<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
												<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
												<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
											</ul>
										</div>
									</div>

									<div class="jp-no-solution">
										<span>Update Required</span>
										To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
									</div>
								</div>
							</div>		
					
                    <div class="audio-desc">
				<?php    
				if($f->getDescription() != '') {
					echo '<p>' . $f->getDescription() . '</p>';
				}
			?>
			 <p><a href="<?php   echo $f->getDownloadURL(); ?>" target="_blank" title="Download the <?php   echo $f->getTitle() ?>">Download this File</a></p>
            </div>
           </div>
 <?php   } ?>

