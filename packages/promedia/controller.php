<?php   

defined('C5_EXECUTE') or die(_("Access Denied."));

class PromediaPackage extends Package {

	protected $pkgHandle = 'promedia';
	protected $appVersionRequired = '5.6.0';
	protected $pkgVersion = '7.5.0';
	
	public function getPackageDescription() {
		return t("A Professional Media Player");
	}
	
	public function getPackageName() {
		return t("Pro Media");
	}
	
	public function install() {
		$pkg = parent::install();
		
		//install blocks
	  	BlockType::installBlockTypeFromPackage('promedia_list', $pkg);	
		
		$this->load_required_models();
		
		$this->install_pm_attributes($pkg);
		
		$this->add_pm_pages($pkg);

	}
	
	
	public function upgrade(){
	
		$this->load_required_models();
		
		$pkg = Package::getByHandle('promedia');
		
		$_pkgVersion = $pkg->getPackageCurrentlyInstalledVersion();

	
		$eaku = AttributeKeyCategory::getByHandle('file');
		$media_type = AttributeType::getByHandle('media_type');
		
		if(!is_object($media_type) || !intval($media_type->getAttributeTypeID()) ) { 
		
			$media_type = AttributeType::add('media_type', t('Media Type'), $pkg);	  
			$eaku->associateAttributeKeyType($media_type);

			
			$evset = AttributeSet::getByHandle('promedia');
			
			$download=FileAttributeKey::getByHandle('media_download'); 
		  	$download->delete();
		  	$mediaScr = FileAttributeKey::getByHandle('media_script');
		  	$mediaScr->delete();
		  	$medianotes=FileAttributeKey::getByHandle('media_notes'); 
		  	$medianotes->delete();
		
			$media_type = AttributeType::getByHandle('media_type');
		  	$mediavid=FileAttributeKey::getByHandle('media_video'); 
		  	if( !is_object($mediavid) ) {
		     	FileAttributeKey::add($media_type, 
		     	array('akHandle' => 'media_video', 
		     	'akName' => t('Video'), 
		     	),$pkg)->setAttributeSet($evset); 
		  	}
		  	
		  	$mediaaud=FileAttributeKey::getByHandle('media_audio'); 
		  	if( !is_object($mediaaud) ) {
		     	FileAttributeKey::add($media_type, 
		     	array('akHandle' => 'media_audio', 
		     	'akName' => t('Audio'), 
		     	),$pkg)->setAttributeSet($evset); 
		  	}
		  	
		  	$mediadown=FileAttributeKey::getByHandle('media_download'); 
		  	if( !is_object($mediadown) ) {
		     	FileAttributeKey::add($media_type, 
		     	array('akHandle' => 'media_download', 
		     	'akName' => t('Download'), 
		     	),$pkg)->setAttributeSet($evset); 
		  	}
		}
		
		
		$promedia_dash = Page::getByPath('/dashboard/files/pro_media/add');
		if(!is_object($promedia_dash) || $promedia_dash->cID==NULL ){
			// install pages
			$iak = CollectionAttributeKey::getByHandle('icon_dashboard');
			
			$pm = SinglePage::add('/dashboard/files/pro_media', $pkg);
			$pm->update(array('cName'=>t('ProMedia'), 'cDescription'=>t('Multimedia Management for C5')));
			$pm->setAttribute($iak,'icon-facetime-video');
			
			$pml = SinglePage::add('/dashboard/files/pro_media/list', $pkg);
			$pml->update(array('cName'=>t('ProMedia File List')));
			$pml->setAttribute($iak,'icon-list-alt');
			
			$pma = SinglePage::add('/dashboard/files/pro_media/add', $pkg);
			$pma->update(array('cName'=>t('ProMedia Add/Edit')));
			$pma->setAttribute($iak,'icon-facetime-video');
		}
		
		$checkn = AttributeType::getByHandle('boolean'); 
	  	$pmfile=FileAttributeKey::getByHandle('promedia_file'); 
		if( !is_object($pmfile) ) {
	     	FileAttributeKey::add($checkn, 
	     	array('akHandle' => 'promedia_file', 
	     	'akName' => t('ProMedia File'),
	     	'akIsSearchable' => '1', 
	     	'akIsSearchableIndexed' => '1'
	     	)); 
	  	}
	  	
	  	$evseta = AttributeSet::getByHandle('promedia_additional_attributes');
	  	if(!$evseta->asID){
		  	$eaku->addSet('promedia_additional_attributes', t('Pro Media Additional Attributes'),$pkg);
	  	}
	}
	

	public function uninstall(){
	
		$this->load_required_models();
		
		$results= Page::getByPath('/media');
		$results->delete();
		
		$fs = FileSet::getByName('Audio');
		$fs->delete();
  		$fs = FileSet::getByName('Video');
  		$fs->delete();
		
		parent::uninstall();
	}
	
	
    function install_pm_attributes($pkg) {
    
  	$eaku = AttributeKeyCategory::getByHandle('file');
  	$eaku->setAllowAttributeSets(AttributeKeyCategory::ASET_ALLOW_SINGLE);
  	$evset = $eaku->addSet('promedia', t('Pro Media'),$pkg);
  	$evseta = $eaku->addSet('promedia_additional_attributes', t('Pro Media Additional Attributes'),$pkg);
  	
  	
  	FileSet::createAndGetSet('Audio',1);
  	FileSet::createAndGetSet('Video',1);
  	
  	
    $checkn = AttributeType::getByHandle('boolean'); 
  	$pmfile=FileAttributeKey::getByHandle('promedia_file'); 
	if( !is_object($pmfile) ) {
     	FileAttributeKey::add($checkn, 
     	array('akHandle' => 'promedia_file', 
     	'akName' => t('ProMedia File'),
     	'akIsSearchable' => '1', 
     	'akIsSearchableIndexed' => '1'
     	)); 
  	}
  	
  	
  	$media_file = AttributeType::getByHandle('media_image_file');
  	if(!is_object($media_file) || !intval($media_file->getAttributeTypeID()) ) { 
  		$media_file = AttributeType::add('media_image_file','Media File', $pkg);
  		$eaku->associateAttributeKeyType($media_file);
  	}
  	
  	$media_file = AttributeType::getByHandle('media_image_file');

  	
  	
  	$media_type = AttributeType::getByHandle('media_type');
	if(!is_object($media_type) || !intval($media_type->getAttributeTypeID()) ) { 
		$media_type = AttributeType::add('media_type', t('Media Type'), $pkg);	  
	
	  	$eaku->associateAttributeKeyType($media_type);
	  	
	    $pulln = AttributeType::getByHandle('select'); 
	  	$mediacat=FileAttributeKey::getByHandle('media_type'); 
		if( !is_object($mediacat) ) {
	     	FileAttributeKey::add($pulln, 
	     	array('akHandle' => 'media_type', 
	     	'akName' => t('Media Type'), 
	     	'akIsSearchable' => '1', 
	     	'akIsSearchableIndexed' => '1', 
			'akSelectAllowOtherValues' => true, 
	     	),$pkg)->setAttributeSet($evset); 
	     	$ak=FileAttributeKey::getByHandle('media_type');
	     	SelectAttributeTypeOption::add($ak,'worship');
	     	SelectAttributeTypeOption::add($ak,'teaching');
	     	SelectAttributeTypeOption::add($ak,'childrens');
	  	}
	}

  	$date = AttributeType::getByHandle('date');
  	$mediadate=FileAttributeKey::getByHandle('media_date'); 
  	if( !is_object($mediadate) ) {
     	FileAttributeKey::add($date, 
     	array('akHandle' => 'media_date', 
     	'akName' => t('Media Date'), 
     	),$pkg)->setAttributeSet($evset); 
  	}
  	
  	
  	$imagen = AttributeType::getByHandle('media_image_file'); 
  	$mediaart=FileAttributeKey::getByHandle('media_artwork'); 
	if( !is_object($mediaart) ) {
     	FileAttributeKey::add($imagen, 
     	array('akHandle' => 'media_artwork', 
     	'akName' => t('Media Artwork'), 
     	),$pkg)->setAttributeSet($evset); 
  	}
  	 
  	
  	$media_type = AttributeType::getByHandle('media_type');
  	$mediavid=FileAttributeKey::getByHandle('media_video'); 
  	if( !is_object($mediavid) ) {
     	FileAttributeKey::add($media_type, 
     	array('akHandle' => 'media_video', 
     	'akName' => t('Video'), 
     	),$pkg)->setAttributeSet($evset); 
  	}
  	
  	$mediaaud=FileAttributeKey::getByHandle('media_audio'); 
  	if( !is_object($mediaaud) ) {
     	FileAttributeKey::add($media_type, 
     	array('akHandle' => 'media_audio', 
     	'akName' => t('Audio'), 
     	),$pkg)->setAttributeSet($evset); 
  	}
  	
  	$mediadown=FileAttributeKey::getByHandle('media_download'); 
  	if( !is_object($mediadown) ) {
     	FileAttributeKey::add($media_type, 
     	array('akHandle' => 'media_download', 
     	'akName' => t('Download'), 
     	),$pkg)->setAttributeSet($evset); 
  	}

  }
  
	function add_pm_pages($pkg) {
	
	
	// install pages
	$iak = CollectionAttributeKey::getByHandle('icon_dashboard');
	
	$pm = SinglePage::add('dashboard/files/pro_media', $pkg);
	$pm->update(array('cName'=>t('ProMedia'), 'cDescription'=>t('Multimedia Management for C5')));
	$pm->setAttribute($iak,'icon-facetime-video');
	
	$pml = SinglePage::add('dashboard/files/pro_media/list', $pkg);
	$pml->update(array('cName'=>t('ProMedia File List')));
	$pml->setAttribute($iak,'icon-list-alt');
	
	$pma = SinglePage::add('dashboard/files/pro_media/add', $pkg);
	$pma->update(array('cName'=>t('ProMedia Add/Edit')));
	$pma->setAttribute($iak,'icon-facetime-video');
	
	
	$pageType= CollectionType::getByHandle('left_sidebar');
 	if(!is_object($pageType)){  
 		$pageType= CollectionType::getByHandle('right_sidebar');
	 }
		if(!is_object($pageType)){  
 		$mediaPageTypes = array(
    	array('ctHandle' => 'media_item',   'ctName' => t('media item'),'ctIcon'=>t('template3.png')),
 		 );
  		foreach( $mediaPageTypes as $mediaPageType ) {
    		CollectionType::add($mediaPageType, $pkg);
 	 	}
  		$pageType= CollectionType::getByHandle('media_item');
		 }

	$pagemediaParent = Page::getByID(HOME_CID);
	$pagemediaParent->add($pageType, array('cName' => 'Media', 'cHandle' => 'media'),$pkg);
  }
  
  
  function load_required_models() {
    Loader::model('single_page');
    Loader::model('collection');
    Loader::model('file_set');
    Loader::model('page');
    loader::model('block');
    Loader::model('collection_types');
    Loader::model('/attribute/categories/file');
  }		
		
}