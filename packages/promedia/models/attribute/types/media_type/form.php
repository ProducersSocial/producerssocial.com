<?php  
$fm = Loader::helper('form');
$ak = $this->getAttributeKey();
//var_dump(substr($value,0,4));
//$value = $this->getValue();

if($value){
	if(substr($value,0,4)=='http'){
		$type = 'url';
		$url = $value;
	}else{
		$type = 'file';
		$lf = File::getByID($value);
		$url = '';
	}
}
?>
<div class="ccm-media-type">
	<input type="radio" class="type_<?php  echo $ak->getAttributeKeyID()?>" name="<?php  echo 'akID['.$ak->getAttributeKeyID().'][type]'?>" value="url" <?php   if($type=='url'){echo 'checked';}?>/> url
	<input type="radio" class="type_<?php  echo $ak->getAttributeKeyID()?>" name="<?php  echo 'akID['.$ak->getAttributeKeyID().'][type]'?>" value="choose" <?php   if($type=='file'){echo 'checked';}?>/> Choose From File Manager <br/><br/>
	<div class="url_<?php  echo $ak->getAttributeKeyID()?>" style="display: <?php   if($type=='url'){echo 'block';}else{echo 'none';}?>;">
		<?php  echo t('Please paste the full url')?><br/>
		<?php  echo $fm->text('akID['.$ak->getAttributeKeyID().'][url]',$url,array('size'=>60))?>
	</div>
	<div class="file_<?php  echo $ak->getAttributeKeyID()?>" style="display: <?php   if($type=='file'){echo 'block';}else{echo 'none';}?>;">
		<?php  echo Loader::helper('concrete/asset_library')->file('file_'.$ak->getAttributeKeyID(),'akID['.$ak->getAttributeKeyID().'][file]','Choose File',$lf)?>
	</div>
</div>
<script type="text/javascript">
	$('.type_<?php  echo $ak->getAttributeKeyID()?>').click(function(){
		var type = $(this).val();
		//alert(type);
		if(type == 'url'){
			$('.url_<?php  echo $ak->getAttributeKeyID()?>').show();
			$('.file_<?php  echo $ak->getAttributeKeyID()?>').hide();
		}else{
			$('.url_<?php  echo $ak->getAttributeKeyID()?>').hide();
			$('.file_<?php  echo $ak->getAttributeKeyID()?>').show();
		}
	});
</script>