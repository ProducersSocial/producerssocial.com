<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('file_list');
/**
*
* An object that allows a filtered list of blogs to be returned.
* @package ProBlog
*
**/
class PromediaList extends FileList {
	
	public function filterByKeyword($keywords) {
		$dbs = Loader::db();
		$keywordsExact = $dbs->quote($keywords);
		$qkeywords = $dbs->quote('%' . $keywords . '%');
		//$keys = FileAttributeKey::getSearchableIndexedList();
		//$attribsStr = '';
		//foreach ($keys as $ak) {
		//	$cnt = $ak->getController();			
		//	$attribsStr.=' OR ' . $cnt->searchKeywords($keywords);
		//}
		$this->filter(false,"(fv.fvTags LIKE $qkeywords OR fv.fvDescription LIKE $qkeywords OR fv.fvFilename LIKE $qkeywords OR fv.fvTitle LIKE $qkeywords)");
		//$this->filter(false, '(fv.fvTitle LIKE ' . $qkeywords . $attribsStr . ')');
	}

}