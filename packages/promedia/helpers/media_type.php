<?php   
/**
 * @package Helpers
 * @category ProMedia
 * @author ChadCantrell <stratisdesign.com>
 * @copyright  Copyright (c) 2003-2008 StratisDesign. (http://www.stratisdesign.com)
 * @license    http://www.concrete5.org/license/     MIT License
 */

defined('C5_EXECUTE') or die("Access Denied.");

// Load a compatiblity class for pre php 5.2 installs
//Loader::library('datetime_compat');

class MediaTypeHelper {

	function getMediaTypeOutput($media=null){
		$media = str_replace('www.','',$media);
		if(substr($media,0,18)=='http://youtube.com' || substr($media,0,19)=='https://youtube.com'){
			$video_path = str_replace("https","http",$media);
			$video_path = str_replace("http://youtube.com/watch?v=","",$video_path);
			$vid_array= explode('&',$video_path);
			$video_id = $vid_array[0];
			$path = array(
				'path'=> "http://www.youtube.com/embed/".$video_id,
				'id'=>$video_id,
				'type'=>'youtube'
			);
		}elseif(substr($media,0,16)=='http://vimeo.com' || substr($media,0,17)=='https://vimeo.com'){
			$video_id = str_replace("https","http",$media);
			$video_id = str_replace("http://vimeo.com/","",$video_id);
			$path = array(
				'path'=>"http://player.vimeo.com/video/".$video_id,
				'id'=>$video_id,
				'type'=>'vimeo'
			);
		}elseif(substr($media,0,4)=='http'){
			$path = array(
				'path'=> $media,
				'id'=>$video_id,
				'type'=>'remote_file'
			);
		}else{
			$path = array(
				'path'=>  BASE_URL.File::getRelativePathFromID($media),
				'id'=>$media,
				'type'=>'local_file'
			);
		}
		
		return $path;
	}
	
	function getMediaThumbnail($id=null,$type=null){
		switch ($type) {
			case 'vimeo':
				$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
				$img = $hash[0]['thumbnail_medium'];
				break;
			
			case 'youtube':
				$img = "http://img.youtube.com/vi/$id/0.jpg";
				break;
		}
		
		return $img;	
		
	}

}