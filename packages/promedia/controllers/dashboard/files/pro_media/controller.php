<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 
class DashboardFilesProMediaController extends Controller {
	
	public function view() {
		$this->redirect('/dashboard/files/pro_media/list/');
	}
	
}