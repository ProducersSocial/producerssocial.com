<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 
class DashboardFilesProMediaListController extends Controller {
	
	public function on_start(){
		Loader::model('file_list');
		Loader::model('file_set');
		$this->set('form',Loader::helper('form'));
	}
	
	public function view(){
		
		$fs = New FileSetList();
		$fsets = $fs->get();
		$this->set('fsets',$fsets);
		
		$fl = new FileList();
		$fl->filterByPromediaFile(1);
		if(!empty($_GET['set'])){
			$fss = FileSet::getByID($_GET['set']);
			$fl->filterBySet($fss);
		}
		if(!empty($_GET['types'])){
			$type = $_GET['types'];
			$fl->filter(false,"ak_media_type LIKE '%\n$type\n%'");
		}
		if(!empty($_GET['like'])){
			$fl->filterByKeywords($_GET['like']);
		}
		$files = $fl->get();
		$this->set('fl',$fl);
		$this->set('files',$files);
	}
	
	public function delete_check($fID,$name) {
		$this->set('remove_name',$name);
		$this->set('remove_fid',$fID);
		$this->view();
	}
	
	public function deletethis($fID,$name) {
		$f = File::getByID($fID);
		$f->delete();
		$this->set('message', t('"'.$name.'" has been deleted')); 
		$this->set('remove_name','');
		$this->set('remove_cid','');
		$this->view();
	}
	
	public function clear_warning(){
		$this->set('remove_name','');
		$this->set('remove_cid','');
		$this->view();
	}
	
	public function media_added() {
		$this->set('message', t('Media Item added.'));
		$this->view();
	}
	
	public function media_updated() {
		$this->set('message', t('Media Item updated.'));
		$this->view();
	}
}