<?php   
defined('C5_EXECUTE') or die(_("Access Denied.")); 
class DashboardFilesProMediaAddController extends Controller {
	
	public function on_start(){
		$this->error = Loader::helper('validation/error');
		Loader::model('/attribute/categories/file');
		Loader::model('file_set');

		$this->set('form',Loader::helper('form'));
	}
	

	public function view(){
		
	}
	
	
	public function edit($fID=null) {
		$f = File::getByID($fID);
		$this->set('media',$f);
		
		$sets = array();
		$fsl = $f->getFileSets();
		foreach($fsl as $fs){
			$sets[] = $fs->fsID;
		}
		$this->set('sets',$sets);
		if ($this->isPost()) {
			$this->validate();
			if (!$this->error->has()) {
				$prefix = $f->getPrefix();
				$f->updateDescription($this->post('mediaDescription'));
				$f->updateTitle($this->post('mediaTitle'));
				$f->updateFile($this->post('mediaTitle'),$prefix);
				$this->resetSets($f);
				$this->saveData($f);
				$this->redirect('/dashboard/files/pro_media/list/', 'media_updated');
			}
		}
	}
	

	public function add() {
		$this->set('sets',array());
		if ($this->isPost()) {
			$this->validate();
			if (!$this->error->has()) {
				
				Loader::library("file/importer");
		  		$fh = Loader::helper('file');
				$source = DIR_BASE . '/packages/promedia/files/';
				$files = $fh->getDirectoryContents($source);
				if(is_array($files)){
					foreach($files as $file){
						// replace the file in the system
						$fi = new FileImporter();
						$f = $fi->import($source.$file);
					}
				}
				$prefix = $f->getPrefix();
				$f->updateDescription($this->post('mediaDescription'));
				$f->updateTitle($this->post('mediaTitle'));
				$f->updateFile($this->post('mediaTitle'),$prefix);
				$this->resetSets($f);
				$this->saveData($f);
				$this->redirect('/dashboard/files/pro_media/list/', 'media_added');
			}
		}
	}


	protected function validate() {
		$vt = Loader::helper('validation/strings');
		$vn = Loader::Helper('validation/numbers');
		$dt = Loader::helper("form/date_time");		
		
		if (!$vt->notempty($this->post('mediaTitle'))) {
			$this->error->add(t('Title is required'));
		}
	}
	

	private function resetSets($f){
		$f = File::getByID($f->fID);
		Loader::model('file_set');
		$sets = $f->getFileSets();
		foreach($sets as $set){
			$set->removeFileFromSet($f);
		}
		if($this->post('file_set')){
			foreach($this->post('file_set') as $fsID){
				$fs = FileSet::getByID($fsID);
				$fs->addFileToSet($f);
			}
		}
	}
	
	private function saveData($f) {
	
		$cak = FileAttributeKey::getByHandle('promedia_file');
		$f->setAttribute($cak,1);	
	
		$cak = FileAttributeKey::getByHandle('media_type');
		$cak->saveAttributeForm($f);	
		
		$cck = FileAttributeKey::getByHandle('media_date');
		$cck->saveAttributeForm($f);
		
		$cck = FileAttributeKey::getByHandle('media_artwork');
		$cck->saveAttributeForm($f);
		
		$cck = FileAttributeKey::getByHandle('media_video');
		$cck->saveAttributeForm($f);
		
		$cnv = FileAttributeKey::getByHandle('media_audio');
		$cnv->saveAttributeForm($f);
		
		$ct = FileAttributeKey::getByHandle('media_download');
		$ct->saveAttributeForm($f);
			
	}

	public function on_before_render() {
		$this->set('error', $this->error);
	}
}