<?php   
$df = Loader::helper('form/date_time');

if (is_object($media)) { 
	$mediaTitle = $media->getFileName();
	$mediaDescription = $media->getDescription();
	$task = 'edit';
	$buttonText = t('Update Media Item');
	$title = 'Update';
} else {
	$task = 'add';
	$buttonText = t('Add Media Item');
	$title= 'Add';
}
?>
<?php  echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t($title.' Media Item').'<span class="label" style="position:relative;top:-3px;left:12px;">'.t('* required field').'</span>', false, false, false);?>
	<div class="ccm-pane-body">
	<?php   if ($this->controller->getTask() == 'edit') { ?>
		<form method="post" action="<?php   echo $this->action($task,$media->getFileID())?>" id="media-form">
	<?php   }else{ ?>
		<form method="post" action="<?php   echo $this->action($task)?>" id="media-form">
	<?php   } ?>
	
			<ul class="tabs">
				<li class="active"><a href="javascript:void(0)" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.post').show();"><?php  echo t('General')?></a>
				</li>
				<li><a href="javascript:void(0)" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.options').show();"><?php  echo t('Options')?></a>
				</li>
				<li><a href="javascript:void(0)" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.attributes').show();"><?php   echo t('Additional Options')?></a>
				<li><a href="javascript:void(0)" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.files').show();"><?php  echo t('Files')?></a>
				</li>
			</ul>
			<div class="pane post">
	
				<div class="clearfix">
					<?php   echo $form->label('mediaTitle', t('Media Title'))?> *
					<div class="input">
						<?php   echo $form->text('mediaTitle', $mediaTitle, array('style' => 'width: 230px'))?>
					</div>
				</div>

				<div class="clearfix">
					<?php   echo $form->label('mediaDescription', t('Media Description'))?>
					<div class="input">
						<div><?php   echo $form->textarea('mediaDescription', $mediaDescription, array('style' => 'width: 98%; height: 90px; font-family: sans-serif;'))?></div>
					</div>
				</div>
			</div>
			<div class="pane options" style="display: none;">
				<div class="clearfix">
					<?php   echo $form->label('media_date', t('Media Date'))?>
					<div class="input">
						<?php   
						Loader::model("attribute/categories/collection");
						$akct = FileAttributeKey::getByHandle('media_date');
						if (is_object($media)) {
							$tcvalue = $media->getAttributeValueObject($akct);
						}
						?>
						<?php   echo $akct->render('form', $tcvalue, true);?>
					</div>
				</div>
				<div class="clearfix">
					<?php   echo $form->label('mediaCategory', t('Media Type'))?>
					<div class="input">
						<?php   
						Loader::model("attribute/categories/collection");
						$akct = FileAttributeKey::getByHandle('media_type');
						if (is_object($media)) {
							$tcvalue = $media->getAttributeValueObject($akct);
						}
						?>
						<?php   echo $akct->render('form', $tcvalue, true);?>
					</div>
				</div>
				<div class="clearfix">
					<?php   echo $form->label('media_artwork', t('Media Artwork'))?>
					<div class="input">
						<?php   
						Loader::model("attribute/categories/collection");
						$akct = FileAttributeKey::getByHandle('media_artwork');
						if (is_object($media)) {
							$tcvalue = $media->getAttributeValueObject($akct);
						}
						?>
						<?php   echo $akct->render('form', $tcvalue, true);?>
					</div>
				</div>
				<div class="clearfix">
					<?php   echo $form->label('media_set', t('File Set'))?>
					<?php  
					$fs = New FileSetList();
					$fsets = $fs->get();
					?>
					<div class="input">
						<?php  
						foreach($fsets as $set){
							if(in_array($set->getFileSetID(),$sets)){$selected = 'checked';}else{$selected=null;}
							//echo '<option '.$selected.' value="'.$set->getFileSetID().'">'.$set->getFileSetName().'</option>';
					
							echo '<input type="checkbox" name="file_set[]"'.$selected.' value="'.$set->getFileSetID().'"/>'.$set->getFileSetName().'<br/>';
						}	
						?>
					</div>
				</div>
			</div>
			<div class="pane attributes" style="display: none;">
			<?php    
				$set = AttributeSet::getByHandle('promedia_additional_attributes');
				$setAttribs = $set->getAttributeKeys();
				if($setAttribs){
					foreach ($setAttribs as $ak) {
						if(is_object($blog)) {
							$aValue = $blog->getAttributeValueObject($ak);
						}
						?>
						<div class="clearfix">
							<?php    echo $ak->render('label');?>
							<div class="input">
								<?php    echo $ak->render('form', $aValue)?>
							</div>
						</div>
						<?php  
					}
				}
			?>
			</div>
			<div class="pane files" style="display: none;">
				<div class="clearfix">
					<?php   echo $form->label('media_audio', t('Media Audio'))?>
					<div class="input">
						<?php   
						Loader::model("attribute/categories/collection");
						$akct = FileAttributeKey::getByHandle('media_audio');
						if (is_object($media)) {
							$tcvalue = $media->getAttributeValueObject($akct);
						}
						?>
						<?php   echo $akct->render('form', $tcvalue, true);?>
					</div>
				</div>
				<div class="clearfix">
					<?php   echo $form->label('media_video', t('Media Video'))?>
					<div class="input">
						<?php   
						Loader::model("attribute/categories/collection");
						$akct = FileAttributeKey::getByHandle('media_video');
						if (is_object($media)) {
							$tcvalue = $media->getAttributeValueObject($akct);
						}
						?>
						<?php   echo $akct->render('form', $tcvalue, true);?>
					</div>
				</div>
				<div class="clearfix">
					<?php   echo $form->label('media_download', t('Media Download'))?>
					<div class="input">
						<?php   
						Loader::model("attribute/categories/collection");
						$akct = FileAttributeKey::getByHandle('media_download');
						if (is_object($media)) {
							$tcvalue = $media->getAttributeValueObject($akct);
						}
						?>
						<?php   echo $akct->render('form', $tcvalue, true);?>
					</div>
				</div>
			</div>
	</div>
	<div class="ccm-pane-footer">
    	<?php   $ih = Loader::helper('concrete/interface'); ?>
        <?php   print $ih->submit(t($title.' Media Item'), 'media-form', 'right', 'primary'); ?>
        <?php   print $ih->button(t('Cancel'), $this->url('/dashboard/files/pro_media/list/'), 'left'); ?>
    </div>
	</form>
</div>