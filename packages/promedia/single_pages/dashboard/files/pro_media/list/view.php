<style type="text/css">
.icon {
display: block;
float: left;
height:20px;
width:20px;
background-image:url('<?php   echo ASSETS_URL_IMAGES?>/icons_sprite.png'); /*your location of the image may differ*/
}
.edit {background-position: -22px -2225px;margin-right: 6px!important;}
.copy {background-position: -22px -439px;margin-right: 6px!important;}
.delete {background-position: -22px -635px;}
	strong label {width: 120px!important; float: left; padding: 10px 0 0 40px!important;}
	.entry-form td {padding: 6px!important;}
	.allday_form{padding-top: 12px!important;}
</style>
<?php  echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProMedia Manager'), false, false, false);?>
	<div class="ccm-pane-body">
		<?php  
		if($remove_name){
		?>
		<div class="alert-message block-message error">
		  <a class="close" href="<?php   echo $this->action('clear_warning');?>">?</a>
		  <p><strong><?php   echo t('Holy guacamole! This is a warning!');?></strong></p><br/>
		  <p><?php   echo t('Are you sure you want to delete '.$remove_name.'?');?></p>
		  <p><?php   echo t('This action may not be undone!');?></p>
		  <div class="alert-actions">
		    <a class="btn small" href="<?php   echo $this->action('deletethis', $remove_fid, $remove_name)?>"><?php   echo t('Yes Remove This');?></a> <a class="btn small" href="<?php   echo $this->action('clear_warning');?>"><?php   echo t('Cancel');?></a>
		  </div>
		</div>
		<?php  
		}
		?>
		<form method="get" action="<?php   echo $this->action('view')?>">
		<?php   
		$sections[0] = '** All';
		asort($sections);
		?>
		<table class="ccm-results-list">
			<tr>
				<th><strong><?php   echo t('by Set')?></strong></th>
				<th><strong><?php   echo t('by Name')?></strong></th>
				<th><strong><?php   echo t('by Type')?></strong></th>
				<th></th>
			</tr>
			<tr>
				<td>
					<select name="set">
						<option value=''>--</option>
					<?php  
						foreach($fsets as $set){
							if($_GET['set']==$set->getFileSetID()){$selected = 'selected="selected"';}else{$selected=null;}
							echo '<option '.$selected.' value="'.$set->getFileSetID().'">'.$set->getFileSetName().'</option>';
						}	
					?>
					</select>
				</td>
				<td><?php   echo $form->text('like', $like)?></td>
				<td>
				<?php  
					$ak = FileAttributeKey::getByHandle('media_type');
					$akc = $ak->getController();
					$options = $akc->getOptions();
				?>
				<select name="types" style="width: 110px!important;">
					<option value=''>--</option>
				<?php  
					foreach($options as $option){
						if($_GET['types']==$option->value){$selected = 'selected="selected"';}else{$selected=null;}
						echo '<option '.$selected.' value="'.$option->value.'">'.$option->value.'</option>';
					}	
				?>
				</select>
				</td>
				<td>
				<?php   echo $form->submit('submit', 'Search')?>
				</td>
			</tr>
		</table>
		
		</form>
		<br/>
		<div style="float: right; margin-bottom: 12px;">
			<a href="<?php echo BASE_URL.DIR_REL?>/index.php/dashboard/files/pro_media/add/" class="btn primary"><?php  echo t('Add New Media')?></a>
		</div>
		<?php   
		$nh = Loader::helper('navigation');
		if ($fl->getTotal() > 0) { 
			$fl->displaySummary();
			?>
			
		<table border="0" class="ccm-results-list" cellspacing="0" cellpadding="0">
			<tr>
				<th>&nbsp;</th>
				<th class="<?php   echo $fl->getSearchResultsClass('fvName')?>"><a href="<?php   echo $fl->getSortByURL('fvName', 'asc')?>"><?php   echo t('Name')?></a></th>
				<th class="<?php   echo $fl->getSearchResultsClass('cvDatePublic')?>"><a href="<?php   echo $fl->getSortByURL('media_type', 'asc')?>"><?php   echo t('Date')?></a></th>
				<th class="<?php   echo $fl->getSearchResultsClass('media_type')?>"><a href="<?php   echo $fl->getSortByURL('media_category', 'asc')?>"><?php   echo t('Media Type')?></a></th>
			</tr>
			<?php   
			$pkt = Loader::helper('concrete/urls');
			$pkg= Package::getByHandle('promedia');
			foreach($files as $f) { 
			
				$media_type = $f->getAttribute('media_type');
				$media_date = $f->getAttribute('media_date');
			?>
			<tr>
				<td width="60px">
				<a href="<?php   echo $this->url('/dashboard/files/pro_media/add', 'edit', $f->getFileID())?>" class="icon edit"></a> &nbsp;
				<a href="<?php   echo $this->url('/dashboard/files/pro_media/list', 'delete_check', $f->getFileID(),$f->getFileName())?>" class="icon delete"></a>
				</td>
				<td><?php   echo $f->getFileName()?></td>
				<td>
				<?php   
				if ($media_date > date(DATE_APP_GENERIC_MDYT_FULL) ){
					echo '<font style="color:green;">';
					echo $media_date;
					echo '</font>';
				}else{
					echo date(DATE_APP_GENERIC_MDYT_FULL,strtotime($media_date));
				}
				?>
				</td>
				<td><?php   echo $media_type;?></td>
			</tr>
			<?php   } ?>
			</table>
			<br/>
			<?php   
			$fl->displayPaging();
		} else {
			print t('No media entries found.');
		}
		?>
	</div>
	<div class="ccm-pane-footer">

    </div>
 
</div>