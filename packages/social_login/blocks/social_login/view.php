<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

// Check for redirect if the user tried accessing a restricted page before logging in.
$a = strpos($_SERVER['PHP_SELF'], "forward/");
if($a !== false) {
	$a += 8;
	$rcID = substr($_SERVER['PHP_SELF'], $a);
	
	$a = strpos($rcID, "/");
	if($a !== false) {
		$rcID = substr($rcID, $a-1);
	}
}
//echo "RCID:".$rcID;

global $c;
$u = new User();

if($askemail) {
	$html = Loader::helper('html');
	echo $html->css('ccm.forms.css');
	/*
?>
	<div id="social_login_askemail_<?php  echo $controller->bID ?>" style="display: none">
		<form action="#" id="social_login_askemail_form" method="GET">
			<p>
			<?php   echo t('Email'); ?>
				<input style="float: right; margin-top: -3px;" type="text"
					name="reg_email" />
			</p>
			<p>
			<?php   echo t('Confirm Email'); ?>
				<input type="text" style="float: right; margin-top: -3px;"
					name="email_confirm" />
			</p>
			<br />
			<div style="display: none; float: left; color: red" id="email_message"></div>
			<input type="submit" class="ccm-button-v2 ccm-input-submit"
				id="submit" style="float: right" value="<?php   echo t('Send') ?>" /> <input
				type="hidden" name="provider"
				value="<?php   echo htmlentities($_GET['provider'],ENT_QUOTES)?>" />
		</form>
	</div>

	<script type="text/javascript">
	$(document).ready(function() {
		$('#social_login_askemail_<?php  echo $controller->bID ?>').submit(function(){
			if($('input[name="reg_email"]').val()!=''&&$('input[name="reg_email"]').val()==$('input[name="email_confirm"]').val())
				return true;
			else
			{	$('#email_message').html('<?php   echo t('Please check your email') ?>');
				$('#email_message').show();
			}
			return false;
		});
		$('#social_login_askemail_<?php  echo $controller->bID ?>').dialog({
			title: '<?php   echo t('Please insert information below'); ?>',
			element: '#social_login_askemail_<?php  echo $controller->bID ?>',
			width: 400,
			modal: false,
			height: 220
		});
	});
	</script>
<?php
	*/
}
if($emailpresent) {
	$html = Loader::helper('html');
	echo $html->css('ccm.forms.css');
?>
	<?php
		echo "<div class='alert alert-warning'>";
		echo $emailpresentmessage;
		echo "</div>";
	?>	

<?php
}
if(!$u->isLoggedIn()) {

?>
	<div class="socialLogin">
	<?php   
	foreach($active_providers as $provider) {
		echo '<a class="signin_'.strtolower($provider['providerName']).'" ';
		echo 'href="?provider='.$provider['providerID'];
		if(isset($_REQUEST['redirect'])) {
			if(ADMIN) {
				echo "&redirect=".$_REQUEST['redirect'];
			}
			else {
				$f = "redirect=";
				$p = strpos($f, $_SERVER['QUERY_STRING']);// + strlen($f);
				$redirect = substr($_SERVER['QUERY_STRING'], $p);
				$redirect = str_replace("provider", "last", $redirect);
				echo "&".$redirect;
			}
		}
		else
		if($rcID) {
			echo "&rcID=".$rcID;
		}
		echo '">';
		//echo t('Sign-in with').' '.$provider['providerName'];
		echo '</a><br>';
	}
	?>
	</div>
	<br class='clear'>
<?php 
}
else {
?>
	<div class="socialLogin">
	<?php   foreach($active_providers as $provider){ if(!SocialLoginModel::isAssociatedWith($provider['providerID'])){ //Associate links ?>
		<a href="?provider=<?php   echo $provider['providerID'] ?>"><?php   echo t('Associate with') ?>
		<?php   echo $provider['providerName'] ?> </a><br />
		<?php   }else{ //Disassociate links ?>
		<a
			href="?provider=<?php   echo $provider['providerID'] ?>&disassociate=1"><?php   echo t('Disassociate from') ?>
			<?php   echo $provider['providerName'] ?> </a><br />
			<?php   }} ?>
	</div>
	<br class='clear'><br>
	<div class="login">
		<a href="<?php   echo $this->action('logout') ?>"><?php   echo t('Logout')?></a>
	</div>
<?php
}
?>