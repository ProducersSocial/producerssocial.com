<?php   
defined("C5_EXECUTE") or die(_("Access Denied."));
/**
 * This is the "connection" to HybridAuth library
 */
Loader::library('hybridauth/index','social_login');