<?php
//echo $_SERVER['REMOTE_ADDR']."<br>";
Define("LOCATION", explode(".", $_SERVER['HTTP_HOST'])[0]);
Define("DEV", LOCATION == "dev");
Define("LOCALHOST", LOCATION == "localhost");
Define("ADMIN", LOCALHOST || $_SERVER['REMOTE_ADDR'] == "72.203.117.35");
//echo $_SERVER['REMOTE_ADDR'];

ini_set("log_errors", 1);
ini_set("error_log", "/home/dh_w4724d/producerssocial.net/logs/error.log");
ini_set("session.gc_maxlifetime", 76*60*60);

if(ADMIN) {
	//phpinfo();
	//die;
	//if($_REQUEST['REQUEST_URI'] == "/axongenesis") {
	//	$_REQUEST['REQUEST_URI'] = "/mebmers/profile/axongenesis";
	//	$_REQUEST['REDIRECT_URL'] = "/mebmers/profile/axongenesis";
	//}
	//print_r($_REQUEST);
	error_reporting(E_ALL);
}

$requirePassword = false;
if($requirePassword) {
	$set = false;
	$name = "devpass";
	$pass = "dev144";
	if(isset($_COOKIE[$name])) {
		$data = unserialize(stripslashes($_COOKIE[$name]));
		if($data['password'] == $pass) {
			Define("DEVPASS", true);
			$set = true;
		}
		else {
			setcookie($name, "", time() - 3600);
		}
	}
	else {
		if(isset($_POST['login'])) {
			if($_POST['password'] == $pass) {
				$data = serialize($_POST);
				$_COOKIE[$name] = $data;
				$exp = 24 * 60 * 60 * 7;
				setcookie($name, $data, time()+$exp);
				Define("DEVPASS", true);
				$set = true;
			}
		}
	}
	if(!$set) {
		Define("DEVPASS", false);
	}
}

require('concrete/dispatcher.php');
