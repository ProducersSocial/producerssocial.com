<?php
defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//:: ajax – util to streamline ajax calls
//-----------------------------------------------------------------------------------------------------------------------------
function ajax($area, $cmd, $value=1, $confirm=null) {
	if($confirm) {
		return "href='javascript:;' onclick='if(".$confirm.") return ps.ondeck.".$area."(\"".$cmd."\", \"".$value."\", \"\");'";
	}
	else {
		return "href='javascript:;' onclick='return ps.ondeck.".$area."(\"".$cmd."\", \"".$value."\", \"\");'";
	}
}


//-----------------------------------------------------------------------------------------------------------------------------
//:: ajax – util to streamline ajax calls
//-----------------------------------------------------------------------------------------------------------------------------
function reloadPage($area, $cmd, $value=1, $confirm=null) {
	if($confirm) {
		return "href='javascript:;' onclick='if(".$confirm.") return ps.ondeck.reloadPage(\"".$area."\", \"".$cmd."\", \"".$value."\", this);'";
	}
	else {
		return "href='javascript:;' onclick='return ps.ondeck.reloadPage(\"".$area."\", \"".$cmd."\", \"".$value."\", this);'";
	}
}

	
//-----------------------------------------------------------------------------------------------------------------------------
//: AjaxHelper
//-----------------------------------------------------------------------------------------------------------------------------
class AjaxHelper {
}

?>