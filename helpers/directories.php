<?php
defined('C5_EXECUTE') or die("Access Denied.");

class DirectoriesHelper 
{
	public function getListing($path) 
	{
		$dirArray = array();
		if(file_exists($path)) {
			if($handle = opendir($path)) {
				while(false !== ($file = readdir($handle))) {
					if($file && $file != "." && $file != "..") {
						if(is_dir($path.$file)) {
							array_push($dirArray, $file);
						}
					}
				}
				closedir($handle);
			}
			sort($dirArray);
		}
		return $dirArray;
	}
	
	public static function getFileListing($path, $matchExtensions=null) 
	{
		$dirArray = array();
		$extensions = array();
		if($matchExtensions) {
			$extensions = explode(" ", $matchExtensions);
		}
		if(file_exists($path)) {
			$dirArray = array();
			if($handle = opendir($path)) {
				while(false !== ($file = readdir($handle))) {
					if($file && $file != "." && $file != "..") {
						if($matchExtensions) {
							foreach($extensions as $ext) {
								if(strpos($file, $ext)) {
									array_push($dirArray, $file);
									break;
								}
							}
						}
						else array_push($dirArray, $file);
					}
				}
				closedir($handle);
			}
			sort($dirArray);
		}
		return $dirArray;
	}
	
	public static function getFileListingRecursive($path, $matchExtensions=null) 
	{
		$dirArray = array();
		$extensions = array();
		if($matchExtensions) {
			$extensions = explode(" ", $matchExtensions);
		}
		if(file_exists($path)) {
			$dirArray = array();
			if($handle = opendir($path)) {
				while(false !== ($file = readdir($handle))) {
					if($file && $file != "." && $file != "..") {
						$filepath = $path."/".$file;
						if(is_dir($filepath)) {
							$dirArray[basename($file)] = DirectoryUtils::GetFileListingRecursive($filepath, $matchExtensions);
						}
						else
						if($matchExtensions) {
							foreach($extensions as $ext) {
								if(strpos($file, $ext)) {
									$dirArray[basename($file)] = $filepath;
									break;
								}
							}
						}
						else {
							$dirArray[basename($file)] = $filepath;
						}
					}
				}
				closedir($handle);
			}
			asort($dirArray);
		}
		return $dirArray;
	}

	public static function create($dir) 
	{
		if(!file_exists($dir)) {
			if(!mkdir($dir, 0777)) {
				return false;
			}
		}
		return true;
	}

	public static function formattedFileSize($file, $decimals=0) 
	{
		$bytes = filesize($dir.$file);
		$sz = 'BKMGTP';
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
	}
}