<?php
defined('C5_EXECUTE') or die("Access Denied.");

class CookieHelper {

	public static function Save($name, $data, $exp=86400, $useJavaScript=false) 
	{
		if($name) {
			// To ensure that the data is preserved, we must serialize it
			$data = serialize($data);

			$_COOKIE[$name] = $data;

			if($useJavaScript) {
				if($_REQUEST['AJ'] || $GLOBALS['PAGE_LOADED']) {
				//	ExecuteJavascript("createCookie('".$name."', '".$data."', '".$exp."')");
				}
				else {
				//	AddJavascript("createCookie('".$name."', '".$data."', '".$exp."')");
				}
			}
			else {
				if(!setcookie($name, $data, time()+$exp, '/'.BASE_URL)) {
					echo "setcookie(".$name.") FAILED";
				}
				else {
					echo "setcookie(".$name.") SUCCESS";
				}
			}
		}
	}

	public static function HasValue($name) 
	{
		return isset($_COOKIE[$name]);
	}

	public static function Load($name) 
	{
		$data = null;
		$data = unserialize(stripslashes($_COOKIE[$name]));
		return $data;
	}

	public static function Clear($name, $useJavaScript=false) 
	{
		if($useJavaScript) {
			ExecuteJavascript("eraseCookie('".$name."')");
		}
		else {
			setcookie($name, $data, time() - 3600);
		}

		unset($_COOKIE[$name]);
	}
}

?>