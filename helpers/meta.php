<?php
defined('C5_EXECUTE') or die("Access Denied.");

class MetaHelper 
{
	static public $data = null;
	
	public function __construct() {
		MetaHelper::$data = array();
		MetaHelper::$data['image'] 			= "http://producerssocial.com/images/producerssocial.jpg";
		MetaHelper::$data['url'] 			= "http://producerssocial.com";
		MetaHelper::$data['title'] 			= "A space for producers, musicians, and vocalists to meet, learn, and share";
		MetaHelper::$data['description'] 	= "The Producers Social meets every month, bringing music producers together to share their latest work and to get constructive feedback. Whether you are a producer, musician, vocalist, all are welcome!";
	}
	
	public function __set($name, $value) {
		MetaHelper::$data[$name] = $value;
	}
	
	public function __get($name) {
		return MetaHelper::$data[$name];
	}
}