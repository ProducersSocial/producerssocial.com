<?php
defined('C5_EXECUTE') or die("Access Denied.");

class FeatureHelper 
{
	public function getItemURL($item) {
		$path = "members";
		if($item->type == "blog") {
			$path = "blog";
		}
		else {
			$path .= "/".$item->type;	
		}
		$url = "/".$path."/".$item->getHandle();
		return $url;
	}
	
	public function truncateName($name, $maxLength=20) 
	{
		$parts = explode(" ", $name);
		$new = $parts[0];
		if(count($parts) > 1) {
			$i = 1;
			while(strlen($new) + strlen($parts[$i]) <= $maxLength) {
				$i++;
				$new .= " ".$parts[$i];
				if($i > count($parts)) break;
			}
		}
		if(strlen($new) > $maxLength) {
			$new = substr($new, 0, $maxLength-1)."&#8230";
		}
		return $new;
	}
	
	public function displayEmailPreview($feature_id)
	{
		Loader::model("member_feature");
		$item = MemberFeature::getByID($feature_id);	
		
		$out = "";

		if(!$item) {
			$out .= "Failed loading the feature:".$feature_id;	
		}
		else {
			$out .= "<div style='text-align:left; font-size:18px;'>";
			
			$url = BASE_URL;
			if($item->type == "blog") {
				$url .= "/blog/".$item->handle;	
			}
			else {
				$url .= "/members/".$item->type."/".$item->handle;	
			}
			$out .= "<a href='".$url."'>";
			$file = "_feature.jpg";
			$out .= "<img style='max-width:100%;' src='".BASE_URL."/images/features/".$item->id.$file."' alt='".$item->summary."'>";
			$out .= "</a>";
			
			$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px; margin-bottom:20px;";
			$out .= "<br style='clear:both;'>";
			//$out .= "<div class='slide_title'>".$item->name."</div>";
			$out .= "<div style='margin-top:20px; margin-bottom:20px;'>".$item->summary."</div>";
			$out .= "<div style='text-align:center;'>";
			$out .= "<a href='".$url."' style='".$buttonStyle."'>Read More...</a>";
			$out .= "</div>";
			$out .= "</div>";
			$out .= "</div>";
		}
		return $out;
	}
	
	public function displaySlides($items, $title=null, $path="members", $hideAfter=3) 
	{
		$out = "";
		$out .= "<div class='item_page_navigation'>";
		if($title) {
			$out .= "<div class='item_list_title'>".$title."</div>";
		}
		$out .= "<a class='button white small right' href='/".$path."?list=true'><i class='fa fa-list-alt' aria-hidden='true'></i> View List</a>";
		
		$user = Loader::helper('user');
		if($user->isSuperAdmin()) {
			$out .= "<a href='/manage/features/edit/' class='button white small right pad-right'><i class='fa fa-file-o' aria-hidden='true'></i> New</a>";
		}
		$out .= "</div>";
		
		$section = str_replace("/", "_", $path);
		$out .= "<div id='".$section."_slides' class='slides_container'>";
		$out .= "<div class='slides_container_inner'>";
		
		$i = 1;
		$hidden = false;
		foreach($items as $item) {
			$out .= $this->displaySlide($item, $hidden, $path);
			if($i >= $hideAfter) {
				$hidden = true;
			}
			else {
				$out .= "<hr class='large'>";	
			}
			$i++;
			if($i == 3) break;
		}
		
		$items2 = array();
		for($i = $i-1; $i < count($items); $i++) {
			$items2[] = $items[$i];	
		}
		$out .= $this->displayGrid($items2, null, null, "col-md-4 col-sm-6 col-xs-12", true, false, true);
		
		$out .= "</div>";
		$out .= "</div>";
		return $out;
	}
	
	public function displaySlide($item, $hidden=false) 
	{
		$style = "";
		if($hidden) $style = "style='display:none;'";
		
		$class = "";
		if(!$item->isPublished()) {
			$class .= "unpublished";
		}
		
		$user = Loader::helper('user');
		$url = $this->getItemURL($item);
		
		$out = "";
		$out .= "<div class='slide ".$class."' id='slide_".$item->id."' ".$style.">";
		$out .= $this->displayImageAndStats($item, $url, false);
		$out .= "<div class='slide_content'>";
		//$out .= "<div class='slide_title'>".$item->name."</div>";
		$out .= "<div class='slide_summary'>".$item->summary."</div>";
		$out .= "<a class='slide_read_more button white small' href='".$url."'>More <i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>";
		$out .= "</div>";
		$out .= "</div>";
		
		return $out;
	}

	public function displayItemPage($item, $url=null, $showNavUp=true, $path="members") 
	{
		$out = "";
		$style = null;
		if(!$item) return null;
		
		$item->incrementViewCount();
		
		$user = Loader::helper('user');
		Loader::model("user_like");
		
		$out .= "<div class='item_page_navigation'>";
		if($showNavUp) {
			$out .= "<a class='button white small left' ";
			$out .= "href='/".$path."'><i class='fa fa-angle-double-up' aria-hidden='true'></i> ";
			$out .= "<span class='hidden-sm-down'>View All ".$item->getTypeName(true)."</span></a>";
		}
		if($user->isSuperAdmin() || $user->isLocationAdmin($item->location)) {
			$out .= "<a href='/manage/features/edit/".$item->id."' class='button white small right'><i class='fa fa-pencil' aria-hidden='true'></i> <span class='hidden-sm-down'>Edit</span></a>";
			if($user->isSuperAdmin()) {
				$out .= "<a href='/manage/features/edit/' class='button white small right pad-right'><i class='fa fa-file-o' aria-hidden='true'></i> <span class='hidden-sm-down'>New</span></a>";
			}
		}
		$out .= "</div>";
		
		$url = $this->getItemURL($item);
		
		$out .= "<div class='item_page'>";
		$out .= "<div class='item_page_header'>";
		$out .= $this->displayImageAndStats($item, $url, false, false);
		$out .= "</div>";
		
		if(!$item->isPublished()) {
			$out .= "<div class='clear admin_area'>Scheduled for publication on <b>".$item->getPublishedDate(true)."</b></div>";
		}
		
		$out .= "<div class='item_page_content'>";
		$out .= UserLike::likeButton($item->id);
		$out .= "<div class='item_page_title'>".$item->name."</div>";
		$out .= "<div class='item_page_description'>";
		$out .= $item->getContent($user->isMember());
		
		if($item->id == 116) {
		// SPECIAL EXCEPTION FOR WORKSHOP SERIES EVENT
			$stripe = Loader::helper('stripe');
			$out .= "<div class='alert alert-white'>";
			$out .= "<h1>Purchase Tickets</h1>";
			$out .= $stripe->purchaseButton("workshop1_weekend", "Weekend + Social $200 ($265 value)", 20000, 1, "", "", 'button large');
			$out .= "<br class='clear'>";
			$out .= $stripe->purchaseButton("workshop1_sat", "Saturday Only $125", 12500, 1, "", "", 'button large white left halfwidth');
			$out .= $stripe->purchaseButton("workshop1_sun", "Sunday Only $125", 12500, 1, "", "", 'button large white right halfwidth');
			$out .= "<br class='clear'>";
			$out .= "</div>";
		}
		$out .= "</div>";
		
		if($item->type == "submissions") {
			Loader::model("user_submission");
			
			if(!$user->isLoggedIn()) {
				if($item->isExpired()) {
					$out .= "<div class='clear alert alert-infolight'>";
					$out .= "Submissions are now closed.";
					$out .= "</div>";
				}
				else {
					$out .= "<a class='button clear' href='/login?redirect=/members/submissions/".$item->getHandle()."'>Please Log In to Enter</a>";
				}
			}
			else {
				$sub = null;
				$edit = null;
				if(isset($_REQUEST['edit']) && $_REQUEST['edit']) {
					$edit = $_REQUEST['edit'];
					$sub = UserSubmission::getByID($edit);
					if($sub->user_id != $user->id && !$user->isAdmin()) {
						$sub = null;
						$edit = null;
					}
				}
			
				if(isset($_REQUEST['error'])) {
					$out .= "<div class='clear alert alert-warning'>";
					$out .= "<h3>Your submission is incomplete</h3>";
					$out .= $_REQUEST['error'];
					$out .= "</div>";
				}
				else
				if(isset($_REQUEST['success'])) {
					$out .= "<div class='clear alert alert-info fadeout'>";
					$out .= "<h3>Thank you for your submission!</h3>";
					$out .= "</div>";
				}
				if(isset($_REQUEST['message'])) {
					$out .= "<div class='clear alert alert-info fadeout'>";
					$out .= $_REQUEST['message'];
					$out .= "</div>";
				}
				
				$out .= "<a id='featuretop' name='featuretop'></a>";
				
				if(!$edit) {
					$userSubs = UserSubmission::getAllByUserID($user->id, "submission_id=".$item->id);
					$userSubCount = count($userSubs);
					if($userSubs && $userSubCount > 0) {
						$out .= "<br class='clear'>";
						$out .= "<div class='clear alert alert-info'>";
						$out .= "<h4>Your Submissions</h4>";
						foreach($userSubs as $sub) {
							$out .= "<div class='clear alert alert-white'>";
							//if($user->isSuperAdmin()) {
							//	$out .= "ID: <b>".$sub->id."</b><br class='clear'>";
							//}
							$out .= "Title: <b>".$sub->title."</b><br class='clear'>";
							$out .= "Artist: <b>".$sub->artist."</b><br class='clear'>";
							$out .= "Real Name: <b>".$sub->realname."</b><br class='clear'>";
							$out .= "Genre: <b>".$sub->genre."</b><br class='clear'>";
							$out .= "Email: <b>".$sub->email."</b><br class='clear'>";
							$out .= "File: <b><a target='_blank' href='".BASE_URL."/files/submissions/".$sub->submission_id."/".$sub->filename."'>".$sub->filename."</a></b><br class='clear'>";
							if($sub->note) {
								$out .= "Note: <i>".$sub->note."</i><br class='clear'>";
							}
							
							//$out .= "<div class='small'>If you wish to make changes to your submission, please delete this submissions and resubmit.</div>";
							
							if(!$item->isExpired()) {
								$out .= "<br class='clear'>";
								$out .= "<a class='button small white left' href='/members/submissions/edit/".$sub->id."'>Edit This Submission</a>";
								$out .= "<a class='button small white left' href='/members/submissions/delete/".$sub->id."' onclick='return ps.confirm(\"Are you sure you want to delete your submission? This is undoable, but you can submit again.\");'>Delete</a>";
							}
							else {
								$out .= "<div class='small'>The deadline has ended for this submission, so no changes or new submissions can be made.</div>";	
							}
							
							$out .= "</div>";
						}
						$out .= "</div>";
					}
				}
				
				if($item->submit_limit < 1) $item->submit_limit = 1;
				if($item->isExpired()) {
					$out .= "<div class='clear alert alert-infolight'>";
					$out .= "Submissions are now closed.";
					$out .= "</div>";
				}
				else
				if(!$edit && $item->submit_limit <= $userSubCount) {
					$out .= "<strong><i>Any mistakes or omissions cannot be changed after the deadline.</i></strong>";
					$out .= "<div class='small light'>You have reached the limit allowed for submissions. If you wish to submit a different track, you may delete one of your submissions above and resubmit.</div>"; 
				}
				else {
					if(!$edit) {
						$sub = new UserSubmission();
						$sub->artist = $user->profile->uArtistName;
						$sub->email = $user->getEmail();
					}
					
					$out .= "<br class='clear'>";					
					$form = Loader::helper("form");
					
					$out .= "<div class='clear alert alert-infolight'>";
					
					
					$out .= "<div class='ccm-form clear'>";
					$out .= "<form id='submission-form' class='ccm-ui' action='/members/submissions/submit' method='post' enctype='multipart/form-data'>";
					$out .= "<fieldset>";
					
					$out .= $form->hidden('id', $sub->id);
					$out .= $form->hidden('submission_id', $item->id);
					
					if($userSubCount > 0) {
						$out .= "<h5>Submit Another Track</h5>";	
					}
					else {
						$out .= "<h5>Submit Your Track</h5>";	
					}
					$out .= "<strong>Please complete the information below with accurate spelling and capitalization.<br><i>Any mistakes or omissions cannot be changed after the deadline.</i></strong>";
				
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_title', t('Track Title'));
						$out .= "<p class='small light'>Enter the name of the track <i>exactly</i> as you want it to appear</p>";
						$out .= $form->text('submit_title', $sub->title);
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_artist', t('Artist Name(s)'));
						$out .= "<p class='small light'>Enter your artist name <i>exactly</i> as you want it to appear</p>";
						$out .= $form->text('submit_artist', $sub->artist);
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_artist', t('Real Name(s)'));
						$out .= "<p class='small light'>Enter your real name <i>exactly</i> as you want it to appear</p>";
						$out .= $form->text('submit_realname', $sub->artist);
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_genre', t('Genre'));
						$out .= "<p class='small light'>Enter up to 3 genres separated by comma. Ex. \"trap, future bass\"</p>";
						$out .= $form->text('submit_genre', $sub->genre);
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_email', t('Contact Email(s)'));
						$out .= "<p class='small light'>Enter at least 1 email address for contact. Please separate additional emails with a comma.</p>";
						$out .= $form->text('submit_email', $sub->email);
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_file', t('24bit Unmastered Wav'));
						$out .= "<p class='small light'>Please upload a <b><i>24bit unmastered wav file</i></b>. Maximum file size: 100MB</p>";
						$out .= $form->file('submit_file');
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_note', t('Notes'));
						$out .= "<p class='small light'>Optional. Add any special notes about your tune.</p>";
						$out .= $form->textarea('submit_note', $sub->note);
					$out .= "</div>";
					
					$out .= "<div class='ccm-profile-attribute'>";
						$out .= $form->label('submit_agree', t('Agreement'));
						$out .= "<p class='small light'>";
						//$out .= "<a href='javascript:;' onclick=''>View Agreement</a>";
						$out .= "Your submission is subject to review and there is no guarantee of inclusion. By submitting your track you are granting the Producers Social Record Label authorization to distribute and/or sell your track as part of the product being offered. By accepting this agreement you are also verifying that the music you submit is 100% your intellectual property and does not violate any third-party copyrights or prior agreements. No artist royalties are being offered at this time and all proceeds go to the continued growth of the Producers Social community and services offered to members. We appreciate your support and contributions to make this possible!";
						$out .= "</p>";
						$out .= "<b class='left'>I Accept</b>".$form->checkbox('submit_agree', 1);
					$out .= "</div>";
					
					$out .= "<div class='manage_list_controls'>";
					$out .= "<div id='submit_form_message'></div>";
					$out .= "<a class='button' href='javascript:;' onclick='return ps.submissions.validate();'>Submit</a>";
					//$out .= "<input type='submit' class='button center' id='submit' name='submit' value='Submit'>";
					$out .= "</div>";
				
					$out .= "</fieldset>";
					$out .= "</form>";
					
					$out .= "<div id='submit_progress' style='display:none'>";
						$out .= "<div id='submit_progress_status'></div>";
						$out .= "<div id='submit_progress_bar'>";
						$out .= "<a id='submit_progress_cancel' href='javscript:;' onclick='return ps.submissions.cancelUpload(\"submission-form\");'><i class='fa fa-times' aria-hidden='true'></i></a>";
						$out .= "<div id='submit_progress_bar_inner'>";
						$out .= "<div id='submit_progress_percent'>0%</div>";
						$out .= "</div>";
						$out .= "</div>";
						$out .= "<br class='clear'>";
					$out .= "</div>";
					
					$out .= "</div>";
					$out .= "</div>";
				}
				
				if($user->isLocationAdmin($item->location)) {
					$subs = UserSubmission::getAll("submission_id=".$item->id);
					$total = count($subs);
					$out .= "<div class='admin_area' style='text-align:left;'>";
					$out .= "<h3>".($total == 0 ? "No" : $total)." Submission".($total == 1 ? "" : "s")."</h3>";
					if($subs && $total) {
						$user->getLocations();
						foreach($subs as $sub) {
							$p = UserProfile::getByUserID($sub->user_id);
							$out .= "<div class='alert alert-white'>";
							$out .= "<div class='right'>";
							$out .= "Submitted By: ";
							$out .= "<a href='".$p->getURL()."'>".$p->uArtistName."</a>";
							$out .= "&nbsp;<span style='line-height:1; padding:0; margin:0; width:20px; height:18px; color:white; font-weight:bold; font-size:12px; background:#".$user->locations[$p->uLocation]->Color."'>".$p->uLocation."</span>";
							$out .= "</div>";
							$out .= "Title: <b>".$sub->title."</b><br class='clear'>";
							$out .= "Artist: <b>".$sub->artist."</b><br class='clear'>";
							$out .= "Real Name: <b>".$sub->realname."</b><br class='clear'>";
							$out .= "Genre: <b>".$sub->genre."</b><br class='clear'>";
							$out .= "Email: <b>".$sub->email."</b><br class='clear'>";
							$out .= "File: <b><a target='_blank' href='".BASE_URL."/files/submissions/".$sub->submission_id."/".$sub->filename."'>".$sub->filename."</a></b><br class='clear'>";
							if($sub->note) {
								$out .= "Note: <i>".$sub->note."</i><br class='clear'>";
							}
							//$out .= "<br class='clear'>";
							
							if(!$item->isExpired()) {
								$out .= "<br class='clear'>";
								$out .= "<a class='button small white left' href='/members/submissions/edit/".$sub->id."'>Edit</a>";
								$out .= "<a class='button small white left' href='/members/submissions/delete/".$sub->id."' onclick='return ps.confirm(\"Are you sure you want to delete your submission? This is undoable, but you can submit again.\");'>Delete</a>";
							}
							else {
								$out .= "<div class='small'>The deadline has ended for this submission, so no changes or new submissions can be made.</div>";	
							}
							$out .= "</div>";
						}
					}
					else {
						$out .= "<div class='alert alert-white'>";
						$out .= "No submissions have been received yet.";
						$out .= "</div>";
					}
					$out .= "</div>";
				}
			}
		}
		
		if($user->isMember()) {
			//if($item->membersonly || $item->link) {
				$out .= "<div class='item_page_membersonly'>";
				$out .= "<div class='item_page_membersonly_title'>Members Only</div>";
				
				if($item->isExpired()) {
					if($item->membersonly) {
						$out .= "<div class='alert alert-gray pad-top'>";
						$out .= "<h2 class='center'>This content is expired as of ".$item->getExpirationDate()."</h2>";
						$out .= "</div>";
					}
				}
				else {
					if($item->hasExpiration()) { 
						$exp = strtotime($item->expiration);
						if($exp < time() + (24 * 60 * 60 * 7)) {
							$out .= "<div class='alert alert-info pad-top'>";
							$out .= "<h3 class='center'>This item will expire on ".$item->getExpirationDate()."</h3>";
							$out .= "</div>";
						}
					}
					$out .= $item->membersonly;
	
					if($item->link) {                              
						$isDownload = false;
						if(strpos($item->link, ".zip") !== false) {
							$isDownload = true;
						}
						if($isDownload) {
							$out .= "<a class='item_page_download button' href='".$item->link."'><i class='fa fa-download' aria-hidden='true'></i> Download</a>";
						}
						else {
							$out .= "<div class='item_page_download'><a target='_blank' href='".$item->link."'>Link: ".$item->link." <i class='fa fa-external-link' aria-hidden='true'></i></a></div>";
						}
					}
	
					$directories = Loader::helper('directories');
					$dir = DIR_BASE.'/files/features/'.$item->filekey."/";
					//echo $dir;
					$files = $directories->getFileListing($dir);
					if($files) {
						$out .= "<div class='item_page_files'>";
						$out .= "<div class='item_page_files_title'>DOWNLOADS</div>";
						$i = 1;
						$alt = null;
						foreach($files as $file) {
							$size = $directories->formattedFileSize($dir.$file);
							$rowid = "item_".$item->id."_".$i;
							$out .= "<div class='item_page_file_row ".$alt."' id='".$rowid."'>";
							if($user->isSuperAdmin()) {
								$out .= "<a class='right' href='javascript:;' onclick='return ps.members.deleteFile(".$item->id.", \"".$file."\", \"".$rowid."\");'><i class='fa fa-trash' aria-hidden='true'></i></a>";
							}
							$download = "/members/download/".$item->id."?file=".$file;
							//$download = "/files/features/".$item->filekey."/".$file;
							$out .= "<a class='left' href='".$download."'><i class='fa fa-download' aria-hidden='true'></i> ".$file." (".$size.")</a>";
							$out .= "</div>";
							$i++;
							$alt = $alt ? null : "alt";
						}
						$out .= "</div>";
					}
				}
				$out .= "</div>";
			//}
		}
		else {
			$out .= "<br class='clear'>";
			$out .= $item->nonmembersonly;
			$out .= "<br><div class='clear alert alert-info'><a href='/membership'><i class='fa fa-id-card' aria-hidden='true'></i> <strong>Become a Premium Member today for full access!</strong></a></div>";
		}
		
		if($user->isSuperAdmin() || $user->isLocationAdmin($item->location)) {
			if($item->notes || $item->ondeck_id) {
				$out .= "<div class='admin_area'>";
				if($item->ondeck_id) {
					//Loader::model("on_deck");
					//$ondeck = OnDeck::getByEventID($item->ondeck_id);
					$out .= "<a class='button white medium' href='/manage/features/edit/".$item->id."?ondeck=".$item->ondeck_id."&rebuild=true'>Admin Rebuild Post</a>";
					$out .= "<br class='clear'>";
				}
				$out .= $item->notes;
				$out .= "</div>";
			}
			
			
			$out .= "<div class='admin_area'>";
			$out .= "Upload files for member-only access.<br><br>";
			$out .= "<form id='item_upload_form' action='/ajax' method='post' enctype='multipart/form-data'>";
			$out .= "<input type='submit' class='button medium white right' value='Upload Files'>";
			$out .= "<div class='left'><input type='file' class='button white medium' name='item_upload_file[]' id='item_upload_file' multiple></div>";
			$out .= "<input type='hidden' name='item' id='item' value='".$item->id."'>";
			$out .= "<input type='hidden' name='item_upload' id='item_upload' value='1'>";
			$out .= "</form>";
			
			$out .= "<div id='upload_progress' style='display:none'>";
				$out .= "<div id='upload_progress_bar'>";
				$out .= "<a id='upload_progress_cancel' href='javscript:;' onclick='return ps.members.cancelUpload(\"item_upload_form\");'><i class='fa fa-times' aria-hidden='true'></i></a>";
				$out .= "<div id='upload_progress_bar_inner'>";
				$out .= "<div id='upload_progress_percent'>0%</div>";
				$out .= "</div>";
				$out .= "</div>";
			$out .= "</div>";
			/*
			$out .= "<div class='progress'>";
				$out .= "<div id='upload_progress_bar'></div>";
				$out .= "<div id='upload_progress_percent'>0%</div>";
			$out .= "</div>";
			*/
			
			$out .= "<div id='upload_progress_status'></div>";
			$out .= "</div>";
		}
		
		$out .= "</div>";
		
		$out .= "<div class='item_page_footer'>";
		$out .= "<div class='col-sm-4'>";
		
		$next = $item->getNext();
		$prev = $item->getNext();
		if($prev && ($next == null || $prev->id != $next->id)) {
			$out .= "<a href='/".$path."/".$prev->id."' class='button white small left'><i class='fa fa-arrow-circle-left' aria-hidden='true'></i> <span class='hidden-sm-down'>".$this->truncateName($prev->name)."</span></a>";
		}
		$out .= "</div>";
		$out .= "<div class='col-sm-4'>";
		if($item->hasPublishedDate()) {
			$out .= "<div class='item_page_date'>Published on ".$item->getPublishedDate()."</div>";
		}
		if($user->isAdmin()) {
			$out .= "<div class='item_page_date'>ID: ".$item->id."</div>";
			$out .= "<div class='item_page_date'>Stats A:".$item->tracking_a." B:".$item->tracking_b." C:".$item->tracking_c." T:".$item->views."</div>";
		}
		$out .= "</div>";
		$out .= "<div class='col-sm-4'>";
		if($next) {
			$out .= "<a href='/".$path."/".$next->id."' class='button white small right'><span class='hidden-sm-down'>".$this->truncateName($next->name)."</span> <i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>";
		}
		$out .= "</div>";
		$out .= "</div>";
		
		return $out;
	}
	
	public function displaySummaryList($items, $title=null, $path="members") 
	{
		$out = "";
		$style = null;
		
		$user = Loader::helper('user');
		Loader::model("user_like");
		
		$out .= "<div class='item_page_navigation'>";
		if($title) {
			$out .= "<div class='item_list_title'>".$title."</div>";
		}
		$out .= "<a class='button white small right' href='/".$path."'><i class='fa fa-newspaper-o' aria-hidden='true'></i> <span class='hidden-sm-down'>Show Featured</span></a>";
		if($user->isSuperAdmin()) {
			$out .= "<a href='/manage/features/edit/' class='button white small right pad-right'><i class='fa fa-file-o' aria-hidden='true'></i> <span class='hidden-sm-down'>New</span></a>";
		}
		$out .= "</div>";
		
		$alt = "";
		$out .= "<div class='item_list'>";
		foreach($items as $item) {
			$url = $this->getItemURL($item);
			
			$out .= "<div class='item_list_row ".$alt."'>";
			//$out .= "<tr>";
			
			$out .= "<div class='item_list_row_image'>";
			$out .= $this->displayImageAndStats($item, $url, true, true);
			$out .= "</div>";
			
			$out .= "<div class='item_list_row_info'>";
			$out .= "<a class='item_list_row_title' href='".$url."'>".$item->name."</a>";
			$out .= "<div class='item_list_row_buttons'>";
			$out .= "<a href='".$url."' class='button white small right pad-right'><i class='fa fa-arrow-circle-right' aria-hidden='true'></i> <span class='hidden-sm-down'>View</span></a>";
			if($user->isSuperAdmin() || $user->isLocationAdmin($item->location)) {
				$out .= "<a href='/manage/features/edit/".$item->id."' class='button white small right pad-right'><i class='fa fa-pencil' aria-hidden='true'></i> <span class='hidden-sm-down'>Edit</span></a>";
			}
			$out .= "</div>";
			$out .= "<div class='item_list_row_summary'>".$item->summary."</div>";
			$out .= "</div>";
			
			//$out .= "</tr>";
			$out .= "</div>";
			
			$alt = $alt ? "" : "alt";
		}
		
		return $out;
	}
	
	public function displayGrid($items, $title=null, $path="members", $class="col-md-4", $squareImage=true, $showNav=true, $stackTitle=true) 
	{
		$out = "";
		$style = null;
		
		$user = Loader::helper('user');
		Loader::model("user_like");
		
		if($showNav) {
			$out .= "<div class='item_page_navigation'>";
			if($title) {
				$out .= "<div class='item_list_title'>".$title."</div>";
			}
			$out .= "<a class='button white small right' href='/".$path."'><i class='fa fa-newspaper-o' aria-hidden='true'></i> <span class='hidden-sm-down'>Show Featured</span></a>";
			if($user->isSuperAdmin()) {
				$out .= "<a href='/manage/features/edit/' class='button white small right pad-right'><i class='fa fa-file-o' aria-hidden='true'></i> <span class='hidden-sm-down'>New</span></a>";
			}
			$out .= "</div>";
		}
		
		$alt = "";
		$out .= "<div class='item_grid'>";
		foreach($items as $item) {
			$url = $this->getItemURL($item);
			
			$out .= "<div class='".$class." ".$alt."'>";
			$out .= "<div class='item_grid_row'>";
			
				$out .= "<div class='item_grid_row_image'>";
				$out .= $this->displayImageAndStats($item, $url, $squareImage);
				$out .= "</div>";
			
				$out .= "<div class='item_grid_row_info'>";
					$out .= "<a class='item_grid_row_title' href='".$url."'>";
					if($stackTitle) {
						if(strlen($item->name) >= 24) {
							$parts = explode(" ", $item->name);
							/*
							if(count($parts) > 3) {
								$out .= $parts[0]." ".$parts[1]." ".$parts[2]."<br>";
								for($i = 3; $i < count($parts); $i++) {
									$out .= $parts[$i]." ";	
								}
							}
							else {
							*/
								$out .= $parts[0]." ".$parts[1]."<br>";
								for($i = 2; $i < count($parts); $i++) {
									$out .= $parts[$i]." ";	
								}
							//}
						}
						else {
							$out .= $item->name;
						}
					}
					else {
						$out .= $item->name;
					}
					$out .= "</a>";
					$out .= "<div class='item_grid_row_summary'>".$item->summary."</div>";
					$out .= "<div class='item_grid_row_buttons'>";
						$out .= "<div class='item_grid_date'>".$item->getPublishedDate()."</div>";
						$out .= "<a href='".$url."' class='button white small right'><i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>";
						if($user->isSuperAdmin() || $user->isLocationAdmin($item->location)) {
							$out .= "<a href='/manage/features/edit/".$item->id."' class='button white small right pad-right'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
						}
					$out .= "</div>";
				$out .= "</div>";
			
			$out .= "</div>";
			$out .= "</div>";
			//$alt = $alt ? "" : "alt";
		}
		$out .= "</div>";
		
		return $out;
	}
	
	public function displayImageAndStats($item, $url=null, $squareImage=false, $showLike=true) {
		$class = $item->hasExpiration() && $item->isExpired() ? "ended" : "";
		$out = "<div class='slide_image ".$class."'>";
		
		if($showLike) {
			Loader::model("user_like");
			$thumb = "_thumb";//$squareImage ? "_thumb" : "";
			$out .= "<div class='likeheart".$thumb."'>";
			$out .= UserLike::likeButton($item->id, null, 'right', false, !$squareImage);
			$out .= "</div>";
		}

		if(!$item->isActive()) {
			$out .= "<div class='slide_unpublished_badge'>".$item->getStatusName()."</div>";
		}
		else
		if(!$item->isPublished()) {
			$out .= "<div class='slide_unpublished_badge'><i class='fa fa-clock-o' aria-hidden='true'></i> ".$item->getPublishedDate(true)."</div>";
		}
		else
		if($item->hasExpiration()) {
			if($item->isExpired()) {
				$out .= "<div class='slide_expiration_badge'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Ended: ".$item->getExpirationDate(false)."</div>";
			}
			else {
				$out .= "<div class='slide_expiration_badge'><i class='fa fa-clock-o' aria-hidden='true'></i> Ends: ".$item->getExpirationDate(false)."</div>";
			}
		}
		if($url) $out .= "<a href='".$url."'>";
		$file = $squareImage ? "_thumb.jpg" : "_feature.jpg";
		$out .= "<img class='slide_image' src='/images/features/".$item->id.$file."' alt='".$item->summary."'>";
		if($url) $out .= "</a>";
		$out .= "</div>";
		return $out;
	}
	
	public function displaySponsor($item, $class=null) 
	{
		$out = "";
		$out .= "<div class='sponsor_view ".$class."'>";
		$out .= "<a href='".$item->link."' target='_blank'>";
		$out .= "<img src='/images/sponsors/".$item->id.".png' alt='".$item->name." a Producers Social Sponsor'>";
		$out .= "</a>";
		$out .= "</div>";
		return $out;
	}
	
	public function displaySponsorGrid($items) 
	{
		$out = "";
		$out .= "<div class='sponsor_grid'>";
		foreach($items as $item) {
			$out .= $this->displaySponsor($item, "col-md-4");
		}
		$out .= "<div id='sponsor_contact'><a href='/sponsors'>Want to get involved?</a>";
		$user = Loader::helper('user');
		if($user->isSuperAdmin()) {
			$out .= "<div class='right small'><a href='/manage/sponsors'>Edit</a></div>";
		}
		$out .= "</div>";
		$out .= "</div>";
		return $out;
	}
	
	public function displaySponsorScroll($items) 
	{
		$out = "";
		$out .= "<div id='sponsor_scroll' class='col-lg-12' style='overflow: hidden;'>";
			$out .= "<div id='jssor_1' style='position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1000px; height: 140px; overflow: hidden; visibility: hidden;'>";
				$out .= "<div data-u='loading' style='position: absolute; top: 0px; left: 0px;'>";
					$out .= "<div style='filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;'></div>";
					$out .= "<div style='position:absolute;display:block;background:none;top:0px;left:0px;width:100%;height:100%;'></div>";
				$out .= "</div>";
				$out .= "<div data-u='slides' style='cursor: default; position: relative; top: 0px; left: 0px; width: 1000px; height: 140px; overflow: hidden;'>";
				
				foreach($items as $item) {
					$out .= "<div style='display: none;'>";
					$out .= "<a target='_blank' href='".$item->link."'><img alt='".$item->name."' src='/images/sponsors/".$item->id.".png' width='220' height='140' data-pin-nopin='true'></a>";
					$out .= "</div>";
				}
				$out .= "</div>";
			$out .= "</div>";
			$out .= "<div id='sponsor_contact'><a href='/sponsors'>Want to get involved?</a>";
			$user = Loader::helper('user');
			if($user->isSuperAdmin()) {
				$out .= "<div class='right small'><a href='/manage/sponsors'>Edit</a></div>";
			}
			$out .= "</div>";
			
			
		$out .= "</div>";
		return $out;
	}
	/*
	public function displaySubmission($item, $class=null) 
	{
		$out = "";
		$out .= "<div class='submission_view ".$class."'>";
		$out .= "<a href='".$item->link."' target='_blank'>";
		$out .= "<img src='/images/sponsors/".$item->id.".png' alt='".$item->name." a Producers Social Sponsor'>";
		$out .= "</a>";
		$out .= "</div>";
		return $out;
	}
	*/

}