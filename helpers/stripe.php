<?php
defined('C5_EXECUTE') or die("Access Denied.");

use \Stripe\Stripe as Stripe;

class StripeHelper 
{
	// VISA TEST: 4242424242424242
	//var $apiKey	= "sk_test_1u4LdK6Gft8uT3GsFlbYP0Vn";
	//var $pubKey	= "pk_test_s1vScavy8NuY2XM5wumlFZXw";
	var $apiKey	= "sk_live_DuVJWfDKu3aB1oKAC6gIC1uc";
	var $pubKey	= "pk_live_LUVor9N6xyM239Nrc8l5y3pb";

	function startup() 
	{
		//$this->controller = &$controller;
		Loader::library('stripe/init','');
	
		Stripe::setApiKey($this->apiKey);
		Stripe::setAppInfo("Producers Social", "1.0.1", "http://producerssocial.com");
	}

	// One-time setup. Once created plans can be referred to by id
	function setupPlans() 
	{
		echo "SETUP PLANS<br>";
		$yearly = \Stripe\Plan::create(array(
		  "name" 			=> "One Year Membership",
		  "id" 				=> "yearly-membership",
		  "interval" 		=> "year",
		  "currency" 		=> "usd",
		  "amount" 			=> 10000,
		));
		print_r($yearly);
		
		$monthly = \Stripe\Plan::create(array(
		  "name" 			=> "3 Month Membership",
		  "id" 				=> "quarterly-membership",
		  "interval" 		=> "month",
		  "interval_count"	=> 3,
		  "currency" 		=> "usd",
		  "amount" 			=> 3000,
		));
		print_r($monthly);
		
		$yearly = \Stripe\Plan::create(array(
		  "name" 			=> "One Year Membership (Admin Discount)",
		  "id" 				=> "yearly-membership-admin",
		  "interval" 		=> "year",
		  "currency" 		=> "usd",
		  "amount" 			=> 100,
		));
		print_r($yearly);
		
		$monthly = \Stripe\Plan::create(array(
		  "name" 			=> "3 Month Membership (Admin Discount)",
		  "id" 				=> "quarterly-membership-admin",
		  "interval" 		=> "month",
		  "interval_count"	=> 3,
		  "currency" 		=> "usd",
		  "amount" 			=> 100,
		));
		print_r($monthly);
	}
	
	function planName($plan) {
		$name = "None";
		if($plan == "yearly-membership") {
			$name = "One Year Membership";
		}
		else
		if($plan == "quarterly-membership") {
			$name = "3 Month Membership";
		}
		else
		if($plan == "yearly-membership-admin") {
			$name = "One Year Membership (Admin Discount)";
		}
		else
		if($plan == "quarterly-membership-admin") {
			$name = "3 Month Membership (Admin Discount)";
		}
		return $name;
	}
	
	function planPrice($plan) {
		$price = 0;
		if($plan == "yearly-membership") {
			$price = 10000;
		}
		else
		if($plan == "quarterly-membership") {
			$price = 3000;
		}
		else
		if($plan == "yearly-membership-admin") {
			$price = 100;
		}
		else
		if($plan == "quarterly-membership-admin") {
			$price = 100;
		}
		return $price;
	}
	
	function getCurrentCustomer() {
		$customer = null;
		$u = new User();
		if($u->isLoggedIn()) {
			Loader::model('user_profile');
			$user = UserInfo::getByID($u->uID);
			$profile = UserProfile::getByUserID($u->uID);
			$customer = $this->getCustomer($user, $profile);
		}
		return $customer;
	}
	
	function getCustomer($user, $profile) {
		$customer = null;
		if($profile->uCustomerID) {
			try {
				$obj = \Stripe\Customer::retrieve($profile->uCustomerID);
				if($obj) $customer = $obj->__toArray(true);
			}
			catch(Exception $e) {
				$out .= "Customer does not exist: ".$profile->uCustomerID."<br>";
				$customer = null;
			}
		}
		if(!$customer) {
			$obj = \Stripe\Customer::create(array(
				'email' => $user->uEmail
			));
			$customer = $obj->__toArray(true);
			
			$profile->uCustomerID = $customer['id'];
			UserProfile::save($profile);
		}
		return $customer;
	}
	
	function purchaseButton($item, $name, $price, $quantity, $redirect, $target, $class='button') 
	{
		$user = Loader::helper('user');
		if($user->isLocationAdmin()) {
			$price = 100;	
		}
		$out = "";
		$out .= "<a id='".$item."-button' class='".$class." purchase-button' href='javascript:;' ";
		$out .= "bt-item='".$item."' bt-price='".$price."' bt-quantity='".$quantity."' bt-name='".$name."' bt-redirect='".$redirect."' bt-target='".$target."' ";
		$out .= "data-zip-code='true'>".$name."</a>";
		return $out;
	}
	
	function subscribeButton($url=null, $plan='yearly-membership')
	{
		ob_start();
		$out = "";

		$user = Loader::helper('user');
		if($user->isLocationAdmin() && strpos($plan, "-admin") === false) {
			$plan = $plan."-admin";
		}
		
		$name 	= $this->planName($plan);
		$price 	= $this->planPrice($plan);
		
		if($user->isLocationAdmin()) {
			$price = 100;	
		}
		$nameAndPrice = $name." $".substr($price, 0, strlen($price)-2);
		$nameAndPrice = str_replace(" (", "<br>(", $nameAndPrice);
		
		if(!$url) $url = $_SERVER['PHP_SELF'];
		
		$u = new User();
		if($u->isLoggedIn()) {
			Loader::model('user_profile');
			Loader::model('user_subscription');
			Loader::model('locations');
	
			$form = Loader::helper('form');
			
			$p = UserProfile::getByUserID($profile->uID);
			$sub = UserSubscription::getByUserID($u->uID);
			
			if($sub && $sub->isActive()) {
				//$out .= "ALREADY SUBSCRIBED: ".$sub->planName()."<br>";
			}
	
			$white = null;
			//if($plan == "quarterly-membership") {
			//	$white = "white";	
			//}
			//$out .= "USER:".$u->uID."<br>";
			//$out .= $form->select('uLocation', Locations::getSelectList(1), $p->uLocation)."<br><br>";
			$out .= "<a id='".$plan."-button' class='button larger centerblock ".$white."' href='javascript:;' data-zip-code='true'>";
			//$out .= "<i class='fa fa-credit-card huge' aria-hidden='true'></i>&nbsp;<b>Buy Now</b><br class='clear'>";
			$out .= $nameAndPrice."</a>";
			$out .= "<input type='hidden' id='".$plan."-amount' value='".$price."'>";
			$out .= "<input type='hidden' id='".$plan."-name' value='".$name."'>";
		}
		else {
			$out .= "<a id='".$plan."-notloggedin' class='button' href='/login'>".$nameAndPrice."</a>";
			//$out .= "You must be <a href='/login'>signed in</a> to create a new subscription.";	
		}
		ob_end_clean();
		return $out;
	}
	
	function purchaseProcess($item, $quantity, $token) 
	{
		$out = "";
		if($token) {
			Loader::model('user_profile');
			Loader::model('user_purchase');
			Loader::model('purchase_item');
			
			$slack = Loader::helper('slack');
			
			$u = new User();
			if($u->isLoggedIn()) {
				$user = UserInfo::getByID($u->uID);
				$profile = UserProfile::getByUserID($u->uID);
				if($user && $profile) {
					$sub = $profile->getMemberSubscription();
					
					$purchase = PurchaseItem::getByID($item);					
					if($purchase) {
						if($purchase->isAvailable()) {
							$price = $purchase->price;
							
							$isadmin = $profile->isLocationAdmin();
							$error = false;
							$amount = $price * $quantity;
							try {
								$obj = \Stripe\Charge::create(array(
									"source"	=> $token,
									"capture"	=> !$isadmin,
									"amount" 	=> $amount,
									'currency' 	=> 'usd',
									'metadata' 	=> array("item"=>$item, "quantity"=>$quantity, "user_id"=>$u->uID)
								));
								$charge = $obj->__toArray(true);
							}
							catch(Stripe_CardError $e) {
							// Since it's a decline, Stripe_CardError will be caught
								$body = $e->getJsonBody();
								$err  = $body['error'];
							
								$error = "The card was declined. (".$err['code']." - ".$err['type']." : ".$e->getHttpStatus().")";
								$error .= ". ".$err['message'];
							} 
							catch (Stripe_InvalidRequestError $e) {
							// Invalid parameters were supplied to Stripe's API
								$body = $e->getJsonBody();
								$err  = $body['error'];
								$error = "Failed connecting to Stripe API. ".$err['message'];
							} 
							catch (Stripe_AuthenticationError $e) {
							// Authentication with Stripe's API failed
								$body = $e->getJsonBody();
								$err  = $body['error'];
								$error = "Authentication failed. Please contact support for help. ".$err['message'];
							} 
							catch (Stripe_ApiConnectionError $e) {
							// Network communication with Stripe failed
								$body = $e->getJsonBody();
								$err  = $body['error'];
								$error = "Network communication failure. Please check your internet connection and try again. ".$err['message'];
							} 
							catch (Stripe_Error $e) {
							// Display a very generic error to the user, and maybe send
								$body = $e->getJsonBody();
								$err  = $body['error'];
								$error = "An unknown error occurred. Please contact support for help. ".$err['message'];
							} 
							catch (Exception $e) {
							// Something else happened, completely unrelated to Stripe
								$error = "An unknown error occurred. Please contact support for help. ".$e->getMessage();
							}
							
							if($error) {
								$slack->send("Stripe Payment Failure:".$error." UserID:".$user->uID." Item:".$item);
								
								$out .= "<div class='alert alert-warning'>".$error."</div>";	
								$out .= "<br><br>";
								$out .= "To contact support, please email <a href='mailto:admin@producerssocial.com'>admin@producerssocial.com</a>";
								$out .= "<br><br>";
								if($sub && $sub->isActive()) {
									$out .= "<a class='button' href='/membership'>Goto Membership Portal</a>";
								}
								else {
									$out .= "<a class='button' href='/events'>Goto Events</a>";
								}
							}
							else {
								$userPurchase = new UserPurchase();
								//$userPurchase->uID 				= $user->uID;
								$userPurchase->uTransactionID 	= $charge['id'];
								$userPurchase->uItem 			= $item;
								$userPurchase->uEventID 		= $purchase->event_id;
								$userPurchase->uLocation		= $profile->uLocation;
								$userPurchase->uQuantity		= $quantity;
								$userPurchase->uAmount 			= $amount;
								$userPurchase->uStatus 			= 'Pending';
								$userPurchase->uDate 			= date("Y-m-d H:i:s", time());
								UserPurchase::create($userPurchase);
								
								$out .= $purchase->processOrder($quantity, $charge['id']);
								
								$slack = Loader::helper("slack");
								$slack->send("Ticket Purchase ".$userPurchase->uLocation.":".$quantity." $".($userPurchase->uAmount/100)." user:".$u->uID." (Stripe)");
							}
						}
						else {
							$out .= "<div class='alert alert-warning'>We are sorry, but the item you requested is sold out. Your account has not been charged.</div>";	
							$out .= "<br><br>";
							if($sub && $sub->isActive()) {
								$out .= "<a class='button' href='/membership'>Goto Membership Portal</a>";
							}
							else {
								$out .= "<a class='button' href='/events'>Goto Events</a>";
							}
						}
					}
					else {
						$out .= "<div class='alert alert-warning'>Failed finding the item to be purchased: ".$item."</div>";	
						$out .= "<br><br>";
						if($sub && $sub->isActive()) {
							$out .= "<a class='button' href='/membership'>Goto Membership Portal</a>";
						}
						else {
							$out .= "<a class='button' href='/events'>Goto Events</a>";
						}
					}
				}
				else {
					$out .= "<div class='alert alert-error'>Failed retrieving your user profile.</div>";
				}
				$email = $user->uEmail;
			}
			else {
				$out .= "<div class='alert alert-error'>You must be logged in to create a new membership.</div>";
			}
			
		}
		return $out;
	}
	
	// VISA TEST: 4242424242424242
	function subscribeProcess($plan, $token) 
	{
		$out = "";
		if($token) {
			Loader::model('user_profile');
			Loader::model('user_purchase');
			Loader::model('user_subscription');
			Loader::model('user_ticket');
			
			$slack = Loader::helper("slack");
			
			$u = new User();
			if($u->isLoggedIn()) {
				$user = UserInfo::getByID($u->uID);
				$profile = UserProfile::getByUserID($u->uID);
				if($user && $profile) {
					$lastExpiration = null;
					$existing = UserSubscription::getByUserID($u->uID);
					if($existing && $existing->isActive()) {
						$lastExpiration = $existing->ending;
					}
					
					// Cancel any existing subscriptions before creating new ones
					$this->cancelUserSubscriptions(false);
					
					$customer = $this->getCustomer($user, $profile);
					
					$isadmin = false;
					if($profile->isLocationAdmin() && strpos($plan, "-admin") === false) {
						$plan = $plan."-admin";
						$isadmin = true;
					}
					
					$error = false;
					try {
						$obj = \Stripe\Subscription::create(array(
							"source"	=> $token,
							"customer" 	=> $customer['id'],
							"plan" 		=> $plan,
							'metadata' 	=> array("user_id"=>$u->uID)
						));
						$sub = $obj->__toArray(true);
					}
					catch(Stripe_CardError $e) {
					// Since it's a decline, Stripe_CardError will be caught
						$body = $e->getJsonBody();
						$err  = $body['error'];
					
						$error = "The card was declined. (".$err['code']." - ".$err['type']." : ".$e->getHttpStatus().")";
						$error .= ". ".$err['message'];
					} 
					catch (Stripe_InvalidRequestError $e) {
					// Invalid parameters were supplied to Stripe's API
						$body = $e->getJsonBody();
						$err  = $body['error'];
						$error = "Failed connecting to Stripe API. ".$err['message'];
					} 
					catch (Stripe_AuthenticationError $e) {
					// Authentication with Stripe's API failed
						$body = $e->getJsonBody();
						$err  = $body['error'];
						$error = "Authentication failed. Please contact support for help. ".$err['message'];
					} 
					catch (Stripe_ApiConnectionError $e) {
					// Network communication with Stripe failed
						$body = $e->getJsonBody();
						$err  = $body['error'];
						$error = "Network communication failure. Please check your internet connection and try again. ".$err['message'];
					} 
					catch (Stripe_Error $e) {
					// Display a very generic error to the user, and maybe send
						$body = $e->getJsonBody();
						$err  = $body['error'];
						$error = "An unknown error occurred. Please contact support for help. ".$err['message'];
					} 
					catch (Exception $e) {
					// Something else happened, completely unrelated to Stripe
						$error = "An unknown error occurred. Please contact support for help. ".$e->getMessage();
					}
					
					if($error) {
						$slack->send("Stripe Subscription Failure:".$error." UserID:".$user->uID." Item:".$item);
								
						$out .= "<div class='alert alert-warning'>".$error."</div>";	
						$out .= "<br><br>";
						$out .= "To contact support, please email <a href='mailto:admin@producerssocial.com'>admin@producerssocial.com</a>";
						$out .= "<br><br>";
						if($sub && $sub->isActive()) {
							$out .= "<a class='button' href='/membership'>Goto Membership Portal</a>";
						}
						else {
							$out .= "<a class='button' href='/members'>Goto Members</a>";
						}
					}
					else {
						$userPurchase = new UserPurchase();
						$userPurchase->uID 				= $user->uID;
						$userPurchase->uTransactionID 	= $sub['id'];
						$userPurchase->uItem 			= $plan;
						$userPurchase->uEventID 		= '';
						$userPurchase->uLocation		= $profile->uLocation;
						$userPurchase->uQuantity		= 1;
						$userPurchase->uAmount 			= $sub['plan']['amount'];
						$userPurchase->uStatus 			= 'Completed';
						$userPurchase->uDate 			= date("Y-m-d H:i:s", time());
						UserPurchase::create($userPurchase);
	
						$subscription = new UserSubscription();
						$subscription->id 				= $sub['id'];
						$subscription->customer_id 		= $customer['id'];
						$subscription->user_id 			= $user->uID;
						$subscription->plan 			= $plan;
						$subscription->status 			= 1;	// 0=inactive 1=active 2=canceled
						$subscription->ending 			= date("Y-m-d H:i:s", $sub['current_period_end']);
						$subscription->created 			= date("Y-m-d H:i:s", $sub['created']);
						$subscription = UserSubscription::create($subscription);
						
						$quantity = $subscription->planTicketQuantity();
						
						$exp = $subscription->planTicketExpiration();
						if($lastExpiration) {
						// Apply any time remaining to extend the expiration of tickets
							$t = strtotime($lastExpiration);
							if($t > time()) {
								$r = $t - time();
								$e = strtotime($exp);
								$exp = date("Y-m-d H:i:s", $e + $r);
							}
						}
						UserTicket::generate($quantity, $user->uID, null, $profile->uLocation, $exp); 
						
						$out .= "<div class='alert alert-white' style='width:660px; max-width:100%; float:none; margin:auto;'>";
						$out .= "<h1>Thank You!</h1>Your membership has been activated and ".$quantity." tickets added to your account!<br class='clear'><br>";
						$out .= "<a class='button left' href='/members'><i class='fa fa-id-card-o' aria-hidden=true></i> Goto Member Portal</a>";
						$out .= "<a class='button white right' href='/profile/membership'>View Membership Details <i class='fa fa-id-card-o' aria-hidden=true></i></a>";
						$out .= "<br class='clear'></div>";
						
						// SEND CONFIRMATION EMAIL
						$memberName = $profile->uFullName;
						if(!$memberName) $memberName = $user->getUserName();
						
						$mh = Loader::helper('mail');
						$mh->from(EMAIL_DEFAULT_FROM_ADDRESS, EMAIL_DEFAULT_FROM_NAME);
						$mh->to($user->uEmail, $memberName);
						$mh->addParameter('profile', $profile);
						$mh->addParameter('user_id', $user->getUserID());
						$mh->addParameter('memberName', $memberName);
						$mh->addParameter('sub', $subscription);
						$mh->addParameter('subject', "Membership Confirmation");
						$mh->load("subscription_confirm");
						$mh->sendMail();
							
						$slack = Loader::helper("slack");
						$slack->send("New Subscription ".$userPurchase->uLocation.":".$plan." $".($userPurchase->uAmount/100)." user:".$subscription->user_id);
					}
				}
				else {
					$out .= "<div class='alert alert-error'>Failed retrieving your user profile.</div>";
				}
				//$email = $user->uEmail;
				//$mh->addParameter('uID', $user->uID);
			}
			else {
				$out .= "<div class='alert alert-error'>You must be logged in to create a new membership.</div>";
			}
			
		}
		return $out;
	}
	
	public function cancelUserSubscriptions($sendConfirmation=true) {
		$u = new User();
		if($u->isLoggedIn()) {
			Loader::model('user_subscription');
			Loader::model('user_profile');
			
			$subscriptions = UserSubscription::getAllByUserID($u->uID);
			if($subscriptions && count($subscriptions)) {
				foreach($subscriptions as $sub) {
					if($sub && $sub->id) {
						try {
							$obj = \Stripe\Subscription::retrieve($sub->id);
							if($obj) {
							// Simply cancel renewal - there is no need to fully cancel
								$obj->cancel(array('at_period_end' => true));
							}
							$sub->status = 2;
						}
						catch(Exception $e) {
							$sub->status = 0;
						}
						UserSubscription::save($sub);

						$user = UserInfo::getByID($sub->user_id);
						$profile = UserProfile::getByUserID($sub->user_id);
						
						if($sendConfirmation) {
						// SEND CONFIRMATION EMAIL
							$memberName = $profile->uFullName;
							if(!$memberName) $memberName = $user->getUserName();
							
							$mh = Loader::helper('mail');
							$mh->from(EMAIL_DEFAULT_FROM_ADDRESS, EMAIL_DEFAULT_FROM_NAME);
							$mh->to($user->uEmail, $memberName);
							$mh->addParameter('user_id', $sub->user_id);
							$mh->addParameter('profile', $profile);
							$mh->addParameter('memberName', $memberName);
							$mh->addParameter('sub', $sub);
							$mh->addParameter('subject', "Subscription Cancelled");
							$mh->load("subscription_cancel");
							$mh->sendMail();
						
							$slack = Loader::helper("slack");
							$slack->send("Cancelled Subscription ".$profile->uLocation.":".$sub->plan." user:".$subscription->user_id);
						}
					}
				}
			}
		}
	}
	
	public function setAutorenew($subscriptionId, $on=true) {
		$error = null;
		try {
			if($on) {
				\Stripe\Subscription::update($subscriptionId);
			}
			else {
				$sub = \Stripe\Subscription::retrieve($subscriptionId);
				if($sub) {
					$sub->cancel(array('at_period_end' => true));
				}
			}
		}
		catch(Exception $e) {
			$error = "Error - The subscription does not exist: ".$subscriptionId."<br>";//.$this->getErrorMessage($e);
		}
		return $error;
	}
	
	public function updateAllSubscriptions() {
		// Find all active subscriptions that have ended but are still active and see if they have been renewed
		// otherwise set them to lapsed
		
		Loader::model('user_subscription');
		Loader::model('user_profile');
		Loader::model('user_ticket');
		
		$out = "<h1>Updating Subscriptions</h1>";
		
		$db = Loader::db();
		$q = "UPDATE UserProfiles SET uIsMember=0 WHERE 1";
		$db->query($q);
		
		$slack = Loader::helper("slack");
		
		// Find all recently lapsed subscriptions and renew or cancel them
		$time = time();// - (6 * 60 * 60); 
		$now = date("Y-m-d H:i:s", $time);
		$yesterday = date("Y-m-d H:i:s", $time - (48 * 60 * 60));
		$all = UserSubscription::getAll("ending < '".$now."' AND ending > '".$yesterday."' AND status != 2");
		if($all) {
			$out .= "Found ".count($all)." subscriptions to update:<br>";
			foreach($all as $s) {
				if($s->type == 'gift') {
				// This subscription was not processed by stripe
					if($s->autorenew) {
						$s->renewed = date("Y-m-d H:i:s", time());
						$s->ending = date("Y-m-d H:i:s", time() + $s->planDuration());
						$s->status = 1;
						UserSubscription::save($s);
						
						$p = UserProfile::getByUserID($s->user_id);
						$quantity = $s->planTicketQuantity();
						UserTicket::generate($quantity, $s->user_id, null, $p->uLocation, $s->ending); 
						
						Loader::model('user_log');
						UserLog::record("UserSubscription", $s->user_id, "renewed gift");
						$out .= "RENEWED: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
						
						$slack->send("Renewed Subscription ".$p->uLocation." [comped]:".$s->plan." user:".$s->user_id." subid:".$s->id);
					}
					else {
						$s->status = 4;	
						UserSubscription::save($s);
						
						Loader::model('user_log');
						UserLog::record("UserSubscription", $s->user_id, "lapsed gift");
						$out .= "LAPSED: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
						
						$p = UserProfile::getByUserID($s->user_id);
						$slack->send("Lapsed Subscription ".$p->uLocation." [comped]:".$s->plan." user:".$s->user_id." subid:".$s->id);
					}
				}
				else {
					$out .= $s->id."<br>";
					try {
						$obj = \Stripe\Subscription::retrieve($s->id);
						if($obj) {
							$obj->cancel(array('at_period_end' => true));
							$sub = $obj->__toArray(true);
							$p = UserProfile::getByUserID($s->user_id);
							
							if($sub['status'] == "active") {
							// The subscription has been renewed
								if($s->created == date("Y-m-d H:i:s", $sub['current_period_start'])) {
									$s->ending = date("Y-m-d H:i:s", $sub['current_period_end']);
									$s->status = 1;
									UserSubscription::save($s);
									
									Loader::model('user_log');
									UserLog::record("UserSubscription", $s->user_id, "updated from stripe");
									$out .= "UPDATED: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
								}
								else {
									$s->renewed = date("Y-m-d H:i:s", $sub['current_period_start']);
									$s->ending = date("Y-m-d H:i:s", $sub['current_period_end']);
									$s->status = 1;
									UserSubscription::save($s);
									
									$quantity = $s->planTicketQuantity();
									UserTicket::generate($quantity, $s->user_id, null, $p->uLocation, $s->ending); 
									
									Loader::model('user_log');
									UserLog::record("UserSubscription", $s->user_id, "renewed from stripe");
									$out .= "RENEWED: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
									
									$slack->send("Renewed Subscription ".$p->uLocation.":".$s->plan." user:".$s->user_id." subid:".$s->id);
								}
							}
							else {
								if($sub['status'] == 'past_due') {
									$s->status = 4; // lapsed
									UserSubscription::save($s);
									
									Loader::model('user_log');
									UserLog::record("UserSubscription", $s->user_id, "stripe past due");
									$out .= "LAPSED: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
									
									$slack->send("Lapsed Subscription ".$p->uLocation.":".$s->plan." user:".$s->user_id." subid:".$s->id);
								}
								else {
									$s->status = 3; // pending
									UserSubscription::save($s);
									
									Loader::model('user_log');
									UserLog::record("UserSubscription", $s->user_id, "stripe pending");
									$out .= "PENDING: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
									
									//$slack->send("Pending Subscription ".$p->uLocation.":".$s->plan." user:".$s->user_id." subid:".$s->id);
								}
							}
						}
					}
					catch(Exception $e) {
						$s->status = 0;
						
						Loader::model('user_log');
						UserLog::record("UserSubscription", $s->user_id, "invalid subscription");
						$out .= "INVALID: ".$s->id.": ".$s->plan." for <a href='/profile/view/".$s->user_id."'>user ".$s->user_id."</a><br>";
						
						$slack->send("Invalid Subscription ".$s->id.": ".$s->plan." user:".$s->user_id);
						UserSubscription::save($s);
					}
				}
			}
		}
		else {
			$out .= "No subscriptions found to update:<br>";
		}
		
		// Update all active subscriptions
		$valid = UserSubscription::getAll("(ending > '".$now."' AND status=1) OR type='permanent'");
		if($valid) {
			$out .= "<div class='alert alert-info'>";
			$out .= "There are a total of ".count($valid)." active members.";
			$out .= "</div>";
			foreach($valid as $s) {
				$q = "UPDATE UserProfiles SET uIsMember=1 WHERE uID=".$s->user_id." LIMIT 1";
				$db->query($q);
			}
		}
		return $out;
	}
	
	function sendRenewalReminders($daysInAdvance) 
	{
		Loader::model('user_subscription');
		Loader::model('user_profile');
		
		$oneDay = (24 * 60 * 60);
		$date = ($daysInAdvance * $oneDay) + time();
		$start = date("Y-m-d H:i:s", $date);
		$end = date("Y-m-d H:i:s", $date + $oneDay);
		$q = "status=1 AND ending>='".$start."' AND ending<='".$end."'";
		echo $q.'<br>';
		$subs = UserSubscription::getAll($q);
		if($subs && count($subs)) {
			echo "FOUND:".count($subs)."<br>";
			foreach($subs as $sub) {
				echo $sub->id."<br>";
				
				$p = UserProfile::getByUserID($sub->user_id);
				$info = UserInfo::getByID($sub->user_id);
				
				$mh = Loader::helper('mail');
				$mh->from("admin@producerssocial.com");
				$mh->bcc("stephen@walkerfx.com");
				$mh->to($info->uEmail);
				
				$name = $p->uArtistName;
				if(!$name) $name = $info->uName;
				
				$mh->addParameter('memberName', $name);
				$mh->addParameter('sub', $sub);
				$mh->addParameter('profile', $p);
				
				$locationName = Locations::getName($e->Location);
				$mh->addParameter('location', $e->Location);
				$mh->addParameter('locationName', $locationName);
				$mh->addParameter('subject', "Renewal Reminder");
				$mh->load("renewal_reminder");
				$out .= $mh->getBodyHTML();
				$mh->sendMail();
			}
			
			$slack = Loader::helper("slack");
			$slack->send(count($subs)." Renewal Reminder Emails Sent");
		}
		else {
			echo "No reminders sent.<br><br>";	
		}
	}
	
	function getErrorMessage($e) {
		$err = $e->getJsonBody();
		return $err['message'];
	}

	function cancelSubscription() 
	{
		ob_start();
		$out = "";
		if($this->ENABLED) {
			Loader::model('user_profile');
		}
		ob_end_clean();
		return $out;
	}
}
?>