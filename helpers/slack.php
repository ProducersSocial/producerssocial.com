<?php
defined('C5_EXECUTE') or die("Access Denied.");

class SlackHelper 
{
	var $endpoint = "https://hooks.slack.com/services/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";
	
	public function send($message, $channel = "hq-notifications", $icon = ":longbox:") {
        $channel = ($channel) ? $channel : "hq-notifications";
        $data = "payload=" . json_encode(array(
                "channel"       =>  "#{$channel}",
                "text"          =>  $message,
                "icon_emoji"    =>  $icon
            ));
	
        $ch = curl_init($this->endpoint);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
	
        return $result;
    }	
}