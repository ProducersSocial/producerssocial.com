<?php
defined('C5_EXECUTE') or die("Access Denied.");
class ProducersHelper {

	public function replaceAttr(&$input, $attr, $end, $replaceWith) {
		$found = false;
		$a = strpos($input, $attr);
		if($a !== false) {
			$a2 = $a + strlen($attr);
			$b = strpos($input, $end, $a2);
			if($b !== false) {
				//echo "input {$input}<br>";
				//echo "a:$a a2:$a2 b:$b<br>";
				$found = true;
				$input = substr($input, 0, $a-1).$replaceWith.substr($input, $b+1);
			}
		}
		return $found;
	}
	
	public function presentFeature($feature, $forceSmall=false, $isCurrentUser=false) {
		$type = null;
		
		if($forceSmall) {
			if(!$this->replaceAttr($feature, "height=\"", "\"", "")) {
				$this->replaceAttr($feature, "height:", ";", "");
			}
		}
		
		if(strpos($feature, "bandcamp") !== false) {
			$size = "small";
			if($forceSmall) {
				$feature = str_replace("artwork=small", "", $feature);
				$feature = str_replace("size=large", "size=small", $feature);
			}
			else
			if(strpos($feature, "size=large") !== false) {
				if(strpos($feature, "artwork=small") !== false) {
					$size = "medium";
				}
				else {
					$size = "large";
				}
			}
			$type = "bandcamp_embed_".$size;
		}
		else
		if(strpos($feature, "soundcloud") !== false) {
			$type = "soundcloud_embed_large";
			if($forceSmall) {
				$type = "soundcloud_embed_small";
			}
		}
		
		if(!$type) {
			if($isCurrentUser) {
				$feature = "<div class='error'>This embedded player is not recognized. Please contact admin@producerssocial for assistance.</div>";
			}
			else {
				$feature = null;
			}
		}
		else {
			$feature = str_replace("<iframe", "<iframe class='$type'", $feature);
		
			if(!$this->replaceAttr($feature, "width=\"", "\"", "")) {
				$this->replaceAttr($feature, "width:", ";", "");
			}
			$feature .= "<br class='clear'>";
		}
		$feature = str_replace("auto_play=true", "auto_play=false", $feature);
		
		return $feature;
	}
}
?>