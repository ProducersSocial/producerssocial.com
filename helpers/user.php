<?php
defined('C5_EXECUTE') or die("Access Denied.");

class UserHelper 
{
	public static $instance = null;
	
	var $id				= null;
	var $me				= null;
	var $user			= null;
	var $info			= null;
	var $profile		= null;
	var $sub			= null;
	var $adminLocation	= null;
	
	var $locations		= null;
	
	public function __construct() {
		if($instance) {
			return $instance;	
		}
		else {
			$instance = $this->setup();
		}
	}

	public function setup($userid=null) 
	{
		Loader::model('locations');
		Loader::model('user_profile');
		Loader::model('user_subscription');
		Loader::model('user_ticket');
		
		//if(ADMIN) {
		//echo "SETUP:".$userid."\n";
		//}
		$this->me = new User();
		if($this->me && $this->me->isLoggedIn()) {
			$this->user = $this->me;
			$this->id = $this->me->uID;
			
			$this->profile = UserProfile::getByUserID($this->id);
			if(!$this->profile) {
				$this->profile = UserProfile::createForUser($this->id);
			}
			
			//if($this->profile->isSuperAdmin()) {
				if($userid) {
					$this->id = $userid;
				}
				else
				if(isset($_REQUEST['uid']) && $_REQUEST['uid']) {
					$this->id = $_REQUEST['uid'];
				}
			//}		
			
			if($this->id != $this->profile->uID) {
				$this->user 		= User::getByUserID($this->id);
				$this->profile 		= UserProfile::getByUserID($this->id);
				$this->me->profile 	= UserProfile::getByUserID($this->me->uID);
			}
			else {
				$this->me->profile = $this->profile;	
			}
			$this->info = UserInfo::getByID($this->id);
			$this->sub 	= UserSubscription::getByUserID($this->id);
			
			$this->adminLocation = strtoupper($this->profile->uLocationAdmin);
		}
		else {
			if($userid) {
				$this->id = $userid;
			}
			else
			if(isset($_REQUEST['uid']) && $_REQUEST['uid']) {
				$this->id = $_REQUEST['uid'];
			}
			$this->profile 	= UserProfile::getByUserID($this->id);
			$this->info 	= UserInfo::getByID($this->id);
			$this->sub 		= UserSubscription::getByUserID($this->id);
		}
	}
	
	public function isLoggedIn() 
	{
		if($this->me && $this->me->isLoggedIn()) {
			return true;
		}
		return false;
	}
	
	public function isActive() 
	{
		return $this->user->uIsActive;
	}
	
	public function isSuperAdmin() 
	{
		if($this->me && $this->me->isSuperUser()) {
			return true;	
		}
		if($this->me->profile) {
			return $this->me->profile->isSuperAdmin();	
		}
		return false;
	}
	
	public function getEmail() 
	{
		if($this->info) {
			return $this->info->getUserEmail();
		}
		else 
		if($this->profile) {
			return $this->profile->uEmail;
		}
		return null;
	}
	
	public function isMasterAdmin() 
	{
		if($this->me) {
			return $this->me->isSuperUser();	
		}
		return false;
	}
	
	public function isLocationAdmin($loc=null) 
	{
		if($this->me->profile) {
			return $this->me->profile->isLocationAdmin($loc);	
		}
		return false;
	}
	
	public function isAdmin() 
	{
		return $this->isSuperAdmin() || $this->me->profile->uLocationAdmin;
	}
	
	public function isProfileOwner() 
	{
		if($this->profile) {
			return $this->profile->uID == $this->me->uID;	
		}
		return false;
	}
	
	public function isProfileComplete() 
	{
		if($this->profile) {
			return $this->profile->uArtistName != null && $this->profile->hasAvatar();	
		}
		return false;
	}
	
	public function isMember() 
	{
		if(($this->sub && $this->sub->isActive()) || $this->isMasterAdmin()) {
			return true;	
		}
		return false;
	}
	
	public function isGuest() 
	{
		if(!$this->isLoggedIn() || $this->isMasterAdmin()) {
			return true;	
		}
		return false;
	}
	
	public function getLocations() 
	{
		if(!$this->locations) {
			$this->locations = Locations::getList();
		}
		return false;
	}
	/*
	public function displayProfilePopup($uid) {
		$user 	= User::getByUserID($uid);
		$p 		= UserProfile::getByUserID($uid);
		$sub 	= UserSubscription::getByUserID($uid);
		$link 	= "/profile/view/".$uid;
		
		echo "<div class='user_profile_popup_container'>";
		$this->getLocations();
		
		if($sub && !$sub->isActive()) {
			$sub = null;	
		}
		if($sub && $sub->isActive() && $p->uHandle) {
			$link = "/".$p->uHandle;	
		}
		
		$av = Loader::helper('concrete/avatar');
		echo "<table class='user_profile_popup_table'>";
			echo "<tr>";
			echo "<td class='user_profile_popup_table_left'>";
				echo $p->getAvatar();
			echo "</td>";
			echo "<td class='user_profile_popup_table_right'>";
				$icons = null;
				if($p && $sub) {
					if($p->uSoundcloud) {
						$icons .= '<a class="social-soundcloud-32 right" target="_blank" href="'.UserProfile::completeURL($p->uSoundcloud, "http://soundcloud.com/###").'">&nbsp;</a>';
					}
					if($p->uBandcamp) {
						$icons .= '<a class="social-bandcamp-32 right" target="_blank" href="'.UserProfile::completeURL($p->uBandcamp, "http://###.soundcloud.com/").'">&nbsp;</a>';
					}
					if($p->uFacebook) {
						$icons .= '<a class="social-facebook-32 right" target="_blank" href="'.UserProfile::completeURL($p->uFacebook, "http://facebook.com/###").'">&nbsp;</a>';
					}
					if($p->uTwitter) {
						$icons .= '<a class="social-twitter-32 right" target="_blank" href="'.UserProfile::completeURL($p->uTwitter, "http://twitter.com/###").'">&nbsp;</a>';
					}
					if($p->uInstagram) {
						$icons .= '<a class="social-instagram-32 right" target="_blank" href="'.UserProfile::completeURL($p->uInstagram, "http://instagram.com/###").'">&nbsp;</a>';
					}
					if($p->uTube) {
						$icons .= '<a class="social-youtube-32 right" target="_blank" href="'.UserProfile::completeURL($p->uTube, "http://youtube.com/###").'">&nbsp;</a>';
					}
					//echo $icons;
				}
				echo "<a class='members_profile_name' href='".$link."'>".($p ? $p->uArtistName : $user->getUserName())."</a><br>";
				
				if($p->uWebsite) echo "<div class='profile_website'><a target='_blank' href='".$p->uWebsite."'>".$p->uWebsite."</a></div>";
				if($p->uStyleTags) echo "<div class='members_profile_styletags'>Style: ".trim($p->uStyleTags)."</div>";
					
				if($p && $sub) {
					echo "<div class='members_social_icons2'>".$icons."</div>";
				}
			echo "</td>";
			echo "</tr>";
		echo "</table>";
			
		echo "<div class='user_profile_popup_summary'>";
			$bio = substr($p->uSummary, 0, 255);
			if(strlen($bio) < strlen($p->uSummary)) $bio .= "... <a href='".$link."'>Read More</a>";
	
			$embed = false;
			$u = new User();
			if($p && $sub) {
				if($p->uFeaturedTrack) {
					$embed = true;
					$producers = Loader::helper("producers");
					echo "<div class='members_profile_embed'>";
					echo $producers->presentFeature($p->uFeaturedTrack, true, $p->uID == $u->uID);
					//echo "<div class='members_profile_bio_right'>";
					//echo $bio;
					//echo "<div class='fadeout'></div>";
					//echo "</div>";
					echo "</div>";
				}
			}
			if(!$embed) {
				echo "<div class='members_profile_bio'>";
				echo "<div class='members_profile_bio_div'>";
				echo $bio;
				echo "</div>";
				echo "<a class='slide_read_more button white small' href='".$link."'><i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>";
				echo "<div class='fadeout'></div>";
				echo "</div>";
			}
		echo "</div>";
		echo "</div>";
	}
	*/
	
	public function displaySummary($uid, $small=true, $link=null, $user=null, $p=null, $sub=null, $divId=null, $showEmbedAndSummary=false, $showLittleArrow=true) {
		if(!$user) 	$user 	= User::getByUserID($uid);
		if(!$p) 	$p 		= UserProfile::getByUserID($uid);
		if(!$sub)	$sub 	= UserSubscription::getByUserID($uid);
		if(!$link) 	$link 	= "/members/profile/".$uid;
		//$info = UserInfo::getByID($uid);
		
		if($divId) {
			echo "<div id='".$divId."'>";	
		}
		if(!$small) {
			echo "<div class='row pad-left'>";
		}
		$this->getLocations();
		
		$name = $user->getUserName();
		if($p && $p->uArtistName) {
			$name = $p->uArtistName;
		}
		
		if($sub && !$sub->isActive()) {
			$sub = null;	
		}
		if($sub && $sub->isActive() && $p->uHandle) {
			$link = "/".$p->uHandle;	
		}
		$small = $small ? "small col-md-4" : null; 
		
		$link = str_replace(BASE_URL, "", $link);	
		$link = str_replace("https://www.producerssocial.com/", "", $link);	
		//if(ADMIN) {
		//	echo BASE_URL."<br>";
		//	echo $uid.":".$link."<br>";
		//}
		
		$av = Loader::helper('concrete/avatar');
		
		echo "<table class='members_profile ".$small."'>";
			echo "<tr>";
			echo "<td class='members_profile_left'>";
				if($p) {
					echo $p->getAvatar();
				}
				else {
					echo "<div class='member_avatar ".$memberType."'>";
					echo "<img src='/files/avatars_small/avatar.jpg'>";
					echo "</div>";
				}
				//if($sub) {
				//	echo '<a class="premium_member_badge_small clear" href="'.$link.'">&nbsp;</a>';
				//}
				if($small) {
					echo "<div class='members_profile_name clear center hidden-sm-up'>".$name."</div>";
				}
			echo "</td>";
			echo "<td class='members_profile_right ".($small ? "hidden-sm-down" : "")."'>";
				if(!$p) {
					echo "<div class='alert alert-white'><h3>Sorry! We don't have a profile for this member.</h3></div>";
				}
				else {
					$icons = null;
					if($p && $sub) {
						if($p->uSoundcloud) {
							$icons .= '<a class="social-soundcloud-32 right" target="_blank" href="'.UserProfile::completeURL($p->uSoundcloud, "http://soundcloud.com/###").'">&nbsp;</a>';
						}
						if($p->uBandcamp) {
							$icons .= '<a class="social-bandcamp-32 right" target="_blank" href="'.UserProfile::completeURL($p->uBandcamp, "http://###.soundcloud.com/").'">&nbsp;</a>';
						}
						if($p->uFacebook) {
							$icons .= '<a class="social-facebook-32 right" target="_blank" href="'.UserProfile::completeURL($p->uFacebook, "http://facebook.com/###").'">&nbsp;</a>';
						}
						if($p->uTwitter) {
							$icons .= '<a class="social-twitter-32 right" target="_blank" href="'.UserProfile::completeURL($p->uTwitter, "http://twitter.com/###").'">&nbsp;</a>';
						}
						if($p->uInstagram) {
							$icons .= '<a class="social-instagram-32 right" target="_blank" href="'.UserProfile::completeURL($p->uInstagram, "http://instagram.com/###").'">&nbsp;</a>';
						}
						if($p->uTube) {
							$icons .= '<a class="social-youtube-32 right" target="_blank" href="'.UserProfile::completeURL($p->uTube, "http://youtube.com/###").'">&nbsp;</a>';
						}
						//echo $icons;
					}
					echo "<a class='members_profile_name' href='".$link."'>".$name."</a><br>";
					
					if($p->uWebsite) echo "<div class='profile_website'><a target='_blank' href='".$p->uWebsite."'>".$p->uWebsite."</a></div>";
					if($p->uStyleTags) echo "<div class='members_profile_styletags'>Style: ".trim($p->uStyleTags)."</div>";
						
					if($p && $sub) {
						echo "<div class='members_social_icons2'>".$icons."</div>";
					}
				}
			echo "</td>";
			echo "</tr>";
		echo "</table>";
		
		if(!$small && $p) {
			echo "<div class='members_profile_summary'>";
				//echo "<tr>";
					$bio = substr($p->uSummary, 0, 255);
					if(strlen($bio) < strlen($p->uSummary)) $bio .= "... <a href='".$link."'>Read More</a>";
			
					$embed = false;
					$u = new User();
					if($p && $sub) {
						if($p->uFeaturedTrack) {
							$embed = true;
						}
					}
					if($embed) {
						$producers = Loader::helper("producers");
						echo "<div class='members_profile_embed'>";
						echo $producers->presentFeature($p->uFeaturedTrack, true, $p->uID == $u->uID);
						//echo "<div class='members_profile_bio_right'>";
						//echo $bio;
						//echo "<div class='fadeout'></div>";
						//echo "</div>";
						echo "</div>";
					}
					if(!$embed || $showEmbedAndSummary) {
						echo "<div class='members_profile_bio ".($embed ? "" : "noembed")."'>";
						echo "<div class='members_profile_bio_div'>";
						echo $bio;
						echo "</div>";
						if($showLittleArrow) {
							echo "<a class='slide_read_more button white small' href='".$link."'><i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>";
						}
						echo "<div class='fadeout'></div>";
						echo "</div>";
					}
				//echo "</tr>";
			echo "</div>";
		}
		if(!$small) {
			echo "</div>";//row
		}
		if($divId) {
			echo "</div>";	
		}
	}
}