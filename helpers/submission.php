<?php
defined('C5_EXECUTE') or die("Access Denied.");

class SubmissionHelper 
{
	
	public function displaySlide($item, $hidden=false) 
	{
		$style = "";
		if($hidden) $style = "style='display:none;'";
		
		$class = "";
		if($item->closed) {
			$class .= "unpublished";
		}
		
		$user = Loader::helper('user');
		$url = $this->getItemURL($item);
		
		$out = "";
		$out .= "<a id='submissionform' name='submissionform'></a>";
		$out .= "<div class='slide ".$class."' id='slide_".$item->id."' ".$style.">";
		$out .= $this->displayImageAndStats($item, $url, false);
		$out .= "<div class='slide_content'>";
		//$out .= "<div class='slide_title'>".$item->name."</div>";
		$out .= "<div class='slide_summary'>".$item->summary."</div>";
		$out .= "<a class='slide_read_more button white small' href='".$url."'>More <i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>";
		$out .= "</div>";
		$out .= "</div>";
		
		return $out;
	}

}