<?php
defined('C5_EXECUTE') or die("Access Denied.");

class PagingHelper 
{	
	public function __construct() {
	}
	
	public function show($page, $itemsPerPage, $totalItems, $itemCount, $url, $query=null) {
		$pages = ceil($totalItems / $itemsPerPage);
		echo "<div class='paging'>";
		if($page > 1) {
			echo "<a class='paging_start' href='".$url."'><i class='fa fa-step-backward' aria-hidden='true'></i> </a>";
		}
		else {
			echo "<div class='paging_start gray' href='".$url."'><i class='fa fa-step-backward' aria-hidden='true'></i> </div>";
		}
		if($page > 1) {
			echo "<a class='paging_prev' href='".$url."&page=".($page-1)."'><i class='fa fa-chevron-left' aria-hidden='true'></i> </a>";
		}
		else {
			echo "<div class='paging_prev gray'><i class='fa fa-chevron-left' aria-hidden='true'></i> </div>";
		}
		echo $page." of ".$pages;
		if($page < $pages) {
			echo "<a class='paging_end' href='".$url."&page=".$pages."'><i class='fa fa-step-forward' aria-hidden='true'></i> </a>";
		}
		else {
			echo "<div class='paging_end gray'><i class='fa fa-step-forward' aria-hidden='true'></i> </div>";
		}
		if($page < $pages) {
			echo "<a class='paging_next' href='".$url."&page=".($page+1)."'><i class='fa fa-chevron-right' aria-hidden='true'></i> </a>";
		}
		else {
			echo "<div class='paging_next gray'><i class='fa fa-chevron-right' aria-hidden='true'></i> </div>";
		}
		echo "</div>";
		
		if(ADMIN) {
			if($query) {
				echo "<div class='admin_area'>";
				echo "<h3>Viewing: ".$itemCount." of ".$totalItems."</h3>";
				echo "<p class='small'>".$query."</p>";
				echo "</div>";
			}
		}
	}
	
	public function getLimit($itemsPerPage, &$page=null) {
		if($page == null) {
			if(isset($_REQUEST['page'])) {
				$page = $_REQUEST['page'];
			}
		}
		if(!$page || $page < 1) $page = 1;
		$limit = ($itemsPerPage * ($page-1)).",".$itemsPerPage;
		return "LIMIT ".$limit;
	}
	
}