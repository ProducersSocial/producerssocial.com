<?php
defined('C5_EXECUTE') or die("Access Denied.");
	
//-----------------------------------------------------------------------------------------------------------------------------
//: TicketsHelper
//-----------------------------------------------------------------------------------------------------------------------------
class TicketsHelper {

	public function navbarTicketDisplay($user_id, $class='navbar-tickets') {
		echo "<a href='/members/tickets' class='".$class."'>";
		$info = TicketsHelper::getUserTicketsArray($user_id);
		echo "<i class='fa fa-ticket' aria-hidden='true'></i>".$info['unusedCount']." &nbsp; ";
		echo "<i class='fa fa-calendar-check-o' aria-hidden='true'></i>".($info['rsvpCount']+$info['liveCount']);
		echo "</a>";
	}
	
	public function getUserTicketStatusForEmail($user_id, $event_id, $title=null)
	{
		Loader::model('facebook_event');
		Loader::model('user_ticket');
		$user = Loader::helper('user');
		$user->setup($user_id);
		
		$event = FacebookEvent::getID($event_id);
		$tickets = UserTicket::getAllTicketsForUserAtEvent($user_id, $event_id);
		
		$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px;";
		
		$out = "";
		$out .= "<div style='text-align:center; font-size:18px; font-weight:normal; padding:20px; border:1px solid #DDD; border-radius:10px; margin-top:20px; margin-bottom:20px;'>";
		if($title) {
			$out .= $title;
			$out .= "<br><br>";
		}
		
		if(!$tickets) {
			if($event->TicketMode != 'online') {
				if($event->TicketMode == 'free') {
					$out .= "<b>This is a free event!</b><br>Please arrive early to sign up on the list if you have a tune to share.<br><br>";	
				}
				else {
					$out .= "Please see the event description for ticketing information. Please arrive early to sign up on the list if you have a tune to share.<br><br>";	
				}
				$out .= "<a href='".BASE_URL."eventinfo?id=".$event_id."' style='".$buttonStyle."'>View Event Details</a></em>";
			}
			else {
				$out .= "<em style='text-align:18px;'>You do not have any tickets for this event!</em><br><br>";
				$out .= "<a href='".BASE_URL."eventinfo?id=".$event_id."' style='".$buttonStyle."'>Purchase Tickets Now</a></em>";
			}
		}
		else {
			$count = count($tickets);
			
			$reserved = null;
			if($user->isMember()) {
				$reserved = " reserved";	
			}
			$out .= "<b>You have ".$count." ticket".($count == 1 ? "" : "s").$reserved." for this event!</b>";
			$out .= "<br><br><br>";
			$out .= "<a href='".BASE_URL."eventinfo?id=".$event_id."' style='".$buttonStyle."'>View Event Details</a></em>";
			$out .= "<br><br><br>";
			
			$hasAutoSignup = false;
			foreach($tickets as $t) {
				if($user->isMember() && $t->autosignup) {
					$hasAutoSignup = true;
					break;
				}
			}
			if($hasAutoSignup) {
				$out .= "You will be added to the list automatically once it opens.<br><br>Please arrive early so you do not miss your turn.";
			}
			else {
				$out .= "<em>A ticket does not guarantee your spot on the list!</em><br><br>Please arrive early to sign-up on the list if you have a tune to share.";
			}
			if($count > 1) {
				$out .= "<br><em>Additional tickets must be signed up in person at the event.</em>";
			}
			$out .= "<br>";
		}
		$out .= "</div>";
		
		return $out;
	}
	
	public function getUserTicketsArray($user_id) {
		Loader::model('user_ticket');
		Loader::model('facebook_event');
		Loader::model('on_deck');
		
		$openDecks		= OnDeck::getAllLiveOrOpenDecks();
		$countLive		= 0;
		$countRsvp 		= 0;
		$countUnused	= 0;
		$countUsed 		= 0;
		
		$rsvpTickets	= UserTicket::getAllRSVPTicketsForUser($user_id, null, $order);
		$unusedTickets 	= UserTicket::getAllAvailableTicketsForUser($user_id, null, $order);
		$usedTickets 	= UserTicket::getAllUsedTicketsForUser($user_id, null, $order);
		
		$liveTickets = null;
		 
		if($openDecks) {
			if($usedTickets) {
			// Find any tickets for events currently live
				$i = 0;
				foreach($usedTickets as $t) {
					foreach($openDecks as $d) {
						if($d->EventID && $t->event_id && $d->EventID == $t->event_id) {
							if(!$liveTickets) $liveTickets = array();
							$liveTickets[] = $t;
							unset($usedTickets[$i]);
						}
					}
					$i++;
				}
			}
			if($rsvpTickets) {
			// Find any tickets for events currently live
				$i = 0;
				//echo "RSVP:".$i."<br>";
				foreach($rsvpTickets as $t) {
					foreach($openDecks as $d) {
						if($d->EventID && $t->event_id && $d->EventID == $t->event_id) {
							if(!$liveTickets) $liveTickets = array();
							$liveTickets[] = $t;
							unset($rsvpTickets[$i]);
						}
					}
					$i++;
				}
			}
		}
		if($rsvpTickets) {
		// Check that the event hasn't already passed, and if so mark the ticket as used
			$i = 0;
			//echo "RSVP:".$i."<br>";
			foreach($rsvpTickets as $t) {
				if($t->event_id) {
					$deck = OnDeck::getByEventID($t->event_id);
					if($deck && $deck->isEnded()) {
						$t->used = date("Y-m-d H:i:s", $start);
						UserTicket::save($t);
						if(!$usedTickets) $usedTickets = array();
						$usedTickets[] = $t;
						unset($rsvpTickets[$i]);
					}
					else {
						$event = FacebookEvent::getID($t->event_id);
						$start = strtotime($event->StartTime);
						if($start < time()) {
							$t->used = date("Y-m-d H:i:s", $start);
							UserTicket::save($t);
							if(!$usedTickets) $usedTickets = array();
							$usedTickets[] = $t;
							unset($rsvpTickets[$i]);
						}
					}
				}
				
				$i++;
			}
		}
		
		$data = array();
		$data['live'] 			= $liveTickets;
		$data['rsvp'] 			= $rsvpTickets;
		$data['used'] 			= $usedTickets;
		$data['unused'] 		= $unusedTickets;
		$data['liveCount'] 		= count($liveTickets);
		$data['rsvpCount'] 		= count($rsvpTickets);
		$data['unusedCount'] 	= count($unusedTickets);
		$data['usedCount'] 		= count($usedTickets);
		$data['count'] 			= $data['liveCount'] + $data['rsvpCount'] + $data['unusedCount'] + $data['usedCount'];
		
		return $data;
	}
	
	public function displayMemberTicketStatus($showUsed=false, $asButton=true, $class='ticket_status', $style='right button medium white') {
		echo "<div class='".$class."'>";
		Loader::model('user_ticket');
		
		$user = Loader::helper('user');
		$user->setup();
		
		if($user->isLoggedIn()) {
			$tickets = $this->getUserTicketsArray($user->id);
			if($tickets['count'] > 0) {
				if($asButton) {
					echo "<a class='".$style."' href='/members/tickets'>";
				}
				else {
					echo "<div class='".$style."'>";
				}
				echo "Tickets: ";
				echo "<i class='fa fa-ticket' aria-hidden='true'></i>".$tickets['unusedCount']." &nbsp; ";
				echo "<i class='fa fa-calendar-check-o' aria-hidden='true'></i>".($tickets['rsvpCount']+$tickets['liveCount']);
				if($showUsed) {
					echo " &nbsp; <i class='fa fa-times' aria-hidden='true'></i>".$tickets['usedCount'];
				}
				if($asButton) {
					echo "</a>";
				}
				else {
					echo "</div>";	
				}
			}
			else
			if(!$user->sub) {
				echo "<a class='right button medium white' href='/membership'><i class='fa fa-id-card-o' aria-hidden='true'></i> Become a Member</a>";
			}
			else 
			if(!$user->sub->isActive() || !$tickets['count']) {
				echo "<a class='right button medium white' href='/membership'><i class='fa fa-sign-in' aria-hidden='true'></i> Renew Your Membership</a>";
			}
		}
		else {
			echo "<a class='right button medium white' href='/login'><i class='fa fa-sign-in' aria-hidden='true'></i> Log In</a>";
		}
		echo "</div>";
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: displayPurchases
	//-----------------------------------------------------------------------------------------------------------------------------
	public function displayPurchases($fromTime=null) {
		Loader::model('user_profile');
		Loader::model('user_purchase');
		Loader::model('facebook_event');
		Loader::model('locations');
		
		$user = Loader::helper('user');
		if(!$user->isLoggedIn()) {
			echo "<div class='alert alert-white'><h1>You are logged out</h1>Please <a href='".View::url("/login?redirect=/purchase_complete")."'>log in</a> to view this page.</div>";
		}
		else {
			if(!$fromTime) $fromTime = time() - (60*60*24); // Past 24 hours
			$fromDate = date("Y-m-d H:i:s", $fromTime);
			
			$tickets = UserPurchase::getTicketsForUser($user->id, null, "uDate>='".$fromDate."'");
			if(!$tickets || count($tickets) < 1) {
				echo "<div class='alert alert-danger'><h1>Oh no...</h1>We did not find any confirmed purchases for your account. Please <a href='".View::url("/about/contact-us")."'>contact us</a> for assistance.</div>";
			}
			else {
				foreach($tickets as $t) {
					$link = null;
					if($t->uEventID) {
						$link = View::url("/eventinfo?id=".$t->uEventID);
					}
					echo "<div class='alert alert-success'>";
					if($link) echo "<a href='".$link."'>";
					
					echo "<h3>".$t->uItem."</h3>";
					echo "<table class='purchase_complete_table'>";
					echo "<tr>";
					echo "<th class='purchase_complete_date'>";
					echo "Date";
					echo "</th>";
					echo "<th class='purchase_complete_status'>";
					echo "Status";
					echo "</th>";
					echo "<th class='purchase_complete_transid'>";
					echo "Transaction ID";
					echo "</th>";
					echo "</tr>";
					
					echo "<tr>";
					echo "<td class='purchase_complete_date'>";
					echo date("M d, Y g:ia", strtotime($t->uDate));
					echo "</td>";
					echo "<td class='purchase_complete_status'>";
					echo $t->uStatus;
					echo "</td>";
					echo "<td class='purchase_complete_transid'>";
					echo $t->uTransactionID;
					echo "</td>";
					echo "</tr>";
					
					echo "</table>";
					if($link) echo "</a>";
					echo "</div>";
				}
			}
		}
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: buyTickets
	//-----------------------------------------------------------------------------------------------------------------------------
	public function buyTickets($event) {
		Loader::model('user_profile');
		Loader::model('user_purchase');
		Loader::model('user_subscription');
		Loader::model('user_on_deck');
		Loader::model('user_on_deck_entry');
		Loader::model('user_ticket');
		Loader::model('facebook_event');
		Loader::model('locations');
	
		$user = Loader::helper('user');
		$location = Locations::getID($event->Location);
		
		$end = strtotime($event->EndTime);
		if($date > $end) {
			echo "<div class='alert alert-white'><h2>Event Ended</h2>Tickets for this event are no longer available.</div>";
			return;
		}
		if(!$user->isMember() && $event->TicketMode == "free") {
			echo "<div class='alert alert-white'><h2>Free Event!</h2>There is no cover charge.</div>";
			return;
		}
		if(!$user->isMember() && ($event->TicketMode == "door" || !$location->PaypalEmail || !$event->TicketPrice)) {
			echo "<div class='alert alert-white'><i class='fa fa-ticket superhuge center' aria-hidden='true'></i><h2>Buy Tickets</h2>Please see the event details for ticket information.</div>";
			return;
		}
		// TODO: Provide admin controls to set ticket price and options
		
		$user 		= Loader::helper('user');
		$form 		= Loader::helper('form');
		$paypal 	= Loader::helper('paypal');
		$stripe 	= Loader::helper('stripe');
	
		$paypal->SandboxMode = false;
		$paypal->SellerEmail = $location->PaypalEmail;
	
		$name 				= null;
		$email 				= null;
		$ondeck				= null;
		$soldOut 			= false;
		$expired 			= false;
		$remainingTickets 	= 0;
		
		if(ENABLE_ONDECK) {
			$ondeck = OnDeck::getForEvent($event);
		}
		
		$time = strtotime($event->StartTime);
		$time += 60 * 60 * 3; // Add 3 hours as a grace period
		if($time < time()) {
			//echo "EXPIRED:".$event->StartTime;
			$expired = true;	
		}
		if($ondeck) {
			if($ondeck->isEnded()) {
				//echo "ENDED:".$ondeck->EndTime;
				$expired = true;
			}
		}
		
		if($expired) {
			echo "<h3>No Tickets Available</h3>";
			echo "<div class='alert alert-info'>Tickets are no longer being sold online for this event.</div>";
			if($ondeck) {
				echo "<a class='button' href='".View::url("ondeck?odid=".$ondeck->ID)."'>View On Deck Session <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
			}
		}
		else {
			if($event->TicketLimit > 0) {
				$sold = UserPurchase::countTicketsForEvent($event->ID);
				if($sold >= $event->TicketLimit) {
					$soldOut = true;
				}
				else {
					$remainingTickets = $event->TicketLimit - $sold;
				}
				if($soldOut) {
					echo "<div class='alert alert-success center'><h1>Sold Out!</h1></div>";
				}
				else
				if($remainingTickets < 10) {
					echo "<div class='alert alert-success center'><h3>Only ".$remainingTickets." Tickets Left!</h3></div>";
				}
				else {
					echo "<div class='alert alert-success center'><h4>".$remainingTickets." Tickets Available</h4></div>";
				}
			}
			
			$eventUrl = "eventinfo?id=".$event->ID;
						
			if(!$user->isLoggedIn()) {
				$loginurl = View::url("/login?redirect=".$url);
				echo "<div class='center'>";
				echo "<h3>Purchase Tickets</h3>";
				echo "<div class='alert alert-white'><i class='fa fa-sign-in' aria-hidden='true'></i> To purchase tickets, <a href='".$loginurl."'>please log in</a> or <a href='".View::url("/register?redirect=eventinfo?id=".$event->ID)."'>register for free</a>.</div>";
				echo "<a class='button' href='".$loginurl."'>Login or Register <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
				echo "</div>";
			}
			else {
				if($user->sub && $user->sub->isActive()) {
					$unusedTickets = UserTicket::getAllAvailableTicketsForUser($user->id, $event->ID, null, "ORDER BY id ASC");
					if(!$unusedTickets || !count($unusedTickets)) {
						echo "<div class='alert alert-white small'>You have used all of the tickets in your subscription. ";
						echo "<a class='button white medium' style='margin-top:10px;' href='/members/tickets'><i class='fa fa-cog' aria-hidden='true'></i> Manage Membership</a>";
						echo "</div>";
					}
				}
				$ticketsCount = 0;
				$tickets = UserTicket::getAllTicketsForUserAtEvent($user->id, $event->ID, null, "ORDER BY id ASC");
				if($tickets) {
					foreach($tickets as $t) {
						$ticketsCount++; 
					}
				}
				
				if(!$soldOut) {
					if(!$ticketsCount || $_REQUEST['buymore'] || $_REQUEST['buydirect']) {
						//echo $user->id;
						if($user->profile) {
							$name = $user->profile->uArtistName;
							$email = $user->info->uEmail;
					
							$discounted = false;
							$originalPrice = $event->TicketPrice;
							if($user->profile->isLocationAdmin($event->Location) && $event->TicketPrice) {
								$discounted = true;
								$event->TicketPrice = 1;
							}
							if($user->sub) {
								if($user->sub->isActive()) {
									//echo "<div class='alert alert-success'>Active Member</div>";
								}
							}
							
							$purchaseDirect = $_REQUEST['buydirect']; // could be set to true to buy tickets for event directly instead of using existing tickets
							$canUseMemberTickets = true; // could be set to false for special events or workshops
							
							$select = "<select id='ticket_quantity' name='ticket_quantity' class='center nopadding' onchange='ps.events.updatePurchaseTotal();'>";
							$select .= "<option value='1' selected='selected'>1</option>";
							
							$tickets = $this->getUserTicketsArray($user->id);
							
							if(!$purchaseDirect && $canUseMemberTickets && $tickets['unusedCount']) {
								echo "<div id='event_ticket_purchase'>";
								echo "<div class='alert alert-info center'>";
								echo "<a class='medium center' href='/members/tickets'><i class='fa fa-ticket' aria-hidden='true'></i> You have ".$tickets['unusedCount']." unused tickets</a>";
								echo "<h2 class='center'>RSVP</h2>";
								
								$max = 10;
								if($tickets['unusedCount'] < $max) $max = $tickets['unusedCount'];
								for($i = 2; $i <= $max; $i++) {
									$select .= "<option value='".$i."'>".$i."</option>";
								}
								$select .= "</select>";
								
								echo "<div class='center nopadding'>".$select."</div>";
								
								echo "<br class='clear'>";
								
								echo "<a id='event_ticket_rsvp_button' class='button' href='javascript:;' onclick='return ps.events.rsvp(\"".$event->ID."\");'>RSVP Using 1 Ticket</a>";
								echo "</div>";
		
								echo "</div>";
								echo "<br class='clear'><a class='small center clear pad-top' href='".$eventUrl."&buydirect=true'>Buy tickets for this event</a>";
							}
							else {
								if(!$user->sub || !$user->sub->isActive()) {
									echo "<a href='/membership' class='button center'>Membership Options <i class='fa fa-id-card-o' aria-hidden='true'></i></a>";
									echo "<br class='clear'>";
									echo "<p style='margin-top:10px'>Become a Premium Member to save money and get exclusive membership benefits!</p>";
								}
								echo "<div id='event_ticket_purchase'>";
								echo "<div class='alert alert-info'>";
								echo "<div class='center bold'>".$event->Name."</div>";
								echo "<div class='center medium bold'>".date("D M j", strtotime($event->StartTime))." @".date("g:ia", strtotime($event->StartTime))."</div>";
								echo "<hr>";
								echo "<h2 class='center'>Purchase Tickets</h2>";	
							
								$select .= "<option value='2'>2</option>";
								$select .= "<option value='3'>3</option>";
								$select .= "<option value='4'>4</option>";
								$select .= "<option value='5'>5</option>";
								$select .= "<option value='6'>6</option>";
								$select .= "<option value='7'>7</option>";
								$select .= "<option value='8'>8</option>";
								$select .= "<option value='9'>9</option>";
								$select .= "<option value='10'>10</option>";
								$select .= "</select>";
								echo "<div class='center nopadding'>".$select."</div>";
								
								echo "<input type='hidden' id='ticket_event_id' value='".$event->ID."'>"; 
								echo "<input type='hidden' id='ticket_price' value='".$event->TicketPrice."'>"; 
								echo "<div id='paypal_error'></div>";
								
								if($user->profile->isLocationAdmin($event->Location) && $event->TicketPrice) {
									echo "<div class='small'>Admin Discount</div>";	
								}
								echo "<div id='total_price'>$".number_format($event->TicketPrice, 2)."</div>";
								echo $stripe->purchaseButton("ticket-".$event->ID, "Buy 1 Ticket", round($event->TicketPrice * 100), 1, $eventUrl, "event_ticket_purchase", "button center");	
								//echo "<br class='clear'>";
								echo $paypal->Button("Purchase", $event->Location." Producers Social", $event->ID, $event->TicketPrice, 1, $user->id, "ps.events.purchaseTicket();");
								echo "<div class='small clear' style='margin-top:10px;'>*Tickets valid for this event only. Non-transferrable.</div>";
								/*
			 
								echo "<div id='event_ticket_purchase'>";
						
									
									
									if($discounted) {
										echo "<br><div class='small'>Price discounted for admin.<br>Regular price is $".$originalPrice."</div>";
									}
									echo "</div>";
								echo "</div>";
					
								if($sandboxMode) echo "<div class='admin_area center'>[Sandbox Mode]</div>"; 
								echo "<br class='clear'>";
								*/
								echo "</div>";
								echo "</div>";
							}
				
							//echo "<br class='clear'>";
							echo "<br class='clear'><div class='center'><a href='javascript:;' onclick='ps.showHelp();'><i class='fa fa-question-circle-o' aria-hidden='true'></i> Have Questions</a></div>";
						}
					}
					else {
						echo "<a class='small center clear' href='".$eventUrl."&buymore=true'>Get more tickets</a>";
					}
				}
				if($tickets) {
					if($ticketsCount) {
						$verb = "have";
						$alertType = "info";
						if($ondeck && $ondeck->isEnded()) {
							$alertType = "gray";
							$verb = "used";
						}
						$reserved = null;
						if($user->isMember()) {
							$reserved = " reserved";	
						}
		
						echo "<div class='alert alert-".$alertType." large center clear pad-top'>";
						echo "<i class='fa fa-ticket center-large' aria-hidden='true'></i>";
						echo "<br><a href='/members/tickets'>You ".$verb." ".$ticketsCount." ticket".($ticketsCount > 1 ? "s":"").$reserved."!</a>";
						if(ENABLE_ONDECK) {
							$startTime = date("g:ia", strtotime($event->StartTime));
							if($ondeck->IsOpen) {
								//echo "<div class='center bold'>Open for sign-ups now</div>";
								echo "<br class='clear'><br>";
								$userOnDeck = UserOnDeck::getByUserID($ondeck->ID, $user->id);
								if($userOnDeck) {
									echo "<a class='button medium blue' href='".View::url("ondeck?odid=".$ondeck->ID)."'><i class='fa fa-check-square-o' aria-hidden='true'></i> You are #".$userOnDeck->OrderNum." On Deck</a>";
								}
								else {
									echo "<a class='button' href='".View::url("ondeck?odid=".$ondeck->ID."&cmd_add_me=1")."'><i class='fa fa-plus' aria-hidden='true'></i> Add Me On Deck</a>";
								}
								if($ticketsCount > 1) {
									echo "<div class='center nopadding small'><br><em>*Additional tickets must be signed-up in person.</em><br></div>";
								}
							}
							else {
								if($ondeck->isStarted()) {
									echo "<div class='center medium'>Online sign-ups have ended.<br class='clear'>";
									echo "<a href='".View::url("ondeck?odid=".$ondeck->ID)."'>View Session</a></div>";
								}
								else {
									echo "<div class='center bold'>Sign-ups begin at ".$startTime."</div>";
									if($user->sub && $user->sub->isActive()) {
										echo "<div class='alert alert-info' style='background:rgba(255,255,255,0.4);'>";
										if($tickets[0]->autosignup) {
											$icon = "<i class='fa fa-check-square-o' aria-hidden='true'></i>";
											echo "<div id='automatic_signup_title' class='center nopadding bold'>Automatic Sign-Up On!</div>";
										}
										else {
											$icon = "<i class='fa fa-square-o' aria-hidden='true'></i>";
											echo "<div id='automatic_signup_title' class='center nopadding bold'>Automatic Sign-Up?</div>";
										}
										echo "<input type='hidden' id='autosignup' name='autosignup' value='".$tickets[0]->autosignup."'>";
										echo "<div class='center nopadding'><a id='auto_signup_toggle' style='font-size:30px' href='javascript:;' ";
										echo "onclick='return ps.events.autoSignupToggle(\"".$tickets[0]->id."\");'>".$icon."</a></div>";
										echo "<div class='center nopadding small bold'><em>1 spot only per member.</em></div>";
										echo "<div class='center nopadding small'><br><em>*Additional tickets must be signed up in person.</em><br></div>";
										echo "</div>";
									}
									else {
										echo "<p class='small light'>A spot on the list is <i>not</i> guaranteed. Check back here at the time of the event, or sign-up in person at the event.</p>";
									}
								}
								//echo "<br class='clear'><br><p>To share a tune, either <a href='".View::url("ondeck?odid=".$ondeck->ID)."'>sign-up here</a> or in person at the event.<p>";
							}
						}
						else {
							echo "<br class='clear'><br><p>To share a tune please arrive early to sign-up. The sign-up list is first-come, first-serve.<p>";
						}
						
						if($ondeck && !$ondeck->isStarted()) {
							echo "<b class='clear'>Please arrive early. </b>";
							if($user->sub && $user->sub->isActive()) {
								echo "<div class='center nopadding'><a id='unrsvp' class='small italic' href='javascript:;' ";
								echo "onclick='return ps.events.unrsvp(\"".$event->ID."\");'>Remove My RSVP</a></div>";
							}
						}
						echo "</div>";
					}
					echo "<br class='clear'>";
				}
			}
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showPresales
	//-----------------------------------------------------------------------------------------------------------------------------
	public function showPresales($event, $showOndeck=true) {
		Loader::model('facebook_event');
		Loader::model('user_profile');
		Loader::model('user_subscription');
		Loader::model('user_ticket');
		Loader::model('on_deck');

		$events = Loader::helper('events');
		$ondeck = OnDeck::getID($event->OnDeckID);
	
		if(!$event || !$ondeck) {
			echo "<div class='alert alert-white'>No On Deck sessions have been started yet</div>";	
			return;
		}
		$location = "HQ";
		//$eventid = null;
		//if(isset($_REQUEST['event'])) $eventid = $_REQUEST['event'];
	
		//$event = null;
		//if($eventid) {
			//$event = FacebookEvent::getID($eventid);
			$location = $event->Location;
		//}

		$u = new User();
		if(!$u || !$u->isLoggedIn()) {
			echo "<div class='alert alert-white'>This is a restricted page.</div>";
			echo "<a class='button' href='".View::url("/login")."'>Login or Register</a>"; 
		}
		else {
			$p = UserProfile::getByUserID($user->id);
			if($p && $p->isLocationAdmin()) {
				
				$tickets = UserTicket::getAllTicketsForEvent($event->ID);
	
				$count = count($tickets);
				if(!$count) {
					echo "<div class='alert alert-info'><h3>No tickets have been sold for this event yet</h3></div>";
				}
				else {
					Loader::helper('ajax');

					$list = null;
					echo "<h3 class='center'>".$count." Ticket".($count == 1 ? "" : "s")." Sold or Reserved</h3>";
					$i = 1;
					
					$usersDisplayed = array();
					
					foreach($tickets as $t) {
						if(!isset($usersDisplayed[$t->user_id])) {
							$usersDisplayed[$t->user_id] = 1;
							$up = UserProfile::getByUserID($t->user_id);
							$ui = UserInfo::getByID($t->user_id);
							$sub = UserSubscription::getByUserID($t->user_id);
							$name = $up->uArtistName;
							if(!$name) $name = $ui->getUserName();
							$listItem = $name." &lt;".$ui->getUserEmail()."&gt;";
							
							$userTickets = UserTicket::getAllTicketsForUserAtEvent($t->user_id, $event->ID);
							$ticketCount = count($userTickets);
							$img = $up ? $up->getAvatar(false) : UserProfile::getDefultAvatar(false);
	
							$uod = $ondeck->getUser($t->user_id);
							//$entry = "<div class='ondeck_user_add'>";
							if($uod->ID || !$showOndeck) {
								$entry = "<div class='ondeck_user_add_presale ".($showOndeck ? "" : "admin")."'>";
							}
							else {
								$entry = "<a class='ondeck_user_add_presale' ".ajax('users', 'cmd_add_user', $t->user_id).">";
							}
							$entry .= "	<div class='ondeck_user_add_location' style='background:#".$locations[$u->Location]->Color."'>".$u->Location."</div>";
							if($ticketCount) {
								$ticketClass = $sub && $sub->isActive() ? "ondeck_user_add_tickets_member" : "ondeck_user_add_tickets";
								$entry .= "<div class='".$ticketClass."'>";
								for($x = 0; $x < $ticketCount; $x++) {
									$entry .= "<i class='fa fa-ticket left' aria-hidden='true'></i>";
								}
								$entry .= "</div>";
							}
							$entry .= "	<img class='ondeck_user_add_img clear' src='".$img."'>";
							$entry .= "	<div class='ondeck_user_add_name'>".($up->uArtistName ? $up->uArtistName : $u->Name)."</div>";
							if($uod->ID || !$showOndeck) {
								if($showOndeck) {
									$entry .= "<div class='small center'>#".$uod->OrderNum." On List</div>";
								}
								$entry .= "</div>";
							}
							else {
								$entry .= "<div class='button small center'>Add <i class='fa fa-plus' aria-hidden='true'></i></div>";
								$entry .= "</a>";
							}
							echo $entry;
							
							
							if($list) $list .= ", ";
							$list .= $listItem;
							$i++;
						}
					}
				}
				
				// Display active members with available tickets
				
			}
		}
		echo "<br class='clear'>";
	}

}
?>

