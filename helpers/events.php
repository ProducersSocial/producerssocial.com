<?php
defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//:: EventsHelper
//-----------------------------------------------------------------------------------------------------------------------------
class EventsHelper {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: __construct
	//-----------------------------------------------------------------------------------------------------------------------------
	public function __construct() {}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: eventStatus
	//-----------------------------------------------------------------------------------------------------------------------------
	public function eventStatus($event, $echo=true) {
		$box = null;
		if(ENABLE_ONDECK) {
			if(!$event || !$event->ID) {
				$box .= "<div class='alert alert-danger'>No event or event id was provided.</div>";
			}
			else {
				Loader::model('on_deck');
				$navigation = Loader::helper('navigation');
				
				$user = Loader::helper('user');
		
				$ondeck = OnDeck::getForEvent($event);
				if($ondeck->IsEnabled) {
					$status = "";
					if(!$ondeck->isStarted() && !$ondeck->IsOpen) {
						$status = "notopenyet";
					}
					else
					if($ondeck->isEnded()) {
						$status = "ended";
					}
					else
					if($ondeck->IsOpen) {
						$status = "open";
					}
					else {
						$status = "closed";
					}
					$start = strtotime($event->StartTime);
					$end = strtotime($event->EndTime);
			
					$box = "<div class='ondeck_event_status ".$status."'>";
					if($ondeck->IsPrivate) {
						$box .= "<div class='center small error'>PRIVATE</div>";
					}
					$link = "#";
					if($ondeck->isStarted() || $ondeck->IsOpen || $user->isLocationAdmin($event->Location)) {
						$link = BASE_URL.View::url("/ondeck?odid=".$ondeck->ID);
						if(!$user->isLoggedIn()) {
							$link = BASE_URL.View::url("/login?redirect=".$link);
						}
					}

// 					if($ondeck->isEnded()) {
// 						$box .= "<div class='ondeck_list_status_top'>Session Ended</div>";
// 					}
// 					else
// 					if(!$ondeck->IsOpen) {
// 						$box .= "<div class='ondeck_list_status_top'>List is Closed</div>";
// 					}
		
					if($start > time() || $ondeck->isStarted()) {
						$box .= "<a class='ondeck_list_status ".$status."' href='".$link."'>";
						if($ondeck->IsOpen) {
							$box .= "Open for Sign-up <i class='fa fa-sign-in' aria-hidden='true'></i>";
						}
						else {
							if(!$ondeck->isStarted()) {
								$box .= "Not Open Yet <i class='fa fa-hourglass-half' aria-hidden='true'></i>";
							}
							else
							if($ondeck->isEnded()) {
								$box .= "Session Ended <i class='fa fa-television' aria-hidden='true'></i>";
							}
							else {
								$box .= "Now In Session <i class='fa fa-television' aria-hidden='true'></i>";
							}
						}
						$box .= "</a>";
					}
					if(!$ondeck->IsOpen) {
						$box .= "<i class='ondeck_list_status_sub'>The list is closed</i>";
					}
			
					$liveStatus = null;
					if($ondeck->IsLive) {
						$liveStatus .= "<i class='fa fa-television' aria-hidden='true'></i>&nbsp; ";
						$liveStatus .= "In Session Now until ".date("g:ia", $end);
					}
					else {
						if($ondeck->isEnded()) {
							$liveStatus .= "<i class='fa fa-circle-thin' aria-hidden='true'></i>&nbsp; ";
							$liveStatus .= "Ended at ".date("g:ia", $end);
						}
						else
						if($start > time()) {
							$bliveStatusox .= "<i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp; ";
							$liveStatus .= "Opens at ".date("g:ia", $start);
						}
						else {
							$bliveStatusox .= "<i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp; ";
							$liveStatus .= "Opened at ".date("g:ia", $start);
						}
					}
					if($liveStatus) {
						$box .= "<div class='ondeck_live_status'>";
						$box .= $liveStatus;
						$box .= "</div>";
					}
		
					if(!$ondeck->IsOpen) {
						if($status != "ended" && UserProfile::isLocationAdmin($event->Location)) {
							$box .= "<br class='clear'><a href='".BASE_URL.View::url("/ondeck?odid=".$ondeck->ID."&cmd_open_deck=1")."' class='button center'><i class='fa fa-lock' aria-hidden='true'></i> Open the Session</a>";
							$box .= "<br class='clear'><br><a href='".BASE_URL.View::url("/ondeck?odid=".$ondeck->ID)."' class='button white clear center'><i class='fa fa-television' aria-hidden='true'></i> View Session</a>";
						}
					}
					$box .= "</div>";
				}
				else
				if($user->isLocationAdmin($event->Location)) {
					$openurl = BASE_URL.View::url("/ondeck?cmd_enable_deck=1&odid=".$ondeck->ID);
					$box .= "<a class='ondeck_list_status' href='".$openurl."'>";
					$box .= "START NEW DECK";
					$box .= "</a>";
				}
			}
		}
		if($echo) echo $box;
		return $box;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: sendEventFollowups
	//-----------------------------------------------------------------------------------------------------------------------------
	public function sendEventFollowups() {
		Loader::model('on_deck');
		Loader::model('member_feature');
		Loader::model('user_profile');
		
		$testmode = false;
		
		$out = "";
		$now = time();//strtotime("2017-03-09 00:00:00");//
		$prev = date("Y-m-d H:i:s", $now - (96 * 60 * 60));
		$start = date("Y-m-d H:i:s", $now - (72 * 60 * 60));
		$end = date("Y-m-d H:i:s", $now);

		// Find any events past the 72 hour window that don't have a blog post and create one.
		$q = "WHERE HostedBy != 0 AND IsOpen = 0 AND IsEnabled = 1 AND BlogPost=0  AND EventDate >= '".$prev."' AND EventDate <= '".$start."'";
		$events = OnDeck::getAll($q);
		if($events && count($events)) {
			foreach($events as $e) {
				$item = new MemberFeature();
				$item->generateOnDeckBlog();
				$item = MemberFeature::create($item);
				
				$e->BlogPost = $item->id;
				OnDeck::save($e);
			}
		}
		
		
		$q = "WHERE HostedBy != 0 AND IsOpen = 0 AND IsEnabled = 1 AND (HostFollowup = 0 OR MembersFollowup = 0) AND EventDate >= '".$start."' AND EventDate <= '".$end."'";
		$events = OnDeck::getAll($q);
		
		$out .= "<div class='alert alert-white'>";
		$out .= "<h2>Host Follow Up Email</h2>";
		if(!$events || !count($events)) {		
			$out .= "<h4>No events found</h4><br>".$q;
		}
		else {
			$c = count($events);
			$out .= "<h4>".$c." event".($c == 1 ? "" : "s")." found</h4><br>".$q;
			foreach($events as $e) {
				$out .= "<h4>".$e->ID.": ".$e->EventDate." - ".$e->Title."</h4>";	
				
				if(!$e->BlogPost && !$e->HostFollowup) {
				// Send follow up to host asking them to publish the on deck blog post	
					$p = UserProfile::getByUserID($e->HostedBy);
					$info = UserInfo::getByID($e->HostedBy);
					$out .= $p->uArtistName." : ".$info->uEmail."<br>";
					
					$email = $testmode ? "steve@walkerfx.com" : $info->uEmail;
					$mh = Loader::helper('mail');
					$mh->from("admin@producerssocial.com");
					$mh->bcc("stephen@walkerfx.com");
					$mh->to($email);
					
					$name = $p->uArtistName;
					if(!$name) $name = $info->uName;
					
					$mh->addParameter('memberName', $name);
					$mh->addParameter('ondeck', $e);
					
					$locationName = Locations::getName($e->Location);
					$mh->addParameter('location', $e->Location);
					$mh->addParameter('locationName', $locationName);
					$mh->addParameter('subject', "Event Followup");
					$mh->load("ambassador_followup");
					$out .= $mh->getBodyHTML();
					$mh->sendMail();
					
					$e->HostFollowup = 1;
					OnDeck::save($e);
				}
			}
		}
		$out .= "</div>";
		
		$q = "WHERE HostedBy != 0 AND IsOpen = 0 AND IsEnabled = 1 AND (HostFollowup = 0 OR MembersFollowup = 0) AND EventDate >= '".$prev."' AND EventDate <= '".$end."'";
		$events = OnDeck::getAll($q);
		
		$out .= "<div class='alert alert-white'>";
		$out .= "<h2>Users On Deck - Follow Up Email</h2>";
		if(!$events || !count($events)) {
			$out .= "<h4>No events found</h4><br>".$q;
		}
		else {
			foreach($events as $e) {
				if($e->BlogPost && !$e->MembersFollowup) {
				// Send follow up email to all members on deck
					$mh = Loader::helper('mail');
					$blog = MemberFeature::getByID($e->BlogPost);
					if(!$blog) {
						$out .= "ERROR: Failed loading blog post:". $e->BlogPost;	
					}
					else
					if($blog->status != 'active' || strtotime($blog->published) > $now) {
						$out .= "The blog is not published yet.";	
					}
					else {
						$usersOnDeck = $e->getUsersOnDeck();
						$count = 0;
						foreach($usersOnDeck['all'] as $u) {
							$p = UserProfile::getByUserID($u->UserID);
							if($p && $p->uSendReminders) {
								$out .= $u->Name." : ".$u->Email."<br>";
								
								$mh->reset();
								$mh->from("admin@producerssocial.com");
								$mh->bcc("stephen@walkerfx.com");
								
								$email = $testmode ? "steve@walkerfx.com" : $u->Email;
								$mh->to($email);
								
								$mh->addParameter('memberName', $u->Name);
								$mh->addParameter('ondeck', $e);
								
								$locationName = Locations::getName($e->Location);
								$mh->addParameter('location', $e->Location);
								$mh->addParameter('locationName', $locationName);
								$mh->addParameter('feature_id', $e->BlogPost);
								$mh->addParameter('subject', "You've been featured!");
								$mh->load("event_followup");
								//$out .= $mh->getBodyHTML();
								$mh->sendMail();
								$count++;
							}
						}
					
						$e->MembersFollowup = 1;
						OnDeck::save($e);
						
						$slack = Loader::helper("slack");
						$slack->send($count." Event Followup Emails Sent\n".$e->Title);
					}
				}
				else {
					$out .= "All followup emails already sent.";	
				}
			}
		}
		$out .= "</div>";
		return $out;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: eventBox
	//-----------------------------------------------------------------------------------------------------------------------------
	public function eventBox($ondeck) {
		Loader::model('on_deck');
		Loader::model('facebook_event');
		
		$box = null;
		//if($ondeck->IsEnabled) {
			if(!$ondeck || !$ondeck->ID) {
				$box .= "<div class='alert alert-danger'>No event or event id was provided.</div>";
			}
			else {
				$navigation = Loader::helper('navigation');
		
				$event = FacebookEvent::getID($ondeck->EventID);
			
				$u = new User();
				if(!$u || !$u->isLoggedIn()) $u = null;
	
				$box = "<div class='ondeck_event_box'>";
				if($ondeck->IsPrivate) {
					$box .= "<div class='center small error'>PRIVATE</div>";
				}
				$link = "#";
// 				if(!ENABLE_ONDECK || !$ondeck->IsEnabled) {
// 					if($ondeck->EventID) {
// 						$link = BASE_URL.View::url("/eventinfo?id=".$ondeck->EventID);
// 					}
// 				}
// 				else
				if(!$u) {
					$link = "/login?redirect=ondeck?odid=".$ondeck->ID;
				}
				else
				if(UserProfile::isLocationAdmin($event->Location) || ($ondeck->IsOpen || $ondeck->IsLive)) {
					$link = "/ondeck?odid=".$ondeck->ID;
				}
				else
				if($ondeck->EventID) {
					$link = "/eventinfo?id=".$ondeck->EventID;
				}
		
				$imgSrc = null;
				if($event && $event->ImageURL) {
					$imgSrc = str_replace("http:", "https:", $event->ImageURL);
				}
				if(!$imgSrc) {
					$imgSrc = "/images/features/".strtolower($ondeck->Location)."_feature.jpg";
				}
				$box .= "<a href='".$link."'>";//class='".strtolower($ondeck->Location)."_256' 
				$box .= "<div class='ondeck_event_title'>".$ondeck->Title."</div>";
				$box .= "<img class='ondeck_event_box_img' class='responsive' src='".$imgSrc."'>";
				$box .= "</a>";
			
				if(ENABLE_ONDECK || !$ondeck->IsEnabled) {
					$box .= "<a class='button clear' style='width:100%;' href='".$link."'>".$ondeck->getStatus()."</a>";
				}
				if(!$ondeck->IsOpen) {
					if(UserProfile::isLocationAdmin($event->Location)) {
						$box .= "<a href='"."/ondeck?odid=".$ondeck->ID."&cmd_open_deck=1"."' class='button white full'>Open the Session</a>";
					}
				}
				$box .= "</div>";
			}
		//}
		//if($echo) echo $box;
		return $box;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getEvents
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getEvents($fromDate=null, $toDate=null, $limit=null, $order="ASC", $returnNull=true, $location=null, $query=null, $showPrivate=false) {
		Loader::model('facebook_event');
		Loader::model('user_profile');
		Loader::model('locations');
		Loader::model('on_deck');
		
		$anyFound = false;
		
		if(!$fromDate) $fromDate = date("Y-m-d 00:00:00", time());
		if($showPrivate) {
			$q = "LEFT JOIN OnDeck AS od ON od.ID=OnDeckID WHERE (fb.StartTime>='".$fromDate."'";
		}
		else {
			$q = " WHERE (fb.StartTime>='".$fromDate."'";
		}
		if($toDate) {
			$q .= " AND fb.StartTime<='".$toDate."'";
		}
		if($location) {
			$q .= " AND fb.Location='".$location."'";	
		}
		$q .= ")";
		if($query) {
			$q .= " AND (".$query.")";	
		}
		if($showPrivate) {
			$q .= " AND IsPrivate=0";
			$q .= " ORDER BY od.IsOpen DESC, fb.StartTime ".$order;
		}
		else {
			$q .= " ORDER BY fb.StartTime ".$order;
		}
		if($limit) $q .= " ".$limit;
		
		//if(ADMIN) echo $q."<br>";
		
		return FacebookEvent::getAll($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: listEvents
	//-----------------------------------------------------------------------------------------------------------------------------
	public function listEvents($events, $smallFormat=false, $title=null, $manage=false, $iCalLink=true) {
		if($events && is_array($events)) {		
			$u = new User();
			if(!$u || !$u->isLoggedIn()) {
				$u = null;
			}
			$sub = null;
			$p = null;
			
			Loader::model('user_profile');
			Loader::model('user_subscription');
			Loader::model('user_ticket');
			Loader::model('user_on_deck');
			
			if($u && $u->isLoggedIn()) {		
				$p		= UserProfile::getByUserID($u->uID);
				$sub	= UserSubscription::getByUserID($u->uID);
			}
			
			$small = $smallFormat ? "small_list" : "";
			
			$output .= "<div class='events_list_inset ".$small."'>";
			if($title) {
				$output .= $title;	
			}
			$output .= "<table class='events_list_table'>";
			foreach($events as $e) {
				$loc = Locations::getID($e->Location);
				$startTime = strtotime($e->StartTime);
				
				$ondeck = OnDeck::getID($e->OnDeckID);
				if(!$ondeck) {
					$output .= "<div class='alert alert-danger'>Failed loading the deck for the event (".$e->OnDeckID.")</div>";
				}
				else {
					$status = $ondeck->getStatus();
					$url = BASE_URL.View::url("/eventinfo?id=".$e->ID);
					if($ondeck->IsOpen) {
						if($u) {
							$url = BASE_URL.View::url("/ondeck?odid=".$ondeck->ID);
						}
						else {
							$url = BASE_URL.View::url("/login?redirect=ondeck?odid=".$ondeck->ID);
						}
					}
					if(!ENABLE_ONDECK || !$ondeck->IsEnabled) {
						$url = BASE_URL.View::url("/eventinfo?id=".$e->ID);
					}
				
					$output .= "<tr class='events_list_row ".$small."'>";
					$output .= "<td class='events_list_cal_column'>";
			
					$output .= "<a href='".$url."' class='".strtolower($e->Location)."_color'>";
					$output .= "	<div class='events_list_tinycal'>";
					$output .= "		<div class='events_list_tinycal_box'>";
					$output .= "			<div class='events_list_tinycal_month' style='background-color: #".$loc->Color."!important;'>".date("M", $startTime)."</div>";
					$output .= "			<div class='events_list_tinycal_day'>".date("d", $startTime)."</div>";
					$output .= "		</div>";
					$output .= "	</div>";
					$output .= "</a>";
					if(!$smallFormat && $iCalLink) {
						$output .= $e->getCalendarLink();
					}
				
					$output .= "</td>";
					$output .= "<td>";
				
					$output .= "	<div class='events_list_heading'>";
					$output .= "		<a href='".$url."' class='".strtolower($e->Location)."_color'>";
					$output .= "		<div class='events_list_title'>".$e->Name."</div>";
					
					//if(!$smallFormat) {
						$t = strtotime($e->StartTime);
						$format = "D M jS g:ia";
						$year = date("Y", $t);
						$thisYear = date("Y", time());
						if($year < $thisYear) {
							$format = "D M jS Y g:ia";	
						}
						$output .= "			<div class='events_list_time'>".date($format, $t);//M dS 
						
						$hasEnded = false;
						if($e->EndTime != "0000-00-00 00:00:00") {
							$endTime = strtotime($e->EndTime);
							$output .= " to ".date("g:ia", $endTime);
							
							if($endTime < time()) {
								$hasEnded = true;	
							}
						}
						if($e->AddressName) {
							$output .= " |&nbsp;".$e->AddressName;
						}
						$output .= "		</div>";
					//}
					//else {
					//	$output .= "			<div class='events_list_time'>".date("D M jS g:ia", strtotime($e->StartTime));//M dS 
					//	$output .= "		</div>";
					//}
					$output .= "		</a>";
					$output .= "	</div>";
				
					
					if(!$smallFormat && ENABLE_ONDECK && !$hasEnded) {
						$output .= "<div class='event_list_buttons'>";
						if($manage) {
							$tickets = UserTicket::getAllTicketsForEvent($e->ID);
							$count = count($tickets);
							if(!$tickets || !$count) {
								$output .= "<a class='button white medium right' href='#'>No Tickets Sold</a>";
							}
							else {
								$label = "<i class='fa fa-ticket' aria-hidden='true'></i> ".$count." Ticket".($count == 1 ? '' : 's')." Sold";
								if(isset($_REQUEST['event'])) {
									$label = "<i class='fa fa-refresh' aria-hidden='true'></i> Refresh";
								}
								$output .= "<a class='button white medium' href='/manage?event=".$e->ID."'>".$label."</a>";
							}
						}
						else {
							if($sub && $sub->isActive()) {
								$tickets = UserTicket::getAllTicketsForUserAtEvent($u->uID, $e->ID);
								if($tickets && $count = count($tickets)) {
									if(!$ondeck->isStarted() && !$ondeck->IsOpen) {
										$output .= "<a class='button right medium white' href='".$url."'><i class='fa fa-ticket' aria-hidden='true'></i>".$count." Reserved</a>";
									}
									else
									if(!$ondeck->isEnded() || $ondeck->IsOpen || $ondeck->IsLive) {
										$userOnDeck = UserOnDeck::getByUserID($ondeck->ID, $u->uID);
										if($userOnDeck) {
											$output .= "<a class='button right medium blue' href='".View::url("ondeck?odid=".$ondeck->ID)."'><i class='fa fa-check-square-o' aria-hidden='true'></i> You are #".$userOnDeck->OrderNum." On Deck</a>";
										}
										else {
											$output .= "<a class='button right medium' href='".View::url("ondeck?odid=".$ondeck->ID."&cmd_add_me=1")."'><i class='fa fa-plus' aria-hidden='true'></i> Add Me On Deck</a>";
										}
									}
									else {
										$output .= "<a class='button right medium white' href='".$url."'><i class='fa fa-ticket' aria-hidden='true'></i>".$count." Used</a>";
									}
								}
								else {
									$output .= "<a class='button right medium' href='".$url."'>RSVP</a>";
								}
							}
							else
							if($e->TicketMode == "free" || !$e->TicketPrice || $e->TicketMode == "door" ) {
								$output .= "<a class='button right medium white' href='".$url."'>More Info...</a>";
							}
							else {
								$output .= "<a class='button right medium' href='".$url."'><i class='fa fa-ticket' aria-hidden='true'></i> Buy Tickets</a>";
							}
							if($ondeck->IsEnabled) {
								if($ondeck->IsOpen) {
									$output .= "<a class='button white right medium' href='".View::url("ondeck?odid=".$ondeck->ID)."'>LIST IS OPEN <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
								}
								else 
								if(!$ondeck->isEnded()) {
									$output .= "<a class='button white right medium' href='".View::url("ondeck?odid=".$ondeck->ID)."'>LIST IS CLOSED <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
								}
							}
						}
						$output .= "</div>";
						
						if(UserProfile::isLocationAdmin($e->Location)) {
							$output .= "<div class='event_list_admin'>";
							if($ondeck->IsEnabled) {
								//if($ondeck->IsOpen) {
								//	$output .= "<a class='button left medium' href='".View::url("ondeck?odid=".$ondeck->ID)."'>LIST IS OPEN : VIEW SESSION <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
								//}
								//else 
								//if(!$ondeck->isEnded()) {
								//	$output .= "<a class='button white left medium' href='".View::url("ondeck?odid=".$ondeck->ID)."'>LIST IS CLOSED : VIEW SESSION <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
								//}
								if(!$ondeck->IsOpen) {
									$openurl = BASE_URL.View::url("/ondeck?cmd_open_deck=1&odid=".$ondeck->ID);
									$output .= "<a class='badge small gray_bg left pad-left' href='".$openurl."'>";
									$output .= "OPEN THE LIST";
									$output .= "</a>&nbsp; ";
								}
							}
							else {
								$openurl = BASE_URL.View::url("/ondeck?cmd_enable_deck=1&odid=".$ondeck->ID);
								$output .= "<a class='badge small gray_bg left pad-left' href='".$openurl."'>";
								$output .= "START NEW DECK";
								$output .= "</a>&nbsp; ";
							}
						}
						$output .= "</div>";
					}
					else 
					if(!$smallFormat){
						$output .= "<br class='clear'><br>";
					}
				
					$output .= "</td>";
					$output .= "</tr>";
				}
			}
			$output .= "</table>";
			$output .= "</div>";
		}
		else {
			$output = "<div class='alert alert-warning'>No events were found</div>";
		}
		return $output;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: listEvents
	//-----------------------------------------------------------------------------------------------------------------------------
	public function listDecks($decks, $smallFormat=false, $title=null, $manage=false) {
		if($decks && is_array($decks)) {		
			$u = new User();
			if(!$u || !$u->isLoggedIn()) {
				$u = null;
			}
			$sub = null;
			$p = null;
			
			Loader::model('user_profile');
			Loader::model('user_subscription');
			Loader::model('user_ticket');
			Loader::model('user_on_deck');
			
			if($u && $u->isLoggedIn()) {		
				$p		= UserProfile::getByUserID($u->uID);
				$sub	= UserSubscription::getByUserID($u->uID);
			}
			
			$small = $smallFormat ? "small_list" : "";
			
			$output .= "<div class='events_list_inset ".$small."'>";
			if($title) {
				$output .= $title;	
			}
			$output .= "<table class='events_list_table'>";
			foreach($decks as $ondeck) {
				$loc = Locations::getID($ondeck->Location);
				$startTime = strtotime($ondeck->StartTime);
				
				$status = $ondeck->getStatus();
				$url = BASE_URL.View::url("/ondeck?odid=".$ondeck->ID);
				if($ondeck->IsOpen) {
					if($u) {
						$url = BASE_URL.View::url("/ondeck?odid=".$ondeck->ID);
					}
					else {
						$url = BASE_URL.View::url("/login?redirect=ondeck?odid=".$ondeck->ID);
					}
				}
				if(!ENABLE_ONDECK || !$ondeck->IsEnabled) {
					$url = BASE_URL.View::url("/ondeck?odid=".$ondeck->ID);
				}
			
				$output .= "<tr class='events_list_row ".$small."'>";
				$output .= "<td class='events_list_cal_column'>";
		
				$output .= "<a href='".$url."' class='".strtolower($ondeck->Location)."_color'>";
				$output .= "	<div class='events_list_tinycal'>";
				$output .= "		<div class='events_list_tinycal_box'>";
				$output .= "			<div class='events_list_tinycal_month' style='background-color: #".$loc->Color."!important;'>".date("M", $startTime)."</div>";
				$output .= "			<div class='events_list_tinycal_day'>".date("d", $startTime)."</div>";
				$output .= "		</div>";
				$output .= "	</div>";
				$output .= "</a>";
			
				$output .= "</td>";
				$output .= "<td>";
			
				$output .= "	<div class='events_list_heading'>";
				$output .= "		<a href='".$url."' class='".strtolower($ondeck->Location)."_color'>";
				$output .= "		<div class='events_list_title'>".$ondeck->Title."</div>";
				
				//if(!$smallFormat) {
					$t = strtotime($ondeck->StartTime);
					$year = date("Y", $t);
					$thisYear = date("Y", time());
					$format = $year == $thisYear ? "D M jS g:ia" : "D M jS Y g:ia";
					
					$output .= "			<div class='events_list_time'>".date($format, $t);//M dS 
					
					$hasEnded = false;
					if($ondeck->EndTime != "0000-00-00 00:00:00") {
						$endTime = strtotime($ondeck->EndTime);
						$output .= " to ".date("g:ia", $endTime);
						
						if($endTime < time()) {
							$hasEnded = true;	
						}
					}
					if($ondeck->AddressName) {
						$output .= " |&nbsp;".$ondeck->AddressName;
					}
					$output .= "		</div>";
				//}
				//else {
				//	$output .= "			<div class='events_list_time'>".date("D M jS g:ia", strtotime($ondeck->StartTime));//M dS 
				//	$output .= "		</div>";
				//}
				$output .= "		</a>";
				$output .= "	</div>";
			
				
				if(!$smallFormat && ENABLE_ONDECK) {
					$output .= "<div class='event_list_buttons'>";
					if($manage) {
						$tickets = UserTicket::getAllTicketsForEvent($ondeck->EventID);
						$count = count($tickets);
						if(!$tickets || !$count) {
							$output .= "<a class='button white medium right' href='#'>No Tickets Sold</a>";
						}
						else {
							$label = "<i class='fa fa-ticket' aria-hidden='true'></i> ".$count." Ticket".($count == 1 ? '' : 's')." Sold";
							if(isset($_REQUEST['event'])) {
								$label = "<i class='fa fa-refresh' aria-hidden='true'></i> Refresh";
							}
							$output .= "<a class='button white medium' href='/manage?event=".$ondeck->EventID."'>".$label."</a>";
						}
					}
					$output .= "</div>";
					
					$output .= "<div class='event_list_admin'>";
					if($ondeck->IsEnabled) {
						if($ondeck->IsOpen) {
							$output .= "<a class='button left medium' href='".View::url("ondeck?odid=".$ondeck->ID)."'>LIST IS OPEN : VIEW SESSION <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
						}
						else 
						if(!$ondeck->isEnded()) {
							$output .= "<a class='button white left medium' href='".View::url("ondeck?odid=".$ondeck->ID)."'>LIST IS CLOSED : VIEW SESSION <i class='fa fa-sign-in' aria-hidden='true'></i></a>";
						}
						if(UserProfile::isLocationAdmin($ondeck->Location) && !$ondeck->IsOpen) {
							$openurl = BASE_URL.View::url("/ondeck?cmd_open_deck=1&odid=".$ondeck->ID);
							$output .= "<a class='badge small gray_bg left pad-left' href='".$openurl."'>";
							$output .= "OPEN THE LIST";
							$output .= "</a>&nbsp; ";
						}
					}
					else
					if(UserProfile::isLocationAdmin($ondeck->Location)) {
						$openurl = BASE_URL.View::url("/ondeck?cmd_enable_deck=1&odid=".$ondeck->ID);
						$output .= "<a class='badge small gray_bg left pad-left' href='".$openurl."'>";
						$output .= "START NEW DECK";
						$output .= "</a>&nbsp; ";
					}
					$output .= "</div>";
				}
				else 
				if(!$smallFormat){
					$output .= "<br class='clear'><br>";
				}
			
				$output .= "</td>";
				$output .= "</tr>";
			}
			$output .= "</table>";
			$output .= "</div>";
		}
		else {
			$output = "<div class='alert alert-warning'>No events were found</div>";
		}
		return $output;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: closeSessionsLeftOpen
	//-----------------------------------------------------------------------------------------------------------------------------
	public function closeSessionsLeftOpen() {
		Loader::model('on_deck');
		
		$out = "<div class='alert alert-white'>";
		$twelveHoursAgo = date("Y-m-d H:i:s", time() - (12 * 60 * 60));
		$q = " WHERE ((EndTime = '0000-00-00 00:00:00' OR IsOpen=1 OR IsLive=1) AND StartTime != '0000-00-00 00:00:00' AND StartTime<'".$twelveHoursAgo."') ";
		$events = OnDeck::getAll($q);
		$out .= "<h3>Decks Closed: ".count($events)."</h3>";
		if($events) {
			foreach($events as $e) {
				// Assume the event lasted 8 hours max
				$out .= "CLOSED EVENT:".$e->Name." STARTED AT:".$e->StartTime."<br>";
				$e->close(date("Y-m-d H:i:s", strtotime($e->StartTime) + (8 * 60 * 60)));
						
				//$slack = Loader::helper("slack");
				//$slack->send("Closed On Deck Session:\n".$e->Title);
			}
		}
		return $out;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: upcomingEventsEmail
	//-----------------------------------------------------------------------------------------------------------------------------
	public function upcomingEventsEmail(&$count, $limit=4, $order="ASC", $location=null) {
		Loader::model('facebook_event');
		Loader::model('user_profile');
		Loader::model('locations');
		Loader::model('on_deck');
		
		$empty = false;
		$output = null;
		$output .= "<div style='float:left; margin:20px; width:100%;'>";
		$output .= "<table style='clear:both; float:left; width:100%; overflow:hidden;'>";
		
		$fromDate = date("Y-m-d 00:00:00", time());
		$q = "LEFT JOIN OnDeck AS od ON od.ID=OnDeckID WHERE fb.StartTime>='".$fromDate."'";
		if($location) {
			$q .= " AND fb.Location='".$location."'";
		}
		$q .= " ORDER BY od.IsOpen DESC, fb.StartTime ".$order." LIMIT ".$limit;
		
		$u = new User();
		if(!$u || !$u->isLoggedIn()) $u = null;
		
		if($events = FacebookEvent::getAll($q)) {
			$count = count($events);
			foreach($events as $e) {
				$loc = Locations::getID($e->Location);
				$startTime = strtotime($e->StartTime);
				
				$ondeck = OnDeck::getID($e->OnDeckID);
				if(!$ondeck) {
					//echo "<div class='alert alert-danger'>Failed loading the deck for the event (".$e->OnDeckID.")</div>";
				}
				else {
					$status = $ondeck->getStatus();

					$url = BASE_URL.View::url("/eventinfo?id=".$e->ID);
				
					$output .= "<tr style='border-bottom:1px solid #DDD;'>";
					$output .= "<td style='width:60px; vertical-align: top;'>";
			
					$output .= "<a href='".$url."' style='color:#".$loc->Color.";'>";
					$output .= "	<div style='float: left; margin-top:10px; margin-right:20px; width: 44px; height: 44px;'>";
					$output .= "		<div style='margin-top: 3px; background-color: white; border: 1px solid #BDBDBD; width: 44px; height: 44px; border-radius:2px;'>";
					$output .= "			<div style='color: #ffffff; padding:2px; font-size: 12px; line-height: 14px; text-align: center; text-transform:uppercase; background-color: #".$loc->Color."!important;'>".date("M", $startTime)."</div>";
					$output .= "			<div style='color: #000000; font-size: 16px; line-height: 18px; padding-top:3px; font-weight:bold; text-align: center;'>".date("d", $startTime)."</div>";
					$output .= "		</div>";
					$output .= "	</div>";
					$output .= "</a>";
				
					$output .= "</td>";
					$output .= "<td>";
				
					$output .= "	<div style='float: left; width: auto; text-align:left;'>";
					$output .= "		<a href='".$url."' style='color:#".$loc->Color."'>";
					$output .= "		<div style='float: left; margin-top:10px; padding-bottom: 2px; padding-top: 2px; font-weight: bold; font-family: sans-serif; font-size: 18px;'>".$e->Name."</div>";
					$output .= "			<div style='float:left; clear:both;'>".$e->StartTime;
					if($e->EndTime != "0000-00-00 00:00:00") {
						$output .= " to ".date("g:ia", strtotime($e->EndTime));
					}
					if($e->AddressName) {
						$output .= " |&nbsp;".$e->AddressName;
					}
					$output .= "		</div>";
					$output .= "		</a>";
					$output .= "	</div>";
				
					$output .= "<br class='clear'><br>";
				
					$output .= "</td>";
					$output .= "</tr>";
				}
			}
		}
		else {
			$empty = true;
			$output = "<div class='alert alert-warning'>No events were found</div>";
		}
		$output .= "</table>";
		$output .= "</div>";
		if($empty) $output = null;
		return $output;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateEventInfo
	//-----------------------------------------------------------------------------------------------------------------------------
	public function updateEventInfo() {
		Loader::model('on_deck');
		Loader::model('facebook_event');

		$twoYears = 60*60*24*365*2;
		$startDate = date("Y-01-01", time());//date("Y", time())."-1-1";//date("Y-m-d", time() - $twoYears);
		$endDate = date("Y-m-d", time() + $twoYears);

		$out = "";
		$out .= "<div class='alert alert-info'>Searching for events from ".$startDate." to ".$endDate."</div>";
		if($events = FacebookEvent::pullEventsFromFacebook($startDate, $endDate)) {
			foreach($events as $e) {
				$out .= "<div class='alert alert-success'>";
				$out .= "<h3>".($e->IsNew ? "NEW" : "UPDATED")." ".$e->ID."</h3>";
				$out .= "<table>";
				
				$out .= "<tr>";
				$out .= "<td>Name</td>";
				$out .= "<td>".$e->Name."</td>";
				$out .= "</tr>";

				$out .= "<tr>";
				$out .= "<td>Location</td>";
				$out .= "<td>".$e->Location."</td>";
				$out .= "</tr>";

				$out .= "<tr>";
				$out .= "<td>Ticket Price</td>";
				$out .= "<td>".$e->TicketPrice."</td>";
				$out .= "</tr>";

				$out .= "<tr>";
				$out .= "<td>Ticket Mode</td>";
				$out .= "<td>".$e->TicketMode."</td>";
				$out .= "</tr>";
				
				$out .= "</table>";
				$out .= "</div>";
				
				//OnDeck::updateFromEvent($fb, true);
			}
		}
		else {
			$out .= "<div class='alert alert-warning'>No events were pulled from facebook.</div>";
		}
		return $out;
	}

}
?>