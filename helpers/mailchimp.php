<?php
defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: MailchimpHelper
//-----------------------------------------------------------------------------------------------------------------------------
class MailchimpHelper {
	var $ENABLED	= true;
	var $mainList	= '37a6aecc29';
	var $apiKey		= 'd4c5da4eb79b08dacdaed7fa4fa871d1-us10';
	var $mc 		= null;


	//-----------------------------------------------------------------------------------------------------------------------------
	//:: startup
	//-----------------------------------------------------------------------------------------------------------------------------
	function startup() {
		$this->controller = &$controller;

		Loader::library('mailchimp/MailChimp','');
		
		ob_start();
		$this->mc = new \Drewm\MailChimp($this->apiKey);
		ob_end_clean();
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getLists
	//-----------------------------------------------------------------------------------------------------------------------------
	function getLists() {
		ob_start();
		$out = $this->mc->call('lists/list');
		ob_end_clean();
		return $out;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: subscribe
	//-----------------------------------------------------------------------------------------------------------------------------
	function subscribe($uid, $confirm=false) {
		ob_start();
		$out = "";
		if($this->ENABLED) {
			Loader::model('user_profile');
			
			$user = UserInfo::getByID($uid);
			$profile = UserProfile::getByUserID($uid);
			if($user && $profile) {
				$data['NAME'] = $profile->uFullName;
				$data['ARTIST'] = $profile->uArtistName;
				$data['LOCATION'] = $profile->uLocation;
					
				$r = $this->mc->call('lists/subscribe', array(
					'id'                => $this->mainList,
					'email'             => array('email'=>$user->uEmail),
					'merge_vars'        => $data,
					'double_optin'      => $confirm,
					'update_existing'   => true,
					'replace_interests' => false,
					'send_welcome'      => false,
				));
				
				$out .= "USER:".$user->uID."<br>";	
				$out .= "Email:".$user->uEmail."<br>";	
				$out .= "Artist Name:".$profile->uArtistName."<br>";	
				$out .= "Real Name:".$profile->uFullName."<br>";	
				$out .= "uNewsletter:".$profile->uNewsletter."<br>";	
			}
		}
		ob_end_clean();
		return $out;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: unsubscribe
	//-----------------------------------------------------------------------------------------------------------------------------
	function unsubscribe($uid, $goodbye=false) {
		ob_start();
		$out = "";
		if($this->ENABLED) {
			Loader::model('user_profile');
			
			$user = UserInfo::getByID($uid);
			$profile = UserProfile::getByUserID($uid);
			if($user && $profile) {
				$data['NAME'] = $profile->uFullName;
				$data['ARTIST'] = $profile->uArtistName;
					
				$r = $this->mc->call('lists/unsubscribe', array(
					'id'                => $this->mainList,
					'email'             => array('email'=>$user->uEmail),
					'delete_member'   	=> false,
					'send_goodbye '   	=> $goodbye,
					'send_notify' 		=> false
				));
				
				$out .= "Unsubscribe:".$user->uID." Email:".$user->uEmail."<br>";	
			}
		}
		ob_end_clean();
		return $out;
	}
}
?>