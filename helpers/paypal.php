<?php
defined('C5_EXECUTE') or die("Access Denied.");

define('PAYPAL_HOST', 		"www.paypal.com");
define('PAYPAL_URL', 		"https://".PAYPAL_HOST);
define('PAYPAL_EMAIL', 		"Producers.social@gmail.com");
define('PAYPAL_COUNTRY', 	"US");
define('PAYPAL_TICKET', 	"ProducersSocial_BuyNow_WPS_US");
define('PAYPAL_SUBSCRIBE', 	"ProducersSocial_Subscribe_WPS_US");

define('PAYPAL_TEST_HOST', 	"www.sandbox.paypal.com");
define('PAYPAL_TEST_URL', 	"https://www.sandbox.paypal.com");

define("LOGS", "/home/producerssocial/public_html/logs/");

class PaypalHelper {
	public $SandboxMode 	= false;
	public $LogEnabled 		= true;
	public $SellerEmail		= PAYPAL_EMAIL;
	public $EnableBypass	= false;
	public $BypassKey		= "sSkDd202332k201sDeSSI82";

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: LogTransaction
	//-----------------------------------------------------------------------------------------------------------------------------
	public function LogTransaction($cmd) {
		$output = $cmd."\n";

		if($this->LogEnabled) {
			$filename = LOGS . 'transaction.log';
			$log = new File($filename, true);
			if($log->writable()) {
				return $log->append($output);
			}
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: Log
	//-----------------------------------------------------------------------------------------------------------------------------
	public function Log($type, $msg) {
		$output = ">>>".date('Y-m-d H:i:s').' '.$type.': '.$msg."\n";

		if($this->LogEnabled) {
			$filename = LOGS . 'paypal.log';
			$log = new File($filename, true);
			if($log->writable()) {
				return $log->append($output);
			}
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: Button
	//-----------------------------------------------------------------------------------------------------------------------------
	public function Button($label, $itemName, $itemNum, $amount, $quantity, $userid, $javascript, $subscribe=null, $cycle=null, $modifySubscription=false) {
		$html = null;
		$amount = number_format($amount, 2);
		//$amount = "0.01";

		$userid = trim($userid);
		if(!$userid) {
			$html = "Login Required";
		}
		else {
			if($this->EnableBypass) {
				$html .= "<form id='paypal_form' action='".View::url("/ipn")."' method='post'>";
				$html .= "<input type='hidden' name='bypass' value='".$this->BypassKey."'>";
				$html .= "<input type='hidden' name='redirect' value='http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]."'>";
				$html .= "<input type='hidden' name='payment_status' value='Complete'>";
				$html .= "<input type='hidden' name='mc_gross' value='".$amount."'>";
				$html .= "<input type='hidden' name='payment_date' value='".date("Y-m-d H:i:s", time())."'>";
				$html .= "<input type='hidden' name='txn_id' value='bypass_".uniqid()."'>";
			}
			else
			if($this->SandboxMode) {
				$html .= "<form id='paypal_form' action='".PAYPAL_TEST_URL."/cgi-bin/webscr' method='post'>";
				$html .= "<input type='hidden' name='business' value='".$this->SellerEmail."'>";
			}
			else {
				$html .= "<form id='paypal_form' action='".PAYPAL_URL."/cgi-bin/webscr' method='post'>";
				$html .= "<input type='hidden' name='business' value='".$this->SellerEmail."'>";
			}

			$html .= "<input type='hidden' name='item_name' value='".$itemName."'>";
			if($subscribe) {
				$html .= "<input type='hidden' name='cmd' value='_xclick-subscriptions'>";
				$html .= "<input type='hidden' name='bn' value='".PAYPAL_SUBSCRIBE."'>";
				$html .= "<input type='hidden' name='src' value='1'>";
				$html .= "<input type='hidden' name='sra' value='1'>";
				$html .= "<input type='hidden' name='a3' value='".$amount."'>";
				$html .= "<input type='hidden' name='p3' value='".$cycle."'>";
				$html .= "<input type='hidden' name='t3' value='M'>";
				$html .= "<input type='hidden' name='usr_manage' value='0'>";
				$html .= "<input type='hidden' name='modify' value='".($modifySubscription?2:0)."'>"; // 0=new / 1=new or modify / 2=modify
			}
			else {
				$html .= "<input type='hidden' name='cmd' value='_xclick'>";
				$html .= "<input type='hidden' name='bn' value='".PAYPAL_TICKET."'>";
				$html .= "<input type='hidden' name='amount' value='".$amount."'>";
			}
			$html .= "<input type='hidden' name='lc' value='".PAYPAL_COUNTRY."'>";
			$html .= "<input type='hidden' name='currency_code' value='USD'>";
			$html .= "<input type='hidden' name='no_note' value='1'>";
			$html .= "<input type='hidden' name='no_shipping' value='1'>";
			$html .= "<input type='hidden' name='rm' value='2'>";
			$html .= "<input type='hidden' name='custom' value='".$userid."'>";
			$html .= "<input type='hidden' name='item_number' value='".$itemNum."'>";
			$html .= "<input type='hidden' name='quantity' value='".$quantity."'>";

			$html .= "<input type='hidden' name='cpp_header_image' value='".BASE_URL."/img/paypal_heading.gif'>";
			$html .= "<input type='hidden' name='cpp_headerback_color' value='c3731c'>";
			$html .= "<input type='hidden' name='cpp_headerborder_color' value='994619'>";
			//$html .= "<input type='hidden' name='cpp_payflow_color' value='994619'>";//FEFAE8

			$html .= "<input type='hidden' name='return' value='".BASE_URL."/events'>";
			$html .= "<input type='hidden' name='cancel_return' value='".BASE_URL."/events'>";

			//$html .= "<input class='button' type='submit' name='submit' value='".$label."' alt='PayPal - The safer, easier way to pay online!'>";
			$label = "Pay with PayPal";
			$html .= "<br><a id='paypal_submit' class='button blue small center' href='javascript:;' onclick='return ".$javascript."'>";
			$html .= "<i class='fa fa-paypal' aria-hidden='true'></i>&nbsp; ";
			$html .= $label;
			$html .= "</a>";
			//$html .= "<img alt='' border='0' src='https://www.paypal.com/en_US/i/scr/pixel.gif' width='1' height='1'>";
			$html .= "</form>";
		}		
		return $html;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: UnsubscribeButton
	//-----------------------------------------------------------------------------------------------------------------------------
	public function UnsubscribeButton($title='Unsubscribe', $style='normal') {
		global $html;
		global $format;

		$id = urlencode($this->SellerEmail);
		$url = PAYPAL_URL."/cgi-bin/webscr?cmd=_subscr-find&alias=".$id;

		if($style == 'normal') {
		// Normal Button
			echo $format->button($title, $url, $attr=array('target'=>'_blank'));
		}
		else {
		// Large Button Style
			echo "<a id='unsubscribe' target='_blank' href='".$url."'>";
			echo "<IMG BORDER='0' SRC='".BASE_URL."/img/unsubscribeButton.gif'>";
			echo "</a>";
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: CancelSubscription
	//-----------------------------------------------------------------------------------------------------------------------------
	public function CancelSubscription($id) {
		$api_request = 'USER=' . urlencode( 'office_api1.bota.org' )
			.  '&PWD=' . urlencode( 'A5W7VM5TJ7Q6J4CM' )
			.  '&SIGNATURE=' . urlencode( 'AOfIV7wu-MNjv1w1f4nYb2EqazHRARnz.2H313ZNAAvtd4y5VJPPfCkm' )
			.  '&VERSION=76.0'
			.  '&METHOD=ManageRecurringPaymentsProfileStatus'
			.  '&PROFILEID=' . urlencode( $id )
			.  '&ACTION=' . urlencode( 'Cancel' )
			.  '&NOTE=' . urlencode( 'Subscription cancelled' );
		 
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, 'https://api-3t.paypal.com/nvp' ); // For live transactions, change to 'https://api-3t.paypal.com/nvp'
		curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
	 
		// Uncomment these to turn off server and peer verification
		// curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		// curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
	 
		// Set the API parameters for this transaction
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );
	 
		// Request response from PayPal
		$response = curl_exec( $ch );
	 
		// If no response was received from PayPal there is no point parsing the response
		if( ! $response )
			die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );
	 
		curl_close( $ch );
	 
		// An associative array is more usable than a parameter string
		parse_str( $response, $parsed_response );
	 
		return $parsed_response;	
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: Verify
	//-----------------------------------------------------------------------------------------------------------------------------
	// Possible return values:
	//	Canceled_Reversal	- Reversal has been cancelled (dispute)
	//	Completed			- Payment Processed
	//	Created			- Express Checkout
	//	Denied			- Denied by seller manually
	//	Expired			- Authorization expired
	//	Failed			- Payment Failed
	//	Pending			- Payment is pending (see pending_reason)
	//	Refunded			- Payment refunded by seller
	//	Reversed			- Disputed payment refunded
	//	Processed			- Payment accepted
	//	Voided			- Transaction void
	//-----------------------------------------------------------------------------------------------------------------------------
	public function Verify() {
		$start = "url=members/ipn&";
		$errno = $errstr = null;

		if(isset($_REQUEST['txn_id'])) PaypalHelper::Log("Transaction", $_REQUEST['txn_id']);

		$req = 'cmd=_notify-validate';
		foreach($_REQUEST as $key => $value) {
			if(!is_array($value)) {
				$value = urlencode(stripslashes($value));
				$req .= "&$key=$value";
			}
		}
		PaypalHelper::Log("Request", $req); 

		if($this->SandboxMode) {
			PaypalHelper::Log("Verify:", "TESTMODE");
			$verified = true;
		}
		else {
			PaypalHelper::Log("Verify:", "BEGIN");
			$verified = false;
	
			$host = $this->SandboxMode ? PAYPAL_TEST_HOST : PAYPAL_HOST;
	
			$fp = fsockopen($host, 80, $errno, $errstr, 30);
			if(!$fp) {
				PaypalHelper::Log("ERROR", "Failed connecting to PayPal:$errstr ($errno)\n");
			}
			else {
				$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
				$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
				$header .= "Content-Length: ".strlen($req)."\r\n\r\n";
				fputs($fp, $header.$req);
		
				$header = true;
				while(!feof($fp)) {
					$res = trim(fgets($fp, 1024));
		
					if(strlen($res) == 0) {
						$header = false;
						continue;
					}
					if($header) {
						continue;
					}
		
					PaypalHelper::Log("Response", $res);
					if(strcmp($res, "VERIFIED") == 0) {
						$verified = true;
						break;
					}
					else {
						$verified = false;
						break;
					}
				}
			}
			fclose($fp);
		}

		PaypalHelper::Log("VERIFIED:", $verified?"YES":"NO");
		return $verified;
	}

}
?>