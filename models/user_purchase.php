<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserPurchase
//-----------------------------------------------------------------------------------------------------------------------------
class UserPurchase extends Object { 

	public $upID 			= '';
	public $uID 			= '';
	public $uTransactionID 	= '';
	public $uItem 			= '';
	public $uEventID 		= '';
	public $uLocation		= '';
	public $uQuantity		= 1;
	public $uAmount 		= '';
	public $uStatus 		= '';
	public $uDate 			= '';
	public $uNote 			= '';
	
	public static $Fields = array(
		"upID", "uID", "uTransactionID", "uItem", "uEventID", "uLocation", "uQuantity", "uAmount", "uStatus", "uDate", "uNote");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserPurchase::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByUserID($uID) {
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserPurchases WHERE uID='".$uID."'";
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserPurchase();
					foreach(UserPurchase::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
			}
			$all[] = $nu;
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByTransactionID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByTransactionID($transID) {
		$db = Loader::db();
		$q = "SELECT * FROM UserPurchases WHERE uTransactionID='".$transID."' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new UserPurchase();
			foreach(UserPurchase::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getTicketsForUser
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getTicketsForUser($uID, $eventID, $addQuery=null) {
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserPurchases WHERE uID='".$uID."'";
		if($eventID) $q .= " AND uEventID='".$eventID."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserPurchase();
					foreach(UserPurchase::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getTicketsForEvent
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getTicketsForEvent($eventID, $addQuery=null) {
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserPurchases WHERE uEventID='".$eventID."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserPurchase();
					foreach(UserPurchase::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: countTicketsForUser
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function countTicketsForUser($uID, $eventID) {
		$count = 0;
		if($eventID) {
			$all = UserPurchase::getTicketsForUser($uID, $eventID);
			if($all) {
				foreach($all as $t) {
					$count += $t->uQuantity;
				}
			}
		}
		return $count;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: countTicketsForEvent
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function countTicketsForEvent($eventID, $addQuery=null) {
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserPurchases WHERE uEventID='".$eventID."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO UserPurchases (".UserPurchase::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserPurchase::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) { 
		//print_r(debug_backtrace());
		$db = Loader::db();
		$q = "UPDATE UserPurchases SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(UserPurchase::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE upID='".$up->upID."'";
		$db->execute($q);
	}
}
