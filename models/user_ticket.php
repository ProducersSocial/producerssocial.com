<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserTicket
//-----------------------------------------------------------------------------------------------------------------------------
class UserTicket extends Object { 

	public $id 				= '';
	public $user_id 		= '';
	public $event_id 		= '';
	public $autosignup 		= '';
	public $location 		= '';
	public $used 			= '0000-00-00 00:00:00';
	public $created 		= '';
	public $expires 		= '';
	
	public static $Fields = array(
		"id", "user_id", "event_id", "autosignup", "location", "used", "created", "expires");
	
	public static function getFieldsList() 
	{
		return implode(", ", UserTicket::$Fields);
	}
	
	public function locationName() {
		Loader::model('locations');
		return Locations::getName($this->location);
	}
	
	public function isUsed() {
		return $this->used != 0 && $this->used != "0000-00-00 00:00:00";	
	}
	
	public function isExpired() {
		$expired = false;
		if($this->hasExpiration()) {
			$exp = strtotime($this->expires);
			if($exp < time()) {
				$expired = true;
			}
		}
		return $expired;	
	}
	
	public function hasExpiration() {
		return !$this->event_id && $this->expires != 0 && $this->expires != "0000-00-00 00:00:00";	
	}
	
	public static function delete($id) {
		$db = Loader::db();
		$q = "DELETE FROM UserTickets WHERE id='".$id."'";
		$db->execute($q);
	}
	
	public function deleteAll($query=null) {
		$q = "DELETE FROM UserTickets WHERE 1=1";
		if($query) {
			$q .= " AND (".$query.")";
		}
		$db = Loader::db();
		$db->query($q);
		//echo $q;
	}
	
	public static function getByID($id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserTickets WHERE id='".$id."' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserTicket();
			foreach(UserTicket::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getByUserID($user_id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserTickets WHERE user_id='".$user_id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserTicket();
			foreach(UserTicket::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getAllByUserID($user_id, $query=null) 
	{
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserTickets WHERE user_id='".$user_id."'";
		if($query) $q .= " AND ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserTicket();
					foreach(UserTicket::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
			}
			$all[] = $nu;
		}
		return $all;
	}
	
	public static function getAll($query="1=1", $order=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserTickets WHERE ".$query;
		if($order) $q .= " ".$order;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserTicket();
					foreach(UserTicket::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	public static function getAllTicketsForEvent($event_id, $query=null, $order="ORDER BY created DESC") 
	{
		if(!$event_id) {
			return null;	
		}
		$q = "event_id='".$event_id."'";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		return UserTicket::getAll($q);
	}
	
	public static function getTicketForEvent($event_id, $query=null, $order="ORDER BY created DESC LIMIT 1") 
	{
		if(!$event_id) {
			return null;	
		}
		$q = "event_id='".$event_id."'";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		return UserTicket::getAll($q);
	}
	
	public static function getTicketForUser($user_id, $addQuery=null, $order="ORDER BY created DESC LIMIT 1") 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserTickets WHERE user_id='".$user_id."'";
		if($addQuery) $q .= " AND ".$addQuery;
		if($order) $q .= " ".$order;
		
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new UserTicket();
			foreach(UserTicket::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		
		return $nu;
	}
	
	public static function getAllTicketsForUser($user_id, $addQuery=null, $order=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserTickets WHERE user_id='".$user_id."'";
		if($addQuery) $q .= " AND ".$addQuery;
		if($order) $q .= " ".$order;
		//echo $q;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserTicket();
					foreach(UserTicket::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	public static function getAllTicketsForUserAtEvent($user_id, $event_id, $query=null, $order=null) 
	{
		if(!$event_id) {
			return null;	
		}
		$q = "event_id='".$event_id."'";
		if($query) $q .= " AND (".$query.")";
		
		return UserTicket::getAllTicketsForUser($user_id, $q, $order);
	}
	
	public static function getAllAvailableTicketsForUser($user_id, $query=null, $order="ORDER BY expires ASC") 
	{
		$q = "((event_id IS NULL OR event_id='') AND used = '0000-00-00 00:00:00' AND (expires = '0000-00-00 00:00:00' OR expires > '".date("Y-m-d H:i:s", time())."'))";
		if($query) $q .= " AND ".$query;
		return UserTicket::getAllTicketsForUser($user_id, $q, $order);
	}

	public static function getAllRSVPTicketsForUser($user_id, $query=null, $order=null) 
	{
		$q = "(event_id IS NOT NULL AND event_id != '' AND (used = '0000-00-00 00:00:00' AND (expires = '0000-00-00 00:00:00' OR expires > '".date("Y-m-d H:i:s", time())."')))";
		if($query) $q .= " AND ".$query;
		return UserTicket::getAllTicketsForUser($user_id, $q, $order);
	}

	public static function getAllUsedTicketsForUser($user_id, $query=null, $order=null) 
	{
		$q = "(used != '0000-00-00 00:00:00' OR expires < '".date("Y-m-d H:i:s", time())."')";
		if($query) $q .= " AND ".$query;
		return UserTicket::getAllTicketsForUser($user_id, $q, $order);
	}
	
	public static function countAllTicketsForEvent($event_id, $addQuery=null) 
	{
		if(!$event_id) {
			return 0;	
		}
		$count = 0;
		$db = Loader::db();
		if($event_id) {
			$q = "SELECT count(*) as count FROM UserTickets WHERE event_id='".$event_id."'";
			if($addQuery) $q .= " AND ".$addQuery;
			
			if($r = $db->query($q)) {
				$count = $r->FetchRow()['count'];
			}
		}
		return $count;
	}
	
	public static function countAllTicketsForUser($user_id, $addQuery=null) 
	{
		$count = 0;
		$db = Loader::db();
		$q = "SELECT count(*) as count FROM UserTickets WHERE user_id='".$user_id."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}
	
	public static function countAllTicketsForUserAtEvent($user_id, $event_id, $query=null, $order=null) 
	{
		if(!$event_id) {
			return 0;	
		}
		$q = "event_id='".$event_id."'";
		if($query) $q .= " AND (".$query.")";
		
		return UserTicket::countAllTicketsForUser($user_id, $q, $order);
	}
	
	public static function countAllAvailableTicketsForUser($user_id, $query=null) 
	{
		$q = "(used = '0000-00-00 00:00:00' AND (expires = '0000-00-00 00:00:00' OR expires > '".date("Y-m-d H:i:s", time())."'))";
		if($query) $q .= " AND ".$query;
		return UserTicket::countAllTicketsForUser($user_id, $q);
	}
	
	public static function countAllUsedTicketsForUser($user_id, $query=null) 
	{
		$q = "(used != '0000-00-00 00:00:00' OR expires < '".date("Y-m-d H:i:s", time())."')";
		if($query) $q .= " AND ".$query;
		return UserTicket::countAllTicketsForUser($user_id, $q);
	}
	
	public static function autosignup($id, $isOn) 
	{
		$ticket = UserTicket::getByID($id);
		if($ticket) {
			$ticket->autosignup = $isOn ? time() : 0;
			UserTicket::save($ticket);
		}
	}
	
	public static function rsvp($eventid, $quantity) 
	{
		if(!$eventid) {
			return "Error: missing event ID";	
		}
		$out = "";
		if(!$eventid) {
			$out = "Error - Invalid event ID";
		}
		else
		if(!$quantity) {
			$out = "Error - No quantity specified for ticket RSVP";
		}
		else {
			Loader::model("facebook_event");
			$event = FacebookEvent::getID($eventid);
			
			$u = new User();
			$q = "(expires='0000-00-00 00:00:00' OR expires >= '".$event->StartTime."')";
			$available = UserTicket::getAllAvailableTicketsForUser($u->uID, $q, "ORDER BY expires ASC");
			if(count($available) < $quantity) {
				$out = "Sorry, but you do not have enough available tickets, or they expire before the event date.";
				if($quantity > 1) {
					$out .= " Try selecting fewer tickets, or you may <a href='http://producerssocial.net/eventinfo?id=".$event->ID."&buydirect=true'>purchase additional tickets</a>";	
				}
			}
			else {
				for($i = 0; $i < $quantity; $i++) {
					$available[$i]->event_id = $eventid;
					UserTicket::save($available[$i]);
				}
				$out = "You have reserved ".$quantity." ticket".($quantity > 1 ? "s" : "")." for this event";
			}
		}
		return $out;
	}
	
	public static function unrsvp($eventid) 
	{
		if(!$eventid) {
			return "Error: missing event ID";	
		}
		$out = "";
		if(!$eventid) {
			$out = "Error - Invalid event ID";
		}
		else {
			$u = new User();
			$tickets = UserTicket::getAllTicketsForUserAtEvent($u->uID, $eventid);
			if($tickets) {
				Loader::model('user_subscription');
				$sub = UserSubscription::getByUserID($u->uID);
				
				// Only allow active subscribers access to this feature
				if($sub && $sub->isActive()) {
					$out .= "EVENT:".$eventid."<br>";
					$count = count($tickets);
					if($count) {
						foreach($tickets as $t) {
							$out .= "TICKET:".$t->id."<br>";
							$t->event_id = "";
							UserTicket::save($t);
						}
						$out .= "You have removed your reservation of ".$count." ticket".($count > 1 ? "s" : "")." for this event";
					}
				}
				else {
					$out .= "Only users with active memberships can unrsvp from events.";
				}
			}
		}
		return $out;
	}
	
	public static function unrsvpTicket($ticketid) 
	{
		$out = "";
		if(!$ticketid) {
			$out = "Error - Invalid ticket ID";
		}
		else {
			$u = new User();
			Loader::model('user_subscription');
			$sub = UserSubscription::getByUserID($u->uID);
			
			// Only allow active subscribers access to this feature
			if($sub && $sub->isActive()) {
				$t = UserTicket::getByID($ticketid);
				if($t) {
					$out .= "TICKET:".$t->id."<br>";
					$t->event_id = "";
					UserTicket::save($t);
					$out .= "You have unreserved 1 ticket.";
				}
				else {
					$out .= "Error - could not find the ticket:".$ticketid;
				}
			}
			else {
				$out .= "Error - Only users with active memberships can unrsvp from events.";
			}
		}
		return $out;
	}
	
	public static function rsvpCommitTickets($eventid) 
	{
		if($eventid) {
			$all = UserTicket::getAll("event_id='".$eventid."'");
			if($all) {
				foreach($all as $t) {
					$t->used = date("Y-m-d H:i:s", time());
					UserTicket::save($t);
				}
			}
		}
	}
	
	public static function countAll() 
	{
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserTickets";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}
	
	public static function countByPlan($plan, $addQuery=null) 
	{
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserTickets WHERE plan='".$plan."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}

	public static function create($up) 
	{
		$db = Loader::db();
		$q = "INSERT INTO UserTickets (".UserTicket::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserTicket::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == 'created') {
				$up->$f = date("Y-m-d H:i:s", time());
			}
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);		
		//echo $q;
	}
	
	public static function save($up) 
	{
		$db = Loader::db();
		$q = "UPDATE UserTickets SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(UserTicket::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
	}
	
	public static function generate($quantity, $user_id, $event_id, $location, $expiration=null) {
		$tickets = array();
		for($i = 0; $i < $quantity; $i++) {
			$t = new UserTicket();
			$t->user_id 	= $user_id;
			$t->event_id 	= $event_id;
			$t->location 	= $location;
			$t->expires 	= $expiration;
			
			$t = UserTicket::create($t);
			$tickets[] = $t;
		}
		return $tickets;
	}
}
