<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserProfile
//-----------------------------------------------------------------------------------------------------------------------------
class UserProfile extends Object { 

	public $uID 			= '';
	public $uHandle 		= '';
	public $uCustomerID  	= '';
	public $uHide 			= 0;
	public $uImageType 		= '';
	public $uFullName 		= ''; 
	public $uArtistName 	= '';
	public $uSummary		= '';
	public $uBio 			= '';
	public $uStyleTags 		= '';
	public $uEmail 			= '';
	public $uMobile 		= '';
	public $uWebsite 		= '';
	public $uBandcamp 		= '';
	public $uSoundcloud 	= '';
	public $uSesh			= '';
	public $uEmbed 			= '';
	public $uFeaturedTrack	= '';
	public $uEdited			= 0;
	public $uSendReminders	= 1;
	public $uNewsletter		= 1;
	public $uLocation		= '';
	public $uLocationAdmin	= '';
	public $uAdminPin		= 0;
	public $uReported		= 0;
	public $uIsMember		= 0;
	public $uHasAvatar		= 0;

	public static $Fields = array(
		"uID", "uHandle", "uCustomerID", "uHide", "uImageType", "uFullName", "uArtistName", "uSummary", "uBio", "uStyleTags", "uEmail", "uMobile", "uWebsite", "uBandcamp", 
		"uSoundcloud", "uFacebook", "uTwitter", "uInstagram", "uYoutube", "uSesh", "uEmbed", "uFeaturedTrack", "uEdited", "uSendReminders", "uReported",
		"uNewsletter", "uLocation", "uLocationAdmin", "uAdminPin", "uIsMember", "uHasAvatar");
	
	public static $ReservedHandles = array(
		'!drafts',
		'!stacks',
		'!trash',
		'ajax',
		'blog',
		'blogsearch',
		'dashboard',
		'download_file',
		'eventinfo',
		'events',
		'eventsxml',
		'feedback',
		'ipn',
		'login',
		'loginhelp',
		'manage',
		'members',
		'membership',
		'memberships',
		'moonclerk',
		'ondeck',
		'page_forbidden',
		'page_not_found',
		'problog',
		'profile',
		'purchase_complete',
		'register',
		'reminders',
		'stripetest',
		'validate',
		
		//
		'ems', 'ableton','about','access','account','accounting','active','add','adds','advanced','advertisers','affiliate','affiliates',
		'africa','airbnb','am','andrioid','anual','api','app','app-store','apple','application','appstore','artist','artist-spotlight',
		'artistspotlight','asia','asshole','austin','auth','authorize','beta','bitch','bitching','blog','booking','buy','cal','call',
		'canada','caption','cart','celeb','celebrities','chat','check-out','checkout','confirm','confirmedemail','confirmemail','confrence',
		'confrencecall','contact','contactus','contribute','crew','crop','cunt','customize','deactivatd','declined','delete','delete-profile',
		'deleteprofile','delivery','dev','developer','developers','direct-message','directions','directmessage','dl','dm','download','drive',
		'dropbox','drugs','edit','editprofile','europe','event','eventsummary','expanding','explore','facebook','fag','fake','featured',
		'feed','fm','forum','founders','fuck','gallery','gay','general','gethelp','git','git-hub','github','give-aways','giveaway',
		'giveaways','google','hack','hangout','harddick','help','hiring','history','homo','hooker','host','how-to','howto','hq',
		'imageuploader','inbox','insta','instagram','ios','itunes','jobs','jobs','law','law','learn','legal','listview','live',
		'live','live-stream','livestream','location','location','locations','locations','log-in','logic','login','lounge','lyft',
		'mac','mac','mail','map','map-view','mapview','meet_up','meet-up','meetup','merch','merchendise','messages','meth','model',
		'monthly','mothism','music','namm','news','northamerica','notifications','off-air','offair','offering','on_deck','on-air',
		'on-deck','onair','parites','partnership','partnerships','party','password','pay-pal','payments','payments','paypal','pending',
		'penis','photo','photos','pivotal','post','presets','presets','press','preview','prices','privacy-policy','privacypolicy',
		'private','prize','prizes','producer','producers','producers-social','producersocial','producerssocial','promo','promoter-list',
		'promoterlist','promotion','promotional','psx','public','pussy','question','questions','radio','rape','reactivate','read',
		'recomended','remix','remixcomp','remixcompetition','resources','retarded','review','reviews','rhq','save','scam','scan',
		'school','schools','scolarships','search','secret','serial','settings','share','shoppingcart','show','sign-up','signup',
		'sms','snort','social','socialites','software','soundcloud','sourse','splice','sponshorships','sponsor','staff','start',
		'story','stream','stripe','style','styleguide','subpac','sucks','support','sxsw','sync','tax','taxes','teach','teachers',
		'team','tech','terms-of-service','termsofservice','tickets','tickets','tits','tos','tour','trademark','troll','tutor',
		'tutors','tv','twat','twit','twitter','uber','unread','upcoming','upgrade','url','validation','vanity','vanity-url',
		'vanityurl','venue','verify','vetting','videos','vimeo','vip','volume','w9','webapp','website','win','wip','workshop','workshops',
		
		// Google bad words
		'4r5e','5h1t','5hit','a55','anal','anus','ar5e','arrse','arse','ass','ass-fucker','asses','assfucker','assfukka','asshole',
		'assholes','asswhole','a_s_s','b!tch','b00bs','b17ch','b1tch','ballbag','balls','ballsack','bastard','beastial','beastiality',
		'bellend','bestial','bestiality','bi+ch','biatch','bitch','bitcher','bitchers','bitches','bitchin','bitching','bloody','blow_job',
		'blowjob','blowjobs','boiolas','bollock','bollok','boner','boob','boobs','booobs','boooobs','booooobs','booooooobs','breasts',
		'buceta','bugger','bum','bunny_fucker','butt','butthole','buttmuch','buttplug','c0ck','c0cksucker','carpet_muncher','cawk','chink',
		'cipa','cl1t','clit','clitoris','clits','cnut','cock','cock-sucker','cockface','cockhead','cockmunch','cockmuncher','cocks',
		'cocksuck_','cocksucked_','cocksucker','cocksucking','cocksucks_','cocksuka','cocksukka','cok','cokmuncher','coksucka','coon',
		'cox','crap','cum','cummer','cumming','cums','cumshot','cunilingus','cunillingus','cunnilingus','cunt','cuntlick_','cuntlicker_',
		'cuntlicking_','cunts','cyalis','cyberfuc','cyberfuck_','cyberfucked_','cyberfucker','cyberfuckers','cyberfucking_','d1ck','damn',
		'dick','dickhead','dildo','dildos','dink','dinks','dirsa','dlck','dog-fucker','doggin','dogging','donkeyribber','doosh','duche',
		'dyke','ejaculate','ejaculated','ejaculates_','ejaculating_','ejaculatings','ejaculation','ejakulate','f_u_c_k','f_u_c_k_e_r',
		'f4nny','fag','fagging','faggitt','faggot','faggs','fagot','fagots','fags','fanny','fannyflaps','fannyfucker','fanyy','fatass',
		'fcuk','fcuker','fcuking','feck','fecker','felching','fellate','fellatio','fingerfuck_','fingerfucked_','fingerfucker_',
		'fingerfuckers','fingerfucking_','fingerfucks_','fistfuck','fistfucked_','fistfucker_','fistfuckers_','fistfucking_',
		'fistfuckings_','fistfucks_','flange','fook','fooker','fuck','fucka','fucked','fucker','fuckers','fuckhead','fuckheads',
		'fuckin','fucking','fuckings','fuckingshitmotherfucker','fuckme_','fucks','fuckwhit','fuckwit','fudge_packer','fudgepacker',
		'fuk','fuker','fukker','fukkin','fuks','fukwhit','fukwit','fux','fux0r','f_u_c_k','gangbang','gangbanged_','gangbangs_',
		'gaylord','gaysex','goatse','God','god-dam','god-damned','goddamn','goddamned','hardcoresex_','hell','heshe','hoar','hoare',
		'hoer','homo','hore','horniest','horny','hotsex','jack-off_','jackoff','jap','jerk-off_','jism','jiz_','jizm_','jizz','kawk',
		'knob','knobead','knobed','knobend','knobhead','knobjocky','knobjokey','kock','kondum','kondums','kum','kummer','kumming',
		'kums','kunilingus','l3i+ch','l3itch','labia','lmfao','lust','lusting','m0f0','m0fo','m45terbate','ma5terb8','ma5terbate',
		'masochist','master-bate','masterb8','masterbat*','masterbat3','masterbate','masterbation','masterbations','masturbate','mo-fo',
		'mof0','mofo','mothafuck','mothafucka','mothafuckas','mothafuckaz','mothafucked_','mothafucker','mothafuckers','mothafuckin',
		'mothafucking_','mothafuckings','mothafucks','mother_fucker','motherfuck','motherfucked','motherfucker','motherfuckers',
		'motherfuckin','motherfucking','motherfuckings','motherfuckka','motherfucks','muff','mutha','muthafecker','muthafuckker',
		'muther','mutherfucker','n1gga','n1gger','nazi','nigg3r','nigg4h','nigga','niggah','niggas','niggaz','nigger','niggers_',
		'nob','nob_jokey','nobhead','nobjocky','nobjokey','numbnuts','nutsack','orgasim_','orgasims_','orgasm','orgasms_','p0rn',
		'pawn','pecker','penis','penisfucker','phonesex','phuck','phuk','phuked','phuking','phukked','phukking','phuks','phuq',
		'pigfucker','pimpis','piss','pissed','pisser','pissers','pisses_','pissflaps','pissin_','pissing','pissoff_','poop','porn',
		'porno','pornography','pornos','prick','pricks_','pron','pube','pusse','pussi','pussies','pussy','pussys_','rectum','retard',
		'rimjaw','rimming','s_hit','s.o.b.','sadist','schlong','screwing','scroat','scrote','scrotum','semen','sex','sh!+','sh!t',
		'sh1t','shag','shagger','shaggin','shagging','shemale','shi+','shit','shitdick','shite','shited','shitey','shitfuck','shitfull',
		'shithead','shiting','shitings','shits','shitted','shitter','shitters_','shitting','shittings','shitty_','skank','slut','sluts',
		'smegma','smut','snatch','son-of-a-bitch','spac','spunk','s_h_i_t','t1tt1e5','t1tties','teets','teez','testical','testicle',
		'tit','titfuck','tits','titt','tittie5','tittiefucker','titties','tittyfuck','tittywank','titwank','tosser','turd','tw4t',
		'twat','twathead','twatty','twunt','twunter','v14gra','v1gra','vagina','viagra','vulva','w00se','wang','wank','wanker','wanky',
		'whoar','whore','willies','willy','xrated','xxx'
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserProfile::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByHandle
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByHandle($handle, $excludeUserID=null) {
		$q = "uHandle='".$handle."'";
		if($excludeUserID) {
			$q .= " AND uID != '".$excludeUserID."'";
		}
		$p = UserProfile::getOne($q);
		return $p;
	}
	
	public function setMetaData() {
		$meta = Loader::helper("meta");
		$meta->title 		= $this->uArtistName;//." :: Producers Social Member";
		$meta->description 	= trim($this->uSummary);
		
		if(!$this->uLocation) $this->uLocation = "LA";
		
		if(!$meta->description) {
			$meta->description = trim(str_replace("\"", "'", substr(strip_tags($this->uBio), 0, 200)));
		}
		
		Loader::model('user_subscription');
		$sub = UserSubscription::getByUserID($this->uID);
		if($sub && $sub->isActive()) {
			$meta->title .= " : Official Member";
			$meta->url = BASE_URL."/".$this->uHandle;
		}
		else {
			$meta->url = BASE_URL."/members/profile/".$this->uID;
		}
		$meta->image = $this->getAvatar(false, true);
		$size = getimagesize(str_replace(BASE_URL, DIR_BASE, $meta->image));
		if($size[0] < 200) {
			$meta->image = BASE_URL.'/files/avatars/avatar_'.strtolower($this->uLocation).'.jpg';
		}
	}

	public static function validateHandle($handle, $id) {
		if(!$handle || strlen($handle) < 3) {
			return false;
		}
		foreach(UserProfile::$ReservedHandles as $r) {
			if($handle == $r) {
				return false;
			}
		}
		$p = UserProfile::getByHandle($handle, $id);
		if($p) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public function getMemberSubscription() {
		Loader::model('user_subscription');
		$sub = UserSubscription::getByUserID($this->uID);
		return $sub;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByUserID($uID) {
		$db = Loader::db();
		$q = "SELECT * FROM UserProfiles WHERE uID='$uID' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserProfile();
			foreach(UserProfile::$Fields as $f) {
				//echo $f."=".$row[$f]."<br>";
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query="1=1") {
		$db = Loader::db();
		$q = "SELECT * FROM UserProfiles WHERE ".$query." LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserProfile();
			foreach(UserProfile::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getQuery
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getQuery($query) {
		$users = array();
		$db = Loader::db();
		if($r = $db->query($query)) {
			while($row = $r->FetchRow()) {
				$nu = new UserProfile();
				foreach(UserProfile::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCount
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getCount($query="1") {
		$count = 0;
		$db = Loader::db();
		$q = "SELECT COUNT(*) as total FROM UserProfiles WHERE ".$query;
		if($r = $db->query($q)) {
			$d = $r->FetchRow();
			$count = $d['total'];
		}
		return $count;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM UserProfiles ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserProfile();
				foreach(UserProfile::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllReported
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllReported() {
		return UserProfile::getAll("WHERE uReported > 0");
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: anyReported
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function anyReported() {
		return UserProfile::getCount("uReported > 0");
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		
		if($up->uHandle && !UserProfile::validateHandle($up->uHandle, $up->uID)) {
			$up->uHandle = null;	
		}
		
		$q = "INSERT INTO UserProfiles (".UserProfile::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserProfile::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "uSendReminders") {
				$up->$f = $up->$f ? 1 : 0;
			}
			else
			if($f == "uNewsletter") {
				$up->$f = $up->$f ? 1 : 0;
			}
			else
			if($f == "uFeaturedTrack") {
				$up->$f = UserProfile::truncateFeature($up->$f);
			}
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
		$up->ID = $db->Insert_ID();
		
		UserProfile::updateNewsletterSubscription($up);
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		
		if($up->uHandle && !UserProfile::validateHandle($up->uHandle, $up->uID)) {
			$up->uHandle = null;	
		}
		
		$q = "UPDATE UserProfiles SET ";
		$t = null;
		$up->uEdited = 1;
		$up->uHasAvatar = $up->hasAvatar() ? 1 : 0;
		if(!$up->uSummary) $up->uSummary = trim(str_replace("\"", "'", substr(strip_tags($up->uBio), 0, 200)));
		
		if(!$up->uArtistName) {
			$u = new User();
			$up->uArtistName = $u->Name;
		}
	
		foreach(UserProfile::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "uSendReminders") {
				$up->$f = $up->$f ? 1 : 0;
			}
			else
			if($f == "uFeaturedTrack" || $f == "uBio") {
				$up->$f = str_replace("xframe", "iframe", $up->$f);
			}
			else
			if($f == "uNewsletter") {
				$up->$f = $up->$f ? 1 : 0;
			}
			else
			if($f == "uEdited") {
				$up->$f = 1;
			}
			else
			if($f == "uFeaturedTrack") {
				$up->$f = UserProfile::truncateFeature($up->$f);
			}
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE uID=".$up->uID;
		$db->execute($q);		
		
		//echo $q;
		$q = "UPDATE Users SET uLocation='".$up->uLocation."', uTimezone='".Locations::getTimezoneName($up->uLocation)."' WHERE uID='".$up->uID."' LIMIT 1";
		$db->execute($q);
		
		UserProfile::updateNewsletterSubscription($up);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateNewsletterSubscription
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function updateNewsletterSubscription($up) {
		$mailchimp = Loader::helper('mailchimp');
		$mailchimp->startup();
		
		if($up->uNewsletter) {
			$mailchimp->subscribe($up->uID, false);
		}
		else {
			$mailchimp->unsubscribe($up->uID, false);
		}
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function delete($up) {
		$db = Loader::db();
		$q = "DELETE FROM UserProfiles WHERE uID=".$up->uID;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: createForUser
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function createForUser($id) {
		$u = User::getByUserID($id);
		$p = new UserProfile();
		$p->uID = $id;
		UserProfile::create($p);
		return $p;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: completeURL
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function completeURL($input, $url) {
		$final = $input;
		if(strpos($input, "http") === false) {
			$final = str_replace("###", $input, $url);
		}
		return $final;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: truncateFeature
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function truncateFeature($feature) {
	// Double-check that there isn't a second iframe
		if(strlen($feature) > 10) {
			$a = strpos($feature, "<iframe", 2);
			if($a !== false) {
				$feature = substr($feature, 0, $a-1);
			}
		}
		return $feature;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: hasAvatar
	//-----------------------------------------------------------------------------------------------------------------------------
	public function hasAvatar() {
		$hasFile = false;
		$src = '/files/avatars/'.$this->uID.'.jpg';
		$file = DIR_BASE.$src;
		//if(ADMIN) echo $file."<br>";
		if(file_exists($file)) {
			$hasFile = true;
		}
		return $hasFile;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateHasAvatar
	//-----------------------------------------------------------------------------------------------------------------------------
	public function updateHasAvatar() {
		$this->uHasAvatar = $this->hasAvatar() ? 1 : 0;
		UserProfile::save($this);
	}
	
 	//-----------------------------------------------------------------------------------------------------------------------------
	//:: createAvatarSmall
	//-----------------------------------------------------------------------------------------------------------------------------
	public function createAvatarSmall() {
		// Create small thumbnail
		$filename = DIR_FILES_AVATARS.'/'.$this->uID.'.jpg';
		if(file_exists($filename)) {
			$info = getimagesize($filename);
			$fullwidth = $info[0];
			$fullheight = $info[1];
			
			$size = 140;
			$dstname = DIR_FILES_AVATARS.'_small/'.$this->uID.'.jpg';
			$src = @imagecreatefromjpeg($filename);
			if(!$src) {
				$src = @imagecreatefrompng($filename);
				imagejpeg($src, $filename);
			}
			$dst = imagecreatetruecolor($size, $size);
			imagecopyresampled($dst, $src, 0, 0, 0, 0, $size, $size, $fullwidth, $fullheight);
			imagejpeg($dst, $dstname);
		}
 	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAvatar
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getAvatar($asHtml=true, $fullsize=false, $asLink=true) {
		$dir = "avatars_small";
		if($fullsize) {
			$dir = "avatars";	
		}
		$src = '/files/'.$dir.'/'.$this->uID.'.jpg';
		$file = DIR_BASE.$src;
		if(!file_exists($file)) {
			$src ='/files/'.$dir.'/avatar_'.strtolower($this->uLocation).'.jpg';
		}
		$img = $file;
		
		if(isset($_REQUEST['updated'])) $src .= "?id=".uniqid();
		if($asHtml) {
			$user = Loader::helper('user');
			$user->getLocations();
			$img = "";
			$memberType = 'standard_user';
			if($user->isMember()) {
				$memberType = 'premium_member';
			}
			$img .= "<div class='member_avatar ".$memberType."'>";
			if(!isset($user->locations[$this->uLocation])) {
				$img .= "<div class='error'>Invalid Location:".$this->uLocation."</div>";
			}
			$img .= "<div class='members_profile_location_tag' style='background:#".$user->locations[$this->uLocation]->Color."'>".$this->uLocation."</div>";
			if($asLink) {
				$url = $this->getURL();
				$img .= "<a href='".$url."'>";
			}
			$img .= "<img class='profile_image' src='".$src."' />";
			if($asLink) {
				$img .= "</a>";
			}
			//if($user->isMember()) {
				//$img .= "MEMBER";
			//}
			$img .= "</div>";
			return $img;
		}
		else {
			$src = BASE_URL.$src;	
		}
		return $src;
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAvatarLightbox
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getAvatarLightbox() {
		$thumb = $this->getAvatar(false);//'/files/avatars_small/'.$this->uID.'.jpg';
		$fullsize = $this->getAvatar(false, true);//'/files/avatars/'.$this->uID.'.jpg';
		$url = $this->getURL();
		$img = "";
		$memberType = 'standard_user';
		$user = Loader::helper('user');
		$user->getLocations();
		if($user->isMember()) {
			$memberType = 'premium_member';
		}
		$u = new User();
		$isOwner = $u->uID == $this->uID;
		$img .= "<div class='profile_image_box ".$memberType."'>";
		if(!isset($user->locations[$this->uLocation])) {
			$img .= "<div class='error'>Invalid Location:".$this->uLocation."</div>";
		}
		$img .= "<div class='members_profile_location_tag' style='background:#".$user->locations[$this->uLocation]->Color."'>".$this->uLocation."</div>";
		$img .= "<img class='profile_image' src='".$thumb."' data-featherlight='".$fullsize."'/>";
		$user = Loader::helper('user');
		if($isOwner || $user->isSuperAdmin()) {
			$uid = $user->isSuperAdmin() ? "?uid=".$user->id : "";
			$img .= "<a class='profile_image_edit button white nopadding' href='/profile/avatar".$uid."'>Change Avatar <i class='fa fa-upload' aria-hidden='true'></i></a>";
		}
		$img .= "</div>";
		return $img;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getURL
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getURL() {
		$user = Loader::helper('user');
		if($user->me->uID == $this->uID && !$this->hasAvatar()) {
			$url = '/profile/avatar';
		}
		else {
			$sub = null;
			if($this->uHandle) {
				$sub = UserSubscription::getByUserID($this->uID);
			}
			if($sub && $sub->isActive() && $this->uHandle) {
				$url = '/'.$this->uHandle;
			}
			else {
				$url = '/members/profile?uid='.$this->uID;
			}
		}
		return $url;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getDefultAvatar
	//-----------------------------------------------------------------------------------------------------------------------------
	static public function getDefultAvatar($asImg=true) {
		$src ='/files/avatars/avatar.jpg';
		$img = DIR_BASE.$src;
		
		$img = $src;
		if($asImg) {
			$img = "<img class='profile_image' src='".$src."' />";
		}
		return $img;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isAdmin
	//-----------------------------------------------------------------------------------------------------------------------------
	static public function isAdmin() {
		$isAdmin = false;
		$u = new User();
		if(!$u || !$u->isLoggedIn()) {
			$u = null;
		}
		if($u) {
			if($u->uID == 1) {
				$isAdmin = true;
			}
			else {
				$profile = UserProfile::getByUserID($u->uID);
				if($profile) {
					if($profile->uLocationAdmin == "HQ") {
						$isAdmin = true;
					}
					else
					if($profile->uLocationAdmin) {
						$isAdmin = true;
					}
				}
			}
		}
		return $isAdmin;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isLocationAdmin
	//-----------------------------------------------------------------------------------------------------------------------------
	static public function isLocationAdmin($loc=null) {
		$isAdmin = false;
		$u = new User();
		if(!$u || !$u->isLoggedIn()) {
			$u = null;
		}
		if($u) {
			$profile = UserProfile::getByUserID($u->uID);
			if($profile) {
				$adminLoc = strtoupper($profile->uLocationAdmin);
				if($adminLoc == "HQ") {
					$isAdmin = true;
				}
				else
				if($loc) {
					if($adminLoc == $loc) {
						$isAdmin = true;
					}
				}
				else 
				if($adminLoc) {
					$isAdmin = true;
				}
			}
		}
		return $isAdmin;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isSuperAdmin
	//-----------------------------------------------------------------------------------------------------------------------------
	public function isSuperAdmin() {
		$isAdmin = false;
		$u = User::getByUserID($this->uID);
		if($u && $u->isLoggedIn()) {
			if($u->isSuperUser()) {
				$isAdmin = true;
			}
			else
			if($this->uLocationAdmin == "HQ") {
				$isAdmin = true;
			}
		}
		return $isAdmin;
	}
}
