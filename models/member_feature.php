<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: MemberFeature
//-----------------------------------------------------------------------------------------------------------------------------
class MemberFeature extends Object { 

	public $id 				= 0;
	public $handle 			= '';
	public $ondeck_id 		= '';
	public $type 			= '';
	public $status 			= 'active'; //inactive, hidden
	public $featured 		= 1;
	public $name 			= '';
	public $summary 		= '';
	public $content 		= '';
	public $membersonly 	= '';
	public $nonmembersonly 	= '';
	public $submit_notify	= '';
	public $submit_members_only		= 0;
	public $submit_location_only	= 0;
	public $submit_limit	= 2;
	public $tags			= '';
	public $filekey			= '';
	public $link 			= '';
	public $views 			= 0;
	public $tracking_a		= 0;
	public $tracking_b		= 0;
	public $tracking_c		= 0;
	public $downloads		= 0;
	public $published 		= '0000-00-00 00:00:00';
	public $expiration 		= '0000-00-00 00:00:00';
	public $location 		= 'HQ';
	public $ordernum		= 0;
	public $notes			= '';
	public $createdby 		= 0;
	public $modifiedby 		= 0;
	public $created 		= '0000-00-00 00:00:00';
	public $modified 		= '0000-00-00 00:00:00';
	
	static private $typesList = array(
		'blog' 			=> "Blog Article",
		'samples' 		=> "Sample Pack",
		'tutorials' 	=> "Tutorial",
		'software' 		=> "Plugin / Software",
		'offers'		=> "Coupon / Special Offer",
		'submissions'	=> "Track Submission Form"
	);
	static private $typesPluralList = array(
		'blog' 			=> "Blog Articles",
		'samples' 		=> "Sample Packs",
		'tutorials' 	=> "Tutorials",
		'software' 		=> "VST Plugins / Software",
		'offers'		=> "Coupons / Special Offers",
		'submissions'	=> "Track Submission Forms"
	);
	
	static private $statusList = array(
		'active' 	=> "Active",
		'inactive' 	=> "Inactive",
		'hidden'	=> "Hidden",
		'archived'	=> "Archived"
	);
	
	public static $Fields = array("id", "handle", "ondeck_id", "type", "status", "featured", "name", "summary", "content", "membersonly", "nonmembersonly", "tags", "filekey", 
		"link", "views", "downloads", "published", "expiration", "location", "ordernum", "notes", "createdby", "modifiedby", "created", "modified",
		"tracking_a", "tracking_b", "tracking_c", "submit_notify", "submit_members_only", "submit_location_only", "submit_limit");
	
	public static function getFieldsList() 
	{
		return implode(", ", MemberFeature::$Fields);
	}
	
	public function getTypeName($plural=false) 
	{
		if($plural) {
			return MemberFeature::$typesPluralList[$this->type];
		}
		else {
			return MemberFeature::$typesList[$this->type];
		}
	}
	
	public function getHandle() 
	{
		if(!$this->handle) {
			$this->handle = strtolower($this->name);
			$this->handle = str_replace(" ", "_", $this->handle);
			$this->handle = preg_replace("/[^a-zA-Z0-9_-]+/", "", $this->handle);
			MemberFeature::save($this);
		}
		return $this->handle;
	}
	
	public function getURL() {
		$path = "members";
		if($this->type == "blog") {
			$path = "blog";
		}
		else {
			$path .= "/".$this->type;	
		}
		$url = "/".$path."/".$this->getHandle();
		return $url;
	}
	
	public function setMetaData($path='members') {
		$meta = Loader::helper("meta");
		$meta->title 		= $this->name;
		$meta->description 	= $this->summary;
		$meta->url 			= BASE_URL."/".$path."/".$this->getHandle();
		$meta->image 		= BASE_URL."/images/features/".$this->id."_feature.jpg";
	}
	
	public function getStatusName() 
	{
		return MemberFeature::$statusList[$this->status];
	}
	
	public function isActive() 
	{
		return $this->status == 'active';
	}
	
	public function isExpired() 
	{
		if($this->expiration && $this->expiration != "0000-00-00 00:00:00") {
			return strtotime($this->expiration) <= time();
		}
		return false;
	}
	
	public function hasExpiration() 
	{
		return $this->expiration && $this->expiration != "0000-00-00 00:00:00";
	}
	
	public function getExpirationDate($long=false) 
	{
		return MemberFeature::formatDate($this->expiration, $long);
	}
	
	public function isPublished() 
	{
		if($this->published && $this->published != "0000-00-00 00:00:00") {
			return strtotime($this->published) <= time();
		}
		return true;
	}
	
	public function hasPublishedDate() 
	{
		return $this->published && $this->published != "0000-00-00 00:00:00";
	}
	
	public function getPublishedDate($long=false) 
	{
		return MemberFeature::formatDate($this->published, $long);
	}
	
	public function getContent($isMember=false) 
	{
		$content = $this->content;
		$content = str_replace("../../", "/", $content);
		$content = str_replace("http:/", "http://", $content);
		$content = str_replace("https:/", "https://", $content);
		$content = str_replace(":///", "://", $content);
		//$content = str_replace("src=\"//", "src=\"http://", $content);
		
		$p1 = strpos($content, "{{");
		while($p1 !== false) {
			$a = substr($content, 0, $p1);
			$p2 = strpos($content, "}}", $p1);
			$b = substr($content, $p1+2, $p2);
			$c = substr($content, $p2+2);
			
			$insert = "";
			$info = explode(":", $b);
			if($info[0] == "profile_summary") {
				$insert = "";//$user->displaySummary($id, false, $m['link'], $m['user'], $m['profile'], $m['sub']);
			}
			
			$content = $a.$insert.$c;
			$p1 = strpos($content, "[[");
		}
		/*
		if(!$isMember) {
			//$content = preg_replace('|https?://www\.[a-z\.0-9]+|i', '', $content);
			$content = preg_replace("/<a[^>]+>/i", "", $content);
			$regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
			$content = preg_replace($regex, ' ', $content);
			
			$replace = '"(https?://.*)(?=;)"';
			$content = preg_replace($replace, '', $content);
		}
		*/
		return $content;
	}
	
	public function getNext($sameType=true) 
	{
		// Assume descending order by date
		$q = "ordernum < '".$this->ordernum."' AND id !=".$this->id;
		if($sameType) {
			$q .=  " AND type='".$this->type."'";	
		}
		return MemberFeature::getOne($q);
	}
	
	public function getPrev() 
	{
		$q = "ordernum > '".$this->ordernum."' AND id !=".$this->id;
		if($sameType) {
			$q .=  " AND type='".$this->type."'";	
		}
		return MemberFeature::getOne($q);
	}
	
	public static function formatDate($date, $long=false) 
	{
		$formatted = "";
		if($date && $date != '0000-00-00 00:00:00') {
			$d = strtotime($date);	
			$year = date("Y", $d);
			$thisYear = date("Y", time());

			if($long) {
				$formatted .= date("D", $d)." ";	
			}
			
			$formatted .= date("M j", $d);
			if($thisYear != $year) {
				$formatted .= " ".$year;	
			}
			if($long) {
				$formatted .= " at ".date("h:ia", $d);	
			}
		}
		return $formatted;
	}
	
	public static function getTypesList() {
		return MemberFeature::$typesList;
	}
	
	public static function getStatusList() {
		return MemberFeature::$statusList;
	}
	
	public static function getOne($query=null, $order=null) 
	{
		$db = Loader::db();
		
		if(!$query) $query = "1=1";
		$q = "SELECT * FROM MemberFeatures WHERE ".$query;
		if($order) $q .= " ".$order;
		$q .= " LIMIT 1";
		
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new MemberFeature();
			foreach(MemberFeature::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getByID($id) {
		$q = "id='$id'";
		return MemberFeature::getOne($q);
	}

	public static function validateHandle($handle, $id) {
		if(!$handle || strlen($handle) < 3) {
			return false;
		}
		Loader::model('user_profile');
		foreach(UserProfile::$ReservedHandles as $r) {
			if($handle == $r) {
				return false;
			}
		}
		$p = MemberFeature::getByHandle($handle, null, $id);
		if($p) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static function getByHandle($handle, $id = null, $notid = null) {
		$q = "handle='$handle'";
		if($id) {
			$q .= " AND id='".$id."'";	
		}
		if($notid) {
			$q .= " AND id!='".$notid."'";	
		}
		return MemberFeature::getOne($q);
	}
	
	public static function getCount($query="1=1") 
	{
		$count = 0;
		$db = Loader::db();
		$q = "SELECT count(*) as count FROM MemberFeatures WHERE ".$query;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}
	
	public static function getAll($query=null, $order="published DESC, created DESC") 
	{
		$db = Loader::db();
		
		$all = array();
		if(!$query) $query = "1=1";
		$q = "SELECT * FROM MemberFeatures WHERE ".$query;		
		if($order) $q .= " ORDER BY ".$order;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if($row) {
					$nu = new MemberFeature();
					foreach(MemberFeature::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				$all[] = $nu;
			}
		}
		return $all;
	}
	
	public static function getAllByType($type) 
	{
		$q = "type='".$type."'";
		return MemberFeature::getAll($q);
	}

	public static function create($up) 
	{
		$user = Loader::helper('user');
		$db = Loader::db();
		$q = "INSERT INTO MemberFeatures (".MemberFeature::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		
		$up->created = date("Y-m-d H:i:s", time());
		$up->createdby = $user->id;
		$up->modified = date("Y-m-d H:i:s", time());
		$up->modifiedby = $user->id;
		
		if($up->published && $up->published != "0000-00-00 00:00:00") {
			$up->published = date("Y-m-d H:i:s", strtotime($up->published));
		}
		else {
			$up->published = date("Y-m-d H:i:s", time());
		}
		$up->filekey = uniqid();	
		
		foreach(MemberFeature::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
		$up->id = $db->Insert_ID();
		Loader::model('user_log');
		UserLog::record("MemberFeatures", $up->id, "created:".$up->toString());
		return $up;
	}
	
	public static function save($up) 
	{ 
		//if(ADMIN) echo "HANDLE:".$up->handle."<br>";
		if($up->handle) {
			if(!MemberFeature::validateHandle($up->handle, $up->id)) {
				$up->handle = null;	
			}
		}
		$user = Loader::helper('user');
		$db = Loader::db();
		$q = "UPDATE MemberFeatures SET ";
		$t = null;
		$up->uEdited = 1;
		if($up->created == "0000-00-00 00:00:00") {
			$up->created = date("Y-m-d H:i:s", time());
			$up->createdby = $user->id;
		}
		$up->modified = date("Y-m-d H:i:s", time());
		$up->modifiedby = $user->id;
		
		if($up->published && $up->published != "0000-00-00 00:00:00") {
			$up->published = date("Y-m-d H:i:s", strtotime($up->published));
		}
		else {
			$up->published = date("Y-m-d H:i:s", time());
		}
		if(!$up->filekey) {
			$up->filekey = uniqid();	
		}
		
		foreach(MemberFeature::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		//if(ADMIN) {
		//	echo $q;
		//	die();
		//}
		
		$db->execute($q);
		Loader::model('user_log');
		UserLog::record("MemberFeatures", $up->id, "updated:".$up->toString());
		return $up;
	}
	
	public function clearStats() 
	{
		$db = Loader::db();
		$this->views = 0;
		$this->tracking_a = 0;
		$this->tracking_b = 0;
		$this->tracking_c = 0;
		$q = "UPDATE MemberFeatures SET views=0, tracking_a=0, tracking_b=0, tracking_c=0 WHERE id='".$this->id."'";
		$db->execute($q);
	}
	
	public function incrementViewCount() {
		$tracking = null;
		if(isset($_REQUEST['tr'])) {
			$_REQUEST['tr'] = strtolower($_REQUEST['tr']);
			if($_REQUEST['tr'] == "a") {
				$this->tracking_a++;
				$tracking = ", tracking_a=".$this->tracking_a;
			}
			else
			if($_REQUEST['tr'] == "b") {
				$this->tracking_b++;
				$tracking = ", tracking_b=".$this->tracking_b;
			}
			else
			if($_REQUEST['tr'] == "c") {
				$this->tracking_c++;
				$tracking = ", tracking_c=".$this->tracking_c;
			}
		}
		$db = Loader::db();
		$this->views++;
		$q = "UPDATE MemberFeatures SET views=".$this->views." ".$tracking." WHERE id='".$this->id."'";
		$db->execute($q);
	}
	
	public static function deleteByID($id) {
		$db = Loader::db();
		$q = "DELETE FROM MemberFeatures WHERE id='".$id."'";
		$db->execute($q);
		//echo $q;
		Loader::model('user_log');
		UserLog::record("MemberFeatures", $id, "deleted:".$id);
	}
	
	public static function uploadFiles() {
		$out = "";
		$user = Loader::helper('user');
		if($user->isSuperAdmin()) {
			if($_REQUEST['item']) {
				$item = MemberFeature::getByID($_REQUEST['item']);
				if($item) {
					$dir = DIR_BASE.'/files/features/'.$item->filekey;
					if(!file_exists($dir)) {
						mkdir($dir);
					}
					//print_r($_FILES['item_upload_file']);
					$out .= "<div class='clear pad-top alert alert-success' rel='fadeaway'><h2>File upload complete!</h2>";
					for($x = 0; $x < count($_FILES['item_upload_file']); $x++) {
						if($_FILES['item_upload_file']["name"][$x]) {
							$dst = $dir."/".$_FILES['item_upload_file']["name"][$x];
							move_uploaded_file($_FILES['item_upload_file']["tmp_name"][$x], $dst);
							$out .= "<div class='clear small'>".$dst."</div>";
						}
					}
					$out .= "</div>";
				}
				else {
					$out .= "Error: Failed loading the item: ".$_REQUEST['item'];	
				}
			}
			else {
				$out .= "Error: Invalid item id.";	
			}
		}
		else {
			$out .= "Error: You do not have sufficient privileges to upload files for this item.";	
		}
		return $out;
	}
		
	public static function deleteFile($item_id, $filename) {
		$out = "";
		if($item_id && $filename) {
			$item = MemberFeature::getByID($item_id);
			if($item) {
				$dir = DIR_BASE.'/files/features/'.$item->filekey."/";
				$filepath = $dir.$filename;
				if(file_exists($filepath)) {
					unlink($filepath);
					$out = "<div class='alert alert-success' rel='fadeaway'>The file '".$filename."' has been deleted.</div>";
				}
			}
			else {
				$out = "<div class='alert alert-warning' rel='fadeaway'>The item id '".$item_id."' is invalid.</div>";
			}
		}
		else {
			$out = "<div class='alert alert-warning' rel='fadeaway'>Missing the item id '".$item_id."' or filename '".$filename."'.</div>";
		}
		return $out;
	}
	
	public function generateOnDeckBlog($ondeck) 
	{
		Loader::model("on_deck");
		Loader::model("user_on_deck");
		Loader::model("user_on_deck_entry");
		
		$usersOnDeck = $ondeck->getUsersOnDeck();
		
		$t = strtotime($ondeck->EventDate);
		$m = date("M", $t);
		$d = date("D", $t);
		$y = date("Y", $t);
		$dayOfWeek = date("l", $t);
		
		$this->published	= $ondeck->EventDate;
		$this->status 	= 'active';
		
		$this->name 	= Locations::getName($ondeck->Location)." ".$d." ".$m." ".date("j", $t).", ".$y;
		$this->ondeck_id = $ondeck->ID;
		$this->handle 	= strtolower($this->name);
		$this->handle 	= str_replace(" ", "_", $this->handle);
		$this->handle 	= preg_replace("/[^a-zA-Z0-9_-]+/", "", $this->handle);
		
		$this->type 	= 'blog';
		$this->location	= $ondeck->Location;
		$this->tags 	= 'social, '.Locations::getName($ondeck->Location).", ".$ondeck->Location;
		
		$this->summary 	= "The ".Locations::getName($ondeck->Location)." Producers Social ";//.$m." ".$d.date("S", $t);
		$this->summary .= "had ";
		
		$count = count($usersOnDeck['all']);
		if($count < 7) {
			$this->summary .= "a cozy";
		}
		else
		if($count <= 15) {
			$this->summary .= "a decent";
		}
		else
		if($count <= 25) {
			$this->summary .= "a great";
		}
		else {
			$this->summary .= "an amazing";
		}
		$this->summary .= " turnout ".$dayOfWeek." with a total of ".$count." people presenting a new original tune.";
		$this->content = $this->summary;
		$this->content .= "<div class='blog_ondeck_members'>";
		
		$this->content .= "<table id='ondeck_list_table'>";
		$this->content .= "<tbody>";
		$this->content .= "<tr>";
		$this->content .= "<th class='ondeck_list_num'>#</th>";
		$this->content .= "<th class='ondeck_list_avatar'>&nbsp;</th>";
		$this->content .= "<th class='ondeck_list_name'>Artist Name</th>";
		$this->content .= "<th class='ondeck_list_start'>Start</th>";
		$this->content .= "<th class='ondeck_list_end'>End</th>";
		$this->content .= "<th class='ondeck_list_controls'>&nbsp;</th>";
		$this->content .= "</tr>";
		$this->content .= "<tr>";
		
		$i = 1;
		foreach($usersOnDeck['all'] as $u) {
			$this->content .= "<tr>";
			$this->content .= "<td class='ondeck_list_num'>";
			$this->content .= "<div class='ondeck_list_num_circle'>".$i."</div>";
			$this->content .= "</td>";
			$a = "<a href='javascript:;' onclick='return ps.profile.popup(".$u->UserID.");'>";
			$p = UserProfile::getByUserID($u->UserID);
			if($p && $p->hasAvatar()) {
				$img = "<img src='/files/avatars/".$u->UserID.".jpg'>";
			}
			else {
				$img = "<img src='/files/avatars/avatar.jpg'>";
			}
			$ud = UserOnDeckEntry::getByUserOnDeckID($u->ID, 1);
			$name = $u->Name;
			$this->content .= "<td class='ondeck_list_avatar blog'>".$a.$img."</a></td>";
			$this->content .= "<td class='ondeck_list_name '>".$a.$name."</a></td>";
			if($ud) {
				$this->content .= "<td class='ondeck_list_start'>".date("h:ia", strtotime($ud->StartTime))."</td>";
				$this->content .= "<td class='ondeck_list_end'>".date("h:ia", strtotime($ud->EndTime))."</td>";
			}
			else {
				$this->content .= "<td class='ondeck_list_start'>--:--</td>";
				$this->content .= "<td class='ondeck_list_end'>--:--</td>";
			}
			//$this->content .= "<td class='ondeck_list_controls'>&nbsp;</td>";
			$this->content .= "</tr>";
			/*
			$this->content .= "<div class='blog_ondeck_member col-lg-3 col-md-4 col-xs-6'>";
			//$this->content .= "<a href='/members/profile/".$u->UserID."'>";
			$this->content .= "<a href='javascript:;' onclick='return ps.profile.popup(".$u->UserID.");'>";
			$this->content .= "<div class='blog_ondeck_member_name'>".$u->Name."</div>";
			$this->content .= "</a>";					
			$this->content .= "</div>";
			*/
			$i++;
		}
		$this->content .= "</tr>";
		$this->content .= "</tbody>";
		$this->content .= "</table>";
		$this->content .= "</div>";					
		$this->content .= "<a href='/ondeck?odid=".$_REQUEST['ondeck']."' class='button white'>View the Session</a>";					
		//$this->published = date("Y-m-d H:i:s", time());
		
		$this->notes = "";
		//$this->notes = "<a class='button white medium' href='/manage/features/edit/".$this->id."?ondeck=".$_REQUEST['ondeck']."&rebuild=true'>Admin Rebuild Post</a>";
	}
	
	public function toString() {
		$out = "id:".$this->id."\n";
		$out = "type:".$this->type."\n";
		$out = "status:".$this->status."\n";
		return $out;
	}
}
