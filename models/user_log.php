<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserLog
//-----------------------------------------------------------------------------------------------------------------------------
class UserLog extends Object { 

	public $id 				= '';
	public $user_id 		= '';
	public $item_id 		= '';
	public $type 			= '';
	public $note 			= '';
	public $created 		= "0000-00-00 00:00:00";
	
	public static $Fields = array("id", "user_id", "item_id", "type", "note", "created");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserLog::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query=null, $order=null) {
		$db = Loader::db();
		$q = "SELECT * FROM UserLogs WHERE 1=1";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserLog();
			foreach(UserLog::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null, $order=null) {
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserLogs WHERE 1=1";
		if($query) $q .= " AND (".$query.")";
		if($order) $q .= " ORDER BY ".$order;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserLog();
					foreach(UserLog::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
			}
			$all[] = $nu;
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByUserID($user_id) {
		$all = UserLog::getAll("user_id='".$user_id."'");
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByItemID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByItemID($item_id) {
		$all = UserLog::getAll("item_id='".$item_id."'");
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: record
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function record($type=null, $item_id, $note=null, $user_id=null) {
		if(!$user_id) {
			$user = Loader::helper('user');
			$user_id = $user->id;
		}
		
		$r = new UserLog();
		$r->item_id 	= $item_id;
		$r->user_id	 	= $user_id;
		$r->note 		= $note;
		$r->type 		= $type;
		
		UserLog::create($r);
		
		return $r;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$up->created = date("Y-m-d H:i:s", time());
		
		$q = "INSERT INTO UserLogs (".UserLog::getFieldsList().") ";
		$q .= "VALUES (";
		$t = "";
		foreach(UserLog::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) { 
		$db = Loader::db();
		$q = "UPDATE UserLogs SET ";
		$t = "";
		foreach(UserLog::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
	}
}
