<?php defined('C5_EXECUTE') or die("Access Denied.");

/**
 * @package Users
 * @author Andrew Embler <andrew@concrete5.org>
 * @copyright  Copyright (c) 2003-2008 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 *
 */

/**
 * The user object deals primarily with logging users in and session-related activities.
 *
 * @package Users
 * @category Concrete
 * @copyright  Copyright (c) 2003-2008 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 *
 */

	class User extends Concrete5_Model_User { 
	
		public static function getByUserID($uID, $login = false, $cacheItemsOnLogin = true) {
			$db = Loader::db();
			$v = array($uID);
			$q = "SELECT * FROM Users WHERE uID = ? LIMIT 1";
			$r = $db->query($q, $v);
			$row = $r ? $r->FetchRow() : null;
			$nu = null;
			if ($row) {
				$nu = new User();
				$nu->uID = $row['uID'];
				$nu->uName = $row['uName'];
				$nu->uEmail = $row['uEmail'];
				$nu->uIsActive = $row['uIsActive'];
				$nu->uDefaultLanguage = $row['uDefaultLanguage'];
				$nu->uLastLogin = $row['uLastLogin'];
				$nu->uTimezone = $row['uTimezone'];
				$nu->uGroups = $nu->_getUserGroups(true);
				$nu->superUser = ($nu->getUserID() == USER_SUPER_ID);
				if ($login) {
					User::regenerateSession();
					$_SESSION['uID'] = $row['uID'];
					$_SESSION['uName'] = $row['uName'];
					$_SESSION['uBlockTypesSet'] = false;
					$_SESSION['uGroups'] = $nu->uGroups;
					$_SESSION['uLastOnline'] = $row['uLastOnline'];
					$_SESSION['uTimezone'] = $row['uTimezone'];
					$_SESSION['uDefaultLanguage'] = $row['uDefaultLanguage'];
					if ($cacheItemsOnLogin) { 
						Loader::helper('concrete/interface')->cacheInterfaceItems();
					}
					$nu->recordLogin();
				}
			}
			return $nu;
		}
	
		public static function getByEmail($email) {
			$db = Loader::db();
			$q = "SELECT * FROM Users WHERE uEmail = '".$email."' LIMIT 1";
			$r = $db->query($q);
			$row = $r ? $r->FetchRow() : null;
			$nu = null;
			if ($row) {
				$nu = new User();
				$nu->uID = $row['uID'];
				$nu->uName = $row['uName'];
				$nu->uEmail = $row['uEmail'];
				$nu->uIsActive = $row['uIsActive'];
				$nu->uDefaultLanguage = $row['uDefaultLanguage'];
				$nu->uLastLogin = $row['uLastLogin'];
				$nu->uTimezone = $row['uTimezone'];
				$nu->uGroups = $nu->_getUserGroups(true);
				$nu->superUser = ($nu->getUserID() == USER_SUPER_ID);
			}
			return $nu;
		}
	
		public static function getByUsername($username) {
			$db = Loader::db();
			$q = "SELECT * FROM Users WHERE uName = '".$username."' LIMIT 1";
			$r = $db->query($q);
			$row = $r ? $r->FetchRow() : null;
			$nu = null;
			if ($row) {
				$nu = new User();
				$nu->uID = $row['uID'];
				$nu->uName = $row['uName'];
				$nu->uEmail = $row['uEmail'];
				$nu->uIsActive = $row['uIsActive'];
				$nu->uDefaultLanguage = $row['uDefaultLanguage'];
				$nu->uLastLogin = $row['uLastLogin'];
				$nu->uTimezone = $row['uTimezone'];
				$nu->uGroups = $nu->_getUserGroups(true);
				$nu->superUser = ($nu->getUserID() == USER_SUPER_ID);
			}
			return $nu;
		}
		
		public function __construct() {
			$args = func_get_args();
			
			if (isset($args[1])) {
				// first, we check to see if the username and password match the admin username and password
				// $username = uName normally, but if not it's email address
				
				$username = $args[0];
				$password = $args[1];
				if (!$args[2]) {
					$_SESSION['uGroups'] = false;
				}
				$v = array($username);
				if (defined('USER_REGISTRATION_WITH_EMAIL_ADDRESS') && USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) {
					$q = "select uID, uName, uEmail, uIsActive, uIsValidated, uTimezone, uDefaultLanguage, uPassword from Users where uEmail = ?";
				} else {
					$q = "select uID, uName, uEmail, uIsActive, uIsValidated, uTimezone, uDefaultLanguage, uPassword from Users where uName = ?";
				}
				$db = Loader::db();
				$r = $db->query($q, $v);
				if ($r) {
					$row = $r->fetchRow(); 
					$pw_is_valid_legacy = (defined('PASSWORD_SALT') && User::legacyEncryptPassword($password) == $row['uPassword']);
					$pw_is_valid = $pw_is_valid_legacy || $this->getUserPasswordHasher()->checkPassword($password, $row['uPassword']); 
					if ($row['uID'] && $row['uIsValidated'] === '0' && defined('USER_VALIDATE_EMAIL_REQUIRED') && USER_VALIDATE_EMAIL_REQUIRED == TRUE) {
						$this->loadError(USER_NON_VALIDATED);
					} else if ($row['uID'] && $row['uIsActive'] && $pw_is_valid) {
						$this->uID = $row['uID'];
						$this->uName = $row['uName'];
						$this->uIsActive = $row['uIsActive'];
						$this->uTimezone = $row['uTimezone'];
						$this->uDefaultLanguage = $row['uDefaultLanguage'];
						$this->uGroups = $this->_getUserGroups($args[2]);
						if ($row['uID'] == USER_SUPER_ID) {
							$this->superUser = true;
						} else {
							$this->superUser = false;
						}
						$this->recordLogin();
						if (!$args[2]) {
							User::regenerateSession();
							$_SESSION['uID'] = $row['uID'];
							$_SESSION['uName'] = $row['uName'];
							$_SESSION['uEmail'] = $row['uEmail'];
							$_SESSION['superUser'] = $this->superUser;
							$_SESSION['uBlockTypesSet'] = false;
							$_SESSION['uGroups'] = $this->uGroups;
							$_SESSION['uTimezone'] = $this->uTimezone;
							$_SESSION['uDefaultLanguage'] = $this->uDefaultLanguage;
							Loader::helper('concrete/interface')->cacheInterfaceItems();
						}
					} else if ($row['uID'] && !$row['uIsActive']) {
						$this->loadError(USER_INACTIVE);
					} else {
						$this->loadError(USER_INVALID);
					}
					$r->free();
					if ($pw_is_valid_legacy) {
						// this password was generated on a previous version of Concrete5. 
						// We re-hash it to make it more secure.
						$v = array($this->getUserPasswordHasher()->HashPassword($password), $this->uID);
						$db->execute($db->prepare("update Users set uPassword = ? where uID = ?"), $v);
					}
				} else {
					$this->getUserPasswordHasher()->hashpassword($password); // hashpassword and checkpassword are slow functions. 
									 	// We run one here just take time.
										// Without it an attacker would be able to tell that the 
										// username doesn't exist using a timing attack.
					$this->loadError(USER_INVALID);
				}
			} else {
				$req = Request::get();
				if ($req->hasCustomRequestUser()) {
					$this->uID = null;
					$this->uName = null;
					$this->superUser = false;
					$this->uDefaultLanguage = null;
					$this->uTimezone = null;
					$ux = $req->getCustomRequestUser();
					if ($ux) {
						$this->uID = $ux->getUserID();
						$this->uName = $ux->getUserName();
						$this->superUser = $ux->getUserID() == USER_SUPER_ID;
						if ($ux->getUserDefaultLanguage()) {
							$this->uDefaultLanguage = $ux->getUserDefaultLanguage();
						}
						$this->uTimezone = $ux->getUserTimezone();
					}
				} else if (isset($_SESSION['uID'])) {
					$this->uID = $_SESSION['uID'];
					$this->uName = $_SESSION['uName'];
					if(isset($_SESSION['uEmail'])) {
						$this->uEmail = $_SESSION['uEmail'];
					}
					$this->uTimezone = $_SESSION['uTimezone'];
					if (isset($_SESSION['uDefaultLanguage'])) {
						$this->uDefaultLanguage = $_SESSION['uDefaultLanguage'];
					}
					$this->superUser = ($_SESSION['uID'] == USER_SUPER_ID) ? true : false;
				} else {
					$this->uID = null;
					$this->uName = null;
					$this->superUser = false;
					$this->uDefaultLanguage = null;
					$this->uTimezone = null;
				}
				$this->uGroups = $this->_getUserGroups();
				if (!isset($args[2]) && !$req->hasCustomRequestUser()) {
					$_SESSION['uGroups'] = $this->uGroups;
				}
			}
			
			return $this;
		}

	}
