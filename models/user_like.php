<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserLike
//-----------------------------------------------------------------------------------------------------------------------------
class UserLike extends Object { 

	public $id 				= '';
	public $user_id 		= '';
	public $item_id 		= '';
	public $type 			= '';
	public $liked 			= 1;
	public $date 			= "0000-00-00 00:00:00";
	
	public static $Fields = array("id", "user_id", "item_id", "type", "liked", "date");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserLike::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query=null, $order=null) {
		$db = Loader::db();
		$q = "SELECT * FROM UserLikes WHERE 1=1";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserLike();
			foreach(UserLike::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query="1", $order=null) {
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserLikes WHERE ".$query;
		if($order) $q .= " ORDER BY ".$order;
		//echo $q;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if($row) {
					$nu = new UserLike();
					foreach(UserLike::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCount
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getCount($query="1=1") {
		$count = 0;
		$db = Loader::db();
		$q = "SELECT COUNT(*) as total FROM UserLikes WHERE ".$query;
		if($r = $db->query($q)) {
			$d = $r->FetchRow();
			$count = $d['total'];
		}
		return $count;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: countAllByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function countAllByUserID($user_id, $query=null) {
		$count = 0;
		$q = "user_id='".$user_id."'";
		if($query) $q .= " AND (".$query.")";
		$q = "SELECT COUNT(*) as total FROM UserLikes WHERE ".$q;
		//if(ADMIN) echo $q."<br>";
		$db = Loader::db();
		if($r = $db->query($q)) {
			$d = $r->FetchRow();
			$count = $d['total'];
		}
		return $count;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByUserID($user_id, $query=null, $limit=null) {
		$q = "user_id='".$user_id."'";
		if($query) $q .= " AND (".$query.") ".$limit;
		//if(ADMIN) echo $q."<br>";
		return UserLike::getAll($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByItemID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByItemID($item_id) {
		return UserLike::getAll("item_id='".$item_id."'");
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCountForItem
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getCountForItem($item_id) {
		return UserLike::getCount("item_id='".$item_id."' AND liked=1");
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isLiked
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function isLiked($item_id, $user_id=null) {
		$r = UserLike::getLike($item_id, $user_id);
		return $r->liked;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getLike
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getLike($item_id, $user_id=null, $returnNew=true) {
		$r = null;
		if(!$user_id) {
			$u = new User();
			$user_id - $u->uID;
		}
		if($user_id) {
			$r = UserLike::getOne("user_id='".$user_id."' AND item_id='".$item_id."'");
			return $r;
		}
		if($returnNew) {
			$r = new UserLike();
			$r->item_id 	= $item_id;
			$r->user_id	 	= $user_id;
			$r->liked 		= $liked;
			return $r;
		}
		return null;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setLike
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function setLike($item_id, $user_id=null, $liked=true) {
		$liked = $liked ? 1 : 0;
		if(!$user_id) {
			$user = Loader::helper('user');
			$user_id = $user->id;
		}
		$r = UserLike::getLike($item_id, $user_id, false);
		if(!$r) {
			$r = new UserLike();
			$r->item_id 	= $item_id;
			$r->user_id	 	= $user_id;
			$r->liked 		= $liked;
			UserLike::create($r);
		}
		else
		if($r->liked != $liked) {
			$r->liked = $liked;
			UserLike::save($r);
		}
		return $r;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$up->date = date("Y-m-d H:i:s", time());
		
		$q = "INSERT INTO UserLikes (".UserLike::getFieldsList().") ";
		$q .= "VALUES (";
		$t = "";
		foreach(UserLike::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) { 
		$db = Loader::db();
		$q = "UPDATE UserLikes SET ";
		$t = "";
		foreach(UserLike::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
	}
	
	public static function delete($id) {
		$db = Loader::db();
		$q = "DELETE FROM UserLikes WHERE id='".$id."'";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: likeButton
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function likeButton($item_id, $user_id=null, $class='right', $innerOnly=false, $showCount=true) { 
		if(!$user_id) {
			$user = Loader::helper('user');
			$user_id = $user->id;
		}
		$like = UserLike::getLike($item_id, $user_id, true);
		
		$out = "";
		if(!$innerOnly) {
			$out .= "<a id='like_".$item_id."' href='javascript:;' class='likeheart ".$class."'";
			$out .= "onclick='ps.members.like(\"".$item_id."\");' >";
		}
		if($like->liked) {
			$out .= "<i class='liked fa fa-heartbeat' aria-hidden='true'></i>";
		}
		else {
			$out .= "<i class='unliked fa fa-heart-o' aria-hidden='true'></i>";
		}		
		if($showCount) {
			$count = UserLike::getCountForItem($item_id);
			$out .= "<div class='likecount'>".$count."</div>";
		}
		if(!$innerOnly) {
			$out .= "</a>";
			$out .= "<input type='hidden' id='likeval_".$item_id."' value='".$like->liked."'/>";
			$out .= "<input type='hidden' id='likecount_".$item_id."' value='".$showCount."'/>";
		}
		return $out;
	}
}
