<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserOnDeckEntryNote
//-----------------------------------------------------------------------------------------------------------------------------
class UserOnDeckEntryNote extends Object { 

	public $ID 						= '';
	public $UserOnDeckEntryID 		= '';
	public $OnDeckID 				= '';
	public $UserID 					= '';
	public $FromUserID 				= '';
	public $Note					= 1;

	public static $Fields = array(
		"ID", "UserOnDeckEntryID", "OnDeckID", "UserID", "FromUserID", "Note");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserOnDeckEntryNote::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntryNote WHERE ID=$id LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeckEntryNote();
			foreach(UserOnDeckEntryNote::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByUserOnDeckEntry
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByUserOnDeckEntry($udid) {
		$list = array();
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntryNote WHERE UserOnDeckEntryID='".$udid."' ORDER BY Created ASC";
		$r = $db->query($q);
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserOnDeckEntryNote();
				foreach(UserOnDeckEntryNote::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$list[] = $nu;
			}
		}
		return $list;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllOnDeck
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllOnDeck($deckid, $query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntryNote WHERE OnDeckID=".$deckid." ORDER BY Created ASC";
		if($query) $q .= " AND ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserOnDeckEntryNote();
				foreach(UserOnDeckEntryNote::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO UserOnDeckEntryNote (".UserOnDeckEntryNote::getFieldsList().", Created) ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserOnDeckEntryNote::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$t .= "'".date("Y-m-d H:i:s", time())."'";
		$q .= $t.")";
		$db->execute($q);
		//echo $q;
		$up->ID = $db->Insert_ID();
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE UserOnDeckEntryNote SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(UserOnDeckEntryNote::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE ID=".$up->ID;
		//echo $q;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function delete($up) {
		$db = Loader::db();
		$q = "DELETE FROM UserOnDeckEntryNote WHERE ID=".$up->ID;
		$db->execute($q);
	}
	
}
