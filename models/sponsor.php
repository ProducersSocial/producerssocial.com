<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: Sponsor
//-----------------------------------------------------------------------------------------------------------------------------
class Sponsor extends Object { 

	public $id 				= 0;
	public $handle 			= '';
	public $name 			= '';
	public $link 			= '';
	public $status 			= 'active'; // inactive
	public $ordernum 		= 0;
	public $created 		= '0000-00-00 00:00:00';
	
	static private $statusList = array(
		'active' 	=> "Active",
		'inactive' 	=> "Inactive"
	);
	
	public static $Fields = array("id", "handle", "name", "link", "status", "ordernum", "created");
	
	public static function getFieldsList() 
	{
		return implode(", ", Sponsor::$Fields);
	}
	
	public function getHandle() 
	{
		if(!$this->handle) {
			$this->handle = strtolower($this->name);
			$this->handle = str_replace(" ", "_", $this->handle);
			Sponsor::save($this);
		}
		return $this->handle;
	}
	
	public function getStatusName() 
	{
		return Sponsor::$statusList[$this->status];
	}
	
	public function isActive() 
	{
		return $this->status == "active";
	}
	
	public function getNext($sameType=true) 
	{
		// Assume descending order by date
		$q = "ordernum >= '".$this->ordernum."' AND id !=".$this->id." AND status='active'";
		return Sponsor::getOne($q);
	}
	
	public function getPrev() 
	{
		$q = "ordernum <= '".$this->ordernum."' AND id !=".$this->id." AND status='active'";
		return Sponsor::getOne($q);
	}
	
	public static function getStatusList() 
	{
		return Sponsor::$statusList;
	}
	
	public static function getOne($query=null, $order=null) 
	{
		$db = Loader::db();
		
		if(!$query) $query = "1=1";
		$q = "SELECT * FROM Sponsors WHERE ".$query;
		if($order) $q .= " ".$order;
		$q .= " LIMIT 1";
		
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new Sponsor();
			foreach(Sponsor::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getByID($id) 
	{
		$q = "id='$id'";
		return Sponsor::getOne($q);
	}
	
	public static function getByHandle($handle) 
	{
		$q = "handle='$handle'";
		return Sponsor::getOne($q);
	}
	
	public static function getAll($query=null, $order="ordernum DESC") 
	{
		$db = Loader::db();
		
		$all = array();
		if(!$query) $query = "1=1";
		$q = "SELECT * FROM Sponsors WHERE ".$query;		
		if($order) $q .= " ORDER BY ".$order;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if($row) {
					$nu = new Sponsor();
					foreach(Sponsor::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				$all[] = $nu;
			}
		}
		return $all;
	}
	
	public static function getAllByType($type) 
	{
		$q = "type='".$type."'";
		return Sponsor::getAll($q);
	}
	
	public static function getCount($query="1=1") 
	{
		$count = 0;
		$db = Loader::db();
		$q = "SELECT count(*) as count FROM Sponsors WHERE ".$query;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}

	public static function create($up) 
	{
		$user = Loader::helper('user');
		$db = Loader::db();
		$q = "INSERT INTO Sponsors (".Sponsor::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		
		$up->created = date("Y-m-d H:i:s", time());
		
		foreach(Sponsor::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
		$up->id = $db->Insert_ID();
		Loader::model('user_log');
		UserLog::record("Sponsors", $up->id, "created:".$up->toString());
		return $up;
	}
	
	public static function save($up) 
	{ 
		$user = Loader::helper('user');
		$db = Loader::db();
		$q = "UPDATE Sponsors SET ";
		$t = null;
		if($up->created == "0000-00-00 00:00:00") {
			$up->created = date("Y-m-d H:i:s", time());
			$up->createdby = $user->id;
		}
		
		foreach(Sponsor::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
		Loader::model('user_log');
		UserLog::record("Sponsors", $up->id, "updated:".$up->toString());
		return $up;
	}
	
	public static function deleteByID($id) 
	{
		$db = Loader::db();
		$q = "DELETE FROM Sponsors WHERE id='".$id."'";
		$db->execute($q);
		Loader::model('user_log');
		UserLog::record("Sponsors", $id, "deleted:".$id);
	}
	
	public function toString() 
	{
		return $this->id.":".$this->name;	
	}
}
