<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: PSEvent
//-----------------------------------------------------------------------------------------------------------------------------
class PSEvent extends Object { 

	public $eID 			= '';
	public $title 			= '';
	public $category 		= '';
	public $section 		= '';
	public $location		= '';
	public $eventID 		= '';
	public $date			= '';
	public $end_date 		= '';
	public $allday 			= '';
	public $grouped 		= '';
	public $sttime 			= '';
	public $entime 			= '';
	public $description 	= '';
	public $additional_data = '';
	public $status 			= '';
	public $updated 		= '';
	public $event_qty 		= '';
	public $event_price 	= '';

	public static $Fields = array(
		"eID", "title", "category", "section", "location", "eventID", "date", "end_date", "allday", "grouped", "sttime", "entime", 
		"description", "additional_data", "status", "updated", "event_qty", "event_price");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", PSEvent::$Fields);
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query) {
		$db = Loader::db();
		$q = "SELECT * FROM btProEventDates ".$query." LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new PSEvent();
			foreach(PSEvent::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM btProEventDates WHERE ID=$id LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new PSEvent();
			foreach(PSEvent::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM btProEventDates ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new PSEvent();
				foreach(PSEvent::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	/*
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO PSEvent (".PSEvent::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		
		$u = new User();
		foreach(PSEvent::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "Created") {
				$t .= "'".date("Y-m-d H:i:s")."'";
			}
			else
			if($f == "CreatedBy") {
				$t .= "'".$u->uID."'";
			}
			else {
				$t .= "'".addslashes($up->$f)."'";
			}
		}
		$q .= $t.")";
		$db->execute($q);
		
		$up->ID = $db->Insert_ID();
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE PSEvent SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(PSEvent::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "Modified") {
				$t .= $f."='".date("Y-m-d H:i:s")."'";
			}
			else {
				$t .= $f."='".addslashes($up->$f)."'";
			}
		}
		$q .= $t." WHERE ID=".$up->ID;
		//echo $q;
		$db->execute($q); 
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public function delete() {
		$this->clear();
		
		$db = Loader::db();
		$q = "DELETE FROM PSEvent WHERE ID=".$this->ID;
		$db->execute($q);
	}
	*/
}
