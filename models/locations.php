<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: Locations
//-----------------------------------------------------------------------------------------------------------------------------
class Locations extends Object { 

	public $ID 				= '';
	public $Name 			= '';
	public $Color 			= '351260';
	public $Status 			= '';		// 0:Closed | 1:Open
	public $PaypalEmail		= '';
	public $Timezone		= '';
	public $Countdown		= '10:00';
	public $Autostart		= 1;
	public $DefaultHost		= 0;
	public $SearchTerms		= '';

	public static $Fields = array(
		"ID", "Name", "Color", "Status", "PaypalEmail", "Timezone", "Countdown", "Autostart", "DefaultHost", "SearchTerms");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", Locations::$Fields);
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getTimezoneName
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getTimezoneName($id) {
		$tz = "America/Los_Angeles";
		if($id) {
			$d = Locations::getID($id);
			if($d->Timezone) $tz = $d->Timezone;
		}
		return $tz;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM Locations WHERE ID = '$id' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new Locations();
			foreach(Locations::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getList($status=null, $includeDefault=false, $includeOther=false, $query=null) {
		$locations = array();
		if($includeDefault) {
			$nu = new Locations();
			$nu->ID = "0";
			$nu->Name = "Please Select";
			$locations[] = $nu; 
		}
		$db = Loader::db();
		
		if(!$query) $query = "1";
		$q = "WHERE ".$query;
		if($status) {
			$q .= " AND status=".$status;
		}
		$q = "SELECT * FROM Locations ".$q;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new Locations();
				foreach(Locations::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$locations[$nu->ID] = $nu;
			}
		}
		if($includeOther) {
			$nu = new Locations();
			$nu->ID = "OT";
			$nu->Name = "Other";
			$locations[$nu->ID] = $nu;
		}
		return $locations;
	}
	
	public static function getAll($query=null, $order=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM Locations WHERE 1=1";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new Locations();
					foreach(Locations::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getName
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getName($id) {
		$name = null;//"Location Undefined (".$id.")";
		$l = Locations::getID($id);
		if($l) {
			$name = $l->Name;
		}
		return $name;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getSelectList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getSelectList($status=null, $includeDefault=false, $includeOther=false, $query=null) {
		$select = array();
		$list = Locations::getList($status, $includeDefault, $includeOther, $query);
		foreach($list as $l) {
			$select[$l->ID] = $l->Name;
		}
		return $select;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO Locations (".Locations::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(Locations::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE Locations SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(Locations::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE ID='".$up->ID."'";
		//echo $q;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function delete($up) {
		$db = Loader::db();
		$q = "DELETE FROM Locations WHERE ID='".$up->ID."'";
		$db->execute($q);
	}
}
