<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserSubscription
//-----------------------------------------------------------------------------------------------------------------------------
class UserSubscription extends Object { 

	public $id 				= '';
	public $customer_id 	= '';
	public $user_id 		= '';
	public $plan 			= '';
	public $type 			= 'paid';
	public $location 		= '';
	public $status 			= 1;	// 0=inactive 1=active 2=canceled 3=lapsed
	public $autorenew 		= 1;
	public $ending 			= '';
	public $renewed 		= '';
	public $created 		= '';
	public $note 			= '';
	
	public static $Fields = array(
		"id", "customer_id", "user_id", "plan", "type", "location", "status", "autorenew", "ending", "renewed", "created", "note"
	);
	
	public static $Types = array(
		"paid", "gift", "permanent"	
	);
	
	public function isActive() {
		if($this->status == 1) {
			if(strtotime($this->ending) < time()) {
				//echo "ENDING:".strtotime($this->ending)."<br>";
				//echo "time:".time()."<br>";
				//$this->status = 0;
				//UserSubscription::save($this);
				return false;
			}
			return true;
		}
		return false;
	}
	
	public function canRenew() {
		return true;
	}
	
	public function getStatus() {
		$stat = "Inactive";
		
		if($this->status == 1) {
			$stat = "Active";	
		}
		else
		if($this->status == 2) {
			$stat = "Cancelled";
		}
		else
		if($this->status == 3) {
			$stat = "Expired";
		}
		return $stat;
	}
	
	public function planName() {
		$plan = "None";
		if($this->plan == "yearly-membership") {
			$plan = "One Year Membership";
		}
		else
		if($this->plan == "quarterly-membership") {
			$plan = "3 Month Membership";
		}
		else
		if($this->plan == "yearly-membership-admin") {
			$plan = "One Year Membership (Admin Discount)";
		}
		else
		if($this->plan == "quarterly-membership-admin") {
			$plan = "3 Month Membership (Admin Discount)";
		}
		return $plan;
	}
	
	public function planDuration() {
		$duration = 0;
		if($this->plan == "yearly-membership" || $this->plan == "yearly-membership-admin") {
			$duration = 365 * 24 * 60 * 60;
		}
		else
		if($this->plan == "quarterly-membership" || $this->plan == "quarterly-membership-admin") {
			$duration = 92 * 24 * 60 * 60;
		}
		return $duration;
	}
	
	function planTicketQuantity() {
		$tickets = 0;
		if($this->plan == "yearly-membership" || $this->plan == "yearly-membership-admin") {
			$tickets = 12;
		}
		else
		if($this->plan == "quarterly-membership" || $this->plan == "quarterly-membership-admin") {
			$tickets = 3;
		}
		return $tickets;
	}
	
	function planTicketExpiration() {
		$extension = 60 * 60 * 24 * 31;
		if($this->plan == "yearly-membership") {
			$extension *= 2;
		}
		$exp = strtotime($this->ending) + $extension;
		return $exp;
	}
	
	public function locationName() {
		Loader::model('locations');
		return Locations::getName($this->location);
	}
	
	public function setAutorenew($on=true) {		
		$this->autorenew = $on ? 1 : 0;
		UserSubscription::save($this);
	}
	
	public static function getFieldsList() 
	{
		return implode(", ", UserSubscription::$Fields);
	}
	
	public static function getByUserID($user_id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserSubscriptions WHERE user_id='".$user_id."' ORDER BY created DESC LIMIT 1"; // AND status > 0
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserSubscription();
			foreach(UserSubscription::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		if($nu && !$nu->id) $nu = null;
		return $nu;
	}
	
	public static function getAllByUserID($user_id) 
	{
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserSubscriptions WHERE user_id='".$user_id."'";
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserSubscription();
					foreach(UserSubscription::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
			}
			$all[] = $nu;
		}
		return $all;
	}
	
	public static function getBySubscriptionID($sub_id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserSubscriptions WHERE id='".$sub_id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new UserSubscription();
			foreach(UserSubscription::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getByCustomerID($customer_id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserSubscriptions WHERE customer_id='".$customer_id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new UserSubscription();
			foreach(UserSubscription::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getAll($query=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserSubscriptions WHERE 1=1";
		if($query) $q .= " AND ".$query;
		
		//echo $q;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserSubscription();
					foreach(UserSubscription::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	public static function getSubscriptionForUser($user_id, $addQuery=null, $order="ORDER BY created DESC LIMIT 1") 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserSubscriptions WHERE user_id='".$user_id."'";
		if($addQuery) $q .= " AND ".$addQuery;
		if($order) $q .= " ".$order;
		
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new UserSubscription();
			foreach(UserSubscription::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		
		return $nu;
	}
	
	public static function getAllSubscriptionsForUser($user_id, $addQuery=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserSubscriptions WHERE user_id='".$user_id."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserSubscription();
					foreach(UserSubscription::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	public static function countAll() 
	{
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserSubscriptions";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}
	
	public static function countByPlan($plan, $addQuery=null) 
	{
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserSubscriptions WHERE plan='".$plan."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}

	public static function create($up) 
	{
		$db = Loader::db();
		$q = "INSERT INTO UserSubscriptions (".UserSubscription::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		$up->status = 1;
		foreach(UserSubscription::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);	
		//echo $q;
		
		Loader::model("user_profile");
		$p = UserProfile::getByUserID($up->user_id);
		$p->uIsMember = 1;
		UserProfile::save($p);
		
		return $up;
	}
	
	public static function save($up) 
	{
		$db = Loader::db();
		$q = "UPDATE UserSubscriptions SET ";
		$t = null;
		foreach(UserSubscription::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
		
		Loader::model("user_profile");
		$p = UserProfile::getByUserID($up->user_id);
		$p->uIsMember = $up->isActive();
		UserProfile::save($p);
		//echo $p->uID.":".$up->status."<br>";
		//print_r(debug_backtrace());
	}
	
	public static function delete($id) {
		$db = Loader::db();
		$q = "DELETE FROM UserSubscriptions WHERE id='".$id."'";
		$db->execute($q);
	}
}
