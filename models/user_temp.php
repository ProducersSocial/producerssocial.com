<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserTemp
//-----------------------------------------------------------------------------------------------------------------------------
class UserTemp extends Object { 

	public $ID 				= '';
	public $UserID 			= '';
	public $Name 			= '';
	public $Value 			= null;
	
	public static $Fields = array("ID", "UserID", "Name", "Value");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserTemp::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query=null, $order=null) {
		$db = Loader::db();
		$q = "SELECT * FROM UserTemp WHERE 1=1";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserTemp();
			foreach(UserTemp::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByUserID($uID) {
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserTemp WHERE UserID='".$uID."'";
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserTemp();
					foreach(UserTemp::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
			}
			$all[] = $nu;
		}
		return $all;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getValue
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getValue($name) {
		$u = new User();
		$r = null;
		if($u && $u->isLoggedIn()) {
			$r = UserTemp::getOne("UserID='".$u->uID."' AND Name='".$name."'");
			return $r->Value;
		}
		return null;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setValue
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function setValue($name, $value) {
		$u = new User();
		$r = null;
		if($u && $u->isLoggedIn()) {
			$create = false;
			$r = UserTemp::getOne("UserID='".$u->uID."' AND Name='".$name."'");
			if(!$r) {
				$create = true;
				$r = new UserTemp();
				$r->Name = $name;
				$r->UserID = $u->uID;
			}
			$r->Value = $value;
			
			if($create) {
				UserTemp::create($r);
			}
			else {
				UserTemp::save($r);
			}
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO UserTemp (".UserTemp::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserTemp::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) { 
		$db = Loader::db();
		$q = "UPDATE UserTemp SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(UserTemp::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE ID='".$up->ID."'";
		//echo $q;
		$db->execute($q);
	}
}
