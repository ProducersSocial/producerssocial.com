<?php defined('C5_EXECUTE') or die("Access Denied.");

define("FB_APP_ID", "1075076569201657");
define("FB_APP_SECRET", "4ed7630d5957d715369350d44fc4ee2e");
define("FB_ACCESS_TOKEN", "1075076569201657|Gw97cKI2venVMV46TcPFbskIcYU");
define("FB_PRODUCERSSOCIAL_PAGE_ID", "380085802162305");

//-----------------------------------------------------------------------------------------------------------------------------
//: FacebookEvent
//-----------------------------------------------------------------------------------------------------------------------------
class FacebookEvent extends Object {

	public $ID 				= '';
	public $OnDeckID		= '';
	public $Name			= '';
	public $Description 	= '';
	public $ImageURL		= '';
	public $StartTime 		= '0000-00-00 00:00:00';
	public $EndTime 		= '0000-00-00 00:00:00';
	public $Timezone 		= 'America/Los_Angeles';
	public $Location		= '';
	public $AddressID		= '';
	public $AddressName		= '';
	public $AddressStreet	= '';
	public $AddressCity		= '';
	public $AddressState 	= '';
	public $AddressCountry 	= '';
	public $AddressZip 		= '';
	public $TicketPrice		= '';
	public $TicketMode		= '';
	public $TicketLimit		= 100;
	public $Created 		= '';
	public $Modified 		= '';
	
	public $IsNew			= false;
	
	public static $Fields = array(
		"ID", "OnDeckID", "Name", "ImageURL", "Description", "StartTime", "EndTime", "Timezone", "Location", "AddressID", "AddressName", "AddressStreet", 
		"AddressCity", "AddressState", "AddressZip", "AddressCountry", "TicketPrice", "TicketMode", "TicketLimit", "Created", "Modified");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", FacebookEvent::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query) {
		$db = Loader::db();
		$q = "SELECT fb.* FROM FacebookEvents AS fb ".$query." LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new FacebookEvent();
			foreach(FacebookEvent::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM FacebookEvents WHERE ID='$id' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new FacebookEvent();
			foreach(FacebookEvent::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT fb.* FROM FacebookEvents AS fb ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new FacebookEvent();
				foreach(FacebookEvent::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public function create() {
		$db = Loader::db();
		$q = "INSERT INTO FacebookEvents (".FacebookEvent::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		
		$u = new User();
		foreach(FacebookEvent::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "Created") {
				$t .= "'".date("Y-m-d H:i:s")."'";
			}
			else {
				$t .= "'".addslashes($this->$f)."'";
			}
		}
		$q .= $t.")";
		//echo $q."<br><br>";
		$db->execute($q);
		return $this;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public function save() {
		$db = Loader::db();
		$q = "UPDATE FacebookEvents SET ";
		$t = null;
		$this->uEdited = 1;
		foreach(FacebookEvent::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "Modified") {
				$t .= $f."='".date("Y-m-d H:i:s")."'";
			}
			else {
				$t .= $f."='".addslashes($this->$f)."'";
			}
		}
		$q .= $t." WHERE ID='".$this->ID."'";
		$db->execute($q); 
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	static public function saveObject($obj) {
		//$obj->save();
		$db = Loader::db();
		$q = "UPDATE FacebookEvents SET ";
		$t = null;
		$obj->uEdited = 1;
		foreach(FacebookEvent::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "Modified") {
				$t .= $f."='".date("Y-m-d H:i:s")."'";
			}
			else {
				$t .= $f."='".addslashes($obj->$f)."'";
			}
		}
		$q .= $t." WHERE ID='".$obj->ID."'";
		$db->execute($q); 
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public function delete() {
		$this->clear();
		
		$db = Loader::db();
		$q = "DELETE FROM FacebookEvents WHERE ID='".$this->ID."'";
		$db->execute($q);
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: IsTicketRequired
	//-----------------------------------------------------------------------------------------------------------------------------
	public function IsTicketRequired() {
		$req = 0;
		//echo "P:".$this->TicketPrice."<br>";
		//echo "M:".$this->TicketMode."<br>";
		if($this->TicketPrice > 0 && $this->TicketMode == "online") {
			$req = 1;
		}
		return $req;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAccessToken
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAccessToken() {
		//$url = "https://graph.facebook.com/oauth/access_token?client_id=".FB_APP_ID."&client_secret=".FB_APP_SECRET."&grant_type=client_credentials";
		//$request = file_get_contents($url);	
		//echo $request;
		//$access_token = "1075076569201657|Gw97cKI2venVMV46TcPFbskIcYU";
		//return $request;
		return FB_ACCESS_TOKEN;
	}

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: pullEventsFromFacebook
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function pullEventsFromFacebook($startDate=null, $endDate=null) {
		$events = null;
		Loader::model('locations');
		
		if(!$startDate) $startDate = date("Y-m-d", time());
		if(!$endDate) $endDate = date("Y-m-d", time()+(60*60*24*365*2)); // Look forward 2 years
 
		$fields = "id,name,description,place,timezone,start_time,end_time,cover,event_times";
		$link = "https://graph.facebook.com/".FB_PRODUCERSSOCIAL_PAGE_ID."/events/attending/?fields=".$fields."&access_token=".FB_ACCESS_TOKEN."&since={$startDate}&until={$endDate}";
		//echo $link;
		
		$json = file_get_contents($link);	
		//print_r($json);
		
		$obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
		//return $obj;
		if(!$events) $events = array();
		
		foreach($obj['data'] as $event) {

			if(isset($event['event_times'])) {
				foreach($event['event_times'] as $t) {
					$e = $event;
					$e['id'] 			= $t['id'];
					$e['start_time'] 	= $t['start_time'];
					$e['end_time'] 		= $t['end_time'];
					
					$fb = FacebookEvent::saveFetchedEvent($e);
					if($fb) $events[] = $fb;
				}
			}
			else {
				$fb = FacebookEvent::saveFetchedEvent($event);
				if($fb) $events[] = $fb;
			}
			
		}
		return $events;
	}
	
	public static function saveFetchedEvent($event)
	{
		$create = true;
		if($fb = FacebookEvent::getID($event['id'])) {
			$create = false;
		}
		else {
			$fb = new FacebookEvent();
		}
		
		if(!$create) {
		// If this event has already passed, don't change anything
			$t = strtotime($event['start_time']);
			if($t < time()) {
				return null;	
			}
		}
		
		// Copy image to server
		$path_parts = pathinfo($event['cover']['source']);
		$content = file_get_contents($event['cover']['source']);
		$imgUrl = "/images/events/".$event['id'].".".$path_parts['extension'];
		$p = strpos($imgUrl, "?");
		if($p !== false) {
			$imgUrl = substr($imgUrl, 0, $p-1);
		}
		
		$fp = fopen(DIR_BASE.$imgUrl, "w");
		fwrite($fp, $content);
		fclose($fp);
			
		$fb->ID					= $event['id'];
		$fb->Name				= $event['name'];
		$fb->Description		= $event['description'];
		$fb->ImageURL			= BASE_URL.$imgUrl;
		$fb->StartTime			= $event['start_time'];
		$fb->EndTime			= $event['end_time'];
		$fb->Timezone			= $event['timezone'];
		$fb->AddressID			= $event['place']['id'];
		$fb->AddressName		= $event['place']['name'];
		$fb->AddressStreet		= $event['place']['location']['street'];
		$fb->AddressCity		= $event['place']['location']['city'];
		$fb->AddressState		= $event['place']['location']['state'];
		$fb->AddressCountry		= $event['place']['location']['country'];
		$fb->AddressZip			= $event['place']['location']['zip'];
		
		$locations = Locations::getAll();
		foreach($locations as $loc) {
			if($loc->SearchTerms) {
				$terms = explode(",", $loc->SearchTerms);
				foreach($terms as $term) {
					$t = trim($term);
					if(strpos($fb->Name, $t) !== false) {
						$fb->Location = $loc->ID;
						break;
					}
				}
			}
		}

		$fb->TicketPrice = 0;
		$fb->TicketMode = "none";
		$fb->TicketLimit = 0;
		
		$desc = strtolower($fb->Description);
		$t = strpos($desc, "no cover");
		if($t !== false) {
			$fb->TicketMode = "free";
		}
		else {
			$t = strpos($desc, "admission is free");
			if($t !== false) {
				$fb->TicketMode = "free";
			}
			else {
				$t = strpos($desc, "free admission");
				if($t !== false) {
					$fb->TicketMode = "free";
				}
			}
		}
		if($fb->TicketMode != "free") {
			$t = strpos($desc, "tickets:");
			if($t === false) {
				$t = strpos($desc, "entry:");
			}
			if($t !== false) {
				$desc2 = substr($desc, $t, 200);
				$p = strpos($desc2, "$");
				if($p !== false) {
					$fb->TicketPrice = intval(substr($desc2, $p+1, 3));
				}
			
				$m = strpos($desc2, "online");
				if($m !== false) {
					$fb->TicketMode = "online";
				}
				else {
					$m = strpos($desc2, "http");
					if($m !== false) {
						$m2 = strpos($desc2, " ", $m);
						if($m2 !== false) {
							$fb->TicketMode = substr($desc2, $m, $m2-$m);
						}
						else {
							$m2 = strpos($desc2, "\n", $m);
							if($m2 !== false) {
								$fb->TicketMode = substr($desc2, $m, $m2-$m);
							}
						}
					}
				}
			}			
		}			
		$t = strpos($desc, "limit:");
		if($t !== false) {
			$t += 6;
			$q = strpos($desc, " ", $t);
			$l = $q - $t;
			if($l > 4) {
				$q2 = strpos($desc, "\n", $t);
				if(!$q2 !== false) {
					$q = $q2;
				}
				else {
					$q = $t + 3;// attempt to guess
				}
			}
			$fb->TicketLimit = intval(trim(substr($desc, $t, $q)));
		}
		if($create) {
			$fb->IsNew = true;
			$fb->create();
		}
		else {
			$fb->IsNew = false;
			$fb->save();
		}
		
		if($fb->TicketMode == "online") {
			Loader::model('purchase_item');
			
			$create = false;
			$id = 'ticket-'.$fb->ID;
			$item = PurchaseItem::getByID($id);
			if(!$item) {
				$item = new PurchaseItem();
				$create = true;
			}
			$item->id 		= $id;
			$item->type 	= 'ticket';
			$item->event_id = $fb->ID;
			$item->name 	= $fb->Name;
			$item->price 	= $fb->TicketPrice * 100; // format as int with decimal percision
			
			if($create) {
				$item->status 	= 1;
				$item->created 	= date("Y-m-d H:i:s", time());
				PurchaseItem::create($item);
			}
			else {
				PurchaseItem::save($item);
			}
		}
		
		OnDeck::updateFromEvent($fb, true);
		
		return $fb;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCalendarLink
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getCalendarLink($size=24) {
		$view = View::getInstance();
		$output .= "<span class='addtocalendar '>";
		$output .= "<a class='atcb-link' tabindex='1' id='atc_icon_calred4_link'>";
		//$output .= "<img src='".$view->getThemePath()."/images/cal-red-04.png' width='".$size."'>";
		$output .= "<i class='fa fa-calendar-plus-o' aria-hidden='true'></i>";
		$output .= "</a>";
		$output .= "<var class='atc_event'>";
		$output .= "<var class='atc_date_start'>".$this->StartTime."</var>";
		$output .= "<var class='atc_date_end'>".$this->EndTime."</var>";
		$output .= "<var class='atc_timezone'>".$this->Timezone."</var>";
		$output .= "<var class='atc_title'>".$this->Name."</var>";
		$output .= "<var class='atc_description'>Please see event page for more information: http://producerssocial.com/eventinfo?id=".$this->ID."</var>";
		$output .= "<var class='atc_location'>".$this->AddressName." ".$this->AddressStreet." ".$this->AddressCity.", ".$this->AddressState." ".$this->AddressZip."</var>";
		$output .= "<var class='atc_organizer'>".$this->Location." Producers Social</var>";
		$output .= "<var class='atc_organizer_email'>admin@producerssocial.com</var>";
		$output .= "</var>";
		$output .= "</span>";
		return $output;
	} 
}
