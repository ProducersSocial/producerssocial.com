<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserSubmission
//-----------------------------------------------------------------------------------------------------------------------------
class UserSubmission extends Object { 

	public $id 				= '';
	public $user_id 		= '';
	public $submission_id 	= '';
	public $title 			= '';
	public $artist 			= '';
	public $realname 		= '';
	public $genre 			= '';
	public $email 			= '';
	public $filename 		= '';
	public $status 			= 'incomplete';
	public $note	 		= '';
	public $modified 		= '';
	public $created 		= '';
	
	public static $Fields = array(
		"id", "user_id", "submission_id", "title", "artist", "realname", "genre", "filename", "status", "email", "note", "modified", "created"
	);
	
	public static $Statuses = array(
		"incomplete", "pending", "approved", "rejected"	
	);
	
	public static function getFieldsList() 
	{
		return implode(", ", UserSubmission::$Fields);
	}
	
	public function sendSlackNotifcation()
	{
		Loader::model("member_feature");
		$feature = MemberFeature::getByID($this->submission_id);
		
		$slack = Loader::helper("slack");
		$n = "";//$heading.": ";
		if($feature) {
			$n .= $feature->name."\n";
		}
		else {
			$n .= "Submission:\n";
		}
		$n .= "Artist: ".$this->artist."\n";
		$n .= "Real Name: ".$this->realname."\n";
		$n .= "Track: ".$this->title."\n";
		$n .= "Genre: ".$this->genre."\n";
		$n .= "File: ".BASE_URL."/files/submissions/".$sub->submission_id."/".$this->filename."\n";
		$n .= "Email: ".$this->email."\n";
		if($this->note) {
			$n .= "Note: ".$this->note."\n";
		}
		$n .= "View: ".BASE_URL."/members/submissions/".$feature->handle;
		
		//echo $n;
		$slack->send($n);
	}
	
	public static function getByID($id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserSubmissions WHERE id='".$id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserSubmission();
			foreach(UserSubmission::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		if($nu && !$nu->id) $nu = null;
		return $nu;
	}
	
	public static function getByUserID($user_id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserSubmissions WHERE user_id='".$user_id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserSubmission();
			foreach(UserSubmission::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		if($nu && !$nu->id) $nu = null;
		return $nu;
	}
	
	public static function getAllByUserID($user_id, $addQuery=null) 
	{
		$db = Loader::db();
		$all = array();
		$q = "SELECT * FROM UserSubmissions WHERE user_id='".$user_id."'";
		if($addQuery) {
			$q .= " AND ".$addQuery;	
		}
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserSubmission();
					foreach(UserSubmission::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	public static function getBySubmissionID($sub_id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM UserSubmissions WHERE submission_id='".$sub_id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new UserSubmission();
			foreach(UserSubmission::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getAll($query=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM UserSubmissions WHERE 1=1";
		if($query) $q .= " AND ".$query;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new UserSubmission();
					foreach(UserSubmission::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}
	
	public static function countAll($addQuery=null) 
	{
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserSubmissions";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}
	
	public static function countBySubmission($submission_id, $addQuery=null) 
	{
		$count = 0;
		$db = Loader::db();
		$all = null;
		$q = "SELECT count(*) as count FROM UserSubmissions WHERE submission_id='".$submission_id."'";
		if($addQuery) $q .= " AND ".$addQuery;
		
		if($r = $db->query($q)) {
			$count = $r->FetchRow()['count'];
		}
		return $count;
	}

	public static function create($up) 
	{
		$db = Loader::db();
		$q = "INSERT INTO UserSubmissions (".UserSubmission::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		$up->status = 'pending';
		$up->modifed = date("Y-m-d H:i:s", time());
		$up->created = date("Y-m-d H:i:s", time());
		foreach(UserSubmission::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);
		
		return $up;
	}
	
	public static function save($up) 
	{
		$db = Loader::db();
		$q = "UPDATE UserSubmissions SET ";
		$t = null;
		$up->modifed = date("Y-m-d H:i:s", time());
		foreach(UserSubmission::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
	}
	
	public static function delete($id) {
		$sub = UserSubmission::getByID($id);
		if($sub) {
			if($sub->filename) {
				$filepath = DIR_BASE.'/files/submissions/'.$sub->submission_id.'/'.$sub->filename;
				if(file_exists($filepath)) {
					unlink($filepath);	
				}
			}
			
			$db = Loader::db();
			$q = "DELETE FROM UserSubmissions WHERE id='".$id."'";
			$db->execute($q);
		}
	}
}
