<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: OnDeck
//-----------------------------------------------------------------------------------------------------------------------------
class OnDeck extends Object { 
	public $ID 					= '';
	public $EventID				= '';
	public $Title				= '';
	public $Location 			= '';
	public $EventDate 			= '0000-00-00 00:00:00';
	public $StartTime 			= '0000-00-00 00:00:00';
	public $EndTime 			= '0000-00-00 00:00:00';
	public $IsEnabled			= 0;
	public $IsOpen	 			= 0;
	public $IsLive 				= 0;
	public $IsOnBreak			= 0;
	public $IsPrivate			= 0;
	public $CurrentNum			= 1;
	public $RoundNum 			= 1;
	public $RefreshID 			= '';
	public $HostedBy 			= '';
	public $HostID				= 0;
	public $BlogPost			= 0;
	public $HostFollowup		= 0;
	public $MembersFollowup		= 0;
	public $CreatedBy 			= '';
	public $Created 			= '';
	public $Modified 			= '';
	
	public $Event				= null;
	public $Entry				= null;

	public static $Fields = array(
		"ID", "EventID", "Title", "Location", "EventDate", "StartTime", "EndTime", "IsOpen", "CurrentNum", "IsLive", "BlogPost", "HostFollowup", "MembersFollowup", 
		"IsEnabled", "IsOnBreak", "IsPrivate", "RoundNum", "RefreshID", "HostedBy", "HostID", "CreatedBy", "Created", "Modified");

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: __construct
	//-----------------------------------------------------------------------------------------------------------------------------
	public function __construct() {}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isLocationAdmin
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function isLocationAdmin() {
		$isLocationAdmin = false;
		$u = new User();
		if(!$u || !$u->isLoggedIn()) {
			$u = null;
		}
		if($u) {
			if($u->isSuperUser()) {
				$isLocationAdmin = true;
			}
			else {
				Loader::model('user_profile');
				$profile = UserProfile::getByUserID($u->uID);
				if($profile) {
					if($profile->uLocationAdmin) {
						$isLocationAdmin = $profile->uLocationAdmin;
					}
				}
			}
		}
		return $isLocationAdmin;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", OnDeck::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query) {
		$db = Loader::db();
		$q = "SELECT * FROM OnDeck ".$query." LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new OnDeck();
			foreach(OnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM OnDeck WHERE ID='$id' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new OnDeck();
			foreach(OnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByEventID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByEventID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM OnDeck WHERE EventID='$id' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new OnDeck();
			foreach(OnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null, $order=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM OnDeck ".$query;
		if($order) $q .= " ".$order;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new OnDeck();
				foreach(OnDeck::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllOpenDecks
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllOpenDecks($query=null, $order=null) {
		$q = "WHERE IsOpen=1";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		return OnDeck::getAll($q);
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllLiveDecks
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllLiveDecks($query=null, $order=null) {
		$q = "WHERE IsLive=1";
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		return OnDeck::getAll($q);
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllLiveOrOpenDecks
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllLiveOrOpenDecks($query=null, $order=null, $includePrivate=false) {
		$q = "WHERE (IsLive=1 OR IsOpen=1)";
		if(!$includePrivate) {
			$q .= " AND IsPrivate=0";
		}
		if($query) $q .= " AND ".$query;
		if($order) $q .= " ".$order;
		//echo $q;
		return OnDeck::getAll($q);
	} 

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) 
	{
		$db = Loader::db();
		
		// Check first to be sure a deck for the same event hasn't already been created
		$deck = null;
		if($up->EventID) {
			$deck = OnDeck::getByEventID($up->EventID);
		}
		if($deck) {
			$up->ID	= $deck->ID;
			OnDeck::save($up);
		}
		else {
			$q = "INSERT INTO OnDeck (".OnDeck::getFieldsList().") ";
			$q .= "VALUES (";
			$t = null;
			
			$u = new User();
			foreach(OnDeck::$Fields as $f) {
				if($t) $t .= ", ";
				if($f == "Created") {
					$t .= "'".date("Y-m-d H:i:s")."'";
				}
				else
				if($f == "CreatedBy") {
					$t .= "'".$u->uID."'";
				}
				else {
					$t .= "'".addslashes($up->$f)."'";
				}
			}
			$q .= $t.")";
			$db->execute($q);
			//echo $q."<br>";
			
			$up->ID = $db->Insert_ID();
		}
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getForEvent
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getForEvent($event) {
		$d = OnDeck::getByEventID($event->ID);
		//if(!$d) {
		//	$d = OnDeck::createFromEvent($event);
		//}
		return $d;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: createFromEvent
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function createFromEvent($event) {
		//echo "OnDeck::createFromEvent:".$event->ID."<br>";
		$d = new OnDeck();
		$d->EventID 			= $event->ID;
		$d->Title 				= $event->Name;
		$d->Location			= $event->Location;
		$d->EventDate			= $event->StartTime;
		$d->IsTicketRequired 	= $event->Location == "LA" ? 1 : 0;
		$d = OnDeck::create($d);
		
		$event->OnDeckID = $d->ID;
		FacebookEvent::saveObject($event);
		
		return $d;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateFromEvent
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function updateFromEvent($event, $create=true) {
		$exists = false;
		if($event->OnDeckID) {
			$d = OnDeck::getID($event->OnDeckID);
			if($d) {
				$exists = true;
				$d->EventID 			= $event->ID;
				$d->Title 				= $event->Name;
				$d->Location			= $event->Location;
				$d->EventDate			= $event->StartTime;
				$d->IsTicketRequired 	= $event->Location == "LA" ? 1 : 0;
				OnDeck::save($d);
			}
		}
		if(!$exists && $create) {
			$d = OnDeck::createFromEvent($event);
			$event->OnDeckID = $d->ID;
			$event->save();
		}
		return $d;
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE OnDeck SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(OnDeck::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "RefreshID") {
				$t .= $f."='".uniqid()."'";
			}
			else
			if($f == "Modified") {
				$t .= $f."='".date("Y-m-d H:i:s")."'";
			}
			else {
				$t .= $f."='".addslashes($up->$f)."'";
			}
		}
		$q .= $t." WHERE ID=".$up->ID;
		//echo $q;
		$db->execute($q); 
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public function delete() {
		$this->clear();
		
		$db = Loader::db();
		$q = "DELETE FROM OnDeck WHERE ID=".$this->ID;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: clear
	//-----------------------------------------------------------------------------------------------------------------------------
	public function clear() {
		$this->IsLive 		= 0;
		$this->StartTime 	= '0000-00-00 00:00:00';
		$this->EndTime 		= '0000-00-00 00:00:00';
		$this->IsEnabled 	= 1;
		$this->IsOpen 		= 0;
		$this->IsLive 		= 0;
		$this->IsOnBreak 	= 0;
		$this->CurrentNum 	= 1;
		$this->RoundNum 	= 1;
		OnDeck::save($this);
		
		$db = Loader::db();
		$q = "DELETE FROM UserOnDeck WHERE OnDeckID='".$this->ID."'";
		$db->execute($q);
		
		$q = "DELETE FROM UserOnDeckEntry WHERE OnDeckID='".$this->ID."'";
		$db->execute($q);
		
		$q = "DELETE FROM OnDeckRound WHERE OnDeckID='".$this->ID."'";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getUser
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getUser($uid) {
		$ud = null;
		$u = User::getByUserID($uid);
		if($u && $this->ID) {
			Loader::model('user_on_deck');
			$ud = UserOnDeck::getOne("WHERE UserID='".$uid."' AND OnDeckID='".$this->ID."'");
		}
		return $ud;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: addUser
	//-----------------------------------------------------------------------------------------------------------------------------
	public function addUser($uid) {
		//echo "ADD USER";
		$ud = null;
		$u = User::getByUserID($uid);
		if($u && $this->ID) {
			Loader::model('user_on_deck');
			Loader::model('user_profile');
			
			$ud = new UserOnDeck();
			$ud->UserID = $uid;
			$ud->OnDeckID = $this->ID;
			
			$up = UserProfile::getByUserID($uid);
			if($up) {
				$ud->Name = $up->uArtistName;
				$ud->Email = $up->uEmail;
			}
			if(!$ud->Name) $ud->Name = $u->uName;
			if(!$ud->Email) $ud->Email = $u->uEmail;
			
			$ud->OrderNum = UserOnDeck::getCount($this->ID)+1;
			$ud = UserOnDeck::create($ud);
		}
		$this->renumber();
		return $ud;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: removeUser 
	//-----------------------------------------------------------------------------------------------------------------------------
	public function removeUser($udid) {
		//echo "REMOVE USER";
		Loader::model('user_on_deck');
		$name = null;
		$u = UserOnDeck::getOne("WHERE UserID='".$udid."' AND OnDeckID='".$this->ID."'");
		if($u) {
			$name = $u->Name;
			UserOnDeck::delete($u);
		}
		$this->renumber();
		return $name;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getUserByOrderNum
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getUserByOrderNum($num) {
		Loader::model('user_on_deck'); 
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeck WHERE OnDeckID='".$this->ID."' AND OrderNum=$num LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeck();
			foreach(UserOnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getUsersOnDeck
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getUsersOnDeck() {
		Loader::model('user_on_deck');
		Loader::model('user_on_deck_entry');
		
		$usersOnDeck = array();
		$usersOnDeck['current'] 	= null;
		$usersOnDeck['prev'] 		= null;
		$usersOnDeck['next'] 		= null;
		$usersOnDeck['second'] 		= null;
		$usersOnDeck['last'] 		= null;
		$usersOnDeck['playable'] 	= array();	// Only uses on the list that are not crossed off
		
		$usersOnDeck['all'] = UserOnDeck::getAll("WHERE OnDeckID='".$this->ID."' ORDER BY OrderNum ASC");
		$usersOnDeck['breaks'] = UserOnDeckEntry::getAllOnDeck($this->ID, "UserOnDeckID=-1 ORDER BY StartTime DESC");
		
		$save = false;
		if($usersOnDeck['all']) {
			foreach($usersOnDeck['all'] as $uod) {
				$uod->getEntries();
				
				if(!$uod->IsCrossedOff) {
					$usersOnDeck['playable'][] = $uod;
					
					if(!$usersOnDeck['current'] && $uod->OrderNum >= $this->CurrentNum) {//$uod->OrderNum == $this->CurrentNum) {//
						if(!$uod->IsCrossedOff) {
							$usersOnDeck['current'] = $uod;
						}
						if($this->CurrentNum != $uod->OrderNum) {
							$this->CurrentNum = $uod->OrderNum;
							OnDeck::save($this);
						}
					}
					else {
						if($uod->OrderNum < $this->CurrentNum && !$uod->IsCrossedOff && ($usersOnDeck['prev'] == null || $usersOnDeck['prev']->OrderNum < $uod->OrderNum)) {
							$usersOnDeck['prev'] = $uod;
						}
						if($usersOnDeck['next'] == null && !$uod->IsCrossedOff && $uod->OrderNum > $this->CurrentNum) {
							$usersOnDeck['next'] = $uod;
						}
						if($usersOnDeck['next'] != null && $usersOnDeck['second'] == null && !$uod->IsCrossedOff && $uod->OrderNum > $usersOnDeck['next']->OrderNum) {
							$usersOnDeck['second'] = $uod;
						}
					}
					
					$usersOnDeck['last'] = $uod;
				}
			}
		}
		if($save) {
			OnDeck::save($this);
		}
		
		return $usersOnDeck;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: renumber
	//-----------------------------------------------------------------------------------------------------------------------------
	public function renumber() {
		//echo "RENUMBER";
		Loader::model('user_on_deck'); 
		$users = UserOnDeck::getAll("WHERE OnDeckID='".$this->ID."' ORDER BY OrderNum ASC");
		if($users && count($users) > 0) {
			$i = 1;
			foreach($users as $u) {
				$u->OrderNum = $i;
				UserOnDeck::save($u);
				$i++;
			}
		}
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getStatus
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getStatus() {
		$status = "";
		
		if(!$this->IsLive && $this->EndTime && $this->EndTime != '0000-00-00 00:00:00' && time() > strtotime($this->EndTime)) {
			$status .= "ENDED";
		}
		else {
			if($this->IsLive) {
				$status .= "IN SESSION";
				$status .= "<br>";
				$status .= "<div class='small'>";
			}
			if($this->IsOpen) {
				$status .= "LIST IS OPEN";
			}
			else {
				//if(time() < strtotime($this->EventDate)) {
				if($this->StartTime == "0000-00-00 00:00:00") {
					$status .= "NOT OPEN YET";
				}
				else {
					$status .= "LIST IS CLOSED";
				}
			}
			if($this->IsLive) {
				$status .= "</div>";
			}
		} 
		
		return $status;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isStarted
	//-----------------------------------------------------------------------------------------------------------------------------
	public function isStarted() {
		return $this->StartTime && $this->StartTime != '0000-00-00 00:00:00';
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isEnded
	//-----------------------------------------------------------------------------------------------------------------------------
	public function isEnded() {
		return $this->EndTime && $this->EndTime != '0000-00-00 00:00:00';
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isActive
	//-----------------------------------------------------------------------------------------------------------------------------
	public function isActive() {
		return $this->IsOpen || $this->IsLive;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: close
	//-----------------------------------------------------------------------------------------------------------------------------
	public function close($endtime=null) {
		if($endtime) $this->EndTime = $endtime;
		$this->IsOpen = 0;
		OnDeck::save($this);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: end
	//-----------------------------------------------------------------------------------------------------------------------------
	public function end($endtime=null) {
		if(!$endtime) $endtime = date("Y-m-d H:i:s", time());
		$this->EndTime = $endtime;
		$this->IsOpen = 0;
		$this->IsLive = 0;
		$this->IsOnBreak = 0;
		OnDeck::save($this);
		
		// Make sure all reserved tickets get marked as used
		Loader::model('user_ticket');
		$tickets = UserTicket::getAll("event_id='".$this->ID."' AND used='0000-00-00 00:00:00'");
		if($tickets) {
			foreach($tickets as $t) {
				$t->used = date("Y-m-d H:i:s", time());
				UserTicket::save($t);	
			}
		}
	}
}
