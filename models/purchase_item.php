<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: PurchaseItem
//-----------------------------------------------------------------------------------------------------------------------------
class PurchaseItem extends Object { 

	public $id 					= '';
	public $type 				= '';
	public $event_id 			= '';
	public $name 				= '';
	public $price 				= '';
	public $status 				= 0;
	public $created 			= '0000-00-00 00:00:00';
	public $ending 				= '0000-00-00 00:00:00';
	
	public static $Fields = array(
		"id", "type", "event_id", "name", "price", "status", "created", "ending");
	
	public static function getFieldsList() 
	{
		return implode(", ", PurchaseItem::$Fields);
	}
	
	public function isAvailable() 
	{
		$ended = false;
		if($this->ending != '0000-00-00 00:00:00') {
			$end = strtotime($this->ending);
			if($end < time()) {
				$ended = true;	
			}
		}
		return !$ended && $this->status == 1;	
	}
	
	public static function getNewExpirationDate($months) {
		$date = '0000-00-00 00:00:00';
		if($months > 0) {
			//$offsetSeconds = $this->expiration_months * 31 * 24 * 60 * 60;
			$y = date("Y", time());
			$m = date("m", time());
			$d = date("d", time());
			
			$m = intval($m) + $months;
			if($m < 10) $m = "0".$m;
			
			$d = intval($d) + 1;
			if($d < 10) $d = "0".$d;
			
			$t = strtotime($y."-".$m."-".$d);
			$date = date("Y-m-d H:i:s", $t);
		}
		return $date;
	}
	
	public function processOrder($quantity, $transactionid) {
		$out = "";
		Loader::model('user_ticket');
		
		if($quantity <= 0) {
			$out = "<div class='alert alert-warning'>Please specify the number of items you wish to purchase.</div>";
		}
		else {
			$u = new User();
			if($u && $u->isLoggedIn()) {
				$profile = UserProfile::getByUserID($u->uID);
				if($profile) {
					Loader::model('user_purchase');
					
					$done = false;
					$purchase = UserPurchase::getByTransactionID($transactionid);
					
					if($this->type == "ticket") {
						$numTickets = $quantity;
						$months = $numTickets+1;
						if($this->id == "tickets3") {
							$numTickets = 3;
							$months = 4;
						}
						else
						if($this->id == "tickets10") {
							$numTickets = 10;
							$months = 12;
						}
						UserTicket::generate($numTickets, $u->uID, $this->event_id, $profile->uLocation, PurchaseItem::getNewExpirationDate($quantity));
	
						$out = "<div class='alert alert-success'>";
						$out .= "<h1>Thank you</h1>";
						$out .= $numTickets." ticket".($numTickets > 1 ? "s have" : " has")." been added to your account.";
						$out .= "<br><br>";
						$out .= "<a class='button center' href='/profile/tickets'>View Your Tickets</a>";
						$out .= "</div>";
						
						$info = UserInfo::getByID($profile->uID);
						$memberName = $profile->uArtistName;
						if(!$memberName) $memberName = $info->getUserName();
					
						$mh = Loader::helper('mail');
						$mh->from(EMAIL_DEFAULT_FROM_ADDRESS, EMAIL_DEFAULT_FROM_NAME);
						$mh->to($info->uEmail, $memberName);
						$mh->addParameter('profile', $profile);
						$mh->addParameter('user_id', $profile->uID);
						$mh->addParameter('event_id', $this->event_id);
						$mh->addParameter('numTickets', $numTickets);
						$mh->addParameter('memberName', $memberName);
						$mh->addParameter('subject', "Ticket Confirmation");
						$mh->load("ticket_confirmation");
						$mh->sendMail();
						
						$done = true;
					}
					
					$purchase->uStatus = 'Completed';
					UserPurchase::save($purchase);
				}
			}
		}
		if(!$out) {
			$out = "<div class='alert alert-warning'>There was an error processing the order for item '".$this->id."'. Please <a href='/about/contact-us/'>contact support</a></div>";
		}
		return $out;
	}
	
	public static function getByID($id) 
	{
		$db = Loader::db();
		$q = "SELECT * FROM PurchaseItems WHERE id='".$id."' ORDER BY created DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if ($row) {
			$nu = new PurchaseItem();
			foreach(PurchaseItem::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	public static function getAll($query=null) 
	{
		$db = Loader::db();
		$all = null;
		$q = "SELECT * FROM PurchaseItems WHERE 1=1";
		if($query) $q .= " AND ".$query;
		
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = null;
				if ($row) {
					$nu = new PurchaseItem();
					foreach(PurchaseItem::$Fields as $f) {
						$nu->$f = stripslashes($row[$f]);
					}
				}
				if($nu) {
					if(!$all) $all = array();
					$all[] = $nu;
				}
			}
		}
		return $all;
	}

	public static function create($up) 
	{
		$db = Loader::db();
		$q = "INSERT INTO PurchaseItems (".PurchaseItem::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(PurchaseItem::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == 'created') {
				$up->$f = date("Y-m-d H:i:s", time());
			}
			$t .= "'".addslashes($up->$f)."'";
		}
		$q .= $t.")";
		$db->execute($q);		
		//echo $q;
	}
	
	public static function save($up) 
	{
		$db = Loader::db();
		$q = "UPDATE PurchaseItems SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(PurchaseItem::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE id='".$up->id."'";
		$db->execute($q);
	}
}
