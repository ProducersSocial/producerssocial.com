<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserOnDeckEntry
//-----------------------------------------------------------------------------------------------------------------------------
class UserOnDeckEntry extends Object { 

	public $ID 				= '';
	public $OnDeckID 		= '';
	public $UserOnDeckID 	= '';
	public $RoundNum		= 1;
	public $OrderNum		= 0;
	public $Timer 			= 0;
	public $TimerRunning	= 0;
	public $StartTime 		= '0000-00-00 00:00:00';
	public $EndTime 		= '0000-00-00 00:00:00';
	public $CrossedOff 		= '0000-00-00 00:00:00';

	public static $Fields = array(
		"ID", "OnDeckID", "UserOnDeckID", "RoundNum", "OrderNum", "Timer", "TimerRunning", "StartTime", "EndTime", "CrossedOff");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserOnDeckEntry::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntry WHERE ".$query." LIMIT 1";
		//echo $q;
		
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeckEntry();
			foreach(UserOnDeckEntry::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntry WHERE ID=$id LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeckEntry();
			foreach(UserOnDeckEntry::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByUserOnDeckID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByUserOnDeckID($udid, $roundNum=0) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntry WHERE UserOnDeckID='".$udid."' ";
		if($roundNum) $q .= "AND RoundNum=".$roundNum." ";
		$q .= "ORDER BY RoundNum DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeckEntry();
			foreach(UserOnDeckEntry::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByUserOnDeckIDLastRound
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByUserOnDeckIDLastRound($udid) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntry WHERE UserOnDeckID='".$udid."' ORDER BY RoundNum DESC LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeckEntry();
			foreach(UserOnDeckEntry::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllByUserOnDeck
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllByUserOnDeck($udid) {
		$list = array();
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntry WHERE UserOnDeckID='".$udid."' ORDER BY RoundNum ASC";
		$r = $db->query($q);
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserOnDeckEntry();
				foreach(UserOnDeckEntry::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$list[] = $nu;
			}
		}
		return $list;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllOnDeck
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllOnDeck($deckid, $query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeckEntry WHERE OnDeckID=".$deckid;
		if($query) $q .= " AND ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserOnDeckEntry();
				foreach(UserOnDeckEntry::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCountOnDeck
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getCountOnDeck($deckid) {
		$count = 0;
		$db = Loader::db();
		$q = "SELECT COUNT(*) as total FROM UserOnDeckEntry WHERE OnDeckID=".$deckid;
		if($r = $db->query($q)) {
			//$d = mysql_fetch_assoc($r);
			$d = $r->FetchRow();
			$count = $d['total'];
		}
		return $count;
	} 

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO UserOnDeckEntry (".UserOnDeckEntry::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserOnDeckEntry::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "StartTime" && $up->$f == null) {
				$t .= "'".date("Y-m-d H:i:s", time())."'";
			}
			else {
				$t .= "'".addslashes($up->$f)."'";
			}
		}
		$q .= $t.")";
		$db->execute($q);
		//echo $q;
		$up->ID = $db->Insert_ID();
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE UserOnDeckEntry SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(UserOnDeckEntry::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE ID=".$up->ID;
		//echo $q;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function delete($up) {
		$db = Loader::db();
		$q = "DELETE FROM UserOnDeckEntry WHERE ID=".$up->ID;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: end
	//-----------------------------------------------------------------------------------------------------------------------------
	public function end() {
		$this->TimerRunning = 0;
		$this->EndTime = date("Y-m-d H:i:s", time());
		UserOnDeckEntry::save($this);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: ended
	//-----------------------------------------------------------------------------------------------------------------------------
	public function ended() {
		return $this->EndTime != '0000-00-00 00:00:00';
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: crossOff
	//-----------------------------------------------------------------------------------------------------------------------------
	public function crossOff() {
		$this->CrossedOff = date("Y-m-d H:i:s", time());
		UserOnDeckEntry::save($this);
	}
	
}
