<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: UserOnDeck
//-----------------------------------------------------------------------------------------------------------------------------
class UserOnDeck extends Object { 

	public $ID 				= '';
	public $UserID 			= '';
	public $OnDeckID 		= '';
	public $Name 			= '';
	public $Email	 		= '';
	public $OrderNum 		= '';
	public $InviteSent 		= false;
	public $Created 		= '0000-00-00 00:00:00';
	
	public $HasEntries		= false;
	public $Entries			= null;
	public $IsCrossedOff	= false;
	public $LastEntry		= null;

	public static $Fields = array(
		"ID", "UserID", "OnDeckID", "Name", "Email", "OrderNum", "InviteSent", "Created");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", UserOnDeck::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeck ".$query." LIMIT 1";
		//echo $q;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			//print_r($row);
			$nu = new UserOnDeck();
			foreach(UserOnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeck WHERE ID='$id' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeck();
			foreach(UserOnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByUserID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByUserID($deckid, $userid) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeck WHERE OnDeckID='".$deckid."' AND UserID=$userid LIMIT 1";
		//echo $q;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeck();
			foreach(UserOnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByOrderNum
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByOrderNum($deckid, $num) {
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeck WHERE OnDeckID='".$deckid."' AND OrderNum=$num LIMIT 1";
		//echo $q;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new UserOnDeck();
			foreach(UserOnDeck::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM UserOnDeck ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserOnDeck();
				foreach(UserOnDeck::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getEntries
	//-----------------------------------------------------------------------------------------------------------------------------
	public function getEntries($query=null) {
		Loader::model("user_on_deck_entry");
		$this->HasEntries 	= false;
		$this->Entries 		= null;
		$this->LastEntry	= null;
		
		$entries = UserOnDeckEntry::getAllByUserOnDeck($this->ID);
		if($entries && count($entries) > 0) {
			$this->Entries = array();
			$this->HasEntries = true;
			$this->LastEntry = $entries[count($entries) - 1];
			$this->IsCrossedOff = $this->LastEntry->CrossedOff && $this->LastEntry->CrossedOff != '0000-00-00 00:00:00';
			
			foreach($entries as $e) {
				$this->Entries[$e->RoundNum] = $e;
			}
		}
		return $this->Entries;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAllUsers
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAllUsers($query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT u.uID, u.uName, u.uEmail, u.uLocation, up.uArtistName FROM Users AS u ".$query." LEFT JOIN UserProfiles AS up ON u.uID=up.uID ORDER BY up.uArtistName, u.uName ASC";
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new UserOnDeck();
				$nu->UserID			= $row['uID'];
				$nu->Name 			= $row['uArtistName'] ? $row['uArtistName'] : $row['uName'];
				$nu->Email 			= $row['uEmail'];
				$nu->Location 		= $row['uLocation'];
				$users[] = $nu;
			}
		}
		return $users;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCount
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getCount($deckID) {
		$count = 0;
		$db = Loader::db();
		$q = "SELECT COUNT(*) as total FROM UserOnDeck WHERE OnDeckID=".$deckID;
		if($r = $db->query($q)) {
			//$d = mysql_fetch_assoc($r);
			$d = $r->FetchRow();
			$count = $d['total'];
		}
		return $count;
	} 

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO UserOnDeck (".UserOnDeck::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(UserOnDeck::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "Created") {
				$t .= "'".date("Y-m-d H:i:s", time())."'";
			}
			else {
				$t .= "'".addslashes($up->$f)."'";
			}
		}
		$q .= $t.")";
		$db->execute($q);
		//echo $q;
		$up->ID = $db->Insert_ID();
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE UserOnDeck SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(UserOnDeck::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE ID=".$up->ID;
		//echo $q;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function delete($up) {
		$db = Loader::db();
		$q = "DELETE FROM UserOnDeck WHERE ID=".$up->ID;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: start
	//-----------------------------------------------------------------------------------------------------------------------------
	public function start($round) {
		$entry = new UserOnDeckEntry();
		$entry->UserOnDeckID	= $this->ID;
		$entry->OnDeckID 		= $this->OnDeckID;
		$entry->RoundNum 		= $round;
		$entry->OrderNum 		= $this->OrderNum;
		$entry->StartTime 		= date("Y-m-d H:i:s", time());
		
		$this->LastEntry = UserOnDeckEntry::create($entry);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: end
	//-----------------------------------------------------------------------------------------------------------------------------
	public function end() {
		if($this->LastEntry) $this->LastEntry->end();
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: clear
	//-----------------------------------------------------------------------------------------------------------------------------
	public function clear($round) {
		$db = Loader::db();
		$q = "DELETE FROM UserOnDeckEntry WHERE UserOnDeckID='".$this->ID."' AND RoundNum=".$round." LIMIT 1";
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: clearLastEntry
	//-----------------------------------------------------------------------------------------------------------------------------
	public function clearLastEntry() {
		if($this->LastEntry) {
			UserOnDeckEntry::delete($this->LastEntry);
		}
	}
	
}
