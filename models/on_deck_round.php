<?php defined('C5_EXECUTE') or die("Access Denied.");

//-----------------------------------------------------------------------------------------------------------------------------
//: OnDeckRound
//-----------------------------------------------------------------------------------------------------------------------------
class OnDeckRound extends Object { 

	public $ID 				= '';
	public $OnDeckID 		= '';
	public $RoundNum 		= '';
	public $StartTime 		= '0000-00-00 00:00:00';
	public $EndTime 		= '0000-00-00 00:00:00';
	
	public $HasEntries		= false;
	public $Entries			= null;

	public static $Fields = array(
		"ID", "OnDeckID", "RoundNum", "EndTime", "StartTime");
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getFieldsList
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getFieldsList() {
		return implode(", ", OnDeckRound::$Fields);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getOne
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getOne($query) {
		$db = Loader::db();
		$q = "SELECT * FROM OnDeckRound ".$query." LIMIT 1";
		//echo $q;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			//print_r($row);
			$nu = new OnDeckRound();
			foreach(OnDeckRound::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getID
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getID($id) {
		$db = Loader::db();
		$q = "SELECT * FROM OnDeckRound WHERE ID='$id' LIMIT 1";
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new OnDeckRound();
			foreach(OnDeckRound::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getByRoundNum
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getByRoundNum($deckid, $num, $create=false) {
		$db = Loader::db();
		$q = "SELECT * FROM OnDeckRound WHERE OnDeckID='".$deckid."' AND RoundNum=$num LIMIT 1";
		//echo $q;
		$r = $db->query($q);
		$row = $r ? $r->FetchRow() : null;
		$nu = null;
		if($row) {
			$nu = new OnDeckRound();
			foreach(OnDeckRound::$Fields as $f) {
				$nu->$f = stripslashes($row[$f]);
			}
		}
		if(!$nu && $create) {
			$nu = new OnDeckRound();
			$nu->OnDeckID = $deckid;
			$nu->RoundNum = $num;
			$nu = OnDeckRound::create($nu);
		}
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: newRound
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function newRound($deckid, $num) {
		$nu = new OnDeckRound();
		$nu->OnDeckID = $deckid;
		//$nu->StartTime = date("Y-m-d H:i:s", time());
		$nu->RoundNum = $num;
		$nu = OnDeckRound::create($nu);
		return $nu;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getAll
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getAll($query=null) {
		$users = array();
		$db = Loader::db();
		$q = "SELECT * FROM OnDeckRound ".$query;
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$nu = new OnDeckRound();
				foreach(OnDeckRound::$Fields as $f) {
					$nu->$f = $row[$f];
				}
				$users[] = $nu;
			}
		}
		return $users;
	} 
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: getCount
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function getCount($deckID) {
		$count = 0;
		$db = Loader::db();
		$q = "SELECT COUNT(*) as total FROM OnDeckRound WHERE OnDeckID=".$deckID;
		if($r = $db->query($q)) {
			$d = $r->FetchRow();
			$count = $d['total'];
		}
		return $count;
	} 

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: create
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function create($up) {
		$db = Loader::db();
		$q = "INSERT INTO OnDeckRound (".OnDeckRound::getFieldsList().") ";
		$q .= "VALUES (";
		$t = null;
		foreach(OnDeckRound::$Fields as $f) {
			if($t) $t .= ", ";
			if($f == "StartTime" && $up->$f == null) {
				$t .= "'".date("Y-m-d H:i:s", time())."'";
			}
			else {
				$t .= "'".addslashes($up->$f)."'";
			}
		}
		$q .= $t.")";
		$db->execute($q);
		//echo $q;
		$up->ID = $db->Insert_ID();
		return $up;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: save
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function save($up) {
		$db = Loader::db();
		$q = "UPDATE OnDeckRound SET ";
		$t = null;
		$up->uEdited = 1;
		foreach(OnDeckRound::$Fields as $f) {
			if($t) $t .= ", ";
			$t .= $f."='".addslashes($up->$f)."'";
		}
		$q .= $t." WHERE ID=".$up->ID;
		//echo $q;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: delete
	//-----------------------------------------------------------------------------------------------------------------------------
	public static function delete($up) {
		$db = Loader::db();
		$q = "DELETE FROM OnDeckRound WHERE ID=".$up->ID;
		$db->execute($q);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: start
	//-----------------------------------------------------------------------------------------------------------------------------
	public function start() {
		$this->StartTime = date("Y-m-d H:i:s", time());
		OnDeckRound::save($this);
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: end
	//-----------------------------------------------------------------------------------------------------------------------------
	public function end() {
		$this->EndTime = date("Y-m-d H:i:s", time());
		OnDeckRound::save($this);
	}
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isStarted
	//-----------------------------------------------------------------------------------------------------------------------------
	public function isStarted() {
		return $this->StartTime != "0000-00-00 00:00:00";
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: isComplete
	//-----------------------------------------------------------------------------------------------------------------------------
	public function isComplete() {
		return $this->EndTime != "0000-00-00 00:00:00";
	}
	
}
