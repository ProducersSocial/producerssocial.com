<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');

$u = new User();
if($u->isLoggedIn()) {
	//header("Location: ".BASE_URL."/profile");
	//die;
}

Loader::library('authentication/open_id');
$form = Loader::helper('form'); 

$redirect = null;
if(isset($_REQUEST['redirect'])) {
//	echo "<h2>REDIRECT:".$redirect."</h2>";
}
if($redirect) {
	$redirect = str_replace("redirect=", "", $_SERVER['QUERY_STRING']);
	$redirect = str_replace(BASE_URL, "", $redirect);
}
?>

<div class='login_page'>	
	<div class="white_page">
		<div class="producers-main-pad">
		<div class="producers-main-center ccm-ui">
			<div class="login_page_center">

			<?php
				if($u->isLoggedIn()) {
					$ui = UserInfo::getByID($u->uID);
					echo "<h2>You are logged in as ".$ui->getUserName()."</h2>";
					//echo "<a href='/login/logout/'>Sign Out</a>";
		
					$a = new Area('SigninBlock');
					$a->display($c);
					echo "<script>setTimeout(function() { window.location='".BASE_URL."/profile/account'; }, 4000);</script>";
				}
				else {
					//print_r($_REQUEST);
				
			?>
			<script type="text/javascript">
			$(function() {
				$("input[name=uName]").focus();
			});
			</script>
	
		<legend><?php echo t('Please sign in...')?></legend>
		
		<div class="row">
		<div class="login_page_column col-sm-6">
			<?php
				if($error && $error->has()) {
					echo $error->output();
				}
				//if($error) {
				//	echo $error;
				//}
			?>
			<?php if (isset($intro_msg)) { ?>
			<div class="alert-message block-message success"><p><?php echo $intro_msg?></p></div>
			<?php } ?>

			<?php if( $passwordChanged ){ ?>

				<div class="block-message info alert-message"><p><?php echo t('Password changed.  Please login to continue. ') ?></p></div>

			<?php } ?> 

			<?php if($changePasswordForm){ ?>

				<p><?php echo t('Enter your new password below.') ?></p>

				<div class="ccm-form">	

				<form method="post" action="<?php echo $this->url( '/login', 'change_password', $uHash )?>"> 
					<?php
						if($redirect) {
							echo "<input type='hidden' id='redirect' name='redirect' value='".$redirect."'>";
						}
					?>

					<div class="control-group">
					<label for="uPassword" class="control-label"><?php echo t('New Password')?></label>
					<div class="controls">
						<input type="password" name="uPassword" id="uPassword" class="ccm-input-text">
					</div>
					</div>
					<div class="control-group">
					<label for="uPasswordConfirm"  class="control-label"><?php echo t('Confirm Password')?></label>
					<div class="controls">
						<input type="password" name="uPasswordConfirm" id="uPasswordConfirm" class="ccm-input-text">
					</div>
					</div>

					<div class="actions">
					<?php echo $form->submit('submit', t('Sign In') . ' &gt;', null, "button")?>
					</div>
				</form>
	
				</div>

			<?php }
			/*
			else
			if($validated) { ?>

			<h3><?php echo t('Email Address Verified')?></h3>

			<div class="success alert-message block-message">
			<p>
			<?php echo t('The email address <b>%s</b> has been verified and you are now a fully validated member of this website.', $uEmail)?>
			</p>
			<div class="alert-actions"><a class="button small" href="<?php echo $this->url('/')?>"><?php echo t('Continue to Site')?></a></div>
			</div>


			<?php 
			}
			*/
			else if (isset($_SESSION['uOpenIDError']) && isset($_SESSION['uOpenIDRequested'])) { ?>

			<div class="ccm-form">
				<?php switch($_SESSION['uOpenIDError']) {
					case OpenIDAuth::E_REGISTRATION_EMAIL_INCOMPLETE: ?>

						<form method="post" action="<?php echo $this->url('/login', 'complete_openid_email')?>">
							<p><?php echo t('To complete the signup process, you must provide a valid email address.')?></p>
							<label for="uEmail"><?php echo t('Email Address')?></label><br/>
							<?php echo $form->text('uEmail')?>
				
							<div class="ccm-button">
							<?php echo $form->submit('submit', t('Sign In') . ' &gt;', null, "button")?>
							</div>
						</form>

					<?php break;
					case OpenIDAuth::E_REGISTRATION_EMAIL_EXISTS:
	
					$ui = UserInfo::getByID($_SESSION['uOpenIDExistingUser']);
	
					?>

						<form method="post" action="<?php echo $this->url('/login', 'do_login')?>">
							<?php
								if($redirect) {
									echo "<input type='hidden' id='redirect' name='redirect' value='".$redirect."'>";
								}
							?>
							<p><?php echo t('The OpenID account returned an email address already registered on this site. To join this OpenID to the existing user account, login below:')?></p>
							<label for="uEmail"><?php echo t('Email Address')?></label><br/>
							<div><strong><?php echo $ui->getUserEmail()?></strong></div>
							<br/>
			
							<div>
							<label for="uName"><?php if (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) { ?>
								<?php echo t('Email Address')?>
							<?php } else { ?>
								<?php echo t('Username')?>
							<?php } ?></label><br/>
							<input type="text" name="uName" id="uName" <?php echo (isset($uName)?'value="'.$uName.'"':'');?> class="ccm-input-text">
							</div>			<div>

							<label for="uPassword"><?php echo t('Password')?></label><br/>
							<input type="password" name="uPassword" id="uPassword" class="ccm-input-text">
							</div>
			
							<br class='hidden-xs'>
							<div class="ccm-button">
							<?php echo $form->submit('submit', t('Sign In') . ' &gt;', null, "button")?>
							</div>
						</form>

					<?php break;

					}
				?>
			</div>

			<?php } else if ($invalidRegistrationFields == true) { ?>

			<div class="ccm-form">
				<p><?php echo t('You must provide the following information before you may login.')?></p>
				<form method="post" action="<?php echo $this->url('/login', 'do_login')?>">
					<?php
						if($redirect) {
							echo "<input type='hidden' id='redirect' name='redirect' value='".$redirect."'>";
						}
					?>
					<?php 
					$attribs = UserAttributeKey::getRegistrationList();
					$af = Loader::helper('form/attribute');
	
					$i = 0;
					foreach($unfilledAttributes as $ak) { 
						if ($i > 0) { 
							print '<br/><br/>';
						}
						print $af->display($ak, $ak->isAttributeKeyRequiredOnRegister());	
						$i++;
					}
					?>
	
					<?php echo $form->hidden('uName', Loader::helper('text')->entities($_POST['uName']))?>
					<?php echo $form->hidden('uPassword', Loader::helper('text')->entities($_POST['uPassword']))?>
					<?php echo $form->hidden('uOpenID', $uOpenID)?>
					<?php echo $form->hidden('completePartialProfile', true)?>

					<div class="ccm-button">
						<?php echo $form->submit('submit', t('Sign In', null, "button"))?>
						<?php echo $form->hidden('rcID', $rcID); ?>
					</div>
	
				</form>
			</div>	

			<?php } else { ?>

			<form method="post" action="<?php echo $this->url('/login', 'do_login')?>" class="form-horizontal ccm-login-form">
				<?php
					if($redirect) {
						echo "<input type='hidden' id='redirect' name='redirect' value='".$redirect."'>";
					}
				?>
				<fieldset>
					<div class="control-group">
					<label for="uName" class="control-label"><?php if (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) { ?>
						<?php echo t('Email Address')?>
					<?php } else { ?>
						<?php echo t('Username')?>
					<?php } ?></label>
					<div class="controls">
						<input type="text" name="uName" id="uName" <?php echo (isset($uName)?'value="'.$uName.'"':'');?> class="ccm-input-text">
					</div>
	
					</div>
					<div class="control-group">

					<label for="uPassword" class="control-label"><?php echo t('Password')?></label>
	
					<div class="controls">
						<input type="password" name="uPassword" id="uPassword" class="ccm-input-text" />
					</div>
	
					</div>
					<?php if (isset($locales) && is_array($locales) && count($locales) > 0) { ?>
						<div class="control-group">
							<label for="USER_LOCALE" class="control-label"><?php echo t('Language')?></label>
							<div class="controls"><?php echo $form->select('USER_LOCALE', $locales)?></div>
						</div>
					<?php } ?>
	
					<div class="control-group">
						<label id="remain_logged_in" class="checkbox"><?php echo $form->checkbox('uMaintainLogin', 1)?> <span><?php echo t('Remain logged in.')?></span></label>
					</div>
					<?php $rcID = isset($_REQUEST['rcID']) ? Loader::helper('text')->entities($_REQUEST['rcID']) : $rcID; ?>
					<input type="hidden" name="rcID" value="<?php echo $rcID?>" />
					<br class='hidden-xs'>
					<div class="ccm-ui">
					<?php echo $form->submit('submit', t('Sign In') . ' &gt;', array('class' => 'button'))?>
						<a class='clear pad-top' href='<?php echo $this->url('/loginhelp')?>'>Need help?</a>
					</div>
				</fieldset>

				<?php if (OpenIDAuth::isEnabled()) { ?>
					<fieldset>

					<legend><?php echo t('OpenID')?></legend>

					<div class="control-group">
						<label for="uOpenID" class="control-label"><?php echo t('Login with OpenID')?>:</label>
						<div class="controls">
							<input type="text" name="uOpenID" id="uOpenID" <?php echo (isset($uOpenID)?'value="'.$uOpenID.'"':'');?> class="ccm-input-openid">
						</div>
					</div>
					</fieldset>
				<?php } ?>
			</form>

			<br>
			</div>
			<div class="login_page_column col-sm-6 ">
				<?php
					$a = new Area('SigninBlock');
					$a->display($c);
				?>
				
				<br>
				<a id='register_button' class="button" href="<?php echo $this->url('/register')?>"><?php echo t('Register for Free!')?> <i class='fa fa-sign-in' aria-hidden='true'></i></a>
				<?php
				/*
				<fieldset>
					<legend><?php echo t('New Account')?></legend>
					<?php if (ENABLE_REGISTRATION == 1) { ?> 
					<div class="control-group">
					<p><?php echo t('Sign up for a free user account now, or use one of the social login buttons.')?></p>
					<div class="ccm-ui">
					<a class="btn primary" href="<?php echo $this->url('/register')?>"><?php echo t('Register')?></a>
					</div>
					</div>
					<?php } ?>
				</fieldset>

				<br>
				<fieldset>
					<a name="forgot_password"></a>
					<form method="post" action="<?php echo $this->url('/login', 'forgot_password')?>" class="form-horizontal ccm-forgot-password-form">
					<legend><?php echo t('Forgot Your Password?')?></legend>
					<p><?php echo t("Enter your email address below. We will send you instructions to reset your password.")?></p>
					<input type="hidden" name="rcID" value="<?php echo $rcID?>" />
						<div class="control-group">
							<label for="uEmail" class="control-label"><?php echo t('Email Address')?></label>
							<div class="controls">
								<input type="text" name="uEmail" value="" class="ccm-input-text" >
							</div>
						</div>
	
						<br>
						<div class="ccm-ui">
							<?php echo $form->submit('submit', t('Reset and Email Password') . ' &gt;', array('class' => 'primary btn'))?>
						</div>
					</form>
				</fieldset>
				*/
				?>
			</div>
		</div>





	<?php } ?>

	<?php
	}
	?>
			</div>
		</div>
		</div>
	</div>
	<br><br><br><br>
</div>
<?php $this->inc('elements/footer.php'); ?>
