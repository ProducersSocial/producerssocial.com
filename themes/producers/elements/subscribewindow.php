<div id="overlay" onclick="closeSubscribeWindow">
	<div id="subscribePopup">
		<div class="popup" onlick="">
			<a class="close" href="javascript:closeSubscribeWindow();">×</a>
			<div id="subscribecontent" class="content">
				<!-- Begin MailChimp Signup Form -->
				<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
				<style type="text/css">
					#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
				</style>
				<div id="mc_embed_signup">
				<form action="//producerssocial.us10.list-manage.com/subscribe/post?u=bf3673bdc54000ca2436fd90a&amp;id=37a6aecc29" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">
					<h2>Subscribe to our mailing list</h2>
				<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
				<div class="mc-field-group">
					<label for="mce-NAME">Real Name </label>
					<input type="text" value="" name="NAME" class="" id="mce-NAME">
				</div>
				<div class="mc-field-group">
					<label for="mce-ARTIST">Artist Name </label>
					<input type="text" value="" name="ARTIST" class="" id="mce-ARTIST">
				</div>
				<div class="mc-field-group">
					<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
				</label>
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
				</div>
				<div class="mc-field-group input-group">
					<strong>Locations </strong>
					<ul>
						<li><input type="checkbox" value="64" name="group[4405][64]" id="mce-group[4405]-4405-5"><label for="mce-group[4405]-4405-6"> Boston</label></li>
						<li><input type="checkbox" value="8" name="group[4405][8]" id="mce-group[4405]-4405-3"><label for="mce-group[4405]-4405-3"> Denver</label></li>
						<li><input type="checkbox" value="1" name="group[4405][1]" id="mce-group[4405]-4405-0"><label for="mce-group[4405]-4405-0"> Los Angeles</label></li>
						<li><input type="checkbox" value="4" name="group[4405][4]" id="mce-group[4405]-4405-2"><label for="mce-group[4405]-4405-2"> New York City</label></li>
						<li><input type="checkbox" value="32" name="group[4405][32]" id="mce-group[4405]-4405-5"><label for="mce-group[4405]-4405-5"> San Diego</label></li>
						<li><input type="checkbox" value="2" name="group[4405][2]" id="mce-group[4405]-4405-1"><label for="mce-group[4405]-4405-1"> San Francisco</label></li>
						<li><input type="checkbox" value="16" name="group[4405][16]" id="mce-group[4405]-4405-4"><label for="mce-group[4405]-4405-4"> Seattle</label></li>
						<li><input type="checkbox" value="128" name="group[4405][128]" id="mce-group[4405]-4405-7"><label for="mce-group[4405]-4405-7"> Michigan</label></li>
						<li><input type="checkbox" value="256" name="group[4405][256]" id="mce-group[4405]-4405-8"><label for="mce-group[4405]-4405-8"> Reno</label></li>
					</ul>
				</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;"><input type="text" name="b_bf3673bdc54000ca2436fd90a_37a6aecc29" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					</div>
				</form>
				</div>
				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='NAME';ftypes[1]='text';fnames[2]='ARTIST';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
				<!--End mc_embed_signup-->		
			</div>
		</div>
	</div>
</div>

