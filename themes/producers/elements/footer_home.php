			<?php
				$this->inc('elements/footer_content.php');
			?>    
	
			<script src="<?php echo $this->getThemePath()?>/js/bootstrap.min.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/colorbox/jquery.colorbox-min.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/jquery.cropit.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/monthly.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/featherlight.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/jquery.mask.min.js"  type="text/javascript"></script>
		
			<script src="<?php echo BASE_URL;?>/js/js.cookie.js"></script>
			<script src="<?php echo BASE_URL;?>/js/jquery.popupoverlay.js"></script>
			<script src="<?php echo $this->getThemePath();?>/js/parallax.min.js"></script>

			<script src="<?php echo $this->getThemePath()?>/js/html2canvas.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/feedback.js"  type="text/javascript"></script>
			
			<script src="<?php echo BASE_URL;?>/js/tinymce/tinymce.min.js"></script>
			<script src="<?php echo $this->getThemePath()?>/js/producers.js?v=<?php echo REVISION;?> type="text/javascript"></script>

			<script src="<?php echo $this->getThemePath();?>/js/jssor.slider.debug.js"></script>
			<script src="<?php echo $this->getThemePath()?>/js/producers_home.js"  type="text/javascript"></script>
		</div>
	<?php 
	Loader::element('footer_required');
	$this->inc('elements/help.php');
	$this->inc('elements/feedback.php');
	?>
    </body>
</html>