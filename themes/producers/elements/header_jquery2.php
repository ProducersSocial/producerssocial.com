<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
    <head>
	<?php  Loader::element('header_required_jquery2'); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<meta property="og:image" content="http://producerssocial.com/images/producerssocial.jpg" />
		<meta property="og:url" content="http://producerssocial.com" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Producers Social" />
		<meta property="fb:app_id" content="1075076569201657" />
		<meta property="og:description" content="The Producers Social meets every month, bringing music producers together to share their latest work and to get constructive feedback. Whether you are a producer, musician, vocalist, all are welcome!" />

		<link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/images/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
		<link rel="manifest" href="/images/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#000000">
		<meta name="msapplication-TileImage" content="/images/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#000000">
         
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	
		<link rel="stylesheet" href="http://producerssocial.com/css/jquery-ui.min.css">
		<link rel="stylesheet" href="http://producerssocial.com/css/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="http://producerssocial.com/css/jquery-ui.theme.min.css">
		
        <link href="<?php echo $this->getThemePath()?>/css/monthly.css" rel='stylesheet' type='text/css'>
        <link href="<?php echo $this->getThemePath()?>/css/bootstrap.css" rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">

		<?php
			echo "<link href='".$this->getThemePath()."/css/producers_style.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			
			if(ADMIN) {
				//echo LOCATION;
				if(LOCATION != "producerssocial" && LOCATION != "www") {
					echo "<link href='".$this->getThemePath()."/css/".LOCATION."_producers_style.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
				}
			}
			if(DEV) {
				echo "<link href='".$this->getThemePath()."/css/dev_producers_style.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			}
		?>
       
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-65925802-1', 'auto');
		  ga('send', 'pageview');

		</script>

	</head>
    <body id='psb'>
    <script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '1075076569201657',
		  xfbml      : true,
		  version    : 'v2.5'
		});
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1075076569201657";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

	<?php
		$page = Page::getCurrentPage();
		$p = $page->getCollectionName();
		
		$u = new User();
		$editmode = "";
		if($u->isSuperUser()) { 
			$editmode = "editmode";
		}
	?>
	<div id='ps' class="ccm-page <?php echo strtolower($p)." ".$editmode;?>">
