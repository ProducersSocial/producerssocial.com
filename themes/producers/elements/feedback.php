<?php
$form = Loader::helper('form'); 

$u = new User();
if($u && $u->isLoggedIn()) {
	$u = User::getByUserID($u->uID);
?>
	<a id='feedback_button' href='javascript:;' onclick='return ps.feedback.open();'>Feedback</a>

	<div id='feedback_window' class='hidden'>
		<div id='feedback_window_pad'>
			<a id='feedback_close' href='javascript:;' onclick='return ps.feedback.close();'><i class="fa fa-times" aria-hidden="true"></i></a>
			<div id='feedback_heading'>Send Feedback</div>
			<br class='clear'>
			<hr>
			<div id='feedback_thankyou'>Thank you for your feedback.<div id='feedback_result'></div></div>
			<form id='feedback_form' method='post' action='<?php echo $this->url("/feedback");?>'>
				<input id="feedback_title" type="text" placeholder='Brief message title'></input>
				<textarea id='feedback_text' placeholder='Please describe the issue or request'></textarea>
				<div class='feedback_label'>Optional: upload screenshot or file</div>
				<input id="feedback_file" type="file" />
				<br class='clear'>
				<input class='right' id="feedback_sendcopy" type="checkbox" value="1" onclick="return ps.feedback.toggleEmail();" checked/>
				<label class='left' for="feedback_sendcopy">Send me a copy</label>
				<input id="feedback_email" type="text" value="<?php echo $u->uEmail;?>" placeholder="Your Email"></input>
				<div id='feedback_error'></div>
				<a id='feedback_cancel' class='button white' href='javascript:;' onclick='return ps.feedback.close();'>Cancel</a>
				<input type="submit" id="feedback_submit" class="button" name="Send"/>
			</form>
		</div>
	</div>
<?php
}
?>