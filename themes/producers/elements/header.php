<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
    <head>
	<?php  
		Loader::element('header_required');
		$user = Loader::helper('user');
	?>
    	
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property='fb:app_id' content='1075076569201657' />
        <meta property='og:type' content='website' />
        
        <?php
        $meta = Loader::helper("meta");
        $meta->description = str_replace('"', "'", strip_tags($meta->description));
        
		echo "<meta property='og:image' content='".$meta->image."' />\n";
		echo "<meta property='og:url' content='".$meta->url."' />\n";
		echo "<meta property='og:title' content='Producers Social : ".$meta->title."' />\n";
		echo "<meta property='og:description' content='".$meta->description."' />\n";

		echo "<meta property='twitter:image' content='".$meta->image."' />\n";
		echo "<meta property='twitter:url' content='".$meta->url."' />\n";
		echo "<meta property='twitter:title' content='Producers Social : ".$meta->title."' />\n";
		echo "<meta property='twitter:description' content='".$meta->description."' />\n";
		?>
		<meta name="twitter:card" content="summary_large_image">

		<link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/images/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
		<link rel="manifest" href="/images/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#000000">
		<meta name="msapplication-TileImage" content="/images/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#000000">
         
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
		
        <link href="<?php echo BASE_URL;?>/css/jquery-ui.min.css">
        <link href="<?php echo BASE_URL;?>/css/jquery-ui.structure.min.css">
        <link href="<?php echo BASE_URL;?>/css/jquery-ui.theme.min.css">
        
        <link href="<?php echo $this->getThemePath()?>/css/monthly.css" rel='stylesheet' type='text/css'>
        <link href="<?php echo $this->getThemePath()?>/css/bootstrap.css" rel='stylesheet' type='text/css'>
        <link href="<?php echo $this->getThemePath()?>/css/featherlight.css" rel='stylesheet' type='text/css'>
        <!--<link href="<?php echo $this->getThemePath()?>/css/font-awesome.min.css" rel='stylesheet' type='text/css'>-->
        <script src="https://use.fontawesome.com/ee2e25621d.js"></script>

		<?php
			echo "<link href='".$this->getThemePath()."/css/producers_style.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			echo "<link href='".$this->getThemePath()."/css/locations.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			
			if(ADMIN) {
				//echo LOCATION;
				if(LOCATION != "producerssocial" && LOCATION != "www") {
					echo "<link href='".$this->getThemePath()."/css/".LOCATION."_producers_style.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
				}
			}
			if(DEV) {
				echo "<link href='".$this->getThemePath()."/css/dev_producers_style.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			}
			
			if($user->isMasterAdmin()) {
				echo "<link href='".$this->getThemePath()."/css/editmode.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			}
			if($user->isMasterAdmin() || $user->id == 38) {
				echo "<link href='".$this->getThemePath()."/css/admin.css?v=".REVISION."'  rel='stylesheet' type='text/css'>";
			}
		?>
       
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		
		<script src="<?php echo $this->getThemePath()?>/js/atc.min.js"  type="text/javascript"></script>
   		<link href="<?php echo $this->getThemePath()?>/css/atc-style-blue.css" rel="stylesheet" type="text/css">

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-65925802-1', 'auto');
		  ga('send', 'pageview');

		</script>

	</head>
    <body id='psb'>
		<script>
		  window.fbAsyncInit = function() {
			FB.init({
			  appId      : '1075076569201657',
			  cookie     : true,
			  xfbml      : true,
			  version    : 'v2.12'
			});
			  
			FB.AppEvents.logPageView();   
		  };
		
		  (function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "https://connect.facebook.net/en_US/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>
		
		<div id="fb-root"></div>

	<?php
		$page = Page::getCurrentPage();
		$p = $page->getCollectionName();
		
		$usermode = "guest";
		if($user->isLoggedIn()) {
			$usermode = "user";
			if($user->isMember()) {
				$usermode = "member";	
			}
		}
		echo "<input type='hidden' id='usermode' value='".$usermode."'>";
	?>
	<div id='ps' class="ccm-page <?php echo strtolower($p)." ".$editmode;?>">
<?php
if(!DEVPASS) {
	echo "<div class='alert alert-white center' style='width:300px; max-width:100%; margin:auto; margin-top:20px; margin-bottom:20px;'>";
	echo "<p>The Producers Social website is offline for maintenance and will be back shortly.</p>";
	echo "<form id='devlogin' action='/' method='post'>";
	echo "<br class='clear pad-top'>";
	echo "<input type='text' id='password' name='password' style='width:100%;'>";
	echo "<br class='clear pad-top'>";
	echo "<input type='submit' class='button' id='login' name='login' value='Log In'>";
	echo "</form>";	
	echo "</div>";

	$this->inc('elements/footer_home.php');
	die;
}

