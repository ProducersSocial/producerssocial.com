<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
    <head>
	<?php  Loader::element('header_required'); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link rel="shortcut icon" href="PUT YOUR FAVICON HERE">-->
         
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

		<style type="text/css">@import "/concrete/css/ccm.default.theme.css";</style>
	
		<link rel="stylesheet" href="http://producerssocial.com/css/jquery-ui.min.css">
		<link rel="stylesheet" href="http://producerssocial.com/css/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="http://producerssocial.com/css/jquery-ui.theme.min.css">
		
        <link href="<?php echo $this->getThemePath()?>/css/bootstrap.css" rel='stylesheet' type='text/css'>
        <link href="<?php echo $this->getThemePath()?>/js/colorbox/colorbox.css"  rel='stylesheet' type='text/css'>
        
        
        
	
		<?php
			echo "<link href='".$this->getThemePath()."/css/producers_style.css'  rel='stylesheet' type='text/css'>";
			
			if(ADMIN) {
				//echo LOCATION;
				if(LOCATION != "producerssocial" && LOCATION != "www") {
					echo "<link href='".$this->getThemePath()."/css/".LOCATION."_producers_style.css'  rel='stylesheet' type='text/css'>";
				}
			}
		?>
       
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		
		
	</head>
    
    <body>
    <script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '1075076569201657',
		  xfbml      : true,
		  version    : 'v2.5'
		});
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	
	<div class="producers-top-menu">
		<div class="container">
			<!-- Static navbar -->
			<div class="navbar navbar-inverse" role="navigation">
				<div class="container">
					<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="/">&nbsp;</a>
					</div>
					<div class="navbar-collapse collapse" id="producers-nav-bar">
					  <div class="navbar-right">
						<div class="navbar-signin">
						<?php 
						$u = new User();
						if ($u->isRegistered()) { ?>
							<?php  
							if (Config::get("ENABLE_USER_PROFILES")) {
								$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
							} else {
								$userName = $u->getUserName();
							}
							?>
							<span class="sign-in"><?php echo t('<b>%s</b> |', $userName)?> <a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('SIGN OUT')?></a></span>
						<?php  } else { ?>
							<span class="sign-in"><a href="<?php echo $this->url('/login')?>"><?php echo t('SIGN IN')?></a></span>
						<?php  } ?>
						</div>
						<ul class="nav navbar-nav navbar-right">
							<?php
								$r = $_SERVER['REQUEST_URI'];
								$page = Page::getCurrentPage();
								$p = $page->getCollectionName();
								//echo "[".$p."]";
							?>
							<li <?php if(strpos($r, '/about') !== false) { echo 'class="active"'; }?>><a href="/about">ABOUT</a></li>
							<li <?php if(strpos($r, '/events') !== false) { echo 'class="active"'; }?>><a href="/events">EVENTS</a></li>
							<li <?php if(strpos($r, '/stream') !== false) { echo 'class="active"'; }?>><a href="/stream">STREAM</a></li>
							<li <?php if(strpos($r, '/forum') !== false) { echo 'class="active"'; }?>><a href="/forum">FORUM</a></li>
						</ul>
					   </div>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</div><!--/.navbar -->
		</div> <!-- /container -->
	</div>

