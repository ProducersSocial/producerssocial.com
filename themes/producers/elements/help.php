<div id='help_popup'>
	<div id='ps' class='help_popup_inner'>
		<div class='help_popup_close right iconbutton'><i class='fa fa-times' aria-hidden='true'></i></div>
		<h2>Welcome to the Producers Social</h2>
		We are excited that you want to come share your music with us! To ensure a great experience for everyone, we have a few guidelines we ask everyone to follow:<br>
		<ul class='help_guidelines'>
			<li>Positive and constructive feedback only!</li>
			<li>Please have your track rendered to an mp3 or wav format</li>
		</ul>
		
		<div class='alert alert-success'>
		<h2>Tickets and Sign-up</h2>
		<ul class='help_guidelines'>
			<li>Some events are free. Please check the event details for ticket price, as it varies by location.</li>
			<li>A ticket purchase <i>does not</i> guarantee that you will get on the list to share a tune.</li>
			<li>We suggest arriving and signing up on the list early</li>
			<li>Each person may only sign up once. If you have a collab or something, please request an additional entry in person.</li>
		</ul>
		</div>
		
		<?php
			if(ENABLE_ONDECK) {
		?>
		<div class='alert alert-info'>
		<h2>List Rules</h2>
		<ul class='help_guidelines'>
			<li>The list is first-come, first-serve</li>
			<li>A ticket purchase <i>does not</i> guarantee that you will get on the list to share a tune. Please sign-up early.</li>
			<li>You may remove and re-add yourself to the list, as long as it is before you are on deck.</li>
			<li>You may not enter your name more than once on the list, though you may request it in person at the event</li>
			<li>If you purchased more than one ticket, please request in person any additional sign-ups.</li>
		</ul>
		</div>
		<br>
		<?php
			}
		?>
		
		<br>
		If you need further help, please contact <a href='<?php echo View::url("/about/contact-us");?>'>contact us</a> for assistance.
	</div>
</div>