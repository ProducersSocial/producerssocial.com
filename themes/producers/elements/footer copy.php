<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
	
	<footer class="footer">
      <div class="container">
     	 <div class="footer-inner">
			<p class="footer-copyright">&copy;<?php echo date('Y')?> <?php echo h(SITE)?>.</p>
			
			<?php 
			$u = new User();
			if ($u->isRegistered()) { ?>
				<?php  
				if (Config::get("ENABLE_USER_PROFILES")) {
					$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
				} else {
					$userName = $u->getUserName();
				}
				?>
				<span class="sign-in"><?php echo t('<b>%s</b> |', $userName)?> <a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out')?></a></span>
			<?php  } else { ?>
				<span class="sign-in"><a href="<?php echo $this->url('/login')?>"><?php echo t('Sign In')?></a></span>
			<?php  } ?>
	      </div>
      </div>
    </footer>

<?php  Loader::element('footer_required'); ?>

	
	<script src="<?php echo $this->getThemePath(); ?>/jsjs/bootstrap.min.js" charset="utf-8"></script>
	<script src="<?php echo $this->getThemePath(); ?>/js/main.js" type="text/javascript"></script>
	
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="<?php echo $this->getThemePath(); ?>/js/ie10-viewport-bug-workaround.js" type="text/javascript"></script>

</body>
</html>
