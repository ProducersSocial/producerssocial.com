<footer>
	<div class="container">
		<div class="row">
			<div class="text-center">
				<br class='clear'>
				
				<div id='donate_button'>
				<form style='width:150px; text-align:center; margin:auto;' action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" name="hosted_button_id" value="EK5JS9AMX8K58">
					<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
					<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
				<div id='donate_button_msg'>We are a community driven event, every dollar goes to helping our community!</div>
				</div>

				<div class="footer_bottom_content">Copyright &copy; <?php echo date("Y", time());?> Producers Social&reg; &nbsp;&nbsp; 
				&nbsp;<a href="/terms-and-privacy" target="_blank">Terms and Privacy</a>
				&nbsp;
				<?php 
				$u = new User();
				if ($u->isRegistered()) { ?>
					<?php  
					if (Config::get("ENABLE_USER_PROFILES")) {
						$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
					} else {
						$userName = $u->getUserName();
					}
					?>
					<span class="sign-in"><a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out')?></a></span>
				<?php  } else { ?>
					<span class="sign-in"><a href="<?php echo $this->url('/login')?>"><?php echo t('Sign In')?></a></span>
				<?php  } ?>
				</div>
				
				<div class="fb-like" data-href="http://producerssocial.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
			</div>
		</div>
	</div>
</footer>
<?php
	global $c;
	if($c->isEditMode()) { 
		echo "<span id='editmode'></span>";
	}
?>    
