			<?php
				$u = new User();
				if($showNav) {
			?>
			<br class='clear'>
			<footer>
				<div class="container">
					<div class="row">
						<div class="text-center">
							<br class='clear'>
							<div class="footer_bottom_content">Copyright &copy; <?php echo date("Y", time());?> Producers Social&reg; &nbsp;&nbsp; 
							&nbsp;<a href="/terms-and-privacy" target="_blank">Terms and Privacy</a>
							&nbsp;
							<?php 
							if ($u->isRegistered()) { ?>
								<?php  
								if (Config::get("ENABLE_USER_PROFILES")) {
									$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
								} else {
									$userName = $u->getUserName();
								}
								?>
								<span class="sign-in"><a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out')?></a></span>
							<?php  } else { ?>
								<span class="sign-in"><a href="<?php echo $this->url('/login')?>"><?php echo t('Sign In')?></a></span>
							<?php  } ?>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<?php
			}
				global $c;
				if($c->isEditMode()) { 
					echo "<span id='editmode'></span>";
				}
			?>    
			<?php
				$this->inc('elements/footer_content.php');
			?>    
	
			<script src="<?php echo $this->getThemePath()?>/js/bootstrap.min.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/colorbox/jquery.colorbox-min.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/jquery.cropit.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/jquery.cookie.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/jquery.form.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/monthly.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/featherlight.js"  type="text/javascript"></script>
			<script src="<?php echo $this->getThemePath()?>/js/jquery.mask.min.js"  type="text/javascript"></script>
		
			<script src="<?php echo BASE_URL;?>/js/js.cookie.js"></script>
			<script src="<?php echo BASE_URL;?>/js/jquery.popupoverlay.js"></script>
	  
			<script src="<?php echo $this->getThemePath();?>/js/jssor.slider.mini.js"></script>
			<script src="<?php echo BASE_URL;?>/js/tinymce/tinymce.min.js"></script>
			<script src="https://checkout.stripe.com/checkout.js"></script>
			<script src="<?php echo $this->getThemePath()?>/js/producers.js?v=<?php echo REVISION;?>" type="text/javascript"></script>
			<?php
				if(isset($extra)) {
					echo $extra;
				}
			?>
		</div>
	<?php 
	echo "<div id='user_profile_popup' style='display:none;'>";
		echo "<div id='ps' class='user_profile_win'>";
			echo "<div id='user_profile_content'>";
			echo "</div>";
		echo "</div>";
		//echo "<a class='custom_entry_popup_close button white left' href=''>Ok</a>";
	echo "</div>";
	Loader::element('footer_required');
	$this->inc('elements/help.php');
	$this->inc('elements/feedback.php');
	?>
  	<div id='loading'></div>
	</body>
</html>