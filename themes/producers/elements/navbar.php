<?php
	if(!isset($_REQUEST['navbar'])) {
		echo "<div id='navbar'>";
	}
?>
	<div class="navbar_container">
		<div class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle pad-left" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="navlogo" class="navbar-brand" href="/">&nbsp;</a>
			</div>
			
			<div class="navbar-signin">
				<div class='navbar-status'>
				<?php
					$user = Loader::helper('user');
					
					if($user->isLoggedIn()) {
						echo "<div class='navbar_logout'>";
						echo "<a href='/login/logout'>";
						//echo "<i class='fa fa-sign-out' aria-hidden='true'></i>";
						echo "LOG OUT";
						echo "</a>";
						echo "</div>";
						
						$tickets = Loader::helper('tickets');
						$tickets->navbarTicketDisplay($user->me->uID);
					}
					
					Loader::model("on_deck");
					$section = "session";
					$live = OnDeck::getOne("WHERE IsLive=1 AND IsPrivate=0");
					$ondeck = $live;
					if(!$live) {
						$section = "list";
						$open = OnDeck::getOne("WHERE IsOpen=1 AND IsPrivate=0");
						$ondeck = $open;
					}
					
					$link = "/ondeck?odid=".$ondeck->ID."&section=".$section;
					if(!$user->isLoggedIn()) {
						if(strpos($link, "/login?redirect=") === false) {
							$link = "/login?redirect=".$link;
						}
						$link = str_replace(BASE_URL, "", $link);
					}
					
					if($live) {
						echo "<div id='navbar_events_live_status' class='live'><a href='".$link."'>live</a></div>";	
					}
					else
					if($open) {
						echo "<div id='navbar_events_open_status' class='open'><a href='".$link."'>open</a></div>";	
					}
					else {
						echo "<div id='navbar_events_offline_status' class='offline'><a href='/events'>off air</a></div>";	
					}
					
				?>
				</div>
				<?php 
				if($user->isLoggedIn()) {
					$profile = $user->profile;
					if($user->id != $user->me->uID) {
						$profile = $user->me->profile;	
					}
					$adminBadge = null;
					if($profile->uLocationAdmin) {
						$reported = UserProfile::anyReported();
						if($reported) $adminBadge = " <span class='reported_badge'>".$reported."</span>";
					}
					echo "<a class='navbar-help navbar-help-icon right' href='javascript:;' onclick='ps.showHelp();'>&nbsp;</a>";
					$moreLinks = null;
					if($profile) {
						$moreLinks = null;
						if($profile->uLocationAdmin) {
							if($moreLinks) $moreLinks .= " | ";
							$moreLinks .= "<a class='navbar_admin_status' href='/manage'>".$profile->uLocationAdmin." Admin".$adminBadge."</a>";
						}
					}
					$out = '<span class="navbar_username">';
					$out .= '<a class="navbar_username_name" href="/profile"><i class="fa fa-user" aria-hidden="true"></i> ' . $user->me->getUserName() . '</a>';
					$out .= $moreLinks;
					
					$out .= "<ul id='username_dropdown'>";
					if($profile->uLocationAdmin) {
						$out .= "<li><a href='/manage'>".$profile->uLocationAdmin." Admin".$adminBadge."</a></li>";
					}
					$out .= "<li><a href='/profile'>Profile</a></li>";
					$out .= "<li><a href='/profile/account'>Account</a></li>";
					if($user->sub) {
						$out .= "<li><a href='/profile/membership'>Membership</a></li>";
					}
					$out .= "</ul>";
					
					$out .= '</span>';
					?>
					<span class="sign-in"><?php echo $out;?></span>
					
				<?php  
				} 
				else { 
					$link = $_SERVER['REQUEST_URI'];
					if(!$link || strlen($link) < 2) {
						$link = "/members";	
					}
					$link = str_replace("/login?redirect=", "", $link);
					$link = str_replace(BASE_URL, "", $link);
				?>
					<span class="sign-in">
						<a class="join_free" href="<?php echo '/register';?>"><?php echo t('JOIN FREE')?></a>
						<a href="<?php echo '/login?redirect='.$link;?>"><?php echo t('LOG IN')?></a>
					</span>
				<?php  
				} 
				?>
			</div>
			<?php
			?>
			<div class="navbar-collapse collapse" id="producers-nav-bar">
			
			<ul class="nav navbar-nav">
				<?php
					$r = $_SERVER['REQUEST_URI'];
					//if(ADMIN) echo "REQ:".$r;
				?>
				<li <?php if(strpos($r, '/about') !== false) { echo 'class="active"'; }?>><a href="/about">ABOUT</a></li>
				<li <?php if(strpos($r, '/members') !== false && strpos($r, '/profile/members') === false) { echo 'class="active"'; }?>><a href="/members">MEMBERS</a></li>
				
				<?php
					
					echo "<li id='navbar-events' class='";
					if(strpos($r, '/events') !== false) { 
						echo "active"; 
					}
					if($live) {
						echo " live";	
					}
					else
					if($open) {
						echo " open";	
					}
					echo "'><a href='/events'>";
					echo "EVENTS";
					echo "</a>";
					
					$link = "/ondeck?odid=".$ondeck->ID."&section=".$section;
					if(!$user->isLoggedIn()) {
						$link = "/login?redirect=".$link;
					}
					if($live) {
						echo "<div id='navbar_events_live'><a href='".$link."'>live</a></div>";	
					}
					else
					if($open) {
						echo "<div id='navbar_events_open'><a href='".$link."'>open</a></div>";	
					}
					else {
						echo "<div id='navbar_events_offline' class='offline'><a href='/events'>off air</a></div>";	
					}
					echo "</li>";
				?>	
				
				<li <?php if(strpos($r, '/blog') !== false) { echo 'class="active"'; }?>><a href="/blog">BLOG</a></li>
				<li class="navbar-help"><a href="javascript:;" onclick='ps.showHelp();'><span class='navbar-help-icon'>&nbsp;</span></a></li>
				<?php
				/*
				<li <?php if(strpos($r, '/stream') !== false) { echo 'class="active"'; }?>><a href="/stream">STREAM</a></li>
				<li <?php if(strpos($r, '/forum') !== false) { echo 'class="active"'; }?>><a href="/forum">FORUM</a></li>
				*/
				?>
			</ul>
		   </div>
		</div>
	</div>
<?php
	if(!isset($_REQUEST['navbar'])) {
		echo "</div>";
	}
?>

