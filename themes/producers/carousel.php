<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<div id='carousel_page'>
	<div>
		<!-- Carousel -->
		<?php if($c->isEditMode()) {?>
			<div id='producers-carousel' class="carousel-setup">
				<div class="carousel-block">
				&nbsp;
				<?php
					$a = new Area('Carousel1');
					//$a->enableGridContainer();
					$a->display($c);
				?>
				</div>
				<div class="carousel-block">
				&nbsp;
				<?php
					$a = new Area('Carousel2');
					//$a->enableGridContainer();
					$a->display($c);
				?>
				</div>
				<div class="carousel-block">
				&nbsp;
				<?php
					$a = new Area('Carousel3');
					//$a->enableGridContainer();
					$a->display($c);
				?>
				</div>
			</div>
		
		<?php } else {?>
		<div id="producers-carousel" class="carousel slide" <?php if(!$c->isEditMode()) { echo 'data-ride="carousel"';}?>>
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#producers-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#producers-carousel" data-slide-to="1"></li>
				<li data-target="#producers-carousel" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<div class="container-box">
						<div class="carousel-caption">
						<div class="carousel-box">
						<?php
							$a = new Area('Carousel1');
							//$a->enableGridContainer();
							$a->display($c);
						?>
						</div>
						</div>
					</div>
				</div>
			
				<div class="item">
					<div class="container-box">
							<div class="carousel-caption">
							<?php
								$a = new Area('Carousel2');
								//$a->enableGridContainer();
								$a->display($c);
							?>
							</div>
					</div>
				</div>
					<div class="item">
						<div class="container-box">
							<div class="carousel-caption">
							<?php
								$a = new Area('Carousel3');
								//$a->enableGridContainer();
								$a->display($c);
							?>
							</div>
						</div>
					</div>
			</div>
			<a class="left carousel-control" href="#producers-carousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#producers-carousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div><!-- /#producers-carousel -->
		<?php } ?>
	</div>

	<div class="producers-main" id="producers-main">
		<div class="producers-main-pad">
		<?php
			$a = new Area('Main');
			//$a->enableGridContainer();
			$a->display($c);
		?>
		</div>
	</div>
</div>		
<?php  $this->inc('elements/footer.php'); ?>
