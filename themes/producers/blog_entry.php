<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');
?>
<div class='center_page'>
	<div class="white_center">
	<?php
		Loader::element('system_errors', array('error' => $error));
		print $innerContent;
	
		$a = new Area('Main');
		$a->display($c);
		
		global $u; 
		if(!$u->isLoggedIn() || $u->isSuperUser()) {
			$a = new Area("GuestsOnly");
			$a->display($c);
		}
		if($u->isLoggedIn()) {
			$a = new Area("MembersOnly");
			$a->display($c);
					
			Loader::model('user_subscription');
			$sub 	= UserSubscription::getByUserID($u->uID);	

			if($sub && $sub->isActive()) {
				$a = new Area("PremiumMembersOnly");
				$a->display($c);
			}
		}
	?>
	</div>
	
	<br class='clear'>
</div>	
<?php  $this->inc('elements/footer.php'); ?>
