<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');
?>
<div class='full_page'>
	<div class="white_page">
		<div class="page_container">
			<?php
				global $u; 
				if($u->isLoggedIn()) {
					$a = new Area("MembersOnly");
					$a->display($c);
					
					Loader::model('user_subscription');
					$sub 	= UserSubscription::getByUserID($u->uID);	

					if($sub && $sub->isActive()) {
						$a = new Area("PremiumMembersOnly");
						$a->display($c);
					}
				}
				if(!$u->isLoggedIn() || $u->isSuperUser()) {
					$a = new Area("GuestsOnly");
					$a->display($c);
				}
				$a = new Area('Main');
				//$a->enableGridContainer();
				$a->display($c);
			?>
			<br class='clear'>
		</div>
	</div>
</div>	

<?php  $this->inc('elements/footer.php'); ?>
