jQuery(function($) {
	$(document).ready( function() {
		ps.init();
    });
	$(document).ajaxComplete( function() {
		//console.log("ajaxComplete");
		ps.reinit();
    });
});

//-----------------------------------------------------------------------------------------------------------------------------
//: ps : Producers Social Javascript
//-----------------------------------------------------------------------------------------------------------------------------
var ps = {
	editmode: 			false,
	usermode:			'guest', 
	autoFadeoutTimer: 	null,
	autoFadeoutWait: 	8000,
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init	: Main startup routine
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		//console.log("ps.init");
		ps.editmode = $("#editmode").length;
		
		// Run the init function for all of the ps packages
		$.each(ps, function(key, obj) {
			//console.log(key+":"+(typeof obj));
			if(typeof obj === "object") {
				if(obj && obj.init) obj.init(re);
			}
		});
		
		ps.usermode = $("#usermode").val();
		//console.log("usermode:"+ps.usermode);
		if(ps.usermode == 'guest') {
			$(".guestsonly").show();	
		}
		else
		if(ps.usermode == 'user') {
			$(".usersonly").show();	
		}
		else
		if(ps.usermode == 'member') {
			$(".membersonly").show();	
		}
		
		ps.autoFadeout();
		
		if(!re) {
			//setTimeout(ps.refreshNavbar, 30000);
    		
			$('.collapse').on('show.bs.collapse hide.bs.collapse', function(e) {
				e.preventDefault();
			});
			$('[data-toggle="collapse"]').on('click', function(e) {
				e.preventDefault();
				$($(this).data('target')).toggleClass('in');
			});		
			$("#help_popup").popup({
				onopen: function() {},
				onclose: function() {}
			});
		
			//var autorefresh = $("#auto_refresh_page");
			//if(autorefresh != "undefined") {
			//	setTimeout("ps.reloadPage()", 10000);
			//}
			
			tinymce.init({
			  selector: 'textarea.tinymce', 
			  height: 500,
			  body_class: 'ps',
			  relative_urls: false,
			  convert_urls: false,
			  forced_root_block: false,
			  document_base_url: "https://producerssocial.com/",
			  valid_elements : '*[*]',
			  extended_valid_elements : "iframe[class|src|frameborder|alt|title|width|height|align|name]",
			  plugins: [
				'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen jbimages',
				'insertdatetime media table contextmenu paste code'
			  ],
			  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages',
			  content_css: [
				'//www.tinymce.com/css/codepen.min.css?v=1',
				'/themes/producers/css/bootstrap_tinymce.css?v=1',
				'/themes/producers/css/tinymce.css?v=5'
			  ],
			  setup: function(editor) {
					editor.on('submit', function(e) {
						var c = editor.getContent().trim().replace(/iframe/g, "xframe");
						//editor.setContent(c);
						//console.log('submit event', c);
					});
				}
			});
			
			ps.hideLoading();
		}
	},
	reinit: function() { ps.init(true); },
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: refreshNavbar
	//-----------------------------------------------------------------------------------------------------------------------------
	refreshNavbar: function() {
		$.ajax({
			url: "/ajax?navbar=true",
			dataType: 'text', 
			error: function(xhr, status, error) {
				console.trace();
				console.error(error);
			},
			success: function(result) {
				//console.log("refresh");
				$("#navbar").html(result);
				setTimeout(ps.refreshNavbar, 30000);
			}
		});
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: autoFadeout
	//-----------------------------------------------------------------------------------------------------------------------------
	autoFadeout: function(m) {
		ps.autoFadeoutTimer = setTimeout(function() {
        	$("[rel='fadeaway']").hide('slow')
    	}, ps.autoFadeoutWait);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: autoFadeoutStop
	//-----------------------------------------------------------------------------------------------------------------------------
	autoFadeoutStop: function(m) {
		if(ps.autoFadeoutTimer != null) clearTimeout(ps.autoFadeoutTimer);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: reloadPage
	//-----------------------------------------------------------------------------------------------------------------------------
	reloadPage: function(m) {
		window.location = location.href;
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showHelp
	//-----------------------------------------------------------------------------------------------------------------------------
	showHelp: function(m) {
		$("#help_popup").popup('show');
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showLoading
	//-----------------------------------------------------------------------------------------------------------------------------
	showLoading: function() {
		$("#loading").popup('show');
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: hideLoading
	//-----------------------------------------------------------------------------------------------------------------------------
	hideLoading: function() {
		$("#loading").popup('hide');
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: defaultValue
	//-----------------------------------------------------------------------------------------------------------------------------
	defaultValue: function(inputValue, defaultValue) {
		if(typeof(inputValue) == "undefined") {
			return defaultValue;
		}
		else {
			return inputValue;
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: log
	//-----------------------------------------------------------------------------------------------------------------------------
	// This is an alias for console.log but it first tests that the console is available
	//-----------------------------------------------------------------------------------------------------------------------------
	log: function(m) {
	//	if(DEBUG_ON) {
			//if(!Browser.Engine.trident) {
				if(typeof console != "undefined") {
					if(m && typeof console.log === 'undefined') console.log(m);
				}
			//}
	//	}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: error
	//-----------------------------------------------------------------------------------------------------------------------------
	// This is an alias for console.log but it first tests that the console is available
	//-----------------------------------------------------------------------------------------------------------------------------
	error: function(m) {
		if(!Browser.Engine.trident) {
			if(m && typeof console.error != 'undefined') console.error(m);
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: encode
	//-----------------------------------------------------------------------------------------------------------------------------
	// All data sent through Ajax must be properly treated using url encoding with JSON structure.
	//-----------------------------------------------------------------------------------------------------------------------------
	encode: function(d) {
		return encodeURIComponent(JSON.encode(d));
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: decode
	//-----------------------------------------------------------------------------------------------------------------------------
	// This reverses the encode function, returning the original input string
	//-----------------------------------------------------------------------------------------------------------------------------
	decode: function(d) {
		return decodeURIComponent(d);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: alert
	//-----------------------------------------------------------------------------------------------------------------------------
	alert: function(txt) {
		alert(txt);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: confirm
	//-----------------------------------------------------------------------------------------------------------------------------
	confirm: function(txt) {
		var msg = decodeURIComponent(txt);
		return confirm(msg.replace(/\+/g, " "));
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: submitForm
	//-----------------------------------------------------------------------------------------------------------------------------
	submitForm: function(id) {
		$("#"+id).submit();
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: hide
	//-----------------------------------------------------------------------------------------------------------------------------
	hide: function(id, mode) {
		$("#"+id).hide(mode);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: admin
//-----------------------------------------------------------------------------------------------------------------------------
ps.admin = {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: makeAdmin
	//-----------------------------------------------------------------------------------------------------------------------------
	makeAdmin: function(userid, username, location) {
		if(ps.confirm("Are you sure you want to make the user '"+username+"' an admin for "+location+"?")) {
			$.ajax({
				url: "/ajax?makeadmin="+userid+"&location="+location,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log("success");
					//console.log(result);
					window.location.reload();
				}
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: revokeAdmin
	//-----------------------------------------------------------------------------------------------------------------------------
	revokeAdmin: function(userid, username) {
		if(ps.confirm("Are you sure you want to revoke admin status for the user '"+username+"'?")) {
			$.ajax({
				url: "/ajax?revokeadmin="+userid,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log("success");
					//console.log(result);
					window.location.reload();
				}
			});
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: fx
//-----------------------------------------------------------------------------------------------------------------------------
ps.fx = {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: highlight
	//-----------------------------------------------------------------------------------------------------------------------------
	highlight: function(elem) {
		var e = $(elem);
		if(e) {
			e.addClass('highlight');
			e.focus();
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: unhighlight
	//-----------------------------------------------------------------------------------------------------------------------------
	unhighlight: function(elem) {
		var e = $(elem);
		if(e) {
			e.removeClass('highlight');
			e.focus();
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: unhighlightAll
	//-----------------------------------------------------------------------------------------------------------------------------
	unhighlightAll: function(elem) {
		$$('.highlight').each(function(i) {
			ps.fx.unhighlight(i);
		});
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: alert
	//-----------------------------------------------------------------------------------------------------------------------------
	alert: function(elem, msg) {
		ps.fx.highlight(elem);
		//alert(msg);
		var a = $('alert');
		if(a) {
			a.innerHTML = "<div id='alertMsg'>"+msg+"</div>";
		}
		else {
			alert(msg);
		}

		setTimeout(ps.fx.unhighlightAll, 2000);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}

}; // fx

//-----------------------------------------------------------------------------------------------------------------------------
//: newsletter
//-----------------------------------------------------------------------------------------------------------------------------
ps.newsletter = {
	checkRefresh:	null,

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		//var asktosubscribe = Cookies.get('asktosubscribe');
		//console.log("asktosubscribe:"+asktosubscribe);
		//if(!asktosubscribe) {
		//	window.setTimeout(ps.newsletter.openSubscribeWindow, 3000);
		//}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: openSubscribeWindow
	//-----------------------------------------------------------------------------------------------------------------------------
	openSubscribeWindow: function() {
		$("#overlay").css({display:"block"});
		checkRefresh = window.setInterval(checkSubscribeStatus, 250);
		Cookies.set('asktosubscribe', 1);
		
		$(document).keyup(function(e) {
			if (e.keyCode == 27) { // escape key
				closeSubscribeWindow();
			}
		});
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: closeSubscribeWindow
	//-----------------------------------------------------------------------------------------------------------------------------
	closeSubscribeWindow: function() {
		$("#overlay").css({display:"none"});
		//console.log("clear:"+checkRefresh);
		window.clearInterval(checkRefresh);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: checkSubscribeStatus
	//-----------------------------------------------------------------------------------------------------------------------------
	checkSubscribeStatus: function() {
		//console.log("checkSubscribeStatus");
		if($("#mce-success-response").html()) {
			$("#subscribecontent").html("<h3>Please check your email to confirm your subscription.</h3>");
			window.setTimeout(closeSubscribeWindow, 5000);
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: testimonials
//-----------------------------------------------------------------------------------------------------------------------------
ps.testimonials = {
	fade:		800,
	hold:		18000,
	all: 		null,
	index:		1,
	current:	null,
	timer:		null,

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			ps.testimonials.all = $(".testimonial");
			//console.log("testimonials:"+ps.testimonials.all.length);
			
			ps.testimonials.current = $("#testimonial_"+ps.testimonials.index);
			ps.testimonials.timer = setTimeout(ps.testimonials.next, ps.testimonials.hold);
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: increment
	//-----------------------------------------------------------------------------------------------------------------------------
	increment: function(amount) {
		ps.testimonials.current.fadeOut(ps.testimonials.fade);
		
		ps.testimonials.index += amount;
		if(ps.testimonials.index > ps.testimonials.all.length) {
			ps.testimonials.index = 1;	
		}
		else
		if(ps.testimonials.index < 1) {
			ps.testimonials.index = ps.testimonials.all.length;
		}
		
		ps.testimonials.current = $("#testimonial_"+ps.testimonials.index);
		ps.testimonials.current.delay(ps.testimonials.fade).fadeIn();
		
		clearTimeout(ps.testimonials.timer);
		ps.testimonials.timer = setTimeout(ps.testimonials.next, ps.testimonials.hold);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: next
	//-----------------------------------------------------------------------------------------------------------------------------
	next: function() {
		ps.testimonials.increment(1);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: prev
	//-----------------------------------------------------------------------------------------------------------------------------
	prev: function() {
		ps.testimonials.increment(-1);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: hide
	//-----------------------------------------------------------------------------------------------------------------------------
	hide: function() {
		$("#home_testimonials").hide();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: show
	//-----------------------------------------------------------------------------------------------------------------------------
	show: function() {
		$("#home_testimonials").show();
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
}

//-----------------------------------------------------------------------------------------------------------------------------
//: home
//-----------------------------------------------------------------------------------------------------------------------------
ps.home = {
	jssorSlider:		null,

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			var jssor = $("#jssor_1");
			if(jssor.length) {
				var jssorOptions = {
				  $AutoPlay: true,
				  $Idle: 0,
				  $AutoPlaySteps: 4,
				  $SlideDuration: 3000,
				  $SlideEasing: $Jease$.$Linear,
				  $PauseOnHover: 4,
				  $SlideWidth: 250,
				  $Cols: 6
				};
			
		
				if(!ps.editmode) {
					ps.home.jssorSlider = new $JssorSlider$("jssor_1", jssorOptions);
					
					ps.home.scaleSlider();
					$(window).bind("load", ps.home.scaleSlider);
					$(window).bind("resize", ps.home.scaleSlider);
					$(window).bind("orientationchange", ps.home.scaleSlider);
				}
			}
			
			$(document).on("scroll", function() {
				if($("#ps_home").length) {
					//console.log("animate logo"); 
					var resize = "1";//$("header").attr("resize");
					if(typeof(resize) != undefined && resize == "1" && $(window).width() > 640) {
						//var top = ($(document).width() * 0.5625) - 80;
						var top = 100;
						if($(document).scrollTop() > top) {
							$("#navbar").addClass("expand");
							$('#navlogo').addClass("expand");
							$('#home_header_logo').addClass("expand");
						}
						else {
							$("#navbar").removeClass("expand");
							$('#navlogo').removeClass("expand");
							$('#home_header_logo').removeClass("expand");
						}
					}
				}
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: scaleSlider
	//-----------------------------------------------------------------------------------------------------------------------------
	scaleSlider: function() {
		//console.log("ScaleSlider");
		var refSize = ps.home.jssorSlider.$Elmt.parentNode.clientWidth;
		if (refSize) {
			//refSize = Math.min(refSize, 2000);
			ps.home.jssorSlider.$ScaleWidth(refSize);
		}
		else {
			window.setTimeout(ps.home.scaleSlider, 30);
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: sponsorContact
	//-----------------------------------------------------------------------------------------------------------------------------
	sponsorContact: function() {
		$("#sponsor_contact").html("We'd love to talk, just send us an email at <a href='mailto:sponsors@producerssocial.com'>sponsors@producerssocial.com</a>");
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: register
//-----------------------------------------------------------------------------------------------------------------------------
ps.register = {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: validate
	//-----------------------------------------------------------------------------------------------------------------------------
	validate: function() {
		//console.log("validateRegistration");
		var pass = false;
		var location = $("#uLocation");
	
		if(typeof(location) != "undefined") {
			//console.log(location.val());
			if(location.val() != "0") {
				pass = true;
			}
			else {
				//window.alert("Please select a location to register.");
				$("#pick_location").html("Please select a location");
			}
			
		}
	
		return pass;
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updatesociallogin
	//-----------------------------------------------------------------------------------------------------------------------------
	updatesociallogin: function() {
		console.log("updatesociallogin");
		var pass = false;
		
		var fb = $(".signin_facebook");
		var tw = $(".signin_twitter");
		var gg = $(".signin_google");
		
		var location = $("#uLocation");
		var username = $("#uName");
	
		if(typeof(location) != "undefined") {
			//console.log(location.val());
			if(location.val() != "0") {
				pass = true;
			}
			else {
				//window.alert("Please select a location to register.");
				$("#pick_location").html("Please select a location");
			}
		}
		if(pass) {
			fb.attr('href', "?provider=Facebook&uLocation="+location.val()+"&uName="+username.val());
			tw.attr('href', "?provider=Twitter&uLocation="+location.val()+"&uName="+username.val());
			gg.attr('href', "?provider=Google&uLocation="+location.val()+"&uName="+username.val());
		}
		else {
			fb.attr('href', "javascript:ps.register.validate();");
			tw.attr('href', "javascript:ps.register.validate();");
			gg.attr('href', "javascript:ps.register.validate();");
		}
	
		return pass;
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: stripe
//-----------------------------------------------------------------------------------------------------------------------------
ps.stripe = {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			ps.stripe.createForms();
			ps.stripe.setupButtons();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: createForms
	//-----------------------------------------------------------------------------------------------------------------------------
	createForms: function() {
	// Autofill the ticket purchasing form with the user info (if the user is logged in)
		var yearly = $("#yearly-membership-form");
		if(yearly.length) {
			//console.log("yearly");
			$.ajax({
				url: "/ajax?plan=yearly-membership",
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log("success");
					yearly.html(result);
					var admin = "";
					if(result.indexOf("Admin") != -1) admin = "-admin";
					ps.stripe.setupForm("yearly-membership"+admin);
				}
			});
		}
		var quarterly = $("#quarterly-membership-form");
		if(quarterly.length) {
			//console.log("quarterly");
			$.ajax({
				url: "/ajax?plan=quarterly-membership",
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result);
					quarterly.html(result);
					var admin = "";
					if(result.indexOf("Admin") != -1) admin = "-admin";
					ps.stripe.setupForm("quarterly-membership"+admin);
				}
			});
		}
		
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setupForm
	//-----------------------------------------------------------------------------------------------------------------------------
	setupForm: function(id) {
		//console.log("setupForm:"+id);
		var handler = StripeCheckout.configure({
		  key: 'pk_live_LUVor9N6xyM239Nrc8l5y3pb',
		  image: 'https://producerssocial.com/images/logoonly.jpg',
		  locale: 'auto',
		  zipCode: true,
		  token: function(token) {
		  	  //console.log("TOKEN:"+token);
		  	  //window.location.href = "/ajax?subscribe="+id+"&token="+token.id;
		  	  $.ajax({
					url: "/ajax?subscribe="+id+"&token="+token.id,
					dataType: 'text', 
					error: function(xhr, status, error) {
						console.trace();
						console.error(error);
					},
					success: function(result) {
						//console.log(id+":success");
						var area = $("#membership_page");
						area.html(result);
						
						if(result.indexOf("error") == -1) {
							setTimeout(function() {
								//window.location.href = "/members";
							}, 4000);
						}
					}
			  });
		  }
		});
		
		var amount = $("#"+id+"-amount").val();
		var name = $("#"+id+"-name").val();
		//console.log(id+": "+amount);
		
		var button = $("#"+id+"-button");
		if(button.length) {
			button.click(function(e) {
			  handler.open({
				name: 'Producers Social',
				description: name,
				amount: parseInt(amount),
				zipCode: true
			  });
			  e.preventDefault();
			});
			
			window.addEventListener('popstate', function() {
				handler.close();
			});		
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setupButtons
	//-----------------------------------------------------------------------------------------------------------------------------
	setupButtons: function() {
		$(".purchase-button").each(function(index) {
			var item 		= $(this).attr('bt-item');
			var name 		= $(this).attr('bt-name');
			var price 		= $(this).attr('bt-price');
			var quantity 	= $(this).attr('bt-quantity');
			var redirect 	= $(this).attr('bt-redirect');
			var target 		= $(this).attr('bt-target');
			//console.log("purchase-button:"+item+" price:"+price+" name:"+name+" redirect:"+redirect);
			ps.stripe.setupButton(this, item, name, price, quantity, redirect, target);
		});
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setupButton
	//-----------------------------------------------------------------------------------------------------------------------------
	setupButton: function(button, item, name, price, quantity, redirect, target) {
		//console.log("setupButton:"+item);
		var handler = StripeCheckout.configure({
		  key: 'pk_live_LUVor9N6xyM239Nrc8l5y3pb',
		  image: 'https://producerssocial.com/images/logoonly.jpg',
		  locale: 'auto',
		  zipCode: true,
		  token: function(token) {
		  	  //console.log("TOKEN:"+token);
		  	  var url = "/ajax?purchase="+item+"&quantity="+quantity+"&token="+token.id;
		  	  //window.location.href = url;
		  	  $.ajax({
					url: url,
					dataType: 'text', 
					error: function(xhr, status, error) {
						console.trace();
						console.error(error);
					},
					success: function(result) {
						//console.log(item+":success");
						//console.log(result);
						if(target.length) {
							$("#"+target).html(result);
						}
						
						if(result.indexOf("error") == -1) {
							if(redirect) {
								setTimeout(function() {
									if(window.location.href == redirect) {
										window.location.reload();
									}
									else {
										window.location.href = redirect;
									}
								}, 2500);
							}
						}
					}
			  });
		  	}
		});
		
		button = $(button);
		if(button.length) {
			button.click(function(e) {
			  handler.open({
				name: 'Producers Social',
				description: name,
				amount: parseInt(price),
				zipCode: true
			  });
			  e.preventDefault();
			});
			
			window.addEventListener('popstate', function() {
				handler.close();
			});		
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: profile
//-----------------------------------------------------------------------------------------------------------------------------
ps.profile = {
	zoom: 1,
	reservedHandles: [
		'!drafts',
		'!stacks',
		'!trash',
		'ajax',
		'blog',
		'blogsearch',
		'dashboard',
		'download_file',
		'eventinfo',
		'events',
		'eventsxml',
		'feedback',
		'ipn',
		'login',
		'loginhelp',
		'manage',
		'members',
		'membership',
		'memberships',
		'moonclerk',
		'ondeck',
		'page_forbidden',
		'page_not_found',
		'problog',
		'profile',
		'purchase_complete',
		'register',
		'reminders',
		'stripetest',
		'validate',
		
		// From doc
		'ableton','about','access','account','accounting','active','add','adds','advanced','advertisers','affiliate','affiliates',
		'africa','airbnb','am','andrioid','anual','api','app','app-store','apple','application','appstore','artist','artist-spotlight',
		'artistspotlight','asia','asshole','austin','auth','authorize','beta','bitch','bitching','blog','booking','buy','cal','call',
		'canada','caption','cart','celeb','celebrities','chat','check-out','checkout','confirm','confirmedemail','confirmemail','confrence',
		'confrencecall','contact','contactus','contribute','crew','crop','cunt','customize','deactivatd','declined','delete','delete-profile',
		'deleteprofile','delivery','dev','developer','developers','direct-message','directions','directmessage','dl','dm','download','drive',
		'dropbox','drugs','edit','editprofile','europe','event','eventsummary','expanding','explore','facebook','fag','fake','featured',
		'feed','fm','forum','founders','fuck','gallery','gay','general','gethelp','git','git-hub','github','give-aways','giveaway',
		'giveaways','google','hack','hangout','harddick','help','hiring','history','homo','hooker','host','how-to','howto','hq',
		'imageuploader','inbox','insta','instagram','ios','itunes','jobs','jobs','law','law','learn','legal','listview','live',
		'live','live-stream','livestream','location','location','locations','locations','log-in','logic','login','lounge','lyft',
		'mac','mac','mail','map','map-view','mapview','meet_up','meet-up','meetup','merch','merchendise','messages','meth','model',
		'monthly','mothism','music','namm','news','northamerica','notifications','off-air','offair','offering','on_deck','on-air',
		'on-deck','onair','parites','partnership','partnerships','party','password','pay-pal','payments','payments','paypal','pending',
		'penis','photo','photos','pivotal','post','presets','presets','press','preview','prices','privacy-policy','privacypolicy',
		'private','prize','prizes','producer','producers','producers-social','producersocial','producerssocial','promo','promoter-list',
		'promoterlist','promotion','promotional','psx','public','pussy','question','questions','radio','rape','reactivate','read',
		'recomended','remix','remixcomp','remixcompetition','resources','retarded','review','reviews','rhq','save','scam','scan',
		'school','schools','scolarships','search','secret','serial','settings','share','shoppingcart','show','sign-up','signup',
		'sms','snort','social','socialites','software','soundcloud','sourse','splice','sponshorships','sponsor','staff','start',
		'story','stream','stripe','style','styleguide','subpac','sucks','support','sxsw','sync','tax','taxes','teach','teachers',
		'team','tech','terms-of-service','termsofservice','tickets','tickets','tits','tos','tour','trademark','troll','tutor',
		'tutors','tv','twat','twit','twitter','uber','unread','upcoming','upgrade','url','validation','vanity','vanity-url',
		'vanityurl','venue','verify','vetting','videos','vimeo','vip','volume','w9','webapp','website','win','wip','workshop','workshops',
		
		// Google bad words
		'4r5e','5h1t','5hit','a55','anal','anus','ar5e','arrse','arse','ass','ass-fucker','asses','assfucker','assfukka','asshole',
		'assholes','asswhole','a_s_s','b!tch','b00bs','b17ch','b1tch','ballbag','balls','ballsack','bastard','beastial','beastiality',
		'bellend','bestial','bestiality','bi+ch','biatch','bitch','bitcher','bitchers','bitches','bitchin','bitching','bloody','blow_job',
		'blowjob','blowjobs','boiolas','bollock','bollok','boner','boob','boobs','booobs','boooobs','booooobs','booooooobs','breasts',
		'buceta','bugger','bum','bunny_fucker','butt','butthole','buttmuch','buttplug','c0ck','c0cksucker','carpet_muncher','cawk','chink',
		'cipa','cl1t','clit','clitoris','clits','cnut','cock','cock-sucker','cockface','cockhead','cockmunch','cockmuncher','cocks',
		'cocksuck_','cocksucked_','cocksucker','cocksucking','cocksucks_','cocksuka','cocksukka','cok','cokmuncher','coksucka','coon',
		'cox','crap','cum','cummer','cumming','cums','cumshot','cunilingus','cunillingus','cunnilingus','cunt','cuntlick_','cuntlicker_',
		'cuntlicking_','cunts','cyalis','cyberfuc','cyberfuck_','cyberfucked_','cyberfucker','cyberfuckers','cyberfucking_','d1ck','damn',
		'dick','dickhead','dildo','dildos','dink','dinks','dirsa','dlck','dog-fucker','doggin','dogging','donkeyribber','doosh','duche',
		'dyke','ejaculate','ejaculated','ejaculates_','ejaculating_','ejaculatings','ejaculation','ejakulate','f_u_c_k','f_u_c_k_e_r',
		'f4nny','fag','fagging','faggitt','faggot','faggs','fagot','fagots','fags','fanny','fannyflaps','fannyfucker','fanyy','fatass',
		'fcuk','fcuker','fcuking','feck','fecker','felching','fellate','fellatio','fingerfuck_','fingerfucked_','fingerfucker_',
		'fingerfuckers','fingerfucking_','fingerfucks_','fistfuck','fistfucked_','fistfucker_','fistfuckers_','fistfucking_',
		'fistfuckings_','fistfucks_','flange','fook','fooker','fuck','fucka','fucked','fucker','fuckers','fuckhead','fuckheads',
		'fuckin','fucking','fuckings','fuckingshitmotherfucker','fuckme_','fucks','fuckwhit','fuckwit','fudge_packer','fudgepacker',
		'fuk','fuker','fukker','fukkin','fuks','fukwhit','fukwit','fux','fux0r','f_u_c_k','gangbang','gangbanged_','gangbangs_',
		'gaylord','gaysex','goatse','God','god-dam','god-damned','goddamn','goddamned','hardcoresex_','hell','heshe','hoar','hoare',
		'hoer','homo','hore','horniest','horny','hotsex','jack-off_','jackoff','jap','jerk-off_','jism','jiz_','jizm_','jizz','kawk',
		'knob','knobead','knobed','knobend','knobhead','knobjocky','knobjokey','kock','kondum','kondums','kum','kummer','kumming',
		'kums','kunilingus','l3i+ch','l3itch','labia','lmfao','lust','lusting','m0f0','m0fo','m45terbate','ma5terb8','ma5terbate',
		'masochist','master-bate','masterb8','masterbat*','masterbat3','masterbate','masterbation','masterbations','masturbate','mo-fo',
		'mof0','mofo','mothafuck','mothafucka','mothafuckas','mothafuckaz','mothafucked_','mothafucker','mothafuckers','mothafuckin',
		'mothafucking_','mothafuckings','mothafucks','mother_fucker','motherfuck','motherfucked','motherfucker','motherfuckers',
		'motherfuckin','motherfucking','motherfuckings','motherfuckka','motherfucks','muff','mutha','muthafecker','muthafuckker',
		'muther','mutherfucker','n1gga','n1gger','nazi','nigg3r','nigg4h','nigga','niggah','niggas','niggaz','nigger','niggers_',
		'nob','nob_jokey','nobhead','nobjocky','nobjokey','numbnuts','nutsack','orgasim_','orgasims_','orgasm','orgasms_','p0rn',
		'pawn','pecker','penis','penisfucker','phonesex','phuck','phuk','phuked','phuking','phukked','phukking','phuks','phuq',
		'pigfucker','pimpis','piss','pissed','pisser','pissers','pisses_','pissflaps','pissin_','pissing','pissoff_','poop','porn',
		'porno','pornography','pornos','prick','pricks_','pron','pube','pusse','pussi','pussies','pussy','pussys_','rectum','retard',
		'rimjaw','rimming','s_hit','s.o.b.','sadist','schlong','screwing','scroat','scrote','scrotum','semen','sex','sh!+','sh!t',
		'sh1t','shag','shagger','shaggin','shagging','shemale','shi+','shit','shitdick','shite','shited','shitey','shitfuck','shitfull',
		'shithead','shiting','shitings','shits','shitted','shitter','shitters_','shitting','shittings','shitty_','skank','slut','sluts',
		'smegma','smut','snatch','son-of-a-bitch','spac','spunk','s_h_i_t','t1tt1e5','t1tties','teets','teez','testical','testicle',
		'tit','titfuck','tits','titt','tittie5','tittiefucker','titties','tittyfuck','tittywank','titwank','tosser','turd','tw4t',
		'twat','twathead','twatty','twunt','twunter','v14gra','v1gra','vagina','viagra','vulva','w00se','wang','wank','wanker','wanky',
		'whoar','whore','willies','willy','xrated','xxx'
	],
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init	: Main startup routine
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			ps.profile.avatarEditor();
			
			$("#uHandle").keypress(function (e) {
				var regex = new RegExp("^[a-zA-Z0-9_-]+$");
				var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
				
				setTimeout(function() {
					ps.profile.updateHandle();
				}, 500);
				
				if(regex.test(str)) {
					return true;
				}
			
				e.preventDefault();
				return false;
			});
			$("#uHandle").blur(function(event) {
				ps.features.updateHandle();
			});
			
			$("#uMobile").mask('(000) 000-0000');

			var form = $("#profile-edit-form");
			form.submit(function(event) {
				if($("#handle_valid").val() == 0) {
					ps.alert("The custom URL handle you entered is invalid. Please correct this then try again.");
					event.preventDefault();
				}
				else {
					/*
					var featured = $("#uFeaturedTrack").val().trim().replace(/iframe/g, "xframe");
					$("#uFeaturedTrack").val(featured); 
					
					//var bio = $("#uBio").val().trim().replace(/iframe/g, "xframe");
					var bio = tinymce.get('uBio').getContent().trim().replace(/iframe/g, "xframe");
					$("#uBio").val(bio); 
					console.log($("#uBio").val());
					*/
				}
				/*
				event.preventDefault();
				var data = form.serialize();
				console.log(data);
				$.ajax({
					url: form.attr('action'),
					type: 'POST',
					data: data,
					dataType: 'text', 
					processData: false,
					contentType: false,
					error: function(xhr, status, error) {
						console.log("ERROR:"+error);
					},
					success: function(data, status, xhr) {
						//form.html(data);
						console.log(data);
					}
				});
				*/
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: popup
	//-----------------------------------------------------------------------------------------------------------------------------
	popup: function(user_id) {
		$("#user_profile_popup").popup('show');
		$("#user_profile_content").empty();
		$.ajax({
			url: "/ajax?profile="+user_id,
			type:'POST',
			success: function(result) {
				$("#user_profile_content").html(result);
			}
		});
	},
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateHandle
	//-----------------------------------------------------------------------------------------------------------------------------
	updateHandle: function() {
		//console.log("ps.profile.updateHandle");
		var handle = $("#uHandle");
		var preview = $("#handle_preview");
		var val = handle.val().toLowerCase();
		preview.html(val);
		
		if(val.length < 3 || ps.profile.reservedHandles.indexOf(val) != -1) {
			$("#handle_validate").html("<span class='error'><i class='fa fa-warning' aria-hidden='true'></i> Taken</i></span>");
			$("#handle_valid").val(0);
		}
		else {
			var uid = $("#uid").val();
			if(uid == 'undefined') uid = null;
			$.ajax({
				url: "/ajax?verifyhandle="+val+"&uid="+uid,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result);
					$("#handle_validate").html(result);
					if(result.indexOf("Taken") == -1) {
						$("#handle_valid").val(1);
					}
					else {
						$("#handle_valid").val(0);
					}
				}
			});
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: avatarEditor
	//-----------------------------------------------------------------------------------------------------------------------------
	avatarEditor: function() {
		var avatar = $("#avatar_container");
		if(avatar.length) {
			//console.log("avatarEditor");
			if($('.image-editor') !== undefined) {
				var zoom = 1;//499 / $('.cropit-preview').width();
				//console.log("zoom:"+zoom);
				$('.cropit-preview').height($('.cropit-preview').width());
				$('.image-editor').cropit({
	 				maxZoom:10,
					minZoom:'fill',
					initialZoom: 'image',
					exportZoom:zoom,
					imageState: {
						src: '',//'/files/avatars/1.jpg',
					},
					onImageLoaded: function() {
						ps.profile.zoom = $('.image-editor').cropit('zoom');
						//$("#profile_image_saved").html("onImageLoaded");
					},
					onFileReaderError: function() {
						$("#profile_image_saved").html("There was an error reading the file selected. Please check that it meets the requirements and try again.");
					},
					onImageError: function(obj, num, err) {
						$("#profile_image_saved").html("<div class='alert alert-warning'>The image uploaded does not meet the requirements.<br>Please upload a JPEG or PNG of at 500x500 pixels large.</div>");
					}
				});
				$('.image-editor').cropit('setupZoomer', 1);
				
				$('.rotate-cw').click(function() {
					$('.image-editor').cropit('rotateCW');
				});
				$('.rotate-ccw').click(function() {
					$('.image-editor').cropit('rotateCCW');
				});
				$('.zoom-out').click(function() {
					ps.profile.zoom *= 0.9;
					//console.log("zoom-out:"+ps.profile.zoom);
					$('.image-editor').cropit('zoom', ps.profile.zoom);
					ps.profile.zoom = $('.image-editor').cropit('zoom');
					//console.log("zoom:"+ps.profile.zoom);
				});
				$('.zoom-in').click(function() {
					//console.log("zoom-in");
					ps.profile.zoom *= 1.1;
					//console.log("zoom-in:"+ps.profile.zoom);
					$('.image-editor').cropit('zoom', ps.profile.zoom);
					ps.profile.zoom = $('.image-editor').cropit('zoom');
					//console.log("zoom:"+ps.profile.zoom);
				});
	
				$('.export').click(function() {
					var imageData = $('.image-editor').cropit('export', {type: 'image/jpeg', originalSize: true, quality: .75});
					window.open(imageData);
				});
				
				$("#profile_image_form").submit(function(e) {
					//console.log("SAVE");
					e.preventDefault();
			
					var img = $("#profile_image_results > img");
					if(img == null || img.attr("src") == "" || img.attr("src") == "undefined") {
						alert("Please upload and crop an image first");
					}
					else {
						//console.log('src:'+img.attr("src"));
						var uid = $("#uid").val();
						var form = $("#profile_image_form");
						var imageData = $('.image-editor').cropit('export', {type: 'image/jpeg', quality: .75});
						var fd = new FormData();
						fd.append('fname', 'avatar.jpg');
						fd.append('uid', $("#uid").val());
						fd.append('data', imageData);
				
						//console.log("UID:"+$("#uid").val());
						//console.log(form.attr('action'));
						$.ajax({
							url: form.attr('action'),
							type: 'POST',
							async: true,
							data: fd,
							dataType: 'text', 
							processData: false,
							contentType: false,
							error: function(xhr, status, error) {
								console.log("ERROR:"+error);
							},
							success: function(data, status, xhr) {
								var saved = $("#profile_image_saved");
								//console.log(data);
								if(data.indexOf("http") == 0) {
									document.location = data;
								}
								else {
									saved.html(data);
								}
							}
						});
					}
				});
			}
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showAvatarUpload
	//-----------------------------------------------------------------------------------------------------------------------------
	showAvatarUpload: function() {
		var cont = $("#crop_img_container");
		cont.css('display', 'block');
	
		var sel = $("#profile_select_image");
		sel.css('display', 'none');
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: hideAvatarUpload
	//-----------------------------------------------------------------------------------------------------------------------------
	hideAvatarUpload: function() {
		var cont = $("#crop_img_container");
		cont.css('display', 'none');
	
		var sel = $("#profile_select_image");
		sel.css('display', 'block');
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: saveAvatar
	//-----------------------------------------------------------------------------------------------------------------------------
	saveAvatar: function() {
		$("#profile_image_form").submit();
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: reportUser
	//-----------------------------------------------------------------------------------------------------------------------------
	reportUser: function(userid, byuser) {
		if(ps.confirm("Are you sure you want to report abuse by this user? If you continue, our admins will be notified of this user's account and will investigate the issue.")) {
			$.ajax({
				url: "/ajax?reportabuse="+userid+"&byuser="+byuser,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					$(".profile_report_user").html('User Reported!');
					$(".profile_report_user").attr('onclick', "");
					//console.log(result);
				}
			});
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: reportUser
	//-----------------------------------------------------------------------------------------------------------------------------
	reportClear: function(userid, byuser) {
		if(ps.confirm("Are you sure you want to remove the 'reported' status for this user?")) {
			$.ajax({
				url: "/ajax?reportclear="+userid+"&byuser="+byuser,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result);
					$(".profile_user_reported").remove();
				}
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: events
//-----------------------------------------------------------------------------------------------------------------------------
ps.events = {
	isList: true,

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			if($('#calendar').length) {
				$('#calendar').monthly({
					mode: 'event',
					xmlUrl: '/eventsxml'
				});
			}
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleCalendar
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleCalendar: function() {
		var list = $("#events_list_view");
		var cal = $("#events_calendar_view");
		var toggle = $("#events_list_toggle");
		
		if(ps.events.isList) {
			list.hide();
			cal.show();
			toggle.html("<i class='fa fa-list' aria-hidden='true'></i> Switch to Calendar");
		}
		else {
			list.show();
			cal.hide();
			toggle.html("<i class='fa fa-calendar' aria-hidden='true'></i> Switch to Calendar");
		}
		ps.events.isList = !ps.events.isList;
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: purchaseTicket
	//-----------------------------------------------------------------------------------------------------------------------------
	purchaseTicket: function() {
		var paypal_form = $("#paypal_form");
		if(paypal_form) {
			//console.log("Paypal Submit");
			//var ticket_name = $("#ticket_name");
			//var ticket_email = $("#ticket_email");
			
			//console.log("ticket_name:"+ticket_name.val());
			//console.log("ticket_email:"+ticket_email.val());
			
			var error = null;
			//if(ticket_name == "undefined" || ticket_name.val() == "" || ticket_email == "undefined" || ticket_email.val() == "") {
			//	error = "Please enter your name and email address to continue.";
			//}
			
			error = ps.events.updatePurchaseTotal();
			
			if(error) {
				//console.log("error:"+error);
				var paypal_error = $("#paypal_error");
				paypal_error.html("<div class='alert alert-danger'>"+error+"</div>");
			}
			else {
				paypal_form.submit();
			}
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updatePurchaseTotal
	//-----------------------------------------------------------------------------------------------------------------------------
	updatePurchaseTotal: function() {
		var error = "";
		var ticket_quantity = $("#ticket_quantity");
		var ticket_price 	= $("#ticket_price");
		var event_id 		= $("#ticket_event_id");
		
		if(typeof(ticket_quantity) == "undefined" || !ticket_quantity.val() || 
			typeof(ticket_price) == "undefined" || !ticket_price.val() ||
			typeof(event_id) == "undefined" || !event_id.val()
		) {
			//error = "An invalid quantity or price was specified.";
		}
		else {
			var total = ticket_price.val() * ticket_quantity.val();
			total = parseFloat(total).toFixed(2);
			//console.log("event_id:"+event_id.val());
			//console.log("ticket_price:"+ticket_price.val());
			//console.log("ticket_quantity:"+ticket_quantity.val());
			//console.log("total:"+total);

			var total_price	= $("#total_price");
			total_price.html("$"+total);
			
			$("input[name='quantity']").val(ticket_quantity.val());
			$("input[name='amount']").val(ticket_price.val());
			
			var num = ticket_quantity.val();
			var itemID = "ticket-"+event_id.val();
			var buttonID = "#"+itemID+"-button";
			var button = $(buttonID);
			var price = ticket_price.val().replace(".", "");
			price = price * num;
			
			var buttonName = "Buy "+num+" Ticket"+(num > 1 ? "s" : "");
			
			button.html(buttonName);
			button.attr('bt-quantity', num);
			button.attr('bt-price', price);
			button.attr('bt-name', buttonName);
			
			//console.log('bt-quantity:'+num);
			//console.log('bt-price:'+price);
			//console.log('bt-name:'+buttonName);
			
			ps.stripe.setupButton(buttonID, itemID, buttonName, price, num, button.attr('bt-redirect'), button.attr('bt-target'));
		}
		
		var rsvpButton = $("#event_ticket_rsvp_button");
		if(rsvpButton.length) {
			var num = ticket_quantity.val();
			rsvpButton.html("RSVP "+num+" Ticket"+(num > 1 ? "s" : ""));
		}
		return error;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: autoSignupToggle
	//-----------------------------------------------------------------------------------------------------------------------------
	autoSignupToggle: function(ticketid) {
		var toggle = $("#auto_signup_toggle");
		var autosignup = $("#autosignup");
		if(toggle.length && autosignup.length) {
			var ison = autosignup.val() == 1 ? 0 : 1;
			autosignup.val(ison);
			
			var checkmark = "<i class='fa fa-square-o' aria-hidden='true'></i>";
			if(ison) {
				checkmark = "<i class='fa fa-check-square-o' aria-hidden='true'></i>";
			}
			toggle.html(checkmark);
			
			$("#automatic_signup_title").html(ison ? "Automatic Sign-Up On!" : "Automatic Sign-Up Off");
			
			$.ajax({
				url: "/ajax?autosignup="+ison+"&id="+ticketid,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log("success");
					//console.log(result);
				}
			});
		}
		return false;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: rsvp
	//-----------------------------------------------------------------------------------------------------------------------------
	rsvp: function(eventid) {
		var ticket_quantity = $("#ticket_quantity");
		if(!ticket_quantity.length) {
			ps.error("Ticket quantity field is missing!");
		}
		else {
			var quantity = ticket_quantity.val();
			if(quantity <= 0) {
				ps.alert("Please select the number of tickets you wish to RSVP for this event");
			}
			else {
				$.ajax({
					url: "/ajax?rsvp="+quantity+"&eventid="+eventid,
					dataType: 'text', 
					error: function(xhr, status, error) {
						console.trace();
						console.error(error);
					},
					success: function(result) {
						//console.log("success");
						//console.log(result);
						if(result.indexOf("Error") != -1 || result.indexOf("Sorry") != -1) {
							alert(result);
						}
						else {
							window.location.reload();
						}
					}
				});
			}
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: unrsvp
	//-----------------------------------------------------------------------------------------------------------------------------
	unrsvp: function(eventid) {
		if(eventid && eventid.length) {
			$.ajax({
				url: "/ajax?unrsvp="+eventid,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result);
					window.location.reload();
				}
			});
		}
		else {
			ps.error("The event id is invalid.");	
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: unrsvpTicket
	//-----------------------------------------------------------------------------------------------------------------------------
	unrsvpTicket: function(ticketid) {
		if(ticketid && ticketid.length) {
			$.ajax({
				url: "/ajax?unrsvpticket="+ticketid,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result);
					window.location.reload();
				}
			});
		}
		else {
			ps.error("The event id is invalid.");	
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: validateNewSession
	//-----------------------------------------------------------------------------------------------------------------------------
	validateNewSession: function() {
		//console.log("validateNewSession");
		var pass = true;
		var error = null;
		var location = $("select#location");
		var event_name = $("#event_name");
	
		if(typeof(location) != "undefined") {
			if(location.val() == "0") {
				pass = false;
				error = "Please select a location";
			}
		}
	
		if(typeof(event_name) != "undefined") {
			if(event_name.val() == "") {
				pass = false;
				error = "Please enter a name for the event";
			}
		}
		if(error) {
			$("#events_custom_session_error").html("<div class='alert alert-danger'>"+error+"</div>");
		}
	
		return pass;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: locationChanged
	//-----------------------------------------------------------------------------------------------------------------------------
	locationChanged: function(id) {
		var select = $("#"+id);
		//console.log("locationChanged:"+select.val());
		var url = "/events?location="+select.val();
		window.location.href = url;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: feedback
//-----------------------------------------------------------------------------------------------------------------------------
ps.feedback = {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			$("#feedback_form").submit(function(e) {
				e.preventDefault();
		
				if(ps.feedback.validate()) {
					var formData = new FormData();    
					formData.append( 'feedback_title', $('#feedback_title').val());
					formData.append( 'feedback_text', $('#feedback_text').val());
					formData.append( 'feedback_file', $('#feedback_file')[0].files[0]);
				
					//console.log(formData);
				
					$.ajax({
						url: $("#feedback_form").attr('action'),
						type: 'POST',
						data: formData,
						cache: false, 
						processData: false,
						contentType: false,
						error: function(xhr, status, error) {
							console.log("ERROR:"+error);
						},
						success: function(data, status, xhr) {
							//console.log(data);
				
							$("#feedback_form").addClass("hidden");
							$("#feedback_thankyou").removeClass("hidden");
							$("#feedback_title").val("");
							$("#feedback_text").val("");
							window.setTimeout(ps.feedback.close, 5000);
						
							$("#feedback_result").html(data);
						}
					});
				}
			});
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: validate
	//-----------------------------------------------------------------------------------------------------------------------------
	validate: function() {
		var pass = true;
		if($("#feedback_title").val() == "") {
			pass = false;
			$("#feedback_error").html("Please enter a title");
		}
		else
		if($("#feedback_text").val() == "") {
			pass = false;
			$("#feedback_error").html("Please describe the issue");
		}
		return pass;
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: open
	//-----------------------------------------------------------------------------------------------------------------------------
	open: function() {
		$("#feedback_window").removeClass("hidden");
		$("#feedback_form").removeClass("hidden");
		$("#feedback_thankyou").addClass("hidden");
		$("#feedback_button").addClass("hidden");
		$("#feedback_error").html("");
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: close
	//-----------------------------------------------------------------------------------------------------------------------------
	close: function() {
		$("#feedback_window").addClass("hidden");
		$("#feedback_button").removeClass("hidden");
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleEmail
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleEmail: function() {
		$("#feedback_email").toggle();
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: members
//-----------------------------------------------------------------------------------------------------------------------------
ps.members = {
	xhr: null,
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			if($(window).width() < 768) {
				ps.members.toggleNav(false);
			}
			
			ps.members.uploadForm();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: uploadForm
	//-----------------------------------------------------------------------------------------------------------------------------
	uploadForm: function() {
		var form = $("#item_upload_form");
		if(form.length) {
			var bar 		= $('#upload_progress_bar_inner');
			var percent 	= $('#upload_progress_percent');
			var status 		= $('#upload_progress_status');
			var progress 	= $('#upload_progress');
		
			form.ajaxForm({
				beforeSend: function() {
					ps.members.xhr = this;
					status.empty();
					progress.show();
					form.hide();
					var percentVal = '0%';
					bar.width(percentVal);
					percent.html(percentVal);
				},
				uploadProgress: function(event, position, total, percentComplete) {
					var percentVal = percentComplete + '%';
					bar.width(percentVal);
					percent.html(percentVal);
				},
				complete: function(xhr) {
					console.log(xhr.responseText);
					form.show();
					form.resetForm();
					progress.hide();
					status.html(xhr.responseText);
					//setTimeout(function() {
						window.location.reload();
					//}, 3000);
				}
			});		
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: cancelUpload
	//-----------------------------------------------------------------------------------------------------------------------------
	cancelUpload: function(form) {
		if(ps.members.xhr) {
			$(ps.members.xhr).abort();
		}
		form = $(form);
		form.show();
		form.resetForm();
		return false;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: deleteFile
	//-----------------------------------------------------------------------------------------------------------------------------
	deleteFile: function(item, file, rowid) {
		if(confirm("Are you sure you want to permanently delete the file '"+file+"' from the server?")) {
			$.ajax({
				url: "/ajax?deletefile="+file+"&item="+item,
				dataType: 'text',  
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					$("#"+rowid).html(result);	
				}
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleNav
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleNav: function(show) {
		var left 	= $("#members_nav_arrow_left");
		var right 	= $("#members_nav_arrow_right");
		var nav 	= $("#members_content_left");
		
		if(show) {
			nav.removeClass("collapse");
			left.show();
			right.hide();
		}
		else {
			nav.addClass("collapse");
			left.hide();
			right.show();
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: like
	//-----------------------------------------------------------------------------------------------------------------------------
	like: function(item) {
		if(item) {
			var heart = $("#like_"+item);
			var like = $("#likeval_"+item);
			var showCount = $("#likecount_"+item);
			var liked = parseInt(like.val()) ? "0" : "1";
			
			like.val(liked);

			$.ajax({
				url: "/ajax?like="+liked+"&item="+item+"&showcount="+showCount.val(),
				dataType: 'text',  
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result);
					if(result.indexOf("LOGIN") != -1) {
						var url = result.substr(result.indexOf(":")+1);
						window.location = "/login?redirect="+url;
					}
					else {
						heart.html(result);	
					}
				}
			});
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};

//-----------------------------------------------------------------------------------------------------------------------------
//: features
//-----------------------------------------------------------------------------------------------------------------------------
ps.features = {
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init	: Main startup routine
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		if(!re) {
			$("#handle").keypress(function (e) {
				var regex = new RegExp("^[a-zA-Z0-9_-]+$");
				var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
				
				setTimeout(function() {
					ps.features.updateHandle();
				}, 500);
				
				if(regex.test(str)) {
					return true;
				}
			
				e.preventDefault();
				return false;
			});
			$("#handle").blur(function(event) {
				ps.features.updateHandle();
			});
			
			$("#edit-feature-form").submit(function(event) {
				//console.log('edit-feature-form:'+$("#handle_valid").val());
				if($("#handle_valid").val() == 0) {
					ps.alert("The handle you entered is invalid. Please correct this then try again.");
					event.preventDefault();
				}
			});
			
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateHandle
	//-----------------------------------------------------------------------------------------------------------------------------
	updateHandle: function() {
		console.log("ps.features.updateHandle");
		var handle = $("#handle");
		var preview = $("#handle_preview");
		var val = handle.val().toLowerCase();
		preview.html(val);
		
		if(val.length < 3 || ps.profile.reservedHandles.indexOf(val) != -1) {
			$("#handle_validate").html("<span class='error'><i class='fa fa-warning' aria-hidden='true'></i> Taken</i></span>");
			$("#handle_valid").val(0);
		}
		else {
			var uid = $("#id").val();
			if(uid == 'undefined') uid = null;
			var url = "/ajax?verifyfeaturehandle="+val+"&id="+uid;
			$.ajax({
				url: url,
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
				},
				success: function(result) {
					//console.log(result+":"+result.indexOf("Taken"));
					$("#handle_validate").html(result);
					if(result.indexOf("Taken") == -1) {
						$("#handle_valid").val(1);
					}
					else {
						$("#handle_valid").val(0);
					}
				}
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateItemHandle
	//-----------------------------------------------------------------------------------------------------------------------------
	updateItemHandle: function(re) {
		var name = $("input#name");
		var handle = $("input#handle");
		var newHandle = name.val().trim();
		newHandle = newHandle.split('-').join('_');
		newHandle = newHandle.split(' ').join('_');
		newHandle = newHandle.split('.').join('_');
		newHandle = newHandle.split(',').join('');
		newHandle = newHandle.split('/').join('_');
		newHandle = newHandle.split(':').join('_');
		newHandle = newHandle.split(';').join('_');
		newHandle = newHandle.split('!').join('_');
		newHandle = newHandle.split('`').join('_');
		newHandle = newHandle.split('#').join('_');
		newHandle = newHandle.split('@').join('_');
		newHandle = newHandle.split('%').join('');
		newHandle = newHandle.split('^').join('');
		newHandle = newHandle.split('*').join('');
		newHandle = newHandle.split('(').join('');
		newHandle = newHandle.split(')').join('');
		newHandle = newHandle.split('&').join('_');
		newHandle = newHandle.split('+').join('_');
		newHandle = newHandle.split("'").join('');
		newHandle = newHandle.split('"').join('');
		newHandle = newHandle.split('__').join('_');
		newHandle = newHandle.split('__').join('_');
		newHandle = newHandle.split('__').join('_');
		newHandle = encodeURI(newHandle.toLowerCase());
		$("#handle_preview").html(newHandle);
		handle.val(newHandle);
		
		ps.features.updateHandle();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateItemHandleDirect
	//-----------------------------------------------------------------------------------------------------------------------------
	updateItemHandleDirect: function(re) {
		$("#handle_preview").html($("input#handle").val());
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateItemStatus
	//-----------------------------------------------------------------------------------------------------------------------------
	updateItemStatus: function(isTop) {
		/*
		if(isTop) {
			$("#status2").val($("#status").val());
		}
		else {
			$("#status").val($("#status2").val());
		}
		*/
	},
};

//-----------------------------------------------------------------------------------------------------------------------------
//: submissions
//-----------------------------------------------------------------------------------------------------------------------------
ps.submissions = {
	xhr: null,
	
	init: function(re) 
	{
		if(!re) {
			ps.submissions.submitForm();
		}
	},
	
	validate: function() 
	{
		//console.log("validate");
		var title = $("#submit_title").val();
		var artist = $("#submit_artist").val();
		var genre = $("#submit_genre").val();
		var email = $("#submit_email").val();
		var file = $("#submit_file").val();
		var agree = $("#submit_agree").attr('checked');
		
		var msg = "";
		
		if(title == "") {
			msg = "Please enter the title of your track";	
		}
		else
		if(artist == "") {
			msg = "Please enter your artist name";	
		}
		else
		if(genre == "") {
			msg = "Please enter at least one genre";	
		}
		else
		if(email == "") {
			msg = "Please provide your email address";	
		}
		else
		if(file == "") {
			msg = "Please select a file to upload";	
		}
		else
		if(!agree) {
			msg = "You must accept our agreement to submit.";	
		}
		location.href = "#featuretop";
	
		if(msg != "") {
			//console.log("ERROR:"+msg);
			$("#submit_form_message").html("<div class='alert alert-warning'>"+msg+"</div>");
			return false;
		}
		else {
			//console.log("submit");
			$("#submission-form").submit();	
		}
		return true;
	},
	
	submitForm: function() 
	{
		var form = $("#submission-form");
		if(form.length) {
			var bar 		= $('#submit_progress_bar_inner');
			var percent 	= $('#submit_progress_percent');
			var status 		= $('#submit_progress_status');
			var progress 	= $('#submit_progress');
		
			form.ajaxForm({
				beforeSend: function() {
					//console.log("beforeSend");
					ps.submissions.xhr = this;
					status.html("Submission in progress. Please wait for the upload to finish...");
					progress.show();
					form.hide();
					var percentVal = '0%';
					bar.width(percentVal);
					percent.html(percentVal);
				},
				uploadProgress: function(event, position, total, percentComplete) {
					var percentVal = percentComplete + '%';
					//console.log("uploadProgress:"+percentVal);
					bar.width(percentVal);
					percent.html(percentVal);
				},
				complete: function(xhr) {
					console.log("complete:"+xhr.responseText);
					//form.show();
					//form.resetForm();
					//progress.hide();
					status.html("Thank you! Your submission has finished uploading.");
					setTimeout(function() {
						var href = window.location.href;
						var n = href.indexOf("?");
						if(n > 0) {
							href = href.substr(0, n);
						}
						console.log("location:"+href);
						window.location = href;
					}, 500);
				}
			});		
			location.href = "#featuretop";
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: cancelUpload
	//-----------------------------------------------------------------------------------------------------------------------------
	cancelUpload: function(form) {
		if(ps.submissions.xhr) {
			$(ps.submissions.xhr).abort();
		}
		form = $(form);
		form.show();
		form.resetForm();
		return false;
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};


//-----------------------------------------------------------------------------------------------------------------------------
//: empty
//-----------------------------------------------------------------------------------------------------------------------------
ps.empty = {

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};




