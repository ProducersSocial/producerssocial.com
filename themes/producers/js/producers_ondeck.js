
//-----------------------------------------------------------------------------------------------------------------------------
//: ondeck
//-----------------------------------------------------------------------------------------------------------------------------
ps.ondeck = {
	hostid:					0,
	isPrimaryHost:			false,
	isLive:					false,
	refreshID:				"",
	view: 					"list",
	url: 					"",
	pin:					0,
	autoRefreshTimer: 		null,
	autoRefreshWait: 		10000,
	titleRefreshTimer: 		null,
	titleRefreshWait: 		30000,
	verify_cmd:				null,
	verify_value:			1,
	filter_region:			"All",
	filter_alpha:			"All",
	navIsOpen:				true,
	statusHidden:			false,
	enableAutoRefresh:		true,
	autoCloseNav:			false,
	haltRefresh:			false,
	isAjaxWaiting:			false,
	listEditMode:			false,
	usersScroll:			0,
	listScroll:				0,
	listScrollMobile:		0,
	countdownIsRunning: 	false,
	countdownIsStopped: 	false,
	countdownRefreshTimer: 	null,
	countdownLimit:			"00:00",
	countdownGoal:			0,
	countdownGoalTemp:		0,
	countdownStartTime:		0,
	countdownIsReset:		false,
	countdownCurrent:		"00:00",
	countdownTime:			0,
	countdownAutostart:		0,
	countdownHasFlashed:	false,
	lastAjaxUrl:			"",
	isPresentationMode:		false,
	membersOnly:			false,
	sessionRefresh:			30000,
	sessionTimeout:			null,
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: init	: Main startup routine
	//-----------------------------------------------------------------------------------------------------------------------------
	init: function(re) {
		//console.log("init");
		var hostid = $("#hostid").val();
		if(hostid != 0) {
			ps.ondeck.hostid = $.cookie('hostid');
			ps.ondeck.isPrimaryHost = $("#hostid").val() == ps.ondeck.hostid;
			console.log("val:"+$("#hostid").val()+" hostid:"+ps.ondeck.hostid+" isPrimary:"+ps.ondeck.isPrimaryHost);
		}
		else {
			ps.ondeck.isPrimaryHost = true;
		}
		if(!ps.ondeck.isPrimaryHost) {
			//console.log("primary_host_only");
			$(".primary_host_only").hide();//prop("disabled", true);
		}
		
		ps.ondeck.isLive = $("#ondeck_islive").val();
		//console.log("ps.ondeck.isLive:"+ps.ondeck.isLive);
		
		if(!re) {
			ps.ondeck.refreshID 	= $("#ondeck_refreshid").val();
			ps.ondeck.view 			= $("#ondeck_view").val();
			ps.ondeck.url 			= $("#ondeck_url").val();
			
			if(ps.ondeck.view == "session" && ps.ondeck.isLive == 1) {
				ps.ondeck.autoReload();
			}
			else {
				ps.ondeck.autoRefresh();
			}
			ps.ondeck.titleRefresh();
			ps.ondeck.navOpen();
			
			if($(window).width() < 768) {
				ps.ondeck.autoCloseNav = true;
			}
			
			$(window).resize(function() {
				ps.ondeck.sessionTableRefresh();
			});
			
			ps.ondeck.setupCountdown();
		
			if(ps.ondeck.hostid == undefined) {
				ps.ondeck.hostid = Math.floor(Math.random() * 1000000);
				$.cookie('hostid', ps.ondeck.hostid, { expires: 7, path: '/' });
			}

			$.ajax({
				url: ps.ondeck.url+"&ajax=pin&hostid="+$.cookie('hostid'),
				dataType: 'text', 
				error: function(xhr, status, error) {
					console.trace();
					console.error(error);
					ps.ondeck.setStatus(status+": "+error, "danger");
					//window.alert(status+": "+error);
				},
				success: function(result) {
					ps.ondeck.pin = parseInt(result);
				}
			});
		}
		
		$("#ondeck_list_scroll").scroll(function() {
			ps.ondeck.listScroll = $("#ondeck_list_scroll").scrollTop();
		});
		$("#ondeck_list_scroll_mobile").scroll(function() {
			ps.ondeck.listScrollMobile = $("#ondeck_list_scroll_mobile").scrollTop();
		});
		$("#ondeck_users").scroll(function() {
			ps.ondeck.usersScroll = $("#ondeck_users").scrollTop();
		});
			
		$("#suggestions_popup").popup({
			onopen: function() {},
			onclose: function() {}
		});
		
		if(ps.ondeck.statusHidden) {
			ps.ondeck.dismissStatus();
		}
		
		ps.ondeck.updateListEditMode();
		
		ps.ondeck.filterUpdate();
		
		$("#ondeck_table_inner").height($(window).height());
		
		var h = $("#ondeck_table").height();
		$("#ondeck_content").height(h);
		//console.log("h:"+h);
		
		var th = $("#ondeck_table_inner_top").height();
		var botHeight = h - th;
		$("#ondeck_table_inner_bot").height(botHeight);
		$("#ondeck_list_container").height(botHeight);
		$("#ondeck_list").height(botHeight);
		$("#ondeck_list_scroll").height(botHeight);
		$("#ondeck_session_container").height(botHeight);
		$("#ondeck_session").height(botHeight);
		$("#ondeck_users_container").height(botHeight);
		$("#ondeck_users").height(botHeight);
		
		ps.ondeck.sessionTableRefresh();
		ps.ondeck.sessionTableRefresh();
		ps.ondeck.sessionTableRefresh();
		ps.ondeck.sessionTableRefresh();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: sessionTableRefresh
	//-----------------------------------------------------------------------------------------------------------------------------
	sessionTableRefresh: function() {
		var split = $("#ondeck_session_split");
		var session = $("#ondeck_session");
		if(split && session && session.offset()) {
			var topOffset = (session.offset().top + 50);
			var height = $(window).height() - topOffset;
			split.height(height);
			//console.log("sessionTableRefresh:"+height+" top:"+topOffset);
			
			var avatar = $("#ondeck_avatar_nowplaying");
			var td = $("#ondeck_session_split_avatar");
			var w = td.width();
			if(w > td.height()) w = td.height();
			w *= 0.9;
			avatar.width(w);
			avatar.height(w);
			
			var nextAvatar = $("#ondeck_next_avatar");
			if(nextAvatar) {
				var ww = $("#ondeck_session").width() * 0.2;
				var nextLeft = $("#ondeck_next_tdleft");
				var nextRight = $("#ondeck_next_tdright");
				
				var table = $("#ondeck_session_split_next1");
				var nw = table.height() * 0.9;
				nw *= 0.9;
				if(nw > ww) nw = ww;
				
				nextLeft.width(nw);
				nextRight.width(table.width() - nw);
				
				nextAvatar.width(nw);
				nextAvatar.height(nw);
				//console.log("nextAvatar:"+nw);
				
				var nextAvatar2 = $("#ondeck_avatar_next2");
				if(nextAvatar2) {
					var nextLeft2 = $("#ondeck_next_tdleft2");
					var nextRight2 = $("#ondeck_next_tdright2");
					
					nextLeft2.width(nw);
					nextRight2.width(table.width() - nw);
					
					nextAvatar2.width(nw);
					nextAvatar2.height(nw);
					//console.log("nextAvatar2:"+nw);
				}
			}
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: autoReload
	//-----------------------------------------------------------------------------------------------------------------------------
	autoReload: function(target) {
		ps.ondeck.sessionTimeout = setTimeout(function() { 
			if(ps.ondeck.view == "session" && ps.ondeck.isLive == 1) {
				//console.log("autoReload: isLive:"+ps.ondeck.isLive);
				location.reload(); 
			}
		}, ps.ondeck.sessionRefresh);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: autoReloadStop
	//-----------------------------------------------------------------------------------------------------------------------------
	autoReloadStop: function(m) {
		if(ps.ondeck.sessionTimeout != null) clearTimeout(ps.ondeck.sessionTimeout);
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: autoRefresh
	//-----------------------------------------------------------------------------------------------------------------------------
	autoRefresh: function(m) {
		if(ps.ondeck.enableAutoRefresh && ps.ondeck.view != "users") {
			console.log("autoRefresh");
			ps.ondeck.autoRefreshTimer = setTimeout(function() {
				var url = ps.ondeck.url+"&ajax=refreshid&hostid="+ps.ondeck.hostid; 
				$.ajax({
					url: url,
					dataType: 'text', 
					error: function(xhr, status, error) {
						//console.trace();
						console.error(status+":"+error+" url:"+url);
						ps.ondeck.setStatus(status+": "+error, "danger");
						//window.alert(status+": "+error);
						//ps.ondeck.reload();
					},
					success: function(result) {
						if(result != ps.ondeck.refreshID) {
							//console.log("Refreshing:"+result);
							ps.ondeck.refreshID = result;
							ps.ondeck.refresh();
						}
						ps.ondeck.autoRefresh();
					}
				});
				//ps.ondeck.refresh();
			}, ps.ondeck.autoRefreshWait);
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: autoRefreshStop
	//-----------------------------------------------------------------------------------------------------------------------------
	autoRefreshStop: function(m) {
		if(ps.ondeck.autoRefreshTimer != null) clearTimeout(ps.ondeck.autoRefreshTimer);
		ps.ondeck.autoReloadStop();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: show
	//-----------------------------------------------------------------------------------------------------------------------------
	show: function(target) {
		$(target).removeClass("hide");
		$(target).addClass("show");
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: hide
	//-----------------------------------------------------------------------------------------------------------------------------
	hide: function(target) {
		$(target).removeClass("show");
		$(target).addClass("hide");
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: togglePresentationMode
	//-----------------------------------------------------------------------------------------------------------------------------
	togglePresentationMode: function() {
		ps.ondeck.isPresentationMode = !ps.ondeck.isPresentationMode;
		if(ps.ondeck.isPresentationMode) {
			ps.ondeck.presentationModeOn();
		}
		else {
			ps.ondeck.presentationModeOff();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: presentationModeOn
	//-----------------------------------------------------------------------------------------------------------------------------
	presentationModeOn: function() {
		ps.ondeck.isPresentationMode = true;
		$("#ondeck_table").addClass("presentation");
		$("#ondeck_title_container").height(0);
		$("#ondeck_session_to_list").hide();
		ps.ondeck.navClose();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: presentationModeOff
	//-----------------------------------------------------------------------------------------------------------------------------
	presentationModeOff: function() {
		ps.ondeck.isPresentationMode = false;
		$("#ondeck_table").removeClass("presentation");
		$("#ondeck_title_container").height('auto');
		$("#ondeck_session_to_list").show();
		ps.ondeck.navOpen();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setStatus
	//-----------------------------------------------------------------------------------------------------------------------------
	// type: danger, warning, success, info
	setStatus: function(error, type) {
		var status = $("#ondeck_status");
		status.removeClass("alert-danger");
		status.removeClass("alert-warning");
		status.removeClass("alert-success");
		status.removeClass("alert-info");
		
		if(!error || error == null || error == undefined) {
			status.hide();
		}
		else {
			status.show();
			status.addClass("alert-"+type);
			status.html(error);
		}
	},
		
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showSession
	//-----------------------------------------------------------------------------------------------------------------------------
	showSession: function() {
		//console.log("showSession");
		ps.ondeck.view = "session";
		ps.ondeck.show("#ondeck_session_container");
		ps.ondeck.show("#ondeck_nav_session");
		ps.ondeck.hide("#ondeck_list_container");
		ps.ondeck.hide("#ondeck_nav_list");
		ps.ondeck.hide("#ondeck_users_container");
		/*
		$("#ondeck_session_container").css("top", 1024);
		$("#ondeck_session_container").animate({
			top:0
		}, {
			duration: 300
		});
		*/
		ps.ondeck.autoReload();
		
		$("#ondeck_list_button").removeClass("selected");
		$("#ondeck_session_button").addClass("selected");
		$("#ondeck_users_button").removeClass("selected");

		if(ps.ondeck.autoCloseNav) ps.ondeck.navClose();
	},
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showList
	//-----------------------------------------------------------------------------------------------------------------------------
	showList: function(scrollToListNum) {
		//console.log("toggleList");
		clearTimeout(ps.ondeck.sessionTimeout);
		
		ps.ondeck.show("#ondeck_list_container");
		ps.ondeck.show("#ondeck_nav_list");
		
		ps.ondeck.scrollListToItem(scrollToListNum, false);
		ps.ondeck.scrollListToItem(scrollToListNum, true);

		//if(ps.ondeck.view == "session") {
		//	$("#ondeck_list_container").css("top", window.innerHeight);
		//	$("#ondeck_list_container").animate({ top:0 }, { duration: 300 });
		//}
		
		ps.ondeck.hide("#ondeck_session_container");
		ps.ondeck.hide("#ondeck_nav_session");
		ps.ondeck.hide("#ondeck_users_container");
		
		ps.ondeck.view = "list";
		$("#ondeck_list_button").addClass("selected");
		$("#ondeck_session_button").removeClass("selected");
		$("#ondeck_users_button").removeClass("selected");

		if(ps.ondeck.autoCloseNav) ps.ondeck.navClose();
	},
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showUsers
	//-----------------------------------------------------------------------------------------------------------------------------
	showUsers: function() {
		clearTimeout(ps.ondeck.sessionTimeout);
		//console.log("toggleList");
		ps.ondeck.view = "users";
		ps.ondeck.hide("#ondeck_session_container");
		ps.ondeck.hide("#ondeck_nav_session");
		ps.ondeck.hide("#ondeck_list_container");
		ps.ondeck.hide("#ondeck_nav_list");
		ps.ondeck.show("#ondeck_users_container");
		
		$("#ondeck_list_button").removeClass("selected");
		$("#ondeck_session_button").removeClass("selected");
		$("#ondeck_users_button").addClass("selected");

		if(ps.ondeck.autoCloseNav) ps.ondeck.navClose();
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: scrollListToItem
	//-----------------------------------------------------------------------------------------------------------------------------
	scrollListToItem: function(itemNum, mobile) {
		if(itemNum) {
			var m = "";
			if(mobile) m = "_mobile";
			var item = $("#listnum"+m+"_"+itemNum);
			if(typeof(item) != "undefined") {
				var scroll = 0;
				var container = $("#ondeck_list_scroll"+m);
				var containerTop = container.scrollTop(); 
				var containerBottom = containerTop + container.height(); 
				var pos = item.offset();
				if(pos != null) {
					//console.log("p:"+pos);
					var elemTop = pos.top;
					var elemBottom = elemTop + item.height(); 
					if(elemTop < containerTop) {
						scroll = elemTop;
					} 
					else 
					if(elemBottom > containerBottom) {
						scroll = elemBottom - $(container).height();
					}
					//console.log("listnum"+m+":"+itemNum+" scroll:"+scroll+" scrollTop:"+container.scrollTop());
					container.scrollTop(scroll);
					if(mobile) {
						ps.ondeck.listScrollMobile = scroll;
					}
					else {
						ps.ondeck.listScroll = scroll;
					}
				}
			}
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleList
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleList: function() {
		if(ps.ondeck.view == "list") {
			ps.ondeck.showSession();
		}
		else {
			ps.ondeck.showList();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleCountdown: function() {
		if(ps.ondeck.isPrimaryHost) {
			if(!ps.ondeck.countdownIsRunning) {
				ps.ondeck.resumeCountdown();
			}
			else {
				ps.ondeck.haltCountdown();
			}
			ps.ondeck.ajax("session", "cmd_update_timer", 1, "");
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: setupCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	setupCountdown: function() {
		//console.log("setupCountdown");
		
		ps.ondeck.countdownGoal = 0;
		ps.ondeck.countdownTime = 0;
		ps.ondeck.countdownIsRunning = false;
		if(ps.ondeck.countdownRefreshTimer != null) clearTimeout(ps.ondeck.countdownRefreshTimer);
		
		var countdown = $("#countdown");
		ps.ondeck.countdownCurrent = countdown.html();
		countdown.removeClass("warning");
		countdown.removeClass("danger");
		countdown.removeClass("on");

		ps.ondeck.countdownAutostart = $("#countdown_autostart").val();
		//console.log("auto:"+ps.ondeck.countdownAutostart);

		var limit = $("#countdown_limit");
		if(limit != undefined) {
			ps.ondeck.countdownLimit = limit.val();
			if(ps.ondeck.countdownLimit != undefined) {
				var p = ps.ondeck.countdownLimit.split(":");
				var minutes = parseInt(p[0]);
				var seconds = parseInt(p[1]);
				ps.ondeck.countdownGoal = seconds + minutes * 60;
				
				if(ps.ondeck.countdownGoal == 0) {
					//$("#countdown").hide();
					//$("#countdown_reset").hide();
				}
				else {
					//$("#countdown").show();
					//$("#countdown_reset").show();
		
					var progress = parseInt($("#countdownprogress").val());
					ps.ondeck.countdownTime = progress;
					
					var running = parseInt($("#countdown_running").val());
					//console.log("setup: countdownTime:"+ps.ondeck.countdownTime+" running:"+running);
					//console.log("countdownCurrent:"+ps.ondeck.countdownCurrent+" countdownLimit:"+ps.ondeck.countdownLimit);
					//console.log("isPrimaryHost:"+ps.ondeck.isPrimaryHost+" countdownAutostart:"+ps.ondeck.countdownAutostart);
			
					if(running > 0) {
						ps.ondeck.countdownIsReset = false;
						ps.ondeck.resumeCountdown();
					}
					else 
					if(ps.ondeck.isPrimaryHost && ps.ondeck.countdownAutostart == 1 && ps.ondeck.countdownCurrent == ps.ondeck.countdownLimit) {
						//console.log("autostart:"+ps.ondeck.countdownAutostart);
						ps.ondeck.resetCountdown();
						ps.ondeck.resumeCountdown();
					}
				}
			}
		}
		ps.ondeck.countdownCurrent = ps.ondeck.formatTime(ps.ondeck.countdownTime);
		countdown.html(ps.ondeck.countdownCurrent);

		ps.ondeck.showHideCountdown();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showHideCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	showHideCountdown: function() {
		if(ps.ondeck.countdownGoal == 0) {
			$("#countdown").hide();
			$("#countdown_reset").hide();
		}
		else {
			$("#countdown").show();
			$("#countdown_reset").show();
		}
		
		if(!ps.ondeck.isPrimaryHost) {
			$("#countdown_reset").hide();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: resetCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	resetCountdown: function() {
		console.log("resetCountdown");
		ps.ondeck.haltCountdown();
	
		var countdown = $("#countdown");
	
		countdown.removeClass("warning");
		countdown.removeClass("danger");
		countdown.removeClass("on");

		ps.ondeck.countdownHasFlashed = false;
		ps.ondeck.countdownIsReset = true;
		ps.ondeck.countdownLimit = $("#countdown_limit").val();
		ps.ondeck.countdownCurrent = ps.ondeck.countdownLimit;
		ps.ondeck.countdownTime = ps.ondeck.countdownGoal;
		countdown.html(ps.ondeck.countdownLimit);
		ps.ondeck.refreshCountdown();
		//console.log("resetCountdown:"+ps.ondeck.countdownTime);
		
		if(ps.ondeck.isPrimaryHost) {
			ps.ondeck.ajax("session", "cmd_update_timer", null, null);
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: resumeCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	resumeCountdown: function() {
		//if(ps.ondeck.isPrimaryHost) {
			//console.log("resumeCountdown");
			//console.trace();
			ps.ondeck.countdownIsStopped = false;
			ps.ondeck.countdownIsRunning = true;
			ps.ondeck.countdownHasFlashed = false;
			var countdown = $("#countdown");
			if(ps.ondeck.countdownIsReset) {
				ps.ondeck.countdownIsReset = false;
				ps.ondeck.countdownStartTime = new Date().getTime() / 1000;
			}
			else {
			// Resuming from current time
				ps.ondeck.countdownStartTime = (new Date().getTime() / 1000) - (ps.ondeck.countdownGoal - ps.ondeck.countdownTime);
			}
		//}		
		ps.ondeck.countdownRefreshTimer = setTimeout(ps.ondeck.refreshCountdown, 600);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: haltCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	haltCountdown: function() {
		console.log("haltCountdown");
		ps.ondeck.countdownIsStopped = true;
		ps.ondeck.countdownIsRunning = false;
		if(ps.ondeck.countdownRefreshTimer != null) clearTimeout(ps.ondeck.countdownRefreshTimer);
		ps.ondeck.refreshCountdown();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: startCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	startCountdown: function() {
		if(ps.ondeck.isPrimaryHost) {
			console.log("startCountdown");
			ps.ondeck.resumeCountdown();
		}		
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: stopCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	stopCountdown: function() {
		if(ps.ondeck.isPrimaryHost) {
			ps.ondeck.haltCountdown();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: refreshCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	refreshCountdown: function() {
		//console.log("refreshCountdown");
		var countdown = $("#countdown");
		if(!ps.ondeck.countdownIsRunning) {
			//console.log("refreshCountdown: not running");
			countdown.removeClass("warning");
			countdown.removeClass("danger");
			countdown.removeClass("on");
		}
		else
		if(ps.ondeck.countdownStartTime > 0) {
			var now = new Date().getTime() / 1000;
			var time = now - ps.ondeck.countdownStartTime;
			var rem = ps.ondeck.countdownGoal - time;
			ps.ondeck.countdownTime = rem;
			//console.log("refresh: countdownTime:"+ps.ondeck.countdownTime);
	
			//console.log("now:"+now+" start:"+ps.ondeck.countdownStartTime+" goal:"+ps.ondeck.countdownGoal);
			ps.ondeck.countdownCurrent = ps.ondeck.formatTime(rem);
			countdown.html(ps.ondeck.countdownCurrent);
			
			if(rem < 0) {
				countdown.removeClass("warning");
				countdown.addClass("danger");
				if(!ps.ondeck.countdownHasFlashed) {
					ps.ondeck.countdownHasFlashed = true;
					countdown.fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
				}
			}
			else
			if(rem < 60) {
				countdown.removeClass("danger");
				countdown.addClass("warning");
			}
			else {
				countdown.removeClass("warning");
				countdown.removeClass("danger");
			
				if(ps.ondeck.countdownIsRunning) {
					countdown.addClass("on");
				}
				else {
					countdown.removeClass("on");
				}
			}
			
			if(ps.ondeck.countdownIsRunning) ps.ondeck.countdownRefreshTimer = setTimeout(ps.ondeck.refreshCountdown, 990);
		}
			
		var entryID = $("#current_entry_id").val();
		if(entryID != undefined) {
			if(ps.ondeck.isPrimaryHost) {
				//console.log("ajax: countdownTime:"+ps.ondeck.countdownTime);
				var url = ps.ondeck.url+"&ajax=set_timer&set_timer="+ps.ondeck.countdownTime+"&set_timer_id="+entryID+"&set_timer_running="+(ps.ondeck.countdownIsRunning?1:0)+"&hostid="+ps.ondeck.hostid;
				//console.log(url);
				$.ajax({
					url: url,
					dataType: 'text', 
					error: function(xhr, status, error) {
						//console.trace();
						//console.error(error);
						ps.ondeck.setStatus(status+": "+error, "danger");
						//window.alert(status+": "+error);
						//ps.ondeck.reload();
					},
					success: function(result) {
						//console.log(result);
					}
				});
			}
		}
		else {
			ps.ondeck.countdownIsStopped = true;
			ps.ondeck.countdownIsRunning = false;
		}
	},
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleListEditMode
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleListEditMode: function() {
		ps.ondeck.listEditMode = !ps.ondeck.listEditMode;
		ps.ondeck.updateListEditMode();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateListEditMode
	//-----------------------------------------------------------------------------------------------------------------------------
	updateListEditMode: function() {
		if(ps.ondeck.listEditMode) {
			$(".ondeck_list_controls").removeClass("hidden");
			$(".ondeck_list_start_edit").removeClass("hidden");
			$(".ondeck_list_end_edit").removeClass("hidden");
			$(".ondeck_list_start").addClass("hidden");
			$(".ondeck_list_end").addClass("hidden");
			$(".ondeck_list_edit").addClass("isediting");
			
			$(".ondeck_list_start_input").change(function() {
				var val = $(this).attr('data-id')+"_"+$(this).val();
				console.log("edit start:"+val);
				ps.ondeck.ajax("list", "cmd_change_start_time", val, "");
			});
			
			$(".ondeck_list_end_input").change(function() {
				var val = $(this).attr('data-id')+"_"+$(this).val();
				console.log("edit end:"+val);
				ps.ondeck.ajax("list", "cmd_change_end_time", val, "");
			});
		}
		else {
			$(".ondeck_list_controls").addClass("hidden");
			$(".ondeck_list_start_edit").addClass("hidden");
			$(".ondeck_list_end_edit").addClass("hidden");
			$(".ondeck_list_start").removeClass("hidden");
			$(".ondeck_list_end").removeClass("hidden");
			$(".ondeck_list_edit").removeClass("isediting");
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: titleRefresh
	//-----------------------------------------------------------------------------------------------------------------------------
	titleRefresh: function(m) {
		if(ps.ondeck.enableAutoRefresh) {
			//console.log("titleRefresh");
			ps.ondeck.titleRefreshTimer = setTimeout(function() {
				ps.ondeck.title("refresh");
				ps.ondeck.titleRefresh();
			}, ps.ondeck.titleRefreshWait);
		}
	},

	//-----------------------------------------------------------------------------------------------------------------------------
	//:: titleRefreshStop
	//-----------------------------------------------------------------------------------------------------------------------------
	titleRefreshStop: function(m) {
		if(ps.ondeck.titleRefreshTimer != null) clearTimeout(ps.ondeck.titleRefreshTimer);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: reloadPage
	//-----------------------------------------------------------------------------------------------------------------------------
	reloadPage: function(region, cmd, value, elem) {
		var url = ps.ondeck.url+"&view="+region;
		console.log("reloadPage:"+url);
		
		value = ps.defaultValue(value, 1);
		
		//$(elem).hide();
		$(elem).css({"border-color": "rgb(251, 37, 66)", 
             "border-width":"3px", 
             "border-style":"solid"});
        $(elem).attr('onclick','').unbind('click');
        $(elem).html($(elem).html()+"<span id='loading_small' class='left'>&nbsp;</span>");
		
		var data = {
			hostid: ps.ondeck.hostid
		}
		data[cmd] = value;
		
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: 'text', 
			error: function(xhr, status, error) {
				console.trace();
				console.error(error);
				ps.ondeck.setStatus(status+": "+error, "danger");
				//window.alert(status+": "+error);
			},
			success: function(result) {
				console.log("success");
				//console.log(result);
				window.location.href = url;
			}
		});
		
		return false;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: ajax
	//-----------------------------------------------------------------------------------------------------------------------------
	ajax: function(region, cmd, value, more) {
		//console.log("ajax:"+region+":"+cmd);
		if(cmd == "refresh" && ps.ondeck.haltRefresh) {
			//console.log("haltRefresh");
			return;
		}
		if(cmd != "refresh") {
			if(ps.ondeck.isAjaxWaiting) {
				//console.log("isAjaxWaiting");
				return;
			}
			ps.ondeck.isAjaxWaiting = true;
		}
		
		value = ps.defaultValue(value, 1);
		more = ps.defaultValue(more, "");
		
       	if(cmd != "refresh") {
       		ps.ondeck.autoRefreshStop();
       		ps.showLoading();
       	}
       	
		var target = "#ondeck_"+region+"_container";		
		var url = ps.ondeck.url+"&ajax="+region+"&view="+ps.ondeck.view+"&"+cmd+"="+value+more+"&hostid="+ps.ondeck.hostid;
// 		if(cmd != "refresh") {
// 			if(ps.ondeck.lastAjaxUrl == url) {
// 				console.log("Cannot resend same request:"+url);
// 				return;
// 			}
// 			ps.ondeck.lastAjaxUrl = url;
// 		}
		
		$.ajax({
			url: url,
			dataType: 'text', 
			error: function(xhr, status, error) {
				console.log(url);
				console.error(error);
				ps.ondeck.setStatus(status+": "+error, "danger");
				//window.alert(status+": "+error);
			},
			success: function(result) {
				//console.log("result:"+result);
				$(target).html(result);
				//ps.ondeck.setStatus("ajax complete test", "success");
				
				if(region == "list") {
					$("#ondeck_list_scroll").scrollTop(ps.ondeck.listScroll);
					$("#ondeck_list_scroll_mobile").scrollTop(ps.ondeck.listScrollMobile);
				}
				if(region == "users") {
					$(".new_entry_button").prop("disabled", false);
					$("#ondeck_users").scrollTop(ps.ondeck.usersScroll);
					if(cmd == "cmd_add_user") {
						$("#custom_entry_popup").popup('hide');
					}
				}
				if(region == "session") {
					//console.log("cmd:"+cmd);
					ps.ondeck.showHideCountdown();
					if(cmd == "cmd_take_break" || cmd == "cmd_end_deck" || cmd == "cmd_close_deck" || cmd == "cmd_clear_deck") {
						ps.ondeck.haltCountdown();
					}
					else
					if(cmd == "cmd_next" || cmd == "cmd_start_deck" || cmd == "cmd_new_round" || cmd == "cmd_resume") {
						if(ps.ondeck.isPrimaryHost) {
							ps.ondeck.resetCountdown();
						}
					}
					if(cmd == "cmd_next" || cmd == "cmd_prev" || cmd == "cmd_skip" || cmd == "cmd_start_deck" || cmd == "cmd_new_round" || cmd == "cmd_resume") {
						if(ps.ondeck.countdownAutostart == 1) {
							//console.log("autostart:"+ps.ondeck.countdownAutostart);
							ps.ondeck.resumeCountdown();
						}
					}
					else {
						ps.ondeck.refreshCountdown();
					}
					
					if(cmd == "refresh") {
						ps.ondeck.setupCountdown();
					}
				}
				if(cmd != "refresh") {
					ps.ondeck.isAjaxWaiting = false;
       				ps.ondeck.autoRefresh();
					
					if(region != "title") ps.ondeck.title("refresh", 1);
					if(region != "list") ps.ondeck.list("refresh", 1);
					if(region != "session") ps.ondeck.session("refresh", 1);
					if(region != "nav") ps.ondeck.nav("refresh", 1);
					
		       		ps.hideLoading();
				}
				ps.ondeck.updateElementWidths();
			}
		});
	},
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: reload
	//-----------------------------------------------------------------------------------------------------------------------------
	reload: function() {
		window.location = ps.ondeck.url+"&view="+ps.ondeck.view;//+"&countdownprogress="+ps.ondeck.countdownTime;
	},
		
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: refresh
	//-----------------------------------------------------------------------------------------------------------------------------
	refresh: function() {
		ps.ondeck.title("refresh", 1);
		ps.ondeck.list("refresh", 1);
		ps.ondeck.users("refresh", 1);
		ps.ondeck.session("refresh", 1);
		ps.ondeck.nav("refresh", 1);
		ps.ondeck.filterUpdate();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: session
	//-----------------------------------------------------------------------------------------------------------------------------
	session: function(cmd, value) {
		ps.ondeck.ajax("session", cmd, value);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: list
	//-----------------------------------------------------------------------------------------------------------------------------
	list: function(cmd, value) {
		ps.ondeck.ajax("list", cmd, value);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: users
	//-----------------------------------------------------------------------------------------------------------------------------
	users: function(cmd, value) {
		ps.ondeck.ajax("users", cmd, value, "&filter_alpha="+ps.ondeck.filter_alpha+"&filter_region="+ps.ondeck.filter_region);
		//$("#custom_entry_popup").popup('hide');
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: title
	//-----------------------------------------------------------------------------------------------------------------------------
	title: function(cmd, value) {
		ps.ondeck.ajax("title", cmd, value);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: nav
	//-----------------------------------------------------------------------------------------------------------------------------
	nav: function(cmd, value) {
		ps.ondeck.ajax("nav", cmd, value);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: navClose
	//-----------------------------------------------------------------------------------------------------------------------------
	navClose: function() {
		//console.log("navClose");
		$("#ondeck_nav").addClass("slideoff");
		$("#ondeck_content").addClass("navcollapsed");
		$(".ondeck_nav_collapse").hide();
		$("#ondeck_nav_collapse").hide();
		$("#ondeck_nav_open").show();
		ps.ondeck.navIsOpen = false;
		ps.ondeck.updateElementWidths();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: navOpen
	//-----------------------------------------------------------------------------------------------------------------------------
	navOpen: function() {
		$("#ondeck_nav").removeClass("slideoff");
		$("#ondeck_content").removeClass("navcollapsed");
		$("#ondeck_nav_collapse").show();
		$(".ondeck_nav_collapse").show();
		$("#ondeck_nav_open").hide();
		ps.ondeck.navIsOpen = true;
		ps.ondeck.updateElementWidths();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: navToggle
	//-----------------------------------------------------------------------------------------------------------------------------
	navToggle: function() {
		if(ps.ondeck.navIsOpen) {
			ps.ondeck.navClose();
		}
		else {
			ps.ondeck.navOpen();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: updateElementWidths
	//-----------------------------------------------------------------------------------------------------------------------------
	updateElementWidths: function() {
		if(ps.ondeck.navIsOpen) {
			$(".ondeck_fixed_bottom_align").css("width", ($(window).width() - ($("#ondeck_nav").width() + ($("#ondeck_nav_container").width() * 0.5)))+"px");
			$(".ondeck_fixed_bottom_align").css("left", $("#ondeck_nav").width()+"px");
		}
		else {
			$(".ondeck_fixed_bottom_align").css("width", "100%");
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: confirmClearDeck
	//-----------------------------------------------------------------------------------------------------------------------------
	confirmClearDeck: function() {
		return confirm("Are you sure you want to clear all data in this session and reset everything? This cannot be undone!");
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: dismissStatus
	//-----------------------------------------------------------------------------------------------------------------------------
	dismissStatus: function() {
		ps.ondeck.statusHidden = true;
		$(".ondeck_user_status").hide();
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: verify
	//-----------------------------------------------------------------------------------------------------------------------------
	verify: function(cmd, value) {
		if(ps.ondeck.pin) {
			ps.ondeck.verify_cmd = cmd;
			ps.ondeck.verify_value = value;
			$('#admin_verify_pin').val("");
			$('#admin_verify_popup').popup('show');
		}
		else {
			if(ps.ondeck.view == "list") {
				ps.ondeck.list(cmd, value);
			}
			else
			if(ps.ondeck.view == "users") {
				ps.ondeck.users(cmd, value);
			}
			else {
				ps.ondeck.session(cmd, value);
			}
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: verifyRetry
	//-----------------------------------------------------------------------------------------------------------------------------
	verifyRetry: function() {
		//console.log("verifyRetry");
		$('#admin_verify_pin').val("");
		$('#admin_verify_popup').fadeOut(1000).fadeIn(100);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: enterPin
	//-----------------------------------------------------------------------------------------------------------------------------
	enterPin: function(num) {
		//console.log("enterPin:"+num);
		ps.ondeck.autoRefreshStop();
		$('#admin_verify_pin').val($('#admin_verify_pin').val()+""+num);

		var val = $('#admin_verify_pin').val();
		//console.log("enterPin:"+ps.ondeck.pin+" == "+val);
		
		var astring = ""+ps.ondeck.pin;
		var bstring = ""+val;
		//console.log("astring:"+astring.length);
		//console.log("bstring:"+bstring.length);
		
		if(ps.ondeck.pin == val) {
			$('#admin_verify_popup').popup('hide');
			if(ps.ondeck.view == "list") {
				ps.ondeck.list(ps.ondeck.verify_cmd, ps.ondeck.verify_value);
			}
			else
			if(ps.ondeck.view == "users") {
				ps.ondeck.users(ps.ondeck.verify_cmd, ps.ondeck.verify_value);
			}
			else {
				ps.ondeck.session(ps.ondeck.verify_cmd, ps.ondeck.verify_value);
			}
		}
		else 
		if(astring.length < bstring.length) {
			ps.ondeck.verifyRetry();
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: toggleMembersOnly
	//-----------------------------------------------------------------------------------------------------------------------------
	toggleMembersOnly: function() {
		var membersOnly = $("#ondeck_users_members_only");
		var toggle = $("#ondeck_users_members_only_toggle");
		if(membersOnly.val() == 0) {
			membersOnly.val(1);
			ps.ondeck.membersOnly = 1;
			toggle.addClass('active');
			toggle.html("<i class='fa fa-check-square-o' aria-hidden='true'></i> Members Only");	
		}
		else {
			membersOnly.val(0);
			ps.ondeck.membersOnly = 0;
			toggle.removeClass('active');
			toggle.html("<i class='fa fa-square-o' aria-hidden='true'></i> Members Only");
		}
		console.log("toggleMembersOnly:"+membersOnly.val());
		var users = $(".ondeck_user_add");
		if(users.length) {
			$.each(users, function(index, item) {
			});
		}
		ps.ondeck.filterUpdate();
		return false;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: filterClearSearch
	//-----------------------------------------------------------------------------------------------------------------------------
	filterClearSearch: function() {
		//console.log("filterClearSearch");
		
		$("#ondeck_user_search").val("")
		
		var find = $("#ondeck_user_search").val();
		$.ajax({
			url: ps.ondeck.url+"&ajax=member_search&member_search="+find,
			dataType: 'text', 
			success: function(result) {
				//console.log(result);
			}
		});
		
		ps.ondeck.filterUpdate();
		return true;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: filterSearch
	//-----------------------------------------------------------------------------------------------------------------------------
	filterSearch: function() {
		//console.log("filterSearch");
		
		var find = $("#ondeck_user_search").val();
		$.ajax({
			url: ps.ondeck.url+"&ajax=member_search&member_search="+find,
			dataType: 'text', 
			success: function(result) {
				//console.log(result);
			}
		});
		
		ps.ondeck.filterUpdate();
		return true;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: filterUpdate
	//-----------------------------------------------------------------------------------------------------------------------------
	filterUpdate: function() {
		//console.log("filterUpdate");
		var users = $(".ondeck_user_add");
		if(users.length) {
			var find = $("#ondeck_user_search").val();
			find = find.toLowerCase();
			$.each(users, function(index, item) {
				var show = true;
				var member = $(item).attr('data-member');
				if(ps.ondeck.membersOnly == 0 || member == 1) {
					show = true;
				}
				else {				
					show = false;
				}
				if(show && find.length) {
					var name = $(item).attr('data-name');
					if(name.toLowerCase().indexOf(find) != -1) {
						//console.log("match:"+name+" find:"+find);
						show = true;
					}
					else {
						show = false;
					}
				}
				
				if(show) {
					$(item).show();
				}
				else {
					$(item).hide();
				}
			});
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: filterAlpha
	//-----------------------------------------------------------------------------------------------------------------------------
	filterAlpha: function(alpha) {
		alpha = ps.defaultValue(alpha, "All");
		ps.ondeck.filter_alpha = alpha;
		ps.ondeck.users("filter_alpha", alpha);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: filterRegion
	//-----------------------------------------------------------------------------------------------------------------------------
	filterRegion: function(region) {
		region = ps.defaultValue(region, "All");
		ps.ondeck.filter_region = region;
		ps.ondeck.users("filter_region", region);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: validateRemoveUser
	//-----------------------------------------------------------------------------------------------------------------------------
	validateRemoveUser: function() {
		return confirm("Are you certain you want to remove this user from the list?");
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: validateDelete
	//-----------------------------------------------------------------------------------------------------------------------------
	validateDelete: function() {
		return confirm("Are you certain you want to entirely delete this session and its data?");
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: profile
	//-----------------------------------------------------------------------------------------------------------------------------
	profile: function(user_id) {
		ps.ondeck.autoRefreshStop();
		$("#user_profile_popup").popup('show');
		$("#user_profile_content").empty();
		$.ajax({
			url: ps.ondeck.url+"&ajax=profile&uid="+user_id,
			type:'POST',
			success: function(result) {
				$("#user_profile_content").html(result);
			}
		});
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: settings
	//-----------------------------------------------------------------------------------------------------------------------------
	settings: function() {
		//console.log("settings");
		
		ps.ondeck.autoRefreshStop();
		
		//$("#ondeck_settings_popup").popup();
		$("#ondeck_settings_popup").popup('show');

		$("#set_hostid").val(ps.ondeck.hostid);
		$("#set_countdown").focusout(function() {
			ps.ondeck.validateSettingsCountdown();
		});
		
		$("#ondeck_settings_form").submit(function(e) {
			e.preventDefault();
			ps.ondeck.validateSettings();
			ps.ondeck.reload();
		});
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: validateSettings
	//-----------------------------------------------------------------------------------------------------------------------------
	validateSettings: function() {
		ps.ondeck.autoRefreshStop();
		ps.ondeck.validateSettingsCountdown();
		ps.ondeck.countdownGoal = ps.ondeck.countdownGoalTemp;
		if(!ps.ondeck.countdownGoal || ps.ondeck.countdownGoal == "00:00") {
			$("#countdown").addClass('hide');
		}
		else {
			$("#countdown").removeClass('hide');
		}
		$("#settings_hostid").val(ps.ondeck.hostid);

		var form = $("#ondeck_settings_form");
		var url = form.attr('action');
		var data = form.serialize();
		console.log("validateSettings:"+url+" data:"+data);
		
		$.ajax({
			url: url, 
			type:'POST',
			data: data,
			success: function(result) {
				//ps.ondeck.refresh();
				
				if(result.indexOf("alert-danger") != -1) {
					$("#ondeck_settings_alert").html(result);
				}
				else
				if(result.indexOf("alert-warning") != -1) {
					$("#ondeck_settings_alert").html(result);
				}
				else {
				// Only hide the popup and continue of no error was reported on the server
					//$("#ondeck_settings_result").html(result);
					//$("#ondeck_settings_form_view").hide();
					//$("#ondeck_settings_result_view").show();
			
					setTimeout(function() {$("#ondeck_settings_popup").popup('hide')}, 2000);
				}
			
				ps.ondeck.isAjaxWaiting = false;
			}
		});
		return true;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: validateSettingsCountdown
	//-----------------------------------------------------------------------------------------------------------------------------
	validateSettingsCountdown: function() {
		//console.log("validateSettingsCountdown");
		var time = $("#set_countdown").val();
		ps.ondeck.countdownAutostart = $("#set_autostart").attr('checked') ? 1 : 0;
		//console.log("countdownAutostart:"+ps.ondeck.countdownAutostart);
		
		var seconds = 0;
		if(!time) {
			seconds = 0;
		}
		else
		if(time.indexOf(":") != -1) {
			var parts = time.split(":");
			seconds = parseInt(parts[0] * 60) + parseInt(parts[1]);
		}
		else {
			seconds = parseInt(time) * 60;
		}
		ps.ondeck.countdownGoalTemp = seconds;
		time = ps.ondeck.formatTime(seconds);
		$("#set_countdown").val(time);
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: formatTime
	//-----------------------------------------------------------------------------------------------------------------------------
	formatTime: function(totalSeconds) {
		//console.log("formatTime:"+totalSeconds);
		var abs = Math.abs(totalSeconds);
		var isNegative = totalSeconds < 0;
		var minutes = Math.floor(abs / 60);
		var seconds = Math.floor(abs - (minutes * 60));
	
		if(minutes == 0) minutes = "00";
		else
		if(minutes < 10) minutes = "0"+minutes;
	
		if(seconds == 0) seconds = "00";
		else
		if(seconds < 10) seconds = "0"+seconds;
	
		var prefix = "";
		if(isNegative) prefix = "-";
		var formatted = prefix+minutes+":"+seconds;
		return formatted;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: customEntry
	//-----------------------------------------------------------------------------------------------------------------------------
	customEntry: function() {
		//console.log("customEntry");
		ps.ondeck.autoRefreshStop();
		$("#ondeck_settings_popup").popup('hide');
		$("#custom_entry_popup").popup({
			focuselement:"#new_entry_name",
			onopen: function() {
				//console.log("customEntry:onopen");
				ps.ondeck.haltRefresh = true;
				$("#new_entry_name").val("");
				$("#new_entry_email").val("");
				$("#custom_entry_form_view").show();
				$("#custom_entry_result_view").hide();
				$("#presale_tickets_view").hide();
				$("#cmd_new_entry").prop("disabled", false);
			},
			onclose: function() {
				//console.log("customEntry:onclose");
				ps.ondeck.haltRefresh = false;
			}
		});
		$("#new_entry_email").change(function() {
			$(".new_entry_button").prop("disabled", false);
		});
		$("#new_entry_name").change(function() {
			$(".new_entry_button").prop("disabled", false);
		});
		
		$("#custom_entry_popup").popup('show');
		$("#custom_entry_result").html("");
		$("#deck_new_entry_alert").html("");
		
		$("#custom_entry_form").submit(function(e) {
			e.preventDefault();
			ps.ondeck.validateCustomEntry();
		});
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//: validateCustomEntry
	//-----------------------------------------------------------------------------------------------------------------------------
	validateCustomEntry: function() {
		//console.log("validateCustomEntry");
		ps.ondeck.autoRefreshStop();
		var pass = false;
		var name = $("#new_entry_name");
		var email = $("#new_entry_email");
		var form = $("#custom_entry_form");

		var error = null;
		if(typeof(name) == "undefined" || name.val() == null || name.val() == "") {
			error = "Please enter your artist name";
		}
		else
		if(typeof(email) == "undefined" || email.val() == null || email.val() == "") {
			error = "Please enter your email address";
		}
		if(error) {
			//console.log("error:"+error);
			$("#deck_new_entry_alert").html("<div class='alert alert-warning'>"+error+"</div>");
		}
		else {
			$(".new_entry_button").prop("disabled", true);
			if(ps.ondeck.isAjaxWaiting) {
				//console.log("isAjaxWaiting");
				return false;
			}
			ps.ondeck.isAjaxWaiting = true;
	
			//console.log("submitting");
			pass = true;
			$.ajax({
				url: form.attr('action'), 
				type:'POST',
				data: form.serialize(),
				success: function(result) {
					ps.ondeck.refresh();
					//console.log(result);
				
					if(result.indexOf("alert-danger") != -1) {
						$("#deck_new_entry_alert").html(result);
					}
					else
					if(result.indexOf("alert-warning") != -1) {
						$("#deck_new_entry_alert").html(result);
					}
					else {
					// Only hide the popup and continue of no error was reported on the server
						$("#custom_entry_result").html(result);
						$("#custom_entry_form_view").hide();
						$("#custom_entry_result_view").show();
				
						setTimeout(function() {$("#custom_entry_popup").popup('hide')}, 2000);
					}
				
					ps.ondeck.isAjaxWaiting = false;
				}
			});
		}
	
		return pass;
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: showPresaleTickets
	//-----------------------------------------------------------------------------------------------------------------------------
	showPresaleTickets: function() {
		//console.log("showPresaleTickets");
// 		if(ps.ondeck.isAjaxWaiting) {
// 			console.log("isAjaxWaiting");
// 			return false;
// 		}
// 		ps.ondeck.isAjaxWaiting = true;
		ps.ondeck.autoRefreshStop();
		if(ps.ondeck.url) {
			$("#ondeck_settings_popup").popup('hide');
			$("#presale_tickets_popup").popup({
				onopen: function() {
					console.log("on open");
					$.ajax({
						url: ps.ondeck.url+"&ajax=presales",
						type:'POST',
						error: function(xhr, status, error) {
							console.error(error);
							ps.ondeck.setStatus(status+": "+error, "danger");
						},
						success: function(result) {
							console.log("success");
							console.log(result);
							//ps.ondeck.refresh();
		
							if(result.indexOf("alert-danger") != -1) {
								$("#presale_tickets_content").html(result);
							}
							else
							if(result.indexOf("alert-warning") != -1) {
								$("#presale_tickets_content").html(result);
							}
							else {
								$("#presale_tickets_content").html(result);
								//setTimeout(function() {$("#custom_entry_popup").popup('hide')}, 2000);
							}
							//ps.ondeck.isAjaxWaiting = false;
						}
					})
				},
				onclose: function() {
					//console.log("customEntry:onclose");
					ps.ondeck.haltRefresh = false;
				}
			});
			$("#presale_tickets_popup").popup('show');
		}
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: confirmLogout
	//-----------------------------------------------------------------------------------------------------------------------------
	confirmLogout: function() {
		return confirm("Are you sure you want to log out?");
	},
	
	//-----------------------------------------------------------------------------------------------------------------------------
	//:: none
	//-----------------------------------------------------------------------------------------------------------------------------
	none: function() {}
};
