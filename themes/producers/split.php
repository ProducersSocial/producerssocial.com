<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$this->inc('elements/navbar.php');
?>
	
<div class='split_page'>
	<div class="white_page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
				<?php 
					$a = new Area('Main');
					$a->display($c);
				?>
				</div>
				<div class="col-md-4">
					<div id="pb_sidebar" class="well">
						<?php 
						$a = new Area('Sidebar');
						$a->display($c);
						?>
					 </div>
				</div>
	        </div>
        </div>
    </div>
</div>

<?php $this->inc('elements/footer.php'); ?>
