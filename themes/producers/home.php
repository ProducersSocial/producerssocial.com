<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 

$user = Loader::helper('user');
?>

<?php $this->inc('elements/navbar.php', array("collapse"=>true)); ?>
<div id='ps_home'>
	<div id='home_header' class="dark">
		<img id='home_header_img' src='<?php echo $this->getThemePath();?>/images/home_header.jpg'/>
		<img id='home_header_logo' src='<?php echo $this->getThemePath();?>/images/biglogo.png' alt='Welcome to the Producers Social' />
    	<div id="home_intro">
		<?php
			$a = new Area('Intro');
			$a->display($c);
		?>
		</div>
	</div>
		<?php
    		echo "<div id='home_testimonials'>";
    		echo "<div id='home_testimonials_inner'>";
			$tests = array();
			$tests[373] = array(
				'quote' => "Just drove over 200 miles round trip to play 4 songs and I couldn't be happier about it.",
				'name'	=> "zodiak_iller"
			);
			$tests[135] = array(
				'quote' => "So many doors have been opened to me since discovering the social, from collaborations and events to providing a sounding board and constructive criticism for my projects.",
				//Whether you are totally new to the music game or you are just looking to take your career to the next level, I would highly recommend you check out Producers Social!",
				'name'	=> "Josh Robin"
			);
			$tests[123] = array(
				'quote' => "My favorite thing about the Producers Social community is the friendships I've gained. Being a part of such a diverse community of producers has affected my work in such a positive way.",
				'name'	=> "P3AZY"
			);
			/*
			$tests[102] = array(
				'quote' => "Sit in window and stare ooo, a bird!",
				'name'	=> "Zimbu"
			);
			$tests[2] = array(
				'quote' => "Short ribs corned beef doner chuck strip steak tri-tip pork belly brisket drumstick shankle tenderloin kielbasa pancetta ham hock.",
				'name'	=> "Stephan Jacbos"
			);
			$tests[254] = array(
				'quote' => "Short ribs meatloaf sausage, burgdoggen tail bacon hamburger.",
				'name'	=> "AQUEOUSI"
			);
			$tests[19] = array(
				'quote' => "Meatloaf pig turducken doner drumstick spare ribs porchetta ribeye. Pastrami chicken t-bone pig.",
				'name'	=> "Da Moth"
			);
			$tests[86] = array(
				'quote' => "Pee in the shoe swat at dog chase dog then run away hopped up on catnip, or hide when guests come over.",
				'name'	=> "Lil Luna"
			);
			$tests[52] = array(
				'quote' => "Friends are not food cat snacks. Climb leg meowzer! ",
				'name'	=> "Asteroids and Earthquakes"
			);
			$tests[110] = array(
				'quote' => "Mew instantly break out into full speed gallop across the house for no reason, but eat a plant, kill a hand steal the warm chair right after you get up, pelt around the house and up and down stairs chasing phantoms sweet beast. ",
				'name'	=> "Dreamlyfe"
			);
			*/
			
			echo "<table id='testimonial_container'>";
			echo "<tr>";
			echo "<td id='testimonial_container_left'>";
    		echo "<a href='javascript:;' onclick='ps.testimonials.prev();'><i class='fa fa-angle-left huge' aria-hidden='true'></i></a>";
    		echo "</td>";
			echo "<td id='testimonial_container_center'>";
			
			$i = 1;
			$first = true; 
			foreach($tests as $id => $t) {
				$p = UserProfile::getByUserID($id);
				$img = $p->getAvatar(false, false, false);
				$link = "/members/profile?uid=".$id;
				$display = $first ? "" : "display:none;";
				echo "<table id='testimonial_".$i."' class='testimonial' style='".$display."'>";
				echo "<tr>";
				echo "<td class='testimonial_avatar'>";
				echo "<a href='".$link."'><img src='".$img."'></a>";
				echo "</td>";
				echo "<td class='testimonial_quote'>";
				echo "<q>".$t['quote']."</q><br>";
				//$p->uFullName.", ".$p->uArtistName
				echo "<a class='testimonial_sig' href='".$link."'>- ".$t['name']."</a>";
				//echo "<br class='clear'>";
				echo "</td>";
				echo "</tr>";
				echo "</table>";
				$first = false;
				$i++;
			}
    		echo "</td>";
			echo "<td id='testimonial_container_right'>";
    		echo "<a href='javascript:;' onclick='ps.testimonials.next();'><i class='fa fa-angle-right huge' aria-hidden='true'></i></a>";
    		echo "</td>";
			echo "</tr>";
    		echo "</table>";
			
			//echo "<div id='testimonial_buttons'>";
    		//echo "<a class='testimonial_button' href='javascript:;' onclick='ps.testimonials.next();'><i class='fa fa-angle-down' aria-hidden='true'></i></a>";
    		//echo "<a class='testimonial_button' href='javascript:;' onclick='ps.testimonials.prev();'><i class='fa fa-angle-up' aria-hidden='true'></i></a>";
    		//echo "<a class='right testimonial_button' href='javascript:;' onclick='ps.testimonials.hide();'><i class='fa fa-times small' aria-hidden='true'></i></a>";
    		//echo "</div>";
    		echo "</div>";
    		
    		//echo "<div id='home_testimonials_show' style='display:none'>";
    		//echo "<a class='right testimonial_button' href='javascript:;' onclick='ps.testimonials.show();'>Testimonials</a>";
    		//echo "</div>";
    		echo "</div>";
    	?>
		

	<?php
		global $u; 
		if(!$u->isLoggedIn() || $u->isSuperUser()) { 
	?>
	<div id='home_signup' class="parallax-window dark" data-parallax="scroll" data-speed="0.4" data-image-src="<?php echo $this->getThemePath()?>/images/scaffoldbg1.jpg">
    	<div class='edgefade_down'></div>
    	<div class="inner_page">
			<div class="col-lg-12">
				<?php
					$a = new Area('Signup');
					$a->display($c);
				?>
			</div>
		</div>
    	<div class='edgefade_up'></div>
	</div>
	<?php
		}
	?>
            
    <div id="home_events" class="row parallax-window" data-parallax="scroll" data-speed="0.2" data-image-src="<?php echo $this->getThemePath()?>/images/sketchbg.jpg">
    	<a class="anchor" id="home_events_anchor"></a>
    	<div class='edgefade_down'></div>
    	<div class="inner_page">
			<div class="col-lg-12">
			<?php
				$a = new Area('Events Heading');
				$a->display($c);
			?>
			</div>
			<?php
				$sponsor = false;
				if(!$sponsor) {
			?>
					<div class="col-md-12">
						<div id="home_events_list_wide">
						<?php
							$a = new Area('Upcoming Events');
							$a->display($c);
						?>
						</div>
						<div class='center'>
							<a href='/events'>More Events</a>
						</div>
					</div>
    		<?php
    			}
    			else {
    		?>
					<div class="col-md-6">
						<div id="home_events_list">
						<?php
							$a = new Area('Upcoming Events');
							$a->display($c);
						?>
						<div class='center'>
							<a href='/events'>More Events</a>
						</div>
						</div>
					</div>
					<div class="col-md-6">
						<div id="home_sponsor"> 
						<?php
							$a = new Area('Sponsor');
							$a->display($c);
						?>
						</div>
					</div>
			<?php
				}
			?>
		</div>
    	<div class='edgefade_up'></div>
	</div>

	<div id='home_coming_soon' class="row parallax-window dark" data-parallax="scroll" data-speed="0.4" data-image-src="<?php echo $this->getThemePath()?>/images/scaffoldbg1.jpg">
    	<div class='edgefade_down'></div>
    	<div class="inner_page">
			<div class="col-lg-12">
				<?php
					$a = new Area('ComingSoon');
					$a->display($c);
				?>
			</div>
		</div>
    	<div class='edgefade_up'></div>
	</div> 
            
    <div id="home_past_events" class="row parallax-window" data-parallax="scroll" data-speed="0.2" data-image-src="<?php echo $this->getThemePath()?>/images/sketchbg.jpg">
    	<div class='edgefade_down'></div>
    	<div class="inner_page">
			<div class="col-lg-12">
			<?php
				$a = new Area('News and Events');
				$a->display($c);
			?>
			</div>
		</div>
    	<div class='edgefade_up'></div>
	</div>
            
    <div id="home_sponors" class="dark">
    	<div class='edgefade_down'></div>
    	<div class="inner_page">
			<div class="col-lg-12">
			<?php
				$a = new Area('Sponsors and Supporters');
				$a->display($c);
			?>
			</div>
			<?php
				Loader::model("sponsor");
				$feature = Loader::helper('feature');
				echo $feature->displaySponsorScroll(Sponsor::getAll("status='active'", "ordernum DESC"));
			?>
		</div>
    	<div class='edgefade_up'></div>
	</div>
             
    <div id="home_lower_section" class="row parallax-window dark" data-parallax="scroll" data-speed="0.4" data-image-src="<?php echo $this->getThemePath()?>/images/scaffoldbg1.jpg">
    	<div class='edgefade_down'></div>
    	<div class="inner_page">
			<div class="col-lg-12">
			<?php
				$a = new Area('More Section');
				$a->display($c);
			?>
			</div>
			<div class="col-md-6">
				<div id="home_feed1_container">
				<?php
					$a = new Area('Feed 1');
					$a->display($c);
				?>
				</div>
				<br class='clear'>
			</div>
			<div class="col-md-6">
				<div id="home_feed2_container">
				<?php
					$a = new Area('Feed 2');
					$a->display($c);
				?>
				</div>
				<br class='clear'>
			</div>
			<br class='clear'>
		</div>
		<br class='clear'>
    	<div class='edgefade_up'></div>
	</div>
       
	<div class='clear'></div>
</div>	
<?php  $this->inc('elements/footer_home.php'); ?>
