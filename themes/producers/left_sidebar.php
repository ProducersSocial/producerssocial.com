<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
	
<div class='sidebar_page'>
    <div class="page_container">
		<div class="col-md-4">
			<div id="pb_sidebar" class="well">
				<?php 
				$a = new Area('Sidebar');
				$a->display($c);
				?>
			 </div>
		</div>
		<div class="col-md-8">
			<?php 
			$a = new Area('Main');
			$a->display($c);
			?>
		</div>
		<br class='clear'>
    </div>
</div>
	
<?php $this->inc('elements/footer.php'); ?>
