<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');
?>

<div class="sidebar_page">
	<div class="white_page">
		<div class="page_container">
			<div class="col-md-8">
			<?php 
				$a = new Area('Main');
				$a->display($c);
			?>
			</div>
			<div class="col-md-4">
				<div id="pb_sidebar">
					<?php 
					$a = new Area('Sidebar');
					$a->display($c);
					?>
				 </div>
			</div>
			<br class='clear'>
			
			<div class='page_inset'>
			<?php 
				$a = new Area('FullWidth');
				$a->display($c);
			?>
			</div>
		</div>
    </div>
</div>

<?php $this->inc('elements/footer.php'); ?>
