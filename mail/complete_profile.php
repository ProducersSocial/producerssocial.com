<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Complete Your Profile";

ob_start();
Loader::element('email_header');

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "Thank you for registering with the Producers Social! To get the most out of your account, please ";
echo "<a target='_blank' href='".BASE_URL."/profile/avatar'>upload a profile image</a> and <a target='_blank' href='".BASE_URL."/profile/edit'>fill out your details</a>. ";
echo "This is a great way to connect and share your talents with the community.";
echo "<br>";

echo "<div style='border:1px solid #DDD; border-radius:4px; padding:20px; text-align:center; margin-top:20px;'>";
echo "<a target='_blank' href='".BASE_URL."/profile/avatar'><img src='".BASE_URL."/themes/producers/images/image_upload.png'></a>";
echo "<div class='image_label'>Upload Your Profile Image</div>";
echo "</div>";

echo "<br><br>";
				
$events = Loader::helper('events');
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $profile->uLocation);
if($upcoming) {
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>Upcoming Events in ".Locations::getName($profile->uLocation)."</a></div>";
	echo $upcoming;
}


Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>