<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Membership Confirmation";

ob_start();
Loader::element('email_header');
$stripe = Loader::helper('stripe');
$planName = $stripe->planName($sub->plan);
$renewalDate = date("F j, Y", strtotime($sub->ending));
$accountUrl = BASE_URL."/profile/membership";

$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px;";

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "You are now an official member of the Producers Social! Your ".$planName." is now active and valid until ".$renewalDate.". To view your membership details, visit:<br>";
echo "<a href='".$accountUrl."' target='_blank'>".$accountUrl."</a>";
echo "<br>";
echo "<br>";
echo "<h2>Tickets</h2>";
echo "Your membership includes ".$sub->planTicketQuantity()." tickets which can be used at any regular Producers Social event. Please note that your tickets must be used within the time frame of your subscription before the expiration date. To RSVP to events online, visit the Events page.<br>";
echo "<br>";
echo "<a href='".BASE_URL."/events' target='_blank' style='".$buttonStyle."'>Find Events Now</a>";

echo "<br>";
echo "<br>";
echo "<br>";
echo "<h2>Member Benefits</h2>";
echo "With your membership you now have full access to exclusive member-only benefits! New content just for members is released on the first Friday of every month.<br>";
echo "<br>";
echo "<a href='".BASE_URL."/members' target='_blank' style='".$buttonStyle."'>Go to the Membership Portal</a>";

echo "<br>";
echo "<br>";
echo "<br>";
echo "<h2>Profile & Custom URL</h2>";
echo "Please take some time to set up your profile to showcase your work to the world! Members can include a featured track, social media links, and define a custom url.<br>";
echo "<br>";
echo "<a href='".BASE_URL."/profile/edit' target='_blank' style='".$buttonStyle."'>Edit Your Profile</a>";

echo "<br>";
echo "<br>";

echo "<br><br>";

$events = Loader::helper('events');
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $profile->uLocation);
if($upcoming) {
	//echo "<h2>Events Near You</h2>";
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>Upcoming Events in ".Locations::getName($profile->uLocation)."</a></div>";
	echo $upcoming;
}



Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>