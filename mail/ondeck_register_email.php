<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Activate Your Account";

ob_start();
Loader::element('email_header');

echo "Hi ".$username.",<br>";
echo "<br>";
echo "Thank you for coming out to the Producers Social! We trust you had a great experience and hope to see you again next time.\n";
echo "For faster sign-ups and to take advantage of other member benefits, please take a moment to verify and set up your free account:\n";

echo "<div style='border:1px solid #DDD; border-radius:4px; padding:20px; text-align:center; margin-top:20px;'>";
$url = BASE_URL.View::url('/login', 'v', $uHash);
echo "<div style='text-align:center; width:100%;'>";
echo "<br style='clear:both;'>";
echo "<a style='margin:auto;' target='_blank' href='".$url."'><img alt='Active your free membership' src='".BASE_URL."/mail/loginbutton.jpg'></a>";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";
echo "<a style='color:#5f10a3;' target='_blank' href='".$url."'>$url</a>";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";
echo "Your username is: <strong>".$username."</strong><br>";
echo "Your temporary password is: <strong>".$password."</strong>";
echo "</div>";
echo "</div>";

//$this->inc('elements/email_footer.php'); 
Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>