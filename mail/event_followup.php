<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Event Followup";

ob_start();
Loader::element('email_header');

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "Thank you for coming out to the Producers Social! Since you shared a tune, you have been included in our event recap blog post.";
echo "<br>";

echo "<div style='border:1px solid #DDD; border-radius:4px; padding:20px; text-align:center; margin-top:20px;'>";
$feature = Loader::helper('feature');
echo $feature->displayEmailPreview($feature_id);
echo "</div>";

echo "<br><br>";
				
Loader::model("locations");
$events = Loader::helper('events');
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $location);
if($upcoming) {
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>Get tickets now for more upcoming events in ".Locations::getName($eventLocation)."</a></div>";
	echo $upcoming; 
}



Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>