<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Tonight! Event Reminder";

ob_start();
Loader::element('email_header');

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "This is a quick reminder that tonight is the Producers Social in <a target='_blank' href='".$eventUrl."'>".$locationName."</a>!";
echo "<br>";

$tickets = Loader::helper('tickets');
$title = "<div style='font-size:20px; font-weight:bold'><a target='_blank' href='".$eventUrl."'>".$eventName." - ".$eventDate."</a></div>";
echo $tickets->getUserTicketStatusForEmail($user_id, $event_id, $title);

echo "<br><br>";
				
$events = Loader::helper('events');
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $eventLocation);
if($upcoming) {
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>More Upcoming Events in ".Locations::getName($eventLocation)."</a></div>";
	echo $upcoming;
}


Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>