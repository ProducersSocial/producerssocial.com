<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Subscription Cancelled";

ob_start();
Loader::element('email_header');
$stripe = Loader::helper('stripe');
$planName = $stripe->planName($sub->plan);
$renewalDate = date("F j, Y", strtotime($sub->ending));
$accountUrl = BASE_URL."/profile/membership";

$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px;";

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "This email is to confirm that autorenew has been turned off for your ".$planName.". Your membership will remain active until ".$renewalDate.". If you change your mind or want to view your subscription details, please visit your account page.<br>";
echo "<br>";
echo "<a href='".$accountUrl."' target='_blank' style='".$buttonStyle."'>View My Membership</a>";
echo "<br>";
echo "<br>";
echo "<br>";
echo "<em>Please note that any tickets remaining after your subscription period ends will also expire. Also, if you are using a custom url, it will no longer be valid once your membership expires.</em><br>";
echo "<br>";
echo "<br>";
echo "We'll be sorry to see you go! If you have any feedback to help us improve and provide greater value to our members, please contact us.";
echo "<br>";
echo "<br>";
echo "<a href='mailto:admin@producerssocial.com' target='_blank' style='".$buttonStyle."'>Send Feedback</a>";
echo "<br><br>";
/*
$events = Loader::helper('events');
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $profile->uLocation);
if($upcoming) {
	//echo "<h2>Events Near You</h2>";
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>Upcoming Events in ".Locations::getName($location)."</a></div>";
	echo $upcoming;
}
*/


Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>