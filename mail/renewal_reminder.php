<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Membership Confirmation";

ob_start();
Loader::element('email_header');
$stripe = Loader::helper('stripe');
$planName = $stripe->planName($sub->plan);
$endtime = strtotime($sub->ending);
$renewalDate = date("F j", $endtime);
$accountUrl = BASE_URL."/profile/membership";

$rem = $endtime - time();
$days = round($rem / (24 * 60 * 60));
if($days == 1) {
	$remaining = "tomorrow";
}
else
if($days >= 28) {
	$months = round($days / 29);
	if($months <= 1) {
		$remaining = "in 1 month";
	}
	else {
		$remaining = "in ".$months." months";
	}
}
else {
	$remaining = "in ".$days." days";
}


$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px;";

echo "Hi ".$memberName.",<br>";
echo "<br>";
if($sub->autorenew) {
	echo "Your ".$planName." is set to automatically renew on ".$renewalDate.", ".$remaining.".";
}
else {
	echo "Your ".$planName." will expire ".$renewalDate.", ".$remaining.". To continue enjoying your member benefits and to receive more tickets, please renew your subscription.";
	echo "<br>";
	echo "<br>";
	echo "<a target='_blank' href='".BASE_URL."/membership' style='".$buttonStyle."'>Renew Now</a>";
}
echo "<br>";
echo "<br>";
echo "To view or edit your subscription, please visit:<br>";
echo "<a href='".$accountUrl."' target='_blank'>".$accountUrl."</a>";
echo "<br>";
echo "<br>";
echo "<br>";

$events = Loader::helper('events');
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $profile->uLocation);
if($upcoming) {
	//echo "<h2>Events Near You</h2>";
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>Upcoming Events in ".Locations::getName($profile->uLocation)."</a></div>";
	echo $upcoming;
}



Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>