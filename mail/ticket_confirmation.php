<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Ticket Confirmation";

ob_start();
Loader::element('email_header');

$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px;";

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "This is confirmation that you have purchased <strong>".$numTickets." ticket".($numTickets == 1 ? "" : "s")."</strong> for:<br><br>";

Loader::model("facebook_event");
$event = FacebookEvent::getID($event_id);
$eventUrl = BASE_URL."/eventinfo?id=".$event_id;
$eventTime = strtotime($event->StartTime);
$eventDate = date("M j, Y", $eventTime)." at ".date("g:ia", $eventTime);

$tickets = Loader::helper('tickets');
$title = "<div style='font-size:20px; font-weight:bold'><a target='_blank' href='".$eventUrl."'>".$event->Name." - ".$eventDate."</a></div>";
echo $tickets->getUserTicketStatusForEmail($user_id, $event_id, $title);

//echo "<br>";
//echo "Please arrive early! Signups are first come first serve.<br>";
echo "<br>";
echo "<br>";
echo "<h2>Automatic Sign-Up</h2>";
echo "Members with an active subscription may also select the 'Automatic Sign-Up' checkbox when RSVPing to an event. ";
echo "This means you'll be added to the list automatically as soon as it is opened for any events you have reserved. ";
echo "RSVP spots are assigned in the order of reservation first come first serve.";
echo "<br>";
echo "<br>";
echo "<a href='".BASE_URL."/membership' style='".$buttonStyle."'>Become a Member Now</a>";

/*
echo "<h2>Important! List Signup</h2>";
echo "Having a ticket does not guarantee you will be able to present a tune until you have signed up on the list. Signups open at the starting time of the event. Check the <a target='_blank' href='".$eventUrl."'>event page</a> at the time of the event to sign-up remotely from your computer or mobile device, or sign-up in person at the event.<br>";
echo "<br>";
*/
echo "<br>";
//echo "<a href='".BASE_URL."/events' style='".$buttonStyle."'>View Events</a>";

echo "<br><br>";


Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>