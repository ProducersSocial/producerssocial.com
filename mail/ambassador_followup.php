<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Event Followup";

ob_start();
Loader::element('email_header');

$buttonStyle = "padding:10px 20px 10px 20px; background:#773ba5; color:white; font-size:18px; font-weight:bold; border-radius:4px; margin-bottom:20px;";

echo "Hi ".$memberName.",<br>";
echo "<br>";
echo "We hope you had a great social in ".$locationName." last night! Let's keep the momentum going by sharing a recap of the evening. ";
echo "Once published, all attendees on the list will be sent a follow up email with a link to the blog post. ";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";

echo "<div style='text-align:center;'>";
if(!$ondeck->BlogPost) {
	echo "<a style='".$buttonStyle."' target='_blank' href='".BASE_URL."/manage/features/edit?ondeck=".$ondeck->ID."'>Review & Publish Blog Post</a>";
}
else {
	Loader::model("member_feature");
	$blog = MemberFeature::getByID($ondeck->BlogPost);
	echo "<a style='".$buttonStyle."' target='_blank' href='".BASE_URL."/blog/".$blog->handle."'>Review Blog Post</a>";
}
echo "</div>";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";
echo "Please note: a default blog post will be automatically published after 72 hours if you have not done so already.";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";

//echo "<div style='border:1px solid #DDD; border-radius:4px; padding:20px; text-align:center; margin-top:20px;'>";
//echo "</div>";
$events = Loader::helper('events');
$url = "https://www.facebook.com/pg/ProducersSocial/events/";
$count = 0;
$upcoming = $events->upcomingEventsEmail($count, 3, "ASC", $location);

if(!$count) {
	echo "<div style='font-weight:bold; font-size:20px; font-style:italic; '>Warning! There are no events scheduled for ".$locationName."!</div>";
	echo "<br style='clear:both;'>";
	echo "<div style='font-size:18px;'>There are no future events scheduled for your location. ";
	echo "We kindly ask that you schedule your next event now and maintain your events scheduled at least 3 months in advance. ";
}
else {
	if($count < 3) {
		echo "<div style='font-weight:bold; font-size:20px;'>Please schedule your upcoming events</div>";
		echo "<br style='clear:both;'>";
		$phrase = $count == 1 ? "is only 1 event " : "are only ".$count." events ";
		echo "<div style='font-size:18px;'>There ".$phrase." on the calendar for ".$locationName.". ";
		echo "We kindly ask that you maintain your events scheduled at least 3 months in advance. ";
		echo "<div>";
	}
	else {
		echo "<div style='font-weight:bold; font-size:18px;'>Reminder to schedule your events</div>";
		echo "<br style='clear:both;'>";
		echo "<div style='font-size:18px;'>Please remember to keep your events scheduled for at least 3 months in advance.<div>";
	}
}
echo "<div style='text-align:center;'>";
echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";
echo "<a style='".$buttonStyle."' target='_blank' href='".$url."'>Schedule Events</a>";
echo "</div>";

echo "<br style='clear:both;'>";
echo "<br style='clear:both;'>";
echo "<div style='font-size:18px;'>If you need assistance from headquarters for anything, please don't hestitate to contact us: ";
echo "<a href='mailto:admin@producerssocial.com'>admin@producerssocial.com</a></div>";

echo "<br style='clear:both;'>";
echo "<br><br>";
				
if($upcoming) {
	echo "<div style='font-size:18px; color:black; font-weight:bold' target='_blank' href='http://producerssocial.com/events'>Your events currently scheduled for ".$locationName."</a></div>";
	echo $upcoming;
}

echo "</div>";

Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>