<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "User Reported: ".$user_id;

ob_start();
Loader::element('email_header');

echo "A user profile has been reported for inappropriate content. Please review the profile for anything offensive or for misuse of the site.<br>";
echo "<br>";
$url = BASE_URL."/manage/abuse";
echo "<a href='".$url."' target='_blank'>".$url."</a>";
echo "<br>";
echo "<br>";

Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>