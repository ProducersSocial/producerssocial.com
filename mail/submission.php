<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "New Submission: ".$item->name;

ob_start();
Loader::element('email_header');

echo "A new submission has been received.<br>";
echo "<br>";
$url = BASE_URL."/members/submissions/".$item->getHandle();
echo "<a href='".$url."' target='_blank'>".$url."</a>";
echo "<br>";
echo "<br>";
echo "Title: <b>".$sub->title."</b><br class='clear'>";
echo "Artist: <b>".$sub->artist."</b><br class='clear'>";
echo "Genre: <b>".$sub->genre."</b><br class='clear'>";
echo "Email: <b>".$sub->email."</b><br class='clear'>";
echo "File: <b><a target='_blank' href='".BASE_URL."/files/submissions/".$sub->submission_id."/".$sub->filename."'>".$sub->filename."</a></b><br class='clear'>";

echo "<br>";
echo "<br>";

Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>