<?php

defined('C5_EXECUTE') or die("Access Denied.");
if(!isset($subject) || !$subject) $subject = "Activate Your Account";

ob_start();
Loader::element('email_header');

echo "Hi ".$artistName.",<br>";
echo "<br>";
echo "Thank you for coming out to the Producers Social! We trust you had a great experience and we would love to see you again next month.\n";
echo "For faster sign-ups and to enjoy other membership benefits, please click the button below to create your free account:\n";
$url = BASE_URL.View::url('/register?email='.$email."&username=".$artistName);
echo "<div style='text-align:center; width:100%;'>";
echo "<br>";
echo "<a style='margin:auto;' target='_blank' href='".$url."'><img alt='Register your free membership' src='".BASE_URL."/mail/registerbutton.jpg'></a>";
echo "<br>";
echo "<a style='color:#5f10a3;' target='_blank' href='".$url."'>$url</a>";
echo "<br>";
echo "</div>";

//$this->inc('elements/email_footer.php'); 
Loader::element('email_footer', array('user_id'=>$user_id));
$bodyHTML = ob_get_contents();
ob_end_clean();

?>