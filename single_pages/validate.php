<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php
	ob_start();
	
	Loader::model('membership');
	Loader::model('user_membership');
	Loader::model('user_profile');
	$u = new User();
	
	$form = Loader::helper('form');
	
	$action = null;
	if(isset($_REQUEST['action'])) {
		$action = $_REQUEST['action'];
	}
	
	$error 		= null;
	$id 		= null;
	$m 			= null;
	$assigned 	= false;
	
	if(!isset($_REQUEST['id']) || !$_REQUEST['id']) {
		$error = "No membership id was provided.";
	}
	else {
		$id = $_REQUEST['id'];
		$m = Membership::getByID($id);
	}
	
	if($u->isSuperUser()) {
		if($m && $action == "assign") {
			$uid = null;
			if(isset($_REQUEST['uid'])) {
				$uid = $_REQUEST['uid'];
			}
			if(!$uid) {
				$error = "Please select a user to assign the membership";
			}
			else {
				$m->uID = $uid;
				$m->mDateAssigned = date("Y-m-d H:i:s", time());
				$m->mNotes .= $u->uName." assigned membership to ".$uid."\n";
				Membership::save($m);
			
				$um = UserMembership::getByUserID($uid);
				if(!$um) {
					$error = "Failed loading the membership for user ".$uid;
				}
				else {
				// Expiration is set at time of purchase
					//$exp = time() + (366 * 24 * 60 * 60);
					$um->mID = $id;
					$um->uStatus = "active";
					//$um->uExpirationDate = date("Y-m-d", $exp);
					UserMembership::save($um);
				}
			}
		}
	}
?> 
<div id="ccm-profile-wrapper">
	<div id="memberships_page">
	<?php
		if(!$error) {
			$name 			= null;
			$email			= null;
			$expiration 	= null;
			$user 			= null;
			$expired 		= false;
			$daysRemaining	= null;
			$status			= "";
			
			$um = UserMembership::getByMembershipID($id);
			if($um) {
				$expiration = $um->uExpirationDate;
				$ex = strtotime($expiration);
				if($ex < time()) {
					$expired = true;
					$um->uStatus = "expired";
					UserMembership::save($um);
				}
				else {
					$daysRemaining = round(($ex - time()) / (24 * 60 * 60));
				}
				$user = User::getByUserID($um->uID);
				if(!$user) {
					$error = "No user was found (".$um->uID.") for membership: ".$id;
				}
				else {
					$name = $user->uName;
					$email = $user->uEmail;
				}
				$status = $um->uStatus;
			}
			
			$location = UserMembership::$Locations[$m->mLocation];
		?>
			<fieldset id='membership_validate' class='membership_<?php echo $status;?>'>
				<h1>Status: <?php echo $status;?></h1>
				<h1>Location: <?php echo $location;?></h1>
				
				<?php
					if($expiration) {
						if($expired) {
							echo "<h2>Expired: $expiration</h2>";
						}
						else {
							echo "<h2>Days Remaining: $daysRemaining</h2>";
						}
					}
					if($user) {
						echo "<h2>Member: $name $email</h2>";
					}
					else {
						echo "<h2>No member has been associated with this account</h2>";
					}
				?>
				<br>
				<div class='membership_id'>Membership ID: <?php echo $id;?></div>
			</fieldset>
		<?php
		}
		
		if($error) {
			echo "<div class='error'>$error</div>";
		}
		
		if($m && $u->isSuperUser()) {
			if($assigned) {
				echo "<p>$assigned</p>";
			}
			else 
			if(!$m->uID) {
			?>
				<h3>Assign to User</h3>
				<?php
				$select = array();
				$memberships = UserMembership::getAll();
				if(!count($memberships)) {
					echo "<div class='error'>No user memberships were found</div>";
				}
				else {
					foreach($memberships as $mm) {
						$r = User::getByUserID($mm->uID);
						$select[$mm->uID] = $mm->uID.": ".$r->uName." (".$r->uEmail.")";
						//echo $select[$mm->uID]."<br>";
					}
		?>
					<div class="ccm-form ccm-ui">
					<form method="post" action="<?php echo $this->action('?action=assign')?>" id="profile-edit-form" enctype="multipart/form-data">
						<fieldset>
							<div class="ccm-profile-attribute">
								<?php echo $form->hidden('id', $id)?><br/>
								<?php echo $form->label('user', null)?><br/>
								<?php echo $form->select('uid', $select)?>
							</div>
							<br>
							<?php echo $form->submit('assign', t('Assign'), array('class'=>'btn btn-lg primary'))?>
						</fieldset>
					</form>
					</div>
		<?php
				}
			}
		}
		else {
		}
	?>
	</div>
</div>

<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>