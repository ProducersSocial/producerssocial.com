<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();

	Loader::model('facebook_event');
	Loader::model('user_profile');
	Loader::model('user_membership');
	Loader::model('user_purchase');

	$events = Loader::helper('events');
	$tickets = Loader::helper('tickets');
	
	$location = "HQ";
	$eventid = null;
	if(isset($_REQUEST['event'])) $eventid = $_REQUEST['event'];
	
	$event = null;
	if($eventid) {
		$event = FacebookEvent::getID($eventid);
		$location = $event->Location;
	}

	$u = new User();
	if(!$u || !$u->isLoggedIn()) {
		echo "<div class='alert alert-warning'>This is a restricted page.</div>";
		echo "<a class='button' href='".View::url("/login")."'>Login or Register</a>"; 
	}
	else {
		$p = UserProfile::getByUserID($u->uID);
		if($p && $p->isLocationAdmin()) {
		
			$tickets = UserPurchase::getTicketsForEvent($event->ID);
?>
		<div id="events_page">
			<div class="row">
				<div class="col-md-8">
				<?php
					//$count = count($tickets);
					$count = 0;//count($tickets);
					foreach($tickets as $t) {
						$count += UserPurchase::countTicketsForUser($t->uID, $event->ID);
					}
					if(!$count) {
						echo "<div class='alert alert-info'><h3>No tickets have been sold for this event yet</h3></div>";
					}
					else {
						$list = null;
						echo "<h1>".$count." Ticket".($count == 1 ? "" : "s")." Sold</h1>";
						$i = 1;
						foreach($tickets as $t) {
							$up = UserProfile::getByUserID($t->uID);
							$ui = UserInfo::getByID($t->uID);
							$name = $up->uArtistName;
							if(!$name) $name = $ui->getUserName();
							$listItem = $name." &lt;".$ui->getUserEmail()."&gt;";
							echo "(".$t->uQuantity.") ".$listItem."<br>";
							if($list) $list .= ", ";
							$list .= $listItem;
							$i++;
						}
					
						echo "<br>";
						echo "<h3>Email List</h3>";
						echo "<textarea class='full left' style='height:400px' onClick='this.select();'>";
						echo $list;
						echo "</textarea>";
					}
				?>
				</div>
				<div class="col-md-4">
					<div id="pb_sidebar" class="well">
						<?php
							echo "<h1>";
							if($event) {
								echo $event->Name;
							}
							else {
								echo "All Events";
							}
							echo "</h1>";
							if($event) {
								echo "<a class='button' href='".View::url("eventinfo?id=".$event->ID)."'>Return to Event</a>";
							}
							else {
								echo "<a class='button' href='".View::url("events")."'>Return to Events</a>";
							}
						?>
					 </div>
					 <?php
						//echo "<a class='button white' style='width:100%; margin-top:20px;' target='_blank' href='https://www.facebook.com/events/".$e->ID."'>Copy Email List</a>";
					 ?>
				</div>
			</div>
		</div>
<?php
		}
	}
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>