<?php 
defined('C5_EXECUTE') or die("Access Denied.");
ob_start();
Loader::library('authentication/open_id');
$form = Loader::helper('form'); 
?>
<div class='container-fluid'>
	<fieldset>
		<a name="forgot_password"></a>
		<form method="post" action="<?php echo $this->url('/login', 'forgot_password')?>" class="form-horizontal ccm-forgot-password-form">
		<legend><?php echo t('Forgot Your Password?')?></legend>
		<p><?php echo t("Enter your email address below. We will send you instructions to reset your password.")?></p>
		<input type="hidden" name="rcID" value="<?php echo $rcID?>" />
			<div class="control-group">
				<label for="uEmail" class="control-label"><?php echo t('Email Address')?></label>
				<div class="controls">
					<input type="text" name="uEmail" value="" class="ccm-input-text" >
				</div>
			</div>

			<br>
			<div class="ccm-ui"> 
				<?php echo $form->submit('submit', t('Send') . ' &gt;', array('class' => 'button pad-right'))?>
				<a class="left button white" href="<?php echo $this->url('/login')?>"><?php echo t('Cancel')?></a>
			</div>
		</form>
	</fieldset>
	<br><br>
	<div class="ccm-ui">
		<a class="button left white" href="mailto:admin@producerssocial.com?subject=Site%20Login%20Issue"><?php echo t('Contact Support')?></a>
	</div>
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>