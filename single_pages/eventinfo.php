<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();

	$events = Loader::helper('events');
	$tickets = Loader::helper('tickets');
	
	$u = new User();
	
	$user = Loader::helper('user');
?>
<div id="events_page">
	<div class="row">
		<div class="col-md-8">
		<?php
			Loader::model('facebook_event');
			Loader::model('user_profile');
			Loader::model('user_subscription');
			Loader::model('user_ticket');

			$e = null;
			$id = null;
			if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];

			if(!$id) {
				//header("Location: ".View::url("/events"));
				echo "NO ID";
			}
			else {
				if($e = FacebookEvent::getID($id)) {
					$img = str_replace("http:", "https:", $e->ImageURL);
					$meta = Loader::helper("meta");
					$meta->image 		= $img;
					$meta->title 		= $e->Name." ".date("M j, Y", strtotime($e->StartTime));
					$meta->description 	= substr($e->Description, 0, 128)."...";
					$meta->url 			= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					
					echo "<div id='eventinfo'>";
					echo "<img class='responsive' src='".$img."'>";
		
					$date = strtotime($e->StartTime);
					$end = null;
					if($e->EndTime != "0000-00-00 00:00:00") {
						$end = strtotime($e->EndTime);
					}
		
					echo "<div id='eventinfo_name_and_date'>";
						echo "<div id='eventinfo_date'>";
							echo "<div id='eventinfo_date_month'>".date("M", $date)."</div>";
							echo "<div id='eventinfo_date_day'>".date("j", $date)."</div>";
						echo "</div>";
		
						echo "<div id='eventinfo_name'>";
						echo $e->Name;
						echo "</div>";
					echo "</div>";
		
					echo "<div id='eventinfo_time_and_loc'>";
					echo "<div id='eventinfo_time'>";
					echo date("l", $date)." at ".date("g:ia", $date);
					if($end) echo " to ".date("g:ia", $end);
					echo $e->getCalendarLink(32);
// 					echo "<br class='clear'>";
					echo "</div>";
		
					echo "<div id='eventinfo_location'>";
					echo "<i class='fa fa-map-o' aria-hidden='true'></i>&nbsp; ";
					echo $e->AddressName." – ";
					if($e->AddressStreet) {
						echo $e->AddressStreet." ";
					}
					if($e->AddressCity) {
						echo ", ".$e->AddressCity;
					}
					if($e->AddressZip) {
						echo " ".$e->AddressZip;
					}
					$map = "https://maps.google.com/maps/place/";
					if(!$e->AddressStreet) {
						$map .= $e->AddressName;
					}
					else {
						$map .= $e->AddressStreet."+".$e->AddressCity."+".$e->AddressZip;
					}
					$map = str_replace(" ", "+", $map);
					
					echo "<a class='button right small white' target='_blank' href='".$map."'>Get Directions</a>";
					echo "</div>";
					echo "</div>";
					
					echo "<div id='eventinfo_status_alt' class='visible-xs'>";
					$events->eventStatus($e);
					echo "</div>";

					echo "<div id='eventinfo_buttons' class='full visible-xs pad-bottom'>";
					echo "<a class='button white right' href='javascript:;' onclick=\"$('#eventinfo_description').hide();\">Tickets</a>";
					echo "<a class='button white' href='javascript:;' onclick=\"$('#eventinfo_description').show();\">Event Info</a>";
					echo "</div>";

					echo "<div id='eventinfo_description'>";
					echo str_replace("\n", "<br>", $e->Description);
					echo "</div>";
					
		
					echo "</div>";
				}
				else {
					echo "<div class='alert alert-danger'><h1>No Event Found</h1>The specified event (".$id.") does not exist</div>";
				}
				$eventDisplayed = true;
			}
		?>
		</div>
		<div class="col-md-4">
			<div id="pb_sidebar" class="well">
				<?php
				
					if($e) {
						echo "<div id='eventinfo_status_alt' class='hidden-xs'>";
						$events->eventStatus($e);
						echo "</div>";
						$tickets->buyTickets($e);
					}
					$sub = UserSubscription::getByUserID($u->uID);
					if($sub && $sub->isActive()) {
						echo "<a class='button white' style='width:100%; margin-top:20px;' href='/profile/tickets'><i class='fa fa-ticket' aria-hidden='true'></i> Manage My Tickets</a>";
					}
					echo "<a class='button white' style='width:100%; margin-top:20px;' target='_blank' href='https://www.facebook.com/events/".$e->ID."'><i class='fa fa-facebook-official' aria-hidden='true'></i>&nbsp;View on Facebook</a>";
				?>
			 </div>
			 <?php
			 	/*
				if($user->isAdmin()) {	
					echo "<div class='admin_area center'>";
					if($user->isLocationAdmin($event->Location)) {
						echo "<a class='button white clear' style='width:100%;' href='".View::url("ticketsales?event=".$event->ID)."'>View Ticket Sales</a>";
					}
					if($user->isSuperAdmin()) {
						echo "<a class='button white clear' style='width:100%;' href='javascript:;' onclick='return ps.confirm(\"Are you sure you want to clear all data and fully reset the deck? This cannot be undone!\");'>Reset On Deck</a>";
					}
					echo "</div>";
				}
				*/
				
				$moreEvents = $events->getEvents(null, null, "LIMIT 3", "ASC", null, $e->Location);
				if($moreEvents) {
					$more = array();
					$remove = -1;
					for($x = 0; $x < count($moreEvents); $x++) {
						if($moreEvents[$x]->ID != $e->ID) {
							$more[] = $moreEvents[$x];
						}
					}
					if(count($more)) {
						echo "<div class='alert alert-none left' style='margin-top:20px;'>";
						echo $events->listEvents($more, true, "<h4 class='center'>More Upcoming Events</h4>");
						echo "</div>";
					}
				}
				echo "<a href='/events' class='block center'>All Events...</a>";
			 ?>
		</div>
	</div>
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>