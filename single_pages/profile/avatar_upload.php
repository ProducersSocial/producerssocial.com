<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
 	$form = Loader::helper(form);
?>
<div id="profile_container">
	<h2>Upload Avatar</h2>
	<p>Please upload a JPEG or PNG file that is at least 500x500 pixels.</p>
	<div id="avatar_container">
		<div class='clear' id='profile_image_saved'></div>
		<div class="image-editor">
			<button type="submit" class="button right" id="submit" name="submit" value="Save" onclick='return ps.profile.saveAvatar();'>Save</button>
			<input type="file" id="file" class="cropit-image-input left">
			<br class='clear'>
			<div class="cropit-preview"></div>
			<a id="rotate-ccw" class="rotate-ccw" href='javascript:;'><i class="fa fa-undo" aria-hidden="true"></i></a>
			<a id="rotate-cw" class="rotate-cw" href='javascript:;'><i class="fa fa-repeat" aria-hidden="true"></i></a>
			<a id="zoom-in" class="zoom-in" href='javascript:;'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
			<a id="zoom-out" class="zoom-out" href='javascript:;'><i class="fa fa-minus-square" aria-hidden="true"></i></a>
			<br class='clear'>
	
			<form id="profile_image_form" action="<?php print $this->action("saveImage");?>" method="post" enctype="multipart/form-data">
				<?php 
					echo $form->hidden("img"); 
					echo $form->hidden("uid", $user->id);
				?> 
				<?php 
					//echo $form->submit("submit", "Save", array('class'=>"button"));
				?>
				<button class='button white left' onclick='return ps.profile.hideAvatarUpload();'>Cancel</button>
				<button type="submit" class="button right" id="submit" name="submit" value="Save" onclick='return ps.profile.saveAvatar();'>Save</button>
			</form>
		</div>
	</div>
 					
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
