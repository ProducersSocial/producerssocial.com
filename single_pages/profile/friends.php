<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();

	Loader::model('user_profile');
	Loader::model('user_subscription');
	Loader::model('locations');
		
	$u 		= new User();
	$p 		= UserProfile::getByUserID($profile->uID);
	$sub 	= UserSubscription::getByUserID($u->uID);	
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('profile'=> $profile, 'u'=> $u, 'p'=>$p, 'sub'=>$sub, 'this'=>$this)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('profile'=> $profile, 'u'=> $u, 'p'=>$p, 'sub'=>$sub, 'this'=>$this)); 
			?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
		
				<div id='profile_content' class='row-fluid nopadding'>
					<h2><?php echo t('My Friends') ?></h2>
					<?php
					$friendsData = UsersFriends::getUsersFriendsData( $profile->getUserID() );
					if (!$friendsData) { ?>
						<div style="padding:16px 0px;">
							<?php echo t('You have not added any friends yet.')?>
						</div>
					<?php
					}
					else {
						$dh = Loader::helper('date');
						/* @var $dh DateHelper */
						foreach($friendsData as $friendsData) {
							$friendUID = $friendsData['friendUID'];
							$friendUI = UserInfo::getById( $friendUID );
							if (!is_object($friendUI)) { ?>
								<div class="ccm-users-friend" style="margin-bottom:16px;">
									<div style="float:left; width:100px;">
										<?php echo $av->outputNoAvatar()?>
									</div>
									<div >
										<?php echo t('Unknown User')?>
									</div>
									<div class="ccm-spacer"></div>
								</div>
							<?php }
							else { ?>
								<div class="ccm-users-friend" style="margin-bottom:16px;">
									<div style="float:left; width:100px;">
										<a href="<?php echo View::url('/profile',$friendUID)?>"><?php 
											//echo $av->outputUserAvatar($friendUI)
											$p = UserProfile::getByUserID($friendUID);
											if($p) {
												echo $p->getAvatar();
											}
										?></a>
									</div>
									<div >
										<a href="<?php echo View::url('/profile',$friendUID) ?>"><?php echo $friendUI->getUsername(); ?></a>
										<div style=" font-size:90%; line-height:90%; margin-top:4px;">
										<?php echo t('Member Since %s') ?> <?php echo $dh->formatDate($friendUI->getUserDateAdded(), true)?>
										</div>
									</div>
									<div class="ccm-spacer"></div>
								</div>
							<?php }
						}
					}
					?>
				</div>
			</td>
		</tr>
	</table>
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
