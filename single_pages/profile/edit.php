<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	$form = Loader::helper('form');
	
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
			?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
		
				<div id='profile_content' class='row-fluid nopadding'>
					<?php  if (isset($error) && $error->has()) {
						$error->output();
					} else if (isset($message)) { ?>
						<div class="message"><?php echo $message?></div>
						<script type="text/javascript">
						$(function() {
							$("div.message").show('highlight', {}, 500);
						});
						</script>
					<?php  } ?> 
	
					<h1 class="profilin">
					<?php 
						echo t('Edit Profile');
					?></h1>
					
					<?php
						if(isset($_REQUEST['invalid'])) {
							echo "<div class='alert alert-warning' style='max-width:600px;'>";
							if($_REQUEST['invalid'] == "handle") {
								echo "<h1 class='center'><i class='fa fa-warning' aria-hidden='true'></i></h1>";
								echo "The custom url handle you entered '".$_REQUEST['handle']."' is in use or invalid. Please select a different one.";
								$user->profile->uHandle = $_REQUEST['handle'];
							}
							echo "</div>";
						}
					?>
					
					<div class="ccm-form ccm-ui">
						<form method="post" action="/profile/edit" id="profile-edit-form" enctype="multipart/form-data">
						<?php
							echo $form->hidden('uid', $user->id);
							//$valt->output('profile_edit');
							//$attribs = UserAttributeKey::getEditableInProfileList(); 
							//if(is_array($attribs) && count($attribs)) { 
						?>
							<fieldset>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uArtistName', t('Artist Name'))?> <span class="ccm-required">*</span><br/>
								<p class='small'>What is your moniker or name you publish music under?</p>
								<?php echo $form->text('uArtistName',$user->profile ? $user->profile->uArtistName : "")?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uFullName', t('Your Real Name'))?> <span class="ccm-required">(optional)</span><br/>
								<?php echo $form->text('uFullName',$user->profile ? $user->profile->uFullName : "")?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uLocation', t('Location'))?><p class='small'>Select your primary location</p>
								<?php echo $form->select('uLocation', Locations::getSelectList(1), $user->profile->uLocation)?>
							</div>
							<?php
							if($user->isMember()) {
							?>
								<div class='alert alert-white left'>
								<label>Members-Only</label>
								
								<div class="ccm-profile-attribute">
									<?php echo $form->label('uHandle', t('Custom URL'))?><p class='small'>Customize your profile url. Only valid while membership is active.</p>
									<?php 
										echo $form->text('uHandle', $user->profile ? $user->profile->uHandle : "", array('autocomplete' => "off"));
										echo $form->hidden('handle_valid', 1);
									?>
									<p class='small'><?php echo BASE_URL."/<span id='handle_preview'>".($user->profile ? $user->profile->uHandle : "");?></span>
									<span id='handle_validate'></span></p>
								</div>
								<div class="ccm-profile-attribute">
									<?php echo $form->label('uFeaturedTrack', t('Featured Track or Album'))?><br/>
									<p class='small'>Paste the embed code for a SoundCloud track or Bandcamp album.</p>
									<?php echo $form->textarea('uFeaturedTrack',$user->profile ? $user->profile->uFeaturedTrack : "")?>
								</div>
								
								<label>Social Links</label>
								<p class='small'>Complete each of the following social urls, or leave them blank.</p>
								<table id='edit_social_links'>
									<tr>
										<td><div class='icon social-email-32 left'>&nbsp;</div>Contact Email: <?php echo $form->text('uEmail',$user->profile ? $user->profile->uEmail : "", array('class'=>'right'))?></td>
									</tr>
									<tr>
										<td><div class='icon social-bandcamp-32 left'>&nbsp;</div>http://<?php echo $form->text('uBandcamp',$user->profile ? $user->profile->uBandcamp : "", array('class'=>'right'))?>bandcamp.com</td>
									</tr>
									<tr>
										<td><div class='icon social-soundcloud-32 left'>&nbsp;</div>http://soundcloud.com/<?php echo $form->text('uSoundcloud',$user->profile ? $user->profile->uSoundcloud : "", array('class'=>'right'))?></td>
									</tr>
									<tr>
										<td><div class='icon social-facebook-32 left'>&nbsp;</div>http://facebook.com/<?php echo $form->text('uFacebook',$user->profile ? $user->profile->uFacebook : "", array('class'=>'right'))?></td>
									</tr>
									<tr>
										<td><div class='icon social-twitter-32 left'>&nbsp;</div>http://twitter.com/<?php echo $form->text('uTwitter',$user->profile ? $user->profile->uTwitter : "", array('class'=>'right'))?></td>
									</tr>
									<tr>
										<td><div class='icon social-instagram-32 left'>&nbsp;</div>http://instagram.com/<?php echo $form->text('uInstagram',$user->profile ? $user->profile->uInstagram : "", array('class'=>'right'))?></td>
									</tr>
									<tr>
										<td><div class='icon social-youtube-32 left'>&nbsp;</div>http://youtube.com/<?php echo $form->text('uYoutube',$user->profile ? $user->profile->uYoutube : "", array('class'=>'right'))?></td>
									</tr>
									<tr>
										<td><div class='icon social-sesh-32 left'>&nbsp;</div>http://sesh.io/<?php echo $form->text('uSesh',$user->profile ? $user->profile->uSesh : "", array('class'=>'right'))?></td>
									</tr>
								</table>
								</div>
							<?php
							}
							else {
							?>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uSoundcloud', t('Soundcloud'))?><p class='small'>Enter your soundcloud handle name</p>
								<?php echo $form->text('uSoundcloud',$user->profile ? $user->profile->uSoundcloud : "")?>
							</div>
							<?php
							}
							?>
							<br class='clear'>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uSummary', t('Summary'))?><br/>
								<p class='small'>Enter a brief description to appear in the members directory (Max: 255 characters).</p>
								<?php echo $form->textarea('uSummary',$user->profile ? $user->profile->uSummary : "", array('maxlength'=>255))?>
							</div>
						<?php
							//if(!ADMIN) {
						?>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uBio', t('Bio'))?><br/>
								<?php echo $form->textarea('uBio',$user->profile ? $user->profile->uBio : "", array('class'=>'tinymce'))?>
							</div>
							<?php
							//}
							?>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uStyleTags', t('Music Style Tags'))?><br/>
								<p class='small'>Enter music genre and style tags separated by comma</p>
								<?php echo $form->text('uStyleTags',$user->profile ? $user->profile->uStyleTags : "")?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uWebsite', t('Website URL'))?><br/>
								<?php echo $form->text('uWebsite',$user->profile ? $user->profile->uWebsite : "")?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uHide', t('Keep profile private'))?><br/>
								<p class='small'>Only you will be able to view your profile if the box is checked.</p>
								<?php echo $form->checkbox('uHide', 1, $user->profile ? $user->profile->uHide : "")?>
							</div>							
							<br>
						 <?php echo $form->submit('save', t('Save Profile'), array('class'=>'button right'))?>
						 <br>
							  </fieldset>
						</form>
						<div class="spacer">&nbsp;</div>
						<div class='small gray'><?php echo "User ID:".$user->id;?></div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
<?php
	// PULL RESERVED WORDS FROM PAGE NAMES
	$showReserved = false;
	if($showReserved) {
		$db = Loader::db();
		
		$pages = array();
		$q = "SELECT cFilename FROM  `Pages` WHERE 1";
		if($r = $db->query($q)) {
			while($row = $r->FetchRow()) {
				$pages[] = $row['cFilename'];
			}
		}
		
		$names = array();
		foreach($pages as $p) {
			$parts = explode("/", substr($p, 1));
			$name = $parts[0];
			$name = trim(str_replace(".php", "", $name));
			if($name) {
				$names[$name] = $name;
			}
		}
		sort($names);
		echo "<div class='alert alert-debug left'>";
		echo "<h2>RESERVED WORDS:</h2>";
		//echo "$"."reserved = array(<br>";
		echo "reservedHandles: [<br>";
		foreach($names as $k => $v) {
			//echo $v."<br>";	
			//echo "'".$v."' => '".$v."',<br>";	
			echo "'".$v."',<br>";	
		}
		//echo ");<br>";
		echo "],<br>";
		echo "</div>";
	}

	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
