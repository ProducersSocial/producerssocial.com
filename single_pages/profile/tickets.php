<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::model('user_ticket');
	Loader::model('facebook_event');
	Loader::model('on_deck');
	Loader::model('user_on_deck');
	
	$dateformat = Loader::helper('date');
	$ticketsHelper = Loader::helper('tickets');

	$form = Loader::helper('form');
	Loader::model('social_login', 'social_login');
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
			?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
		
				<div id='profile_content' class='row-fluid nopadding'>
					<?php  if (isset($error) && $error->has()) {
						$error->output();
					} else if (isset($message)) { ?>
						<div class="message"><?php echo $message?></div>
						<script type="text/javascript">
						$(function() {
							$("div.message").show('highlight', {}, 500);
						});
						</script>
					<?php  } ?>
<?php 
	if($user->isSuperAdmin() && !$user->isProfileOwner()) {
		echo "<div class='admin_area'>";
		echo "<a class='button medium white left pad-right' onclick='return ps.confirm(\"Are you sure you want to give this user 1 ticket?\");' href='/members/tickets?uid=".$user->id."&give=1'>Give 1 Ticket</a> &nbsp;";
		echo "<a class='button medium white left pad-right' onclick='return ps.confirm(\"Are you sure you want to give this user 10 tickets?\");' href='/members/tickets?uid=".$user->id."&give=10'>Give 10 Tickets</a> &nbsp;";
		echo "<a class='button medium white left pad-right' onclick='return ps.confirm(\"Are you sure you want to remove all this user\'s tickets?\");' href='/members/tickets?uid=".$user->id."&remove=1'>Remove All</a>";
		echo "</div>";
	}
	?>
	<div class="profilin clear">
	<?php 				
		$viewall = isset($_REQUEST['viewall']);
		$tickets = $ticketsHelper->getUserTicketsArray($user->id);
		if(!$tickets['count']) {
			$a = new Area("NoTickets");
			$a->display($c);
			/*
			echo "<h1>You don't have any tickets...</h1>";
			echo "<p>There are no tickets currently associated with your account. Tickets can be purchased ";
			echo "individually for each <a href='/events'>event</a>, or you can pre-purchase tickets at a discounted rate by ";
			echo "subscribing with a <a href='/membership'>Producers Social Membership</a>.<br><br>";
			echo "<a class='button' href='/membership'>Learn More...</a>";
			*/
		}
		else {							
			$limit = 0;
			if($tickets['count'] > 7) {
				$limit = 5;
				if($viewall) {
					$limit = 0;
				}
			}
			//$countAvailable = $tickets['count'] - $countUsed - $countRsvp - $countLive;
		
			echo "<div id='profile_tickets_heading'><strong>Tickets</strong> &nbsp;";
			echo " <span class='color'><i class='fa fa-ticket' aria-hidden='true'></i> <strong>".$tickets['unusedCount']." Unused &nbsp;</strong></span>";
			echo " <span class='color'><i class='fa fa-ticket' aria-hidden='true'></i> <strong>".($tickets['liveCount'] + $tickets['rsvpCount'])." Reserved &nbsp;</strong></span>";
			
			if($tickets['usedCount']) {
				echo " <span class='profile_ticket_expires'><i class='fa fa-times' aria-hidden='true'></i> ".$tickets['usedCount']." Used or Expired</span>";
			}
			echo "</div>";
			
			echo "<div id='profile_tickets_list'>";
			
			if($tickets['live']) {
				echo "<div class='alert alert-success' style>";
				echo "<div class='row center small'> EVENTS OPEN NOW  </div>";
				$i = 1;
				$alt = 'alt';
				$isOnDeck = array();
				foreach($tickets['live'] as $t) {
					$class = 'color';
					$alt = $alt ? null : "alt";
					echo "<div class='row ".$class." ".$alt."'>";
					$id = null;//" (".$t->id.")";
					//echo "<div class='profile_ticket_num'>".$i.$id.".</div>";
					
					if($t->event_id) {
						$event = FacebookEvent::getID($t->event_id);
						echo "<div class='profile_ticket_event'><a href='/eventinfo?id=".$t->event_id."'><i class='fa fa-ticket' aria-hidden='true'></i> ".$event->Name."</a></div>";
						
						echo "<div class='profile_ticket_expires'>";
						$ondeck = OnDeck::getByEventID($t->event_id);
						if($ondeck) {
							if(!isset($isOnDeck[$ondeck->ID])) {
								$isOnDeck[$ondeck->ID] = true;
								$userOnDeck = UserOnDeck::getByUserID($ondeck->ID, $user->id);
								if($userOnDeck) {
									echo "<a class='button medium blue' href='".View::url("ondeck?odid=".$ondeck->ID)."'><i class='fa fa-check-square-o' aria-hidden='true'></i> You are #".$userOnDeck->OrderNum." On Deck</a>";
								}
								else {
									echo "<a class='button' href='".View::url("ondeck?odid=".$ondeck->ID."&cmd_add_me=1")."'><i class='fa fa-plus' aria-hidden='true'></i> Add Me On Deck</a>";
								}
							}
							else {
								echo "Please sign-up in person";
							}
						}
						else {
							echo "Event not using On Deck";
						}
						echo "</div>";
					}
					else {
						echo "<div class='profile_ticket_event'><i class='fa fa-ticket' aria-hidden='true'></i> </div>";
					}
					echo "</div>"; //row
					$i++;
				}
				echo "</div>";
			}
			else {
				$openDecks = OnDeck::getAllLiveOrOpenDecks();
				if($openDecks && count($openDecks) > 0) {
					echo "<div class='alert alert-success' style>";
					echo "<div class='row center small'> EVENTS OPEN NOW  </div>";
					foreach($openDecks as $ondeck) {
						$class = 'color';
						$alt = $alt ? null : "alt";
						echo "<div class='row ".$class." ".$alt."'>";
						
						$event = FacebookEvent::getID($ondeck->EventID);
						$name = $event ? $event->Name : $ondeck->Title;
						echo "<div class='profile_ticket_event'><a href='/eventinfo?id=".$ondeck->EventID."'><i class='fa fa-ticket' aria-hidden='true'></i> ".$name."</a></div>";

						echo "<div class='profile_ticket_expires'>";
						//if(!isset($isOnDeck[$ondeck->ID])) {
							$isOnDeck[$ondeck->ID] = true;
							$userOnDeck = UserOnDeck::getByUserID($ondeck->ID, $user->id);
							if($userOnDeck) {
								echo "<a class='button medium blue' href='".View::url("ondeck?odid=".$ondeck->ID)."'><i class='fa fa-check-square-o' aria-hidden='true'></i> You are #".$userOnDeck->OrderNum." On Deck</a>";
							}
							else
							if($tickets['unusedCount']) {
								echo "<a class='button' href='".View::url("ondeck?odid=".$ondeck->ID."&cmd_add_me=1")."'><i class='fa fa-plus' aria-hidden='true'></i> Use Ticket & Add Me On Deck</a>";
							}
							else {
								echo "<a class='button' href='/eventinfo?id=".$ondeck->EventID."'><i class='fa fa-plus' aria-hidden='true'></i> Buy Tickets</a>";
							}
						//}
						//else {
						//	echo "Please sign-up in person";
						//}
						echo "</div>";						
						echo "</div>";						
					}
					echo "</div>";
				}
			}
			
			
			if($tickets['rsvpCount']) {
				echo "<div class='alert alert-white' style>";
				echo "<div class='row center small'> ".$tickets['rsvpCount']." RESERVED TICKET".($tickets['rsvpCount'] == 1 ? "" : "S")." </div>";
				$i = 1;
				$alt = 'alt';
				$isOnDeck = array();
				foreach($tickets['rsvp'] as $t) {
					$class = 'color';
					$alt = $alt ? null : "alt";
					echo "<div class='row ".$class." ".$alt."'>";
					$id = null;//" (".$t->id.")";
					
					if($t->event_id) {
						$event = FacebookEvent::getID($t->event_id);
						echo "<div class='profile_ticket_event'><a href='/eventinfo?id=".$t->event_id."'><i class='fa fa-ticket' aria-hidden='true'></i> ".$event->Name;
						
						$time = strtotime($event->StartTime);
						echo " - ".date("M d", $time)." @".date("h:ia", $time);
						echo "</a></div>";
						
						echo "<div class='profile_ticket_expires'>";
						$ondeck = OnDeck::getByEventID($t->event_id);
						if($ondeck && ($ondeck->IsOpen || $ondeck->isStarted())) {
							if(!isset($isOnDeck[$ondeck->ID])) {
								$isOnDeck[$ondeck->ID] = true;
								$userOnDeck = UserOnDeck::getByUserID($ondeck->ID, $user->id);
								if($userOnDeck) {
									echo "<a class='button medium blue' href='".View::url("ondeck?odid=".$ondeck->ID)."'><i class='fa fa-check-square-o' aria-hidden='true'></i> You are #".$userOnDeck->OrderNum." On Deck</a>";
								}
								else {
									echo "<a class='button' href='".View::url("ondeck?odid=".$ondeck->ID."&cmd_add_me=1")."'><i class='fa fa-plus' aria-hidden='true'></i> Add Me On Deck</a>";
								}
							}
							else {
								echo "Please sign-up in person";
							}
						}
						else {
							if($user->sub && $user->sub->isActive()) {
								echo "<a id='unrsvp' class='small italic' href='javascript:;' ";
								echo "onclick='return ps.events.unrsvpTicket(\"".$t->id."\");'><i class='fa fa-times' aria-hidden='true'></i> Remove RSVP</a>";
							}
							
						}
						echo "</div>";
					}
					else {
						echo "<div class='profile_ticket_event'><i class='fa fa-ticket' aria-hidden='true'></i> </div>";
					}
					echo "</div>"; //row
					$i++;
				}
				echo "</div>";
			}
			
			if($tickets['unusedCount']) {
				echo "<div class='alert alert-white' style>";
				echo "<div class='row center small'> ".$tickets['unusedCount']." UNUSED TICKET".($tickets['unusedCount'] == 1 ? "" : "S")." </div>";
				$i = 1;
				$alt = 'alt';
				foreach($tickets['unused'] as $t) {
					$class = 'color';
					$alt = $alt ? null : "alt";
					echo "<div class='row ".$class." ".$alt."'>";
					$id = null;//" (".$t->id.")";
					echo "<div class='profile_ticket_num'>".$i.$id.".</div>";
					
					echo "<div class='profile_ticket_unused'></div>";
					if($user->isSuperAdmin() && !$user->isProfileOwner()) {
						echo "<a class='button medium white right pad-left' onclick='return ps.confirm(\"Are you sure you want to delete this ticket? This cannot be undone!\");' href='/profile/tickets?uid=".$user->id."&delete=".$t->id."'><i class='fa fa-trash' aria-hidden='true'></i></a>";
					}
					if($t->event_id) {
						$event = FacebookEvent::getID($t->event_id);
						echo "<div class='profile_ticket_event'><a href='/eventinfo?id=".$t->event_id."'><i class='fa fa-ticket' aria-hidden='true'></i> ".$event->Name."</a></div>";
					}
					else {
						echo "<div class='profile_ticket_event'><i class='fa fa-ticket' aria-hidden='true'></i> Any Producers Social</div>";
						if(!$t->hasExpiration()) {
							echo "<div class='profile_ticket_expires'>No Expiration</div>";
						}
						else {
							$soon = false;
							$exp = strtotime($t->expires);
							if($exp > time()) {
								$nextWeek = time() + (24 * 60 * 60 * 7);
								if($exp < $nextWeek) {
									$oneDay = 24 * 60 * 60;
									$numDays = round(($exp - time()) / $oneDay);
									echo "<div class='profile_ticket_expires _color bold'>Expires ";
									if($numDays == 0) {
										echo "today";
									}
									else {
										echo "in ".$numDays." day".($numDays == 1 ? "s" : "");
									}
									echo "</div>";
									$soon = true;
								}
							}
							if(!$soon) {
								echo "<div class='profile_ticket_expires _color'> Valid Until ".$dateformat->formatDate($t->expires)." ".$soon."</div>";
							}
						}
					}
					echo "</div>";//row
					$i++;
				}
				echo "</div>";
			}
			if($viewall && $tickets['usedCount']) {
				echo "<div class='alert alert-gray' style>";
				echo "<div class='row center small'> ".$tickets['usedCount']." USED TICKET".($tickets['usedCount'] == 1 ? "" : "S")." </div>";
				$i = 1;
				foreach($tickets['used'] as $t) {
					if($t) {
						$class = 'light';
						$alt = $alt ? null : "alt";
						echo "<div class='row ".$class." ".$alt."'>";
						echo "<div class='profile_ticket_num'>".$i.".</div>";
						
						echo "<div class='profile_ticket_used'><i class='fa fa-times' aria-hidden='true'></i></div>";
						if($t->event_id) {
							$event = FacebookEvent::getID($t->event_id);
							$ondeck = OnDeck::getByEventID($t->event_id);
							$name = $event->Name." ".date("M d Y", strtotime($event->StartTime));
							if(!$ondeck) {
								echo "<div class='profile_ticket_event'><a href='/eventinfo?id=".$t->event_id."'>".$name."</a></div>";
							}
							else {
								echo "<div class='profile_ticket_event'><a href='/ondeck?odid=".$ondeck->ID."'>".$name."</a></div>";
							}
							echo "<div class='profile_ticket_expires'> Used ".$dateformat->formatDate($t->used)." </div>";
						}
						else {
							echo "<div class='profile_ticket_event'>Any Producers Social</div>";
							if($t->isUsed()) {
								echo "<div class='profile_ticket_expires'> Used ".$dateformat->formatDate($t->used)." </div>";
							}
							else
							if($t->isExpired()) {
								echo "<div class='profile_ticket_expires'> Expired ".$dateformat->formatDate($t->expires)." </div>";
							}
						}
						echo "</div>";
						$i++;
					}
				}
				echo "</div>";
			}

			echo "<div class='row'>";
			if($tickets['count'] > 7 || $tickets['usedCount']) {
				if($limit || $tickets['usedCount']) {
					echo "<a class='clear center' href='".$controller->getUrl(array("viewall"=>1))."'>View All ".$tickets['count']."</a>";
				}
				else {
					echo "<a class='clear center' href='".$controller->getUrl()."'>View Less</a>";
				}
			}
			echo "</div>";
			
			echo "<a class='button medium right' href='/events'>Find Events</a> &nbsp;";
			if(!$user->isProfileOwner()) {
			}
			else
			if($user->sub) {
				$class = "white";
				if(!$tickets['unusedCount']) {
					$class = "";	
				}
				echo "<a class='".$class." button medium left' style='pad-right:10px;' href='/membership?renew=1'><i class='fa fa-id-card-o' aria-hidden='true'></i> Get more tickets by renewing your membership</a> &nbsp;";
			}
			else {
				echo "<a class='button medium left' style='pad-right:10px;' href='/membership'><i class='fa fa-id-card-o' aria-hidden='true'></i> Become a member for discounted tickets and more</a> &nbsp;";
			}
			/*
			Loader::model('purchase_item');
			
			$stripe = Loader::helper('stripe');
			$stripe->startup();
			
			$item3 = PurchaseItem::getByID("tickets3");
			$item10 = PurchaseItem::getByID("tickets10");
			
			echo $stripe->purchaseButton($item3->id, "Purchase 3 Tickets $30", $item3->price, 1, "/members/tickets", "purchase_tickets");
			echo "<div class='left'>&nbsp;</div>";
			echo $stripe->purchaseButton($item10->id, "Purchase 10 Tickets $100", $item10->price, 1, "/members/tickets", "purchase_tickets");
			*/
			echo "</div>";
		}
	?>
	</div>
</td>
</tr>
</table>
</div>
<?php
	if($user->isSuperAdmin()) {
		echo "<hr>";
		echo "<div class='admin_area uppercase small'>All sections visible to super admin only</div>";
	}
	if($user->isGuest()) {
		$a = new Area("GuestsAndNonMembers");
		$a->display($c);
	}
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
