<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
    Loader::element('profile/view', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
