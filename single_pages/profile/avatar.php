<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
 	$form = Loader::helper("form");
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
			?>    
			</td>
	
			<td id="profile_content_right" class='ccm-ui'>
				<div id="profile_content_avatar">
				<div class='profile_div'></div>
				<h1>Avatar Options</h1>
				<?php 
					Loader::model("social_login", "social_login");
					
					$socialTypes = array("Facebook", "Twitter", "Google");
					
					echo "<div id='profile_select_image' class='row'>";
					
					$anySocial = false;
					$url = $this->action("setImageType");
					foreach($socialTypes as $type) {
						if(SocialLoginModel::isConnectedWith($type)) {
							if(ADMIN) {
								echo "CONNECTED:".$type."<br>";	
							}
							if($adapter = SocialLoginModel::getProviderAdapter($type)) {
								if($up = $adapter->getUserProfile()) {
									$anySocial = true;
									echo "<div class='profile_image_container'>";
									echo "<div class='image_select_box'>";
									$ltype = strtolower($type);
									echo "<a href='/profile/avatar/setImageType/?imagetype=".$ltype."';\"><img class='profile_image image_".$ltype."' src='".$up->photoURL."'/></a>";
									echo "<div class='image_label'>Use ".$type."</div>";
									echo "</div>";
									echo "</div>";
								}
							}
						}
					}
					if(!$user->isProfileOwner()) {
						$urlid = "?uid=".$user->id;	
					}
					echo "<div class='profile_image_container left'>";
					echo "<div class='image_select_box'>";
					echo "<a href='/profile/avatar_upload".$urlid."'><img class='profile_image image_upload' src='/themes/producers/images/image_upload.png'/></a>";
					echo "<div class='image_label'>Upload Image</div>";
					echo "</div>";
					echo "</div>";
					
					if(!$anySocial) {
						echo "<div class='left pad' style='width:300px;'>";
						echo "<p>Or connect to a social media account to use an existing profile image</p>";
						echo "<br class='clear'>";
						echo "<a class='left' href='/login?provider=Facebook';\"><i class='fa fa-facebook superhuge' aria-hidden='true'></i> </a>";
						echo "<a class='left pad-left' href='/login?provider=Twitter';\"><i class='fa fa-twitter superhuge' aria-hidden='true'></i> </a>";
						echo "<a class='left pad-left' href='/login?provider=Google';\"><i class='fa fa-google superhuge' aria-hidden='true'></i> </a>";
						echo "</div>";
					}
					
					echo "</div>";
					/*
					$av = SocialLoginModel::getStructuredAvailable();
					foreach($av as $provider){
						if($provider['providerID']=="Facebook") {
		 					echo $form->submit("use_facebook_image", "Use Facebook Image", array('class'=>"btn btn-lg primary", 'action'=>$url, 'onclick'=>"setProfileImageType('facebook');")); 
						}
						else
						if($provider['providerID']=="Twitter") {
		 					echo $form->submit("use_twitter_image", "Use Twitter Image", array('class'=>"btn btn-lg primary", 'action'=>$url, 'onclick'=>"setProfileImageType('twitter');")); 
						}
						else
						if($provider['providerID']=="Google") {
		 					echo $form->submit("use_google_image", "Use Google Image", array('class'=>"btn btn-lg primary", 'action'=>$url, 'onclick'=>"setProfileImageType('google');")); 
						}
					}
					*/
 				?>
				
				<div id="avatar_container">
 					<div class='clear' id='profile_image_saved'></div>
 					
					<div id="crop_img_container" class="image-editor">
						<input type="file" id="file" class="cropit-image-input">
						<div class="cropit-preview"></div>
						<a id="rotate-ccw" class="rotate-ccw" href='javascript:;'><i class="fa fa-undo" aria-hidden="true"></i></a>
						<a id="rotate-cw" class="rotate-cw" href='javascript:;'><i class="fa fa-repeat" aria-hidden="true"></i></a>
						<a id="zoom-in" class="zoom-in" href='javascript:;'><i class="fa fa-plus-square" aria-hidden="true"></i></a>
						<a id="zoom-out" class="zoom-out" href='javascript:;'><i class="fa fa-minus-square" aria-hidden="true"></i></a>
						<br class='clear'>
					
						<form id="profile_image_form" action="<?php print $this->action("saveImage");?>" method="post" enctype="multipart/form-data">
							<?php print $form->hidden("img"); ?> 
							<?php 
								//echo $form->submit("submit", "Save", array('class'=>"button"));
							?>
							<button class='button white' onclick='return ps.profile.hideAvatarUpload();'>Cancel</button>
							<button type="submit" class="button right" id="submit" name="submit" value="Save" onclick='return ps.profile.saveAvatar();'>Save</button>
						</form>
					</div>
 				</div>
 				</div>
			</td>
		</tr>
	</table>
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
