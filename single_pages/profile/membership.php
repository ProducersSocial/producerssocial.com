<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	$dateformat = Loader::helper('date');
	$tickets = Loader::helper('tickets');
	
	$stripe = Loader::helper('stripe');
	$stripe->startup();
	
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('user'=>$user, 'this'=>$this, 'controller'=>$controller)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('user'=>$user, 'this'=>$this, 'controller'=>$controller)); 
			?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
				<div id='profile_content' class='row-fluid nopadding'>
					<?php  if (isset($error) && $error->has()) {
						$error->output();
					} else if (isset($message)) { ?>
						<div class="message"><?php echo $message?></div>
						<script type="text/javascript">
						$(function() {
							$("div.message").show('highlight', {}, 500);
						});
						</script>
					<?php  } ?>
					
					<?php
						if($err) {
							echo "<div class='alert alert-warning'>".$err."</div>";	
						}
						
						//print_r($_REQUEST);
					?>
		
					<div class='clear' id='profile_membership'>
					<div id="profile_membership_heading">
					<?php 
						if($user->isSuperAdmin() && !$user->isProfileOwner()) {
							echo "<div class='admin_area'>";
							echo "<a class='button medium white left pad-right' onclick='return ps.confirm(\"Are you sure you want to give this user a free 3 month membership?\");' href='/profile/membership?uid=".$user->id."&give=quarterly-membership'>Give 3 Months</a> &nbsp;";
							echo "<a class='button medium white left pad-right' onclick='return ps.confirm(\"Are you sure you want to give this user a free 1 year membership?\");' href='/profile/membership?uid=".$user->id."&give=yearly-membership'>Give 1 Year</a> &nbsp;";
							if($user->sub) {
								if($user->sub->isActive()) {
									echo "<a class='button medium white right pad-left' onclick='return ps.confirm(\"Are you sure you want to cancel this membership?\");' href='/profile/membership?uid=".$user->id."&cancel=1'><i class='fa fa-ban' aria-hidden='true'></i></a>";
								}
								else 
								if($user->sub->status == 2) {
									echo "<a class='button medium white right pad-left' onclick='return ps.confirm(\"Are you sure you want to re-activate this membership?\");' href='/profile/membership?uid=".$user->id."&reactivate=1'>Reactivate</a>";
								}
							}
							if($user->sub) {
								echo "<a class='button medium white right pad-left' onclick='return ps.confirm(\"Are you sure you want to fully delete this membership? This cannot be undone!\");' href='/profile/membership?uid=".$user->id."&delete=1'><i class='fa fa-trash' aria-hidden='true'></i></a>";
							}
							echo "</div>";
						}
						if($user->sub && $user->sub->isActive()) {
							echo "<div class='row membership_active'>";
							echo "<div class='col-md-6 profile_membership_label'>".t('Membership Active')."</div>";
							echo "<div class='col-md-6 profile_membership_field'><a class='button medium' href='/members'><i class='fa fa-sign-in' aria-hidden='true'></i> Goto Membership Portal</a></div>";
							echo "</div>";
						}
						else 
						if($user->sub) {
							echo "<div class='row membership_inactive'>";
							echo "<div class='col-md-6 profile_membership_label membership_status'>".t('Membership '.$user->sub->getStatus())."</div>";
							echo "<div class='col-md-6 profile_membership_field'>";
							echo "<a class='button medium' href='/membership?renew=1'><i class='fa fa-refresh' aria-hidden='true'></i> Rewew Now</a>";
							echo "</div>";
							echo "</div>";
						}
					?>
					</div>
					<div id="profile_membership_details">
						<?php
						if($user->sub) {
							//if(ADMIN) echo " canChangeAutorenew:".$canChangeAutorenew."<br>";
							if($turnedon) {
								echo "<div class='alert alert-success' rel='fadeaway'>";
								echo "Auto-renew has been turned back on for your subscription. Your membership will renew automatically on ".$dateformat->formatDate($user->sub->ending, true).".";
								echo "</div>";
							}
							if($turnedoff) {
								echo "<div class='alert alert-success' rel='fadeaway'>";
								echo "Auto-renew has been turned off for your subscription.";
								echo "</div>";
							}
							if($confirm && $canChangeAutorenew) {
								echo "<div class='alert alert-warning'>";
								//echo "Your membership is currently paid through ".$dateformat->formatDate($user->sub->ending, true).", ";
								//echo "so will remain active until that time. Are you sure you wish to disable auto-renew?";
								echo "Are you sure you want to disable auto-renew? This will cancel your membership when the subscription period ends.";
								echo "<br><br>";
								echo "<form class='clear center' action='/profile/membership' method='post'>";
								$url = "/profile/membership?autorenew=";
								if(!$user->isProfileOwner()) {
									$url = "/profile/membership?uid=".$user->id."&autorenew=";	
								}
								echo "<input type='submit' id='autorenewoff' name='autorenewoff' class='right button medium white' style='margin-left:10px' value='Turn Off Auto-Renew'>";
								echo "<input type='submit' id='cancel' name='cancel' class='right button medium white' style='margin-left:10px' value='Cancel'>";
								echo "<input type='hidden' id='confirm' name='confirm' value='1'>";
								echo "<input type='hidden' id='autorenew' name='autorenew' value='1'>";
								echo "<input type='hidden' id='uid' name='uid' value='".$user->id."'>";
								echo "</form>";
								echo "</br>";
								echo "</div>";
							}
							else {
								//if(isset($_REQUEST['cancel'])) {
								//	echo "<div class='alert alert-info'>";
								//	echo "Are you sure you want to cancel auto-renew for your membership? Your benefits will remain active  ";
								//	echo "for the subscription period already paid for, but will no longer be available if the subscription expires.";
								//	echo "</div>";
								//}
								
								echo "<div class='row'>";
								echo "<div class='col-md-6 profile_membership_label bold'>";
								$tickets->displayMemberTicketStatus(true, false, '', 'left');
								echo "</div>";
								if(!$user->isProfileOwner()) {
									$urlid = "?uid=".$user->id;	
								}
								echo "<div class='col-md-6 profile_membership_field'><a class='button white medium' href='/members/tickets".$urlid."'><i class='fa fa-ticket' aria-hidden='true'></i> Manage Tickets</a></div>";
								echo "</div>";
								
								//echo "<div class='clear' id='profile_membership'>";
								
								echo "<div class='row'>";
								echo "<div class='col-md-6 profile_membership_label'>Plan</div>";
								echo "<div class='col-md-6 profile_membership_field'>".$user->sub->planName()."</div>";
								echo "</div>";
					
								/*
								echo "<div>";
								echo "<div class='col-md-6 profile_membership_label'>Location</div>";
								echo "<div class='col-md-6 profile_membership_field'>".$user->sub->locationName()."</div>";
								echo "</div>";
								*/
					
								echo "<div class='row'>";
								echo "<div class='col-md-6 profile_membership_label'>".($user->sub->autorenew ? "Will Renew On" : "Valid Until")."</div>";
								echo "<div class='col-md-6 profile_membership_field'>".$dateformat->formatDate($user->sub->ending, true)."</div>";
								echo "</div>";
					
								echo "<div class='row'>";
								echo "<div class='col-md-6 profile_membership_label'>Subscription Date</div>";
								echo "<div class='col-md-6 profile_membership_field'>".$dateformat->formatDate($user->sub->created, true)."</div>";
								echo "</div>";
					
								echo "<div class='row'>";
								echo "<div class='col-md-6 profile_membership_label'>Auto Renew</div>";
								echo "<div class='col-md-6 profile_membership_field'><span class='left'>".($user->sub->autorenew ? "ON" : "OFF")."</span>";
								echo " &nbsp; ";
								
								if($canChangeAutorenew) {
									echo "<form class='right' action='/profile/membership' method='post'>";
									$url = "/profile/membership?autorenew=";
									if(!$user->isProfileOwner()) {
										$url = "/profile/membership?uid=".$user->id."&autorenew=";	
									}
									if($user->sub->autorenew) {
										echo "<input type='submit' id='autorenewoff' name='autorenewoff' class='right button medium white' style='margin-left:10px' value='Turn Off'>";
									}
									else {
										echo "<input type='submit' id='autorenewon' name='autorenewon' class='right button medium white' style='margin-left:10px' value='Turn On'>";
									}
									echo "<input type='hidden' id='autorenew' name='autorenew' value='1'>";
									echo "<input type='hidden' id='uid' name='uid' value='".$user->id."'>";
									echo "</form>";
								}
								
								echo "</div>";
								echo "</div>";
							}
							if($user->isSuperAdmin() && !$user->isProfileOwner()) {
								echo "<div class='row light'>";
								echo "<div class='col-md-6 profile_membership_label'>Subscription ID</div>";
								echo "<div class='col-md-6 profile_membership_field'>".$user->sub->id."</div>";
								echo "</div>";
								echo "<div class='row light'>";
								echo "<div class='col-md-6 profile_membership_label'>Status</div>";
								echo "<div class='col-md-6 profile_membership_field'>".$user->sub->status."</div>";
								echo "</div>";
							}
						}
						else {
							echo "<p>Thank you for registering for a free account. To enjoy the extended benefits of the Producers Social ";
							echo "such as priority member listing, discounted entry into events, and other perks, become an official member today!</p>";
							echo "<br>";
							echo "<div class='ccm-ui'>";
							echo "<a class='button' href='/membership'><i class='fa fa-id-card-o large' aria-hidden='true'></i> Become a Member!</a>";
							echo "</div>";
						}
						
						echo "<br class='clear'>";
						echo "<div class='profilin'>";
						
						if(!$user->isProfileOwner()) {
						}
						else
						if($user->sub) {
							echo "<a class='button medium white' href='/membership?renew=1'><i class='fa fa-refresh' aria-hidden='true'></i> Renew Membership</a>";
						}
						echo "</div>";
						?>
						<br>
						<div class="spacer">&nbsp;</div>
					</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
