<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();

	$form = Loader::helper('form');
	Loader::model('social_login', 'social_login');
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
			?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
		
				<div id='profile_content' class='row-fluid nopadding'>
					<?php
					if($confirmed) {
						$redirect = BASE_URL;
						//$redirect = $this->url('/login', 'logout');
					?>
						<div class="alert alert-success">
							<h1 class="profilin"><?php echo t('Your account has been deleted')?></h1>
							<?php
							//	echo $redirect;
							?>
							<script>
								setTimeout("location.href = '<?php echo $redirect;?>';", 2000);
							</script>
						</div>
					<?php
					}
					else {
					?>
						<div class='alert alert-white center' style='width:500px; max-width:100%;'>
						<h1 class="profilin">
						<i class='fa fa-warning superhuge center' aria-hidden='true'></i>
						<?php echo t('Delete Account?')?></h1>
						<div class="ccm-form ccm-ui">
							<br class='clear'>
							<?php
								if($user->isSuperAdmin()) {
									echo "<p>Are you sure you wish to permanently delete this user account (".$user->id."), profile and associated data? Once confirmed it cannot be undone.</p>";
								}
								else {
									echo "<p>Are you sure you wish to permanently delete your user account, profile and associated data? Once confirmed it cannot be undone and you will be logged out of the system</p>";
								}
							?>
							<br>
							<?php
								$url = "/profile/delete?confirm=true";
								if($user->isSuperAdmin()) {
									$url .= "&uid=".$user->id;
									echo "<a class='button red' href='".$url."'>Yes, Delete This Account</a>";
								}
								else {
									echo "<a class='button red' href='".$url."'>Yes, Delete My Account</a>";
								}
							?>
						</div>
						</div>
					<?php
					}
					?>
					<div class="spacer">&nbsp;</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>

<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
