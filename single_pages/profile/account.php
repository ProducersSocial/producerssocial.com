<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();

	$form = Loader::helper('form');
	Loader::model('social_login', 'social_login');
?>
<div id="profile_container">
    <?php  
    Loader::element('profile/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    ?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
			<?php  
			Loader::element('profile/navbar', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
			?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
		
				<div id='profile_content' class='row-fluid nopadding'>
					<?php  if (isset($error) && $error->has()) {
						$error->output();
					} else if (isset($message)) { ?>
						<div class="message"><?php echo $message?></div>
						<script type="text/javascript">
						$(function() {
							$("div.message").show('highlight', {}, 500);
						});
						</script>
					<?php  } ?>
		
		
					<div class="ccm-form">
						<form method="post" class="ccm-ui" action="/profile/account/save" id="profile-account-form" enctype="multipart/form-data">
						<?php
							if(!isset($_REQUEST['change_password'])) {
						?>
						<div class='col-md-6'>
						<h3><?php echo t('Account Settings')?></h3>
						<?php
							echo $form->hidden('uid', $user->id);
						?>
						<fieldset>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uName', t('Username / Login Name'))?> <span class="ccm-required">*</span><br/>
								<?php echo $form->text('uName',$user->info->getUserName())?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uEmail', t('Email'))?> <span class="ccm-required">*</span><br/>
								<?php echo $form->text('uEmail',$user->info->getUserEmail())?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uMobile', t('Cell Phone Number'))?><br/>
								<?php 
									echo $form->telephone('uMobile', $user->profile->uMobile);
								?>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uSendReminders', t('Reminder emails for upcoming events'))?><br/>
								<div class="controls">
								<label class="checkbox">
								<?php echo $form->checkbox('uSendReminders', 'uSendReminders', $user->profile->uSendReminders == 1)?> Yes
								</label>
								</div>
							</div>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('uNewsletter', t('Receive news and updates by email'))?><br/>
								<div class="controls">
								<label class="checkbox">
								<?php echo $form->checkbox('uNewsletter', 'uNewsletter', $user->profile->uNewsletter == 1)?> Yes
								</label>
								</div>
							</div>
						</fieldset>
						<br class='clear'>
						<?php 
							$url = "/profile/delete";
							if($user->isSuperAdmin()) {
								$url .= "?uid=".$user->id;	
							}
							echo "<a class='delete_account_link right' href='".$url."'>Delete Account</a>";
						?>
						<?php echo $form->submit('save', t('Save Account Settings'), array('class'=>'button left'))?>
						<br class='clear'><br>
						</div>
						<?php
							}
						?>
						
						<div class='col-md-6'>
						<?php
							if(!isset($_REQUEST['change_password'])) {
						?>
							<h3><?php echo t('Social Login')?></h3>
							<fieldset class="socialLogin">
							<?php   
								$av = SocialLoginModel::getStructuredAvailable();
								$providers = array();
								foreach($av as $provider){
									if(SocialLoginModel::providerEnabled($provider['providerID'])){
										$providers[]=$provider;
									}
								}
								foreach($providers as $provider){
									$active = SocialLoginModel::isAssociatedWith($provider['providerID']);
									
									echo "<div class='profile_connect_social ".($active ? "active" : "")."'>";
									$icon = "<i class='fa fa-";
									$icon .= strtolower($provider['providerID']);
									$icon .= " superhuge' aria-hidden='true'></i>";
									
									$redirect = "&redirect=/profile/account";
									
									if(!$active) { 
										echo "<a href='/profile/account/?provider=".$provider['providerID'].$redirect."'>";
										echo $icon;
										echo "<br class='clear'>";
										echo t('Connect ');
										echo $provider['providerName'];
										echo "</a>";
									}
									else {
										echo "<a href='/profile/account/?provider=".$provider['providerID'].$redirect."&disassociate=1'>";								
										echo $icon;
										echo "<br class='clear'>";
										echo t('Disconnect ');
										echo $provider['providerName'];
										echo "</a>";
									}
									echo "</div>";
								}
							?>
							</fieldset>
							<br class='clear'><br>
							<?php
							 echo "<a href='/profile/account?uid=".$user->id."&change_password=1' class='button white medium'>Change Password</a>";
							?>
						<?php
							}
							else {
						?>
							<h3><?php echo t('Change Password')?></h3>
							<p><?php echo t("Leave this blank to keep your current password.")?></p>
							<fieldset>
							<?php
								echo $form->hidden('uid', $user->id);
							?>
								<div class="ccm-profile-attribute">
									<?php echo $form->label('uPasswordNew', t('New Password'))?>
									<?php echo $form->password('uPasswordNew')?>
								</div>
								<div class="ccm-profile-attribute">
									<?php echo $form->label('uPasswordNewConfirm', t('Confirm New Password'))?>
									<?php echo $form->password('uPasswordNewConfirm')?>
								</div>   
							</fieldset>
							<div class="spacer" style="margin-top:20px">&nbsp;</div>
						<?php
								echo $form->submit('save', t('Save New Password'), array('class'=>'button'));
							}
						?>
						</div>
						</form>
						<div class="spacer">&nbsp;</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>

<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
