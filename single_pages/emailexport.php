<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	error_reporting(E_ERROR);
	
	$out = "<h1>Restricted Area</h1>";
	
	$u = new User();
	if($u &&  $u->isLoggedIn()) {
		Loader::model('user_profile');
		$profile = UserProfile::getByUserID($u->uID);
		if(UserProfile::isLocationAdmin()) {
			//$out = '<h1>Email Export</h1>';
			header('Content-type: text/csv');
			header('Content-disposition: attachment;filename=ProducersSocialEmailList.csv');
			//$out .= "<textarea style='width:100%; height:600px;'>";

			$out = "Email Address\tArtist Name\n";
	
			$profiles = UserProfile::getAll("WHERE uHide=0 ORDER BY uArtistName ASC");
			if(!$profiles) { 
				$out .= "None Found";
			} 
			else { 
				foreach($profiles as $p) {
					$user = UserInfo::getByID($p->uID);
					if($user) {
						$out .= $user->getUserEmail();
						$out .= "\t";
						if($p->uArtistName) {
							//$name = explode(" ", $p->uFullName);
							//$out .= trim($name[0])."\t".trim($name[count($name)-1]);
							$out .= trim($p->uArtistName)."\t";
						}
						else {
							$out .= trim($user->getUserName())."\t";
						}
						$out .= "\n";
					}
				}
			}
	
			//$out .= '</textarea>';
			//Loader::element('view_template', array('innerContent'=> $out));
		}
		//else $out = "Not Admin:".$u->uID;
	}
	
	echo $out;
?>