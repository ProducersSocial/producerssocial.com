<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	ob_start();
	
	Loader::model('membership');
	Loader::model('user_membership');
	$u = new User();
	
	$form = Loader::helper('form');
	 
	$action = null;
	if(isset($_REQUEST['action'])) {
		$action = $_REQUEST['action'];
	}
	
	$validationUrl = "http://producerssocial.com/validate?id=";
?>
<div id="ccm-profile-wrapper">
	<div id="memberships_page" class="ccm-form ccm-ui">
	<?php
		if(!$u->isSuperUser()) {
			echo "<h2>Restricted Area</h2>";
		}
		else {
			?>
			<h2>Memberships</h2>
			<p>This page is used to manage memberships and is only visible to admins.</p>
			<?php
			if($action != "viewall") {
			?>
				<form method="post" action="<?php echo $this->action('?action=viewall')?>" id="profile-edit-form" enctype="multipart/form-data">
					<fieldset>
						<?php echo $form->submit('viewall', t('View All'), array('class'=>'btn btn-lg primary'))?>
					</fieldset>
				</form>
			<?php
			}
			?>
			
			<?php
				$showCommands = true;
				if($action == "showbatch") {
					if(!isset($_REQUEST['batch']) || !$_REQUEST['batch']) {
						$error = "No batch number was specified";
					}
					else {
						$formatted = null;
						$batch = Membership::getBatch($_REQUEST['batch']);
						if(!count($batch)) {
							$error = "No memberships were found for the batch number ".$_REQUEST['batch'];
						}
						else {
							echo "<h3>Batch Export (for printing QR codes)</h3>";
							foreach($batch as $m) {
								$formatted .= $validationUrl.$m->mID."<br>";
							}
							echo "<fieldset class='export'>";
							echo $formatted;
							echo "</fieldset>";
						}
					}
				}
				else
				if($action == "generate") {
					if(!isset($_REQUEST['count']) || !$_REQUEST['count']) {
						$error = "Please enter the number of memberships to generate.";
					}
					else
					if(!isset($_REQUEST['location']) || !$_REQUEST['location']) {
						$error = "Please select a location.";
					}
					 
					if(!$error) {
						echo "<div class='error'>$error</div>";
						echo "<h3>New Memberships Generated</h3>";
						$list = Membership::generate($_REQUEST['count'], $_REQUEST['location']);
						
						$batch = 0;
						$x = 0;
						echo "<table>";
						echo "<tr>";
						echo "<th>Membership ID</th>";
						echo "<th>Batch Number</th>";
						echo "</tr>";
						foreach($list as $m) {
							echo "<tr>";
							echo "<td>".$m->mID."</td>";
							echo "<td>".$m->mBatch."</td>";
							echo "</tr>";
							
							$batch = $m->mBatch;
							$x++;
							if($x > 15) {
								echo "<tr>";
								echo "<td>... plus ".(count($list)-$x)." more</td>";
								echo "<td> </td>";
								echo "</tr>";
								break;
							}
						}
						echo "</table>";
						
						$showCommands = false;
						?>
						<form method="post" action="<?php echo $this->action('?action=showbatch')?>" id="profile-edit-form" enctype="multipart/form-data">
							<fieldset>
								<?php echo $form->hidden('batch', $batch)?>
								<?php echo $form->submit('showbatch', t('Show Batch Export List'), array('class'=>'btn btn-lg primary'))?>
							</fieldset>
						</form>
						<?php
					}
				}
				else
				if($action == "viewall") {
					$all = UserMembership::getAll();
					echo "<table id='memberships_view_all'>";
					echo "<tr>";
					echo "<th>User ID</th>";
					echo "<th>Location</th>";
					echo "<th>Username</th>";
					echo "<th>Email</th>";
					echo "<th>Membership ID</th>";
					echo "<th>Status</th>";
					echo "<th>Expiration</th>";
					echo "</tr>";
					foreach($all as $um) {
						$us = User::getByUserID($um->uID);
						echo "<tr>";
						echo "<td>".$um->uID."</td>";
						echo "<td>".$um->uLocation."</td>";
						echo "<td>".$us->uName."</td>";
						echo "<td>".$us->uEmail."</td>";
						echo "<td><a href='".$validationUrl.$um->mID."' target='_blank'>".$um->mID."</a></td>";
						echo "<td>".$um->uStatus."</td>";
						echo "<td>".$um->uExpirationDate."</td>";
						echo "</tr>";
					}
					echo "</table>";
				}
				if($error) {
					echo "<div class='error'>$error</div>";
				}
				if($showCommands) {
			?>
					<h3>Show Batch</h3>
					<p>Find all the memberships for a given batch and display an export list, for printing card QR codes.</p>
					<form method="post" action="<?php echo $this->action('?action=showbatch')?>" id="profile-edit-form" enctype="multipart/form-data">
						<fieldset>
							<div class="ccm-profile-attribute">
								<?php echo $form->label('batch', t('Enter batch number'))?><br/>
								<?php echo $form->text('batch', 1)?>
							</div>
							<?php echo $form->submit('showbatch', t('Show Batch Export List'), array('class'=>'btn btn-lg primary'))?>
						</fieldset>
					</form>
					<br>
					<br>
					
					<h3>Generate</h3>
					<p>Use this to generate a new series of unique membership IDs. This should only be used in preparation for printing new cards. Each time memberships are generated a new batch number is created.</p>
			
					<div class="ccm-form ccm-ui">
						<form method="post" action="<?php echo $this->action('?action=generate')?>" id="profile-edit-form" enctype="multipart/form-data">
							<fieldset>
								<div class="ccm-profile-attribute">
									<?php echo $form->label('location', t('Location'))?><br/>
									<?php echo $form->select('location', UserMembership::$Locations, "LA")?>
								</div>
								<div class="ccm-profile-attribute">
									<?php echo $form->label('count', t('How many?'))?><br/>
									<?php echo $form->text('count', 100)?>
								</div>
								<br>
								<?php echo $form->submit('generate', t('Generate'), array('class'=>'btn btn-lg primary'))?>
							</fieldset>
						</form>
					</div>
			<?php
				}
		}
	?>
	</div>
</div>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>