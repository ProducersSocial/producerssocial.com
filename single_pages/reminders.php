<?php  
defined('C5_EXECUTE') or die("Access Denied."); 
date_default_timezone_set("America/Los_Angeles");

Loader::model('facebook_event');
Loader::model('on_deck');
Loader::model('userinfo');
Loader::model('user_profile');
Loader::model('locations');

ob_start();

	$isLocationAdmin	= OnDeck::isLocationAdmin();
	$isSuperAdmin 		= $isLocationAdmin == "HQ";

	if($isSuperAdmin) $_REQUEST['pass'] = "nz092d02k1azlsw92wke3";
	
	if($_REQUEST['dontsend']) {
		$p = UserProfile::getByUserID($_REQUEST['dontsend']);
		if($p) {
			$p->uSendReminders = 0;
			UserProfile::save($p);
			echo "<div class='alert alert-white'>";
			echo "<h2><i class='fa fa-cogs' aria-hidden='true'></i> Email Preferences Updated</h2>";
			echo "You will no longer receive reminder emails for events. To make further changes, please visit your <a href='/profile'>Account Settings</a>";
			echo "</div>";
			$_REQUEST['redirect'] = View::url("/profile");
		}
		else {
			echo "<h1>Account Error</h1>";
			echo "No account could be located for the user ID ".$_REQUEST['dontsend'].". For assistance, please contact <a href='mailto:admin@producerssocial.com'>admin@producerssocial.com</a>";
		}
	}
	else
	if(!isset($_REQUEST['pass']) || $_REQUEST['pass'] != "nz092d02k1azlsw92wke3") {
		echo "<h1>Admin Access Only</h1>";
		$_REQUEST['redirect'] = BASE_URL.View::url("/");
	}
	else {
		echo "<h1>Send Reminder Emails</h1>";
	
		$testmode = false;//DEV;
		if($testmode) echo "<h2>TEST MODE</h2>";
		$lookAhead = time() + (60 * 60 * 24 * 3); // 3 days
		//$day = "2016-11-16 00:00:00";
		$day = date("Y-m-d", $lookAhead)." 00:00:00";
		$dayEnd = date("Y-m-d", $lookAhead)." 23:59:59";
		$q = "WHERE StartTime > '".$day."' AND StartTime < '".$dayEnd."'";
		echo $q."<br>";
		
		$events = FacebookEvent::getAll($q);
		if($events && count($events) > 0) {
		
			foreach($events as $event) {
				echo $event->Name." ".$event->Location."<br>";
				$locationName = Locations::getName($event->Location);
			
				$eventTime = strtotime($event->StartTime);
				$eventDate = date("l, M j", $eventTime)." starting at ".date("g:ia", $eventTime);
			
				$eventUrl = BASE_URL.View::url("/eventinfo?id=".$event->ID);
			
				$count = 0;
				$profiles = UserProfile::getAll("WHERE uSendReminders=1 AND uLocation='".$event->Location."'");
				if($profiles) {
					foreach($profiles as $p) {
						$ui = UserInfo::getByID($p->uID);
						if($ui) {
							$memberName = $p->uFullName;
							if(!$memberName) $memberName = $ui->getUserName();
							
							echo $memberName." : ".$ui->getUserEmail()."<br>";
						
							$mh = Loader::helper('mail');
							if($testmode) {
								$mh->to("stephen@walkerfx.com");
							}
							else {
								$mh->to($ui->getUserEmail());
							}
							$mh->from("admin@producerssocial.com");
							$mh->bcc("stephen@producerssocial.com");
							$mh->addParameter('memberName', $memberName);
							$mh->addParameter('locationName', $locationName);
							$mh->addParameter('event_id', $event->ID);
							$mh->addParameter('eventName', $event->Name);
							$mh->addParameter('eventUrl', $eventUrl);
							$mh->addParameter('eventDate', $eventDate);
							$mh->addParameter('user_id', $p->uID);
							
							$count++;
							$mh->load('event_reminder');
							if($testmode) {
								if($ui->uID == 38) {
									echo $mh->getBodyHTML();
									$mh->sendMail();
									break;
								}
							}
							else {
								echo $mh->getBodyHTML();
								$mh->sendMail();
							}
						}
					}
				}
			
				if($count > 0 && !$testmode) {
					$slack = Loader::helper("slack");
					$slack->send($count." Reminder Emails Sent ".$event->Location);
				}
				
				if($testmode) break;
			}
		}
	}
?>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	
	if(isset($_REQUEST['redirect'])) {
		header("Refresh:8; url=".$_REQUEST['redirect'], true, 303);
	}
	Loader::element('view_template', array('innerContent'=> $out));
?>