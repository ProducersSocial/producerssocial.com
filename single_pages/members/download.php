<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 

	echo "<div class='alert alert-white'>";
	echo $message;
	echo "</div>";
	
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>