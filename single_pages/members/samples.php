<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 

	$a = new Area('Main');
	$a->display($c);
	
	echo "<div id='members_content_section'>";
	if($item) {
		echo $feature->displayItemPage($item, null, true, "members/".$item->type);	
	}
	else
	if(!$items) {
		echo "<div class='alert alert-warning'>There are no sample packs to display.</div>";	
	}
	else {
		if(isset($_REQUEST['list'])) {
			echo $feature->displaySummaryList($items, "All ".$items[0]->getTypeName(), "members/".$items[0]->type);
		}
		else {
			echo $feature->displaySlides($items, $items[0]->getTypeName(), "members/".$items[0]->type);
		}
	}
		
	if($user->isMember()) {
		echo "<div id='members_home_news'>";
		$a = new Area("MembersOnly");
		$a->display($c);
		echo "</div>";
	}				
	echo "</div>";
	
	if($itemCount > 1) {
		$paging = Loader::helper("paging");
		
		$url = "/members/samples?list=true";
		$paging->show($page, $limit, $totalItems, $itemCount, $url, $query);
	}
	//else
	if(ADMIN) {
		//echo $query;
		echo "<br class='clear'>";
		echo "itemCount:".$itemCount."<br>";
	}
			
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>


<?php
			/*
			$count = count($items);
			if($count > 1) {
				echo "<div id='members_feature_minors'>";
				if($count == 2) {
					$col = "col-xs-12";
				}
				else
				if($count == 3) {
					$col = "col-xs-6";
				}
				else {
					$col = "col-xs-4";
				}
				
				$style = "style=\"background:url('/images/features/".$items[0]->id."_feature.jpg') no-repeat;\"";
				echo "<div id='members_feature_minor1' class='members_feature_minor ".$col."'>";
					echo "<div class='members_feature_inset'>";
						echo "<img class='feature_thumb' src='/images/features/".$items[0]->id."_thumb.jpg'/>";
						echo "<div class='members_feature_title'>".$items[1]->name."</div>";
						echo "<div class='members_feature_summary'>".$items[1]->summary."</div>";
					echo "</div>";
				echo "</div>";
				
				if($count > 2) {
					echo "<div id='members_feature_minor12' class='members_feature_minor ".$col."'>";
					echo $items[1]->name;
					echo "</div>";
				}
				if($count > 3) {
					echo "<div id='members_feature_minor13' class='members_feature_minor ".$col."'>";
					echo $items[2]->name;
					echo "</div>";
				}
				echo "</div>";
			}
			*/

?>