<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	// Split the list of profiles into premium and free accounts
	$users 		= array();
	$members 	= array();
	
	$isCurrentUserInList = false;
	
	//if(ADMIN) echo "COUNT:".count($profiles)."<br>";
	foreach($profiles as $p) {
		//if($search) { // || $p->hasAvatar()
			$sub 	= UserSubscription::getByUserID($p->uID);
			$link 	= '/members/profile/'.$p->uID;
			$u 		= UserInfo::getByID($p->uID);
			if($u) {
				if($sub && $sub->isActive()) {
					$members[$p->uID] = array("user"=>$u, "sub"=>$sub, "profile"=>$p, "link"=>$link);
				}
				else {
					$users[$p->uID] = array("user"=>$u, "sub"=>null, "profile"=>$p, "link"=>$link);
				}
			}
			else {
				echo "<div class='error'>No user info for ".$p->uID."</div>";	
			}
		//}
	}
?>

<div id="ccm-profile-wrapper">
	<div class='members_header_section'>
		<?php
			if(!$user->isMember()) {
				$a = new Area("NonMembersOnly");
				$a->display($c);
			}
		?>
		<div class='members_heading'>
			<?php 
				if(!count($members)) {
					echo t('Regular Members');
				}
				else {
					echo t('Premium Members');
				}
			?>
		</div>
		<div class='members_search'>
			<form class='members_search_form right' method="get" action='/members/directory'>
				<input type="hidden" name="location" value="<?php echo $location;?>" />
				<input id="keywords" name="keywords" type="hidden" value="" />		
				<button type="submit" value='Clear'><i class='fa fa-times' aria-hidden='true'></i></button>
			</form>
			<form class='members_search_form right' method="get" action='/members/directory/'>
				<input type='hidden' id='filter' value="<?php echo $filter;?>" />
				<input type="hidden" name="location" value="<?php echo $location;?>" />
				<button class='right' type="submit" value='Search'><i class='fa fa-search' aria-hidden='true'></i></button>
				<input class='right' id="keywords" name="keywords" type="text" value="<?php echo $keywords?>" size="20" />		
			</form>
		<?php
			echo "<div class='members_search_form'>";
			echo "<form id='member_location_form' method='get' action='/members/directory/'>";
			echo "<input type='hidden' id='filter' value='".$filter."'>";
			echo "<input type='hidden' id='location' value='".$location."'>";
			$list = Locations::getSelectList(1);
			$list = array_merge(array("all"=>"All Locations"), $list);
			echo $form->select('location', $list, $location, array('onchange'=>"return ps.submitForm('member_location_form');"));
			echo "</form>";
			echo "</div>";
		?>
		</div>
	</div>
	<br class='clear'>
	<div class="ccm-spacer"></div>
	<br class='clear'>
	
	<?php 
	//print_r($_REQUEST);
	if(isset($error_message)) {
		echo "<div class='alert alert-warning'>".$error_message."</div>";
	}
	if(!$profiles) { 
		echo "<div>".t('No members found.')."</div>";
	} 
	else { 
		if(count($members)) {
			foreach($members as $id => $m) {
				if($m['profile'] || ($user->isSuperAdmin() && $search)) {
					$user->displaySummary($id, false, $m['link'], $m['user'], $m['profile'], $m['sub']);
				}
			}
			echo "<br class='clear'>";
		}
		
		if(!$user->isLoggedIn()) {
			echo "<div class='alert alert-white'><a href='".View::url("/register")."'><i class='fa fa-sign-in' aria-hidden='true'></i> Register for free and create your own profile</a></div>";
		}
		else 
		if(!$user->isMember()) {
			echo "<div class='alert alert-white'><a href='".View::url("/membership")."'><i class='fa fa-id-card-o' aria-hidden='true'></i> Become a Premium Member for preferred listing in the directory!</a></div>";
		}
		
		if($users) {
			//echo "USERS:".count($users)."<br>";
			if(count($members)) { 
				echo "<h3>Regular Members</h3>";
				echo "<br>";
			}
			foreach($users as $id => $m) {
				if($m['profile'] || ($user->isSuperAdmin() && $search)) {
					$user->displaySummary($id, true, $m['link'], $m['user'], $m['profile'], $m['sub']);
				}
			}
		}
		echo "<br class='clear'>";
	}
	
	if(!$page) $page = 1;
	$pages = ceil($total / $limit);
	
	$url = "/members/directory?p=1";
	if($keywords) $url .= "&keywords=".$keywords;
	if($location) $url .= "&location=".$location;
	if($filter) $url .= "&filter=".$filter;
	
	echo "<div class='paging'>";
	if($page > 1) {
		echo "<a class='paging_start' href='".$url."'><i class='fa fa-step-backward' aria-hidden='true'></i> </a>";
	}
	else {
		echo "<div class='paging_start gray' href='".$url."'><i class='fa fa-step-backward' aria-hidden='true'></i> </div>";
	}
	if($page > 1) {
		echo "<a class='paging_prev' href='".$url."&page=".($page-1)."'><i class='fa fa-chevron-left' aria-hidden='true'></i> </a>";
	}
	else {
		echo "<div class='paging_prev gray'><i class='fa fa-chevron-left' aria-hidden='true'></i> </div>";
	}
	echo $page." of ".$pages;
	if($page < $pages) {
		echo "<a class='paging_end' href='".$url."&page=".$pages."'><i class='fa fa-step-forward' aria-hidden='true'></i> </a>";
	}
	else {
		echo "<div class='paging_end gray'><i class='fa fa-step-forward' aria-hidden='true'></i> </div>";
	}
	if($page < $pages) {
		echo "<a class='paging_next' href='".$url."&page=".($page+1)."'><i class='fa fa-chevron-right' aria-hidden='true'></i> </a>";
	}
	else {
		echo "<div class='paging_next gray'><i class='fa fa-chevron-right' aria-hidden='true'></i> </div>";
	}
	echo "</div>";
	
	echo "<br class='clear'>";
	if(!$user->isLoggedIn()) {
		echo "<div class='alert alert-white'><a href='".View::url("/register")."'><i class='fa fa-id-card-o' aria-hidden='true'></i> Want to add your own listing? Just login or register for a free account and fill out your profile!</a></div>";
	}
	else
	if(!$isCurrentUserInList) {
		echo "<div class='alert alert-white'><a href='".View::url("/profile/avatar")."'><i class='fa fa-id-card-o' aria-hidden='true'></i> Don't see yourself listed? Just fill out your profile and upload an image!</a></div>";
	}
	
	if(ADMIN) {
		if($query) {
			echo "<div class='admin_area'>";
			echo "<h3>Viewing: ".count($profiles)." of ".$total."</h3>";
			echo "<p class='small'>".$query."</p>";
			echo "</div>";
		}
	}
?>
</div>
<?php
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>