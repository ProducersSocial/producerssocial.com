<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 

	$a = new Area('Main');
	$a->display($c);
	
	echo "<div id='members_content_section'>";
	if($item) {
		echo $feature->displayItemPage($item, null, true, "members/".$item->type);	
	}
	else
	if(!$items) {
		echo "<div class='alert alert-white center' style='width:500px; max-width:100%;'>";
		echo "<i class='fa fa-heart-o superhuge center' aria-hidden='true'></i>";
		echo "<br class='clear'>";
		echo "You have not favorited any items yet.";
		echo "To make a favorite, click the heart icon on the item page.";
		echo "</div>";	
	}
	else {
		if(isset($_REQUEST['list'])) {
			echo $feature->displaySummaryList($items, "Favorites", "members/".$items[0]->type);
		}
		else {
			echo $feature->displayGrid($items, "Favorites", "members/".$items[0]->type);
		}
	}
		
	if($user->isMember()) {
		echo "<div id='members_home_news'>";
		$a = new Area("MembersOnly");
		$a->display($c);
		echo "</div>";
	}				
	echo "</div>";
	
	if($itemCount > 1) {
		$paging = Loader::helper("paging");
		
		$url = "/members/favorites?p=1";
		$paging->show($page, $limit, $totalItems, $itemCount, $url, $query);
	}
	//else
	if(ADMIN) {
		//echo $query;
		echo "<br class='clear'>";
		echo "itemCount:".$itemCount."<br>";
	}
			
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>