<?php 
// members/profile
	
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$user->profile->setMetaData();

	//Loader::element('profile/view', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
?>
<div id="profile_container">
    <?php 
    Loader::element('profile/header', array('user'=> $user, 'profile'=>$profile, 'info'=>$info, 'sub'=>$sub, 'this'=>$this, 'nonav'=>true)); 
	?>    
	
	<div id="profile_content">
	<?php
		if(!$profile) {
			echo "<div class='alert alert-white center'><i class='fa fa-warning superhuge' aria-hidden-'true'></i><br class='clear'>";
			echo "No user found with the id or handle '".$id."'";
			echo "</div>";
		}
		else {
	?>
		<div class='profile_div'></div>

		<div id='profile_content' class='row-fluid nopadding'>
			<div class="profile_bio col-xs-12 col-md-12">
			<?php
				if($profile->uFeaturedTrack) {
					$producers = Loader::helper("producers");
					echo $producers->presentFeature($profile->uFeaturedTrack, false, false);
				}
				if($profile->uBio) echo $profile->uBio;
			?>
			</div>
			<div class="profile_html col-xs-12 col-md-12">
			<?php
				if($profile->uEmbed) {
					echo "<br>";
					echo $profile->uEmbed;
					echo "<br>";
				}
			?>
			</div>
		</div>

		<?php
		$uaks = UserAttributeKey::getPublicProfileList();
		foreach($uaks as $ua) { ?>
			<div>
				<label><?php echo $ua->getAttributeKeyDisplayName()?></label>
				<?php echo $info->getAttribute($ua, 'displaySanitized', 'display'); ?>
			</div>
		<?php } ?>		
		<?php 
			$a = new Area('Main'); 
			$a->setAttribute('profile', $info); 
			$a->setBlockWrapperStart('<div class="ccm-profile-body-item">');
			$a->setBlockWrapperEnd('</div>');
			$a->display($c); 
			
			if($info) {
				echo "<hr class='clear'>";
				if($user->isAdmin()) {
					echo "<div id='profile_userid'>ID:";
					echo $profile->uID;
					echo "</div>";
				}
				echo "<div id='profile_joined'>".t('Joined')." ";
				echo date("M Y", strtotime($info->getUserDateAdded()));
				echo "</div>";
				
				if($user->isLoggedIn()) {
					echo "<a class='profile_report_user' href='javascript:;' onclick='return ps.profile.reportUser(".$profile->uID.", ".$user->me->uID.");'>Report Abuse</a>";
				}
			}
		?>
	<?php
		}
	?>
	</div>
	<br class='clear'>
</div>
<?php
	if($user->isSuperAdmin() && $user->me->uID != $profile->uID) {
		echo "<div class='admin_area'>";
		echo "<div class='admin_area_label'>HQ Admin</div>";
		echo "<a class='button medium white left pad-right' href='/profile/membership?uid=".$profile->uID."'>View Membership</a>";
		echo "<a class='button medium white left pad-right' href='/profile/tickets?uid=".$profile->uID."'>View Member Tickets</a>";
		if($user->isActive()) {
			echo "<a class='button medium red right pad-right' href='/members/profile?uid=".$profile->uID."&deactivate=1'>Deactivate Account</a>";
		}
		else {
			echo "<a class='button medium red right pad-right' href='/members/profile?uid=".$profile->uID."&activate=1'>Activate Account</a>";
		}
		echo "</div>";
	}
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
