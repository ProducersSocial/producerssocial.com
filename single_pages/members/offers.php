<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 

	$a = new Area('Main');
	$a->display($c);
	
	echo "<div id='members_content_section'>";
	if($item) {
		echo $feature->displayItemPage($item, null, true, "members/".$item->type);	
	}
	else
	if(!$items) {
		echo "<div class='alert alert-warning'>There are no offers to display.</div>";	
	}
	else {
		if(isset($_REQUEST['list'])) {
			echo $feature->displaySummaryList($items, "All ".$items[0]->getTypeName(), "members/".$items[0]->type);
		}
		else {
			echo $feature->displaySlides($items, $items[0]->getTypeName(), "members/".$items[0]->type);
		}
	}
		
	if($user->isMember()) {
		echo "<div id='members_home_news'>";
		$a = new Area("MembersOnly");
		$a->display($c);
		echo "</div>";
	}				
	echo "</div>";
			
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>