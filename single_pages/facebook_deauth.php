<?php 
	//https://producerssocial.com/facebook_deauth?signed_request=Wb-ss3nmUUmOmfXENcIXocCnlC1lqCdMTcHFiljjFqA.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTUxNzUwNjY4NCwidXNlciI6eyJjb3VudHJ5IjoidXMiLCJsb2NhbGUiOiJlbl9VUyJ9LCJ1c2VyX2lkIjoiMTE4NTY5MDMyMjkxMjY3In0
	defined('C5_EXECUTE') or die("Access Denied.");
	$this->inc('elements/header.php'); 
	$this->inc('elements/navbar.php');
	
	function parse_signed_request($signed_request) {
	  list($encoded_sig, $payload) = explode('.', $signed_request, 2); 
	
	  $secret = "4ed7630d5957d715369350d44fc4ee2e"; // Use your app secret here
	
	  // decode the data
	  $sig = base64_url_decode($encoded_sig);
	  $data = json_decode(base64_url_decode($payload), true);
	
	  // confirm the signature
	  $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
	  if ($sig !== $expected_sig) {
		error_log('Bad Signed JSON signature!');
		return null;
	  }
	
	  return $data;
	}
	
	function base64_url_decode($input) {
	  return base64_decode(strtr($input, '-_', '+/'));
	}
?>
<div class='full_page'> 
	<div class="white_page">
		<div class="container">
			<div class="row">
			<?php
				if($_REQUEST) {
					//print_r($_REQUEST);
					
					echo "DEAUTH<br>";
					$json = date("Y-m-d H:i:s", time())." >> ";
					
					$id = null;
					if(isset($_REQUEST['signed_request'])) {
						$data = parse_signed_request($_REQUEST['signed_request']);
						$id = $data['user_id'];
						$json .= "id:".$id." ";
						$json .= json_encode($data);
					}
					else
					if(isset($_REQUEST['id'])) {
						$id = $_REQUEST['id'];
						$json .= "id:".$id;
					}
					
					if($id) {
						$db = Loader::db();
						$db->execute("DELETE FROM atSocialOauthValues WHERE identifier = '".$id."'");
					}
					
					$json .= "\n";
					echo $json;
					
					$filename = "/home/dh_w4724d/producerssocial.com/logs/facebook_deauth.log";
					$fp = fopen($filename, "a+");
					fwrite($fp, $json);
					fclose($fp);
				}
			?>
			</div>
		</div>
	</div>
</div>	
<br class='clear'>
<?php  $this->inc('elements/footer.php'); ?>
