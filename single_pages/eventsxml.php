<?php
Loader::model('facebook_event');
Loader::model('locations');

$events = FacebookEvent::getAll();

echo "<"."?xml version=\"1.0\"?".">\n";
echo "<monthly>\n";
foreach($events as $e) {
	$loc = Locations::getID($e->Location);
	$start = strtotime($e->StartTime);
	$end = null;
	if($e->EndTime != "0000-00-00 00:00:00") {
		$end = strtotime($e->EndTime);
	}
	if(!$end) $end = $start + (60 * 60 * 4);
	echo "\t<event>\n";
	echo "\t\t<id>".$e->ID."</id>\n";
	echo "\t\t<name>".$e->Name."</name>\n";
	echo "\t\t<startdate>".date("Y-m-d", $start)."</startdate>\n";
	echo "\t\t<enddate>".date("Y-m-d", $end)."</enddate>\n";
	echo "\t\t<starttime>".date("H:i:s", $start)."</starttime>\n";
	echo "\t\t<endtime>".date("H:i:s", $end)."</endtime>\n";
	echo "\t\t<color>#".$loc->Color."</color>\n";
	echo "\t\t<url>".BASE_URL.View::url("/eventinfo?id=".$e->ID)."</url>\n";
	echo "\t</event>\n\n";
}
echo "</monthly>\n";
?>
