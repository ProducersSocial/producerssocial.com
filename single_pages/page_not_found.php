<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	
	if($_SERVER['REQUEST_URI'] == "/ems") {
		header('location: https://producerssocial.com/members/offers/relativity_workshops_members_get_400_off_tuition?tr=a');
		die();
	}
	else {
		ob_start();
		$profile = null;
		Loader::model("user_profile");
		$path = str_replace("/", "", $_SERVER['REQUEST_URI']);
		$profile = UserProfile::getByHandle($path);
		if($profile) {
			Loader::model('user_subscription');
			$sub = UserSubscription::getByUserID($profile->uID);
			if(!$sub || !$sub->isActive()) {
				$profile = null;
			}
		}
		
		if($profile) {
			http_response_code(200);
			$user = Loader::helper('user');
			$user->setup($profile->uID);
			Loader::element('profile/view', array('user'=> $user, 'this'=>$this)); 
		}
		else {
?>
		<div id="ccm-profile-wrapper">
			<div class='members_header_section'>
			<div class='members_heading'><?php echo t('Page Not Found');?></div> 	
			</div>
			<br class='clear'>
			<div class="ccm-spacer"></div>
			<br class='clear'>
			
		 
		<?php echo t('The page you are looking for does not exist. Please check the url and try again.')?>
		 
		<?php
			//echo "REMOTE_ADDR:".$_SERVER['REMOTE_ADDR'].'<br>';
			if(ADMIN) {
				echo "REQUEST_URI:".$_SERVER['REQUEST_URI']."<br>";
			//	print_r($_REQUEST);	
			//	print_r(debug_backtrace());
			}
		?>
		<?php  if (is_object($c)) { ?>
			<br/><br/>
			<?php  $a = new Area("Main"); $a->display($c); ?>
		<?php  } ?>
		 
		<br/><br/>
		 
		<a href="<?php echo DIR_REL?>/"><?php echo t('Back to Home')?></a>
			
		</div>
		
<?php
		}
		$out = ob_get_contents();
		ob_end_clean();
		Loader::element('view_template', array('innerContent'=> $out));
	}
?>