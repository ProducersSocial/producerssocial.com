<?php 
	defined('C5_EXECUTE') or die("Access Denied.");

	$this->inc('elements/header.php'); 
	$this->inc('elements/navbar.php');

	Loader::model('locations');
	Loader::model('user_profile');
	Loader::model('user_subscription');
	Loader::model('user_ticket');
	Loader::model('on_deck');
	Loader::model('user_subscription');

	$events 	= Loader::helper('events');
	$tickets 	= Loader::helper('tickets');
	$form 		= Loader::helper('form');
	
	$events->closeSessionsLeftOpen();
	
	$u 			= new User();
	$p 			= null;
	$sub 		= null;
	
	if($u && $u->isLoggedIn()) {
		$p	 	= UserProfile::getByUserID($u->uID);
		$sub 	= UserSubscription::getByUserID($u->uID);
	}
	
	$location 		= null;
	$locationName	= null;
	if($p) {
		$location = $p->uLocation;
		if($location) {
			$locationName = Locations::getName($location);
		}
	}
	if(isset($_REQUEST['location'])) {
		if($_REQUEST['location'] == "all" || $_REQUEST['location'] == "hq") {
			$location = null;
		}
		else {
			$location = $_REQUEST['location'];
		}
	}
	
	$all = false;
	if(isset($_REQUEST['all'])) $all = true;
	
	$isLocationAdmin = OnDeck::isLocationAdmin();
?>
 
<div id='events_page' class='full_page'>
	<div class="white_page">
		<div class="container">
			<?php
				echo "<div class='events_page_header'>";
				echo "<div class='events_page_heading'>Events in</div>";
			
				echo "<div class='events_location_select'>";
				$list = Locations::getSelectList(1);
				$list = array_merge(array("all"=>"All Locations"), $list);
				echo $form->select('event_location', $list, $location, array('onchange'=>"return ps.events.locationChanged('event_location');"));
				echo "</div>";
				
				//$tickets->displayMemberTicketStatus();
				echo "</div>";
				echo "<br class='clear'>";
				
				//if($isLocationAdmin && $_REQUEST['refresh']) {
				//	$events->updateEventInfo();
				//}
				
				if($u->isLoggedIn()) {
					$a = new Area("MembersOnly");
					$a->display($c);

					if($sub && $sub->isActive()) {
						$a = new Area("PremiumMembersOnly");
						$a->display($c);
					}
				}
				if(!$u->isLoggedIn() || $u->isSuperUser()) {
					$a = new Area("GuestsOnly");
					$a->display($c);
				}
				$a = new Area('Main');
				$a->display($c);
				
				echo "<div id='ondeck_view'>";
				if(ENABLE_ONDECK) {
					Loader::element('events/live', array('location'=>$location, 'p'=>$p));
				}
				
				$a = new Area('OnDeck');
				$a->display($c);
				echo "</div>";
				
				echo "<div id='events_view'>";
					$startOfToday 	= date("Y-m-d", time())." 00:00:00";
					$endOfToday 	= date("Y-m-d", time())." 23:59:59";
					$oneYear		= date("Y-m-d", time() + (60 * 60 * 24 * 365))." 23:59:59";;
					
					echo "<a id='events_list_toggle' class='button small white' href='javascript:;' onclick='return ps.events.toggleCalendar();'><i class='fa fa-calendar' aria-hidden='true'></i> View Full Calendar</a>";
					echo "<div id='events_list_view'>";
					
					$q = "";//"EventID IS NOT NULL || (IsOpen=1 || IsLive=1)";
					$todaysEvents = $events->getEvents($startOfToday, $endOfToday, "", "ASC", true, $location, $q);
					if($todaysEvents) {						
						echo "<div class='alert alert-white clear'>";
						echo "<div class='events_section_heading'>Events Today</div>";
						//echo "NOW:".date("Y-m-d H:i:s", time())."<br>";
						$a = new Area('EventsList');
						$a->display($c);
						echo $events->listEvents($todaysEvents);
						echo "<br class='clear'>";
						echo "</div>";
					}
					
					$upcomingEvents = $events->getEvents($endOfToday, $oneYear, "", "ASC", true, $location);
					if($upcomingEvents) {
						echo "<div class='alert alert-white clear'>";
						echo "<div class='events_section_heading'>Upcoming Events</div>";
						$a = new Area('EventsList');
						$a->display($c);
						echo $events->listEvents($upcomingEvents); 
						echo "<br class='clear'>";
						echo "</div>";
					}
					
					$pastEvents = $events->getEvents("2013-01-01 00:00:00", $startOfToday, $all ? "" : "LIMIT 4", "DESC", true, $location); 
					if($pastEvents) {
						echo "<div id='pastevents'></div>";
						echo "<div class='alert alert-gray clear'>";
						echo "<div class='events_section_heading'>Past Events</div>";
						$a = new Area('EventsList');
						$a->display($c);
						echo $events->listEvents($pastEvents, true); 
						if(!$all) echo "<a href='".$this->url("/events?all=1&location=all#pastevents")."' class='right'>More...</a>";
						echo "<br class='clear'>";
						echo "</div>";
					}
					
					echo "</div>";//events_list_view
					
					echo "<div id='events_calendar_view'>";
					echo "<div class='alert alert-white clear'>";
					echo "<div class='monthly' id='calendar'></div>";
					echo "</div>";
					echo "</div>";
					
					Loader::element('events/admin');
				echo "</div>";

			?>
		</div>
	</div>
</div>	

<?php
$this->inc('elements/footer.php'); 
?>