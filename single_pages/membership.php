<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	$this->inc('elements/header.php'); 
	$this->inc('elements/navbar.php');
?>
<div class='full_page'> 
	<div class="white_page">
		<div class="container">
			<div class="row">
			<?php
				/*
				echo "<div class='right'>&nbsp;";
				if($user->isMember()) {
					echo "<a href='/profile/membership'><i class='fa fa-cog' aria-hidden='true'></i> My Membership</a>";
				}
				else 
				if($user->isLoggedIn()) {
					echo "<a href='/profile'><i class='fa fa-user' aria-hidden='true'></i> Goto My Profile</a>";
				}
				else {
					echo "<a href='/register'><i class='fa fa-user' aria-hidden='true'></i> Register for Free</a>";
				}
				echo "</div>";
				*/
				
				$a = new Area('Main');
				$a->display($c);
								
				if(!$user->isLoggedIn() || $user->isSuperAdmin()) {
					$a = new Area("Guests");
					$a->display($c);
				}
			?>
			</div>
		</div>
	</div>
</div>	
<br class='clear'>
<?php  $this->inc('elements/footer.php'); ?>
