<?php  
	defined('C5_EXECUTE') or die("Access Denied."); 
	
	$paypal = Loader::helper('paypal');
	
	ob_start();
?>

<h1>IPN</h1><br><br>
<?php
	$sendmail = !ADMIN;
	//define("DIR_BASE", "/home/producerssocial/public_html/");
	define("IPNLOG", DIR_BASE."/".(DEV ? "dev/" : "")."logs/ipn.txt");
	
	echo "EMAIL ENABLED: ".($sendmail ? "YES" : "NO")."<BR>";
	
	$bypass = false;
	if(isset($_REQUEST['bypass'])) {
		if($_REQUEST['bypass'] == $paypal->BypassKey) {
			$bypass = true;
		}
	}
	
	function ipnLog($data) {
		if($log = fopen(IPNLOG, "a+")) {
			fwrite($log, $data);
			fclose($log);
		} 
	}
	
	//print_r($_REQUEST);
	
	$req = "\n\n>>>".date('Y-m-d H:i:s').($bypass ? "BYPASS" : "").":\n";
	$req .= "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]."?";
	
	$query = "";
	foreach($_REQUEST as $key => $value) {
		if(!is_array($value)) {
			$value = urlencode(stripslashes($value));
			if($key != "CONCRETE5" && $key != "__unam" && $key != "_gat" && $key != "_ga" && $key != "asktosubscribe") {
				echo $key."=".$value."<br>";
				$query .= "&$key=$value";
			}
		}
	}
	$req .= $query."\n";
	echo "<br>";
	
	ipnLog($req);
	
	// Remove trailing S from item if quantity is just 1
	$item = $_REQUEST['quantity']." ".$_REQUEST['item_name'];
	if($_REQUEST['quantity'] == 1) {
		$end = strlen($item)-1;
		$e = substr($item, $end);
		if($e == "s") {
			$item = substr($item, 0, $end);
		}
	}
	

	// STEP 1: Read POST data
	// reading posted data from directly from $_POST causes serialization 
	// issues with array data in POST
	// reading raw POST data from input stream instead. 
	$raw_post_data = $query;//$_SERVER['QUERY_STRING'];//file_get_contents('php://input');
	$raw_post_array = explode('&', $raw_post_data);
	$myPost = array();
	foreach ($raw_post_array as $keyval) {
	  $keyval = explode ('=', $keyval);
	  if (count($keyval) == 2)
		 $myPost[$keyval[0]] = urldecode($keyval[1]);
	}
	// read the post from PayPal system and add 'cmd'
	$req = 'cmd=_notify-validate';
	if(function_exists('get_magic_quotes_gpc')) {
	   $get_magic_quotes_exists = true;
	} 
	foreach ($myPost as $key => $value) {        
	   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
			$value = urlencode(stripslashes($value)); 
	   } else {
			$value = urlencode($value);
	   }
	   if($key=='entryID'){
		   $ProformsItemID = $value;
	   }
	   if($key=='txn_id'){
		   $txn_id = $value;
	   }
	   $req .= "&$key=$value";
	}
	
	//$pkg = Package::getByHandle('proforms');
	//$test_mode = $pkg->config('PAYPAL_TESTMODE');
	$test_mode = strpos($_REQUEST['payer_email'], "paypalsandbox") !== false;
	
	if($bypass) {
		echo "<b>BYPASS MODE</b>";
	}
	else
	if($test_mode) {
		echo "<b>SANDBOX MODE</b><br>";
		$pp_address = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	}
	else {
		$pp_address = 'https://www.paypal.com/cgi-bin/webscr';
	}
	 
	 
	if($bypass) {
		$res = "VERIFIED";
	}
	else {
		// STEP 2: Post IPN data back to paypal to validate
		$ch = curl_init($pp_address);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
	
	
		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		if( !($res = curl_exec($ch)) ) {
			error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);
	}
	
	echo $req."<br>";
	echo $res."<br>";

	if(strcmp ($res, "INVALID") == 0) {
		$error = "<b>INVALID TRANSACTION</b><br>";
	}
	else
	if(strcmp ($res, "VERIFIED") == 0) {
		// check the payment_status is Completed
		// check that txn_id has not been previously processed
		// check that receiver_email is your Primary PayPal email
		// check that payment_amount/payment_currency are correct
		// process payment
		if(!$ProformsItemID){
			$ProformsItemID = $_REQUEST['entryID'];
		}
		Loader::model('proforms_item','proforms');
		$pfo = ProFormsItem::getByID($ProformsItemID);
		$question_set = $pfo->asID;
		$as = AttributeSet::getByID($question_set);
		if($as) {
			$setAttribs = $as->getAttributeKeys();
			foreach ($setAttribs as $ak) {
				if($ak->getAttributeType()->getAttributeTypeHandle() == 'price_total'){
					$value = $pfo->getAttributeValueObject($ak);
					$ctl = $ak->getController();
					$amount = $ak->getController()->getRawValue($value->getAttributeValueID());
					$amount = money_format('%(#4n',$amount);
				
					if($_REQUEST['due_now']){
						$amount = money_format('%(#4n',$_REQUEST['due_now']);
					}
				
					//Log::addEntry($amount);
				
					$ctl->savePayment($value->getAttributeValueID(),$amount,null,$txn_id);
				
					Events::fire('proforms_item_payment',$pfo);
				}
			}
		}
		// Process successful transaction
		$userInfo = $item."<br>";
		$userInfo .= "$".$_REQUEST['payment_gross']."<br><br>";
		$userInfo .= $_REQUEST['first_name']." ".$_REQUEST['last_name']."<br>";
		$userInfo .= $_REQUEST['payer_email']."<br><br>";

		$info = "Status: ".$_REQUEST['payment_status']."<br><br>";
		$info .= $userInfo;
	
		if($_REQUEST['custom']) {
		// Save an entry in the database for the user's purchase (if a user was logged in)
			$info .= "User ID: ".$_REQUEST['custom']."<br><br>";
		
			Loader::model('user_profile');
			Loader::model('user_purchase');
			Loader::model('user_ticket');
			Loader::model('purchase_item');
			
			$p = new UserProfile();			
	
			$isNew = true;
			$up = new UserPurchase();
			$purchase = $up->getByTransactionID($_REQUEST['txn_id']);
			if($purchase == null) {
			// New transaction
				$purchase = new UserPurchase();
				echo "<b>New Transaction</b><br>";
			}
			else {
			// Updating an existing transaction
				$isNew = false;
				echo "<b>Updating Transaction</b><br>";
			}
			$purchase->uID = $_REQUEST['custom'];
			$purchase->uTransactionID = $_REQUEST['txn_id'];
			$purchase->uItem = $item;
			$purchase->uAmount = $_REQUEST['mc_gross'];
			$purchase->uStatus = $_REQUEST['payment_status'];
			$purchase->uQuantity = $_REQUEST['quantity'];
			$purchase->uEventID = $_REQUEST['item_number'];
		
			$dateDecode = urldecode($_REQUEST['payment_date']);
			echo "Date: ".$dateDecode."<br>";
			$date = strtotime($dateDecode);
			$purchase->uDate = date("Y-m-d H:i:s", $date);
			
			$purchase->uLocation = strtoupper(substr($_REQUEST['item_name'], 0, 2));
			
			// MEMBERSHIPS
			/*
			if(strpos($_REQUEST['item_name'], "Membership") !== false) {
				$purchase->uType = "Membership";
				Loader::model('user_membership');
				$um = UserMembership::getByUserID($_REQUEST['custom']);
				$create = false;
				if(!$um) {
					$create = true;
					$um = new UserMembership();
				}
				
				$locationValid = false;
				if($_REQUEST['item_name'] == "LA Membership") {
					$locationValid = true;
					$um->uLocation = "LA";
				}
				if(!$locationValid) {
					$error = "ERROR: The item location has not been implemented: ".$_REQUEST['item_name']."<br>";
				}
				else {
				// TODO: auto-assign card number
					//$um->uCardID		= $row['uCardID'];
					$um->uID			= $_REQUEST['custom'];
					$um->uStatus 		= "active";
			
					// Calculate expiration based on purchase date
					$m = date("m", $date);
					$d = date("d", $date);
					$y = date("Y", $date);
			
					$um->uExpirationDate 	= ($y+1)."-".$m."-".$d;
					$um->uLastPurchaseDate	= date("Y-m-d H:i:s", $date);
					$um->uTransactionID	= $_REQUEST['txn_id']; 
			
					if($create) {
						$um->uMemberSince = date("Y-m-d", time());
						UserMembership::create($um);
					}
					else {
						UserMembership::save($um);
					}
				}
			}
			else
			*/
			//if(strpos($_REQUEST['item_name'], "Ticket") !== false) {
				$purchase->uType = "Ticket";
				
			//}
			
			if($isNew) UserPurchase::create($purchase);
			else UserPurchase::save($purchase);
							
			$slack = Loader::helper("slack");
			$slack->send("Ticket Purchase ".$purchase->uLocation.":".$quantity." $".$purchase->uAmount." user:".$purchase->uID." (Paypal)");
			
			//$item = PurchaseItem::getByID("ticket");
			//$item->event_id = $purchase->uEventID;
			//$item->processOrder(1, $purchase->uTransactionID);
			UserTicket::generate($purchase->uQuantity, $purchase->uID, $purchase->uEventID, $purchase->uLocation, PurchaseItem::getNewExpirationDate($purchase->uQuantity));
			
		}
		echo $info;
	
		if($sendmail) {
			$mh = Loader::helper('mail');
			
			// Send admin notification
			$mh->from("admin@producerssocial.com");
			$mh->to("admin@producerssocial.com");
			$mh->bcc("stephen@walkerfx.com");
		
			$mh->addParameter('info', $info);
			$mh->load('payment_notification', 'proforms');
			$mh->setSubject("Payment: ".$item);
			$mh->sendMail(); 
			
			// Send user confirmation
			//$mh->to("stephen@walkerfx.com");
			$mh->to($_REQUEST['payer_email']);
			$mh->bcc("stephen@walkerfx.com");
			$mh->addParameter('info', $userInfo);
			$mh->load('payment_notification_user', 'proforms');
			$mh->setSubject("Confirmation: ".$item);
			$mh->sendMail(); 
		}
	}
	else {
		$error = "UNPROCESSED:".$res;
	}
	
	if($error) {
		echo $error;
		ipnLog("\nERROR:".$error."\n");
	}
?>
<?php
	$out = ob_get_contents();
	ob_end_clean();
	
	if(isset($_REQUEST['redirect'])) {
		header("Refresh:5; url=".$_REQUEST['redirect'], true, 303);
	}
	Loader::element('view_template', array('innerContent'=> $out));
?>