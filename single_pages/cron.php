<?php
$continue = false;

$user = Loader::helper('user');

$key = 'dk8-l8dwsa-dE0Ew0-eMj032';
if(isset($_REQUEST['type']) && $_REQUEST['key'] == $key) {
	$continue = true;
}
else 
if($user->isSuperAdmin()) {
	$continue = true;
}
if(!$continue) {
	//header("Location: ".BASE_URL);
	echo "UNAUTHORIZED\n";
	print_r($_REQUEST);
}
else {
	$out = "";
	if($_REQUEST['type'] == 'refresh_events') {
		$events = Loader::helper('events');
		$out = $events->updateEventInfo();
	}
	else
	if($_REQUEST['type'] == 'event_followup') {
		$events = Loader::helper('events');
		$out = $events->sendEventFollowups();
	}
	else
	if($_REQUEST['type'] == 'update_subscriptions') {
		$stripe = Loader::helper('stripe');
		$stripe->startup();
		$out = $stripe->updateAllSubscriptions();
	}
	else
	if($_REQUEST['type'] == 'renewal_reminders') {
		$stripe = Loader::helper('stripe');
		$stripe->startup();
		$out = $stripe->sendRenewalReminders(30);
		$out .= $stripe->sendRenewalReminders(3);
	}
	else
	if($_REQUEST['type'] == 'close_ondecks') {
		$events = Loader::helper('events');
		$out = $events->closeSessionsLeftOpen();
	}
	else
	if($_REQUEST['type'] == 'update_profile_has_avatar') {
		Loader::model("user_profile");
		$all = UserProfile::getAll($query);
		foreach($all as $p) {
			UserProfile::save($p);	
		}
	}
	else
	if($_REQUEST['type'] == 'reported_abuse') {
		
	}
	else {
		$out = "<div class='alert alert-white'>No jobs performed.</div>";	
	}
	/*
	$mh = Loader::helper('mail');
	$mh->from("admin@producerssocial.com");
	$mh->to("stephen@producerssocial.com");
	$mh->addParameter('out', $out);
	$mh->load("cron");
	$mh->sendMail();
	*/
}

if($user->isSuperAdmin()) {
	$out .= "<br class='clear'><br><a href='/manage/procedures'>Return to Manage Procedures</a>";	
}

echo $out;
//Loader::element('view_template', array('innerContent'=> $out));
?>