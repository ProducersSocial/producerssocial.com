<?php 
defined('C5_EXECUTE') or die("Access Denied.");

//print_r($_REQUEST);
//print_r($_FILES);

if($_REQUEST['feedback_title']) {
	$u = new User();
	if($u && $u->isLoggedIn()) {
		//echo "PRODCESSING...<br>";
		$mail = Loader::helper('mail');
	
		if($_FILES['feedback_file']) {
			$filename = '/feedback/'.date("Y-m-d_H:i:s", time())."_".$u->uID.'.jpg';
			$target = DIR_BASE.$filename;
			move_uploaded_file( $_FILES['feedback_file']['tmp_name'], $target);
		}
		
		$body = $_REQUEST['feedback_text']."\n\n";
		$body .= BASE_URL.$filename;//"<img src='".BASE_URL.$filename."'>";

		$userEmail = $u->uEmail;
		if($_REQUEST['feedback_sendcopy'] && $_REQUEST['feedback_email']) {
			$userEmail = $_REQUEST['feedback_email'];
			$mail->cc($userEmail, $u->uName);
		}		
		$mail->to("stephen@producerssocial.com", "Admin");
		$mail->from($userEmail, $u->uName);
		
		$mail->setSubject("Website Feedback:".$_REQUEST['feedback_title']);
		$mail->setBody($body);
		$mail->sendMail();
	
		echo "<br><br>Message Sent<br>";
	}
}
?>
