<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('blog/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 

	$a = new Area('Main');
	$a->display($c);
	
	echo "<div id='members_content_section'>";
	if($item) {
		echo $feature->displayItemPage($item, null, false, $item->type);	
	}
	else
	if(!$items) {
		echo "<div class='alert alert-warning'>There are no blog articles to display.</div>";	
	}
	else {
		if(isset($_REQUEST['list'])) {
			echo $feature->displaySummaryList($items, "All ".$items[0]->getTypeName(), $items[0]->type);
		}
		else {
			echo $feature->displayGrid($items, "All ".$items[0]->getTypeName(), $items[0]->type);
			//echo $feature->displaySlides($items, $items[0]->getTypeName(), $items[0]->type);
		}
	}
		
	if($user->isMember()) {
		echo "<div id='members_home_news'>";
		$a = new Area("MembersOnly");
		$a->display($c);
		echo "</div>";
	}				
	echo "</div>";
	
	if($itemCount > 1) {
		$paging = Loader::helper("paging");
		
		$url = "/blog/archive?p=1";
		$paging->show($page, $limit, $totalItems, $itemCount, $url, $query);
	}			
	Loader::element('blog/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>