<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

date_default_timezone_set("America/Los_Angeles");

Loader::model('userinfo');
Loader::model('user_profile');
Loader::model('on_deck');
Loader::model('on_deck_round');
Loader::model('user_on_deck');
Loader::model('user_on_deck_entry');
Loader::model('locations');
Loader::model('user_purchase');
Loader::model('user_ticket');
Loader::model('user_subscription');
Loader::model('facebook_event');
Loader::model('user_temp');

Loader::helper('ajax');

$events = Loader::helper('events');

$ajax					= null;
$cron					= null;
$odid 					= null;
$location 				= null;
$hostid					= null;

$cmd_enable_deck		= null;
$cmd_new 				= null;
$cmd_delete_deck 		= null;
$cmd_open_deck 			= null;
$cmd_close_deck	 		= null;
$cmd_start_deck			= null;
$cmd_end_deck			= null;
$cmd_clear_deck			= null;
$cmd_new_round			= null;
$cmd_take_break			= null;
$cmd_take_break_end		= null;

$cmd_next				= null;
$cmd_prev				= null;

$cmd_add_user			= null;
$cmd_remove_user		= null;
$cmd_move_top			= null;
$cmd_move_bottom		= null;
$cmd_move_up			= null;
$cmd_move_down			= null;
$cmd_new_entry			= null;
$cmd_add_me				= null;
$cmd_remove_me			= null;
$cmd_cross_off			= null;
$cmd_readd				= null;
$cmd_change_start_time	= null;
$cmd_change_end_time	= null;
$cmd_use_ticket			= null;

$cmd_settings			= null;
$set_timer				= null;
$set_timer_id			= null;
$set_timer_running		= 0;
$cmd_update_timer		= null;

$view					= null;
$filter_region 			= "";
$filter_alpha 			= "";

$custom_entry_result	= null;
$ondeck_settings_result	= null;

extract($_REQUEST);

//if(ADMIN) {
	//print_r($_REQUEST);
//}
if(isset($cmd_members_only)) {
	UserTemp::setValue('OnDeckMembersOnly', $cmd_members_only);
}
else {
	$cmd_members_only = UserTemp::getValue('OnDeckMembersOnly');
}
if(isset($member_search)) {
	UserTemp::setValue('OnDeckMemberSearch', $member_search);
}
else {
	$member_search = UserTemp::getValue('OnDeckMemberSearch');
}
//echo "member_search:".$member_search;


if($cron == "x0k2ns0i3e4kwo") {
// Auto-start the decks at the scheduled times 
	$opened = null;
	$all = OnDeck::getAll();
	foreach($all as $d) {
		$t = strtotime($d->EventDate);
		if($t > time()) {
			$x = $t - time();
			if($x < (60*5)) {
			// The event is within 5 minutes of starting
				$opened .= $d->Title."<br>";
				//echo "<h1>Opened Deck: ".$d->Title."</h1>";
				$d->IsOpen = 1;
				OnDeck::save($d);
			}
		}
	}
	if(!$opened) {
		echo "<div class='alert alert-warning'>No decks are ready to be opened yet</div>";
	}
	else {
		echo "<div class='alert alert-success' rel='fadeaway'>Opened Decks:".$opened."</div>";
	}
}

$host					= null;
$hostProfile			= null;
$round					= null;
$profile				= null;
$ondeck 				= null;
$event					= null;
$isLocationAdmin		= OnDeck::isLocationAdmin();
$isSuperAdmin 			= $isLocationAdmin == "HQ";

$u = new User();
if(!$u || !$u->isLoggedIn()) {
	$u = null;
}
if($u) {
	$profile = UserProfile::getByUserID($u->uID);
}

//ob_start();

if($isLocationAdmin && $cmd_new) {
	if($location) {
		echo "NEW:".$event_name;
		$ondeck 			= new OnDeck();
		$ondeck->Location 	= $location;
		$ondeck->Title 		= $event_name;
		$ondeck->IsPrivate 	= $IsPrivate;
		$ondeck->HostedBy 	= $u->uID;
		$ondeck->EventDate 	= date("Y-m-d H:i:s", time());
		
		$ondeck = OnDeck::create($ondeck);
		
		$url = '/ondeck?odid='.$ondeck->ID;
		header("Location: ".$url);
		die;
	}
	else {
		echo "<div class='alert alert-danger'>No location was provided. Please go back to the previous page and select a location.</div>";
	}
}
if($odid) {
	$ondeck = OnDeck::getID($odid);
	if(!$ondeck) {
		echo "<div class='alert alert-danger'>An error occurred trying to load the deck ($odid).</div>";
	}
	else {
		$location = Locations::getID($ondeck->Location);
		date_default_timezone_set($location->Timezone);
		
		if($ondeck->HostedBy) {
			$host = User::getByUserID($ondeck->HostedBy);
			$hostProfile = UserProfile::getByUserID($ondeck->HostedBy);
		}
		$event = FacebookEvent::getID($ondeck->EventID);
		$round = OnDeckRound::getByRoundNum($ondeck->ID, $ondeck->RoundNum);
		$usersOnDeck = $ondeck->getUsersOnDeck();
		
		if(!$profile || ($profile->uLocationAdmin != $ondeck->Location && $profile->uLocationAdmin != "HQ")) {
			$isLocationAdmin = false;
		}
		
		if($cmd_enable_deck) {
			if($location->DefaultHost) {
				$cmd_add_me = 1;
			}
		}
		
		$update = false;
		$userEdit = false;
		if($u) {
			if($cmd_use_ticket) {
				$unusedTickets = UserTicket::getAllAvailableTicketsForUser($u->uID);
				if(count($unusedTickets)) {
					$unusedTickets[0]->event_id = $event->ID;
					$unusedTickets[0]->used = date("Y-m-d H:i:s", time());
					$unusedTickets[0]->location = date("Y-m-d H:i:s", time());
					UserTicket::save($unusedTickets[0]);
					$cmd_add_me = 1;
				}
			}
			
			if($cmd_add_me) {
				$cmd_add_user = $u->uID;
				$userEdit = true;
			}
			else
			if($cmd_remove_me) {
				$user = $ondeck->getUser($u->uID);
				$cmd_remove_user = $user->ID;
				$userEdit = true;
			}
			else
			if($cmd_cross_off == 1) {
				$user = $ondeck->getUser($u->uID);
				$cmd_cross_off = $user->ID;
			}
			else
			if($cmd_readd == 1) {
				$user = $ondeck->getUser($u->uID);
				$cmd_readd = $user->ID;
			}
		}
		if($isLocationAdmin || $userEdit) {
			
			if($set_timer && $set_timer_id) {
				if($ondeck->HostID == $hostid) {
					$entry = UserOnDeckEntry::getID($set_timer_id);
					if($entry) {
						//$update = true;
						$location = Locations::getID($ondeck->Location);
						$p = explode(":",  $location->Countdown);
						$countdownTime = intval($p[0]) * 60 + intval($p[1]);
						$entry->Timer = $countdownTime - $set_timer;
						$entry->TimerRunning = $set_timer_running;
						UserOnDeckEntry::save($entry);
						//echo "TIMER:".$set_timer." isRunning:".$entry->TimerRunning;
					}
				}
			}
			else
			if($cmd_update_timer) {
				$update = true; // refresh only
			}
			else
			if($cmd_settings) {
				$update = true;
				if($set_hostid) {
					$ondeck->HostID = $set_hostid;
				}
				$ondeck->Title 		= $set_name;
				$ondeck->IsPrivate 	= $set_private ? 1 : 0;
				
				if(isset($set_location)) {
					$ondeck->Location 	= $set_location;
				}
				OnDeck::save($ondeck);
				
				$location = Locations::getID($ondeck->Location);
				$location->Countdown = $set_countdown;
				$location->Autostart = $set_autostart;
				if(!$location->Countdown) {
					$location->Countdown = "00:00";
				}
				$ondeck_settings_result = "LOCATION:".$location->ID." countdown:".$location->Countdown;
				Locations::save($location);
			}
			else
			if($cmd_add_user) {
				$update = true;
				if(!$ondeck->getUser($cmd_add_user)) {
					$user = $ondeck->addUser($cmd_add_user);
					if(!$user) {
						echo "<div class='alert alert-danger' rel='fadeaway'>Failed adding the user ($cmd_add_user) to the session.</div>";
					}
					else {
						echo "<div class='alert alert-success' rel='fadeaway'><a href='javascript:;' onclick='ps.ondeck.showList(".$user->OrderNum.");'>".$user->Name." is now #".$user->OrderNum." on deck.</a></div>";
						
						$tickets = UserTicket::getAllTicketsForUserAtEvent($user->UserID, $ondeck->EventID);
						if(!$tickets) {
							$tickets = UserTicket::getAllAvailableTicketsForUser($user->UserID, null, "ORDER BY expires ASC LIMIT 1");
							if($tickets) {
								$tickets[0]->event_id = $ondeck->EventID;
								UserTicket::save($tickets[0]);
							}
						}
					}
				}
				else {
					echo "<div class='alert alert-warning' rel='fadeaway'><a href='javascript:;' onclick='ps.ondeck.showList(".$user->OrderNum.");'>".$user->Name." is already in the session, #".$user->OrderNum.".</a></div>";
				}
			}
			else
			if($cmd_remove_user) {
				$update = true;
				$user = UserOnDeck::getID($cmd_remove_user);
				if(!$user) {
					echo "<div class='alert alert-danger' rel='fadeaway'>Failed removing the user ($cmd_remove_user) from the session.</div>";
				}
				else {
					UserOnDeck::delete($user);
					echo "<div class='alert alert-success' rel='fadeaway'><a href='javascript:;' onclick='ps.ondeck.showList(".$user->OrderNum.");'>".$user->Name." has been removed from the session.";
					$tickets = UserTicket::getAllTicketsForUserAtEvent($user->UserID, $ondeck->EventID);
					if($tickets) {
						$tickets[0]->event_id = '';
						UserTicket::save($tickets[0]);
						echo "<div class='alert alert-info'>REMOVED:".$tickets[0]->id."</div>";
					}
					else {
						echo "No tickets found.";	
					}
					echo "</a></div>";
				}
				$user = null;
			}
			else
			if($cmd_cross_off) {
				$update = true;
				$user = UserOnDeck::getID($cmd_cross_off);
				$user->getEntries();
				if($user->LastEntry) {
					$user->LastEntry->crossOff();
				}
				else {
					$entry = new UserOnDeckEntry();
					$entry->OnDeckID 		= $ondeck->ID;
					$entry->UserOnDeckID	= $user->ID;
					$entry->RoundNum 		= $ondeck->RoundNum;
					$entry->OrderNum 		= $user->OrderNum;
					$entry->CrossedOff 		= date("Y-m-d H:i:s", time());
					$entry->StartTime		= '0000-00-00 00:00:00';
					
					$entry = UserOnDeckEntry::create($entry);
				}
			}
			else
			if($cmd_readd) {
				$update = true;
				$user = UserOnDeck::getID($cmd_readd);
				$user->getEntries();
				if($user->Entries) {
					//echo "ENT:".count($user->Entries);
					foreach($user->Entries as $e) {
						$e->CrossedOff = '0000-00-00 00:00:00';
						UserOnDeckEntry::save($e);
					}
				}
			}
			else
			if($cmd_change_start_time) {
				$update = true;
				$parts = explode("_", $cmd_change_start_time);
				$entry = UserOnDeckEntry::getID($parts[0]);
				$time = strtolower($parts[1]);
				$pm = strpos($time, "pm") !== false;
				$time = str_replace("pm", "", $time);
				$time = str_replace("am", "", $time);
				$parts = explode(":", $time);
				$hours = $parts[0];
				$minutes = $parts[1];
				if($pm) $hours += 12;
				$entry->StartTime = date("Y-m-d", strtotime($entry->StartTime))." ".$hours.":".$minutes.":00";
				UserOnDeckEntry::save($entry);
			}
			else
			if($cmd_change_end_time) {
				$update = true;
				$parts = explode("_", $cmd_change_end_time);
				$entry = UserOnDeckEntry::getID($parts[0]);
				$time = strtolower($parts[1]);
				$pm = strpos($time, "pm") !== false;
				$time = str_replace("pm", "", $time);
				$time = str_replace("am", "", $time);
				$parts = explode(":", $time);
				$hours = $parts[0];
				$minutes = $parts[1];
				if($pm) $hours += 12;
				$entry->EndTime = date("Y-m-d", strtotime($entry->EndTime))." ".$hours.":".$minutes.":00";
				UserOnDeckEntry::save($entry);
			}
		}
		if($isLocationAdmin) {
			if($cmd_delete_deck) {
				$ondeck->delete();
				$ondeck = null;
				echo "<div class='alert alert-success' rel='fadeaway'>The session was deleted.</div>";
				header("Location:".View::url("/events"));
				die;
			}
			else {
				if($cmd_resume) {
					$update = true;
					$ondeck->IsOnBreak = 0;
					$ondeck->HostID = $hostid;
					$break = UserOnDeckEntry::getOne("OnDeckID=".$ondeck->ID." AND UserOnDeckID=-1 ORDER BY StartTime DESC");
					$break->end();
					
					if(!$round->isStarted()) {
						$round->start();
						//echo "<div class='alert alert-success'>Resuming round #".$ondeck->RoundNum."</div>";
					}
					else
					if($usersOnDeck['next']) {
						$ondeck->CurrentNum = $usersOnDeck['next']->OrderNum;
						$usersOnDeck['next']->start($ondeck->RoundNum);
						//echo "<div class='alert alert-success'>Resuming round #".$ondeck->RoundNum."</div>";
					}
					else {
						echo "<div class='alert alert-danger'>The end of the round has been reached!</div>";
					}
				}
				
				if($cmd_enable_deck) {
					$update = true;
					$ondeck->IsEnabled = 1;
				}
				else
				if($cmd_open_deck) {
					$update = true;
					$ondeck->IsEnabled = 1;
					$ondeck->IsOnBreak = 0;
					$ondeck->IsOpen = 1;
					$ondeck->HostedBy = $u->uID;
				}
				else
				if($cmd_close_deck) {
					$update = true;
					$ondeck->close();
				}
				if($cmd_start_deck) {
					if(count($usersOnDeck['all'])) {
						$update = true;
						$ondeck->IsLive = 1;
						$ondeck->StartTime = date("Y-m-d H:i:s", time());
						$ondeck->EndTime = "0000-00-00 00:00:00";
						$ondeck->CurrentNum = $usersOnDeck['current']->OrderNum;
						$ondeck->IsOnBreak = 0;
						$ondeck->HostID = $hostid;
					
						UserTicket::rsvpCommitTickets($ondeck->EventID);
						
						$usersOnDeck['current']->start($ondeck->RoundNum);
					
						$round = OnDeckRound::newRound($ondeck->ID, $ondeck->RoundNum);
						$round->start();
						echo "<div class='alert alert-success'>Session starting with #".$ondeck->CurrentNum."</div>";
					}
					else {
						echo "<div class='alert alert-danger'>The session cannot be started with an empty list.</div>";
					}
				}
				else
				if($cmd_end_deck) {
					$ondeck->end();					
					if($round) $round->end();
				}
				else
				if($cmd_clear_deck) {
					$update = false;
					$ondeck->clear();
				}
				else
				if($cmd_new_round) {
					$update = true;
					if($round) $round->end();
					if($usersOnDeck['current']) $usersOnDeck['current']->end();
					
					if(count($usersOnDeck['playable'])) {
						$ondeck->IsLive = 1;
						$ondeck->CurrentNum = 1;
						$ondeck->IsOnBreak = 0;
						$ondeck->RoundNum++;
						$round = OnDeckRound::newRound($ondeck->ID, $ondeck->RoundNum);
						$round->start();
						
						$usersOnDeck['all'][0]->start($ondeck->RoundNum);
						
						echo "<div class='alert alert-success'>Round #".$ondeck->RoundNum." started</div>";
					}
					else {
						echo "<div class='alert alert-danger'>There are no users remaining to start a new round.</div>";
					}
				}
				else
				if($cmd_take_break) {
					$update = true;
					if($usersOnDeck['current']) $usersOnDeck['current']->end();
					$ondeck->IsOnBreak = 1;
							
					$break = new UserOnDeckEntry();
					$break->OnDeckID 		= $ondeck->ID;
					$break->UserOnDeckID 	= -1;
					$break->RoundNum 		= $ondeck->RoundNum;
					$break->OrderNum 		= $ondeck->CurrentNum + 0.5;
					$break->StartTime		= date("Y-m-d H:i:s", time());
					$break = UserOnDeckEntry::create($break);
					
					if($ondeck->CurrentNum == $usersOnDeck['last']->OrderNum) {
						if($round) $round->end();
						if(count($usersOnDeck['playable'])) {
							$ondeck->CurrentNum = 1;
							$ondeck->RoundNum++;
							$round = OnDeckRound::newRound($ondeck->ID, $ondeck->RoundNum);
						
							//echo "<div class='alert alert-success'>Will begin round #".$ondeck->RoundNum." after the break</div>";
						}
						else {
							echo "<div class='alert alert-danger'>There are no users remaining to start a new round.</div>";
						}
					}
				}

				if($ondeck->IsLive) {
					if($cmd_next) {
						$update = true;
						if($usersOnDeck['current']) $usersOnDeck['current']->end();
						if($usersOnDeck['next']) {
							$usersOnDeck['next']->start($ondeck->RoundNum);
							$ondeck->CurrentNum = $usersOnDeck['next']->OrderNum;
							//echo "NEXT:".$ondeck->CurrentNum;
						}
					}
					else
					if($cmd_prev) {
						$update = true;
						if($usersOnDeck['current']) $usersOnDeck['current']->clearLastEntry();
						if($usersOnDeck['prev']) {
							$usersOnDeck['prev']->start($ondeck->RoundNum);
							$ondeck->CurrentNum = $usersOnDeck['prev']->OrderNum;
							if($ondeck->CurrentNum < 1) $ondeck->CurrentNum = 1;
						}
					}
				}
				
				if($cmd_move_top) {
					$update = true;
					$user = UserOnDeck::getID($cmd_move_top);
					if($user) {
						$user->OrderNum = -1;
						UserOnDeck::save($user);
						$ondeck->renumber();
					}
				}
				else
				if($cmd_move_bottom) {
					$update = true;
					$user = UserOnDeck::getID($cmd_move_bottom);
					if($user) {
						$end = UserOnDeck::getCount($ondeck->ID);
						$user->OrderNum = $end  + 1;
						UserOnDeck::save($user);
						$ondeck->renumber();
					}
				}
				else
				if($cmd_move_up) {
					$update = true;
					$user = UserOnDeck::getID($cmd_move_up);
					if($user && $user->OrderNum > 0) {
						if($prev = $ondeck->getUserByOrderNum($user->OrderNum - 1)) {
							$prev->OrderNum = $user->OrderNum;
							UserOnDeck::save($prev);
						}
						$user->OrderNum = $user->OrderNum - 1;
						UserOnDeck::save($user);
						$ondeck->renumber();
					}
				}
				else
				if($cmd_move_down) {
					$update = true;
					$user = UserOnDeck::getID($cmd_move_down);
					if($user->OrderNum == $ondeck->CurrentNum) {
						$usersOnDeck['current']->clear($ondeck->RoundNum);
						$usersOnDeck['next']->start($ondeck->RoundNum);
					}
					if($user && $user->OrderNum < count($usersOnDeck['all'])) {
						if($next = $ondeck->getUserByOrderNum($user->OrderNum + 1)) {
							$next->OrderNum = $user->OrderNum;
							UserOnDeck::save($next);
						}
						$user->OrderNum = $user->OrderNum + 1;
						UserOnDeck::save($user);
						$ondeck->renumber();
					}
				}
				
				if($cmd_open_deck) {
					$all = UserTicket::getTicketForEvent($event->ID, "autosignup != 0", "ORDER BY autosignup ASC");
					if($all) {
						foreach($all as $t) {
							if(!$ondeck->getUser($t->user_id)) {
								$user = $ondeck->addUser($t->user_id);
							}
						}
					}
				}
	
				if($cmd_new_entry) {
					$update = true;
					
					$valc = Loader::helper('concrete/validation');
					
					$err = null;
					if(strlen($new_entry_name) < USER_USERNAME_MINIMUM) {
						$err = 'Your username must be at least '.USER_USERNAME_MINIMUM.' characters long.';
					}
					if(strlen($new_entry_name) > USER_USERNAME_MAXIMUM) {
						$err = 'Your username cannot be more than '.USER_USERNAME_MAXIMUM.' characters long.';
					}
// 					if(strlen($new_entry_name) >= USER_USERNAME_MINIMUM && !$valc->username($new_entry_name)) {
// 						if(USER_USERNAME_ALLOW_SPACES) {
// 							$err = 'A username may only contain letters, numbers and spaces.';
// 						} 
// 						else {
// 							$err = 'A username may only contain letters or numbers.';
// 						}
// 					}
					if(!$valc->isUniqueUsername($new_entry_name) || $new_entry_name == USER_SUPER) {
						$err = "The username ".$new_entry_name." already exists. Please choose another.";
					}		
					
					// Check for an existing account with the same email
					$eu = User::getByEmail($new_entry_email);
					if($eu) {
						$isOnDeck = false;
						foreach($usersOnDeck as $uod) {
							if($uod->UserID == $eu->uID) {
								$isOnDeck = $uod;
								break;
							}
						}
						$custom_entry_result = "<div class='alert alert-warning'>";
						if($isOnDeck) {
							$custom_entry_result .= "You are already on the list as ".$uod->Name." #".$uod->OrderNum;
							$custom_entry_result .= "<br class='clear'>";
						}
						else {
							$custom_entry_result .= "An account already exists using this email. To use that account please tap the profile image below, or enter a different email to register again.";
							$custom_entry_result .= "<br class='clear'>";
							$custom_entry_result .= "<a class='ondeck_user_add center' ".ajax('users', 'cmd_add_user', $eu->uID).">";
						}

						$ep = UserProfile::getByUserID($eu->uID);
						$img = $ep ? $ep->getAvatar(false) : UserProfile::getDefultAvatar(false);
						$custom_entry_result .= "	<img class='ondeck_user_add_img clear' src='".$img."'>";
						$custom_entry_result .= "	<div class='ondeck_user_add_name'>".$ep->uArtistName."</div>";
						$custom_entry_result .= "</a>";
						
						$custom_entry_result .= "<br class='clear'>";
						$custom_entry_result .= "</div>";
					}
					else {
						$userHelper = Loader::helper('concrete/user');
						$password = substr(uniqid(), 7);
						$userHelper->validNewPassword($password,$e);
						if(!$password) {
							$err = 'Invalid password:'.$password;
						}
					
						if(!$err) {
						
							$data = array();
							$data['uName'] = $new_entry_name;
							$data['uEmail'] = $new_entry_email;
							$data['uPassword'] = $password;
							$data['uPasswordConfirm'] = $password;
							$data['uLocation'] = $ondeck->Location;
							$data['uTimezone'] = Locations::getTimezoneName($ondeck->Location);
			
							$ui = new UserInfo();
							$newUser = UserInfo::add($data, array(UserInfo::ADD_OPTIONS_SKIP_CALLBACK));
							if(!is_object($newUser)) $err = "An error occurred registering this new account. Please try again";
						
							if(!$err) {
								Loader::model('user_attributes');
								
								// Send admin email notification of new registration
								$mh = Loader::helper('mail');
								if(EMAIL_ADDRESS_REGISTER_NOTIFICATION) {
									$mh->to(EMAIL_ADDRESS_REGISTER_NOTIFICATION);
								} else {
									$adminUser = UserInfo::getByID(USER_SUPER_ID);
									if (is_object($adminUser)) {
										$mh->to($adminUser->getUserEmail());
									}
								}
				
								$mh->addParameter('uID',    $newUser->getUserID());
								$mh->addParameter('user',   $newUser);
								$mh->addParameter('uName',  $newUser->getUserName());
								$mh->addParameter('uEmail', $newUser->getUserEmail());
								$mh->addParameter('uLocation', $ondeck->Location);
								$attribs = UserAttributeKey::getRegistrationList();
								$attribValues = array();
								foreach($attribs as $ak) {
									$attribValues[] = $ak->getAttributeKeyDisplayName('text') . ': ' . $newUser->getAttribute($ak->getAttributeKeyHandle(), 'display');		
								}						
								$mh->addParameter('attribs', $attribValues);
				
								if (defined('EMAIL_ADDRESS_REGISTER_NOTIFICATION_FROM')) {
									$mh->from(EMAIL_ADDRESS_REGISTER_NOTIFICATION_FROM,  t('Website Registration Notification'));
								} else {
									$adminUser = UserInfo::getByID(USER_SUPER_ID);
									if (is_object($adminUser)) {
										$mh->from($adminUser->getUserEmail(),  t('Website Registration Notification'));
									}
								}
								$mh->load('user_register');
								//$mh->sendMail();
											
								$slack = Loader::helper("slack");
								$m = "On Deck: New User Registration (".strtoupper($ondeck->Location)."): ".$newUser->getUserName()." ".$newUser->getUserEmail();
								$slack->send($m);

								$uHash = $newUser->setupValidation();
					
								// Send new user activation email
								$mh = Loader::helper('mail');
								if (defined('EMAIL_ADDRESS_VALIDATE')) {
									$mh->from(EMAIL_ADDRESS_VALIDATE,  t('Validate Email Address'));
								}
								$mh->addParameter('username', $newUser->getUserName());
								$mh->addParameter('password', $password);
								$mh->addParameter('uEmail', $newUser->getUserEmail());
								$mh->addParameter('uHash', $uHash);
								$mh->to($newUser->getUserEmail());
								//$mh->cc(EMAIL_ADDRESS_REGISTER_NOTIFICATION);
								$mh->load('ondeck_register_email');
								$mh->sendMail();
							
								$newProfile = new UserProfile();
								$newProfile->uID = $newUser->getUserID();
								$newProfile->uArtistName = $new_entry_name;
								$newProfile->uLocation = $ondeck->Location;
								UserProfile::create($newProfile);
							
								$user = new UserOnDeck();
								$user->Name = $newUser->getUserName();
								$user->Email = $newUser->getUserEmail();
								$user->UserID = $newUser->getUserID();
								$user->OnDeckID = $ondeck->ID;
								$user->OrderNum = UserOnDeck::getCount($ondeck->ID) + 5;
								$user = UserOnDeck::create($user);
				
								// Reload to get the updated count
								$ondeck->renumber();
								$user = UserOnDeck::getID($user->ID);
				
								$custom_entry_result = "<div class='alert alert-success'><h4>".$user->Name." is #".$user->OrderNum." on the list. Please check your email to confirm your new account.</h4></div>";
							}
						}
						if($err) {
							$custom_entry_result = "<div class='alert alert-danger'>".$err."</div>";
						}
					}
				}		
			}
		}
		
		if($update && $ondeck) {
			OnDeck::save($ondeck);
		}
	}
}

$view_round	= $ondeck->RoundNum;
if(isset($_REQUEST['view_round'])) {
	$view_round = $_REQUEST['view_round'];
}
	
$thisUserIsOnList = false;
$thisUserOnDeck = null;
if($u && $ondeck) {
	$thisUserOnDeck = $ondeck->getUser($u->uID);
	if($thisUserOnDeck) {
		$thisUserIsOnList = true;
		$thisUserOnDeck->getEntries();
	}
}

$unusedTickets = null;
$tickets = null;
if($u && $event) {
	$tickets = UserTicket::getAllTicketsForUserAtEvent($u->uID, $event->ID);
	$unusedTickets = UserTicket::getAllAvailableTicketsForUser($u->uID);
	
	if(!count($tickets)) $tickets = null;
	if(!count($unusedTickets)) $unusedTickets = null;
}

if($ondeck) {
	$usersOnDeck = $ondeck->getUsersOnDeck(); // Reload in case anything changed
}

$userStatus = null;
if($thisUserIsOnList || $ondeck->IsOpen) {
	$s = null;
	$alert = "success";
	if($thisUserIsOnList) {
		if($usersOnDeck['current'] && $thisUserOnDeck->ID == $usersOnDeck['current']->ID) {
			$s .= "You are up!";
		}
		else
		if($usersOnDeck['next'] && $thisUserOnDeck->ID == $usersOnDeck['next']->ID) {
			$s .= "You are next up!";
			$alert = "info";
		}
		else
		if($thisUserOnDeck->IsCrossedOff) {
			$s .= "You are crossed off the list <a class='button center' ".ajax('session', 'cmd_readd', 1)."><i class='fa fa-repeat' aria-hidden='true'></i> Re-Add Me</a>";
		}
		else
		if($thisUserOnDeck->OrderNum > $ondeck->CurrentNum) {
			$alert = "info";
			$s .= "You are currently #".($thisUserOnDeck->OrderNum);
			//if($ondeck->Rounds <= 1) {
			//	$s .= " <a class='button center' ".ajax('session', 'cmd_remove_me', 1)."><i class='fa fa-times' aria-hidden='true'></i> Remove Me</a>";
			//	$alert = "warning";
			//}
		}
		else 
		if($thisUserOnDeck->HasEntries && !$isLocationAdmin) {
			$s .= "Please cross off the list ifyou are done <a class='button center' ".ajax('session', 'cmd_cross_off', 1)."><i class='fa fa-strikethrough' aria-hidden='true'></i> Cross Off</a>";
			$alert = "warning";
		}
	}
	else
	if($ondeck->IsOpen) {
		$alert = "danger";
		$s .= "<i>You are not on the list yet </i>";
		if((($tickets && count($tickets) > 0) || (!$event || !$event->IsTicketRequired()) || $isLocationAdmin) && !$thisUserIsOnList) {
			$s .= "<a class='button small' ".ajax('session', 'cmd_add_me', 1)."><i class='fa fa-plus' aria-hidden='true'></i>&nbsp; Add Me</a>";
		}
		else 
		if($event && $event->IsTicketRequired()) {
			$alert = "warning";
			$s .= "<br class='clear'>A ticket is required to sign-up for this event.<br><br><a class='button' href='".View::url("/eventinfo?id=".$ondeck->EventID)."'><i class='fa fa-ticket' aria-hidden='true'></i>Purchase Ticket</a><br><br>";
		}
		$s .= "";
	}
	if($s) {
		if(!$isLocationAdmin || !$ondeck->IsLive) {
			$userStatus .= "<div class='ondeck_user_status'>";
				$userStatus .= "<div class='alert alert-".$alert." center'>";
				$userStatus .= "<a class='iconbutton right' alt='Dismiss Status' href='javascript:;' onclick='ps.ondeck.dismissStatus();'><i class='fa fa-times' aria-hidden='true'></i></a>";
				$userStatus .= $s;
				$userStatus .= "</div>";
			$userStatus .= "</div>";//ondeck_user_status
		}
	}
}

$addme = null;
if($profile) {
	if(($tickets || (!$event || !$event->IsTicketRequired()) || $isLocationAdmin) && !$thisUserIsOnList) {
		$addme .= "<div class='alert alert-white center' style='width:400px; max-width:100%;'>";
		$addme .= "You are not on the list yet.<br>Tap the button below to add yourself.";
		$addme .= "<div class='alert alert-info' style='width:160px; margin:auto; float:none'>";
		$addme .= "<a class='clear center' ".ajax('list', 'cmd_add_me', 1).">";
		$addme .= $profile->getAvatar(true, false, false);
		$addme .= "</a>";
		$addme .= "<a class='clear button center' ".ajax('list', 'cmd_add_me', 1)."><i class='fa fa-plus' aria-hidden='true'></i> Add Me</a>";
		$addme .= "</div>";
		$addme .= "</div>";
		$addme .= "<br class='clear'>";
	}
}

//-----------------------------------------------------------------------------------------------------------------------------
//:: Prepare data for views
//-----------------------------------------------------------------------------------------------------------------------------
$u = new User();
if(!$u || !$u->isLoggedIn()) {
	$u = null;
}
if(!$u) {
	$url = View::url("/login");
	header("Location:".$url);
}
else
if($ondeck) {
	$data = array();
	$data['u'] 					= $u;
	$data['this'] 				= $this;
	$data['addme'] 				= $addme;
	$data['host'] 				= $host;
	$data['hostProfile'] 		= $hostProfile;
	$data['profile'] 			= $profile;
	$data['tickets'] 			= $tickets;
	$data['unusedTickets']		= $unusedTickets;
	$data['location'] 			= $location;
	$data['isSuperAdmin'] 		= $isSuperAdmin;
	$data['isLocationAdmin'] 	= $isLocationAdmin;
	$data['userStatus'] 		= $userStatus;
	$data['ondeck'] 			= $ondeck;
	$data['round'] 				= $round;
	$data['event'] 				= $event;
	$data['events'] 			= $events;
	$data['usersOnDeck'] 		= $usersOnDeck;
	$data['countdown']			= $location ? $location->Countdown : "10:00";
	$data['count'] 				= count($data['usersOnDeck']['all']);
	$data['countPlayable'] 		= count($data['usersOnDeck']['playable']);
	$data['allUsers'] 			= UserOnDeck::getAllUsers();
	$data['view_round']			= $view_round;
	$data['members_only']		= $cmd_members_only;
	$data['member_search']		= $member_search;
	
	$data['locations'] 			= Locations::getList();

	$data['hasEnded'] = false;
	if($ondeck->EndTime != "0000-00-00 00:00:00") {
		$data['hasEnded'] = true;
	}

	$availableUsers = array();
	foreach($data['allUsers'] as $us) {
		$isOn = false;
		foreach($data['usersOnDeck']['all'] as $du) {
			if($du->UserID == $us->UserID) {
				$isOn = true;
				break;
			}
		}
		if(!$isOn) {
			$availableUsers[] = $us;
		}
	}
	
	if(!$view) {
		if($ondeck->IsOpen) {
			$view = "list";
		}
		else
		if(!$ondeck->isStarted()) {
			$view = "users";
		}
		else {
			$view = "session";
		}
	}
	$data['view'] 				= $view;

	$data['thisUserIsOnList'] 	= $thisUserIsOnList;
	$data['thisUserOnDeck'] 	= $thisUserOnDeck;
	$data['availableUsers'] 	= $availableUsers;
	$data['filter_alpha']		= $filter_alpha;
	$data['filter_region']		= $filter_region;
	$data['data']				= $data;
	$data['url']				= $this->url('/ondeck?odid='.$ondeck->ID);
	
}


//-----------------------------------------------------------------------------------------------------------------------------
//:: Handle request – either responding to ajax or full page
//-----------------------------------------------------------------------------------------------------------------------------
if(!$data) {
	echo "NOT LOADED";
}
else
if($ajax == "profile") {
	$u = Loader::helper('user');
	$u->displaySummary($_REQUEST['uid'], false, null, null, null, null, "user_profile_container", true, false);
}
else
if($ajax == "refreshid") {
	echo $ondeck->RefreshID;
}
else
if($ajax == "member_search") {
	UserTemp::setValue("OnDeckMemberSearch", $member_search);
	echo "SET:".$member_search;
}
else
if($ajax == "custom_entry") {
	echo $custom_entry_result;
	die;
}
else
if($ajax == "ondeck_settings") {
	echo $ondeck_settings_result;
	die;
}
else
if($ajax == "set_timer") {
	die;
}
else
if($ajax == "pin") {
	echo $profile->uAdminPin;
	
	if(isset($_REQUEST['hostid'])) {
		$ondeck->HostID = $_REQUEST['hostid'];
		OnDeck::save($ondeck);
	}
}
else
if($ajax == "presales") {
	$ticketHelper = Loader::helper('tickets');
	echo $ticketHelper->showPresales($event);
}
else
if($ajax) {
	Loader::element("ondeck/".$ajax, $data); 
}
else {
	$this->inc('elements/header.php');
	if(!$isLocationAdmin) {
		$ticketHelper = Loader::helper('tickets');
		$ticketHelper->navbarTicketDisplay($u->uID, 'navbar-tickets-ondeck');
	}
	else {
		echo "<a class='navbar_admin_status ondeck' href='/manage'>".$profile->uLocationAdmin." Admin</a>";
	}
	echo "<div id='ondeck' class='location_style_".strtolower($ondeck->Location)."'>";
		echo "<table id='ondeck_table'>";
			echo "<tr>";
			
			echo "<td id='ondeck_nav'>";
			echo "<div id='ondeck_nav_container'>";
			Loader::element('ondeck/nav', $data); 
			echo "</div>";
			echo "</td>";
			
			echo "<td id='ondeck_content'>";
				
				Loader::element('system_errors', array('error' => $error));
			
// 				if(!$ondeck->IsOpen && !$ondeck->IsLive && !$isLocationAdmin) {
// 					$time = strtotime($ondeck->EventDate);
// 					$date = date("l F dS", $time)." at ".date("g:ia", $time);
// 					if($ondeck->EndTime != "0000-00-00 00:00:00" && time() > strtotime($ondeck->EndTime)) {
// 						echo "<div class='clear alert alert-info'><i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp; This <a href='".$this->url("/eventinfo?id=".$ondeck->EventID)."'>event</a> is no longer open for sign-ups.</div>";
// 					}
// 					else {
// 						echo "<div class='clear alert alert-info'><i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp; This <a href='".$this->url("/eventinfo?id=".$ondeck->EventID)."'>event</a> will be open for sign-ups ".$date."</div>";
// 					}
// 				}
// 				else {
					echo "<table id='ondeck_table_inner'>";
						echo "<tr id='ondeck_table_inner_top'>";
						echo "<td>";
							echo "<input type='hidden' id='ondeck_refreshid' value='".$ondeck->RefreshID."'>";
							echo "<input type='hidden' id='ondeck_view' value='".$view."'>";
							echo "<input type='hidden' id='ondeck_url' value='".$this->url("/ondeck?odid=".$ondeck->ID)."'>";
							echo "<input type='hidden' id='ondeck_islive' value='".($ondeck->IsLive ? 1 : 0)."'>";
	
							$hide = "class='hide'";
							$show = "class='show'";
					
							echo "<div id='ondeck_title_container'>";
							Loader::element('ondeck/title', $data); 
							echo "</div>";
						echo "</td>";
						
						echo "<tr id='ondeck_table_inner_bot'>";
						echo "<td>";
							echo "<div id='ondeck_session_container' ".($view == "session" ? $show : $hide).">";
							Loader::element('ondeck/session', $data); 
							echo "</div>";
					
							echo "<div id='ondeck_list_container' ".($view == "list" ? $show : $hide).">";
							Loader::element('ondeck/list', $data); 
							echo "</div>";
	
							echo "<div id='ondeck_users_container' ".($view == "users" ? $show : $hide).">";
							Loader::element('ondeck/users', $data); 
							echo "</div>";
						echo "</td>";
// 				}
				
			echo "</td>";
		
		echo "</tr>";
		echo "</table>";
		
	echo "</div>";
	
	echo "<div id='ondeck_panels_container'>";
	Loader::element("ondeck/panels", $data); 
	Loader::element("ondeck/settings", $data); 
	echo "</div>";
	
	$js = "<script src='".$this->getThemePath()."/js/producers_ondeck.js?v=".REVISION."'> type='text/javascript'></script>";

	$this->inc('elements/footer.php', array('showNav'=>!$isLocationAdmin, 'extra'=>$js));
}
?>

