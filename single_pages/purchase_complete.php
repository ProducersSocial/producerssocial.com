<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();

	Loader::model('facebook_event');
	Loader::model('user_profile');
	Loader::model('user_purchase');
	
	if($_REQUEST['item_number'] == 144) {
		echo "<h1>Thank you for your donation!</h1>";
		echo "<p>We are truly grateful for your support.</p>";
	}
	else {
		echo "<h1>Thank you for your purchase!</h1>";
		echo "<p>Your transaction has been completed, and a receipt for your purchase has been emailed to you.</p>";
		echo "<div class='alert alert-info'>Please arrive early to the event and check in to sign-up on the list.</div>";
		
		//http://producerssocial.com/purchase_complete?tx=6T201961BJ288113F&st=Completed&amt=1.00&cc=USD&cm=&item_number=144
		
		//echo "REQUEST:<br>";
		//print_r($_REQUEST);
		//echo "<br>";
		//echo "<br>";
	
		//echo "_POST:<br>";
		//print_r($_POST);
		//echo "<br>";
		//echo "<br>";
	
		$u = new User();
		if($u && $u->isLoggedIn()) {
			echo "<p>You purchased the following items:</p>";
			
			$tickets = Loader::helper('tickets');
			$tickets->displayPurchases($e);
		}
	}
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template_center', array('innerContent'=> $out));
?>