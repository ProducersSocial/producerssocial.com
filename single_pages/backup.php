<?php

define('BACKUP_DIR', '/home/dh_w4724d/producerssocial.com/backups/');
define('DB_HOST', 'mysql.producerssocial.com');
define('DB_PASS', 'S822FutD');
define('DB_LOGIN', 'dh_w4724d');
define('DB_DATABASE', 'producerssocial');
define('MYSQLDUMP', 'mysqldump');
define('MYSQLBIN', 'mysql');
define('GZIP', 'gzip');
define('UNGZIP', 'gzip -cd');
define('BACKUP_EXT', 'gz');

if(isset($_REQUEST['migrate'])) {
	if($_REQUEST['file']) {
		define('NL', "\n");
		$filename = $_REQUEST['file'] ? $_REQUEST['file'] : "export.sql";
		$contentType = "application/x-tar";
		header('Content-Disposition: inline; filename="'.$filename.'"');
		header ("content-type: ".$contentType);
		header('Content-Disposition: attachment; filename="'.$filename.'"');
	}
	else {
		define('NL', "<br>");	
	}
	
	migrate();
}
else {
	backup();
}

function backup() 
{
	$file 		= null;
	$tables 	= null;
	$msg 		= null;
	$start		= microtime();

	$oldmask = umask(0);
	$directory = BACKUP_DIR.date('Y-m', time())."/";
	if(!is_dir($directory)) {
		if(!mkdir($directory, 0777, $recursive=true)) {
			die("Failed creating directory:".$directory);
		}
	}

	@set_time_limit(0);
	if(@is_dir($directory) && @is_writable($directory)) {
		$options = $tables;
		$file = 'ps_'.date('Y-m-d_His').'.sql';
		$file = preg_replace ('/"/','\"',$file);
		$action = MYSQLDUMP." -h ".DB_HOST." --password=".DB_PASS." --user=".DB_LOGIN." ".DB_DATABASE;
		if(GZIP) {
			$file .= ".".BACKUP_EXT;
			$action .= "| ".GZIP;
		}
		$action .= " > \"".$directory.$file."\"";
		echo $action;
		
		system($action);

		$msg = "The database has been backed up.";
		$subject = "Backup Created";
	}
	else {
		$msg = "ERROR: Failed creating database backup.";
		$subject = "Backup Failed";
	}

	umask($oldmask);

	$end = microtime();
	$total = $end - $start;
	
	echo "<h1>".$subject."</h1>".NL;
	echo $msg.NL;
}

function getFields($fields)
{
	$f = null;
	foreach($fields as $i) {
		if($f) $f .= ", ";
		$f .= "`".$i."`";	
	}
	return $f;
}

function getExport($table, $fields, $groupBy=20, $skip=null, $truncate=true, $op="INSERT")
{
	$db = Loader::db();
	$export = "";
	
	if($truncate) {
		$export = "TRUNCATE `".$table."`;".NL;
	}
	
	if($groupBy < 5) {
		$groupBy = 5;	
	}
	
	$count = 0;
	$q = "SELECT count(*) as count FROM `".$table."`";
	if($r = $db->query($q)) {
		$count = $r->fetchRow()['count'];
	}
	echo NL.NL;
	echo "-- ".$table." Count:".$count.NL;
	
	$x = 0;
	while($x < $count) {
		$export .= NL.$op." INTO `".$table."` (".getFields($fields).") VALUES".NL;
		$q = "SELECT * FROM `".$table."` LIMIT ".$x.", ".$groupBy;
		//echo "-- ".$x.":".$q.NL;
		
		$values = null;
		$r = $db->query($q);
		while($row = $r->fetchRow()) {
			$canExport = true;
			if($skip) {
				foreach($skip as $id => $val) {
					if($row[$id] == $val) {
						$canExport = false;
						break;
					}
				}
			}
			if($canExport) {
				if($values) $values .= ",".NL;
				$values .= "(";
				$items = null;
				foreach($fields as $i) {
					if($items) $items .= ", ";
					$items .= $db->quote($row[$i]);
				}
				$values .= $items;
				$values .= ")";
			}
		}
		$values .= ";".NL;
		$export .= $values;
		$x += $groupBy;
	}
	
	return $export;
}

function migrate()
{
	$db = Loader::db();
	
	/*
	$fields = array("id", "type", "event_id", "name", "price", "status", "created", "ending");
	$export = getExport("PurchaseItems", $fields);
	echo $export;
	
	$fields = array("uID", "uName", "uEmail", "uPassword", "uLocation", "uIsActive", "uIsValidated", 
		"uIsFullRecord", "uDateAdded", "uHasAvatar", "uLastOnline", "uLastLogin", "uLastIP", "uPreviousLogin", 
		"uNumLogins", "uTimezone", "uDefaultLanguage");
	$export = getExport("Users", $fields, 20, array('uID'=>1), false, "REPLACE");
	echo $export;
	
	echo "REPLACE INTO `Users` (`uID`, `uName`, `uEmail`, `uPassword`, `uIsActive`, `uIsFullRecord`, `uIsValidated`, `uDateAdded`, `uLastPasswordChange`, `uHasAvatar`, `uLastOnline`, `uLastLogin`, `uPreviousLogin`, `uNumLogins`, `uLastAuthTypeID`, `uLastIP`, `uTimezone`, `uDefaultLanguage`, `uIsPasswordReset`) VALUES";
	echo "(1, 'admin', 'stephen@producerssocial.com', '$2a$12$ry1LcHPCefGtM2FrFO96IO.sQOz5nHR4SJzuxrAxqGB2cq3OaPkj6', 1, 1, 1, '2018-01-31 09:17:52', '2018-01-31 09:17:52', 0, 1517593519, 1517509732, 1517450127, 3, 1, '48cb7523', NULL, NULL, 0);";
	
	$fields = array("ID", "OnDeckID", "Name", "Description", "ImageURL", "StartTime", "EndTime", "Timezone", "Location", "AddressID", "AddressName", "AddressStreet", "AddressCity", "AddressState", "AddressZip", "AddressCountry", "TicketPrice", "TicketMode", "TicketLimit", "Created", "Modified");
	$export = getExport("FacebookEvents", $fields);
	echo $export;
	
	$fields = array("id", "handle", "name", "link", "status", "ordernum", "created");
	$export = getExport("Sponsors", $fields, 50);
	echo $export;
	
	$fields = array("id", "user_id", "item_id", "type", "liked", "date");
	$export = getExport("UserLikes", $fields, 50);
	echo $export;
	
	$fields = array("ID", "UserID", "OnDeckID", "Name", "Email", "OrderNum", "InviteSent", "Created");
	$export = getExport("UserOnDeck", $fields, 50);
	echo $export;
	
	$fields = array("ID", "OnDeckID", "UserOnDeckID", "RoundNum", "OrderNum", "Timer", "TimerRunning", "StartTime", "EndTime", "CrossedOff");
	$export = getExport("UserOnDeckEntry", $fields, 50);
	echo $export;
	
	$fields = array("uID", "uHandle", "uCustomerID", "uArtistName", "uFullName", "uHide", "uImageType", "uSummary", "uBio", "uStyleTags", "uEmail", "uMobile", "uWebsite", "uBandcamp", "uSoundcloud", "uFacebook", "uTwitter", "uInstagram", "uYoutube", "uSesh", "uEmbed", "uFeaturedTrack", "uEdited", "uSendReminders", "uNewsletter", "uReported", "uIsMember", "uHasAvatar", "uLocation", "uLocationAdmin", "uAdminPin");
	$export = getExport("UserProfiles", $fields, 50);
	echo $export;
	
	$fields = array("upID", "uID", "uTransactionID", "uEventID", "uLocation", "uItem", "uQuantity", "uAmount", "uStatus", "uDate", "uNote");
	$export = getExport("UserPurchases", $fields, 50);
	echo $export;
	
	$fields = array("ID", "UserID", "Name", "Value");
	$export = getExport("UserTemp", $fields, 50);
	echo $export;
	*/
	$fields = array("id", "user_id", "event_id", "autosignup", "location", "used", "created", "expires");
	$export = getExport("UserTickets", $fields, 50);
	echo $export;
	/*
	$fields = array("id", "user_id", "event_id", "autosignup", "location", "used", "created", "expires");
	$export = getExport("UserSubscriptions", $fields, 50);
	echo $export;
	*/
}
?>