<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	$user = Loader::helper('user');
	Loader::element('members/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	

	$a = new Area('Main');
	$a->display($c);
	
	if(!$user->isMember() || $user->isMasterAdmin()) {
		echo "<div id='members_welcome' class='alert alert-white'>";
		echo "<a class='right' href='javascript:;' onclick='return ps.hide(\"members_welcome\", \"slow\");'><i class='fa fa-times' aria-hidden='true'></i></a>";
		echo "<h3>Premium Member Benefits</h3>";
		echo "Every month we publish new content created just for our members, including sample packs, ";
		echo "tutorials, and special offers from our partners and sponsors. Become a <a href='/membership'>Premium Member</a> now for full access!";
		echo "<br class='clear'>";
		echo "<a href='/membership' class='button medium left clear pad-top' ><i class='fa fa-id-card-o' aria-hidden='true'></i> Learn More</a>";
		echo "<br class='clear'>";
		echo "</div>";
		echo "<br class='clear'>";
	}
	echo "<div id='members_content_section'>";
	//if($user->isMember()) {
		if(!$items) {
			echo "<div class='alert alert-warning'>There are no items to display.</div>";	
		}
		else {
			if(isset($_REQUEST['list'])) {
				echo $feature->displaySummaryList($items, "All Featured Items");
			}
			else {
				echo $feature->displaySlides($items, "Latest Featured Items");
			}
		}
		
	if($user->isMember()) {
		echo "<div id='members_home_news'>";
		$a = new Area("MembersOnly");
		$a->display($c);
		echo "</div>";
	}				
	if($user->isMasterAdmin()) {
		echo "<div class='admin_area uppercase small'>All sections visible to super admin only</div>";
	}
	if($user->isGuest() || !$user->isMember()) {
		$a = new Area("GuestsAndNonMembers");
		$a->display($c);
	}
	echo "</div>";
	
	if($itemCount > 1) {
		$paging = Loader::helper("paging");
		
		$url = "/members?list=true";
		$paging->show($page, $limit, $totalItems, $itemCount, $url, $query);
	}
	//else
	if(ADMIN) {
		//echo $query;
		echo "<br class='clear'>";
		echo "itemCount:".$itemCount."<br>";
	}
			
	Loader::element('members/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
