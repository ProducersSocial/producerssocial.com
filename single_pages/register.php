<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$_REQUEST['register'] = true;

$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');

//ob_start();
Loader::library('authentication/open_id');
$form = Loader::helper('form');

Loader::model('locations');

$email = null;
$username = null;
if(isset($_REQUEST['email'])) $email = $_REQUEST['email'];
if(isset($_REQUEST['username'])) $username = $_REQUEST['username'];

?>
<div class='login_page'>	
	<div class="white_page">
		<div class="producers-main-pad">
		<div class="register_page_center ccm-ui">

	<div class="container-fluid">
		<?php 
		$attribs = UserAttributeKey::getRegistrationList();
		if($success) { 
		?>
			<div class="ccm-form col-sm-12">
				<?php	
					switch($success) { 
						case "registered": 
							?>
							<p><strong><?php echo $successMsg ?></strong><br/><br/>
							<?php 
						break;
						case "validate": 
							?>
							<p><?php echo $successMsg[0] ?></p>
							<p><?php echo $successMsg[1] ?></p>
							<?php
						break;
						case "pending":
							?>
							<p><?php echo $successMsg ?></p>
							<?php
						break;
					} 
				?>
				<a class="btn btn-lg btn-white" href="<?php echo $this->url('/')?>"><?php echo t('Return to Home')?></a>
				<a class="button" href="<?php echo $this->url('/profile')?>"><?php echo t('Go to My Profile')?></a>
			</div>
		<?php 
		} 
		else {
		?> 
			<legend><?php echo t('Create Your Account')?></legend>
			<div class="ccm-form col-sm-6">
				<p>Get started quickly with social login...</p>
				<?php
					$a = new Area('SigninBlock');
					$a->display($c);
				?>
			</div>
			<div class="ccm-form col-sm-6">
				<p>Or signup with a username and password:</p>
				<?php
					if($error1 && $error1->has()) {
						echo "<div class='alert alert-danger'>";
						echo $error1->output();
						echo "</div>";
					}
				?>
				<form method="post" action="<?php echo $this->url('/register', 'do_register')?>" class="form-horizontal">
					<fieldset>
						<div class="control-group">
						<?php if ($displayUserName) { ?>
							<label for="uName"><?php echo $form->label('uName',t('Username')); ?></label><br/>
							<div class="controls"><?php echo $form->text('uName', $username); ?></div>
						<?php } ?>
						
						<?php
							$locations = Locations::getSelectList(1, true, true, "ID!='HQ'");
						?>

						<label for="uEmail"><?php echo $form->label('uEmail',t('Email Address')); ?></label><br/>
						<div class="controls"><?php echo $form->text('uEmail', $email); ?></div>
						
						<label for="uPassword"><?php echo $form->label('uPassword',t('Password')); ?></label><br/>
						<div class="controls"><?php echo $form->password('uPassword'); ?></div>

						<label for="uLocation"><?php echo $form->label('uLocation',t('Location')); ?></label><br/>
						<div id='uLocation' class="controls"><?php echo $form->select('uLocation', $locations, "0", array('onchange'=>"return ps.register.updatesociallogin();")); ?></div>

						<br>
						
						<?php
						/*
						<label for="uPassword"><?php echo $form->label('uPasswordConfirm',t('Confirm Password')); ?></label><br/>
						<?php echo $form->password('uPasswordConfirm'); ?>
						*/
						?>
						</div>
					</fieldset>
					
					<div class="g-recaptcha" data-sitekey="6LennCUTAAAAADipQ-GpsbD1kXhm845qPi9aNcMQ"></div>
					<?php if (ENABLE_REGISTRATION_CAPTCHA) { ?>

						<div id='register_captcha' class="control-group">
							<?php $captcha = Loader::helper('validation/captcha'); ?>			
							<?php echo $captcha->label()?>
							<div class="controls">
							<?php
							  $captcha->showInput(); 
							  $captcha->display();
							  ?>
							</div>
						</div>


					<?php } ?>
					
					<br>
						<?php echo $form->hidden('rcID', $rcID); ?>
						<?php echo $form->submit('register', t('Register') . ' &gt;', array('class' => 'button left', 'onclick'=>"return ps.register.validate();"))?>
						<!--<a class="button white small left pad-left" href="<?php echo $this->url('/login')?>"><?php echo t('Cancel')?></a>-->
				</form>
			</div>
		<?php } ?>
	</div>
	<br><br>
	<div class='center'>By creating an account you agree to our <a href="/terms-and-privacy" target="_blank">Terms of Use and Privacy Policy</a></div>

		</div>
		</div>
	</div>
	<br><br><br><br>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
	$this->inc('elements/footer.php'); 
	//$out = ob_get_contents();
	//ob_end_clean();
	//Loader::element('view_template', array('innerContent'=> $out));
?>