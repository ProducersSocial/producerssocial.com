<?php
//ini_set('post_max_size', '256M');

$stripe = Loader::helper('stripe');
$stripe->startup();
//$stripe->setupPlans();

//print_r($_REQUEST);
Loader::model('user_profile');
Loader::model('user_subscription');
Loader::model('user_ticket');

$user = Loader::helper('user');
$slack = Loader::helper("slack");

$u = new User();
if($u->isLoggedIn()) {
	$info = UserInfo::getByID($u->uID);
	$profile = UserProfile::getByUserID($u->uID);
}

if(isset($_REQUEST['phpinfo'])) {
	echo phpinfo();
}
if(isset($_REQUEST['navbar'])) {
	$this->inc('elements/navbar.php');
}
else
if(isset($_REQUEST['plan'])) {
	echo $stripe->subscribeButton("/membership", $_REQUEST['plan']);
}
else
if(isset($_REQUEST['subscribe'])) {
	echo $stripe->subscribeProcess($_REQUEST['subscribe'], $_REQUEST['token']);
}
else
if(isset($_REQUEST['makeadmin'])) {
	if($u->isSuperUser() || $profile->isLocationAdmin('HQ')) {
		$p = UserProfile::getByUserID($_REQUEST['makeadmin']);
		if(!$p || !$_REQUEST['makeadmin'] || !$_REQUEST['location']) {
			echo "<div class='alert alert-warning'>Invalid user id [".$_REQUEST['makeadmin']."] or location [".$_REQUEST['location']."]</div>";
		}
		else {
			$p->uLocationAdmin = $_REQUEST['location'];
			UserProfile::save($p);
		}
	}
}
else
if(isset($_REQUEST['revokeadmin'])) {
	if($u->isSuperUser() || $profile->isLocationAdmin('HQ')) {
		$p = UserProfile::getByUserID($_REQUEST['revokeadmin']);
		if(!$p || !$_REQUEST['revokeadmin']) {
			echo "<div class='alert alert-warning'>Invalid user id [".$_REQUEST['makeadmin']."]</div>";
		}
		else {
			$p->uLocationAdmin = "";
			UserProfile::save($p);
		}
	}
}
else
if(isset($_REQUEST['purchase']) && isset($_REQUEST['token'])) {
	echo $stripe->purchaseProcess($_REQUEST['purchase'], $_REQUEST['quantity'], $_REQUEST['token']);
}
else
if(isset($_REQUEST['rsvp'])) {
	if(!isset($_REQUEST['eventid']) || !$_REQUEST['eventid']) {
		echo "Error - Please specify which event to RSVP for";	
	}
	else {
		echo UserTicket::rsvp($_REQUEST['eventid'], $_REQUEST['rsvp']);
	}
}
else
if(isset($_REQUEST['unrsvp'])) {
	if(!$_REQUEST['unrsvp']) {
		echo "Error - Missing event id for removing reservation.";	
	}
	else {
		echo UserTicket::unrsvp($_REQUEST['unrsvp']);
	}
}
else
if(isset($_REQUEST['unrsvpticket'])) {
	if(!$_REQUEST['unrsvpticket']) {
		echo "Error - Missing ticket id for reservation.";	
	}
	else {
		echo UserTicket::unrsvpTicket($_REQUEST['unrsvpticket']);
	}
}
else
if(isset($_REQUEST['autosignup'])) {
	if(!$_REQUEST['id']) {
		echo "Error - No ticket id was provided.";	
	}
	else {
		echo UserTicket::autosignup($_REQUEST['id'], $_REQUEST['autosignup']);
	}
}
else
if(isset($_REQUEST['like'])) {
	Loader::model("user_like");
	if($u->isLoggedIn()) {
		if(!$_REQUEST['item']) {
			echo "Error - No item id was provided.";	
		}
		else {
			$like = UserLike::setLike($_REQUEST['item'], null, $_REQUEST['like']);
			//echo "LIKED:".$like->liked." item:".$like->item_id." user:".$like->user_id;
		}
		echo UserLike::likeButton($_REQUEST['item'], null, 'right', true, $_REQUEST['showcount']);
	}
	else {
		Loader::model("member_feature");
		$url = "";
		$f = MemberFeature::getByID($_REQUEST['item']);
		if($f) {
			$url = $f->getURL();
		}
		echo "LOGIN:".$url;
	}
}
else
if(isset($_REQUEST['item_upload'])) {
	if(!$_REQUEST['item']) {
		echo "Error - No item id was provided.";	
	}
	else {
		Loader::model("member_feature");
		echo MemberFeature::uploadFiles();
	}
}
else
if(isset($_REQUEST['deletefile'])) {
	if(!$_REQUEST['item']) {
		echo "Error - No item id was provided.";	
	}
	else {
		Loader::model("member_feature");
		echo MemberFeature::deleteFile($_REQUEST['item'], $_REQUEST['deletefile']);
	}
}
else
if(isset($_REQUEST['verifyhandle'])) {
	if(!$_REQUEST['verifyhandle']) {
		echo "Error - Invalid handle provided.";	
	}
	else {
		Loader::model("user_profile");
		$p = UserProfile::getByHandle($_REQUEST['verifyhandle'], $_REQUEST['uid']);
		if($p) {
			echo "<span class='error'><i class='fa fa-warning' aria-hidden='true'></i> Taken</i></span>"; 
		}
		else {
			echo "<span class='_color'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Available!</i></span>"; 
		}
	}
}
else
if(isset($_REQUEST['verifyfeaturehandle'])) {
	if(!$_REQUEST['verifyfeaturehandle']) {
		echo "Error - Invalid handle provided.";	
	}
	else {
		Loader::model("member_feature");
		if(!MemberFeature::validateHandle($_REQUEST['verifyfeaturehandle'], $_REQUEST['id'])) {
			echo "<span class='error'><i class='fa fa-warning' aria-hidden='true'></i> Taken</i></span>"; 
		}
		else {
			echo "<span class='_color'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Available!</i></span>"; 
		}
	}
}
else
if(isset($_REQUEST['getreservedhandles'])) {
	Loader::model("user_profile");
	echo json_encode(UserProfile::$ReservedHandles);
}
else
if(isset($_REQUEST['reportabuse']) && $_REQUEST['reportabuse']) {
	if($u->isLoggedIn()) {
		Loader::model("user_log");
		UserLog::record("Report Abuse", $_REQUEST['reportabuse']);
		
		$p = UserProfile::getByUserID($_REQUEST['reportabuse']);
		$p->uReported++;
		UserProfile::save($p);
		
		// SEND ADMIN EMAIL
		$mh = Loader::helper('mail');
		$mh->from(EMAIL_DEFAULT_FROM_ADDRESS, EMAIL_DEFAULT_FROM_NAME);
		$mh->to(EMAIL_DEFAULT_FROM_ADDRESS, EMAIL_DEFAULT_FROM_NAME);
		$mh->addParameter('user_id', $_REQUEST['reportabuse']);
		$mh->load("admin_reported_user");
		$mh->sendMail();
		
		echo "SENT";
	}
}
else
if(isset($_REQUEST['reportclear']) && $_REQUEST['reportclear']) {
	if($user->isSuperAdmin()) {
		Loader::model("user_log");
		UserLog::record("Abuse Resolved", $_REQUEST['reportclear']);
		
		$p = UserProfile::getByUserID($_REQUEST['reportclear']);
		$p->uReported = 0;
		UserProfile::save($p);
	}
}
else
if(isset($_REQUEST['profile']) && $_REQUEST['profile']) {
	$u = Loader::helper('user');
	$u->displaySummary($_REQUEST['profile'], false, null, null, null, null, "user_profile_container", true, false);
}
else
if(isset($_REQUEST['uploadimage'])) {
	$u = Loader::helper('user');
	$accepted_origins = array(BASE_URL);
	
	  $imageFolder = "/images/features/content";
	
	  reset($_FILES);
	  $temp = current($_FILES);
	  if(is_uploaded_file($temp['tmp_name'])){
	  	  if(isset($_SERVER['HTTP_ORIGIN'])) {
	  	  	  // same-origin requests won't set an origin. If the origin is set, it must be valid.
	  	  	  if(in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
	  	  	  	  header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
	  	  	  } 
	  	  	  else {
	  	  	  	  header("HTTP/1.0 403 Origin Denied");
	  	  	  	  return;
	  	  	  }
	  	  }
	
	  	  if(preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
	  	  	  header("HTTP/1.0 500 Invalid file name.");
	  	  	  return;
	  	  }
	
	  	  if(!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
	  	  	  header("HTTP/1.0 500 Invalid extension.");
	  	  	  return;
	  	  }
	
	  	  $filetowrite = $imageFolder . $temp['name'];
	  	  move_uploaded_file($temp['tmp_name'], $filetowrite);
	
	  	  echo json_encode(array('location' => $filetowrite));
	} 
	else {
		header("HTTP/1.0 500 Server Error");
	}
}

?>