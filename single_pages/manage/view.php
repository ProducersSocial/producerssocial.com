<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::model("facebook_event");
	Loader::model("on_deck");
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    
	$events = Loader::helper('events');
	$tickets = Loader::helper('tickets');
	
	$loc = $user->adminLocation;
	if($loc == "HQ") $loc = null;//"LA";
	echo "<h3>".Locations::getName($user->adminLocation)." Management</h3>";
	
	if(isset($refresh_events)) {
		echo $refresh_events;
	}
	if(isset($css)) {
		echo "<h1>CSS for '".strtoupper($css_location)."'</h1>";
		
		echo "<div class='alert alert-white'>";
		echo "<form method='post' action='/manage/generate_colors'>";
		
		echo "<div class='row'>";
		echo "<label for='css_location' class='left' style='width:100px;'>".$css_location."</label>";
		echo "<input class='left' type='text' id='css_location' name='css_location' value='".$css_location."'>";
		echo "</div>";
		
		$defaults = array(
			"color1"	=> "#773ba5", 
			"color2"	=> "#54288a", 
			"grad1"		=> "#914abb", 
			"grad2"		=> "#54288a", 
			"border1"	=> "#a771c9", 
			"border2"	=> "#723d9b", 
			"border3"	=> "#45236e", 
			"border4"	=> "#8553af"
		);
		$colors = array(
			"color1"	=> "#COLOR1", 
			"color2"	=> "#COLOR2", 
			"grad1"		=> "#COLOR_GRADA", 
			"grad2"		=> "#COLOR_GRADB", 
			"border1"	=> "#COLOR_BORDER1", 
			"border2"	=> "#COLOR_BORDER2", 
			"border3"	=> "#COLOR_BORDER3", 
			"border4"	=> "#COLOR_BORDER4"
		);
		foreach($colors as $color => $tag) {
			if(!isset($_REQUEST[$color])) $_REQUEST[$color] = $defaults[$color];
			echo "<div class='row'>";
			echo "<label for='".$color."' class='left' style='width:100px;'>".$color."</label>";
			echo "<input class='left' type='text' id='".$color."' name='".$color."' value='".$_REQUEST[$color]."'>";
			echo "<div style='background-color:".$_REQUEST[$color]."; width:24px; height:24px; float:left;'>&nbsp;</div>";
			echo "</div>";
			$css = str_replace($tag, $_REQUEST[$color], $css);
		}
		
		echo "<input type='submit' value='Regenerate'>";
		echo "</form>";
		echo "<br class='clear'>";
		echo "</div>";
		
		echo "<br class='clear'>";
		echo "<div class='alert alert-white'>";
		$css = str_replace("XX", strtolower($css_location), $css);
		//$css = str_replace("\n", "<br>", $css);
		//$css = str_replace("\t", "&nbsp;&nbsp;&nbsp;", $css);
		echo "<textarea style='width:100%; height:400px;'>";
		echo $css;
		echo "</textarea>";
		echo "</div>";
	}
	else
	if(isset($_REQUEST['custom'])) {
		$locations = Locations::getSelectList(1, true, false);
		//$customEvents = OnDeck::getAll("WHERE EventID IS NULL OR EventID='' ORDER BY EventDate DESC");
		echo "<div id='events_custom_session' class='alert alert-info left'>";
			echo "<h3>Create a Custom On Deck Session</h3>";
			echo "Starts a new session not associate with any Facebook events. This will be visible to the public unless it is made private.";
			echo "<div class='alert alert-white center'>";
			echo "<form id='events_custom_session_form' method='post' action='/ondeck'>";

			echo "<div class='control-group'>";
			echo "<div class='ccm-form ccm-ui' style='width:300px; max-width:100%; margin:auto;'>";
			
			echo "<div class='ccm-profile-attribute'>";
			echo $form->label('location', t('Location'));
			echo "<br class='clear'>";
			if($user->isSuperAdmin()) {
				echo $form->select('location', $locations, "0", array('class'=>'center'));
			}
			else {
				echo $form->hidden('location', $isLocationAdmin);
			}
			echo "</div>";
			
			echo $form->hidden('cmd_new', 1);
			
			echo "<div class='ccm-profile-attribute'>";
			echo $form->label('event_name', t('New Event Name'));
			echo $form->text('event_name', "", array('class'=>'center'));
			echo "</div>";
			
			echo "<div class='ccm-profile-attribute'>";
			echo $form->label('IsPrivate', t('Make Private?'));
			echo $form->checkbox('IsPrivate', 1, 0, array('class'=>'center', 'style'=>"width:50px; margin:auto;"));
			echo "</div>";
			
			echo "</div>";
			echo "</div>";
	
			echo "<br class='clear'>";
			echo "<div id='events_custom_session_error'></div>";
			echo $form->submit('submit', t('New Custom Session'), array('class' => 'button right', 'onclick'=>"return ps.events.validateNewSession();"));
			echo "</form>";
			
			echo "<br class='clear'>";
			echo "<br class='clear'>";
			echo "<p class='small italic light'>Please only use this for testing or private sessions. Any public or regular socials should be started on the event's page.</p>";
			echo "</div>";
		echo "</div>";
	}
	else
	if(isset($_REQUEST['event'])) {
		echo "<div class='alert alert-white'>";

		echo "<a class='button white right medium' href='/manage'><i class='fa fa-arrow-circle-up' aria-hidden='true'></i> Back to Events</a>";
	
		$event = FacebookEvent::getID($_REQUEST['event']);
		$upcoming = array($event);
		echo "<h3 class='left nopadding'>".$event->Name."</h3>";
		
		echo $events->listEvents($upcoming, false, null, true);
		
		echo "<br class='clear'>";
		$tickets->showPresales($event, false);
		
		echo "<br class='clear'>";
		echo "</div>";
	}
	else {
		$q = "WHERE ";
		if(!$loc) {
			$q .= "1=1";
		}
		else {
			$q .= "(HostedBy='".$user->id."' OR Location='".$user->profile->uLocation."')";
		}
		$q .= " AND ((EventDate>='".date("Y-m-d", time())." 00:00:00' AND EventDate<='".date("Y-m-d H:i:s", time() + (24 * 60 * 60))."')";
		$q .= " OR (IsOpen=1 OR IsLive=1))";
		//$q .= " LIMIT 3";
		//echo $q;
		$live = OnDeck::getAll($q);
		if($live && count($live)) {
			echo "<div class='alert alert-white'>";
			echo "<h3>Events Now</h3>";
			foreach($live as $d) {
				echo $events->eventBox($d);
			}
			echo "<br class='clear'>";
			echo "</div>";
		}

		echo "<div class='alert alert-white'>";
		echo "<form id='refresh_facebook_form' class='right' method='post' action='/manage'>";
		echo $form->hidden('refresh_events', 1);
		echo $form->submit('cmd_refresh', t('Refresh Events from Facebook'), array('class' => 'button white medium right'));
		echo "<br class='clear'>";
		echo "</form>";
		
		echo "<h3 class='left'>Scheduled Events</h3>";
		echo "<br class='clear'>";
		$upcoming = $events->getEvents(null, null, null, "ASC", null, $loc);
		if($upcoming && count($upcoming) && is_array($upcoming)) {
			echo $events->listEvents($upcoming, false, null, true);
		}
		else {
			echo "<div class='alert alert-warning'>There are no upcoming events scheduled! Please maintain your events scheduled 3 months in advance.</div>";	
		}
		echo "<br class='clear'>";
		echo "</div>";
		
		echo "<div class='alert alert-gray'>";
		echo "<div class='events_section_heading'>Past On Deck Sessions</div>";
		
		$limit = "LIMIT 10";
		if(isset($_REQUEST['viewall'])) $limit = null;
		$beforeDate = date("Y-m-d 00:00:00", time() - (12 * 60 * 60));
		$q = "WHERE EventDate<='".$beforeDate."'";
		if($loc) $q .= " AND (Location='".$user->profile->uLocationAdmin."' OR HostedBy=".$user->profile->uID.")";
		$pastDecks = OnDeck::getAll($q, "ORDER BY Modified DESC ".$limit);
		echo $events->listDecks($pastDecks, false, null, true);
		echo "<br class='clear'>";
		echo "<a href='/manage?viewall=true' class='small right'>View All...</a>";
		echo "<br class='clear'>";
		echo "</div>";
		/*
		echo "<h4 class='left'>Past Events</h4>";
		$q = "LEFT JOIN OnDeck AS od ON od.ID=OnDeckID WHERE fb.StartTime<='".$beforeDate."'";
		$q .= " AND (fb.Location='".$user->profile->uLocation."' OR od.HostedBy=".$user->profile->uID.")";
		$past = FacebookEvent::getAll($q);
		echo $events->listEvents($past, true, null, true);
		echo "<br class='clear'>";
		*/
		
		echo "<a href='/manage?custom=true' class='button white right'>Create a Custom On Deck Session</a>";
		
		
	}
	
	//echo "<a class='button' href='/manage/slack_test'>Slack Test</a>";
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
