<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$baseUrl = "/manage/features";
	if($item) {
		echo $feature->displayItemPage($item);
	}
	else {
		echo "<div class='manage_header'>";
			$sectionTitle = "";
			if(isset($_REQUEST['status'])) {
				$sectionTitle = ucfirst($_REQUEST['status']);
			}
			
			if(isset($_REQUEST['blog'])) { 
				echo "<div class='manage_heading'>".$sectionTitle." Blog Articles</div>";
			}
			else {
				echo "<div class='manage_heading'>".$sectionTitle." Features</div>";
			}
			if($user->isSuperAdmin()) {
				echo "<a class='button white medium right pad-left' href='".$baseUrl."/edit/'><i class='fa fa-file-o' aria-hidden=true></i></a>";
			}
			echo "<div class='manage_counter'>Total: ".count($items)."</div>";
		echo "</div>";
		
		if($items) {
			echo "<div id='member_feature_list_view' class='manage_list_view'>";
			echo "<div class='manage_list_column_headings'>";
				echo "<div class='col-md-6 manage_item_info'>";
					echo "<div class='manage_item_id'>ID</div>";
					echo "<a class='' href=''>Name</a>";
				echo "</div>";
				echo "<div class='col-md-6 manage_item_controls'>";
					echo "<div class='manage_item_buttons'>&nbsp;</div>";
					echo "<div class='manage_item_type'>Type</div>";
				echo "</div>";
				echo "<div class='manage_item_stats'>";
					echo "<div class='manage_item_status'>Status</div>";
					echo "<div class='manage_item_handle'>Handle</div>";
					echo "<div class='manage_item_date'>Publish Date</div>";
				echo "</div>";
			echo "</div>";
			
			if($items) {
				$alt = null;
				foreach($items as $item) {
					echo "<div id='manage_item_".$item->id."' class='manage_item_row ".$alt."'>";
						echo "<div class='col-md-6 manage_item_info'>";
							echo "<div class='manage_item_id'>".$item->id."</div>";
							echo "<a class='manage_item_name' href='".$baseUrl."/view/".$item->id."'>".$item->name."</a>";
						echo "</div>";
						
						echo "<div class='col-md-6 manage_item_controls'>";
							echo "<div class='manage_item_buttons'>";
								echo "<a class='button small white right pad-right' href='".$baseUrl."/view/".$item->id."'><i class='fa fa-arrow-circle-right' aria-hidden=true></i></a>";
								echo "<a class='button small white right' href='".$baseUrl."/edit/".$item->id."'><i class='fa fa-pencil' aria-hidden=true></i></a>";
							echo "</div>";
							echo "<div class='manage_item_type'>".$item->getTypeName()."</div>";
						echo "</div>";
						
						echo "<div class='manage_item_stats'>";
							echo "<div class='manage_item_status ".$item->status."'>".$item->getStatusName()."</div>";
							echo "<div class='manage_item_handle'><a href='".$item->getURL()."'>".$item->getHandle()."</a></div>";
							echo "<div class='manage_item_date'>".$item->published."</div>";
						echo "</div>";
					echo "</div>";
					$alt = $alt ? null : "alt";
				}
			}
			else {
				echo "<div class='alert alert-info'>No items to display</div>";
			}
			echo "</div>";
		}
		else {
			echo "<div class='clear alert alert-white'>There are no items to display</div>";	
		}
		echo "<div class='manage_list_controls'>";
			if($user->isSuperAdmin()) {
				echo "<a class='button white medium left pad-right' href='".$baseUrl."/edit/'><i class='fa fa-file-o' aria-hidden=true></i> Add Feature</a>";
			}
			$status = 'active';
			if(isset($_REQUEST['status'])) {
				$status = $_REQUEST['status'];
			}
			$section = null;
			if(isset($_REQUEST['blog'])) { 
				$section = "&blog=true";
			}
			if($status != 'active') {
				echo "<a class='button white medium left pad-right' href='".$baseUrl."?status=active".$section."'>View Active</a>";
			}
			if($status != 'archived') {
				echo "<a class='button white medium left pad-right' href='".$baseUrl."?status=archived".$section."'>View Archived</a>";
			}
		echo "</div>";
	}

	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
