<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this)); 
    
	if(!$item->id) {
		echo "<h1>Create New Item</h1>";
	}
	else {
		echo "<h1>Editing Item: ".$item->name."</h1>";
	}
	
	if(isset($error) && $error->has()) {
		echo "<div class='alert	alert-warning'>";
		$error->output();
		echo "</div>";
	}
?>
	<div class="ccm-form">
		<form method="post" class="ccm-ui" action="<?php echo $this->action('save')?>" id="edit-feature-form" enctype="multipart/form-data">
			<fieldset>
				<?php
					echo $form->hidden('id', $item->id);
					
					if(isset($_REQUEST['ondeck'])) {
						echo $form->hidden('ondeck_id', $_REQUEST['ondeck']);
					}
				
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('type', t('Type'));
						echo "<p class='small light'>What kind of item is being offered?</p>";
						echo $form->select('type', MemberFeature::getTypesList($item->type), $item->type);
					echo "</div>";
					//echo "<div class='ccm-profile-attribute'>";
					//	echo $form->label('status', t('Status'));
					//	echo "<p class='small light'>Updates automatically based on publish date and expiration</p>";
					//	echo $form->select('status', MemberFeature::getStatusList($item->status), $item->status, array('onchange'=>'return ps.features.updateItemStatus(true);'));
					//echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('location', t('Location'));
						echo "<p class='small light'>Blog posts associated with a specific location can be managed by location admins</p>";
						echo $form->select('location', Locations::getSelectList(1), $item->location);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('submit_location_only', t('Location Exclusive (for submissions)'));
						echo "<p class='small light'>Only members registered at this location can submit.</p>";
						echo $form->checkbox('submit_location_only', $item->submit_location_only);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('submit_members_only', t('Members Only (for submissions)'));
						echo "<p class='small light'>Only members with an active membership may submit.</p>";
						echo $form->checkbox('submit_members_only', $item->submit_members_only);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('submit_limit', t('Limit of Submissions (per member)'));
						echo "<p class='small light'>Members will only be able to submit tracks up to the number specified.</p>";
						echo $form->text('submit_limit', $item->submit_limit);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('submit_notify', t('Email Notification (for submissions)'));
						echo "<p class='small light'>Enter emails separated by comma to be notified of new submissions</p>";
						echo $form->text('submit_notify', $item->submit_notify);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('featured', t('Featured Item?'));
						echo "<p class='small light'>Feature on the home page? <em>(Only until newer items take precedent)</em></p>";
						echo $form->checkbox('featured', 1, $item->featured);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						$count = MemberFeature::getCount()+1;
						echo $form->label('ordernum', t('Order Number '));
						echo "<p class='small light'>Determines the sorting order, with higher numbers displayed first.</p>";
						if($item->ordernum == 0) {
							$item->ordernum = $count;
						}
						echo $form->text('ordernum',$item->ordernum);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('name', t('Item Name'));
						echo "<span class='ccm-required'>*</span><p class='small light'>The main headline or title</p>";
						echo $form->text('name',$item->name, array('onkeyup'=>"return ps.features.updateItemHandle();"));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('handle', t('Item Handle'));
						if(!$item->handle) $item->handle = "your_handle";
						echo "<span class='ccm-required'>*</span><p class='small light'>Defines the url path. <em>No spaces or special charcters.</em></p>";
						echo $form->text('handle', $item->handle, array('autocomplete' => "off"));
						$path = $item->type == 'blog' ? "/blog/" : "/members/feature/";
						echo "<p class='small'>".BASE_URL.$path."<span id='handle_preview'>".$item->handle."</span> ";
						echo "<span id='handle_validate'></span></p>";
						echo $form->hidden('handle_valid', 1);
					echo "</div>";
					echo "<br>";
					if(isset($_REQUEST['ondeck'])) {
						echo "<p class='small light'>Leave image uploads blank to use the default graphics based on event location.</p>";
					}
					echo "<p class='bold'>Please compress images first using <a href='https://tinyjpg.com/' target='_blank'>https://tinyjpg.com/</a></p>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('thumb', t('Thumbnail Image 768x768'));
						echo $form->file('thumb');
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('feature', t('Feature Image 1536x768'));
						echo $form->file('feature');
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('summary', t('Summary'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Displayed in the preview list</p>";
						echo $form->textarea('summary', $item->summary, array('maxlength'=>255));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('content', t('Public Content'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Full item description formatted for general viewing.</p>";
						$content = $item->getContent();//str_replace("//", "/", $item->getContent());
						echo $form->textarea('content', $content, array('class'=>'tinymce'));
						//if(ADMIN) {
						//	echo $form->textarea('content2', $content);
						//}
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('nonmembersonly', t('Non-Members-Only Content'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Content, links and embedded players for <em>Non-Members only</em>.</p>";
						echo $form->textarea('nonmembersonly', $item->nonmembersonly, array('class'=>'tinymce'));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('membersonly', t('Members-Only Content'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Content, links and embedded players for <em>Premium Members only</em>.</p>";
						echo $form->textarea('membersonly', $item->membersonly, array('class'=>'tinymce'));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('notes', t('Admin Notes'));
						echo "<p class='small light'>Notes visible to admins only</p>";
						echo $form->textarea('notes', $item->notes);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('tags', t('Tags'));
						echo "<p class='small light'>Enter keywords separated by commas (ex. sounds, samples, glitch)</p>";
						echo $form->text('tags',$item->tags);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('filekey', t('Directory Key'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Obfuscated directory name for storing files safely</em>.</p>";
						if(!$item->filekey) $item->filekey = uniqid();
						echo $form->text('filekey', $item->filekey);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('link', t('Download or Acccess Link'));
						echo "<p class='small light'>Special link or url to access the item. Only visible to members with an active subscription.</p>";
						echo $form->text('link',$item->link);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('expiration', t('Expiration Date'));
						echo "<p class='small light'>(Optional) Set a date after which the offering will no longer be available. Upon expiration, the Members-Only content will be fully hidden.</p>";
						echo $form->text('expiration',$item->expiration);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('published', t('Publish Date'));
						echo "<p class='small light'>Schedule the item to be activated (optional). Enter any standard date format.</p>";
						echo $form->text('published',$item->published);
					echo "</div>";
				?>
			</fieldset>
			<?php 
				echo "<div class='manage_list_controls'>";
				echo "<input type='submit' class='button right' id='save' name='save' value='Save'>";
				//echo "<div class='right' id='status_save'>".$item->status."</div>";
				echo $form->select('status', MemberFeature::getStatusList($item->status), $item->status, array('onchange'=>'return ps.features.updateItemStatus(false);'));
				echo "<input type='submit' class='button left white pad-right' id='delete' name='delete' value='Delete' onclick='return ps.confirm(\"Are you sure you want to delete this item? This cannot be undone.\");'>";
				
				//echo "<input type='submit' class='button left white pad-right' id='archive' name='archive' value='Archive'>";
				echo "<input type='submit' class='button left white pad-right' id='cancel' name='resetstats' value='Reset Stats'>";
				echo "<input type='submit' class='button left white pad-right' id='cancel' name='cancel' value='Cancel'>";
				echo "</div>";
			?>
			<br class='clear'><br>
		</form>
		<?php
			if($item->id) {
				echo "<div class='right small'>ID:".$item->id."</div>";
			}
		?>
	</div>

	<input name="image" type="file" id="upload" class="hidden" onchange="">
<?php
/*
	<textarea id="content"></textarea>
	<iframe id="form_target" name="form_target" style="display:none"></iframe>
	<form id="content_img_form" action="/ajax?uploadimage=1" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
		<input name="image" type="file" onchange="$('#content_img_form').submit();this.value='';">
	</form>
*/
?>

<?php
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
