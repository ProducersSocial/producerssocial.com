<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this)); 
    
	if(!$item->id) {
		echo "<h1>Add New Sponsor</h1>";
	}
	else {
		echo "<h1>Editing Sponsor: ".$item->name."</h1>";
	}
	
	if(isset($error) && $error->has()) {
		echo "<div class='alert	alert-warning'>";
		$error->output();
		echo "</div>";
	}
?>
	<div class="ccm-form">
		<form method="post" class="ccm-ui" action="<?php echo $this->action('save')?>" id="edit-feature-form" enctype="multipart/form-data">
			<fieldset>
				<?php
					echo $form->hidden('id', $item->id);
				
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('status', t('Status'));
						echo "<p class='small light'>Inactive sponsors will not be displayed on the website</p>";
						echo $form->select('status', Sponsor::getStatusList($item->status), $item->status);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('name', t('Sponsor Name'));
						echo "<span class='ccm-required'>*</span><p class='small light'>Sponsors display name</p>";
						echo $form->text('name',$item->name, array('onkeyup'=>"return ps.members.updateItemHandle();"));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('handle', t('Handle'));
						if(!$item->handle) $item->handle = "your_handle";
						echo "<span class='ccm-required'>*</span><p class='small light'>Defines the url path (ex. /sponsors/<i id='handle_preview'>".$item->handle."</i>) <em>No spaces or special charcters.</em></p>";
						echo $form->text('handle',$item->handle, array('onkeyup'=>"return ps.members.updateItemHandleDirect();"));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('image', t('Image 220x140'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>PNG white logo over transparent background.</p>";
						echo $form->file('image');
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('link', t('URL'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Link to the sponsors website or product.</p>";
						echo $form->text('link',$item->link);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('ordernum', t('Order Number'));
						echo "<p class='small light'>Sponsors are listed by order number, from highest to lowest.</p>";
						if($item->ordernum == 0) {
							$item->ordernum = Sponsor::getCount()+1;	
						}
						echo $form->text('ordernum',$item->ordernum);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('created', t('Date Added'));
						echo "<p class='small light'>Enter any standard date format.</p>";
						if(!$item->created || $item->created == "0000-00-00 00:00:00") {
							$item->created = date("Y-m-d H:i:s", time());
						}
						echo $form->text('created', $item->created);
					echo "</div>";
				?>
			</fieldset>
			<?php 
				echo "<div class='manage_list_controls'>";
				echo "<input type='submit' class='button right' id='save' name='save' value='Save'>";
				echo "<input type='submit' class='button left white pad-right' id='delete' name='delete' value='Delete' onclick='return ps.confirm(\"Are you sure you want to delete this sponsor? This cannot be undone.\");'>";
				echo "</div>";
			?>
			<br class='clear'><br>
		</form>
	</div>
<?php
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
