<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this)); 
    
	if(!$item->id) {
		echo "<h1>Add New Submission Form</h1>";
	}
	else {
		echo "<h1>Editing Submission Form: ".$item->name."</h1>";
	}
	
	if(isset($error) && $error->has()) {
		echo "<div class='alert	alert-warning'>";
		$error->output();
		echo "</div>";
	}
?>
	<div class="ccm-form">
		<form method="post" class="ccm-ui" action="<?php echo $this->action('save')?>" id="edit-feature-form" enctype="multipart/form-data">
			<fieldset>
				<?php
					echo $form->hidden('id', $item->id);
				
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('name', t('Submission Form Name'));
						echo "<span class='ccm-required'>*</span><p class='small light'>Name of the compilation or competition</p>";
						echo $form->text('name',$item->name, array('onkeyup'=>"return ps.features.updateItemHandle();"));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('handle', t('Handle'));
						if(!$item->handle) $item->handle = "your_handle";
						echo "<span class='ccm-required'>*</span><p class='small light'>Defines the url path (ex. /submissions/<i id='handle_preview'>".$item->handle."</i>) <em>No spaces or special charcters.</em></p>";
						echo $form->text('handle',$item->handle, array('onkeyup'=>"return ps.members.updateItemHandleDirect();"));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('image', t('Image 1536x768'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Feature image.</p>";
						echo $form->file('image');
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('location', t('Location'));
						echo "<p class='small light'>Blog posts associated with a specific location can be managed by location admins</p>";
						echo $form->select('location', Locations::getSelectList(1), $item->location);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('location_only', t('Location Exclusive'));
						echo "<p class='small light'>Only members registered at this location can enter.</p>";
						echo $form->checkbox('location_only', $item->location_only);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('members_only', t('Members Only'));
						echo "<p class='small light'>Only members with an active membership may enter.</p>";
						echo $form->checkbox('members_only', $item->members_only);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('deadline', t('Submission Deadline'));
						echo "<p class='small light'>Submissions will be closed after midnight PST of the date entered.</p>";
						echo $form->text('deadline', $item->deadline);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('closed', t('Closed'));
						echo "<p class='small light'>No submissions will be accepted when closed.</p>";
						echo $form->checkbox('closed', $item->closed);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('notifyemail', t('Email Notification'));
						echo "<p class='small light'>Enter emails separated by comma to be notified of new submissions</p>";
						echo $form->text('notifyemail', $item->notifyemail);
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('summary', t('Summary'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Displayed in the preview list</p>";
						echo $form->textarea('summary', $item->summary, array('maxlength'=>255));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('description', t('Description'));
						echo "<span class='ccm-required'>*</span> <p class='small light'>Full description of the compilation or competition.</p>";
						$description = $item->getDescription();
						echo $form->textarea('description', $description, array('class'=>'tinymce'));
					echo "</div>";
					echo "<div class='ccm-profile-attribute'>";
						echo $form->label('created', t('Date Added'));
						echo "<p class='small light'>Enter any standard date format.</p>";
						if(!$item->created || $item->created == "0000-00-00 00:00:00") {
							$item->created = date("Y-m-d H:i:s", time());
						}
						echo $form->text('created', $item->created);
					echo "</div>";
				?>
			</fieldset>
			<?php 
				echo "<div class='manage_list_controls'>";
				echo "<input type='submit' class='button right' id='save' name='save' value='Save'>";
				echo "<input type='submit' class='button left white pad-right' id='delete' name='delete' value='Delete' onclick='return ps.confirm(\"Are you sure you want to delete this submission form? This cannot be undone.\");'>";
				echo "</div>";
			?>
			<br class='clear'><br>
		</form>
	</div>
<?php
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
