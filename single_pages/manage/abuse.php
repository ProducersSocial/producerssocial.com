<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::model("facebook_event");
	Loader::model("on_deck");
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
    
	if(!$reported) {
		echo "<div class='alert alert-white'>There are no reported users at this time.</div>";
	}
	else {
		$count = count($reported);
		
		echo "<h2>There have been ".$count." reported user profile".($count == 1 ? "" : "s")."</h2>";
		echo "<p>Please investigate the user's profile for any inappropriate content. Admins may either opt to correct the issue, dismiss it, or disable or delete the user's account.</p>";
		
		echo "<br class='clear'>";
		echo "<div class='alert alert-white'>";
		
		foreach($reported as $p) {
			$user->displaySummary($p->uID, false);
		}
		
		echo "</div>";
	}
	
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
?>
