<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$feature = Loader::helper('feature');
	$form = Loader::helper('form');
	
	Loader::model('locations');
?>
	<h3>Emails</h3>
	
<?php
	if($user->isAdmin()) {

		echo "<div class='alert alert-white' style='width:500px; max-width:100%;'>";
		echo "<p class='small'>Export full CSV email list for ".Locations::getName($user->profile->uLocationAdmin)."</p>";
		echo "<br class='clear'>";
		echo "<a class='button small white' href='/manage/emails/exportcsv'>Download CSV</a>";
		echo "</div>";
	}
	if($user->isSuperAdmin()) {

		echo "<div class='alert alert-white' style='width:500px; max-width:100%;'>";
		echo "<p class='small'>Get emails for admins</p>";
		echo "<br class='clear'>";
		echo "<a class='button small white' href='/manage/emails?adminemails=1'>Get All Admin Emails</a>";
		echo "<a class='button small white' href='/manage/emails?adminemails=2'>Get Regional Admin Emails</a>";
		if(isset($email_list)) {
			echo "<textarea style='width:100%; height:300px;'>".$email_list."</textarea>";	
		}
		echo "</div>";
?>
	<div class='alert alert-white' style='width:500px; max-width:100%;'>
	<p class='small'>Select an email template and enter a user ID to generate a test email. </p>
	<form id='email_preview_form' method='post' action='/manage/emails'> 
	<fieldset>
	<div class="ccm-profile-attribute">
		<?php echo $form->label('template', t('Email Template'))?>
		<?php echo $form->select('template', $templates, $template)?>
	</div>
	<div class="ccm-profile-attribute">
		<?php echo $form->label('user_id', t('User ID'))?><br/>
		<p class='small'>Look at the bottom right of a member's profile page to get their user ID.</p>
		<?php echo $form->text('user_id', $user_id)?>
	</div>
	<div class="ccm-profile-attribute">
		<?php echo $form->label('event_id', t('Event ID'))?><br/>
		<p class='small'>Enter the facebook event ID, which is in the url path of the event info page: /eventinfo?id=701544113353972</p>
		<?php echo $form->text('event_id', $event_id)?>
	</div>
	<div class="ccm-profile-attribute">
		<?php echo $form->label('feature_id', t('Feature or Blog ID'))?><br/>
		<p class='small'>Enter the ID for a feature or blog post</p>
		<?php echo $form->text('feature_id', $feature_id)?>
	</div>
	<div class="ccm-profile-attribute">
		<?php echo $form->label('email', t('Send to Email'))?><br/>
		<p class='small'>Enter an email address to send a test email to.</p>
		<?php echo $form->text('email', $email)?>
	</div>
	<br class='clear'>
	<?php echo $form->submit('generate', t('Generate Preview'), array('class'=>'button right medium white'))?>
	<?php 
	echo $form->submit('send', t('Send 1 Email'), array('class'=>'button medium left white'))
	?>
	</fieldset>
	</form>
	</div>
	<br class='clear'>
	
<?php 
	}
?>
	
<?php 	
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	echo "<br class='clear'>";
	
	if($result) {
		echo "<div class='alert alert-success' rel='fadeway'>";
		echo $result;
		echo "</div>";
		echo "<br class='clear'>";
	}
	if($preview) {
		echo "<div class='alert alert-white'>";
		echo "<h2>".$subject."</h2>";
		echo $preview;
		echo "</div>";
	}
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
