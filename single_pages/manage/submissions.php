<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::model("locations");
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$baseUrl = "/manage/submissions";
	$feature = Loader::helper('feature');

	if($item) {
		//echo $feature->displaySubmission($item);
		echo "<div class='row'>";
		echo "<div class='col-md-4'>Name</div>";
		echo "<div class='col-md-8'>".$item->title."</div>";
		echo "</div>";
		
		echo "<div class='row'>";
		echo "<div class='col-md-4'>Handle</div>";
		echo "<div class='col-md-8'><a target='_blank' href='/submission/".$item->getHandle()."'>".$item->getHandle()."</a></div>";
		echo "</div>";
		
		echo "<div class='row'>";
		echo "<div class='col-md-4'>Location</div>";
		echo "<div class='col-md-8 ".$item->location."'>".Locations::getName($item->location)."</div>";
		echo "</div>";

		echo "<div class='row'>";
		echo "<div class='col-md-4'>Deadline</div>";
		echo "<div class='col-md-8'>".$item->deadline."</div>";
		echo "</div>";

		echo "<div class='row'>";
		echo "<div class='col-md-4'>Date Created</div>";
		echo "<div class='col-md-8'>".$item->created."</div>";
		echo "</div>";
		
		echo "<br class='clear'>";
		echo "<a href='/manage/submissions/edit/".$item->id."' class='button right white medium'><i class='fa fa-pencil' aria-hidden=true></i> Edit</a>";
	}
	else {
		echo "<div class='manage_header'>";
			echo "<div class='manage_heading'>Submissions</div>";
			echo "<a class='button right white medium pad-left' href='".$baseUrl."/edit/'><i class='fa fa-file-o' aria-hidden=true></i></a>";
			echo "<div class='manage_counter'>Total: ".count($items)."</div>";
		echo "</div>";
		
		echo "<div id='member_feature_list_view' class='manage_list_view'>";
		echo "<div class='manage_list_column_headings'>";
			echo "<div class='col-md-6 manage_item_info'>";
				echo "<div class='manage_item_id'>ID</div>";
				echo "<a class='' href=''>Name</a>";
			echo "</div>";
			echo "<div class='col-md-6 manage_item_controls'>";
				echo "<div class='manage_item_buttons'>&nbsp;</div>";
				echo "<div class='manage_item_type'>Type</div>";
			echo "</div>";
			echo "<div class='manage_item_stats'>";
				echo "<div class='manage_item_status'>Status</div>";
				echo "<div class='manage_item_handle'>Handle</div>";
				echo "<div class='manage_item_date'>Date Added</div>";
			echo "</div>";
		echo "</div>";
		
		if($items) {
			$alt = null;
			foreach($items as $item) {
				echo "<div id='manage_item_".$item->id."' class='manage_item_row ".$alt."'>";
					echo "<div class='col-md-6 manage_item_info'>";
						echo "<div class='manage_item_id'>".$item->id."</div>";
						echo "<a class='manage_item_name' href='".$baseUrl."/view/".$item->id."'>".$item->name."</a>";
					echo "</div>";
					
					echo "<div class='col-md-6 manage_item_controls'>";
						echo "<div class='manage_item_buttons'>";
							echo "<a class='button small white right pad-right' href='".$baseUrl."/view/".$item->id."'><i class='fa fa-arrow-circle-right' aria-hidden=true></i></a>";
							echo "<a class='button small white right' href='".$baseUrl."/edit/".$item->id."'><i class='fa fa-pencil' aria-hidden=true></i></a>";
						echo "</div>";
					echo "</div>";
					
					echo "<div class='manage_item_stats'>";
						//echo "<div class='manage_item_status ".$item->status."'>".$item->getStatusName()."</div>";
						echo "<div class='manage_item_handle'>".$item->getHandle()."</div>";
						echo "<div class='manage_item_date'>".$item->created."</div>";
					echo "</div>";
				echo "</div>";
				$alt = $alt ? null : "alt";
			}
		}
		else {
			echo "<div class='alert alert-info'>No items to display</div>";
		}
		echo "</div>";
		
		echo "<div class='manage_list_controls'>";
			echo "<a class='button white medium left pad-right' href='".$baseUrl."/edit/'><i class='fa fa-file-o' aria-hidden=true></i> Add Submission Form</a>";
			$closed = 0;
			if(isset($_REQUEST['closed'])) {
				$closed = $_REQUEST['closed'];
			}
			if(!$closed) {
				echo "<a class='button white medium left pad-right' href='".$baseUrl."?status=active'>View Open Forms</a>";
			}
			else {
				echo "<a class='button white medium left pad-right' href='".$baseUrl."?status=archived'>View Closed Forms</a>";
			}
		echo "</div>";
	}
	
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
