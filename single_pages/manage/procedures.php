<?php 
	defined('C5_EXECUTE') or die("Access Denied."); 
	ob_start();
	
	Loader::element('manage/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	
	$baseUrl = "/manage/procedures";
	$feature = Loader::helper('feature');

?>
	<h3>Special Admin Operations</h3>
	
	<div class='alert alert-white col-md-4'>
	<p class='small'>Refetch information from Facebook for all events</p>
	<br class='clear'>
	<a class='button white medium' href='/cron?type=refresh_events'>Refresh Events</a>
	</div>
	
	<div class='alert alert-white col-md-4'>
	<p class='small'>Check all subscriptions for renewal or cancellation and update member statuses</p>
	<br class='clear'>
	<a class='button white medium' href='/cron?type=update_subscriptions'>Update Subscriptions</a>
	</div>
	
	<div class='alert alert-white col-md-4'>
	<p class='small'>Update the status for all On Deck sessions.
	This will also close any sessions left open.</p>
	<br class='clear'>
	<a class='button white medium' href='/cron?type=close_ondecks'>Close Open On Deck Sessions</a>
	</div>
	
<?php 	
	Loader::element('manage/footer', array('user'=> $user, 'this'=>$this));
	
	$out = ob_get_contents();
	ob_end_clean();
	Loader::element('view_template', array('innerContent'=> $out));
