<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class EventsBlockController extends BlockController {
	
	protected $btTable = "btEvents";
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";

	public function getBlockTypeName() {
		return t('Events List');
	}

	public function getBlockTypeDescription() {
		return t('Displays a list of events');
	}
}
