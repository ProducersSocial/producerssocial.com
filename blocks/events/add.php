<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>
<div class="ccm-ui">
	<div class="alert-message block-message info">
		<?php echo t("Displays a list of events.") ?>
	</div>

	<?php 
		echo $form->label('content', t('Name'));
		echo $form->text('content');
	?>
</div>
