<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class TicketPurchaseBlockController extends BlockController {
	
	protected $btTable = "btTicketPurchase";
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";

	public function getBlockTypeName() {
		return t('Ticket Purchase Form');
	}

	public function getBlockTypeDescription() {
		return t('Displays a ticket purchase form');
	}
}
