<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>
<div class="ccm-ui">
	<div class="alert-message block-message info">
		<?php echo t("Presents the user with a ticket purchasing form.") ?>
	</div>

	<?php 
		Loader::model('locations');
	
		echo $form->label('title', t('Title'));
		echo $form->text('title', $title);
		
		echo $form->label('price', t('Price'));
		echo $form->number('price', $price);
		
		echo $form->label('location', t('Location'));
		echo $form->select('location', Locations::getSelectList(null, true, false), $location);
		
		echo $form->label('paypalEmail', t('Paypal Email'));
		echo $form->text('paypalEmail', $paypalEmail);
		
		echo $form->label('sandboxMode', t('Sandbox Mode'));
		echo $form->checkbox('sandboxMode', 1, $sandboxMode);
	?>
</div>
