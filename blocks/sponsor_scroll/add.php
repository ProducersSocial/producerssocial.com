<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>
<div class="ccm-ui">
	<div class="alert-message block-message info">
		<?php echo t("Displays a horizontal scrolling list of sponsors.") ?>
	</div>

	<?php 
		echo $form->label('content', t('Name'));
		echo $form->text('content');
	?>
</div>
