<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class SponsorScrollBlockController extends BlockController {
	public $user = null;
	
	protected $btTable = "btSponsorScroll";
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";

	public function getBlockTypeName() 
	{
		return t('Sponsor Scroll');
	}

	public function getBlockTypeDescription() 
	{
		return t('Displays a horizontal scrolling list of sponsors');
	}
	
	public function view() 
	{
		Loader::model("sponsor");
				
		$this->user = Loader::helper("user");
		$this->set('user', $this->user);
		
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		$items = Sponsor::getAll("status='active'", "ordernum DESC");
		$this->set('items', $items);
	}

}
