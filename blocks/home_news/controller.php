<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class HomeNewsBlockController extends BlockController {
	public $user = null;
	
	protected $btTable = "btHomeNews";
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";

	public function getBlockTypeName() 
	{
		return t('Home News');
	}

	public function getBlockTypeDescription() 
	{
		return t('Displays the latest features on the home page');
	}
	
	public function view() 
	{
		Loader::model("member_feature");
				
		$this->user = Loader::helper("user");
		$this->set('user', $this->user);
		
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		$q = "status='active'";
		if(!$this->user->isSuperAdmin()) {
			$q .= " AND published <= '".date("Y-m-d H:i:s", time())."'";
		}
		$items = MemberFeature::getAll($q, "published DESC LIMIT 2");
		$this->set('items', $items);
	}

}
