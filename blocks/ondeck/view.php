<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>
<?php 
	//echo $content;
?>
<?php 
	
	Loader::model('locations');
	Loader::model('on_deck');
	Loader::model('user_on_deck');
	Loader::model('user_profile');
	Loader::model('user_membership');
	Loader::model('ps_event');
	
	$form 		= Loader::helper('form');
	$ondeck		= Loader::helper('ondeck');
	$navigation = Loader::helper('navigation');
	$eventify 	= Loader::helper('eventify','proevents');
	
	
	$now = date("Y-m-d H:i:s", time());
	$year = date("Y", time());
	$month = date("m", time());
	$day = date("d", time());
	$endOfToday = $year."-".$month."-".$day." 23:59:59";
	
	$startOfDayTime = strtotime($year."-".$month."-".$day." 00:00:00");
	
	// Get all events from today and earlier
	$allDecks = OnDeck::getAll("ORDER BY EventDate DESC"); // WHERE EventDate<'".$endOfToday."' 
	$locations = Locations::getSelectList(1, true, false);
	$events = PSEvent::getAll("ORDER BY date DESC");
	
	$nextEvent = null;
	$userLocation = null;
	$q = "WHERE EventDate>'".$now."'";
	if($p) {
		$userLocation = $p->uLocation;
		$q .= " AND Location='".$userLocation."'";
	}
	$nextEvent = OnDeck::getOne($q." ORDER BY EventDate ASC");
	
	$hasTicket = false; 
	$ticketOnDeckID = 1;
	if($ondeck->user && $hasTicket && !$ondeck->isAdmin) {
		header("Location:".$this->url("/ondeck?show=1&odid=".$ticketOnDeckID));
		die;
	}
	
	//echo "EVENTS:".count($events)."<br>";
	
	// Find events that are currently live or open for sign-ups
	$liveDecks = array();
	$pastDecks = array();
	$upcomingDecks = array();
	$eventsPaired = array();
	foreach($allDecks as $d) {
		//echo "DECK:".$d->ID." event:".$d->EventID."<br>";
		foreach($events as $e) {
		// Pair events with each deck
			//echo "PARING:".$e->eID." == ".$d->EventID."<br>";
			if($e->eID == $d->EventID) {
				$d->Event = $e;
				//echo "PAIRED:".$e->eID."<br>";
				$eventsPaired[] = $e;
				break;
			}
		}
		if($d->IsOpen || $d->IsLive) {
			$liveDecks[] = $d;
		}
		else {
			$date = strtotime($d->EventDate);
			if($date > $startOfDayTime) {
				$upcomingDecks[] = $d;
			}
			else {
				$pastDecks[] = $d;
			}
		}
	}
	
	// Gather the events that do not have a deck yet
	foreach($events as $e) {
		$date = strtotime($e->date." ".$e->sttime);
		//echo $e->date."<br>";
		if(!in_array($e, $eventsPaired) && $date > $startOfDayTime) {
		// create a new temp deck for the event – but only for events starting today or later
			echo "CREATING DECK FOR EVENT:".$e->eID."<br>";
			$d = new OnDeck();
			$d->EventID 	= $e->eID;
			$d->Title		= $e->title;
			$d->Event 		= $e;
			$d->EventDate	= date("Y-m-d H:i:s", $date);
			$d->Location	= strtolower(substr($e->title, 0, 2));
			$d->IsOpen		= 0;
			$d->IsLive		= 0;
			$d = OnDeck::create($d);
			$upcomingDecks[] = $d;
		}
	}
	
?>
<?php
	/*
	if(!$ondeck->user || !$ondeck->user->isLoggedIn()) {
		echo "<div class='alert alert-info'>";
		echo "<h4>Event Sign-ups</h4>";
		echo "Members can sign-up here to get on the deck list during the scheduled times for each event.";
		if($nextEvent) {
			echo "<br class='clear'><br>";
			//echo "EVENT:".$nextEvent->EventID;
			// TODO: Figure out how to link to event page
			//echo "EVENT:".$nextEvent->EventDate;
			$eventTime = strtotime($nextEvent->EventDate);
			echo "<a href='".$eventify->grabURL($nextEvent->EventID)."'><i class='fa fa-calendar-o' aria-hidden='true'></i>&nbsp; ";
			echo "The next scheduled event for <b>".Locations::getName($nextEvent->Location)."</b> is <b>".date("M d", $eventTime)." at ".date("g:ia", $eventTime)."</b>";
			echo "</a>";
		}
		echo "</div>";
	}
	else {
	*/
?>
		<div id="ondeck_live_events">
		<?php
			if(count($liveDecks) > 0) {
				echo "<h3>Events Live Now</h3>";
				if($p) {
					echo "<p>Click an event below to sign-up.</p>";
				}
				else
				if(!$ondeck->user) {
					echo "<div class='alert alert-warning'><i class='fa fa-sign-in' aria-hidden='true'></i>&nbsp; To sign-up for events, please <a href='".$this->url("/login")."'>log in</a> or <a href='".$this->url("/register")."'>register</a>.</div>";
				}
				//else {
				//	echo "<p>Members </p>";
				//}
				foreach($liveDecks as $d) {
					echo $ondeck->eventBox($d);
				}
			}
			else {
				echo "<div class='alert alert-info'>";
				echo "<h4>No Sessions Open</h4>"; 
				echo "Check back here at the scheduled times to sign-up on the list for events.";
				if($nextEvent) {
					echo "<br class='clear'><br>";
					//echo "EVENT:".$nextEvent->EventID;
					// TODO: Figure out how to link to event page
					//echo "EVENT:".$nextEvent->EventDate;
					$eventTime = strtotime($nextEvent->EventDate);
					echo "<a href='".$eventify->grabURL($nextEvent->EventID)."'><i class='fa fa-calendar-o' aria-hidden='true'></i>&nbsp; ";
					echo "The next scheduled event for <b>".Locations::getName($nextEvent->Location)."</b> is <b>".date("M d", $eventTime)." at ".date("g:ia", $eventTime)."</b>";
					echo "</a>";
				}
				echo "</div>";
			}
			echo "<br class='clear'><br>";
?>
		</div>
<?php
		if($ondeck->isAdmin) {
?>
		<br class='clear'>
		<div class="admin_area">
			<div class="admin_area_label">Admin Area – Pending Sessions</div>
			<div id="ondeck_upcoming_events">
				<p class="admin_area_text">Members will not have access to the sign-up list until the session is opened. Click 'Open Session' to open the event for sign-ups.</p>
				<?php
				if(count($upcomingDecks) > 0) {
					foreach($upcomingDecks as $d) {
						echo $ondeck->eventBox($d);
					}
				}
				else {
					echo "<div class='alert'>There are no current or upcoming Producer Social events.</div>";
				}
				?>
<?php
				if($ondeck->isSuperAdmin) { 
?>
					<div id="ondeck_custom_event">
						<h3>Create Custom Session</h3>
						<p class='small'>Use this only to create a custom session not associated with an event.</p>
						<form method="post" action="<?php echo $this->url('/ondeck', '')?>" class="form-horizontal">
							<fieldset>
								<label for="Location"><?php echo $form->label('location',t('Location')); ?></label><br/>
								<div class="controls"><?php echo $form->select('location', $locations, "0"); ?></div>
								<div id="pick_location" class='warning'></div>
								<br>
							</fieldset>
							<br>
							<div class="ccm-form ccm-ui">
								<?php echo $form->submit('cmd_new', t('New Custom Session') . ' &gt;', array('class' => 'button', 'onclick'=>"return ps.ondeck.validateNewSession();"))?>
							</div>
						</form>
					</div>
					<br class='clear'><br>
<?php
				}
?>
			</div>
		</div>
<?php
		}
	/*
		<br class='clear'><br>
		<div id="ondeck_other_events">
			<h3>Past Events</h3>
			<form method="post" action="<?php echo $this->url('/ondeck', '')?>" class="form-horizontal">
			<table id="ondeck_other_events_table">
			<?php
				if($pastDecks && count($pastDecks) > 0) {
					echo "<tr>";
					echo "<th class='ondeck_other_events_num'>ID</th>";
					echo "<th class='ondeck_other_events_location'>Location</th>";
					echo "<th class='ondeck_other_events_date'>Date</th>";
					echo "<th class='ondeck_other_events_rounds'>Rounds</th>";
					echo "<th class='ondeck_other_events_controls'></th>";
					echo "</tr>";

					$i = 0;
					foreach($pastDecks as $d) {
						$date = strtotime($d->EventDate);
						if($date < $startOfDayTime) {
							echo "<tr>";
							echo "<td class='ondeck_other_events_num'>".$d->ID."</td>";
							echo "<td class='ondeck_other_events_location'>".$d->Location."</td>";
							echo "<td class='ondeck_other_events_date'>".date("M d, Y", $date)."</td>";
							echo "<td class='ondeck_other_events_rounds'>".$d->RoundNum."</td>";
							echo "<td class='ondeck_other_events_controls'>";
							if($ondeck->isAdmin) {
								if($d->ID) {
									echo "<a class='iconbutton' href='".$this->url("/ondeck?cmd_delete_deck=1&odid=".$d->ID)."' onclick='return ps.ondeck.deleteSession();'><i class='fa fa-times' aria-hidden='true'></i></a>";
								}
							}
							if($d->ID) {
								echo "<a class='iconbutton' href='".$this->url("/ondeck?show=1&odid=".$d->ID)."'><i class='fa fa-folder-open-o' aria-hidden='true'></i></a>";
							}
							else {
								$t = strtotime($d->EventDate);
								if($t > $startOfDayTime) {
								// Only allow lists to be open for current or future events
									echo "<a class='iconbutton' href='".$this->url("/ondeck?show=1&odid=".$d->ID)."'><i class='fa fa-plus-square' aria-hidden='true'></i></a>";
								}
							}
							//echo $form->submit('show', '', array('class' => 'fa fa-pencil-square-o', 'onclick'=>"return validateNewDeckSession();"));
							echo "</td>";
							echo "</tr>";
							$i++;
						}
					}
				}
				else {
					echo "<div class='alert'>There are no past sessions to view.</div>";
				}
			?>
			</table>
			</form>
		</div>
	*/
?>
<?php
	//}
?>