<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class OnDeckBlockController extends BlockController {
	
	protected $btTable = "btOnDeck";
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";

	public function getBlockTypeName() {
		return t('On Deck Display');
	}

	public function getBlockTypeDescription() {
		return t('Displays deck sign-up list to members');
	}
}
