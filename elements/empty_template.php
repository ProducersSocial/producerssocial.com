<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');
?>
			<?php
				//Loader::element('system_errors', array('error' => $error));
				print $innerContent;
			
				$a = new Area('Main');
				$a->display($c);
			
				$user = Loader::helper('user');
				if(!$user->isLoggedIn()) {
					$a = new Area("GuestsOnly");
					$a->display($c);
				}
				else {
					$a = new Area("UsersOnly");
					$a->display($c);
				}
				if($user->isMember()) {
					$a = new Area("MembersOnly");
					$a->display($c);
				}
			?>
			<br class='clear'>

<?php  $this->inc('elements/footer.php'); ?>
