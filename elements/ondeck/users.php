<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('user_ticket');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');
Loader::model('user_subscription');

$form = Loader::helper('form'); 
$events = Loader::helper('events'); 

	
echo "<div id='ondeck_users_to_list_container' class='center'>";
echo "<a id='ondeck_users_to_list' class='gray' alt='Goto List View' href='javascript:;' onclick='ps.ondeck.showList();'><i class='fa fa-chevron-up' aria-hidden='true'></i> List View</a>";
echo "</div>";
?>

<div id="ondeck_users">
<?php
	$url = $this->url("/ondeck")."?ajax=list&odid=".$ondeck->ID;
	
	if(!$filter_alpha) $filter_alpha = "All";
	if(!$filter_region) $filter_region = "All";
	//echo "filter_alpha:".$filter_alpha;

	if($isLocationAdmin) {
		$userCount = count($availableUsers);
		if($userCount > 0) {
			$alphas = array();
			$regions = array();
			
			foreach($availableUsers as $u) {
				if(!$u->Location) $u->Location = "OT";
				$a = strtoupper(substr($u->Name, 0, 1));
				if(!isset($alphas[$a])) {
					$alphas[$a] = 0;
				}
				else {
					$alphas[$a]++;
				}

				$r = $u->Location;
				if(!isset($regions[$r])) {
					$regions[$r] = 0;
				}
				else {
					$regions[$r]++;
				}
			}
			
			ksort($alphas);
			ksort($regions);
			
			$alphas = array_merge(array("All" => $userCount), $alphas);
			$regions = array_merge(array("All" => $userCount), $regions);
			
			echo "<table id='ondeck_users_table'>";
			echo "<tr>";
			
			echo "<td id='ondeck_users_table_left'>";
			foreach($alphas as $a => $c) {
				echo "<a class='ondeck_filter_button ".($a == $filter_alpha ? "selected" : "")."' onclick='return ps.ondeck.filterAlpha(\"".$a."\");'>".$a."</a>";
			}
			echo "</td>";
			
			echo "<td id='ondeck_users_table_main'>";
			echo "<div id='ondeck_users_table_main_inner'>";
			
			
			echo "<div id='ondeck_users_table_search_bar'>";
			if($isLocationAdmin) {
				$presales = UserTicket::countAllTicketsForEvent($ondeck->EventID);
				echo "<a class='button right white medium' href='javascript:;' onclick='ps.ondeck.showPresaleTickets();'><i class='fa fa-ticket' aria-hidden='true'></i> ".$presales." Presale".($presales == 1 ? "" : "s")."</a>";
			}
			echo "<i class='fa fa-search white medium left' style='margin-right:5px; margin-top:2px;' aria-hidden='true'></i> ";
			//echo "SEARCH:".$member_search."<br>";
			echo "<input class='left pad-right' type='text' id='ondeck_user_search' name='ondeck_user_search' value='".$member_search."' onkeyup='return ps.ondeck.filterSearch();'>";
			echo "<a href='javascript:;' onclick='return ps.ondeck.filterClearSearch();'><i class='fa fa-times white medium left' style='margin-right:5px; margin-top:2px;' aria-hidden='true'></i></a>";
			
			echo "&nbsp; ";
			echo "<input type='hidden' id='ondeck_users_members_only' value='".($members_only ? 1 : 0)."'>";
			echo "<a id='ondeck_users_members_only_toggle' href='javascript:;' onclick='return ps.ondeck.toggleMembersOnly();'>";
			if($members_only) {
				echo " <i class='fa fa-check-square-o' aria-hidden='true'></i>";
			}
			else {
				echo " <i class='fa fa-square-o' aria-hidden='true'></i>";
			}
			echo " Members Only</a>";
			echo "</div>";
			
			$displayCount = 0;
			$list = "";
			foreach($availableUsers as $u) {
				if(!$u->Location) $u->Location = "OT";
				$alpha = strtoupper(substr($u->Name, 0, 1));
				if(($filter_region == "All" || $filter_region == $u->Location) && ($filter_alpha == "All" || $filter_alpha == $alpha)) {
					$ticketCount = UserTicket::countAllTicketsForUserAtEvent($u->UserID, $ondeck->EventID);
					$p = UserProfile::getByUserID($u->UserID);
					$sub = UserSubscription::getByUserID($u->UserID);
					$img = $p ? $p->getAvatar(false) : UserProfile::getDefultAvatar(false);
					$displayName = ($p->uArtistName ? $p->uArtistName : $u->Name);
					$list .= "<a class='ondeck_user_add' data-name='".$displayName."' data-member='".($sub && $sub->isActive() ? "1" : "0")."' ".ajax('users', 'cmd_add_user', $u->UserID).">";
					$list .= "	<div class='ondeck_user_add_location' style='background:#".$locations[$u->Location]->Color."'>".$u->Location."</div>";
					if($ticketCount) {
						$list .= "<div class='ondeck_user_add_tickets'>";
						for($x = 0; $x < $ticketCount; $x++) {
							$list .= "<i class='fa fa-ticket left' aria-hidden='true'></i>";
						}
						$list .= "</div>";
					}
					else {
						if($sub && $sub->isActive()) {
							$availableCount = UserTicket::countAllAvailableTicketsForUser($u->UserID);
							if($availableCount) {
								$list .= "<div class='ondeck_user_add_tickets_member'>";
								$list .= "<i class='fa fa-ticket left' aria-hidden='true'></i>".$availableCount;
								$list .= "</div>";
							}
						}
					}
					if($p && $p->uIsMember) {
						$list .= "<i class='right padsmall fa fa-user-circle-o'></i>";
					}
					$list .= "	<img class='ondeck_user_add_img clear' src='".$img."'>";
					$list .= "	<div class='ondeck_user_add_name'>".$displayName."</div>";
					$list .= "</a>";
					$displayCount++;
				}
			}
			$list .= "<div id='ondeck_user_endspace'>&nbsp;</div>";
			
			$filter = "";
			if($filter_alpha == "All") {
				$filter .= "Showing all names";
			}
			else {
				$filter .= "Showing names starting with '".$filter_alpha."'";
			}
			if($filter_region == "All") {
				$filter .= " in all locations";
			}
			else {
				$filter .= " in ".$locations[$u->Location]->Name;
			}
			if($displayCount) {
				$filter .= " – Tap a name to add it to the list";
			}
			else {
				$filter .= " – Try selecting All names or All locations";
			}
			if(!$displayCount) {
				$filter = str_replace("Showing names", "No names found", $filter);
			}
			echo "<div id='ondeck_users_filter_status'>".$filter."</div>";
			
			echo $list;
			
			//if($displayCount) {
			//	echo "<div id='ondeck_users_message'>Tap a name above to add it to the list</div>";
			//}
			//else {
			//	echo "<div id='ondeck_users_message'>Try selecting All names or All locations</div>";
			//}
			echo "</div>";
			
			echo "</td>";
			
			echo "<td id='ondeck_users_table_right'>";
			foreach($regions as $r => $c) {
				echo "<a class='ondeck_filter_button ";
				if($r == $filter_region) {
					echo "selected";
				}
				echo "' ";
				if($r != $filter_region) {
					echo "style='background:#".$locations[$r]->Color."'";
				}
				echo "onclick='return ps.ondeck.filterRegion(\"".$r."\");'>".$r."</a>";
			}
			echo "</td>";
			
			echo "</tr>";
			echo "</table>";
		}
		else {
			echo "<table class='table_middle'><tr><td>";
			echo "<div class='alert alert-info'>There are no more users available to add to the list.</div>";
			echo "</td></tr></table>";
		}
	}
	else {
		echo "<div class='alert alert-danger'>The list is closed and no new users can be added.</div>";
	}
	//echo "</div>";
?>
</div>
<?php
			
			echo "<div id='ondeck_add_custom_button' class='ondeck_fixed_bottom_align'>";
			echo "<a class='button blue' href='javascript:;' onclick='return ps.ondeck.customEntry();'><i class='fa fa-user-plus' aria-hidden='true'></i> Register New User</a>";
			echo "</div>";
?>
