<?php 
defined('C5_EXECUTE') or die("Access Denied."); 
	
$hide = "class='hide'";
$show = "class='show'";

Loader::model("on_deck");
$user = Loader::helper('user');
$link = BASE_URL.View::url("/ondeck?odid=".$ondeck->ID);
if(!$user->isLoggedIn()) {
	$link = BASE_URL.View::url("/login?redirect=".$link);
}

if($ondeck->IsLive) {
	echo "<div id='navbar_events_live_top' class='live ondeck'><a href='".$link."'>live</a></div>";	
}
else
if($ondeck->IsOpen) {
	echo "<div id='navbar_events_open_top' class='open ondeck'><a href='".$link."'>open</a></div>";	
}
else {
	echo "<div id='navbar_events_offline_top' class='offline ondeck'><a href='/events'>off air</a></div>";	
}
echo "<input type='hidden' id='hostid' name='hostid' value='".$ondeck->HostID."'/>";

//echo "<a class='button' alt='Refresh Page' href='javascript:;' onclick='return ps.ondeck.refresh();'><i class='fa fa-refresh' aria-hidden='true'></i><br>Refresh</a>";
echo "<a class='button top navlogo' alt='Return to Events' href='".View::url("/events")."'>";
//echo "<img src='".$this->getThemePath()."/images/ps_logo_only_small.png' class='navlogo'>";
echo "</a>";

if(!$ondeck->isEnded() || $isLocationAdmin) {
	//$link = "href='javascript:;' onclick='ps.ondeck.showSession();'";
	$link = "href='/ondeck?odid=".$ondeck->ID."&view=session'";
	echo "<a id='ondeck_session_button' class='navbutton button ".($view == "session" ? 'selected' : '')." ".($ondeck->IsLive ? "live" : "")."' alt='Session View' ".$link."><i class='fa fa-television' aria-hidden='true'></i><br>".($ondeck->IsLive ? "LIVE" : "Session")."</a>";
}
//$link = "href='javascript:;' onclick='ps.ondeck.showList();'";
$link = "href='/ondeck?odid=".$ondeck->ID."&view=list'";
echo "<a id='ondeck_list_button' class='navbutton button ".($view == "list" ? 'selected' : '')."' alt='List View' ".$link."><div id='ondeck_list_icon' class='".($ondeck->IsOpen ? "" : "closed")."'></div><div class='ondeck_nav_label'>List</div></a>";//<i class='fa fa-list-alt' aria-hidden='true'></i>
if($isLocationAdmin) {
	//$link = "href='javascript:;' onclick='ps.ondeck.showUsers();'";
	$link = "href='/ondeck?odid=".$ondeck->ID."&view=users'";
	echo "<a id='ondeck_users_button' class='navbutton button ".($view == "users" ? 'selected' : '')."' alt='Users View' ".$link."><i class='fa fa-user-plus' aria-hidden='true'></i><br>Add</a>";
}
if($thisUserIsOnList) {
	if(!$thisUserOnDeck->HasEntries) {
		if(!$isLocationAdmin) {
			echo "<a class='navbutton button' ".ajax('list', 'cmd_remove_me', 1)."><i class='fa fa-times' aria-hidden='true'></i><br>Remove Me</a>";
		}
	}
	else
	if($thisUserOnDeck->IsCrossedOff) {
		echo "<a class='navbutton button' ".ajax('list', 'cmd_readd', 1)."><i class='fa fa-repeat' aria-hidden='true'></i><br>Re-Add Me</a>";
	}
	else 
	if(!$isLocationAdmin) {
		echo "<a class='navbutton button crossoff' ".ajax('list', 'cmd_cross_off', 1)."><i class='fa fa-strikethrough' aria-hidden='true'></i><br>Cross Off</a>";
	}
}
else
if($isLocationAdmin || $ondeck->IsOpen) {
	//echo 'REQ:'.$event->IsTicketRequired();
	if(($tickets || (!$event || !$event->IsTicketRequired()) || $isLocationAdmin) && !$thisUserIsOnList) {
		echo "<a class='button blue_bg' ".ajax('list', 'cmd_add_me', 1)."><i class='fa fa-plus' aria-hidden='true'></i><br>Add Me</a>";
	}
	else 
	if($event && $event->IsTicketRequired()) {
		if($unusedTickets) {
			echo "<a class='button blue_bg' ".ajax('list', 'cmd_use_ticket', 1)."><i class='fa fa-ticket' aria-hidden='true'></i><br>Use Ticket</a>";
		}
		else {
			echo "<a class='button blue_bg' href='".View::url("/eventinfo?id=".$ondeck->EventID)."'><i class='fa fa-ticket' aria-hidden='true'></i><br>Buy Ticket</a>";
		}
	}
}

if($isLocationAdmin) {
	echo "<a class='navbutton button' href='javascript:;' onclick='ps.ondeck.showPresaleTickets();'><i class='fa fa-ticket' aria-hidden='true'></i><br>Presales</a>";
	echo "<hr>";
	echo "<div id='ondeck_nav_list' ".($view == "list" ? $show : $hide).">";
	if(!$ondeck->IsOpen) {
		echo "<a class='navbutton button' ".ajax('verify', 'cmd_open_deck')."><i class='fa fa-lock' aria-hidden='true'></i><br>Locked</a>";
	}
	else {
		echo "<a class='navbutton button' ".ajax('verify', 'cmd_close_deck')."><i class='fa fa-unlock' aria-hidden='true'></i><br>Is Open</a>";
	}
	if($count > 0) {
		echo "<a class='navbutton button' ".ajax('verify', 'cmd_clear_deck', 1, 'ps.ondeck.confirmClearDeck()')."><i class='fa fa-eraser' aria-hidden='true'></i><br>Reset</a>";
	}
	echo "</div>";

	echo "<div id='ondeck_nav_session' ".($view == "session" ? $show : $hide).">";
	if($ondeck->IsLive) {
		if(!$usersOnDeck['next']) {
			echo "<a class='navbutton button' ".ajax('verify', 'cmd_new_round')."><i class='fa fa-spinner' aria-hidden='true'></i><br>New Round</a>";
		}
		if(!$ondeck->IsOnBreak) {
			echo "<a class='navbutton button' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i><br>Break</a>";
		}
		else {
			echo "<a class='navbutton button' ".ajax('verify', 'cmd_resume')."><i class='fa fa-clock-o' aria-hidden='true'></i><br>Resume</a>";
		}
		echo "<a class='navbutton button' ".ajax('verify', 'cmd_end_deck')."><i class='fa fa-minus-circle' aria-hidden='true'></i><br>End</a>";
	}
	else {
		if($hasEnded) {
			echo "<a class='navbutton button' ".ajax('verify', 'cmd_start_deck')."><i class='fa fa-power-off' aria-hidden='true'></i><br>Restart</a>";
		}
		else {
			echo "<a class='navbutton button' ".ajax('verify', 'cmd_start_deck')."><i class='fa fa-power-off' aria-hidden='true'></i><br>Start</a>";
		}
	}
	echo "</div>";
	
}
if($isLocationAdmin) {
	echo "<div id='ondeck_nav_settings'>";
	echo "<a class='navbutton button' href='javascript:;' onclick='return ps.ondeck.settings();'><i class='fa fa-cog' aria-hidden='true'></i><br>Settings</a>";
	echo "</div>";
}

if($hostProfile) {
	//$link = $hostProfile ? $this->url('/profile','view', $hostProfile->uID) : null;
	$img = $hostProfile ? $hostProfile->getAvatar(false) : UserProfile::getDefultAvatar(false);
	//echo "<a id='ondeck_host' href='".$link."'>";
	echo "<a id='ondeck_host' href='javascript:;' onclick='return ps.ondeck.profile(".$hostProfile->uID.")'>";
	echo "<div class='aldrich small gray'>Host</div>";
	echo "	<img src='".$img."'>";
	echo "<div class='aldrich small'>".$hostProfile->uArtistName."</div>";
	echo "</a>";
}

echo "<a class='help_popup_open navbutton button purple' alt='Show Help'><i class='fa fa-question-circle-o' aria-hidden='true'></i><br>Help</a>";
echo "<a class='navbutton button purple' alt='My Profile' href='".$this->url("/profile")."'><i class='fa fa-user' aria-hidden='true'></i><br>Profile</a>";
echo "<a class='navbutton button purple' alt='Log Out' href='".$this->url("/login/logout")."' onclick='return ps.ondeck.confirmLogout();'><i class='fa fa-sign-out' aria-hidden='true'></i><br>Logout</a>";

if($isLocationAdmin && !$ondeck->EventID) {
	echo "<a class='navbutton button' href='".View::url("/ondeck?odid=".$ondeck->ID."&cmd_delete_deck=1")."' onclick='return ps.ondeck.validateDelete();'><i class='fa fa-trash-o' aria-hidden='true'></i><br>Delete</a>";
}
?>

