<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('user_purchase');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');

Loader::helper('ajax');
$form = Loader::helper('form'); 
$events = Loader::helper('events'); 
?>

<div id="ondeck_list">
<?php

displayList($data, true);
displayList($data, false);

function displayList($data, $fullsize=false) {
	extract($data);
	$url = View::url("/ondeck")."?ajax=list&odid=".$ondeck->ID;
	
	if($fullsize) {
		echo "<div class='full left hidden-xs'>";
	}
	else {
		echo "<div class='full left visible-xs'>";
	}
	

	$list = $usersOnDeck['all'];
	if(!$count) {
		echo "<div class='alert alert-white center'><i><b>You are the first one here!</b>";
		echo $addme;
		echo "<br><br></i></div>";
	}
	else {
		echo $addme;
		if($count < 10) {
			$i = 10 - $count;
			$empties = array();
			for($x = 0; $x < $i; $x++) {
				$empties[] = null;
			}
			$list = array_merge($list, $empties);
		}
	
		$strikethru = null;
		if($countPlayable != $count) {
			$strikethru = "<div class='strikethrough inline gray'>".$count."</div>&nbsp;";
		}
		
// 		if(count($usersOnDeck['breaks'])) {
// 			$b = new UserOnDeck();
// 			$b->UserID = -2;
// 			
// 			$b->Entries = array();
// 			foreach($usersOnDeck['breaks'] as $break) {
// 				$b->Entries[$break->RoundNum] = $break;
// 			}
// 			$list[] = $b;
// 			//foreach($usersOnDeck['breaks'] as $break) {
// 			//}
// 		}

		if($ondeck->IsLive) {
			$remaining = "";
			if($ondeck->IsLive && $count) {
				$rem = 0;
				foreach($usersOnDeck['playable'] as $p) {
					if($p->OrderNum > $ondeck->CurrentNum) {
						$rem++;
					}
				}
				$remaining = $rem." Remaining";
			}

			echo "<table id='ondeck_list_header' class='hidden-xs'>";
				echo "<tr>";
				echo "<td id='ondeck_header_now_playing'>".$strikethru.$countPlayable." Total</td>";
				echo "<td id='ondeck_header_status'>".$userStatus."</td>";
				echo "<td id='ondeck_header_count'>";
				echo $remaining;
				echo "</td>";
				echo "</tr>";
			echo "</table>";

			echo "<div class='visible-xs'>";
			echo "<div id='ondeck_header_status'>".$userStatus."</div>";
			echo "</div>";

			echo "<table id='ondeck_list_header' class='visible-xs'>";
				echo "<tr>";
				echo "<td id='ondeck_header_now_playing'>".$strikethru.$countPlayable." Total</td>";
				echo "<td id='ondeck_header_count'>";
				echo $remaining;
				echo "</td>";
				echo "</tr>";
			echo "</table>";
		}
	}
	
	echo "<div class='center'>";
	echo "<a id='ondeck_list_to_session' class='gray' alt='Goto Session View' href='javascript:;' onclick='ps.ondeck.showSession();'><i class='fa fa-chevron-up' aria-hidden='true'></i> Session View</a>";
	echo "</div>";
	
	echo "<div class='right'>";
	echo "<a id='ondeck_list_edit' class='ondeck_list_edit iconbutton right' alt='Edit List' href='javascript:;' onclick='ps.ondeck.toggleListEditMode();'><i class='fa lg fa-pencil-square-o' aria-hidden='true'></i></a>";
	echo "</div>";

	if($ondeck->RoundNum > 1) {
		echo "<div id='ondeck_list_rounds'>";
		echo "<div class='ondeck_list_round_label'>ROUNDS </div>";
		for($r = 1; $r <= $ondeck->RoundNum; $r++) {
			$selected = $r == $view_round ? "selected" : "";
			echo "<a ".ajax('list', 'view_round', $r)." class='ondeck_list_round_button ".$selected."'>".$r."</a>";
		}
		echo "</div>";
	}

	echo "<div id='ondeck_list_scroll".($fullsize ? "":"_mobile")."' class='ondeck_list_scroll'>";
	echo "<div id='ondeck_list_table_inset'>";
	if($count) {
		echo "<table id='ondeck_list_table'>";
		
		if($fullsize) {
			echo "<tr>";
				echo "<th class='ondeck_list_num'>#</th>";
				echo "<th class='ondeck_list_avatar'> </th>";
				echo "<th class='ondeck_list_name'>Artist Name</th>";
				echo "<th class='ondeck_list_start'>Start</th>";
				echo "<th class='ondeck_list_end'>End</th>";
				echo "<th class='ondeck_list_start_edit'>Start</th>";
				echo "<th class='ondeck_list_end_edit'>End</th>";
				echo "<th class='ondeck_list_duration'>Duration</th>";
				if($isLocationAdmin) {
					echo "<th class='ondeck_list_controls hidden'>";
					echo "</th>";
				}
			echo "</tr>"; 
		}
		else {
			echo "<tr>";
				echo "<th>";
					echo "<div class='ondeck_list_num'>#</div>";
					echo "<div class='ondeck_list_avatar'> </div>";
					echo "<div class='ondeck_list_name'>Artist Name</div>";
				echo "</th>"; 
			echo "</tr>";  
		}
		
		if($view_round != $ondeck->RoundNum) {
		// Reorder the users in their original order for the round recorded
			$otherRound = OnDeckRound::getByRoundNum($ondeck->ID, $view_round);
			$newOrder = array();
			foreach($list as $us) {
				if($us) {
					$entry = $us->Entries[$view_round];
					$us->OrderNum = $entry->OrderNum;
					$newOrder[$us->OrderNum] = $us;
				}
			}
			ksort($newOrder);
			$list = $newOrder;
		}
		
		$i = 1;
		foreach($list as $us) {
			$entry = null;
			$turnOver = false;
			$duration = $end = $start = "--:--"; 
			
			if($us) {
				$entry = $us->Entries[$view_round];
			}
			
			if($entry || $view_round == $ondeck->RoundNum) {
				if($entry) {
					$start = $entry->StartTime;
					$end = $entry->EndTime;
					$duration = $entry->Timer;

					if($start == "0000-00-00 00:00:00") {
						$start = "--:--";
					}
					else {
						$t = strtotime($start);
						$start = date("g:ia", $t);
					}
					if($end == "0000-00-00 00:00:00") {
						$end = "--:--";
					}
					else {
						$t = strtotime($end);
						$end = date("g:ia", $t);
						$turnOver = true;
					}
					if($duration = 0) {
						$duration = "--:--";
					}
					else
					if($start != "--:--" && $end != "--:--") {
						$d = strtotime($entry->EndTime) - strtotime($entry->StartTime);
						$dm = floor($d / 60);
						$ds = $d - ($dm * 60.0);
						if($dm < 10) $dm = "0".$dm;
						if($ds < 10) $ds = "0".$ds;
						$duration = $dm.":".$ds;
					}
					else {
						$d = abs($entry->Timer);
						$m = floor(floatval($d) / 60.0);
						$s = intval($d) - ($m * 60);
						if($m == 0) $m = "00";
						else if ($m < 10) $m = "0".$m;
						if($s == 0) $s = "00";
						else if ($s < 10) $s = "0".$s;
						$duration = $m.":".$s;//." [".$entry->Timer."]";
					}
				}
	
				$class = null;
				if($us && $ondeck->IsLive && !$ondeck->IsOnBreak) {
					if($view_round == $ondeck->RoundNum) {
						//if(!$usersOnDeck['next']) {
						//	$class = null; // end of round
						//}
						//else
						if($usersOnDeck['current'] && $us->ID == $usersOnDeck['current']->ID) {
							$class = "class='current'";
						}
						else
						if($usersOnDeck['next'] && $us->ID == $usersOnDeck['next']->ID) {
							$class = "class='next'";
						}
						else
						if($usersOnDeck['second'] && $us->ID == $usersOnDeck['second']->ID) {
							$class = "class='second'";
						}
						else
						if($turnOver) {
							$class = "class='turnover'";
						}
					}
				}

				$avatar = null;
				$profileLink = null;
				if($us) {
					$p = UserProfile::getByUserID($us->UserID);
					if($p) {
						$img = $p->getAvatar(false);
						$profileLink = View::url("/profile", $p->uID);
					}
					else {
						$img = UserProfile::getDefultAvatar(false);
					}
					//if($profileLink) $avatar .= "<a href='".$profileLink."'>";
					$avatar .= "<a href='javascript:;' onclick='return ps.ondeck.profile(".$p->uID.");'>";
					$avatar .= "<img src='".$img."'>";
					$avatar .= "</a>";
					//if($profileLink) $avatar .= "</a>";
				}
				else {
					$avatar = "<div class='placeholder'></div>";
				}
			
				$strikethru = $us->IsCrossedOff ? "strikethrough" : "";

				$name = ($us ? $us->Name : "");
				if($fullsize && $us->ID == $usersOnDeck['current']->ID) {
					$name .= "<span class='ondeck_list_name_nowplaying'>";
					if($ondeck->IsLive) {
						$name .= "Now Playing";
					}
					else {
						$name .= "Up First";
					}
					$name .= "</div>";
				}

				$controls = null;
				if($isLocationAdmin) {// && $view_round == $ondeck->RoundNum) {
					if($us) {
						$roundStarted = $round && $round->isStarted();
						//if(!$round || !$round->isComplete()) {
							$nextOrder = $count;
							if($usersOnDeck['second']) {
								$nextOrder = $usersOnDeck['second']->OrderNum;
							}
							else
							if($usersOnDeck['next']) {
								$nextOrder = $usersOnDeck['next']->OrderNum;
							}

							if(!$roundStarted) {
								$controls .= "<a class='iconbutton' alt='Move to Top' ".ajax('list', 'cmd_move_top', $us->ID)."><i class='fa lg fa-angle-double-up' aria-hidden='true'></i></a>";
							}
							else {
								$controls .= "<a class='iconbutton disabled' alt='Move to Top'><i class='fa lg fa-angle-double-up' aria-hidden='true'></i></a>";
							}
							if($us != $usersOnDeck['last'] && (!$roundStarted || $us->OrderNum > $nextOrder)) {
								$controls .= "<a class='iconbutton' alt='Move to Bottom' ".ajax('list', 'cmd_move_bottom', $us->ID)."><i class='fa lg fa-angle-double-down' aria-hidden='true'></i></a>";
							}
							else {
								$controls .= "<a class='iconbutton disabled' alt='Move to Bottom'><i class='fa lg fa-angle-double-down' aria-hidden='true'></i></a>";
							}
							if(!$ondeck->isStarted() || (!$roundStarted || $us->OrderNum > $usersOnDeck['current']->OrderNum+1)) {
								$controls .= "<a class='iconbutton' alt='Move Up One' ".ajax('list', 'cmd_move_up', $us->ID)."><i class='fa lg fa-arrow-circle-up' aria-hidden='true'></i></a>";
							}
							else {
								$controls .= "<a class='iconbutton disabled' alt='Move Up One'><i class='fa lg fa-arrow-circle-up' aria-hidden='true'></i></a>";
							}
							if($us != $usersOnDeck['last'] && (!$roundStarted || $us->OrderNum > $usersOnDeck['current']->OrderNum)) {
								$controls .= "<a class='iconbutton' alt='Move Down One' ".ajax('list', 'cmd_move_down', $us->ID)."><i class='fa lg fa-arrow-circle-down' aria-hidden='true'></i></a>";
							}
							else {
								$controls .= "<a class='iconbutton disabled' alt='Move Down One'><i class='fa lg fa-arrow-circle-down' aria-hidden='true'></i></a>";
							}
							if(!count($us->Entries) && (!$roundStarted || $us->OrderNum > $usersOnDeck['current']->OrderNum)) {
								$controls .= "<a class='iconbutton' alt='Remove' ".ajax('verify', 'cmd_remove_user', $us->ID, "ps.ondeck.validateRemoveUser()")."'><i class='fa lg fa-times' aria-hidden='true'></i></a>";
							}
							else
							if($us->IsCrossedOff) {
								$controls .= "<a class='iconbutton' alt='Re-add' ".ajax('list', 'cmd_readd', $us->ID)."><i class='fa lg fa-repeat' aria-hidden='true'></i></a>";
							}
							else {
								$controls .= "<a class='iconbutton crossoff' alt='Cross Off' ".ajax('list', 'cmd_cross_off', $us->ID)."><i class='fa lg fa-strikethrough' aria-hidden='true'></i></a>";
							}
						//}
					}
				}
			
				if($fullsize) {
					echo "<tr ".$class.">";
					echo "<td id='listnum_".$i."' class='ondeck_list_num'><div class='ondeck_list_num_circle'>".$i."</div></td>";
					echo "<td class='ondeck_list_avatar'>";
					echo $avatar;
					echo "</td>";
					echo "<td class='ondeck_list_name ".$strikethru."'>";
					echo $name;
					echo "</td>";
					echo "<td class='ondeck_list_start'>".$start."</td>";
					echo "<td class='ondeck_list_end'>".$end."</td>";
					echo "<td class='ondeck_list_start_edit hidden'><input class='ondeck_list_start_input' type='text' value='".$start."' data-id='".$entry->ID."'/></td>";
					echo "<td class='ondeck_list_end_edit hidden'><input class='ondeck_list_end_input' type='text' value='".$end."' data-id='".$entry->ID."'/></td>";
					echo "<td class='ondeck_list_duration'>".$duration."</td>";
					if($isLocationAdmin) {
						echo "<td class='ondeck_list_controls hidden'>";
						echo $controls;
						echo "</td>";
					}
					echo "</tr>";
				}
				else {
					echo "<tr ".$class.">";
						echo "<td id='listnum_mobile_".$i."'>";
							//echo "<div class='row'>";
							//	echo "<div class='col-md-6'>";
								echo "<div class='clear full'>";
									echo "<div class='ondeck_list_num'>";
									echo $i;
									echo "</div>";
							
									echo "<div class='ondeck_list_avatar'>";
									echo $avatar;
									echo "</div>";
							
									echo "<div class='ondeck_list_name'>";
										echo "<div class='left $strikethru'>".$name."</div>";
										echo "<div class='clear full'>";
											echo "<div class='ondeck_list_start'>".$start."</div>";
											echo "<div class='ondeck_list_end'>".$end."</div>";
											echo "<div class='ondeck_list_start_edit hidden'><input class='ondeck_list_start_input' type='text' value='".$start."' data-id='".$entry->ID."'/></div>";
											echo "<div class='ondeck_list_end_edit hidden'><input class='ondeck_list_end_input' type='text' value='".$end."' data-id='".$entry->ID."'/></div>";
											echo "<div class='ondeck_list_end'>".$duration."</div>";
										echo "</div>"; 
									echo "</div>";

									if($isLocationAdmin) {
										echo "<div class='ondeck_list_controls hidden'>";
										echo $controls;
										echo "</div>";
									}
								echo "</div>";
							//	echo "<div class='col-md-6'>";
							//echo "</div>"; 
						echo "</td>"; 
					echo "</tr>";
				}
				
				//echo "breaks:".count($usersOnDeck['breaks']);
				if(count($usersOnDeck['breaks'])) {
					foreach($usersOnDeck['breaks'] as $break) {
						//echo "break:".$break->RoundNum." :".$view_round."<br>";

						if($break->RoundNum == $view_round) {
							$nextOrder = $us->OrderNum + 1;
							//echo "break:".$break->OrderNum." us:".$us->OrderNum." next:".$nextOrder."<br>";
							if($break->OrderNum > $us->OrderNum && $break->OrderNum < $nextOrder) {
								$start = $break->StartTime;
								$end = $break->EndTime;
								$duration = 0;
								$ended = false;

								if($start == "0000-00-00 00:00:00") {
									$start = "--:--";
								}
								else {
									$t = strtotime($start);
									$start = date("g:ia", $t);
								}
								if($end == "0000-00-00 00:00:00") {
									$end = "--:--";
								}
								else {
									$t = strtotime($end);
									$end = date("g:ia", $t);
									$ended = true;
								}
								if(!$ended) {
									$duration = "--:--";
								}
								else {
									$d = strtotime($break->EndTime) - strtotime($break->StartTime);
									$m = floor(floatval($d) / 60.0);
									$s = intval($d) - ($m * 60);
									if($m == 0) $m = "00";
									else if ($m < 10) $m = "0".$m;
									if($s == 0) $s = "00";
									else if ($s < 10) $s = "0".$s;
									$duration = $m.":".$s;//." [".$d."]";
								}
								if($fullsize) {
									echo "<tr class='ondeck_break_row'>";
									echo "<td class='ondeck_list_num'><div class='ondeck_list_num_circle'>&nbsp;</div></td>";
									echo "<td class='ondeck_list_avatar'>";
									echo "</td>";
									echo "<td class='ondeck_list_name'>BREAK";
									echo "</td>";
									echo "<td class='ondeck_list_start'>".$start."</td>";
									echo "<td class='ondeck_list_end'>".$end."</td>";
									echo "<td class='ondeck_list_start_edit hidden'><input class='ondeck_list_start_input' type='text' value='".$start."' data-id='".$entry->ID."'/></td>";
									echo "<td class='ondeck_list_end_edit hidden'><input class='ondeck_list_end_input' type='text' value='".$end."' data-id='".$entry->ID."'/></td>";
									echo "<td class='ondeck_list_duration'>".$duration."</td>";
// 									if($isLocationAdmin) {
// 										echo "<td class='ondeck_list_controls hidden'>";
// 										echo $controls;
// 										echo "</td>";
// 									}
									echo "</tr>";
								}
								else {
									echo "<tr class='ondeck_break_row'>";
										echo "<td>";
										echo "<div class='clear full'>";
											echo "<div class='ondeck_list_num'>&nbsp;";
											echo "</div>";
					
											echo "<div class='ondeck_list_avatar'>";
											echo "</div>";
					
											echo "<div class='ondeck_list_name'>";
												echo "<div class='left'>BREAK</div>";
												echo "<div class='clear full'>";
													echo "<div class='ondeck_list_start'>".$start."</div>";
													echo "<div class='ondeck_list_end'>".$end."</div>";
													echo "<div class='ondeck_list_start_edit hidden'><input class='ondeck_list_start_input' type='text' value='".$start."' data-id='".$entry->ID."'/></div>";
													echo "<div class='ondeck_list_end_edit hidden'><input class='ondeck_list_end_input' type='text' value='".$end."' data-id='".$entry->ID."'/></div>";
													echo "<div class='ondeck_list_duration'>".$duration."</div>";
												echo "</div>"; 
											echo "</div>";

// 											if($isLocationAdmin) {
// 												echo "<div class='ondeck_list_controls hidden'>";
// 												echo $controls;
// 												echo "</div>";
// 											}
										echo "</div>";
										echo "</td>"; 
									echo "</tr>";
								}
							}
						}
					}
				}

			}//entry
			$i++;
		}
		echo "<tr><td><br class='clear'></td></tr>";
		echo "<tr><td><br class='clear'></td></tr>";
		echo "<tr><td><br class='clear'></td></tr>";
		echo "<tr><td><br class='clear'></td></tr>";
		echo "</table>";
		echo "<br class='clear'>";
		echo "<div id='ondeck_user_endspace'>&nbsp;</div>";
	}
	echo "</div>";
	echo "</div>";
	
	echo "</div>";
}

if($isLocationAdmin) {
	echo "<div class='ondeck_fixed_bottom_align'>";
	echo "<a id='ondeck_list_to_users' alt='Add Users' href='javascript:;' onclick='ps.ondeck.showUsers();'><i class='fa fa-chevron-down' aria-hidden='true'></i> Add Users</a>";
	echo "</div>";
}
?>
</div>
