<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('user_purchase');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');

$form = Loader::helper('form'); 
$events = Loader::helper('events'); 
?>

<?php
	if($addme) echo $addme;
	if($ondeck->IsLive) {
		if(!$userStatus) {
			$userStatus = "Now Playing ".$ondeck->CurrentNum." of ".$countPlayable;
		}
		echo "<table id='ondeck_list_header' class='hidden-xs'>";
			echo "<tr>";
			if($ondeck->IsOnBreak) {
				echo "<td id='ondeck_header_now_playing'>On Break</td>";
			}
			else {
				echo "<td id='ondeck_header_now_playing'>";
				//echo "Up Now ";
				//echo $ondeck->CurrentNum." of ".$countPlayable;
				echo "</td>";
			}
			echo "<td id='ondeck_header_status'>".$userStatus."</td>";
			echo "<td id='ondeck_header_count'>";
			//echo $ondeck->CurrentNum." of ".$countPlayable;//." (".$ondeck->RoundNum.")";
			echo "</td>";
			echo "</tr>";
		echo "</table>";

		echo "<div class='visible-xs'>";
		echo "<div id='ondeck_header_status'>".$userStatus."</div>";
		echo "</div>";

		echo "<table id='ondeck_list_header' class='visible-xs'>";
			echo "<tr>";
			echo "<td id='ondeck_header_now_playing'>Now Playing</td>";
			echo "<td id='ondeck_header_count'>";
			echo $ondeck->CurrentNum." of ".$countPlayable;//." (".$ondeck->RoundNum.")";
			echo "</td>";
			echo "</tr>";
		echo "</table>";
	}
?>
<?php
	$url = $this->url("/ondeck")."?odid=".$ondeck->ID;

	$alert = "danger";
	if(!$ondeck->IsLive && $ondeck->IsOpen) {
		$status = "Open for sign-ups. Session will start at ".$startTime.".";
		$alert = "success";
	}
	else
	if($ondeck->IsLive && $ondeck->IsOpen) {
		$status = "Open for sign-ups. Session started at ".date("g:ia", strtotime($ondeck->StartTime));
		$alert = "warning";
	}
	else
	if($ondeck->IsLive && !$ondeck->IsOpen) {
		$status = "List closed. Session started at ".date("g:ia", strtotime($ondeck->StartTime));
	}
	else 
	if($hasEnded) {
		$status = "Session Ended. From ".date("g:ia", strtotime($ondeck->StartTime))." to ".date("g:ia", strtotime($ondeck->EndTime));
		$addtionalMsg = null;
		$alert = "info";
	}
	else {
		$status = "Session Closed";
	}

	$newLayout = true;
	if(!$ondeck->IsLive) {
		//echo "<script>setTimeout(function() { location.reload(); }, 30000);</script>";
		
		echo "<div class='ps_logo_ondeck_large'></div>";
		echo "<div id='ondeck_session_status' class='alert alert-white center' style='width:400px; max-width:100%; z-index:100;'>";
		echo "<a href='javascript:;' onclick='ps.hide(\"ondeck_session_status\");' class='right'><i class='fa fa-times' aria-hidden='true'></i></a>";
		if($ondeck->isEnded()) {
			echo "<h3>The session ended at ".date("g:ia", strtotime($ondeck->EndTime))."</h3>";
		}
		else {
			echo "<h3>The session has not started yet</h3>";
		}
		echo "<div>There are ".$countPlayable." members in the list.</div>";
		if($isLocationAdmin) {
			echo "<br>";
			if($hasEnded) {
				echo "<a class='button' ".ajax('verify', 'cmd_start_deck')."><i class='fa fa-power-off' aria-hidden='true'></i> Restart</a>";
				if($ondeck->BlogPost) {
					Loader::model("member_feature");
					$blog = MemberFeature::getByID($ondeck->BlogPost);
					if($blog) {
						echo "<a class='button pad-left' href='/blog/".$blog->handle."'><i class='fa fa-id-card' aria-hidden='true'></i> View Blog Post</a>";
					}
				}
				else {
					echo "<a class='button pad-left' href='/manage/features/edit?ondeck=".$ondeck->ID."'><i class='fa fa-id-card' aria-hidden='true'></i> Create Blog Post</a>";
				}
			}
			else {
				echo "<a class='button' ".ajax('verify', 'cmd_start_deck')."><i class='fa fa-power-off' aria-hidden='true'></i> Start the Session</a>";
			}
			echo "<br class='clear'>";
		}
		else {
			if($ondeck->BlogPost) {
				Loader::model("member_feature");
				$blog = MemberFeature::getByID($ondeck->BlogPost);
				if($blog) {
					echo "<a class='button pad-left' href='/blog/".$blog->handle."'><i class='fa fa-id-card' aria-hidden='true'></i> View Blog Post</a>";
				}
			}
		}
		echo "</div>";
		
		if($isLocationAdmin) {
			echo "<a class='center suggestions_popup_open white' alt='Hosting Suggestions'><i class='fa fa-hand-o-right' aria-hidden='true'></i> Hosting Suggestions</a>";	
		}
		echo "<input type='hidden' id='current_entry_id' value='0'/>";
		echo "<input type='hidden' id='countdown_autostart' value='".$location->Autostart."'/>";
		echo "<input type='hidden' id='countdown_limit' value='".$countdown."'/>";
		echo "<input type='hidden' id='countdown_running' value='0'/>";
	}
	else {
		if($newLayout) {
			echo "<div id='ondeck_session'>";
				echo "<table id='ondeck_session_split'>";
					echo "<tr>";
					if($ondeck->IsOnBreak) {
						echo "<td colspan='2' class='ondeck_session_break'>";
						echo "<div class='center'><h3 class='white'>Taking a short break.</h3>";
						echo "<br class='clear'>";
						echo "<div class='ps_logo_ondeck_large'></div>";
						if($isLocationAdmin) {
							echo "<br class='clear'>";
							echo "<a class='button' ".ajax('verify', 'cmd_resume')."><i class='fa fa-power-off' aria-hidden='true'></i> ".($round && $round->isStarted() ? "Resume" : "Start")." Round ".$ondeck->RoundNum."</a>";
						}
						echo "</div>";
						echo "<br class='clear'>";
			
						echo "<input type='hidden' id='current_entry_id' value='0'/>";
						echo "<input type='hidden' id='countdown_autostart' value='".$location->Autostart."'/>";
						echo "<input type='hidden' id='countdown_limit' value='".$countdown."'/>";
						echo "<input type='hidden' id='countdown_running' value='0'/>";
					}
					else {
						echo "<td colspan='2' class='ondeck_session_nowplaying'>";
						echo "<table id='ondeck_session_split_top'>";
							echo "<tr>";
								echo "<td id='ondeck_session_split_avatar'>";
									$isGuest = true;
									$p = UserProfile::getByUserID($usersOnDeck['current']->UserID);
									if($p) {
										$img = $p->getAvatar(false, true);
										$isGuest = false;
									}
									else {
										$img = UserProfile::getDefultAvatar(false);
									}
									echo "<img id='ondeck_avatar_nowplaying' src='".$img."'>";
								echo "</td>";
								echo "<td class='ondeck_session_split_info70'>";
									echo "<table id='ondeck_current_details'>";
										$longName = null;
										if(strlen($usersOnDeck['current']->Name) > 22) $longName ="class='longname'";
										echo "<tr><td id='ondeck_current_name' ".$longName.">";
										$name = $p->uArtistName;
										if(!$name) {
											$name = $usersOnDeck['current']->Name;
										}
										echo "<span class='name'>".$name."</span>";
										
										if($p->uIsMember) {
											echo "<div class='right'><i class='fa fa-user-circle-o' aria-hidden='true'></a></div>";
										}
										
										echo "</td></tr>";
										if($p) {
											if($p->uSoundcloud) {
												$soundcloud = $p->uSoundcloud;
												$soundcloud = str_replace("http://", "", $soundcloud);
												$soundcloud = str_replace("https://", "", $soundcloud);
												$soundcloud = str_replace("www.", "", $soundcloud);
												$soundcloud = str_replace("soundcloud.com/", "", $soundcloud);
												$soundcloud = str_replace("soundcloud/", "", $soundcloud);
												echo "<tr><td id='ondeck_current_soundcloud'>";
												echo "soundcloud/".$soundcloud;
												echo "</td></tr>";
											}
											if($p->uStyleTags) {
												echo "<tr><td  id='ondeck_current_styletags'>";
												echo $p->uStyleTags;
												echo "</td></tr>";
											}
											//if($p->uSummary) {
											//	echo "<tr id='ondeck_current_summary'><td>";
											//	echo "<div class='alert alert-white'>";
											//	echo $p->uSummary;
											//	echo "</div>";
											//	echo "</td></tr>";
											//}
										}
										if(!$ondeck->IsOnBreak) {
											echo "<tr id='ondeck_current_stats'><td>";
											echo "Started at ".date("g:ia", strtotime($usersOnDeck['current']->LastEntry->StartTime));
											echo "</td></tr>";
										}
										echo "<tr id='ondeck_current_controls' class='hidden-xs'><td>";
										echo "<input type='hidden' id='current_entry_id' value='".$usersOnDeck['current']->LastEntry->ID."'/>";
										echo "<input type='hidden' id='countdown_autostart' value='".$location->Autostart."'/>";
										echo "<input type='hidden' id='countdown_limit' value='".$countdown."'/>";
										echo "<input type='hidden' id='countdown_running' value='".$usersOnDeck['current']->LastEntry->TimerRunning."'/>";
										
										//echo "countdown_running:".$usersOnDeck['current']->LastEntry->TimerRunning."<br>";
										
										$countdownTime = 0;
										$p = explode(":",  $countdown);
										$countdownTime = intval($p[0]) * 60 + intval($p[1]);
										$remainingSeconds = $countdownTime - $usersOnDeck['current']->LastEntry->Timer;
										if(!$remainingSeconds) {
											$remainingTime = $countdown;
										}
										else {
											$p = abs($remainingSeconds);
											$m = floor($p / 60); 
											$s = $p - ($m * 60);
											if($m == 0) $m = "00";
											else if($m < 10) $m = "0".$m;
											if($s == 0) $s = "00";
											else if($s < 10) $s = "0".$s;
											$remainingTime = ($remainingSeconds < 0 ? "-" : "").$m.":".$s;
										}
										$class = "";
										if($usersOnDeck['current']->LastEntry->TimerRunning) {
											if($remainingSeconds < 0) {
												$class = 'danger';
											}
											else
											if($remainingSeconds < 60) {
												$class = 'warning';
											}
											else {
												$class = "on";
											}
										}
										echo "<input type='hidden' id='countdownprogress' value='".$remainingSeconds."'/>";
										if($isLocationAdmin) {
											echo "<a id='countdown' class='".$class." ".($countdown ? "" : "hide")." left' href='javascript:;' onclick='return ps.ondeck.toggleCountdown();'>".$remainingTime."</a>";
											echo "<a id='countdown_reset' class='iconbutton gray left primary_host_only' href='javascript:;' onclick='return ps.ondeck.resetCountdown();'><i class='fa md fa-undo' aria-hidden='true'></i></a>";
										}
										else {
											echo "<a id='countdown' class='".$class." ".($countdown ? "" : "hide")."' href='javascript:;'>".$remainingTime."</a>";
										}
										echo "</td></tr>";
			
										echo "<tr id='ondeck_current_controls' class='hidden-xs'><td>";
										if($isLocationAdmin) {
											echo "<a class='button left pad-right white primary_host_only' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i> Break</a>";
										}
										if($isLocationAdmin && $usersOnDeck['next']) {
											echo "<a class='button left pad-right white primary_host_only' ".reloadPage('session', 'cmd_move_down', $usersOnDeck['current']->ID).">SKIP</a>";
										}
										if($isLocationAdmin && $ondeck->CurrentNum > 1) {
											echo "<a class='button left pad-right white primary_host_only' ".reloadPage('session', 'cmd_prev', 1).">PREV</a>";
										}
										if($isLocationAdmin && $usersOnDeck['next']) {
											//echo "<form 
											echo "<a class='button left primary_host_only' ".reloadPage('session', 'cmd_next', 1).">NEXT</a>";
										}
										echo "</td></tr>";
								
										echo "<tr class='visible-xs' style='width:100%; '>";
											echo "<td colspan='2' style='padding-top:5px;'>";
											if($isLocationAdmin && $usersOnDeck['next']) {
												echo "<a class='button right primary_host_only' ".reloadPage('session', 'cmd_next', 1).">NEXT</a>";
											}
											if($isLocationAdmin && $ondeck->CurrentNum > 1) {
												echo "<a class='button right pad-right white primary_host_only' ".reloadPage('session', 'cmd_prev', 1).">PREV</a>";
											}
											if($isLocationAdmin && $usersOnDeck['next']) {
												echo "<a class='button right pad-right white primary_host_only' ".reloadPage('session', 'cmd_move_down', $usersOnDeck['current']->ID).">SKIP</a>";
											}
											if($isLocationAdmin) {
												echo "<a class='button right pad-right white primary_host_only' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i> Break</a>";
											}
											echo "</td>";
										echo "</tr>";
									echo "</table>";
								
								//echo "<div class='alert-info'>MAINTENANCE: Please contact <a href='mailto:stephen@producerssocial.com'>stephen@producerssocial.com</a> if you are trying to use the system right now</div>";
								echo "</td>";
							echo "</tr>";
						echo "</table>";
					}
					echo "</td>";		
					echo "</tr>";
					if(!$usersOnDeck['next']) {
						echo "<tr>";
						echo "<td class='ondeck_session_bottom'>";
						echo "<div id='ondeck_next'>";
						echo "<div class='alert alert-info center'>";
						echo "<h2>End of round #".$round->RoundNum."!</h2>";
						if($isLocationAdmin) {
							echo "<a class='button center primary_host_only' ".ajax('verify', 'cmd_end_deck')."><i class='fa fg fa-minus-circle' aria-hidden='true'></i> End Session</a>&nbsp;";
							if(!$ondeck->IsOnBreak) {
								echo "<a class='button center primary_host_only' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i> Break</a>&nbsp;";
							}
							echo "<a class='button center primary_host_only' ".ajax('verify', 'cmd_new_round')."><i class='fa fg fa-spinner' aria-hidden='true'></i> New Round</a>";
							echo "<br class='clear'>";
						}
						echo "</div>";
						echo "</div>";
						echo "</td>";
						echo "</tr>";
					}
					else {
						echo "<tr>";
						echo "<td class='ondeck_session_next1'>";
							echo "<table id='ondeck_session_split_next1'>";
								echo "<tr>";
									echo "<td id='ondeck_next_tdleft'>";
									if($usersOnDeck['next']) {
										$isGuest = true;
										//$profileLink = null;
										$p = UserProfile::getByUserID($usersOnDeck['next']->UserID);
										if($p) {
											$img = $p->getAvatar(false);
											//$profileLink = View::url("/profile", $p->uID);
											$isGuest = false;
										}
										else {
											$img = UserProfile::getDefultAvatar(false);
										}
										//if($profileLink) echo "<a href='".$profileLink."'>";
										echo "<img id='ondeck_next_avatar' src='".$img."'>";
									}
									echo "</td>";
									echo "<td id='ondeck_next_tdright'>";
									if($usersOnDeck['next']) {
										echo "<div id='ondeck_next_heading'>On Deck</div>";
										echo "<div id='ondeck_next_name' class='clear'>";
										echo $usersOnDeck['next']->Name;
										echo "</div>";
										if($isLocationAdmin && $usersOnDeck['next']->ID != $usersOnDeck['last']->ID) {
											echo "<a class='button left clear small white pad-right primary_host_only' ".ajax('verify', 'cmd_move_down', $usersOnDeck['next']->ID).">SKIP</a>";
											if($ondeck->RoundNum > 1) {
												echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_cross_off', $usersOnDeck['next']->ID)."><i class='fa fa-strikethrough' aria-hidden='true'></i></a>";
											}
											else {
												echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_move_bottom', $usersOnDeck['next']->ID).">SEND TO END</a>";
											}
										}
									}
									echo "</td>";
								echo "</tr>";
							echo "</table>";
						echo "</td>";		
						echo "<td class='ondeck_session_next2'>";
							echo "<table id='ondeck_session_split_next2'>";
								echo "<tr>";
									echo "<td id='ondeck_next_tdleft2'>";
									if($usersOnDeck['second']) {
										$isGuest = true;
										$p = UserProfile::getByUserID($usersOnDeck['second']->UserID);
										if($p) {
											$img = $p->getAvatar(false);
											$isGuest = false;
										}
										else {
											$img = UserProfile::getDefultAvatar(false);
										} 
										echo "<img id='ondeck_avatar_next2' src='".$img."'>";
									}
									echo "</td>";
									echo "<td id='ondeck_next_tdright2'>";
									if($usersOnDeck['second']) {
										echo "<div id='ondeck_next_heading'>Up After</div>";
										echo "<div id='ondeck_next_name' class='clear'>";
										echo $usersOnDeck['second']->Name;
										echo "</div>";
										if($isLocationAdmin && $usersOnDeck['second']->ID != $usersOnDeck['second']->ID) {
											echo "<a class='button left clear small white pad-right primary_host_only' ".ajax('session', 'cmd_move_down', $usersOnDeck['second']->ID).">SKIP</a>";
											if($ondeck->RoundNum > 1) {
												echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_cross_off', $usersOnDeck['second']->ID)."><i class='fa fa-strikethrough' aria-hidden='true'></i></a>";
											}
											else {
												echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_move_bottom', $usersOnDeck['second']->ID).">SEND TO END</a>";
											}
										}
									}
									echo "</td>";
								echo "</tr>";
							echo "</table>";
						echo "</td>	";	
						echo "</tr>";
					}
				echo "</table>";
				
		}
		else {
		// OLD LAYOUT
			if($ondeck->IsOnBreak) {
				echo "<div class='center'><h3 class='white'>Taking a short break.</h3>";
				echo "<br class='clear'>";
				echo "<div class='ps_logo_ondeck_large'></div>";
				if($isLocationAdmin) {
					echo "<br class='clear'>";
					echo "<a class='button' ".ajax('verify', 'cmd_resume')."><i class='fa fa-power-off' aria-hidden='true'></i> ".($round && $round->isStarted() ? "Resume" : "Start")." Round ".$ondeck->RoundNum."</a>";
				}
				echo "</div>";
				echo "<br class='clear'>";
	
				echo "<input type='hidden' id='current_entry_id' value='0'/>";
				echo "<input type='hidden' id='countdown_autostart' value='".$location->Autostart."'/>";
				echo "<input type='hidden' id='countdown_limit' value='".$countdown."'/>";
				echo "<input type='hidden' id='countdown_running' value='0'/>";
			}
			else {
				echo "<div id='ondeck_now_playing'>";
					echo "<div id='ondeck_current'>";
						echo "<div id='ondeck_current_box'>";
						echo "<table id='ondeck_current_details'>";
							echo "<tr>";
							echo "<td id='ondeck_current_avatar'>";
							$isGuest = true;
							$p = UserProfile::getByUserID($usersOnDeck['current']->UserID);
							if($p) {
								$img = $p->getAvatar(false, true);
								$isGuest = false;
							}
							else {
								$img = UserProfile::getDefultAvatar(false);
							}
							echo "<img src='".$img."'>";
							echo "</td>";
			
							echo "<td id='ondeck_current_info'>";
	
							echo "<table id='ondeck_current_details'>";
								$longName = null;
								if(strlen($usersOnDeck['current']->Name) > 22) $longName ="class='longname'";
								echo "<tr><td id='ondeck_current_name' ".$longName.">";
								$name = $p->uArtistName;
								if(!$name) {
									$name = $usersOnDeck['current']->Name;
								}
								echo "<span class='name'>".$name."</span>";
								
								if($p->uIsMember) {
									echo "<div class='right'><i class='fa fa-user-circle-o' aria-hidden='true'></a></div>";
								}
								
								echo "</td></tr>";
								if($p) {
									if($p->uSoundcloud) {
										$soundcloud = $p->uSoundcloud;
										$soundcloud = str_replace("http://", "", $soundcloud);
										$soundcloud = str_replace("https://", "", $soundcloud);
										$soundcloud = str_replace("www.", "", $soundcloud);
										$soundcloud = str_replace("soundcloud.com/", "", $soundcloud);
										$soundcloud = str_replace("soundcloud/", "", $soundcloud);
										echo "<tr><td id='ondeck_current_soundcloud'>";
										echo "soundcloud/".$soundcloud;
										echo "</td></tr>";
									}
									if($p->uStyleTags) {
										echo "<tr><td  id='ondeck_current_styletags'>";
										echo $p->uStyleTags;
										echo "</td></tr>";
									}
									//if($p->uSummary) {
									//	echo "<tr id='ondeck_current_summary'><td>";
									//	echo "<div class='alert alert-white'>";
									//	echo $p->uSummary;
									//	echo "</div>";
									//	echo "</td></tr>";
									//}
								}
								if(!$ondeck->IsOnBreak) {
									echo "<tr id='ondeck_current_stats'><td>";
									echo "Started at ".date("g:ia", strtotime($usersOnDeck['current']->LastEntry->StartTime));
									echo "</td></tr>";
								}
	
								echo "<tr id='ondeck_current_controls' class='hidden-xs'><td>";
								if($isLocationAdmin && $usersOnDeck['next']) {
									//echo "<form 
									echo "<a class='button right primary_host_only' ".reloadPage('session', 'cmd_next', 1).">NEXT</a>";
								}
								if($isLocationAdmin && $ondeck->CurrentNum > 1) {
									echo "<a class='button right pad-right white primary_host_only' ".reloadPage('session', 'cmd_prev', 1).">PREV</a>";
								}
								if($isLocationAdmin && $usersOnDeck['next']) {
									echo "<a class='button right pad-right white primary_host_only' ".reloadPage('session', 'cmd_move_down', $usersOnDeck['current']->ID).">SKIP</a>";
								}
								if($isLocationAdmin) {
									echo "<a class='button right pad-right white primary_host_only' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i> Break</a>";
								}
								echo "<input type='hidden' id='current_entry_id' value='".$usersOnDeck['current']->LastEntry->ID."'/>";
								echo "<input type='hidden' id='countdown_autostart' value='".$location->Autostart."'/>";
								echo "<input type='hidden' id='countdown_limit' value='".$countdown."'/>";
								echo "<input type='hidden' id='countdown_running' value='".$usersOnDeck['current']->LastEntry->TimerRunning."'/>";
								
								//echo "countdown_running:".$usersOnDeck['current']->LastEntry->TimerRunning."<br>";
								
								$countdownTime = 0;
								$p = explode(":",  $countdown);
								$countdownTime = intval($p[0]) * 60 + intval($p[1]);
								$remainingSeconds = $countdownTime - $usersOnDeck['current']->LastEntry->Timer;
								if(!$remainingSeconds) {
									$remainingTime = $countdown;
								}
								else {
									$p = abs($remainingSeconds);
									$m = floor($p / 60); 
									$s = $p - ($m * 60);
									if($m == 0) $m = "00";
									else if($m < 10) $m = "0".$m;
									if($s == 0) $s = "00";
									else if($s < 10) $s = "0".$s;
									$remainingTime = ($remainingSeconds < 0 ? "-" : "").$m.":".$s;
								}
								$class = "";
								if($usersOnDeck['current']->LastEntry->TimerRunning) {
									if($remainingSeconds < 0) {
										$class = 'danger';
									}
									else
									if($remainingSeconds < 60) {
										$class = 'warning';
									}
									else {
										$class = "on";
									}
								}
								echo "<input type='hidden' id='countdownprogress' value='".$remainingSeconds."'/>";
								if($isLocationAdmin) {
									echo "<a id='countdown' class='".$class." ".($countdown ? "" : "hide")." left' href='javascript:;' onclick='return ps.ondeck.toggleCountdown();'>".$remainingTime."</a>";
									echo "<a id='countdown_reset' class='iconbutton gray left primary_host_only' href='javascript:;' onclick='return ps.ondeck.resetCountdown();'><i class='fa md fa-undo' aria-hidden='true'></i></a>";
								}
								else {
									echo "<a id='countdown' class='".$class." ".($countdown ? "" : "hide")."' href='javascript:;'>".$remainingTime."</a>";
								}
								echo "</td></tr>";
							echo "</table>";
						
							echo "</td>";
							echo "</tr>";
						
							echo "<tr class='visible-xs' style='width:100%; '>";
								echo "<td colspan='2' style='padding-top:5px;'>";
								if($isLocationAdmin) {
									echo "<a class='button left pad-right white primary_host_only' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i> Break</a>";
								}
								if($isLocationAdmin && $usersOnDeck['next']) {
									echo "<a class='button left pad-right white primary_host_only' ".reloadPage('session', 'cmd_move_down', $usersOnDeck['current']->ID).">SKIP</a>";
								}
								if($isLocationAdmin && $ondeck->CurrentNum > 1) {
									echo "<a class='button left pad-right white primary_host_only' ".reloadPage('session', 'cmd_prev', 1).">PREV</a>";
								}
								if($isLocationAdmin && $usersOnDeck['next']) {
									echo "<a class='button left primary_host_only' ".reloadPage('session', 'cmd_next', 1).">NEXT</a>";
								}
								echo "</td>";
							echo "</tr>";
						echo "</table>";
						
	
						echo "</div>";
			
					echo "</div>";//ondeck_current
				echo "</div>";
			}
			
			echo "<div id='ondeck_next_up'>";
			if(!$usersOnDeck['next']) {
				echo "<div id='ondeck_next'>";
				echo "<div class='alert alert-info center'>";
				echo "<h2>End of round #".$round->RoundNum."!</h2>";
				if($isLocationAdmin) {
					echo "<a class='button center primary_host_only' ".ajax('verify', 'cmd_end_deck')."><i class='fa fg fa-minus-circle' aria-hidden='true'></i> End Session</a>&nbsp;";
					if(!$ondeck->IsOnBreak) {
						echo "<a class='button center primary_host_only' ".ajax('verify', 'cmd_take_break')."><i class='fa fa-clock-o' aria-hidden='true'></i> Break</a>&nbsp;";
					}
					echo "<a class='button center primary_host_only' ".ajax('verify', 'cmd_new_round')."><i class='fa fg fa-spinner' aria-hidden='true'></i> New Round</a>";
					echo "<br class='clear'>";
				}
				echo "</div>";
				echo "</div>";
			}
			else {
				if($usersOnDeck['second']) {
					echo "<div class='row'>";
					echo "<div class='col-xs-6'>";
				}
				echo "<div id='ondeck_next_heading'>On Deck</div>";
				echo "<div id='ondeck_next'>";
					echo "<div id='ondeck_next_avatar'>";
					$isGuest = true;
					//$profileLink = null;
					$p = UserProfile::getByUserID($usersOnDeck['next']->UserID);
					if($p) {
						$img = $p->getAvatar(false);
						//$profileLink = View::url("/profile", $p->uID);
						$isGuest = false;
					}
					else {
						$img = UserProfile::getDefultAvatar(false);
					}
					//if($profileLink) echo "<a href='".$profileLink."'>";
					echo "<img src='".$img."'>";
					//if($profileLink) echo "</a>";
					echo "</div>";
	
					echo "<div id='ondeck_next_info'>";
						echo "<div id='ondeck_next_name'>";
						echo $usersOnDeck['next']->Name;
						echo "</div>";
						if($isLocationAdmin && $usersOnDeck['next']->ID != $usersOnDeck['last']->ID) {
							echo "<a class='button left clear small white pad-right primary_host_only' ".ajax('verify', 'cmd_move_down', $usersOnDeck['next']->ID).">SKIP</a>";
							if($ondeck->RoundNum > 1) {
								echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_cross_off', $usersOnDeck['next']->ID)."><i class='fa fa-strikethrough' aria-hidden='true'></i></a>";
							}
							else {
								echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_move_bottom', $usersOnDeck['next']->ID).">SEND TO END</a>";
							}
						}
					echo "</div>";
				echo "</div>";
				
				if($usersOnDeck['second']) {
					echo "</div>";
					echo "<div class='col-xs-6'>";
					echo "<div id='ondeck_next_heading'>Up After</div>";
					echo "<div id='ondeck_next'>";
						echo "<div id='ondeck_next_avatar'>";
						$isGuest = true;
						$p = UserProfile::getByUserID($usersOnDeck['second']->UserID);
						if($p) {
							$img = $p->getAvatar(false);
							$isGuest = false;
						}
						else {
							$img = UserProfile::getDefultAvatar(false);
						}
						echo "<img src='".$img."'>";
						echo "</div>";
		
						echo "<div id='ondeck_next_info'>";
						echo "<div id='ondeck_next_name'>";
						echo $usersOnDeck['second']->Name;
						echo "</div>";
						if($isLocationAdmin && $usersOnDeck['second']->ID != $usersOnDeck['second']->ID) {
							echo "<a class='button left clear small white pad-right primary_host_only' ".ajax('session', 'cmd_move_down', $usersOnDeck['second']->ID).">SKIP</a>";
							if($ondeck->RoundNum > 1) {
								echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_cross_off', $usersOnDeck['second']->ID)."><i class='fa fa-strikethrough' aria-hidden='true'></i></a>";
							}
							else {
								echo "<a class='button left small white primary_host_only' ".ajax('verify', 'cmd_move_bottom', $usersOnDeck['second']->ID).">SEND TO END</a>";
							}
						}
						echo "</div>";
					echo "</div>"; 
					echo "</div>";//col-md-6
					echo "</div>";//row
				}
			}
			echo "</div>";
		}//oldLayout
	}

	echo "<div class='ondeck_fixed_bottom_align'>";
	echo "<a id='ondeck_session_to_list' class='gray' alt='Goto List View' href='javascript:;' onclick='ps.ondeck.showList();'><i class='fa fa-chevron-down' aria-hidden='true'></i> List View</a>";
	echo "</div>";
	
	
//echo "<div class='center'>";
//echo "<a class='suggestions_popup_open white' alt='Hosting Suggestions'><i class='fa fa-hand-o-right' aria-hidden='true'></i> Hosting Suggestions</a>";	
//echo "&nbsp;|&nbsp;";
//echo "<a href='javascript:;' onclick='ps.ondeck.togglePresentationMode();'><i class='fa fa-desktop' aria-hidden='true'></i> Presentation Mode</a>";
//echo "</div>";
?>
</div>
