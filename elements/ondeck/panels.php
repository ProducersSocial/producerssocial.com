<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('user_purchase');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');

$form = Loader::helper('form'); 
$events = Loader::helper('events'); 

if($isLocationAdmin) {
	echo "<div id='user_profile_popup'>";
		echo "<div id='ps' class='user_profile_win'>";
			echo "<div id='user_profile_content'>";
			echo "</div>";
		echo "</div>";
		//echo "<a class='custom_entry_popup_close button white left' href=''>Ok</a>";
	echo "</div>";
	
	echo "<div id='presale_tickets_popup'>";
		echo "<div id='ps' class='presale_tickets_win'>";
			echo "<div class='presale_tickets_popup_close right iconbutton'><i class='fa fa-times' aria-hidden='true'></i></div>";
			echo "<div id='presale_tickets_content'>";
			echo "</div>";
		echo "</div>";
		//echo "<a class='custom_entry_popup_close button white left' href=''>Ok</a>";
	echo "</div>";
	
	echo "<div id='admin_verify_popup'>";
		echo "<div id='ps' class='admin_verify_popup_win'>";
		echo "<div class='admin_verify_popup_close right iconbutton white'><i class='fa fa-times' aria-hidden='true'></i></div>";
		echo "<div class='admin_verify_popup_heading'>Enter your pin</div>";
		echo "<div id='ps' class='admin_verify_popup_inner'>";
		
		echo "<input type='hidden' id='admin_verify_pin' value=''>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(7);'>7</a>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(8);'>8</a>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(9);'>9</a>";

		echo "<br class='clear'>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(4);'>4</a>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(5);'>5</a>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(6);'>6</a>";

		echo "<br class='clear'>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(1);'>1</a>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(2);'>2</a>";
		echo "<a class='button white' href='javascript:;' onclick='return ps.ondeck.enterPin(3);'>3</a>";

		echo "<br class='clear'>";
		echo "<a class='button white full' href='javascript:;' onclick='return ps.ondeck.enterPin(0);'>0</a>";
		
		echo "</div>";
		echo "<br class='clear'>";
		echo "</div>";
	echo "</div>";

	echo "<div id='custom_entry_popup'>";
		echo "<div id='ps' class='custom_entry_popup_win'>";
		echo "<div class='custom_entry_popup_close right iconbutton white'><i class='fa fa-times' aria-hidden='true'></i></div>";
		echo "<div class='custom_entry_popup_heading'>Please enter your name and email</div>";
		echo "<div id='ps' class='custom_entry_popup_inner'>";
		
		echo "<div id='custom_entry_form_view'>";
			echo "<form id='custom_entry_form' method='post' action='".$this->url("/ondeck?odid=".$ondeck->ID, '')."' class='form-horizontal'>";
				echo "<input type='hidden' name='ajax' value='custom_entry'>";
				//echo "<input type='hidden' name='view' id='custom_entry_view' value='users'>";
			
				echo $form->hidden('odid', $ondeck->ID);
				echo $form->hidden('cmd_new_entry', 1);
				echo "<div id='ondeck_entry_form_name'>";
				echo "<label for='new_entry_name'>".$form->label('new_entry_name',t('Artist Name'))."</label><br/>";
				echo $form->text('new_entry_name', '');
				echo "</div>";
				echo "<div id='ondeck_entry_form_email'>";
				echo "<label for='new_entry_email'>".$form->label('new_entry_email',t('Email'))."</label><br/>";
				echo $form->text('new_entry_email', '');
				echo "<div id='deck_new_entry_alert'></div>";
				echo "</div>";
				//echo "<div id='ondeck_entry_form_submit' class='col-xs-4'>";
				echo "<br class='clear'>";
				echo $form->submit('cmd_new_entry', t('Register & Add to List'), array('class' => 'button right new_entry_button'));
				echo "<a class='custom_entry_popup_close button white left' href=''>Cancel</a>";
			echo "</form>";
		echo "</div>";

		echo "<div id='custom_entry_result_view' style='display:none;'>";
			echo "<div id='custom_entry_result'>";
			echo "</div>";
			//echo "<a class='custom_entry_popup_close button white left' href=''>Ok</a>";
		echo "</div>";
		
		echo "<br class='clear'>";
		echo "</div>";
	echo "</div>";
}
?>

<div id='suggestions_popup'>
	<div id='ps' class='help_popup_inner'>
		<div class='suggestions_popup_close right iconbutton' onclick='ps.ondeck.closeSuggestions();'><i class='fa fa-times' aria-hidden='true'></i></div>
		<h2>Hosting Suggestions</h2>
		To keep the list moving in a timely manner, please consider the following<br>
		<br>

		<div class='alert alert-info'>
		<h2>Presenters</h2>
		<ul class='help_guidelines'>
			<li>Briefly introduce yourself and your track.</li>
			<li>What sort of feedback do you want?</li>
		</ul>
		</div>

		<div class='alert alert-warning'>
		<h2>On Deck</h2>
		<ul class='help_guidelines'>
			<li>Have your track rendered down to mp3/wav and ready to play from a flashdrive or device.</li>
			<li>Hosts, assist the on deck presenter getting their track ready while receiving feedback on the current presenter.</li>
		</ul>
		</div>
		
		<div class='alert alert-success'>
		<h2>Hosts</h2>
		<ul class='help_guidelines'>
			<li>Encourage concise constructive feedback and avoid repeating what's already been said.</li>
			<li>Wait for audience feedback before hosts give their's (if any)</li>
			<li>Feedback may be limited to X number of people if the list is long</li>
			<li>If the list goes a second round, feedback can be reduced or eliminated for speed</li>
		</ul>
		</div>
	</div>
</div>