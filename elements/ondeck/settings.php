<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('user_purchase');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');

$form = Loader::helper('form'); 
$events = Loader::helper('events'); 

if($isLocationAdmin) {
	echo "<div id='ondeck_settings_popup'>";
		echo "<div id='ps' class='ondeck_settings_popup_win'>";
		echo "<div class='location_style_".strtolower($ondeck->Location)."'>";
		echo "<div class='ondeck_settings_popup_close right iconbutton white'><i class='fa fa-times' aria-hidden='true'></i></div>";
		echo "<div class='ondeck_settings_popup_heading'>Session Settings</div>";
		echo "<div id='ps' class='ondeck_settings_popup_inner'>";
		
		echo "<div id='ondeck_settings_form_view'>";
			echo "<form id='ondeck_settings_form' method='post' action='".$this->url("/ondeck?odid=".$ondeck->ID, '')."' class='form-horizontal'>";
				echo "<input type='hidden' id='settings_hostid' name='settings_hostid' value='0'>";
				echo "<input type='hidden' name='ajax' value='ondeck_settings'>";
			
				echo $form->hidden('odid', $ondeck->ID);
				echo $form->hidden('cmd_settings', 1);
				
				echo "<div class='ondeck_settings_row' id='ondeck_settings_form_set_name'>";
				echo "<label for='set_name'>".t('Event Name')."</label><br/>";
				echo $form->text('set_name', $ondeck->Title);
				echo "</div>";
				
				if($isSuperAdmin || !$ondeck->EventID) {
					$locations = Locations::getSelectList(1, true, false);
					echo "<div class='ondeck_settings_row' id='ondeck_settings_form_set_location'>";
					echo $form->label('set_location', t('Location'));
					echo "<br class='clear'>";
					echo $form->select('set_location', $locations, $ondeck->Location, array('class'=>'center large'));
					echo "</div>";
				}
				
				echo "<div class='ondeck_settings_row' id='ondeck_settings_form_set_countdown'>";
				echo "<label for='set_countdown'>".t('Countdown Duration (mm:ss)')."</label><br/>";
				echo $form->text('set_countdown', $countdown);
				echo "</div>";

				echo "<div class='ondeck_settings_row' id='ondeck_settings_form_set_autostart'>";
				echo "<label for='set_autostart'>".t('Auto-Start Countdown')."</label><br/>";
				echo $form->checkbox('set_autostart', 1, $location->Autostart);
				echo "</div>";

				echo "<div class='ondeck_settings_row' id='ondeck_settings_form_set_hostid'>";
				echo "<label for='set_hostid'>".t('Make this computer the primary host')."</label><br/>";
				echo $form->checkbox('set_hostid', 1, 0);
				echo "</div>";

				echo "<div class='ondeck_settings_row' id='ondeck_settings_form_set_private'>";
				echo "<label for='set_private'>".t('Make Private')."</label>";
				echo $form->checkbox('set_private', 1, $ondeck->IsPrivate);
				echo "<p class='small'>Private sessions can only be viewed with a direct link:</p>";
				echo "<a class='small' href='/ondeck?odid=".$ondeck->ID."'>".BASE_URL."ondeck?odid=".$ondeck->ID."</p>";
				echo "<br/>";
				echo "</div>";

 				echo "<div id='ondeck_settings_alert'></div>";
 				echo "</div>";
				echo "<br class='clear'>";
				echo $form->submit('cmd_settings', t('Save Settings'), array('class' => 'button right'));
				echo "<a class='ondeck_settings_popup_close button white left' href=''>Cancel</a>";
			echo "</form>";
		echo "</div>";

		echo "<div id='ondeck_settings_result_view' style='display:none;'>";
			echo "<div id='ondeck_settings_result'>";
			echo "</div>";
			//echo "<a class='ondeck_settings_popup_close button white left' href=''>Ok</a>";
		echo "</div>";
		
		echo "<br class='clear'>";
		echo "</div>";
		echo "</div>";
	echo "</div>";
}
?>