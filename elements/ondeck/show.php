<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('user_purchase');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');

$form = Loader::helper('form'); 
$events = Loader::helper('events'); 

$u = new User();
if(!$u || !$u->isLoggedIn()) {
	$u = null;
}

$usersOnDeck = UserOnDeck::getAll("WHERE OnDeckID='".$ondeck->ID."' ORDER BY OrderNum ASC");
$allUsers = UserOnDeck::getAllUsers();
$nextUserOnDeck = $ondeck->getNextUserOnDeck(false);
$secondUserOnDeck = $ondeck->getNextUserOnDeck(false, 2);

$locations = Locations::getList();

$tickets = UserPurchase::getTicketsForUser($u->uID, $ondeck->EventID);

$thisUserIsOnList = false;
$thisUserOnDeck = null;
if($u) {
	//echo "check";
	$thisUserOnDeck = $ondeck->getUser($u->uID);
	if($thisUserOnDeck) {
		$thisUserIsOnList = true;
		$thisUserOnDeck->getEntries();
	}
}
$startTime = date("g:ia", strtotime($ondeck->EventDate));

$availableUsers = array();
foreach($allUsers as $us) {
	$isOn = false;
	foreach($usersOnDeck as $du) {
		if($du->UserID == $us->UserID) {
			$isOn = true;
			break;
		}
	}
	if(!$isOn) {
		$availableUsers[] = $us;
	}
}
if(!$u) {
	echo "<div class='alert alert-warning'><i class='fa fa-sign-in' aria-hidden='true'></i>&nbsp; To sign-up for events, please <a href='".$this->url("/login")."'>log in</a> or <a href='".$this->url("/register")."'>register</a>.</div>";
}
else
if(!$ondeck->IsOpen && !$ondeck->IsLive && !$ondeck->isAdmin) {
	$time = strtotime($ondeck->EventDate);
	$date = date("l F dS", $time)." at ".date("g:ia", $time);
	if($ondeck->EndTime != "0000-00-00 00:00:00" && time() > strtotime($ondeck->EndTime)) {
		echo "<div class='clear alert alert-info'><i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp; This <a href='".$this->url("/eventinfo?id=".$ondeck->EventID)."'>event</a> is no longer open for sign-ups.</div>";
	}
	else {
		echo "<div class='clear alert alert-info'><i class='fa fa-clock-o' aria-hidden='true'></i>&nbsp; This <a href='".$this->url("/eventinfo?id=".$ondeck->EventID)."'>event</a> will be open for sign-ups ".$date."</div>";
	}
}
else {
?>
<div id='ondeck_topheading' class='clear'>
<a href='javascript:;' onclick='ps.showHelp();'>Please review our guidelines before signing up</a>
<?php
if($ondeck->isSuperAdmin) {
	echo "<div class='admin_badge right'>SUPER ADMIN MODE</div>";
}
else
if($ondeck->isAdmin) {
	echo "<div class='admin_badge right'>ADMIN MODE</div>";
}
?>
</div>
<div id="ondeck">
	<div id="ondeck_current">
	<?php
		$url = $this->url("/ondeck")."?odid=".$ondeck->ID;
	
		$hasEnded = false;
		if($ondeck->EndTime != "0000-00-00 00:00:00") {
			$hasEnded = true;
		}
	
		echo "<a class='iconbutton right' alt='Refresh Page' href='".$url."''><i class='fa fa-refresh' aria-hidden='true'></i></a>";
		echo "<a class='iconbutton' alt='List View' href='".$this->url("/eventinfo?id=".$ondeck->EventID)."''><i class='fa fa-arrow-circle-o-up' aria-hidden='true'></i></a>";
		echo "<div id='ondeck_title'>";
			echo $ondeck->Title;
			echo "<div id='ondeck_date'>";
			echo date("D M j", strtotime($ondeck->Created));
			echo " &nbsp; ";
			echo Locations::getName($ondeck->Location);
			echo "<br class='clear'><div class='small'>";
			if($ondeck->IsLive) {
				echo "Session started at ".date("g:ia", strtotime($ondeck->StartTime))."<br>";
			}
			else
			if($hasEnded) {
				echo "Session started at ".date("g:ia", strtotime($ondeck->StartTime))." and ended at ".date("g:ia", strtotime($ondeck->EndTime))."<br>";
			}
			echo "</div>";//small
			echo "</div>";//ondeck_date
		echo "</div>";//title
		
		if($ondeck->IsLive) {
			if($ondeck->IsOnBreak) {
				echo "<div class='alert alert-success center'>The Producers Social is currently taking a break. We'll be back momentarily.";
				echo "<br class='clear'>";
				echo "<a class='button center' href='".$url."&cmd_resume=1'>RESUME SESSION</a>";
				echo "<br class='clear'>";
				echo "</div>";
			}
			else {
				echo "<h2>Now Playing</h2>";
				echo "<div id='ondeck_current'>";
					if($ondeck->isAdmin && $ondeck->CurrentNum < count($usersOnDeck)) {
						echo "<a class='button right' href='".$url."&cmd_next=1'>NEXT</a>";
						echo "<a class='button right pad-right' href='".$url."&cmd_move_down=".$currentUserOnDeck->ID."' onclick='return ps.events.confirmSkip();'>SKIP</a>";
					}
					if($ondeck->isAdmin && $ondeck->CurrentNum > 0) {
						echo "<a class='button right pad-right' href='".$url."&cmd_prev=1'>PREV</a>";
					}
					echo "<div id='ondeck_current_avatar'>";
					$isGuest = true;
					$p = UserProfile::getByUserID($currentUserOnDeck->UserID);
					if($p) {
						$img = $p->getAvatar(false);
						$isGuest = false;
					}
					else {
						$img = UserProfile::getDefultAvatar(false);
					}
					echo "<img src='".$img."'>";
					echo "</div>";
			
					echo "<div id='ondeck_current_info'>";
					echo "<div id='ondeck_current_name'>";
					echo $currentUserOnDeck->Name;
					//echo " (".$currentUserOnDeck->UserID.")";
					echo "</div>";
			
					if($p) {
						echo "<br><br>";
						echo "<div id='ondeck_current_soundcloud'>";
						echo "Soundcloud: ".$p->uSoundcloud;
						echo "</div>";
					}
					echo "<br>";
					echo "Started: ".date("g:ia", strtotime($currentUserOnDeck->Entry->StartTime));
					echo "</div>";
			
				echo "</div>";//ondeck_current
			}
			
			if(!$nextUserOnDeck) {
				echo "<div id='ondeck_next'>";
				echo "<div class='alert alert-info'>End of List</div>";
				echo "</div>";
			}
			else {
				if($secondUserOnDeck) {
					echo "<div class='row'>";
					echo "<div class='col-md-6'>";
				}
					echo "<h5>Up Next</h5>";
					echo "<div id='ondeck_next'>";
						echo "<div id='ondeck_next_avatar'>";
						$isGuest = true;
						$p = UserProfile::getByUserID($nextUserOnDeck->UserID);
						if($p) {
							$img = $p->getAvatar(false);
							$isGuest = false;
						}
						else {
							$img = UserProfile::getDefultAvatar(false);
						}
						echo "<img src='".$img."'>";
						echo "</div>";
		
						echo "<div id='ondeck_next_info'>";
							echo "<div id='ondeck_next_name'>";
							echo $nextUserOnDeck->Name;
							echo "</div>";
							if($ondeck->isAdmin) {
								echo "<a class='button clear small white pad-right' href='".$url."&cmd_move_down=".$nextUserOnDeck->ID."' onclick='return ps.events.confirmSkip();'>SKIP</a>";
								echo "<a class='button small white' href='".$url."&cmd_move_bottom=".$nextUserOnDeck->ID."' onclick='return ps.ondeck.confirmSendToEnd();'>SEND TO END</a>";
							}
						echo "</div>";
					echo "</div>";
				
				if($secondUserOnDeck) {
					echo "</div>";
					echo "<div class='col-md-6'>";
					echo "<h5>Up Next</h5>";
					echo "<div id='ondeck_next'>";
						echo "<div id='ondeck_next_avatar'>";
						$isGuest = true;
						$p = UserProfile::getByUserID($secondUserOnDeck->UserID);
						if($p) {
							$img = $p->getAvatar(false);
							$isGuest = false;
						}
						else {
							$img = UserProfile::getDefultAvatar(false);
						}
						echo "<img src='".$img."'>";
						echo "</div>";
		
						echo "<div id='ondeck_next_info'>";
						echo "<div id='ondeck_next_name'>";
						echo $secondUserOnDeck->Name;
						echo "</div>";
						if($ondeck->isAdmin) {
							echo "<a class='button clear small white pad-right' href='".$url."&cmd_move_down=".$secondUserOnDeck->ID."'>SKIP</a>";
							echo "<a class='button small white' href='".$url."&cmd_move_bottom=".$secondUserOnDeck->ID."' onclick='return ps.ondeck.confirmSendToEnd();'>SEND TO END</a>";
						}
						echo "</div>";
					echo "</div>"; 
					echo "</div>";//col-md-6
					echo "</div>";//row
				}
			}
			
			if($ondeck->isAdmin) {
				echo "<br class='clear' />";
				echo "<div id='ondeck_controls'>";
				echo "<a class='button right' href='".$url."&cmd_end_deck=1'>END SESSION</a>";
				if($ondeck->CurrentNum < count($usersOnDeck)) {
					if($nextUserOnDeck) {
					}
				}
				if(!$nextUserOnDeck) {
					echo "<a class='button left' href='".$url."&cmd_new_round=1'>START NEW ROUND</a>";
				}
				else 
				if(!$ondeck->IsOnBreak) {
					echo "<a class='button' href='".$url."&cmd_take_break=1'>TAKE BREAK</a>";
				}
				echo "</div>";
			}
		}
 
		//if(!$ondeck->isAdmin) { // bug
			$alert = "danger";
			if(!$ondeck->IsLive && $ondeck->IsOpen) {
				$status = "The list is open for sign-ups. The session will begin at ".$startTime.".";
				$alert = "success";
			}
			else
			if($ondeck->IsLive && $ondeck->IsOpen) {
				$status = "The session has started already, but the list is still open.";
				$alert = "warning";
			}
			else
			if($ondeck->IsLive && !$ondeck->IsOpen) {
				$status = "The list is closed for this session.";
			}
			else 
			if($ondeck->EndTime != "0000-00-00 00:00:00") {
				$status = "The session has ended and the list is closed.";
				$addtionalMsg = null;
				$alert = "note";
			}
			else {
				$status = "The session is not open for sign-ups.";
			}
			//echo "<span id='ondeck_status_label'><strong>Status:</strong></span><span id='ondeck_status'>".$status."</span>";
			echo "<br class='clear'><br>";
		
			echo "<a class='help_popup_open iconbutton right' alt='Show Help'><i class='fa fa-question-circle-o' aria-hidden='true'></i></a>";
		
			echo "<div class='alert alert-".$alert." center'>";
			echo "<div id='ondeck_status'>".$status."</div>";
			if($thisUserIsOnList) {
				if($thisUserOnDeck->ID == $nextUserOnDeck->ID) {
					echo "<h3>You are next up!</h3>";
				}
				else {
					if($thisUserOnDeck->OrderNum > $ondeck->CurrentNum) {
						echo "<h3>You are currently #".($thisUserOnDeck->OrderNum+1)." on the list.</h3>";
					}
					if(!$thisUserOnDeck->HasEntries && $ondeck->Rounds <= 1) {
						echo "<br class='clear'><a class='button center' href='".$url."&cmd_remove_me=1'>REMOVE ME</a>";
					}
				}
			}
			else {
				echo "<h3><i>You are not on the list yet</i></h3>";
			}
			if((($tickets && count($tickets) > 0) || !$ondeck->IsTicketRequired || $ondeck->isAdmin) && !$thisUserIsOnList) {
				echo "<br class='clear'><a class='button center' href='".$url."&cmd_add_me=1'>ADD ME</a>";
			}
			else 
			if($ondeck->IsTicketRequired) {
				echo "<br class='clear'><a class='' href='".View::url("/eventinfo?id=".$ondeck->EventID)."'><i class='fa fa-ticket' aria-hidden='true'></i> A ticket is required to sign-up for this event.</a>";
			}
			echo "</div>";
		//}
			
		if(!$ondeck->IsLive && $ondeck->isAdmin && count($usersOnDeck) > 0) {
			echo "<br class='clear'>";
			if($hasEnded) {
				echo "<a class='button left' href='".$url."&cmd_start_deck=1'>RESTART SESSION</a>";
			}
			else {
				echo "<a class='button left' href='".$url."&cmd_start_deck=1'>START SESSION</a>";
			}
			//echo "<hr class='clear'>";
			echo "<a class='button white right small' href='".$url."&cmd_clear_deck=1' onclick='return ps.events.confirmClearSession();'>CLEAR SESSION</a>";
			//echo "<br class='clear'><br>";
		}
	?>
	</div>

	<br class='clear'>
	<div id="ondeck_list">
	<?php
		if(count($usersOnDeck) > 0) {
			echo "<div id='ondeck_list_table_inset'>";
			echo "<table id='ondeck_list_table'>";
			echo "<tr>";
			echo "<th class='ondeck_list_num'>Num</th>";
			echo "<th class='ondeck_list_avatar'> </th>";
			echo "<th class='ondeck_list_name'>Name</th>";
			echo "<th class='ondeck_list_start'>Start Time</th>";
			echo "<th class='ondeck_list_end'>End Time</th>";
			if($ondeck->isAdmin) {
				echo "<th class='ondeck_list_controls'>";
				echo "";
				echo "</th>";
			}
			echo "</tr>"; 

			$i = 0;
			foreach($usersOnDeck as $us) {
				$end = $start = "--:--"; 
				$entry = UserOnDeckEntry::getByUserOnDeckID($us->ID, $ondeck->RoundNum);
				if($entry) {
					$start = $entry->StartTime;
					$end = $entry->EndTime;
					//echo $start;

					if($start == "0000-00-00 00:00:00") {
						$start = "--:--";
					}
					else {
						$t = strtotime($start);
						$start = date("g:ia", $t);
					}
					if($end == "0000-00-00 00:00:00") {
						$end = "--:--";
					}
					else {
						$t = strtotime($end);
						$end = date("g:ia", $t);
					}
				}
			
				$class = null;
				if($us->ID == $currentUserOnDeck->ID) {
					$class = "class='current'";
				}
			
				echo "<tr ".$class.">";
				echo "<td class='ondeck_list_num'>".($us->OrderNum+1)."</td>";
				echo "<td class='ondeck_list_avatar'>";
				$p = UserProfile::getByUserID($us->UserID);
				if($p) {
					$img = $p->getAvatar(false);
				}
				else {
					$img = UserProfile::getDefultAvatar(false);
				}
				echo "<img src='".$img."'>";
				echo "</td>";
				echo "<td class='ondeck_list_name'>".$us->Name."</td>";
				echo "<td class='ondeck_list_start'>".$start."</td>";
				echo "<td class='ondeck_list_end'>".$end."</td>";
				if($ondeck->isAdmin) {
					echo "<td class='ondeck_list_controls'>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_top=".$us->ID."'><i class='fa fa-caret-square-o-up' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_bottom=".$us->ID."'><i class='fa fa-caret-square-o-down' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_up=".$us->ID."'><i class='fa fa-arrow-circle-up' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_down=".$us->ID."'><i class='fa fa-arrow-circle-down' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_remove_user=".$us->ID."' onclick='return ps.events.confirmRemoveUser();'><i class='fa fa-times' aria-hidden='true'></i></a>";
					echo "</td>";
				}
				echo "</tr>";
				$i++;
			}
			echo "</table>";
			echo "</div>";
		}
		else {
			echo "<div class='alert alert-info center'><i><b>You are the first one here!</b></i></div>";
		}
	?>
	</div>

<?php
	if($ondeck->isAdmin) {
		echo "<div class='admin_area'>";
		echo "<div class='admin_area_label'>";
		echo "<a class='custom_entry_popup_open button right' href='javascript:;'>CUSTOM ENTRY</a>";
		if(!$ondeck->IsOpen) {
			echo "<a class='button left' href='".$url."&cmd_open_deck=1'>OPEN LIST</a>";
		}
		else {
			echo "<a class='button' href='".$url."&cmd_close_deck=1'>CLOSE LIST</a>";
		}
		if(!$ondeck->IsOpen) {
			echo "<div class='admin_area_heading'><i class='fa fa-lock' aria-hidden='true'></i>&nbsp;<i>The list is closed</i></div>";
		}
		else {
			echo "<div class='admin_area_heading'><i class='fa fa-folder-open-o' aria-hidden='true'></i>&nbsp;<i>Open for sign-ups - Click a user to add them to the list</i></div>";
		}
		echo "</div>";
		//if($ondeck->IsOpen) {
			
			echo "<br class='clear'>";
			if(count($availableUsers) > 0) {
				$i = 0;
				foreach($availableUsers as $u) {
					$ticketCount = UserPurchase::countTicketsForUser($u->UserID, $ondeck->EventID);
					$p = UserProfile::getByUserID($u->UserID);
					$img = $p ? $p->getAvatar(false) : UserProfile::getDefultAvatar(false);
					echo "<a class='ondeck_user_add' href='".$url."&cmd_add_user=".$u->UserID."'>";
					echo "	<div class='ondeck_user_add_location' style='background:#".$locations[$u->Location]->Color."'>".$u->Location."</div>";
					if($ticketCount) {
						echo "<div class='ondeck_user_add_tickets'>";
						for($x = 0; $x < $ticketCount; $x++) {
							echo "<i class='fa fa-ticket left' aria-hidden='true'></i>";
						}
						echo "</div>";
					}
					echo "	<img class='ondeck_user_add_img clear' src='".$img."'>";
					echo "	<div class='ondeck_user_add_name'>".$u->Name."</div>";
					echo "</a>";
				}
				/*
				echo "<table id='ondeck_users_table'>";
				echo "<tr>";
				echo "<th class='ondeck_users_name'>Name</th>";
				echo "<th class='ondeck_users_location'>Location</th>";
				echo "<th class='ondeck_users_tickets'>Tickets</th>";
				echo "<th class='ondeck_users_controls'>";
				echo "Add";
				echo "</th>";
				echo "</tr>";

				$i = 0;
				foreach($availableUsers as $u) {
					$ticketCount = UserPurchase::countTicketsForUser($u->UserID, $ondeck->EventID);
					
					$end = $start = "00:00";
					echo "<tr>";
					echo "<td class='ondeck_users_name'>".$u->Name."</td>";
					echo "<td class='ondeck_users_location'>".$u->Location."</td>";
					echo "<td class='ondeck_users_tickets'>";
					if($ticketCount) {
						//echo "<i class='fa fa-ticket' aria-hidden='true'></i>&nbsp; ".count($tickets);
						for($x = 0; $x < $ticketCount; $x++) {
							echo "<i class='fa fa-ticket' aria-hidden='true'></i>";
						}
					}
					echo "</td>";
					echo "<td class='ondeck_users_controls'>";
					echo "<a class='iconbutton' href='".$url."&cmd_add_user=".$u->UserID."'><i class='fa fa-plus-square' aria-hidden='true'></i></a>";
					echo "</td>";
					echo "</tr>";
					$i++;
				}
				echo "</table>";
				*/
			}
			else {
				echo "<div class='alert alert-info'>There are no users available to add to the list.</div>";
			}
			echo "<br class='clear'>";
		echo "</div>";
		 
		echo "<div id='custom_entry_popup'>";
			echo "<div class='custom_entry_popup_close right iconbutton'><i class='fa fa-times' aria-hidden='true'></i></div>";
			echo "<div id='ps' class='custom_entry_popup_win'>";
			echo "<div class='custom_entry_popup_heading'>Please enter your name and email</div>";
			echo "<div id='ps' class='custom_entry_popup_inner'>";
			echo "<form id='custom_entry_form' method='post' action='".$this->url("/ondeck?odid=".$ondeck->ID, '')."' class='form-horizontal'>";
				//echo "<div class='row'>";
				echo $form->hidden('odid', $ondeck->ID);
				echo $form->hidden('cmd_new_entry', 1);
				echo "<div id='ondeck_entry_form_name'>";
				echo "<label for='new_entry_name'>".$form->label('new_entry_name',t('Artist Name'))."</label><br/>";
				echo $form->text('new_entry_name', '');
				echo "</div>";
				echo "<div id='ondeck_entry_form_email'>";
				echo "<label for='new_entry_email'>".$form->label('new_entry_email',t('Email'))."</label><br/>";
				echo $form->text('new_entry_email', '');
				echo "<div id='deck_new_entry_alert' class='warning'></div>";
				echo "</div>";
				//echo "<div id='ondeck_entry_form_submit' class='col-xs-4'>";
				echo "<br class='clear'>";
				echo "<div class='ccm-form ccm-ui'>";
				echo "<a class='custom_entry_popup_close button white left' href=''>Cancel</a>";
				echo $form->submit('cmd_new_entry', t('Add to List') . ' &gt;', array('class' => 'button right', 'onclick'=>"return ps.ondeck.validateCustomEntry();"));
				echo "</div>";
				//echo "</div>";
				//echo "</div>";
			echo "</form>";
			echo "</div>";
			echo "<br class='clear'></div>";
		echo "</div>";
	}
}
?>
