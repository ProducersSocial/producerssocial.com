<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

$startTime = date("g:ia", strtotime($ondeck->EventDate));

$alert = "danger";
if(!$ondeck->IsLive && $ondeck->IsOpen) {
	$status = "Open for sign-ups. Session will start at ".$startTime.".";
	$alert = "success";
}
else
if($ondeck->IsLive && $ondeck->IsOpen) {
	$status = "Open for sign-ups. Round #".$round->RoundNum." started at ".date("g:ia", strtotime($round->StartTime));
	$alert = "success";
}
else
if($ondeck->IsLive && !$ondeck->IsOpen) {
	$status = "List closed. Round #".$round->RoundNum." started at ".date("g:ia", strtotime($round->StartTime));
	$alert = "danger";
}
else 
if($hasEnded) {
	$status = "Session Ended. From ".date("g:ia", strtotime($ondeck->StartTime))." to ".date("g:ia", strtotime($ondeck->EndTime));
	$addtionalMsg = null;
	$alert = "gray";
}
else {
	$status = "Session Closed";
}

echo "<table id='ondeck_title_top' class='full'>";
	echo "<tr>";
	echo "<td style='width:34px'>";
		echo "<a id='ondeck_nav_collapse' class='iconbutton gray' alt='Close Navbar' href='javascript:;' onclick='ps.ondeck.navToggle();'><i class='fa lg fa-bars' aria-hidden='true'></i></a>";
		echo "<a id='ondeck_nav_open' class='iconbutton gray' style='display:none;' alt='Open Navbar' href='javascript:;' onclick='ps.ondeck.navToggle();'><i class='fa lg fa-bars' aria-hidden='true'></i></a>";
	echo "</td>";
	echo "<td>";
		echo "<div id='ondeck_title'>";
			if($ondeck->EventID) echo "<a alt='Goto Event Details' href='".$this->url("/eventinfo?id=".$ondeck->EventID)."''>";
			echo $ondeck->Title." – ".date("M j", strtotime($ondeck->EventDate));
			if($ondeck->EventID) echo "</a>";
		echo "</div>";
	echo "</td>";
	echo "<td style='width:34px'>";
		echo "<a id='ondeck_nav_refresh' class='iconbutton right gray' alt='Refresh Page' href='javascript:;' onclick='return ps.ondeck.reload();'><i class='fa lg fa-refresh' aria-hidden='true'></i></a>";
	echo "</td>";
	echo "</tr>";
echo "</table>";

echo "<table id='ondeck_title_bottom' class='full hidden-xs'>";
	echo "<tr>";
	echo "<td style='width:15%'>";
		echo "<div id='ondeck_date' class='left'>";
		echo "<a alt='List View' href='javascript:;' onclick='ps.ondeck.showList();'>";
		echo $count." on deck";
		echo "</a>";
		echo "</div>";
	echo "</td>";
	echo "<td>";
		echo "<div id='ondeck_status' class='alert alert-".$alert."'>".$status."</div>";
	echo "</td>";
	echo "<td style='width:15%'>";
		echo "<div id='ondeck_time' class='right'>";
		//echo date("M j", time());
		//echo " ";
		echo date("g:ia", time());
		echo "</div>";
// 		if($isLocationAdmin) {
// 			echo "<a class='iconbutton right gray clear' href='javascript:;' onclick='return ps.ondeck.settings();'><i class='fa lg fa-cog' aria-hidden='true'></i></a>";
// 		}
	echo "</td>";
	echo "</tr>";
echo "</table>";

echo "<table id='ondeck_title_bottom' class='full visible-xs'>";
	echo "<tr>";
	echo "<td class='center'>";
		echo "<div id='ondeck_date' class='inline'>";
		echo date("M j", time());
		echo "</div>";
		echo " – ";
		echo "<div id='ondeck_time' class='inline'>";
		echo date("g:ia", time());
		echo "</div>";
		if($isLocationAdmin) {
			echo "<a class='iconbutton gray right' href='javascript:;' onclick='return ps.ondeck.settings();'><i class='fa lg fa-cog' aria-hidden='true'></i></a>";
		}
	echo "</td>";
	echo "</tr>";
echo "</table>";

echo "<div class='full visible-xs'>";
	echo "<div id='ondeck_status' class='alert alert-".$alert."'>".$status."</div>";
echo "</div>";

?>

