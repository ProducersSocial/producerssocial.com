<?php 
defined('C5_EXECUTE') or die("Access Denied."); 

Loader::model('user_profile');
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');
Loader::model('user_membership');

$form = Loader::helper('form'); 

$isAdmin = false;
$isSuperAdmin = false;
$adminLocation = null;
$u = new User();
if($u && $u->isLoggedIn()) {
	$p = UserProfile::getByUserID($u->uID);
	if($p) {
		$isAdmin = $p->isAdmin();
		$isSuperAdmin = $p->isSuperAdmin();
	}
	if($u->isSuperUser()) {
		$isAdmin = true;
		$isSuperAdmin = true;
		$adminLocation = "HQ";
	}
}
else {
	$u = null;
}

$usersOnDeck = UserOnDeck::getAll("WHERE OnDeckID='".$ondeck->ID."' ORDER BY OrderNum ASC");
$allUsers = UserOnDeck::getAllUsers();

$hasTicket = false; 
$onList = false;
if($u) {
	$onList = $ondeck->getUser($u->uID);
}
$startTime = date("g:ia", strtotime($ondeck->EventDate));

$availableUsers = array();
foreach($allUsers as $us) {
	$isOn = false;
	foreach($usersOnDeck as $du) {
		if($du->UserID == $us->UserID) {
			$isOn = true;
			break;
		}
	}
	if(!$isOn) {
		$availableUsers[] = $us;
	}
}
if(!$u || (!$ondeck->IsOpen && !$ondeck->IsLive && !$isAdmin)) {
	echo "<div class='alert alert-warning'><i class='fa fa-sign-in' aria-hidden='true'></i>&nbsp; To sign-up for events, please <a href='".$this->url("/login")."'>log in</a> or <a href='".$this->url("/register")."'>register</a>.</div>";
}
else {
?>
	<div id="ondeck_current">
	<?php
		$url = $this->url("/ondeck")."?odid=".$ondeck->ID;
	
		$hasEnded = false;
		if($ondeck->EndTime != "0000-00-00 00:00:00") {
			$hasEnded = true;
		}
	
		echo "<a class='iconbutton right' alt='Refresh Page' href='".$url."''><i class='fa fa-refresh' aria-hidden='true'></i></a>";
		echo "<a class='iconbutton' alt='Return to List' href='".$this->url("/events")."''><i class='fa fa-arrow-circle-o-up' aria-hidden='true'></i></a>";
		echo "<div id='ondeck_title'>";
			echo $ondeck->Title;
			echo "<div id='ondeck_date'>";
			echo date("D M j", strtotime($ondeck->Created));
			echo " &nbsp; ";
			echo Locations::getName($ondeck->Location);
			//echo " &nbsp; ";
			//echo "Round ".$ondeck->RoundNum;
			echo "</div>";
		echo "</div>";
	
		$alert = "danger";
		if(!$ondeck->IsLive && $ondeck->IsOpen) {
			$status = "The list is open for sign-ups. The session will begin at ".$startTime.".";
			$alert = "success";
		}
		else
		if($ondeck->IsLive && $ondeck->IsOpen) {
			$status = "The session has started already, but the list is still open.";
			$alert = "warning";
		}
		else
		if($ondeck->IsLive && !$ondeck->IsOpen) {
			$status = "The list is closed for this session.";
		}
		else 
		if($ondeck->EndTime != "0000-00-00 00:00:00") {
			$status = "The session has ended and the list is closed.";
			$alert = "note";
		}
		else {
			$status = "The session is not open for sign-ups.";
		}
		//echo "<span id='ondeck_status_label'><strong>Status:</strong></span><span id='ondeck_status'>".$status."</span>";
		echo "<br class='clear'><br>";
		
		echo "<div class='alert alert-".$alert." center'>";
		echo "<div id='ondeck_status'>".$status."</div>";
		if($hasTicket && !$onList) {
			echo "<br class='clear'><a class='button center' href='".$url."&cmd_add_me=1'>ADD ME</a>";
		}
		if($onList) {
			echo "<br class='clear'><a class='button center' href='".$url."&cmd_remove_me=1'>REMOVE ME</a>";
		}
		echo "</div>";
	
		if($ondeck->IsLive) {
			echo "<h2>Now Playing</h2>";
			echo "<div id='ondeck_current'>";
				echo "<div id='ondeck_current_avatar'>";
				$isGuest = true;
				$p = UserProfile::getByUserID($currentUserOnDeck->UserID);
				if($p) {
					echo $p->getAvatar();
					$isGuest = false;
				}
				else {
					echo UserProfile::getDefultAvatar();
				}
				echo "</div>";
			
				echo "<div id='ondeck_current_name'>";
				echo $currentUserOnDeck->Name;
				echo " (".$currentUserOnDeck->UserID.")";
				echo "</div>";
			
				if($p) {
					echo "<br><br>";
					echo "<div id='ondeck_current_soundcloud'>";
					echo "Soundcloud: ".$p->uSoundcloud;
					echo "</div>";
				}
			
				echo "<br>";
				echo "Started:".$currentUserOnDeck->Entry->StartTime;
			
			echo "</div>";

			echo "<h3>Up Next</h3>";
			echo "<div id='ondeck_next'>";
			echo "</div>";

			echo "<br class='clear' />";
			echo "<div id='ondeck_controls'>";
			if($ondeck->CurrentNum < count($usersOnDeck)) {
				echo "<a class='button' href='".$url."&cmd_next=1'>NEXT</a>";
				echo "<a class='button' href='".$url."&cmd_skip=1'>SKIP</a>";
			}
			if($ondeck->CurrentNum > 0) {
				echo "<a class='button' href='".$url."&cmd_prev=1'>PREV</a>";
			}
			echo "<a class='button' href='#'>TAKE BREAK</a>";
			echo "</div>";
			echo "<a class='button' href='".$url."&cmd_end_deck=1'>END SESSION</a>";
		}
		else
		if(count($usersOnDeck) > 0) {
			echo "<br class='clear'>";
			if($hasEnded) {
				echo "<a class='button' href='".$url."&cmd_start_deck=1'>RESTART SESSION</a>";
			}
			else {
				echo "<a class='button right' href='".$url."&cmd_start_deck=1'>START SESSION</a>";
			}
		}
	
		if(count($usersOnDeck) > 0) {
			echo "<a class='button' href='".$url."&cmd_clear_deck=1'>CLEAR SESSION</a>";
		}
	?>
	</div>

	<br class='clear'>
	<div id="ondeck_list">
	<?php
		if(count($usersOnDeck) > 0) {
			echo "<table id='ondeck_list_table'>";
			echo "<tr>";
			echo "<th class='ondeck_list_num'>Num</th>";
			echo "<th class='ondeck_list_avatar'> </th>";
			echo "<th class='ondeck_list_name'>Name</th>";
			echo "<th class='ondeck_list_start'>Start Time</th>";
			echo "<th class='ondeck_list_end'>End Time</th>";
			if($isAdmin) {
				echo "<th class='ondeck_list_controls'>";
				echo "";
				echo "</th>";
			}
			echo "</tr>"; 

			$i = 0;
			foreach($usersOnDeck as $us) {
				$end = $start = "--:--"; 
				$entry = UserOnDeckEntry::getByUserOnDeckID($us->ID, $ondeck->RoundNum);
				if($entry) {
					$start = $entry->StartTime;
					$end = $entry->EndTime;
					//echo $start;

					if($start == "0000-00-00 00:00:00") {
						$start = "--:--";
					}
					else {
						$t = strtotime($start);
						$start = date("g:ia", $t);
					}
					if($end == "0000-00-00 00:00:00") {
						$end = "--:--";
					}
					else {
						$t = strtotime($end);
						$end = date("g:ia", $t);
					}
				}
			
				$class = null;
				if($us->ID == $currentUserOnDeck->ID) {
					$class = "class='current'";
				}
			
				echo "<tr ".$class.">";
				echo "<td class='ondeck_list_num'>".($us->OrderNum+1)."</td>";
				echo "<td class='ondeck_list_avatar'>";
				$p = UserProfile::getByUserID($us->UserID);
				if($p) {
					echo $p->getAvatar();
				}
				else {
					echo UserProfile::getDefultAvatar();
				}
				echo "</td>";
				echo "<td class='ondeck_list_name'>".$us->Name."</td>";
				echo "<td class='ondeck_list_start'>".$start."</td>";
				echo "<td class='ondeck_list_end'>".$end."</td>";
				if($isAdmin) {
					echo "<td class='ondeck_list_controls'>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_top=".$us->ID."'><i class='fa fa-caret-square-o-up' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_bottom=".$us->ID."'><i class='fa fa-caret-square-o-down' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_up=".$us->ID."'><i class='fa fa-arrow-circle-up' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_move_down=".$us->ID."'><i class='fa fa-arrow-circle-down' aria-hidden='true'></i></a>";
					echo "<a class='iconbutton' href='".$url."&cmd_remove_user=".$us->ID."'><i class='fa fa-times' aria-hidden='true'></i></a>";
					echo "</td>";
				}
				echo "</tr>";
				$i++;
			}
			echo "</table>";
		}
		else {
			echo "<div class='alert alert-info'>No one is on the list yet.</div>";
		}
	?>
	</div>

<?php
	if($isAdmin) {
		echo "<div class='admin_area'>";
		echo "<div class='admin_area_label'>Admin Area – List Management</div>";
		if($ondeck->IsOpen) {
			echo "<div id='ondeck_users'>";
			
			echo "<form id='custom_entry_form' method='post' action='".$this->url("/ondeck?odid=".$ondeck->ID, '')."' class='form-horizontal'>";
				//echo "<div class='row'>";
				echo $form->hidden('odid', $ondeck->ID);
				echo $form->hidden('cmd_new_entry', 1);
				echo "<div id='ondeck_entry_form_name'>";
				//echo "<label for='new_entry_name'>".$form->label('new_entry_name',t('Name'))."</label><br/>";
				echo "<div class='controls'>".$form->text('new_entry_name', '', array('placeholder'=>'ARTIST NAME'))."</div>";
				echo "</div>";
				echo "<div id='ondeck_entry_form_email'>";
				//echo "<label for='new_entry_email'>".$form->label('new_entry_email',t('Email'))."</label><br/>";
				echo "<div class='controls'>".$form->text('new_entry_email', '', array('placeholder'=>'EMAIL'))."</div>";
				echo "<div id='deck_new_entry_alert' class='warning'></div>";
				echo "</div>";
				//echo "<div id='ondeck_entry_form_submit' class='col-xs-4'>";
				//echo "<div class='ccm-form ccm-ui'>";
				//echo "<a href='#close' rel='modal:close'>Cancel</a>";
				//echo $form->submit('cmd_new_entry', t('New Entry') . ' &gt;', array('class' => 'btn btn-lg primary', 'onclick'=>"return ps.ondeck.validateCustomEntry();"));
				//echo "</div>";
				//echo "</div>";
				//echo "</div>";
			echo "</form>";
			
		
			echo "<br>";
			echo "<a class='button' href='".$url."&cmd_close_deck=1'>CLOSE LIST</a>";
			echo "<a class='button right' href='javascript:;' onclick='ps.ondeck.customEntry();'>CUSTOM ENTRY</a>";
			//echo "<a class='button right' href='#custom_entry_form' rel='modal:open'>CUSTOM ENTRY</a>";
			echo "</div>";
			
			echo "<br class='clear'>";
			if(count($availableUsers) > 0) {
				echo "<table id='ondeck_users_table'>";
				echo "<tr>";
				echo "<th class='ondeck_users_name'>Name</th>";
				echo "<th class='ondeck_users_location'>Location</th>";
				echo "<th class='ondeck_users_controls'>";
				echo "Add";
				echo "</th>";
				echo "</tr>";

				$i = 0;
				foreach($availableUsers as $u) {
					$end = $start = "00:00";
					echo "<tr>";
					echo "<td class='ondeck_users_name'>".$u->Name."</td>";
					echo "<td class='ondeck_users_location'>".$u->Location."</td>";
					echo "<td class='ondeck_users_controls'>";
					echo "<a class='iconbutton' href='".$url."&cmd_add_user=".$u->UserID."'><i class='fa fa-plus-square' aria-hidden='true'></i></a>";
					echo "</td>";
					echo "</tr>";
					$i++;
				}
				echo "</table>";
			}
			else {
				echo "<div class='alert alert-info'>There are no available users to add to the list.</div>";
			}
		}
		else {
			echo "<h4>The list is closed</h4>";
			echo "<a class='button' href='".$url."&cmd_open_deck=1'>OPEN LIST</a>";
		}
		echo "</div>";
	}
}
?>
