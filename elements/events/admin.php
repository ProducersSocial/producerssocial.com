<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>
<?php 
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');
Loader::model('user_profile');
Loader::model('facebook_event');

//$form 		= Loader::helper('form');
//$events		= Loader::helper('events');

//$locations = Locations::getSelectList(1, true, false);

//$u = new User();
//$isLocationAdmin		= OnDeck::isLocationAdmin();
//$isSuperAdmin 			= $isLocationAdmin == "HQ" || $u->isSuperUser();

//$customEvents = OnDeck::getAll("WHERE EventID IS NULL OR EventID='' ORDER BY EventDate DESC");

$user = Loader::helper('user');

?>
<?php
if($user->isAdmin()) {
	
	echo "<div class='admin_area'>";
	echo "<a href='/manage' class='button white center'>Go to Admin Portal</a>";
	echo "</div>";
	/*
?>
	<br class='clear'>
	<div class="admin_area">
		<div class="admin_area_label">Admin Area</div>
		<div class='row pad'>
			<div class='col-md-7'>
				<p>Members will not be able to access the sign-up list for an event until the event is opened by an admin.</p>
				<?php
				echo "<div class='alert alert-white center'>";
				echo "<h3>Custom Events</h3>";
				if($customEvents && count($customEvents)) {
					foreach($customEvents as $e) {
						echo "<a class='button ".$e->Location."_bg' href='".View::url("/ondeck?odid=".$e->ID)."'>".$e->Title." – ".date("M j", strtotime($e->EventDate))."</a>";
					}
				}
				else {
					echo "There are no custom events";
				}
				echo "<br class='clear'>";
				echo "</div>";
				echo "<div class='alert alert-white center'>";
				if($isLocationAdmin) {
					echo "<form id='refresh_facebook_form' method='post' action='".$this->url("/events", '')."'>";
					echo $form->hidden('refresh', 1);
					echo $form->submit('cmd_refresh', t('Refresh Event Info from Facebook'), array('class' => 'button center'));
					//echo "<a href='".View::url("/events?refresh=1")."' class='button center'>Refresh Event Info from Facebook</a>";
 					echo "<br class='clear'>";
					echo "</form>";
				}
				echo "</div>";
				?>
			</div>
			<div class='col-md-5'>
				<div id="events_custom_session" class='alert alert-info'>
			
					Create a custom session not associated with a Facebook event.
					<?php 
 						echo "<div class='alert alert-white center'>";
 						echo "<form id='events_custom_session_form' method='post' action='".$this->url("/ondeck", '')."'>";
 						if($isSuperAdmin) {
 							echo $form->select('location', $locations, "0");
 						}
 						else {
 							echo $form->hidden('location', $isLocationAdmin);
 						}
 						echo "<div class='control-group'>";
 						echo "<label for='event_name' class='control-label'>New Event Name</label>";
 						echo "<div class='controls'>";
 						echo "<input type='text' name='event_name' id='event_name' class='ccm-input-text center'>";
 						echo "<br>";
 						echo "</div>";
 						echo "</div>";
 				
 						echo "<div id='events_custom_session_error'></div>";
 						echo $form->submit('cmd_new', t('New Custom Session') . ' &gt;', array('class' => 'button center', 'onclick'=>"return ps.events.validateNewSession();"));
 						echo "</form>";
 						echo "<br class='clear'>";
 						echo "</div>";
					?>
				</div>
			</div>
		</div>
		
	</div>
<?php
	*/
}
?>