<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>
<?php 
Loader::model('locations');
Loader::model('on_deck');
Loader::model('user_on_deck');
Loader::model('user_profile');
Loader::model('facebook_event');

$form 		= Loader::helper('form');
$events		= Loader::helper('events');
$navigation = Loader::helper('navigation');

$now 		= date("Y-m-d H:i:s", time());
$year 		= date("Y", time());
$month 		= date("m", time());
$day 		= date("d", time());
$endOfToday = $year."-".$month."-".$day." 23:59:59";

$startOfDayTime = strtotime($year."-".$month."-".$day." 00:00:00");
$today = $year."-".$month."-".$day;

$isLocationAdmin = OnDeck::isLocationAdmin();
$isSuperAdmin = $isLocationAdmin == "HQ";

// Get all events from today and earlier
$q = "WHERE 1";
if($location) {
	$q .= " AND Location='".$location."'";
}
if(!$isSuperAdmin) {
	$q .= " AND IsPrivate=0";
}
$allDecks = OnDeck::getAll($q." ORDER BY EventDate DESC"); // WHERE EventDate<'".$endOfToday."' 

$nextEvent = null;
$q = "WHERE EventDate>'".$now."'";
if($location) {
	$q .= " AND Location='".$location."'";
}
if(!$isSuperAdmin) {
	$q .= " AND IsPrivate=0";	
}
$nextEvent = OnDeck::getOne($q." ORDER BY EventDate ASC");

$u = new User();
if(!$u || !$u->isLoggedIn()) {
	$u = null;
}
/*
$hasTicket = false; 
$ticketOnDeckID = 1;
if($u && $hasTicket && !$isLocationAdmin) {
	header("Location:".$this->url("/ondeck?odid=".$ticketOnDeckID));
	die;
}
*/

// Find events that are currently live or open for sign-ups
$liveDecks = array();
$pastDecks = array();
$upcomingDecks = array();
foreach($allDecks as $d) {
	if($d->IsEnabled && ($d->IsOpen || $d->IsLive)) {
		$liveDecks[] = $d;
	}
	else {
		$date = strtotime($d->EventDate);
		$day = date("Y-m-d", $date);
		//if($day == $today) {
		if($d->IsLive || $d->IsOpen) {
			if($d->EventID || $isLocationAdmin) {
				$liveDecks[] = $d;
			}
		}
		else
		if($date > $startOfDayTime) {
			$upcomingDecks[] = $d;
		}
		else {
			$pastDecks[] = $d;
		}
	}
	$d->Event = FacebookEvent::getID($d->EventID);
}

?>
<div class='alert alert-white'>
<div id="ondeck_live_events">
<?php
	if(count($liveDecks) > 0) {
		echo "<div class='events_section_heading'>Events Live Now</div>";
		if($p) {
			echo "<p>Click an event below to sign-up.</p>";
		}
		foreach($liveDecks as $d) {
			echo $events->eventBox($d);
		}
		if(!$u) {
			echo "<div class='alert alert-white clear'><i class='fa fa-sign-in' aria-hidden='true'></i>&nbsp; To sign-up for events, please <a href='".$this->url("/login?redirect=/events")."'>log in</a> or <a href='".$this->url("/register")."'>register</a>.</div>";
		}
	}
	else {
		echo "<h4>There are no events in session right now</h4>"; 
		echo "<p>Check back here at the scheduled event times to sign-up on the list.</p>";
		if($nextEvent) {
			echo "<div class='alert alert-info' style='margin:0;'>";
			$eventTime = strtotime($nextEvent->EventDate);
			echo "<a href='".View::url("/eventinfo?id=".$nextEvent->EventID)."'><i class='fa fa-calendar-o' aria-hidden='true'></i>&nbsp; ";
			echo "The next scheduled event for <b>".Locations::getName($nextEvent->Location)."</b> is <b>".date("F dS", $eventTime)." at ".date("g:ia", $eventTime)."</b>";
			echo "</a>";
			echo "</div>";
		}
	}
?>
</div>
<br class='clear'>
</div>