<div id="manage_container">
	<table id="manage_content">
		<tr>
			<td id="manage_content_left">
				<div id="manage_navbar_container" class='navbar navbar-light bg-faded'>
					<div class="navbar-collapse collapse" id="manage_navbar">
						<?php 
						echo "<ul class='nav navbar-nav navbar-left'>";
						if($user->isAdmin()) {
							echo "<li class='".($controller->view == 'events' ? "active" : "")."'><a href='/manage'>Events</a></li>";
							echo "<li class='".($controller->view == 'blog' ? "active" : "")."'><a href='/manage/features?blog=true'>Blog</a></li>";
						}
						if($user->isSuperAdmin()) {
							echo "<li class='".($controller->view == 'emails' ? "active" : "")."'><a href='/manage/emails'>Emails</a></li>";
							echo "<li class='".($controller->view == 'features' ? "active" : "")."'><a href='/manage/features'>Features</a></li>";
							echo "<li class='".($controller->view == 'sponsors' ? "active" : "")."'><a href='/manage/sponsors'>Sponsors</a></li>";
							echo "<li class='".($controller->view == 'procedures' ? "active" : "")."'><a href='/manage/procedures'>Procedures</a></li>";
							
							$reported = UserProfile::anyReported();
							if($reported) {
								echo "<li class='".($controller->view == 'abuse' ? "active" : "")." abuse_reported'><a href='/manage/abuse'>Abuse Reported</a></li>";
							}
						}
						echo "</ul>";
						?>
					</div>
				</div>
			</td>
	
			<td id="manage_content_right">
			<?php  
				if(isset($error) && $error->has()) {
					$error->output();
				} 
				else 
				if(isset($message)) { ?>
					<div class="message"><?php echo $message?></div>
					<script type="text/javascript">
					$(function() {
						$("div.message").show('highlight', {}, 500);
					});
					</script>
				<?php  
				} 
			?>
