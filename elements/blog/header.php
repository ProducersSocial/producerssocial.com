<div id="members_container">
	<?php 
		echo "<a id='members_nav_arrow_right' class='members_nav_arrow' href='javascript:;' onclick='return ps.members.toggleNav(true);'><i class='fa fa-bars' aria-hidden='true'></i>&nbsp;</a>";
	?>
	<table id="members_content">
		<td id="members_content_left">
			<div id="members_navbar">
				<?php  
				echo "<a id='members_nav_arrow_left' class='members_nav_arrow' href='javascript:;' onclick='return ps.members.toggleNav(false);'><i class='fa fa-arrow-circle-left' aria-hidden='true'></i>&nbsp;</a>";
				echo "<ul id='members_navbar_ul'>";
				echo "<li ".($controller->view == 'blog' ? "class='active'" : '')."><a href='/blog'><i class='fa fa-user-circle-o large' aria-hidden='true'></i><br>Spotlight</a></li>";
				echo "<li ".($controller->view == 'archive' ? "class='active'" : '')."><a href='/blog/archive'><i class='fa fa-newspaper-o' aria-hidden='true'></i><br>Archive</a></li>";
				echo "</ul>";
				
				$user = Loader::helper('user');
				if($user->isSuperAdmin()) {
					echo "<div class='admin_area'>";
					echo "<a href='/manage/features?blog=true' class='button white center'>Manage</a>";
					echo "</div>";
				}
				?>
			</div>
		</td>

		<td id="members_content_right">
		<?php  
			if(isset($error) && $error->has()) {
				$error->output();
			} 
			else 
			if(isset($message)) { ?>
				<div class="message"><?php echo $message?></div>
				<script type="text/javascript">
				$(function() {
					$("div.message").show('highlight', {}, 500);
				});
				</script>
			<?php  
			} 
		?>
