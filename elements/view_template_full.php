<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$this->inc('elements/navbar.php');
?>

<div class='full_page_white'>	
	<div class="page_container">
		<?php
			Loader::element('system_errors', array('error' => $error));
			print $innerContent;
		
			$a = new Area('Main');
			//$a->enableGridContainer();
			$a->display($c);
		
			global $u; 
			if(!$u->isLoggedIn() || $u->isSuperUser()) {
				$a = new Area("GuestsOnly");
				$a->display($c);
			}
			if($u->isLoggedIn()) {
				$a = new Area("MembersOnly");
				$a->display($c);
			}
		?>
		<br class='clear'>
	</div>
	<br class='clear'>
</div>		

<?php  $this->inc('elements/footer.php'); ?>
