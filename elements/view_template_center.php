<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
if(!isset($pageColor)) $pageColor = "white";
if(!isset($showNav)) $showNav = true;
if($showNav) {
	$this->inc('elements/navbar.php');
}
?>
<div class='center_page'>
	<div class="<?php echo $pageColor;?>_center">
	<?php
		Loader::element('system_errors', array('error' => $error));
		print $innerContent;
	
		$a = new Area('Main');
		$a->display($c);
		
		global $u; 
		if(!$u->isLoggedIn() || $u->isSuperUser()) {
			$a = new Area("GuestsOnly");
			$a->display($c);
		}
		if($u->isLoggedIn()) {
			$a = new Area("MembersOnly");
			$a->display($c);
		}
	?>
	<br class='clear'>
	</div>
</div>	
<?php  
$this->inc('elements/footer.php', array('showNav'=>$showNav)); 
?>
