<div id="profile_navbar_container" class='navbar navbar-light bg-faded'>
	<div class="navbar-collapse collapse" id="profile_navbar">
		<?php  
		if(!$user->isProfileOwner() && $user->isSuperAdmin()) {
			echo "<div class='admin_border left'>";
		}
		echo "<ul id='members_navbar' class='nav navbar-nav navbar-left'>";
		
		if($user->isSuperAdmin() && !$user->isProfileOwner()) {
			echo "<li style='width:100%; align:center;'>";
			//echo "<div class='small'>User ID:".$user->id."</div><br>";
			if($user->adminLocation) {
				echo "<a class='button white small' href='javascript:;' ";
				echo "onclick='return ps.admin.revokeAdmin(\"".$user->profile->uID."\", \"".$user->profile->uArtistName."\");'>REVOKE ADMIN</a>";
			}
			else {
				echo "<div class='small'>Assign Admin</div>";
				$user->adminLocation = strtoupper($user->profile->uLocation);
				echo "<a class='button white small pad-top' href='javascript:;' ";
				echo "onclick='return ps.admin.makeAdmin(\"".$user->profile->uID."\", \"".$user->profile->uArtistName."\", \"".$user->adminLocation."\");'>".$user->adminLocation." ADMIN</a>";

				if($user->adminLocation != "HQ") {
					echo "<a class='button white small pad-top' href='javascript:;' ";
					echo "onclick='return ps.admin.makeAdmin(\"".$user->profile->uID."\", \"".$user->profile->uArtistName."\", \"HQ\");'>HQ ADMIN</a>";
				}
			}
			echo "</li>";
		}
		if($user->isSuperAdmin() || $user->isProfileOwner()) {
			$addUrl = $user->isSuperAdmin() ? "?uid=".$user->id : "";
			echo "<li class='".($controller->view == 'profile' ? 'active' : '')."'><a href='/profile".$addUrl."'>Profile</a>";
			//echo "<a class='right' href='/profile/edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>";
			echo "</li>";
			echo "<li class='hidden-sm-up ".($controller->view == 'avatar' ? 'active' : '')."'><a href='/profile/avatar".$addUrl."'>Avatar</a></li>";
			//echo "<li><a href='/profile/edit".$addUrl."'>Edit Profile</a></li>";
			echo "<li class='".($controller->view == 'account' ? 'active' : '')."'><a href='/profile/account".$addUrl."'>Account</a></li>";
			if($user->isSuperAdmin()) {
				echo "<li class='".($controller->view == 'tickets' ? 'active' : '')."'><a href='/profile/tickets".$addUrl."'>Tickets</a></li>";
			}
			if(!$user->sub) {
				//echo "<li class='highlight'><a href='/membership'>Membership</a></li>";
				echo "<li class='normal ".($controller->view == 'membership' ? 'active' : '')."'><a href='/membership/' class='alert alert-info small pad'><i class='fa fa-id-card-o large' aria-hidden='true'></i><br>Become a member now and get more!</a></li>";
			}
			else {
				//if($user->sub->isActive()) {
					echo "<li class='".($controller->view == 'membership' ? 'active' : '')."'><a href='/profile/membership".$addUrl."'>Membership</a></li>";
				//}
				//else {
				//	echo "<li><a class='emphasize' href='/profile/membership".$addUrl."'>Renew Your Membership</a></li>";
				//}
			}
		}
		//echo "<li ".($controller->view == 'directory' ? "class='active'" : '')."><a href='/members/directory'>Directory</a></li>";						
		echo "</ul>";
		if(!$user->isProfileOwner() && $user->isSuperAdmin()) {
			echo "</div>";
		}

		?>
	</div>
</div>
