<div id="profile_container">
    <?php 
    if($user->profile) $user->profile->setMetaData();
    Loader::element('profile/header', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	?>    
	
	<table id="profile_content">
		<tr>
			<td id="profile_content_left">
				<?php  
				Loader::element('profile/navbar', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
				?>    
			</td>
	
			<td id="profile_content_right">
				<div class='profile_div'></div>
		
				<div id='profile_content' class='row-fluid nopadding'>
					<div class="profile_bio col-xs-12 col-md-12">
					<?php
						if($user->profile->uFeaturedTrack) {
							$producers = Loader::helper("producers");
							echo $producers->presentFeature($user->profile->uFeaturedTrack, false, $user->isProfileOwner());
						}
						if($user->profile->uBio) echo $user->profile->uBio;
					?>
					</div>
					
					<?php
					/*
					<div class="profile_html col-xs-12 col-md-12">
						if($user->profile->uEmbed) {
							echo "<br>";
							echo $user->profile->uEmbed;
							echo "<br>";
						}
					</div>
					*/
					?>
				</div>
	
				<?php
				$uaks = UserAttributeKey::getPublicProfileList();
				foreach($uaks as $ua) { ?>
					<div>
						<label><?php echo $ua->getAttributeKeyDisplayName()?></label>
						<?php echo $user->info->getAttribute($ua, 'displaySanitized', 'display'); ?>
					</div>
				<?php } ?>		
				<?php 
					$a = new Area('Main'); 
					$a->setAttribute('profile', $user->info); 
					$a->setBlockWrapperStart('<div class="ccm-profile-body-item">');
					$a->setBlockWrapperEnd('</div>');
					$a->display($c); 
					
					if($user->info) {
						echo "<hr class='clear'>";
						echo "<div id='profile_userid'>ID:";
						echo $user->id;
						echo "</div>";
						echo "<div id='profile_joined'>".t('Joined')." ";
						echo date("M Y", strtotime($user->info->getUserDateAdded()));
						echo "</div>";
					}
				
				
				if($user->isLoggedIn()) {
					echo "<a class='profile_report_user' href='javascript:;' onclick='return ps.profile.reportUser(".$profile->uID.", ".$user->me->uID.");'>Report Abuse</a>";
				}
				?>
			</td>
		</tr>
	</table>
	<?php
    Loader::element('profile/footer', array('user'=> $user, 'this'=>$this, 'controller'=>$controller)); 
	?>
	<br class='clear'>
</div>