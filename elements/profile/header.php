<?php 
	Loader::model('user_ticket');
	
	$tickets = Loader::helper('tickets');
	
	if(!isset($profile)) $profile = $user->profile;
	if(!isset($sub)) $sub = $user->sub;
	if(!isset($info)) $info = $user->info;
	
	$superAdmin 	= false;
	$adminLocation 	= false;
	$isOwner 		= false;
	
	if(isset($user)) {
		$isOwner 		= $user->isProfileOwner();
		$adminLocation 	= $user->adminLocation;
		if($user->me->profile) {
			$superAdmin = $user->me->profile->isSuperAdmin();
		}
	}
	
?>
<table id="profile_header">
	<tr>
		<td id="profile_header_left">
			<div class="ccm-profile-header">
			<?php
				if($profile) {
					echo $profile->getAvatarLightbox();
				}
				if($sub && $sub->isActive()) {
					$link = $this->url('profile','membership'); 
					echo '<a class="premium_member_badge_small clear" href="'.$link.'">&nbsp;</a>';
				}
				if($adminLocation && ($isOwner || $superAdmin)) {
					echo "<div class='admin_border small center' style='width:80px; align:center;'>".$adminLocation." ADMIN</div>";
				}
			?>
			</div>
			<?php
			if(!isset($nonav) && ($isOwner || $superAdmin)) { 
			?>
				<nav id="profile_navbar_nav" class="navbar navbar-light bg-faded center">
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#profile_navbar">
					<i class="fa fa-bars large pad-top" aria-hidden="true"></i>
				  </button>
				</nav>
			<?php 
			}
			?>
		</td> 
		<td id="profile_header_right">
			<div class="profile_links row-fluid nopadding">
				<div class="profile_website">
				<div class="profile_username">
				<?php 
					$uid = $superAdmin ? "?uid=".$profile->uID : "";
					if(!$info->uIsActive) {
						echo "<div class='button small red right'>Account Deactivated</div>";
					}
					if($isOwner || $superAdmin) { 
						echo "<a class='right button medium white' href='/profile/edit".$uid."'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> ";
						echo "<span class='hidden-sm-down'>Edit Profile</span>";
						echo "</a>";
					}
					if($isOwner) {
						//$tickets->displayMemberTicketStatus();
					}						
					if($profile && $profile->uHide) {
						echo "<div class='profile_private'><i class='fa fa-lock' aria-hidden='true'></i> PRIVATE</div>";
					}
					if($profile && $profile->uArtistName) { 
						echo "<a href='".$profile->getURL()."'>";
						echo $profile->uArtistName;
						echo "</a>";
					}
					else 
					if($info) {
						echo $info->getUserName();
					}
				?>
				</div>
				<?php
					if($sub && $sub->isActive()) {
						if($profile->uHandle) {
							echo "<a class='profile_custom_url' href='".BASE_URL."/".$profile->uHandle."'>@".$profile->uHandle."</a>";	
						}
					}
					if($isOwner) {
						if(!$profile->hasAvatar() || !$profile->uArtistName) {
							echo "<div class='alert alert-success'>";
							echo "<h4 class='nopadding'>Welcome to the Producers Social!</h4>";
							if($profile->uArtistName && !$profile->hasAvatar()) {
								echo "Your profile is almost ready for the <a href='/members/directory'>members directory</a>. Now just <a href='/profile/avatar'>upload a profile image.</a>";
								echo "<br class='clear'>";
								echo "<a class='button clear left white medium' href='/profile/avatar'>Upload Avatar</a>";
							}
							else {
								echo "<a href='/profile/edit'>Please fill out your info</a> and <a href='/profile/avatar'>upload a profile image</a> ";
								echo "to be listed in the <a href='/members/directory'>members directory</a>.";
							}
							echo "<br class='clear'>";
							echo "</div>";
						}
						else 
						if(!$profile->uEdited) {
							echo "<p class='alert alert-success'>Welcome to the Producers Social!<br>Please click 'Edit' to customize your profile.</p>";
						}
						else 
						if(!$info->uEmail) {
							echo "<p class='alert alert-danger'>Please visit the <a href='".NavigationHelper::getLinkToCollection(Page::getByPath('/profile/account'), true)."'>Account</a> section to provide your email address.</p>";
						}
					}
					if($profile && $profile->uFullName) {
						echo "<div class='profile_realname'>".$profile->uFullName."</div>";
					}
				?>
				<?php
					if($profile->uWebsite) echo "<a target='_blank' href='".$profile->uWebsite."'>".$profile->uWebsite."</a>";
				?>
				</div>
				<div class="profile_social_links col-xs-12 nopadding">
				<?php
					if($profile->uEmail) {
						if($user->me->isLoggedIn()) {
							echo '<a class="social-email-32" target="_blank" href="'.UserProfile::completeURL($profile->uEmail, "mailto:".$profile->uEmail).'">&nbsp;</a>';
						}
						else {
							echo '<a class="social-email-32" target="_blank" href="javascript:;" onclick="ps.alert(';
							echo "'You must be logged in to contact members by email.'";
							echo ')">&nbsp;</a>';
						}
					}
					if($profile->uSoundcloud) {
						echo '<a class="social-soundcloud-32" target="_blank" href="'.UserProfile::completeURL($profile->uSoundcloud, "http://soundcloud.com/###").'">&nbsp;</a>';
					}
					if($profile->uBandcamp) {
						echo '<a class="social-bandcamp-32" target="_blank" href="'.UserProfile::completeURL($profile->uBandcamp, "http://###.bandcamp.com/").'">&nbsp;</a>';
					}
					if($profile->uFacebook) {
						echo '<a class="social-facebook-32" target="_blank" href="'.UserProfile::completeURL($profile->uFacebook, "http://facebook.com/###").'">&nbsp;</a>';
					}
					if($profile->uTwitter) {
						echo '<a class="social-twitter-32" target="_blank" href="'.UserProfile::completeURL($profile->uTwitter, "http://twitter.com/###").'">&nbsp;</a>';
					}
					if($profile->uInstagram) {
						echo '<a class="social-instagram-32" target="_blank" href="'.UserProfile::completeURL($profile->uInstagram, "http://instagram.com/###").'">&nbsp;</a>';
					}
					if($profile->uTube) {
						echo '<a class="social-youtube-32" target="_blank" href="'.UserProfile::completeURL($profile->uTube, "http://youtube.com/###").'">&nbsp;</a>';
					}
					if($profile->uSesh) {
						echo '<a class="social-sesh-32" target="_blank" href="'.UserProfile::completeURL($profile->uSesh, "http://sesh.io/###").'">&nbsp;</a>';
					}
				?>
				</div>
				<div class="profile_styletags col-xs-12 nopadding">
				<?php
					if($profile->uStyleTags) echo "Style: ".$profile->uStyleTags;
					
					if($user->isSuperAdmin()) {
						if($profile->uReported) {
							echo "<div class='profile_user_reported'>";
							echo "<div class='left'><i class='fa fa-warning' aria-hidden='true'></i> User Reported!</div>";	
							echo "<a class='button white small right' href='javascript:;' onclick='return ps.profile.reportClear(".$profile->uID.", ".$user->me->uID.");'>Clear Report</a>";	
							//echo "<a class='button white small right'>Disable Account</a>";	
							//echo "<a class='button white small right'>Delete User</a>";
							echo "</div>";
						}
					}
				?>
				</div>
			</div>
		</td>
	</tr>
</table>
