<div id="members_container">
	<?php 
		echo "<a class='members_nav_bars' style='display:none' href='javascript:;' onclick='return ps.members.toggleNav(true);'><i class='fa fa-bars' aria-hidden='true'></i>&nbsp;</a>";
	?>
	<table id="members_content">
		<td id="members_content_left">
			<div id="members_navbar">
				<?php  
				echo "<a id='members_nav_arrow_left' class='members_nav_arrow' href='javascript:;' onclick='return ps.members.toggleNav(false);'><i class='fa fa-bars' aria-hidden='true'></i>&nbsp;</a>";
				echo "<ul id='members_navbar_ul'>";
				
				//$user = Loader::helper('user');
				if(!$user->isMember()) {
					//echo "<li class='members_nav_arrow hidden-lg-up'><a href='javascript:;' onclick='return ps.members.collapseNav();'><i class='fa fa-arrow-circle-left' aria-hidden='true'></i>&nbsp;</a></li>";
					echo "<li ".($controller->view == 'featured' ? "class='active'" : '')."><a href='/members'><i class='fa fa-road' aria-hidden='true'></i><div class='center'>Featured</div></a></li>";
					echo "<li ".($controller->view == 'tickets' ? "class='active'" : '')."><a href='/members/tickets'><i class='fa fa-ticket' aria-hidden='true'></i><div class='center'>Tickets</div></a></li>";
					echo "<li ".($controller->view == 'directory' || $controller->view == 'profile' ? "class='active'" : '')."><a href='/members/directory'><i class='fa fa-users' aria-hidden='true'></i><div class='center'>Socialites</div></a></li>";						
					echo "<li ".($controller->view == 'submissions' ? "class='active'" : '')."><a href='/members/submissions'><i class='fa fa-cloud-upload' aria-hidden='true'></i><div class='center'>Submissions</div></li>";
					echo "</ul>";
					
					echo "<ul id='members_navbar_ul' class='members'>";
					echo "<li class='normal'><a id='become_member' href='/membership/' class='alert alert-info small nomargin'><i class='fa fa-id-card-o large' aria-hidden='true'></i><div class='center'>Become a member now and get more!</div></a></li>";
					echo "<div id='members_navbar_title'>Member Benefits</div>";
					
					echo "<li class='disabled ".($controller->view == 'samples' ? "active" : '')."'><a href='/members/samples'><i class='fa fa-download' aria-hidden='true'></i><div class='center'>Samples</div></li>";
					echo "<li class='disabled ".($controller->view == 'tutorials' ? "active" : '')."'><a href='/members/tutorials'><i class='fa fa-youtube-play' aria-hidden='true'></i><div class='center'>Tutorials</div></li>";
					//echo "<li class='disabled ".($controller->view == 'software' ? "active" : '')."'><a href='/members/software'><i class='fa fa-plug' aria-hidden='true'></i><div class='center'>Software</div></li>";
					//echo "<li class='disabled ".($controller->view == 'offers' ? "active" : '')."'><a href='/members/offers'><i class='fa fa-tags' aria-hidden='true'></i><div class='center'>Offers</div></li>";
					
				}
				else {
					echo "<li ".($controller->view == 'featured' ? "class='active'" : '')."><a href='/members'><i class='fa fa-road' aria-hidden='true'></i><div class='center'>Featured</div></li>";
					echo "<li ".($controller->view == 'tickets' ? "class='active'" : '')."><a href='/members/tickets'><i class='fa fa-ticket' aria-hidden='true'></i><div class='center'>Tickets</div></li>";
					echo "<li ".($controller->view == 'samples' ? "class='active'" : '')."><a href='/members/samples'><i class='fa fa-download' aria-hidden='true'></i><div class='center'>Samples</div></li>";
					echo "<li ".($controller->view == 'tutorials' ? "class='active'" : '')."><a href='/members/tutorials'><i class='fa fa-youtube-play' aria-hidden='true'></i><div class='center'>Tutorials</div></li>";
					echo "<li ".($controller->view == 'submissions' ? "class='active'" : '')."><a href='/members/submissions'><i class='fa fa-cloud-upload' aria-hidden='true'></i><div class='center'>Submissions</div></li>";
					//echo "<li ".($controller->view == 'software' ? "class='active'" : '')."><a href='/members/software'><i class='fa fa-plug' aria-hidden='true'></i><div class='center'>Software</div></li>";
					//echo "<li ".($controller->view == 'offers' ? "class='active'" : '')."><a href='/members/offers'><i class='fa fa-tags' aria-hidden='true'></i><div class='center'>Offers</div></li>";
					echo "<li ".($controller->view == 'favorites' ? "class='active'" : '')."><a href='/members/favorites'><i class='fa fa-heartbeat' aria-hidden='true'></i><div class='center'>Favorites</div></li>";						
					echo "<li ".($controller->view == 'directory' || $controller->view == 'profile' ? "class='active'" : '')."><a href='/members/directory'><i class='fa fa-users' aria-hidden='true'></i><div class='center'>Socialites</div></li>";						
					echo "<li ".($controller->view == 'membership' ? "class='active'" : '')."><a href='/members/membership'><i class='fa fa-id-card-o' aria-hidden='true'></i><div class='center'>Membership</div></li>";						
				}
				//if($user->isSuperAdmin() || $user->isProfileOwner()) {
				//	$addUrl = $user->isSuperAdmin() ? "?uid=".$user->id : "";
				//	echo "<li><a href='/member/benefits'>Member Benefits</a></li>";
				//}
				echo "</ul>";
				
				if($user->isSuperAdmin()) {
					echo "<div class='admin_area'>";
					echo "<a href='/manage/features' class='button white center'>Manage</a>";
					echo "</div>";
				}
				?>
			</div>
		</td>

		<td id="members_content_right">
		<?php  
			echo "<a id='members_nav_arrow_right' class='members_nav_arrow' href='javascript:;' onclick='return ps.members.toggleNav(true);'><i class='fa fa-bars' aria-hidden='true'></i>&nbsp;</a>";
			if(isset($error) && $error->has()) {
				$error->output();
			} 
			else 
			if(isset($message)) { ?>
				<div class="message"><?php echo $message?></div>
				<script type="text/javascript">
				$(function() {
					$("div.message").show('highlight', {}, 500);
				});
				</script>
			<?php  
			} 
		?>
