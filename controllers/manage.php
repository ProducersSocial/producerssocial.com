<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ManageController extends Controller {
	public $view	= 'events';
	public $user 	= null;
	public $helpers = array('html', 'form', 'text'); 
	
	public function on_start(){
		$this->error = Loader::helper('validation/error');
		$this->addHeaderItem(Loader::helper('html')->css('ccm.profile.css'));
		$this->user = Loader::helper('user');
		
		$this->user->setup();
		$this->set('user', $this->user);
		$this->set('controller', $this);
		if(!$this->user->isAdmin()) {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}
	
	public function view() {
		if($_REQUEST['refresh_events']) {
			$events = Loader::helper('events');
			$refresh_events = $events->updateEventInfo();
			$this->set('refresh_events', $refresh_events);
		}
	}
	
	public function slack_test() {
		$slack = Loader::helper("slack");
		$slack->send("This is a test.\nStephen Walker\nhttps://producerssocial.com/members/\nsomething else\nstephen@walkerfx.com", "@axon");
	}
	
	public function on_before_render() {
		$this->set('error', $this->error);
	}
	
	public function generate_colors($css_location=null) {
		if(isset($_REQUEST['css_location'])) {
			$css_location = $_REQUEST['css_location'];
		}
		$path = DIR_REL."themes/producers/css/location_ondeck_template.css";
		//echo $path;
		$css = file_get_contents($path);
		$this->set('css_location', $css_location);
		$this->set('css', $css);
	}
}