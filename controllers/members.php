<?php
defined('C5_EXECUTE') or die("Access Denied.");
//Loader::controller('producers');
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersController extends ProducersController {
	public $view = "featured";
	
	public function view() 
	{
		Loader::model("member_feature");
				
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		$limit = 9;
		$this->set('limit', $limit);
		
		$page = null;
		$paging = Loader::helper('paging');
		$limit = $paging->getLimit($limit, $page);
		$this->set('page', $page);
		
		$q = "status='active' AND (expiration='0000-00-00 00:00:00' OR expiration > '".date("Y-m-d H:i:s", time())."')";
		if(!$this->user->isSuperAdmin()) {
			$q .= " AND published <= '".date("Y-m-d H:i:s", time())."'";
		}
		$q .= " AND type!='blog'";
		$features = MemberFeature::getAll($q, "published DESC ".$limit);
		
		$totalItems = MemberFeature::getCount($q);
		$this->set('totalItems', $totalItems);
		$this->set('query', $q." ".$limit);
		
		$ids = "";
		if($features) {
			foreach($features as $f) {
				if($ids) $ids .= " AND ";
				$ids .= "id != '".$f->id."'";
			}
			$ids = " AND (".$ids.")";
		}
		$articles = MemberFeature::getAll($q.$ids, "published DESC LIMIT 3");
		
		$items = array_merge($features, $articles);
		
		$this->set('items', $items);
		$this->set('itemCount', count($features));
	}
}