<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ProducersController extends Controller {
	public $user 	= null;
	public $form	= null;
	
	public function __construct() 
	{
		$this->user = $GLOBALS['user'] = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		$this->form = Loader::helper('form');
		$this->set('form', $this->form);
		
		Loader::model('user_log');
	}
	
	public function on_start()
	{
		$this->error = Loader::helper('validation/error');
		$this->addHeaderItem(Loader::helper('html')->css('ccm.profile.css'));
	}
	
	public function on_before_render() 
	{
		$this->set('error', $this->error);
	}	
	
}