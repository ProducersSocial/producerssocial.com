<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class SlackController extends ProducersController {
	
	var $token = "PRbPVuNQvst5LhYN6nyuS0FD";
	
	/*
		[token] => PRbPVuNQvst5LhYN6nyuS0FD
		[team_id] => T088BQGD8
		[team_domain] => producerssocial
		[channel_id] => D1A09CVL1
		[channel_name] => directmessage
		[user_id] => U0896MF97
		[user_name] => axon
		[command] => /stats
		[text] =>
		[response_url] => https://hooks.slack.com/commands/T088BQGD8/206616975874/VPml7X0sG0d8cjIDz3atl0zf
    */
    
	public function view($id=null) 
	{
		$this->set("output", "SLACK");
	}
	
	public function stats()
	{
		if($_REQUEST['token'] != $this->token) {
			$output = "UNAUTHORIZED";
		}
		else {
			$nl = "<br>";
			if(isset($_POST['token'])) {
				$nl = "\n";	
			}
			$output = "Website Stats:".$nl;
			//$slack = Loader::helper('slack');
			$db = Loader::db();
			$q = "SELECT COUNT(*) as total FROM Users WHERE 1=1";
			if($r = $db->query($q)) {
				$d = $r->FetchRow();
				$output .= "Total Users: ".$d['total'].$nl;
			}
			
			$output .= "Locations:";
			$q = "SELECT uLocation, COUNT(*) as total FROM Users WHERE 1=1 GROUP BY uLocation";
			if($r = $db->query($q)) {
				while($d = $r->FetchRow()) {
					if($d['uLocation'] && $d['uLocation'] != "OT") {
						$output .= $d['uLocation'].":".$d['total']."  ";
					}
				}
			}
			$output .= $nl;
						
			$output .= "New Users Past 7 Days: ";
			$oneWeekAgo = date("Y-m-d H:i:s", time() - (24 * 60 * 60 * 7));
			$q = "SELECT COUNT(*) as total FROM Users WHERE uDateAdded>'".$oneWeekAgo."'";
			if($r = $db->query($q)) {
				$d = $r->FetchRow();
				$output .= $d['total'].$nl;
			}
			
			$output .= "Users Active Past 24hrs: ";
			$oneDayAgo = time() - (24 * 60 * 60);
			$q = "SELECT COUNT(*) as total FROM Users WHERE uLastOnline>'".$oneDayAgo."'";
			if($r = $db->query($q)) {
				$d = $r->FetchRow();
				$output .= $d['total'].$nl;
			}
		}

		$this->set("output", $output);
	}
}