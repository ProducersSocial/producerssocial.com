<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ProfileEditController extends Controller {//Concrete5_Controller_Profile_Edit
	public $view = 'edit';
	public $user = null;
	public $helpers = array('html', 'form', 'date');
	
	public function __construct() {
		Loader::model("locations");
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		//if(ADMIN) echo "TEST2";
		
		if(isset($_REQUEST['uid']) && ($_REQUEST['uid'] != $this->user->id && !$this->user->isSuperAdmin())) {
			header("Location: ".BASE_URL.'/profile/edit');
			die();
		}
	}
	
	public function on_start() {
		$this->addHeaderItem(Loader::helper('html')->css('ccm.profile.css'));
		$this->set('valt', Loader::helper('validation/token'));
	}

	public function save_complete() {
		$this->set('message', t('Profile Information Saved.'));
	}
	
	public function view() 
	{
		//	if(ADMIN) print_r($_REQUEST);
	 
		if(isset($_REQUEST['save'])) {
			$invalid = false;
			$handle = null;
			if($this->user->isProfileOwner() || $this->user->isSuperAdmin()) {
				$data = $this->post();
				//if(ADMIN) print_r($data);
				if(!isset($data['uHide'])) {
					$data['uHide'] = 0;	
				}
				
				if(!$this->user->sub || !$this->user->sub->isActive()) {
					$data['uHandle'] = null;	
				}
				else 
				if($data['uHandle']) {
					if(!UserProfile::validateHandle($data['uHandle'], $this->user->id)) {
						$handle = $data['uHandle'];
						$data['uHandle'] = "";
						$invalid = 'handle';
					}
				}
				foreach(UserProfile::$Fields as $f) {
					if(isset($data[$f])) {
						$this->user->profile->$f = $data[$f];
					}
				}
				UserProfile::save($this->user->profile);
			}
			if(!$invalid) {
				$this->redirect("/profile/".$this->user->id);
			}
			else {
				$this->redirect("/profile/edit?uid=".$this->user->id."&invalid=".$invalid."&handle=".$handle);
			}
		}
	}
	
	public function save() { 
		//echo "TEST";
		/*
		$this->user = Loader::helper('user');
		
		
		*/
	}
}

?>
