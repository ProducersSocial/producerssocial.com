<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ProfileDeleteController extends Concrete5_Controller_Profile_Edit {
	public $view 	= 'delete';
	public $user	= null;
	public $helpers = array('html', 'form', 'date');
	
	public function __construct() {
		$html = Loader::helper('html');
		parent::__construct();
		$u = new User();
		if (!$u->isRegistered()) {
			$this->set('intro_msg', t('You must sign in order to access this page!'));
			Loader::controller('/login');
			$this->render('/login');
		}
		$this->set('profile', UserInfo::getByID($u->getUserID()));
		$this->set('av', Loader::helper('concrete/avatar'));
		
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if(isset($_REQUEST['uid']) && ($_REQUEST['uid'] != $this->user->id && !$this->user->isSuperAdmin())) {
			header("Location: ".BASE_URL.'/profile/delete');
			die();
		}
	}
	
	public function delete() { 
		Loader::model('user_profile');
		$this->user = Loader::helper('user');
		
		$uid = null;
		if(isset($_REQUEST['uid']) && $this->user->isSuperAdmin()) {
			$uid = $_REQUEST['uid'];
		}
		if(!$uid) {
			$u = new User();
			$uid = $u->uID;
		}
		$ui = UserInfo::getByID($uid);
		$up = UserProfile::getByUserID($uid);

		$confirm = false;
		if(isset($_REQUEST['confirm'])) {
			$confirm = $_REQUEST['confirm'];
		}
		if($confirm) {
		// Delete the user account
			if($up) UserProfile::delete($up);
			$ui->delete();
			if(!$this->user->isSuperAdmin()) {
				$u->logout();
			}
			else {
				$this->redirect("/members/directory");
			}
		}
		
		$this->set('confirmed', $confirm);
		//$this->redirect("/profile");
	}
}

?>
