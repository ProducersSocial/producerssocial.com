<?php
defined('C5_EXECUTE') or die("Access Denied.");
Loader::controller('/profile/edit');

class ProfileAvatarUploadController extends ProfileEditController {
	public $view = 'avatar';
	public $user = null;
	
	public function __construct(){
		parent::__construct();
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if(isset($_REQUEST['uid']) && ($_REQUEST['uid'] != $this->user->id && !$this->user->isSuperAdmin())) {
			header("Location: ".BASE_URL.'/profile/avatar_upload');
			die();
		}
	}

	public function setImageType($type=null) {
		Loader::model('user_profile');
		if($this->user->profile) {
			if($type === null) $type = strtolower($_REQUEST['imagetype']);
			if($type != "facebook" && $type != "twitter" && $type != "google") {
				$type = "";
			}
			$this->user->profile->uImageType = $type;
			UserProfile::save($this->user->profile);
			if($type) {
				$src = null;
				Loader::model("social_login", "social_login");
				Loader::library('hybridauth/Hybrid/Auth','social_login');
				$config = SocialLoginModel::getHybridAuthConfiguration();

				$hybridauth = new Hybrid_Auth( $config );
			
				if($type) {
					$type = ucfirst($type); 
					if($adapter = $hybridauth->authenticate($type)) {
						if($up = $adapter->getUserProfile()) {
							$src = $up->photoURL;
							if($src != null) {
								$filename = DIR_FILES_AVATARS.'/'.$this->user->id.'.jpg';
								copy($src, $filename);
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	public function saveImage() {
		$uid = $_POST['uid'];		
		$this->user->setup($uid);
		if(!is_object($this->user->info) || $uid < 1) {
			echo "Invalid User ID!";
			die();
		}
		ini_set("memory_limit","256M");
		
		$data = substr($_POST['data'], strpos($_POST['data'], ",") + 1);
		$decodedData = base64_decode($data);
		$filename = DIR_FILES_AVATARS.'/'.$this->user->info->getUserID().'.jpg';
		$fp = fopen($filename, 'wb');
		fwrite($fp, $decodedData);
		fclose($fp);
		
		
		$this->setImageType("none");
		$this->user->profile->createAvatarSmall();
		
		$puid = "";
		if($this->user->id != $this->user->me->uID) {
			$puid = "?uid=".$this->user->id;
		}
		if(!$this->user->profile->uArtistName) {
			echo BASE_URL."/profile/edit".$puid;
		}
		else {
			echo BASE_URL."/profile".$puid;
		}
		die;
 	}
	 
}
?>