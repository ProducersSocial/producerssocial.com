<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ProfileAccountController extends Controller {
	public $view 	= 'account';
	public $user	= null;
	public $helpers = array('html', 'form', 'date');
	
	public function __construct() {
		parent::__construct();
		$this->user = Loader::helper('user');
		$uid = null;
		if(isset($_REQUEST['uid'])) {
			$uid = $_REQUEST['uid'];
			$this->user->setup($uid);
		}
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if($uid && ($uid != $this->user->id && !$this->user->isSuperAdmin())) {
			header("Location: ".BASE_URL.'/profile/account');
			die();
		}
	}
	
	public function on_start() {
		$this->addHeaderItem(Loader::helper('html')->css('ccm.profile.css'));
		$this->set('valt', Loader::helper('validation/token'));
	}

	public function save_complete() {
		$this->set('message', t('Profile Information Saved.'));
	}
	
	public function view() {
		Loader::model('social_login', 'social_login');
		$message = null;
		if(isset($_GET['provider'])){
			$provider = $_GET["provider"];
			if(isset($_GET['disassociate']))
			{
				SocialLoginModel::disassociate($provider);
				$message = "<div class='alert alert-success' rel='fadeaway'>Your account login has been disconnected from ".$provider.".</div>";
			}
			else
			{
				SocialLoginModel::performLogin($this);
				//header('Location: '.BASE_URL.'/'.DIR_REL);
				$message = "<div class='alert alert-success' rel='fadeaway'>Your account login has been connected with ".$provider.".</div>";
			}
		}
		if($message) {
			$this->set('message', $message);	
		}
	}
	
	public function save() { 
		if($this->user->isProfileOwner() || $this->user->isSuperAdmin()) {
			$vsh 	= Loader::helper('validation/strings');
			$cvh 	= Loader::helper('concrete/validation');
			$valt 	= Loader::helper('validation/token');
			$e 		= Loader::helper('validation/error');
		
			$data = $this->post();
			
			if(isset($data['uSendReminders'])) $data['uSendReminders'] = 1;
			else $data['uSendReminders'] = 0;
			
			if(isset($data['uNewsletter'])) $data['uNewsletter'] = 1;
			else $data['uNewsletter'] = 0;
			
			//token
			//if(!$valt->validate('profile_account')) {
			//	$e->add($valt->getErrorMessage());
			//}
	
			// validate the user's email
			$email = $this->post('uEmail');
			if(!$vsh->email($email)) {
				$e->add(t('Invalid email address provided.'));
			} 
			else
			if(!$cvh->isUniqueEmail($email) && $this->user->info->getUserEmail() != $email) {
				//$e->add(t("The email address '%s' is already in use. Please choose another.",$email));
			}
	
	
			// password
			if(isset($data['uPasswordNew'])) {
				$passwordNew = $data['uPasswordNew'];
				$passwordNewConfirm = $data['uPasswordNewConfirm'];
				
				if((strlen($passwordNew) < USER_PASSWORD_MINIMUM) || (strlen($passwordNew) > USER_PASSWORD_MAXIMUM)) {
					$e->add(t('A password must be between %s and %s characters', USER_PASSWORD_MINIMUM, USER_PASSWORD_MAXIMUM));
				}		
				
				if(strlen($passwordNew) >= USER_PASSWORD_MINIMUM && !$cvh->password($passwordNew)) {
					$e->add(t('A password may not contain ", \', >, <, or any spaces.'));
				}
				
				if($passwordNew) {
					if($passwordNew != $passwordNewConfirm) {
						$e->add(t('The two passwords provided do not match.'));
					}
				}
				$data['uPasswordConfirm'] = $passwordNew;
				$data['uPassword'] = $passwordNew;
			}		
			$keys = UserAttributeKey::getEditableInProfileList();
			foreach($keys as $key) {
				if($key->isAttributeKeyRequiredOnProfile() && $key->akID != 54) {
					$e1 = $key->validateAttributeForm();
					if($e1 == false) {
						$e->add(t('The field "%s" is required', $key->getAttributeKeyDisplayName()));
					} 
					else 
					if($e1 instanceof ValidationErrorHelper) {
						$e->add($e1);
					}
				}
			}
			
			foreach(UserProfile::$Fields as $f) {
				if(isset($data[$f])) {
					$this->user->profile->$f = $data[$f];
				}
			}
			UserProfile::save($this->user->profile);
			//echo "SAVED:".$this->user->profile->uID;
	
			$url = "/profile";
			if(!$this->user->isProfileOwner()) {
				$url .= "?uid=".$this->user->id;	
			}
			if(!$e->has()) {		
				$data['uEmail'] = $email;		
				if(ENABLE_USER_TIMEZONES) {
					$data['uTimezone'] = $this->post('uTimezone');
				}
				$this->user->info->update($data);
				
				foreach($keys as $key) {
					$key->saveAttributeForm($this->user->info);				
				}
				$this->redirect($url);
			} 
			else {
				$this->set('error', $e);
			}
		}
		$this->redirect($url);
	}
}

?>
