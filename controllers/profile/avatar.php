<?php
defined('C5_EXECUTE') or die("Access Denied.");
Loader::controller('/profile/edit');

class ProfileAvatarController extends ProfileEditController {
	public $view = 'avatar';
	public $user = null;
	
	public function __construct()
	{
		parent::__construct();
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		/*
		if(isset($_REQUEST['uid']) && ($_REQUEST['uid'] != $this->user->id || !$this->user->isSuperAdmin())) {
			//header("Location: ".BASE_URL.'/profile/avatar');
			echo $this->user->id;
			die();
		}
		*/
	}
	
	public function save_thumb()
	{
		if (!is_object($this->user->info) || $this->user->id < 1) {
			return false;
		}
		
		if(isset($_POST['thumbnail']) && strlen($_POST['thumbnail'])) {
			$thumb = base64_decode($_POST['thumbnail']);
			$fp = fopen(DIR_FILES_AVATARS."/".$this->user->id.".jpg","w");
			if($fp) {
				fwrite($fp,base64_decode($_POST['thumbnail']));
				fclose($fp);
				$data['uHasAvatar'] = 1;
				$this->user->info->update($data);
			}
		}	

		$this->redirect('/profile/avatar', 'saved');
	}
	
	public function saved() {
		$this->set('message', 'Avatar updated!');
	}

	public function deleted() {
		$this->set('message', 'Avatar removed.');
	}
	
	public function delete(){ 
		$this->user->info = $this->get('ui');
		$av = $this->get('av');
		
		$av->removeAvatar($this->user->info);
		$this->redirect('/profile/avatar', 'deleted');
	}

	public function setImageType($type=null) {
		Loader::model('user_profile');
		//$this->user->info = $this->get('ui');
		if($this->user->profile) {
			//echo "req:".$_REQUEST['imagetype'];
			if($type === null) $type = strtolower($_REQUEST['imagetype']);
			if($type != "facebook" && $type != "twitter" && $type != "google") {
				$type = "";
			}
			$this->user->profile->uImageType = $type;
			UserProfile::save($this->user->profile);
			//echo "SAVED(".$u->uID."):".$type;
			if($type) {
				$src = null;
				Loader::model("social_login", "social_login");
				Loader::library('hybridauth/Hybrid/Auth','social_login');
				$config = SocialLoginModel::getHybridAuthConfiguration();

				$hybridauth = new Hybrid_Auth( $config );
			
				if($type) {
					$type = ucfirst($type); 
					if($adapter = $hybridauth->authenticate($type)) {
						if($up = $adapter->getUserProfile()) {
							$src = $up->photoURL;
							//if(ADMIN) echo "src:".$src;
							if($src != null) {
								$filename = DIR_FILES_AVATARS.'/'.$this->user->id.'.jpg';
								//echo "COPY:".$src."<br>";
								//echo "TO:".$filename."<br>";
								copy($src, $filename);
							}
						}
					}
				}
			}
		}
		$this->user->profile->createAvatarSmall();
		
		if(!$this->user->profile->uArtistName || !$this->user->profile->uSummary) {
			header("Location: ".BASE_URL."/profile/edit?uid=".$this->user->info->getUserID());
			die;
		}
		return false;
	}
	
	public function saveImage() {
		if (!is_object($this->user->info) || $this->user->id < 1) {
			return false;
		}
		
		$data = substr($_POST['data'], strpos($_POST['data'], ",") + 1);
		$decodedData = base64_decode($data);
		echo ($decodedData);
		$filename = DIR_FILES_AVATARS.'/'.$this->user->getUserID().'.jpg';
		$fp = fopen($filename, 'wb');
		fwrite($fp, $decodedData);
		fclose($fp);
		
		$this->setImageType("none");

		$uid = "";
		if($this->user->id != $this->user->me->uID) {
			$uid = "?uid=".$this->user->id;
		}
		if(!$this->user->profile->uArtistName || !$this->user->profile->uSummary) {
			//header("Location: ".BASE_URL."/profile/edit?uid=".$this->user->info->getUserID());
			//die;
			echo BASE_URL."/profile/edit".$uid;
		}
		else {
			echo BASE_URL."/profile".$uid;
		}
		die;
 	}
	 
}
?>