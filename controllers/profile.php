<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ProfileController extends Controller {
	public $view = 'profile';
	public $helpers = array('html', 'form', 'text'); 
	
	public function on_start(){
		$this->error = Loader::helper('validation/error');
		$this->addHeaderItem(Loader::helper('html')->css('ccm.profile.css'));
		$this->set('controller', $this);
	}
	
	public function view($userid = 0) {
		if(!ENABLE_USER_PROFILES) {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
		$user = Loader::helper('user');
		if(!$userid && !$user->isLoggedIn()) {
			$this->redirect("/login");	
		}
		else {
			$user->setup($userid);
			$this->set('user', $user);
		}
	}
	
	public function save() {
		//echo "SAVE";	
	}
	
	public function on_before_render() {
		$this->set('error', $this->error);
	}	
}