<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class BlogController extends ProducersController {
	public $view = "blog";
	
	public function view($id=null) 
	{
		Loader::model("member_feature");
				
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		if($id) {
			if(is_numeric($id)) {
				$item = MemberFeature::getByID($id);
			}
			else {
				$item = MemberFeature::getByHandle($id);
			}
		}
		if($item) {
			$item->setMetaData("blog");
		}
		else {
			$this->setMetaData();
			
			$q = "type='blog'";
			if(!$this->user->isSuperAdmin()) {
				$q .= " AND status='active' AND published <= '".date("Y-m-d H:i:s", time())."'";
			}
			$item = MemberFeature::getOne($q, "ORDER BY published DESC");
		}
        
		$this->set('item', $item);
	}
	
	public function archive() 
	{
		$this->view = "archive";
		Loader::model("member_feature");
				
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		$limit = 9;
		$this->set('limit', $limit);
		
		$page = null;
		$paging = Loader::helper('paging');
		$limit = $paging->getLimit($limit, $page);
		$this->set('page', $page);
		
		$item = null;
		$q = "type='blog'";
		if(!$this->user->isSuperAdmin()) {
			$q .= " AND status='active' AND published <= '".date("Y-m-d H:i:s", time())."'";
		}
		$this->set('query', $q);
		$totalItems = MemberFeature::getCount($q);
		$this->set('totalItems', $totalItems);
		
		$items = MemberFeature::getAll($q, "published DESC ".$limit);
		
		if(count($items) == 1) {
			$item = $items[0];
			$items = null;
			$this->set('itemCount', 1);
		}
		else {
			$this->set('itemCount', count($items));
		}
		
		$this->set('items', $items);
		$this->set('item', $item);
	}
	
	public function setMetaData($path='members') {
		$meta = Loader::helper("meta");
		$meta->title 		= "Blog - Music News and Features";
		$meta->description 	= "Get the latest tips and tricks, helpful resources, and discover featured artists.";
		$meta->url 			= BASE_URL."/blog";
		$meta->image 		= BASE_URL."/images/blog_feature.jpg";
	}
}