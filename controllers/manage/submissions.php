<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ManageSubmissionsController extends Controller {
	public $view		= 'submissions';
	public $user 		= null;
	public $helpers 	= array('html', 'form', 'text'); 
	
	public function on_start()
	{
		$this->error = Loader::helper('validation/error');
		
		Loader::model('submission');
		
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
	}
	
	public function on_before_render() 
	{
		$this->set('error', $this->error);
	}	
	
	public function view($id=null) 
	{				
		$item = null;
		$items = null;
		if($id) {
			$item = Submission::getByID($id);
		}
		if(!$item) {
			if(isset($_REQUEST['closed'])) {
				$items = Submission::getAll("closed='".$_REQUEST['closed']."'");
			}
			else {
				$items = Submission::getAll();
			}
		}
		$this->set('item', $item);
		$this->set('items', $items);
	}
	
	public function edit($id=null) 
	{
		if($this->user->isSuperAdmin()) {
			if($id) {
				$item = Submission::getByID($id);
			}
			else {
				$item = new Submission();	
			}
			$this->set('item', $item);
			$this->render("/manage/submissions/edit");
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}	
	
	public function delete($id=null) 
	{
		if($this->user->isSuperAdmin()) {
			if($id) {
				Submission::deleteByID($id);
			}
			else {
				$item = new Submission();	
			}
			$this->render("/manage/submissions/");
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}	
	
	public function save() 
	{ 
		if($this->user->isSuperAdmin()) {
			$data = $this->post();
			
			//print_r($data); 
			if(isset($data['delete'])) {
				$item = Submission::getByID($data['id']);
				$this->delete($data['id']);
				$this->redirect("/manage/submissions");
			}
			else {			
				$create = false;
				if($data['id']) {
					$item = Submission::getByID($data['id']);
				}
				else {
					$create = true;
					$item = new Submission();
				}
				foreach(Submission::$Fields as $f) {
					if(isset($data[$f])) {
						$item->$f = $data[$f];
					}
				}
				if($create) {
					$item = Submission::create($item);
				}
				else {
					$item = Submission::save($item);
				}
				//print_r($_FILES);
				$err = "";
				if($_FILES["image"]["tmp_name"]) {
					$size = getimagesize($_FILES["image"]["tmp_name"]);
					if($size !== false && $size[0] == 1536 && $size[1] == 768) {
						$imageFileType = pathinfo($_FILES["image"]["tmp_name"]);
						if($_FILES["image"]["type"] == "image/jpeg") {
							$dstpath = DIR_BASE.'/images/submissions/'.$item->id.'_feature.jpg';
							move_uploaded_file($_FILES["image"]["tmp_name"], $dstpath);
						}
						else {
							$err .= "Invalid file type '".$_FILES["image"]["type"]."'. Please upload a jpeg image for the submission page.<br>";	
						}
					}
					else {
						$err .= "Invalid image size ".$size[0]."x".$size[1].". Please upload a jpeg image that is precisely 1536x768 pixels.<br>";
					}
				}
				
				if(!$err) {
					$this->redirect("/manage/submissions/".$item->handle);
				}
				else {
					$this->error->add($err);
					$this->set('item', $item);
					$this->render("/manage/submissions/edit");
				}
			}
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}
}