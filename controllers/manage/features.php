<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ManageFeaturesController extends Controller {
	public $view		= 'features';
	public $user 		= null;
	public $helpers 	= array('html', 'form', 'text'); 
	
	public function on_start()
	{
		$this->error = Loader::helper('validation/error');
		
		Loader::model('member_feature');
		
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if(!$this->user->isLoggedIn()) {
			$this->redirect("/login?redirect=".$_SERVER['REQUEST_URI']);
		}
		else
		if(!$this->user->isAdmin()) {
			header("HTTP/1.0 404 Not Found"); 
			$this->render("/page_not_found");
		}
	}
	
	public function on_before_render() 
	{
		$this->set('error', $this->error);
	}	
	
	public function view($id=null) 
	{				
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		$item = null;
		$items = null;
		if($id) {
			$item = MemberFeature::getByID($id);
		}
		if(!$item) {
			$this->view = 'features';
			if(isset($_REQUEST['status'])) {
				$q = "status='".$_REQUEST['status']."'";
			}
			else {
				$q = "1";//"status='active'";
			}
			if(isset($_REQUEST['blog'])) {
				$this->view = 'blog';
				$q .= " AND type = 'blog'";
			}
			else {
				$q .= " AND type != 'blog'";
			}
			if(!$this->user->isSuperAdmin()) {
				if($this->user->profile->uLocationAdmin) {
					$q .= " AND (location = '".$this->user->profile->uLocationAdmin."'";	
					$q .= " OR createdby = '".$this->user->id."')";
				}
			}
			//echo $q;
			$items = MemberFeature::getAll($q);
		}
		$this->set('item', $item);
		$this->set('items', $items);
	}
	
	public function edit($id=null) 
	{
		$canPost = $this->user->isSuperAdmin();
		if($id) {
			if(is_numeric($id)) {
				$item = MemberFeature::getByID($id);
			}
			else {
				$item = MemberFeature::getByHandle($id);
			}
			if($item->location == $this->user->profile->uLocationAdmin ||
				$item->createdby == $this->user->id) {
				$canPost = true;	
			}
		}
		
		Loader::model('on_deck');
		Loader::model('user_on_deck');
		Loader::model('user_on_deck_entry');
		$ondeck = null;
		if(isset($_REQUEST['ondeck'])) {
			$ondeck = OnDeck::getID($_REQUEST['ondeck']);
			if(!$ondeck) unset($_REQUEST['ondeck']);
			if(!$canPost) {
				$canPost = $this->user->isLocationAdmin($ondeck->Location);
			}
		}
		if($canPost) {
			if(!$item || isset($_REQUEST['rebuild'])) {
				if(!$item) $item = new MemberFeature();
				$item->status = 'hidden';
				
				if($ondeck) {
					$item->generateOnDeckBlog($ondeck);
				}
			}
			$item->handle = preg_replace("/[^a-zA-Z0-9_-]+/", "", $item->handle);
			
			$this->set('item', $item);
			$this->render("/manage/features/edit");
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}	
	
	
	public function delete($id=null) 
	{
		if($this->user->isSuperAdmin()) {
			if($id) {
				MemberFeature::deleteByID($id);
			}
			else {
				$item = new MemberFeature();	
			}
			$this->render("/manage/features/");
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}	
	
	public function save() 
	{ 
		//if($this->user->isSuperAdmin()) {
			$data = $this->post();
			
			//print_r($data); 
			if(isset($data['resetstats'])) {
				$item = MemberFeature::getByID($data['id']);
				$item->clearStats();
				$id = $data['handle'] ? $data['handle'] : $data['id'];
				if($item->type == 'blog') {
					header("Location: ".BASE_URL."/blog/".$id);
				}
				else {
					header("Location: ".BASE_URL."/members/".$item->type."/".$id);
				}
			}
			else
			if(isset($data['cancel'])) {
				$item = MemberFeature::getByID($data['id']);
				$id = $data['handle'] ? $data['handle'] : $data['id'];
				if($item->type == 'blog') {
					header("Location: ".BASE_URL."/blog/".$id);
				}
				else {
					header("Location: ".BASE_URL."/members/".$item->type."/".$id);
				}
			}
			else 
			if(isset($data['delete'])) {
				$item = MemberFeature::getByID($data['id']);
				MemberFeature::deleteByID($data['id']);
				if($item->type == 'blog') {
					header("Location: ".BASE_URL."/manage/features?blog=true");
				}
				else {
					header("Location: ".BASE_URL."/manage/features");
				}
			}
			else 
			if(isset($data['archive'])) {
				$item = MemberFeature::getByID($data['id']);
				$item->status = "archived";
				MemberFeature::save($item);
				$this->redirect("/manage/features");
			}
			else {			
				if(!$data['handle']) $data['handle'] = preg_replace("/[^a-zA-Z0-9_-]+/", "", $item->handle);
				$create = false;
				if($data['id']) {
					$item = MemberFeature::getByID($data['id']);
				}
				else {
					$create = true;
					$item = new MemberFeature();
				}
				foreach(MemberFeature::$Fields as $f) {
					if(isset($data[$f])) {
						$item->$f = $data[$f];
					}
				}
				if($create) {
					$item = MemberFeature::create($item);
				}
				else {
					$item = MemberFeature::save($item);
				}
				
				$ondeck = null;
				if(isset($data['ondeck_id'])) {
					Loader::model('on_deck');
					$ondeck = OnDeck::getID($data['ondeck_id']);
					$ondeck->BlogPost = $item->id;
					OnDeck::save($ondeck);
				}
				
				$err = "";
				if($_FILES["feature"]["tmp_name"]) {
					$size = getimagesize($_FILES["feature"]["tmp_name"]);
					if($size !== false && $size[0] == 1536 && $size[1] == 768) {
						//$imageFileType = pathinfo($_FILES["feature"]["tmp_name"]);
						if($_FILES["feature"]["type"] == "image/jpeg") {
							$dstpath = DIR_BASE.'/images/features/'.$item->id.'_feature.jpg';
							move_uploaded_file($_FILES["feature"]["tmp_name"], $dstpath);
						}
						else {
							$err .= "Invalid file type '".$_FILES["feature"]["type"]."'. Please upload a JPG for the feature image.<br>";	
						}
					}
					else {
						$err .= "Invalid image size. Please upload a JPG feature image that is precisely 1536x768 pixels<br>";
					}
				}
				else
				if($ondeck) {
					$src = DIR_BASE.'/images/features/'.strtolower($ondeck->Location).'_feature.jpg';
					$dst = DIR_BASE.'/images/features/'.$item->id.'_feature.jpg';
					copy($src, $dst);
				}
				
				if($_FILES["thumb"]["tmp_name"]) {
					$size = getimagesize($_FILES["thumb"]["tmp_name"]);
					if($size !== false && $size[0] == 768 && $size[1] == 768) {
						//$imageFileType = pathinfo($_FILES["thumb"]["tmp_name"]);
						if($_FILES["thumb"]["type"] == "image/jpeg") {
							$dstpath = DIR_BASE.'/images/features/'.$item->id.'_thumb.jpg';
							move_uploaded_file($_FILES["thumb"]["tmp_name"], $dstpath);
						}
						else {
							$err .= "Invalid file type '".$_FILES["thumb"]["type"]."'. Please upload a JPG for the thumbnail image.<br>";	
						}
					}
					else {
						$err .= "Invalid image size. Please upload a JPG thumbnail image that is precisely 768x768 pixels<br>";
					}
				}
				else
				if($ondeck) {
					$src = DIR_BASE.'/images/features/'.strtolower($ondeck->Location).'_thumb.jpg';
					$dst = DIR_BASE.'/images/features/'.$item->id.'_thumb.jpg';
					copy($src, $dst);
				}
				if(!$err) {
					$id = $data['handle'] ? $data['handle'] : $data['id'];
					if($item->type == 'blog') {
						header("Location: ".BASE_URL."/blog/".$id);
					}
					else {
						header("Location: ".BASE_URL."/members/".$item->type."/".$id);
					}
				}
				else {
					$this->error->add($err);
					$this->set('item', $item);
					$this->render("/manage/features/edit");
				}
			}
		//}
		//else {
		//	header("HTTP/1.0 404 Not Found");
		//	$this->render("/page_not_found");
		//}
	}
}