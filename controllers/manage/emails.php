<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ManageEmailsController extends Controller {
	public $view		= 'emails';
	public $user 		= null;
	public $helpers 	= array('html', 'form', 'text'); 
	
	public function on_start()
	{
		$this->error = Loader::helper('validation/error');
		
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if(!$this->user->isAdmin()) {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}
	
	public function on_before_render() 
	{
		$this->set('error', $this->error);
	}	
	
	public function view($id=null) 
	{
		Loader::model("locations");
		Loader::model("on_deck");
		Loader::model("facebook_event");
		Loader::model("user_subscription");
		
		if(isset($_REQUEST['adminemails'])) {
			$this->adminemails();	
		}
		
		if($this->user->isSuperAdmin()) {
			$templates = array(
				//"validate_user_email" 		=> "Welcome",
				"ondeck_register_email" 	=> "Activate Your Account",
				"complete_profile" 			=> "Complete Your Profile",
				"event_reminder" 			=> "Event Reminder",
				"event_reminder_tonight" 	=> "Event Reminder Tonight",
				"event_followup" 			=> "Event Blog Feature",
				"subscription_gift" 		=> "Membership Activated!",
				"subscription_confirm" 		=> "Membership Confirmation",
				"subscription_cancel" 		=> "Subscription Cancelled",
				"renewal_reminder" 			=> "Renewal Reminder",
				"ticket_confirmation" 		=> "Ticket Confirmation",
				"admin_reported_user" 		=> "User Reported",
				"ambassador_followup" 		=> "Ambassador Followup"
			);
			$subjects = array(
				"validate_user_email" 		=> "Welcome to the Producers Social",
				"ondeck_register_email" 	=> "Activate Your Account",
				"complete_profile" 			=> "Complete Your Profile",
				"event_reminder" 			=> "Event Reminder",
				"event_reminder_tonight" 	=> "Tonight!",
				"event_followup" 			=> "You've Been Featured!",
				"subscription_gift" 		=> "Membership Activated!",
				"subscription_confirm" 		=> "Membership Confirmation",
				"subscription_cancel" 		=> "Subscription Cancelled",
				"renewal_reminder" 			=> "Renewal Reminder",
				"ticket_confirmation" 		=> "Ticket Confirmation",
				"admin_reported_user" 		=> "User Reported",
				"ambassador_followup" 		=> "Ambassador Followup"
			);
			$this->set('templates', $templates);
		
			if(!$user_id) 	$user_id 	= $this->user->id;
			if(isset($_REQUEST['user_id'])) {
				$user_id = $_REQUEST['user_id'];
			}
			$this->set("user_id", $user_id);
			
			if(!$event_id) {
				$e = FacebookEvent::getOne("WHERE StartTime < '".date("Y-m-d H:i:s", time())."' ORDER BY StartTime DESC");
				if($e) {
					$event_id = $e->ID;
				}
				else {
					$event_id 	= "701544113353972";
				}
			}
			if(isset($_REQUEST['event_id'])) {
				$event_id = $_REQUEST['event_id'];
			}
			$this->set("event_id", $event_id);
			
			$info = UserInfo::getByID($user_id);
			$this->set("info", $info);
			
			$profile = UserProfile::getByUserID($user_id);
			$this->set("profile", $profile);
			
			$sub = UserSubscription::getByUserID($user_id);
			$this->set("sub", $sub);
			
			$location = $profile->uLocation;
			if(isset($_REQUEST['location'])) {
				$location = $_REQUEST['location'];
			}
			$this->set("location", $location);
		
			
			$feature_id = 61;
			if(isset($_REQUEST['feature_id'])) {
				$feature_id = $_REQUEST['feature_id'];
			}
			$this->set("feature_id", $feature_id);
			
			$template = null;
			if(isset($_REQUEST['template'])) {
				$template = $_REQUEST['template'];
			}
			$this->set("template", $template);
			//echo "TEMPLATE:".$template."<br>";
			
			$email = null;
			if(isset($_REQUEST['email'])) {
				$email = $_REQUEST['email'];
			}
			$this->set("email", $email);
			
			$preview 	= "No preview generated";
			$result 	= "No email sent";
			if($template) {
				$mh = Loader::helper('mail');
				$mh->from("admin@producerssocial.com");
				$mh->bcc("stephen@walkerfx.com");
				
				//$email = $info->getUserEmail();
				$mh->to($email);
				$mh->addParameter('numTickets', 1);
				$mh->addParameter('user_id', $user_id);
				$mh->addParameter('event_id', $event_id);
				$mh->addParameter('profile', $profile);
				$mh->addParameter('info', $info);
				$mh->addParameter('feature_id', $feature_id);
				$mh->addParameter('sub', $sub);
				
				$name = $profile->uArtistName;
				if(!$name) $name = $info->getUserName();
				$mh->addParameter('memberName', $name);
				$mh->addParameter('username', $name);
				
				$password = substr(uniqid(), 7);
				$mh->addParameter('password', $password);
				
				$locationName = Locations::getName($location);
				$mh->addParameter('location', $location);
				$mh->addParameter('locationName', $locationName);
				
				$uHash = $info->setupValidation();
				$mh->addParameter('uHash', $uHash);
				
				$event 		= null;
				$eventName	= null;
				$eventDate 	= null;
				$eventUrl 	= null;
				if($event_id) {
					$event = FacebookEvent::getID($event_id);
					$eventUrl = BASE_URL."eventinfo?id=".$event_id;
					$eventTime = strtotime($event->StartTime);
					$eventDate = date("l, M j", $eventTime)." starting at ".date("g:ia", $eventTime);
					$eventTime = date("g:ia", $eventTime);
					$eventName = $event->Name;
					
					$mh->addParameter('eventID', $eventID);
					$mh->addParameter('eventName', $eventName);
					$mh->addParameter('eventUrl', $eventUrl);
					$mh->addParameter('eventDate', $eventDate);
					$mh->addParameter('eventTime', $eventTime);
					$mh->addParameter('eventLocation', $event->Location);
					
					$ondeck = OnDeck::getByEventID($event_id);
					$mh->addParameter('ondeck', $ondeck);
				}
				
				$dontSendUrl = BASE_URL."/reminders?dontsend=".$user_id;
				$mh->addParameter('dontSendUrl', $dontSendUrl);
				
				if($template) {
					$subject = $subjects[$template];
					if($template == "event_reminder") {
						$subject = "Event Reminder: ".$eventName;
					}
					else 
					if($template == "event_reminder_tonight") {
						$subject = "TONIGHT! ".$eventName;
					}
					$this->set('subject', $subject);
					$mh->addParameter('subject', $subject);
				}
				
				$mh->load($template);
				$preview = $mh->getBodyHTML();
				
				if(isset($_REQUEST['send'])) {
					$mh->sendMail();
					$result = "Email has been sent to:".$email;
				}
				else {
					$result = "Generated preview of '".$template."'. No emails sent.";	
				}
			}
			$this->set('preview', $preview);
			$this->set('result', $result);
		}
	}
	
	public function exportcsv() 
	{
		if($this->user->isAdmin()) {
			$db = Loader::db();
			if(!$this->user->isSuperAdmin()) {
				$q = "AND p.uLocation='".$this->user->profile->uLocationAdmin."'";
			}
			$out = "Full Name, Artist Name, Location, Email\n";
			$r = $db->query("SELECT p.uFullName, p.uArtistName, p.uLocation, p.uEmail, u.uName, u.uEmail as Email2 FROM UserProfiles as p LEFT JOIN Users as u ON u.uID=p.uID WHERE p.uNewsletter=1 ".$q);
			while($row = $r->fetchRow()) {
				//print_r($row);
				$artist = $row['uArtistName'];
				//if(!$artist) $artist = $row['uName'];
				
				$email = $row['uEmail'];
				if(!$email) $email = $row['Email2'];
				
				$out .= $row['uFullName'].", ".$row['uArtistName'].", ".$row['uLocation'].", ".$email."\n";
			}
			
			//echo str_replace("\n", "<br>", $out);
			
			$file = $this->user->profile->uLocationAdmin."_ProducersSocial_".date("Y-m-d", time())."_Emails.csv";
			
			$size = strlen($out);
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . $file); 
			header('Content-Transfer-Encoding: binary');
			header('Connection: Keep-Alive');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . $size);
			
			echo $out;
			
			redirect("/manage/emails");
			die();
		}
		redirect("/manage/emails");
	}	
	
	public function adminemails() 
	{
		if($this->user->isSuperAdmin()) {
			$db = Loader::db();
			if(!$this->user->isSuperAdmin()) {
				$q = "AND p.uLocation='".$this->user->profile->uLocationAdmin."'";
			}
			$list = "";
			
			if($_REQUEST['adminemails'] == 2) {
				$q = "p.uLocationAdmin!='' && p.uLocationAdmin!='HQ' && p.uLocationAdmin!='LA'";
			}
			else {
				$q = "p.uLocationAdmin!=''";
			}
			
			$r = $db->query("SELECT p.uFullName, p.uArtistName, p.uLocation, p.uEmail, u.uName, u.uEmail as Email2 FROM UserProfiles as p LEFT JOIN Users as u ON u.uID=p.uID WHERE ".$q);
			while($row = $r->fetchRow()) {
				$email = $row['uEmail'];
				if(!$email) $email = $row['Email2'];
				
				$name = $row['uFullName'];
				if(!$name) $name = $row['uArtistName'];
				
				$list .= $name." ".$row['uLocation']." <".$email.">, ";
			}
			
			$this->set("email_list", $list);
			
			//$this->render("view");
		}
	}	
}