<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ManageAbuseController extends Controller {
	public $view		= 'abuse';
	public $user 		= null;
	public $helpers 	= array('html', 'form', 'text'); 
	
	public function on_start()
	{
		$this->error = Loader::helper('validation/error');
		
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if(!$this->user->isSuperAdmin()) {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}
	
	public function on_before_render() 
	{
		$this->set('error', $this->error);
	}	
	
	public function view($id=null) 
	{
		Loader::model("user_profile");
		$reported = UserProfile::getAllReported();
		$this->set('reported', $reported);
	}
}