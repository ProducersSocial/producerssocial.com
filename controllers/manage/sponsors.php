<?php
defined('C5_EXECUTE') or die("Access Denied.");

class ManageSponsorsController extends Controller {
	public $view		= 'sponsors';
	public $user 		= null;
	public $helpers 	= array('html', 'form', 'text'); 
	
	public function on_start()
	{
		$this->error = Loader::helper('validation/error');
		
		Loader::model('sponsor');
		
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
	}
	
	public function on_before_render() 
	{
		$this->set('error', $this->error);
	}	
	
	public function view($id=null) 
	{				
		$item = null;
		$items = null;
		if($id) {
			$item = Sponsor::getByID($id);
		}
		if(!$item) {
			if(isset($_REQUEST['status'])) {
				$items = Sponsor::getAll("status='".$_REQUEST['status']."'");
			}
			else {
				$items = Sponsor::getAll();
			}
		}
		$this->set('item', $item);
		$this->set('items', $items);
	}
	
	public function edit($id=null) 
	{
		if($this->user->isSuperAdmin()) {
			if($id) {
				$item = Sponsor::getByID($id);
			}
			else {
				$item = new Sponsor();	
			}
			$this->set('item', $item);
			$this->render("/manage/sponsors/edit");
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}	
	
	public function delete($id=null) 
	{
		if($this->user->isSuperAdmin()) {
			if($id) {
				Sponsor::deleteByID($id);
			}
			else {
				$item = new Sponsor();	
			}
			$this->render("/manage/sponsors/");
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}	
	
	public function save() 
	{ 
		if($this->user->isSuperAdmin()) {
			$data = $this->post();
			
			//print_r($data); 
			if(isset($data['delete'])) {
				$item = Sponsor::getByID($data['id']);
				$this->delete($data['id']);
				$this->redirect("/manage/sponsors");
			}
			else {			
				$create = false;
				if($data['id']) {
					$item = Sponsor::getByID($data['id']);
				}
				else {
					$create = true;
					$item = new Sponsor();
				}
				foreach(Sponsor::$Fields as $f) {
					if(isset($data[$f])) {
						$item->$f = $data[$f];
					}
				}
				if($create) {
					$item = Sponsor::create($item);
				}
				else {
					$item = Sponsor::save($item);
				}
				//print_r($_FILES);
				$err = "";
				if($_FILES["image"]["tmp_name"]) {
					$size = getimagesize($_FILES["image"]["tmp_name"]);
					if($size !== false && $size[0] == 220 && $size[1] == 140) {
						$imageFileType = pathinfo($_FILES["image"]["tmp_name"]);
						if($_FILES["image"]["type"] == "image/png") {
							$dstpath = DIR_BASE.'/images/sponsors/'.$item->id.'.png';
							move_uploaded_file($_FILES["image"]["tmp_name"], $dstpath);
						}
						else {
							$err .= "Invalid file type '".$_FILES["image"]["type"]."'. Please upload a PNG for the sponsor image.<br>";	
						}
					}
					else {
						$err .= "Invalid image size ".$size[0]."x".$size[1].". Please upload a PNG image that is precisely 220x140 pixels, white over transparent background.<br>";
					}
				}
				
				if(!$err) {
					$this->redirect("/manage/sponsors/".$item->handle);
				}
				else {
					$this->error->add($err);
					$this->set('item', $item);
					$this->render("/manage/sponsors/edit");
				}
			}
		}
		else {
			header("HTTP/1.0 404 Not Found");
			$this->render("/page_not_found");
		}
	}
}