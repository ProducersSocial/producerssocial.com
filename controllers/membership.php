<?php
defined('C5_EXECUTE') or die("Access Denied.");

class MembershipController extends Controller {
	public $user = null;
	public $helpers = array('html', 'form', 'text'); 
	
	public function __construct() {
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
	}
	
	public function on_start(){
		$this->error = Loader::helper('validation/error');
		$this->addHeaderItem(Loader::helper('html')->css('ccm.profile.css'));
	}
	
	public function on_before_render() {
		$this->set('error', $this->error);
	}	
}