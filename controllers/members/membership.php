<?php
defined('C5_EXECUTE') or die("Access Denied.");

class MembersMembershipController extends Concrete5_Controller_Profile_Edit {
	public $view 	= 'membership';
	public $user 	= null;
	public $helpers = array('html', 'form', 'date');
	
	public function __construct() {
		parent::__construct();
		$this->user = Loader::helper('user');
		$this->set('user', $this->user);
		$this->set('controller', $this);
		
		if(isset($_REQUEST['uid']) && ($_REQUEST['uid'] != $this->user->id || !$this->user->isSuperAdmin())) {
			header("Location: ".BASE_URL.'/members/membership');
			die();
		}
		
		$err 		= null;
		$confirm 	= false;
		$turnedon 	= false;
		$turnedoff 	= false;
		
		if($this->user->sub && isset($_REQUEST['autorenew']) && !isset($_REQUEST['cancel'])) {
			if(isset($_REQUEST['autorenewon']) || isset($_REQUEST['confirm'])) {
				if(isset($_REQUEST['autorenewoff'])) {
					$this->user->sub->setAutorenew(0);
					$turnedoff = true;
				}
				else {
					$this->user->sub->setAutorenew(1);
					$turnedon = true;
				}
				
				$stripe = Loader::helper('stripe');
				$stripe->startup();
				
				$err = $stripe->setAutorenew($this->user->sub->id, $_REQUEST['autorenew']);
			}
			else {
				$confirm = true;
			}
		}
		$this->set('confirm', $confirm);
		$this->set('turnedon', $turnedon);
		$this->set('turnedoff', $turnedoff);
		
		if($this->user->isSuperAdmin()) {
			Loader::model('user_purchase');
			Loader::model('user_ticket');
						
			$redirect = false;
			if(isset($_REQUEST['give'])) {
				if($this->user->sub && $this->user->sub->isActive()) {
					$this->user->sub->status = 0;
					UserSubscription::save($this->user->sub);
				}
				$userPurchase = new UserPurchase();
				$userPurchase->uID 				= $this->user->id;
				$userPurchase->uTransactionID 	= "admin:".$this->me;
				$userPurchase->uItem 			= $_REQUEST['give'];
				$userPurchase->uEventID 		= '';
				$userPurchase->uLocation		= $this->user->profile->uLocation;
				$userPurchase->uQuantity		= 1;
				$userPurchase->uAmount 			= 0;
				$userPurchase->uStatus 			= 'Completed';
				$userPurchase->uDate 			= date("Y-m-d H:i:s", time());
				$userPurchase->uNote			= "given by admin:".$user->me->id;
				UserPurchase::create($userPurchase);
	
				$subscription = new UserSubscription();
				$subscription->id 				= uniqid();
				$subscription->customer_id 		= '';
				$subscription->user_id 			= $this->user->id;
				if($_REQUEST['give'] == 'permanent') {
					$subscription->plan 			= 'yearly-membership';
					$subscription->type 			= 'permanent';
				}
				else {
					$subscription->plan 			= $_REQUEST['give'];
					$subscription->type 			= 'gift';
				}
				$subscription->autorenew 		= 0;
				$subscription->status 			= 1;	// 0=inactive 1=active 2=canceled
				$subscription->ending 			= date("Y-m-d H:i:s", time() + (365 * 24 * 60 * 60));
				$subscription->created 			= date("Y-m-d H:i:s", time());
				$subscription->note				= "given by admin:".$user->me->id;
				UserSubscription::create($subscription);
				
				$quantity = $subscription->planTicketQuantity();
				UserTicket::generate($quantity, $this->user->id, null, $userPurchase->uLocation, $subscription->ending); 
				$redirect = true;
			}
			else
			if(isset($_REQUEST['cancel'])) {
				if($this->user->sub) {
					$this->user->sub->status = 2;
					UserSubscription::save($this->user->sub);
				}				
				$redirect = true;
			}
			else
			if(isset($_REQUEST['reactivate'])) {
				if($this->user->sub) {
					$this->user->sub->status = 1;
					UserSubscription::save($this->user->sub);
				}				
				$redirect = true;
			}
			else
			if(isset($_REQUEST['delete'])) {
				if($this->user->sub) {
					UserSubscription::delete($this->user->sub->id);
				}				
				$redirect = true;
			}
			
			if($redirect) {
				header("Location: ".BASE_URL.'/profile/membership?uid='.$this->user->id);
				die();
			}
		}
		$this->set('err', $err);
	}
}

?>
