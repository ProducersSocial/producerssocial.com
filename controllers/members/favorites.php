<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersFavoritesController extends ProducersController {
	public $view = "favorites";
	
	public function view($id=null) 
	{
		Loader::model("user_like");
		Loader::model("member_feature");
		
		$feature = Loader::helper('feature');
		$this->set('feature', $feature);
		
		$limit = 9;
		$this->set('limit', $limit);
		
		$page = null;
		$paging = Loader::helper('paging');
		$limit = $paging->getLimit($limit, $page);
		$this->set('page', $page);
		
		if(!$id) $id = $this->user->id;
		$q = "liked=1";
		$totalItems = UserLike::countAllByUserID($id, $q);
		$this->set('totalItems', $totalItems);
		
		$this->set('query', $q." ".$limit);
		$likes = UserLike::getAllByUserID($id, $q, $limit);
		
		$items = null;
		if($likes) {
			$items = array();
			foreach($likes as $like) {
				$i = MemberFeature::getByID($like->item_id);
				if($i) $items[] = $i;
				else {
					//echo "DELETE:".$like->id."<br>";
					UserLike::delete($like->id);
				}
			}
		}
		if(count($items) == 1) {
			$item = $items[0];
			$items = null;
			$this->set('itemCount', 1);
		}
		else {
			$this->set('itemCount', count($items));
		}
		
		$this->setMetaData();
		$this->set('item', $item);
		$this->set('items', $items);
	}
	
	public function setMetaData($path='members') {
		$meta = Loader::helper("meta");
		//$meta->title 		= "Producers Social Blog";
		//$meta->description 	= "Get the latest tips and tricks, helpful resources, and discover featured artists.";
		//$meta->url 			= BASE_URL."/blog";
		//$meta->image 		= BASE_URL."/images/blog_feature.jpg";
	}
}