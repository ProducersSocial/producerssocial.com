<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersSubmissionsController extends ProducersController {
	public $view = "submissions";
	
	public function view($id=null) 
	{
		Loader::model("user_submission");
		Loader::model("member_feature");
		
		$submission = Loader::helper("feature");
		$this->set('feature', $submission);
		
		$limit = 4;
		$this->set('limit', $limit);
		
		$page = null;
		$paging = Loader::helper('paging');
		$limit = $paging->getLimit($limit, $page);
		$this->set('page', $page);
		
		if($id) {
			if(is_numeric($id)) {
				$item = MemberFeature::getByID($id);
			}
			else {
				$item = MemberFeature::getByHandle($id);
			}
			$this->set('item', $item);
		}
		if($item) {
			$item->setMetaData($this->view);
		}
		else {
			$this->setMetaData();
			
			$item = null;
			$q = "type='submissions'";
			if(!$this->user->isSuperAdmin()) {
				$q .= " AND status='active' AND published <= '".date("Y-m-d H:i:s", time())."'";
			}
			$items = MemberFeature::getAll($q, "created DESC ".$limit);
			
			$totalItems = MemberFeature::getCount($q);
			$this->set('totalItems', $totalItems);
			$this->set('query', $q." ".$limit);
			
			if(count($items) == 1) {
				$item = $items[0];
				$items = null;
			}
			
			$this->set('items', $items);
			$this->set('item', $item);
			$this->set('itemCount', count($items));
		}
	}
	
	public function submit() 
	{
		Loader::model("user_submission");
		Loader::model("member_feature");
		//error_reporting(E_ALL);
		//print_r($_REQUEST);
		$errormsg = null;
		if($_REQUEST['submit_title'] == null) {
			$errormsg = "Please enter the title name of the track you are submitting.";	
		}
		else
		if($_REQUEST['submit_artist'] == null) {
			$errormsg = "Please enter your artist name as you want it to appear in the final distribution.";	
		}
		else
		if($_REQUEST['submit_realname'] == null) {
			$errormsg = "Please enter your real name as you want it to appear.";	
		}
		else
		if($_REQUEST['submit_genre'] == null) {
			$errormsg = "Please enter at least one genre for your track.";	
		}
		else
		if($_REQUEST['submit_email'] == null) {
			$errormsg = "Please provide an email address so the coordinator may contact you if there are any issues with your tune.";	
		}
		else
		if(!isset($_FILES["submit_file"]) || !$_FILES["submit_file"]["tmp_name"]) {
			$errormsg = "Please select a 24bit unmastered wav file to upload.";	
		}
		
		$item = MemberFeature::getByID($_REQUEST['submission_id']);
		if(!$errormsg) {		
			$sub = null;
			$isNew = false;
			if(isset($_REQUEST['id']) && $_REQUEST['id']) {
				$sub = UserSubmission::getByID($_REQUEST['id']);	
			}
			if(!$sub) {
				$isNew = true;
				$sub = new UserSubmission();
			}
			$sub->user_id 		= $this->user->id;
			$sub->submission_id = $_REQUEST['submission_id'];
			$sub->title 		= $_REQUEST['submit_title'];
			$sub->artist 		= $_REQUEST['submit_artist'];
			$sub->realname 		= $_REQUEST['submit_realname'];
			$sub->genre 		= $_REQUEST['submit_genre'];
			$sub->email 		= $_REQUEST['submit_email'];
			$sub->note 			= $_REQUEST['submit_note'];
			
			if($_FILES["submit_file"]["tmp_name"]) {
				$dir = DIR_BASE.'/files/submissions/'.$_REQUEST['submission_id'];
				if(!file_exists($dir)) {
					mkdir($dir);	
				}
				//echo "DIR:".$dir;
				$ext = substr($_FILES["submit_file"]["name"], strrpos($_FILES["submit_file"]["name"], "."));
				$sub->filename = $sub->title."-".$sub->artist;
				$sub->filename = str_replace(" ", "_", $sub->filename);
				$sub->filename = preg_replace("/[^a-zA-Z0-9_-]+/", "", $sub->filename);
				$sub->filename .= $ext;
				
				move_uploaded_file($_FILES["submit_file"]["tmp_name"], $dir."/".$sub->filename);
			}
			
			if($isNew) {
				$sub = UserSubmission::create($sub);
			}
			else {
				$sub = UserSubmission::save($sub);
			}
			$sub->sendSlackNotifcation();
			
			$_REQUEST['success'] = true;
			
			if($item->submit_notify) {
				$mh = Loader::helper('mail');
				$mh->from("admin@producerssocial.com");
				$mh->bcc("stephen@producerssocial.com");
				$mh->to($item->submit_notify);
				$mh->addParameter('sub', $sub);
				$mh->addParameter('item', $item);
				$mh->load("submission");
				$mh->sendMail();
			}
			$_REQUEST['error'] = "TEST";	
		}
		else {
			//echo "ERROR:".$errormsg;
			$_REQUEST['error'] = $errormsg;	
		}
		
		//$this->redirect("/members/submissions/".$item->getHandle());
	}
	
	public function delete($id)
	{
		Loader::model("user_submission");
		Loader::model("member_feature");
				
		$sub = UserSubmission::getByID($id);
		$feature = MemberFeature::getByID($sub->submission_id);
		if($sub) {
			if($this->user->id == $sub->user_id || $this->user->isAdmin()) {
				UserSubmission::delete($id);
				$_REQUEST['message'] = "Your submission has been removed.";
			}
		}
		//$this->view($sub->submission_id);
		$this->redirect("/members/submissions/".$feature->getHandle());
	}

	public function edit($id)
	{
		error_reporting(E_ALL);
		Loader::model("user_submission");
		
		$sub = UserSubmission::getByID($id);
		//$this->render('view', $sub->submission_id, $id);
		$this->redirect("/members/submissions/".$sub->submission_id."?edit=".$id);
	}
	
	public function setMetaData($form=null) {
		$meta = Loader::helper("meta");
		//$meta->title 		= $form;
		//$meta->description 	= "Get the latest tips and tricks, helpful resources, and discover submissiond artists.";
		//$meta->url 			= BASE_URL."/blog";
		//$meta->image 		= BASE_URL."/images/blog_submission.jpg";
	}
}