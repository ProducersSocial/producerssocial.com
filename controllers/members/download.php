<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersDownloadController extends ProducersController {
	public $view = "download";
	
	public function view($id=null) 
	{
		Loader::model("member_feature");
		$feature = Loader::helper("feature");
		
		$message = "Your download will begin automatically.";
		if(!$id || !isset($_REQUEST['file'])) {
			$message = "Sorry, but the feature requested could not be found.";
		}
		else
		if($this->user->isLoggedIn() && $this->user->isMember()) {
			$file = $_REQUEST['file'];
			Loader::model("member_feature");
			$item = MemberFeature::getByID($id);
			if(!$item) {
				$message = "The requested file could not be found";	
			}
			else {
				$path = "/files/features/".$item->filekey."/".$file;
				$fullpath = DIR_BASE.$path;
				if(!file_exists($fullpath)) {
					$message = "The requested file '".$file."' does not exist.";
					if(ADMIN) {
						$message .= "<br>PATH:".$fullpath."<br>";	
					}
				}
				else {
					$download = Loader::helper('download');
					set_time_limit(0);
					$download->process($fullpath);
					/*
					$size = filesize($fullpath);
					
					//if(ADMIN) {
					//	echo "SIZE:".$size."<br>";
					//	echo "FILE:".$file."<br>";
					//}
					//else {
						header('Content-Description: File Transfer');
						header('Content-Type: application/octet-stream');
						header('Content-Disposition: attachment; filename="'.$file.'"'); 
						header('Content-Transfer-Encoding: binary');
						header('Connection: Keep-Alive');
						header('Expires: 0');
						header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
						header('Pragma: public');
						header('Content-Length: ' . $size);
						
						readfile($fullpath);
						redirect($feature->getItemURL($item));
						die();
					//}
					*/
				}
			}
		}
		else {
			$message = "<h1>Members Only</h2>";
			$message .= "The content you requested is for members only.<br><br>";
			if(!$this->user->isLoggedIn()) {
				$message .= "<a class='button' href='/login?redirect=".$_SERVER['REQUEST_URI']."'>Log in to your account</a>";
			}
			else
			if(!$this->user->isMember()) {
				$message .= "<a class='button' href='/membership'>Become a Member Now</a>";
			}
		}
		$this->set('message', $message);
	}
}