<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersSoftwareController extends ProducersController {
	public $view = "software";
	
	public function view($id=null) 
	{
		Loader::model("member_feature");
				
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		if($id) {
			if(is_numeric($id)) {
				$item = MemberFeature::getByID($id);
			}
			else {
				$item = MemberFeature::getByHandle($id);
			}
			$this->set('item', $item);
		}
		if($item) {
			$item->setMetaData($this->view);
		}
		else {
			$this->setMetaData();
			
			$item = null;
			$q = "type='software'";
			if(!$this->user->isSuperAdmin()) {
				$q .= " AND status='active' AND published <= '".date("Y-m-d H:i:s", time())."'";
			}
			$items = MemberFeature::getAll($q, "created DESC LIMIT 4");
			
			if(count($items) == 1) {
				$item = $items[0];
				$items = null;
			}

			$this->set('items', $items);
			$this->set('item', $item);
		}
	}
	
	public function setMetaData($path='members') {
		$meta = Loader::helper("meta");
		//$meta->title 		= "Producers Social Blog";
		//$meta->description 	= "Get the latest tips and tricks, helpful resources, and discover featured artists.";
		//$meta->url 			= BASE_URL."/blog";
		//$meta->image 		= BASE_URL."/images/blog_feature.jpg";
	}
}