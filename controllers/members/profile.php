<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersProfileController extends ProducersController {
	public $view = "profile";

	public function view($id=null) 
	{
		if(isset($_REQUEST['uid'])) {
			$id = $_REQUEST['uid'];
		}
		if($id) {
			$user = Loader::helper('user');
			
			if($user->isSuperAdmin()) {
				$db = Loader::db();
				$q = null;
				if(isset($_REQUEST['deactivate'])) {
					$q = "UPDATE Users SET uIsActive=0 WHERE uID=".$id;
					$db->execute($q);
					
					$q = "UPDATE UserProfiles SET uHide=1 WHERE uID=".$id;
					$db->execute($q);
				}
				else
				if(isset($_REQUEST['activate'])) {
					$q = "UPDATE Users SET uIsActive=1 WHERE uID=".$id;
					$db->execute($q);
					
					$q = "UPDATE UserProfiles SET uHide=0 WHERE uID=".$id;
					$db->execute($q);
				}
			}
			if(is_numeric($id)) {
				$profile = UserProfile::getByUserID($id);
				if(!$profile) {
					$profile = UserProfile::createForUser($id);	
				}
			}
			else {
				$profile = UserProfile::getByHandle($id);
				if($profile) {
					$id = $profile->uID;
				}
			}
			$this->set('id', $id);
			$this->set('profile', $profile);
			
			$user->setup($profile->uID);
			$this->set('user', $user);
			
			if($profile) {
				$info = UserInfo::getByID($id);
				$this->set('info', $info);
				
				$sub = UserSubscription::getByUserID($id);
				$this->set('sub', $sub);
			}
		}
		else {
			$this->render("/members/directory");
		}
	}
}