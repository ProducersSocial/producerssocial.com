<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersDirectoryController extends ProducersController {
	public $view = "directory";

	public function view() 
	{
		$keywords = null;
		if(isset($_REQUEST['keywords'])) {
			$keywords = $_REQUEST['keywords'];
		}
		$this->set('keywords', $keywords);

		$location = null;
		if(isset($_REQUEST['location'])) {
			$location = $_REQUEST['location'];
		}
		if($location) $location = strtoupper($location);
		$this->set('location', $location);

		$filter = null;
		if(isset($_REQUEST['filter'])) {
			$filter = $_REQUEST['filter'];
		}
		$this->set('filter', $filter);
		
		$search = null;
		if($keywords) {
			$words = explode(",", $keywords);
			foreach($words as $w) {
				$w = trim($w);
				if($search) $search .= " OR ";
				$search .= "(u.uName LIKE '%".$w."%' OR u.uName LIKE '%".$w."' OR u.uName LIKE '".$w."%' OR u.uName='".$w."')";
				$search .= " OR (u.uEmail LIKE '%".$w."%' OR u.uEmail LIKE '%".$w."' OR u.uEmail LIKE '".$w."%' OR u.uEmail='".$w."')";
				$search .= " OR (p.uArtistName LIKE '%".$w."%' OR p.uArtistName LIKE '%".$w."' OR p.uArtistName LIKE '".$w."%' OR p.uArtistName='".$w."')";
				$search .= " OR (p.uFullName LIKE '%".$w."%' OR p.uFullName LIKE '%".$w."' OR p.uFullName LIKE '".$w."%' OR p.uFullName='".$w."')";
				$search .= " OR (p.uSummary LIKE '%".$w."%' OR p.uSummary LIKE '%".$w."' OR p.uSummary LIKE '".$w."%' OR p.uSummary='".$w."')";
				$search .= " OR (p.uBio LIKE '%".$w."%' OR p.uBio LIKE '%".$w."' OR p.uBio LIKE '".$w."%' OR p.uBio='".$w."')";
				$search .= " OR (p.uStyleTags LIKE '%".$w."%' OR p.uStyleTags LIKE '%".$w."' OR p.uStyleTags LIKE '".$w."%' OR p.uStyleTags='".$w."')";
				$search .= " OR (p.uBandcamp LIKE '%".$w."%' OR p.uBandcamp LIKE '%".$w."' OR p.uBandcamp LIKE '".$w."%' OR p.uBandcamp='".$w."')";
				$search .= " OR (p.uSoundcloud LIKE '%".$w."%' OR p.uSoundcloud LIKE '%".$w."' OR p.uSoundcloud LIKE '".$w."%' OR p.uSoundcloud='".$w."')";
			}
		}		
		if($filter) {
			if($search) $search = "(".$search.") AND";
			$search .= " (p.uArtistName='".strtolower($filter)."%' OR p.uArtistName='".strtoupper($filter)."%')";
		}
		if($location && $location != 'ALL') {
			if($search) $search = "(".$search.") AND ";
			$search .= "p.uLocation='".$location."'";
		}
		if($search) $search = " AND (".$search.")";
		$this->set('search', $search);

		$fields = "";
		foreach(UserProfile::$Fields as $f) {
			if($fields) $fields .= ", ";
			$fields .= "p.".$f;
		}
		$db = Loader::db();
		
		$completeProfile = "p.uHasAvatar=1 AND p.uHide=0 AND p.uArtistName IS NOT NULL AND p.uArtistName != ''";
		
		$total = 0;
		$query = "SELECT COUNT(*) as total FROM UserProfiles as p LEFT JOIN Users as u ON u.uID=p.uID ";
		if($this->user->isSuperAdmin() && $search) {
			$query .= "WHERE 1 ".$search;
		}
		else {
			$query .= "WHERE (".$completeProfile.") ".$search;
		}
		if($r = $db->query($query)) {  
			$d = $r->FetchRow();
			$total = $d['total'];
		}
		$this->set('total', $total);
		
		
		$query = "SELECT ".$fields." FROM UserProfiles as p LEFT JOIN Users as u ON u.uID=p.uID ";
		if($this->user->isSuperAdmin() && $search) {
			$query .= "WHERE 1 ".$search;
		}
		else {
			$query .= "WHERE (".$completeProfile.") ".$search;
		}
		$query .= " ORDER BY p.uIsMember DESC, p.uArtistName ASC";
		
		$page = 0;
		$limit = 20;
		$this->set('limit', $limit);
		if(isset($_REQUEST['page']) && $_REQUEST['page']) {
			$page = $_REQUEST['page'];
			$limit = ($limit * ($_REQUEST['page']-1)).",".$limit;
		}
		$query .= " LIMIT ".$limit;
		$this->set('query', $query);
		$this->set('page', $page);
		
		//try {
			//$profiles = UserProfile::getQuery($query);
			$profiles = array();
			if($r = $db->query($query)) {
				while($row = $r->FetchRow()) {
					$nu = new UserProfile();
					foreach(UserProfile::$Fields as $f) {
						$nu->$f = $row[$f];
					}
					$profiles[] = $nu;
				}
			}
		//}
		//catch(Exception $e) {
		//	$this->set('error_message', $e->getMessage());
		//}
		$this->set('profiles', $profiles);
	}
}