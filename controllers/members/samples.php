<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersSamplesController extends ProducersController {
	public $view = "samples";
	
	public function view($id=null) 
	{
		Loader::model("member_feature");
				
		$feature = Loader::helper("feature");
		$this->set('feature', $feature);
		
		$limit = 4;
		$this->set('limit', $limit);
		
		$page = null;
		$paging = Loader::helper('paging');
		$limit = $paging->getLimit($limit, $page);
		$this->set('page', $page);
		
		if($id) {
			if(is_numeric($id)) {
				$item = MemberFeature::getByID($id);
			}
			else {
				$item = MemberFeature::getByHandle($id);
			}
			$this->set('item', $item);
		}
		if($item) {
			$item->setMetaData($this->view);
		}
		else {
			$this->setMetaData();
			
			$item = null;
			$q = "type='samples'";
			if(!$this->user->isSuperAdmin()) {
				$q .= " AND status='active' AND published <= '".date("Y-m-d H:i:s", time())."'";
			}
			$items = MemberFeature::getAll($q, "created DESC ".$limit);
			
			$totalItems = MemberFeature::getCount($q);
			$this->set('totalItems', $totalItems);
			$this->set('query', $q." ".$limit);
			
			if(count($items) == 1) {
				$item = $items[0];
				$items = null;
			}
			
			$this->set('items', $items);
			$this->set('item', $item);
			$this->set('itemCount', count($items));
		}
	}
	
	public function setMetaData($path='members') {
		$meta = Loader::helper("meta");
		//$meta->title 		= "Producers Social Blog";
		//$meta->description 	= "Get the latest tips and tricks, helpful resources, and discover featured artists.";
		//$meta->url 			= BASE_URL."/blog";
		//$meta->image 		= BASE_URL."/images/blog_feature.jpg";
	}
}