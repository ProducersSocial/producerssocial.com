<?php
defined('C5_EXECUTE') or die("Access Denied.");
require_once($env->getPath(DIRNAME_CONTROLLERS . '/producers.php', null));

class MembersTicketsController extends ProducersController {
	public $view = "tickets";
	
	public function __construct() {
		parent::__construct();
		
		if(isset($_REQUEST['uid']) && ($_REQUEST['uid'] != $this->user->id || !$this->user->isSuperAdmin())) {
			header("Location: ".BASE_URL.'/members/tickets');
			die();
		}
		
		if($this->user->isSuperAdmin()) {
			Loader::model('user_purchase');
			Loader::model('user_ticket');
			Loader::model('user_subscription');
			
			$redirect = false;
			if(isset($_REQUEST['give'])) {
				$userPurchase = new UserPurchase();
				$userPurchase->uID 				= $this->user->id;
				$userPurchase->uTransactionID 	= "admin:".$this->me;
				$userPurchase->uItem 			= "ticket";
				$userPurchase->uEventID 		= '';
				$userPurchase->uLocation		= $this->user->profile->uLocation;
				$userPurchase->uQuantity		= $_REQUEST['give'];
				$userPurchase->uAmount 			= 0;
				$userPurchase->uStatus 			= 'Completed';
				$userPurchase->uDate 			= date("Y-m-d H:i:s", time());
				$userPurchase->uNote			= "given by admin:".$user->me->id;
				UserPurchase::create($userPurchase);
				
				UserTicket::generate($_REQUEST['give'], $this->user->id, null, $userPurchase->uLocation, date("Y-m-d H:i:s", time() + (360 * 24 * 60 * 60))); 
				$redirect = true;
			}
			else
			if(isset($_REQUEST['remove'])) {
				UserTicket::deleteAll("user_id='".$this->user->id."'");
				$redirect = true;
			}
			
			if($redirect) {
				header("Location: ".BASE_URL.'/members/tickets?uid='.$this->user->id);
				die();
			}
		}
	}
	
	public function getUrl($values=null) {
		$url = "/members/tickets";
		$add = "?";
		if(!$this->user->isProfileOwner()) {
			$url .= "?uid=".$this->user->id;
			$add = "&";
		}
		
		if($values) {
			foreach($values as $k => $v) {
				$url .= $add;
				$add = "&";
				$url .= $k."=".$v;
			}
		}
		return $url;
	}
}

?>
