<?php      
defined('C5_EXECUTE') or die("Access Denied.");

class PbPostPageTypeController extends Controller {


	public function view(){
	    Loader::model('problog_list','problog');
		$blogify = Loader::helper('blogify','problog');
		$settings = $blogify->getBlogSettings();
		$embedlykey = $settings['embedly'];
		$this->set('next_link',$this->getNextPost());
		$this->set('prev_link',$this->getPrevPost());
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('page_types/pb_post.css', 'problog'));
		if($embedlykey){
			$this->addHeaderItem($html->javascript('jquery.embedly.js','problog'));
			$this->addFooterItem("
<script type=\"text/javascript\">
$(document).ready(function(){
  $('.embedly').each(function(){
    var w = $(this).parent().parent().width();
  	$(this).embedly({
  		key: '$embedlykey',
  		query: {
  			maxwidth: w,
  		},
  	});
  });
});
</script>
");
		}
	}
	
	public function getNextPost(){
		global $c;

		$page_path = str_replace('/index.php','',Loader::helper('navigation')->getLinkToCollection($c));
		$link = $c->getCollectionHandle();
		
		$shortened = str_replace($link.'/','',$page_path);
		$cID = $c->getCollectionID();
		
		$this->loadblogSections();
		
		$pl = new ProblogList();
		$pl->setItemsPerPage(1);
		$pl->filter(false,"cv.cID > $cID");
		$pl->filter(false,"(CHAR_LENGTH(cv.cvName) > 4 OR cv.cvName NOT REGEXP '^[0-9]')");
		$pl->filterByPublicDate(date('Y-m-d H:i:s'),'<=');
		
		$sections = $this->get('sections');
		$keys = array_keys($sections);
		if(is_array($keys)){
			foreach($keys as $id){
				if($fs){$fs .= ' OR ';}
				$path = Page::getByID($id)->getCollectionPath().'/';
				$fs .= "PagePaths.cPath LIKE '$path%'";
			}
			$pl->filter(false,"($fs)");
		}
		//$pl->debug();
		$posts = $pl->get();
		
		$np = $posts[0];

		if($np){
			return Loader::helper('navigation')->getLinkToCollection($np);
		}
	}
	
	public function getPrevPost(){
		global $c;

		$page_path = str_replace('/index.php','',Loader::helper('navigation')->getLinkToCollection($c));
		$link = $c->getCollectionHandle();
		
		$shortened = str_replace($link.'/','',$page_path);
		$cID = $c->getCollectionID();
              
		$this->loadblogSections();
		
		$pl = new ProblogList();
		$pl->setItemsPerPage(1);
		$pl->filter(false,"cv.cID < $cID");
		$pl->filter(false,"(CHAR_LENGTH(cv.cvName) > 4 OR cv.cvName NOT REGEXP '^[0-9]')");
		$pl->filterByPublicDate(date('Y-m-d H:i:s'),'<=');
		
		$sections = $this->get('sections');
		$keys = array_keys($sections);
		if(is_array($keys)){
			foreach($keys as $id){
				if($fs){$fs .= ' OR ';}
				$path = Page::getByID($id)->getCollectionPath().'/';
				$fs .= "PagePaths.cPath LIKE '$path%'";
			}
			$pl->filter(false,"($fs)");
		}
		
		$posts = $pl->get();
		
		$np = $posts[0];

		if($np){
			return Loader::helper('navigation')->getLinkToCollection($np);
		}
		
		/*
		Select the next highest cID that has a published version and the same parent path
		*/
	}
	
    protected function loadblogSections() {
		$blogSectionList = new PageList();
		$blogSectionList->setItemsPerPage($this->num);
		$blogSectionList->filterByBlogSection(1);
		$blogSectionList->sortBy('cvName', 'asc');
		$tmpSections = $blogSectionList->get();
		$sections = array();
		foreach($tmpSections as $_c) {
			$sections[$_c->getCollectionID()] = $_c->getCollectionName();
		}
		$this->set('sections', $sections);
	}
}