<?php     defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('proforms_item','proforms');
Loader::model('attribute/set');
$bodyHTML = t('
<p>Hey there!  We received your form ').$set->getAttributeSetName().t(' submission! See your details below!</p>

');
$export_skip = array(
	'auto_responder',
	'text_display',
	'title_display',
	'hr_display',
	'css_container',
	'fieldset_container',
	'optional_expand',
	'popup_form',
	'publish',
	'stack_display',
	'stepped_form'
);

$setAttribs = $set->getAttributeKeys();
foreach ($setAttribs as $ak) {
	if($ak->getReviewStatus() > 0){
		if(!in_array($ak->getAttributeType()->getAttributeTypeHandle(),$export_skip)){
			if (is_object($response)) {
				$aValue = $response->getAttributeValueObject($ak);
			}
			$bodyHTML .= '	
			   <div class="clearfix">';
			   if(!$ak->isAttributeKeyLabelHidden() && $ak->getReviewStatus() > 0){
					$bodyHTML .= '
						<strong>'.$ak->getAttributeKeyName().'</strong>';
				}
				$bodyHTML .= '		
				<div class="input">
					<div class="event-attributes">
						<div>
						';
						
			
						try {
						    $cnt = $ak->getController();
							if(method_exists($cnt,'getDisplayValue')){
								$val = $aValue->getDisplayValue('getDisplayValue');
							}elseif(method_exists($cnt,'getDisplayReviewValue')){
								$val = $aValue->getDisplayValue('getDisplayReviewValue');
							}else{
								$val = $aValue->getDisplayValue();
							}
							$bodyHTML .= $val;
						} catch (Exception $e) {
						    Log::addEntry('Mail exception: ',  $e->getMessage(), "\n");
						}
	
						
						$bodyHTML .= '	
	
						</div>
					</div>
				</div>
			</div>
			';
		}
	}
}

$bodyHTML .= t('
<hr/>

<p>Thanks so much!</p>

'.substr(BASE_URL, 7).' Admin Team');

$body = strip_tags($bodyHTML);