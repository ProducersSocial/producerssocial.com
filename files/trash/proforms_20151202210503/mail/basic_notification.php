<?php     defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('attribute/set');

$set = AttributeSet::getByID($setID);

$bodyHTML = t('
<p>There has been a new ').$set->getAttributeSetName().t(' ProForms submission!</p>

<p>Please make your way to your website admin area to review!</p>

<p>Thanks so much!</p>

'.substr(BASE_URL, 7).' Admin Team');