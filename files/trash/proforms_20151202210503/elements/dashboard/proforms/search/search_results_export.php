<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); 

Loader::model('proforms_item', 'proforms');
$searchType = 'proforms-item';

if (!$mode) {
	$mode = $_REQUEST['mode'];
}

$soargs = array();
$soargs['mode'] = $mode;

$pkg = Package::getByHandle('proforms_item');
$txt = Loader::helper('text');
$keywords = $_REQUEST['keywords'];
$bu = REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/search_results';

$pkg = Package::getByHandle('proforms');
$export_all = $pkg->config('PROFORMS_EXPORT_ALL');

if($_REQUEST['asID']!=null){
	if($_REQUEST['asID']===0){
		$_SESSION['asID'] = null;
	}else{
		$_SESSION['asID'] = $_REQUEST['asID'];
	}
}

$export_skip = array(
	'auto_responder',
	'text_display',
	'title_display',
	'hr_display',
	'css_container',
	'fieldset_container',
	'optional_expand',
	'popup_form',
	'publish',
	'stack_display',
	'stepped_form'
);

$csv_text = '';
$ourFileName = "php://output";
$fp = fopen($ourFileName, 'w');

header('Content-disposition: attachment; filename=proforms_export.csv');
header('Content-type: application/csv');

if (count($proforms_entry) > 0) {
	$headers = array();
	
	if(!$_SESSION['asID']){
		$slist = ProformsItemAttributeKey::getSortedColumnHeaderList();
	}else{
		if($export_all<1){
			$slist = ProformsItemAttributeKey::getColumnQuestionHeaderList($_SESSION['asID']);
		}else{
			$slist = AttributeSet::getByID($_SESSION['asID'])->getAttributeKeys();
		}
	}
	foreach($slist as $ak) { 
		if(!in_array($ak->getAttributeType()->getAttributeTypeHandle(),$export_skip)){
			$headers[] = $ak->getAttributeKeyDisplayHandle();
		}
	}
	$headers[] = 'Date';
	
	$count = count($headers);
	fputcsv($fp,$headers);
	
	foreach($proforms_entry as $noO) { 
		$vals = array();

		foreach($slist as $ak) { 
			if(!in_array($ak->getAttributeType()->getAttributeTypeHandle(),$export_skip)){
				$vo = $noO->getAttributeValueObject($ak);
				if (is_object($vo)) {
					$cnt = $ak->getController();
					
					if(method_exists($cnt,'getDisplayValue')){
						$val = $vo->getDisplayValue('getDisplayValue');
					}elseif(method_exists($cnt,'getDisplayReviewValue')){
						$val = $vo->getDisplayValue('getDisplayReviewValue');
					}else{
						$val = $vo->getDisplayValue();
					}
					$vals[] = str_replace('"',"'",$val);
				}else{
					$vals[] = '';
				}
			}
		} 
		$vals[] = date(DATE_APP_GENERIC_MDYT,strtotime($noO->ProformsItemDate));
		//var_dump($vals);
		fputcsv($fp,$vals);
	}
}
fclose($fp);
readfile($ourFileName);