<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<?php    
$searchType = 'proforms-item';

$searchFields = array(
	'' => '** ' . t('Fields'),
	'date_added' => t('Added Between')
);

$category = AttributeKeyCategory::getByHandle('proforms_item');

Loader::model('attribute/categories/proforms_item', 'proforms');
$searchFieldAttributes = ProformsItemAttributeKey::getSearchableList();
foreach($searchFieldAttributes as $ak) {
	$searchFields[$ak->getAttributeKeyID()] = $ak->getAttributeKeyName();
}
?>

<?php    $form = Loader::helper('form'); ?>

	
	<div id="ccm-<?php   echo $searchType?>-search-field-base-elements" style="display: none">
	
		<span class="ccm-search-option ccm-search-option-type-date_time"  search-field="date_added">
		<?php   echo $form->text('date_from', array('style' => 'width: 86px'))?>
		<?php   echo t('to')?>
		<?php   echo $form->text('date_to', array('style' => 'width: 86px'))?>
		</span>

		<span class="ccm-search-option"  search-field="is_active">
		<?php   echo $form->select('active', array('0' => t('Inactive proforms-item'), '1' => t('Active proforms-item')), array('style' => 'vertical-align: middle'))?>
		</span>
		
		<?php    foreach($searchFieldAttributes as $sfa) { 
			$sfa->render('search'); ?>
		<?php    } ?>
	
	</div>
	
	<form method="get" id="ccm-<?php   echo $searchType?>-advanced-search" action="<?php    echo REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/search_results'; ?>">
	<?php   echo $form->hidden('mode', 'search'); ?>
	<?php   echo $form->hidden('searchType', 'proforms-item'); ?>
	<input type="hidden" name="search" value="1" />
	
	<div class="ccm-pane-options-permanent-search">
		<!--
		<div class="span2">
		<?php   echo $form->label('keywords', t('Keywords'))?>
		<div class="input">
			<?php   echo $form->text('keywords', $_SESSION['keywords'], array('placeholder' => t('ProForms Keyword'), 'style'=> 'width: 140px')); ?>
		</div>
		</div>
		-->
		<div class="span4" style="padding-top: 22px;">
		    <a href="<?php   echo $this->url('/dashboard/proforms/overview/')?>" class="btn btn-small default" style="padding: 7px;"><i class="icon-th icon"></i></a>
			<a href="<?php   echo $this->url('/dashboard/proforms/editor/')?>" class="btn btn-small primary"><i class="icon-edit icon-white"></i>  <?php   echo t('Editor')?></a>
			<a href="<?php    echo $this->url('/dashboard/proforms/attributes')?>" id="" class="btn btn-small info "><i class="icon-list-alt icon-white"></i>  <?php    echo t('Questions')?></a>
			<a href="<?php    echo $this->url('/dashboard/proforms/manage_forms', 'category', $category->getAttributeKeyCategoryID())?>" id="" class="btn btn-small warning "><i class="icon-list-alt icon-white"></i>  <?php    echo t('Forms')?></a>
		</div>
		<div class="span7">
			<div class="span3">
			<?php   echo $form->label('asID', t('Form'))?>
			<div class="input">
				<?php   
				$category = AttributeKeyCategory::getByHandle('proforms_item');
				$sel = array('0' => t('** None'));
				$sets = $category->getAttributeSets();
				foreach($sets as $as) {
					$sel[$as->getAttributeSetID()] = $as->getAttributeSetName();
				}
				print $form->select('asID', $sel, $_SESSION['asID']);
				?>
			</div>
			</div>
			
			<div class="span3">
			<?php   echo $form->label('numResults', t('# Per Page'))?>
			<div class="input">
				<?php   echo $form->select('numResults', array(
					'10' => '10',
					'25' => '25',
					'50' => '50',
					'100' => '100',
					'500' => '500'
				), $_SESSION['numResults'], array('style' => 'width:65px'))?>
			</div>
	
			<?php   echo $form->submit('ccm-search-proforms-item', t('Search'), array('style' => 'margin-left: 10px'))?>
			<img src="<?php   echo ASSETS_URL_IMAGES?>/loader_intelligent_search.gif" width="43" height="11" class="ccm-search-loading" id="ccm-<?php   echo $searchType?>-search-loading" />
	
			</div>
		</div>
	</div>

	<a href="javascript:void(0)" onclick="ccm_paneToggleOptions(this)" class="ccm-icon-option-closed"><?php   echo t('Advanced Search')?></a>
	<div class="clearfix ccm-pane-options-content">
		<br/>
		<table class="zebra-striped ccm-search-advanced-fields" id="ccm-<?php   echo $searchType?>-search-advanced-fields" cellspacing="5px" cellpadding="5px">
		<tr>
			<th colspan="2" width="100%"><?php   echo t('Additional Filters')?></th>
			<th style="text-align: right; white-space: nowrap"><a href="javascript:void(0)" id="ccm-<?php   echo $searchType?>-search-add-option" class="ccm-advanced-search-add-field"><span class="ccm-menu-icon ccm-icon-view"></span><?php   echo t('Add')?></a></th>
		</tr>
		<tr id="ccm-search-field-base">
			<td><?php   echo $form->select('searchField', $searchFields);?></td>
			<td width="100%">
			<input type="hidden" value="" class="ccm-<?php   echo $searchType?>-selected-field" name="selectedSearchField[]" />
			<div class="ccm-selected-field-content">
				<?php   echo t('Select Search Field.')?>				
			</div></td>
			<td><a href="javascript:void(0)" class="ccm-search-remove-option"><img src="<?php   echo ASSETS_URL_IMAGES?>/icons/remove_minus.png" width="16" height="16" /></a></td>
		</tr>

		</table>

		<div id="ccm-search-fields-submit">
			<a href="<?php    echo REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/customize_search_columns'?>" id="ccm-list-view-customize"><span class="ccm-menu-icon ccm-icon-properties"></span><?php   echo t('Customize Results')?></a>
		</div>

	</div>	

	</form>	

<script type="text/javascript">
ccm_setupProformsItemSearch();
</script>