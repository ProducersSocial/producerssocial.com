<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); 

Loader::model('proforms_item', 'proforms');
$searchType = 'proforms-item';
?>
<div id="ccm-<?php   echo $searchType?>-search-results">
	<div class="ccm-pane-body">
	<?php    
	if (!$mode) {
		$mode = $_REQUEST['mode'];
	}
	
	$soargs = array();
	$soargs['mode'] = $mode;

	?>
	
		<div id="ccm-list-wrapper" style="margin-bottom: 10px">
		<?php    $form = Loader::helper('form'); ?>
				<a name="ccm-<?php   echo $searchInstance?>-list-wrapper-anchor"></a>
				<?php   
				$akc = AttributeKeyCategory::getByHandle('proforms_item');
				$akcID = $akc->getAttributeKeyCategoryID();
				?>
				
				<a id="ccm-proforms-export-results" href="javascript:void(0)" onclick="$('#ccm-proforms-item-advanced-search').attr('action', '<?php   echo BASE_URL.DIR_REL?>/index.php/tools/packages/proforms/proforms/search_results_export'); $('#ccm-proforms-item-advanced-search').get(0).submit(); $('#ccm-proforms-item-advanced-search').attr('action', '<?php   echo BASE_URL.DIR_REL?>/tools/packages/proforms/proforms/search_results');" style="float: right; margin-right: 8px;" class="btn success"><i class="icon-share icon-white"></i> <?php   echo t("Export Results")?></a>
				
				<select id="ccm-<?php   echo $searchType?>-list-multiple-operations" disabled>
					<option value="">** <?php    echo t('With Selected')?></option>
					<option value="proforms_item"><?php    echo t('Edit Properties')?></option>
					<option value="remove"><?php    echo t('Remove Item')?></option>
					<option value="duplicate"><?php    echo t('Duplicate Item')?></option>
					<?php    if ($mode == 'choose_multiple') { ?>
					<option value="choose"><?php    echo t('Choose')?></option>
					<?php    } ?>
				</select>

		</div>
		<div id="ccm-list-wrapper"><a name="ccm-<?php   echo $searchInstance?>-list-wrapper-anchor"></a>
			<?php    
			$pkg = Package::getByHandle('proforms_item');
			$txt = Loader::helper('text');
			$keywords = $_REQUEST['keywords'];
			$bu = REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/search_results';
			if (count($proforms_entry) > 0) { ?>
			<table border="0" cellspacing="0" cellpadding="0" id="ccm-<?php   echo $searchType?>-list" class="ccm-results-list">
				<tr>
					<th><input id="ccm-<?php   echo $searchType?>-list-cb-all" type="checkbox" /></th>
					<th>ID#</th>
				<?php    
				if(!$_SESSION['asID']){
					$slist = ProformsItemAttributeKey::getSortedColumnHeaderList();
				}else{
					$slist = ProformsItemAttributeKey::getColumnQuestionHeaderList($_SESSION['asID']);
				}
				foreach($slist as $ak) { ?>
					<th class="<?php    echo $ProformsItemList->getSearchResultsClass($ak)?>">
						<a href="<?php    echo $ProformsItemList->getSortByURL($ak, 'asc', $bu)?>"><?php    echo $ak->getAttributeKeyDisplayHandle()?></a>
					</th>
				<?php    } ?>
					<th class="<?php    echo $ProformsItemList->getSearchResultsClass('ProformsItemDate')?>"><a href="<?php    echo $ProformsItemList->getSortByURL('ProformsItemDate', 'asc', $bu)?>"><?php echo t('Date Submitted')?></a></th>
					<th class="ccm-search-add-column-header">
						<?php   if(!$_SESSION['asID']){ ?>
						<a href="<?php    echo REL_DIR_FILES_TOOLS . '/packages/proforms/proforms/customize_search_columns';?>" id="ccm-search-add-column">
							<img src="<?php    echo ASSETS_URL_IMAGES?>/icons/add.png" width="16" height="16" />
						</a>
						<?php   } ?>
					</th>
				
				</tr>
				<?php    
				foreach($proforms_entry as $noO) { 
						
					$action = View::url('/dashboard/proforms/search/edit/' . $noO->getProformsItemID());
					
					if ($mode == 'choose_one' || $mode == 'choose_multiple') {
						$action = 'javascript:void(0); ccm_triggerSelectProformsItem(' . $noO->getProformsItemID() . '); jQuery.fn.dialog.closeTop();';
					}
					
					if (!isset($striped) || $striped == 'ccm-list-record-alt') {
						$striped = '';
					} else if ($striped == '') { 
						$striped = 'ccm-list-record-alt';
					}
		
					?>
				<tr class="ccm-list-record <?php    echo $striped?>">
					<td class="ccm-<?php   echo $searchType?>-list-cb" style="vertical-align: middle !important">
						<input type="checkbox" <?php    if($noO->siteOwner != $agentID && $noO->siteOwner != $brokerID){ echo 'disabled';}?>
							value="<?php    echo $noO->getProformsItemID()?>" />
					</td>
					<td>
						<?php    echo $noO->getProformsItemID()?>
					</td>
					<?php    
					foreach($slist as $ak) { 
						?>
						<td <?php   
							if($noO->siteOwner == $agentID  || $noO->siteOwner == $brokerID){
							?>
							onclick="location.href='<?php    echo View::url('/dashboard/proforms/search/edit/' . $noO->getProformsItemID());?>';"
							<?php   
							}
							?>
						>
						<?php    
						//echo $noO->siteOwner;
						$cnt = $ak->getController();
						$vo = $noO->getAttributeValueObject($ak);
						if (is_object($vo)) {
							if(method_exists($cnt,'getDisplayReviewValue')){
								echo $vo->getDisplayValue('getDisplayReviewValue');
							}elseif(method_exists($cnt,'getDisplayValue')){
								echo $vo->getDisplayValue('getDisplayValue');
							}else{
								echo $vo->getDisplayValue();
							}
						}
						?></td>
					<?php    } ?>
					<td><?php echo date(DATE_APP_GENERIC_MDYT,strtotime($noO->ProformsItemDate))?></td>
					<td>&nbsp;</td>
				</tr>
				<?php    
				}
		
			?>
			</table>
			<?php    } else { ?>
			<?php    echo t('No Proforms Entry found.')?>
			<?php    } 
			if($proforms_entry){
				$ProformsItemList->displayPaging($bu, false, $soargs); 
			}
			?>
			<input type="hidden" id="url" url="<?php   echo BASE_URL.DIR_REL?>" val="<?php   echo BASE_URL.DIR_REL?>"/>
		</div>
		<?php    
		if($proforms_entry){
			echo $ProformsItemList->displaySummary();
		}
		?>
	</div>
</div>
<script type="text/javascript">
$(function() { 
	ccm_setupProformsItemSearch(); 
});
</script>
<?php   
$db = Loader::db();
?>
