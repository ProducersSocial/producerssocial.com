<?php     defined('C5_EXECUTE') or die("Access Denied.");
//Used on both page and file attributes
$c = Page::getCurrentPage();

$sets = array();
if (is_object($category) && $category->allowAttributeSets()) {
	$sets = $category->getAttributeSets();
}
$otypes = AttributeType::getList('proforms_item');
?>
<style type="text/css">
.icon_form {
display: block;
height:22px;
width:18px;
background-image:url('<?php     echo ASSETS_URL_IMAGES?>/icons_sprite.png')!important;
background-size: 47px auto!important;
}
.view,.view:hover {background-position: -6px -842px!important;height: 15px;}
.copy {background-position: -22px -439px;margin-right: 6px!important;}
.edit,.edit:hover {background-position: -6px -1390px!important;margin-right: 6px!important;height: 15px;}
.delete {background-position: -22px -635px;}
.ccm-ui .btn{padding: 4px 4px 3px!important;}
.ccm-ui h3{margin: 12px}
</style>
<div class="ccm-pane-options">
<form method="get" class="form-horizontal inline-form-fix" action="<?php   echo $this->url('/dashboard/proforms/attributes/select_type/')?>" id="ccm-attribute-type-form">
<div class="ccm-pane-options-permanent-search">

	<?php     $form = Loader::helper('form'); ?>


	<div class="span6">
		<a href="<?php   echo $this->url('/dashboard/proforms/editor/')?>" class="btn primary"><i class="icon-edit icon-white"></i>  <?php   echo t('Go To Editor')?></a>
		<a href="<?php    echo $this->url('/dashboard/proforms/manage_forms', 'category', $category->getAttributeKeyCategoryID())?>" id="" class="btn warning "><i class="icon-list-alt icon-white"></i>  <?php    echo t('Manage Forms')?></a>
	</div>


	<div class="input">
	
	<select name="atID" id="atID">
		<?php    foreach($otypes as $at){ ?>
		<option value="<?php   echo $at->getAttributeTypeID()?>" data-image="<?php   echo $at->getAttributeTypeIconSRC()?>"><?php   echo $at->getAttributeTypeName()?></option>
		<?php    } ?>
	</select>
	
	<button type="submit" class="btn success">
	  <i class="icon-ok-circle icon-white"></i> <?php   echo t('Add New Question')?>
	</button>
	
	</div>
</div>
</form>
</div>

<div class="ccm-pane-body">

<?php    
if (count($attribs) > 0) { ?>


	<?php    
	$ih = Loader::helper('concrete/interface');
	$valt = Loader::helper('validation/token');

	
	if (count($sets) > 0 && ($_REQUEST['asGroupAttributes'] !== '0')) { ?>
	
	
		<?php    
	
		foreach($sets as $as) { 
			
			Loader::model('proforms_question_set','proforms');
			$qs = new ProformsQuestionSet();
			$internal = $qs->QuestionSetIsInternal($as->getAttributeSetID());
		?>
	
			
		<h3>
		
		<a href="javascript: void(0);" rel="<?php   echo $as->getAttributeSetID()?>" class="show_questions btn btn-mini" style="font-size: 11px">
		<?php    if($set != $as->getAttributeSetID()){?>
			<i class="icon-plus-sign"></i>
		<?php    }else{ ?>
			<i class="icon-minus-sign"></i>
		<?php    } ?>
		</a> 
		<a href="<?php    echo Loader::helper('concrete/urls')->getToolsURL('proforms/preview_form.php','proforms');?>?qsID=<?php   echo $as->getAttributeSetID()?>" dialog-width="650" dialog-height="440" dialog-modal="true" dialog-title="<?php    echo t('Preview Form'); ?>" dialog-on-close="" onClick="javascript:;" class="dialog-launch btn btn-mini"><i class="icon-eye-open"></i>
		</a> 
		
		<a href="<?php    echo BASE_URL.DIR_REL?>/index.php/dashboard/proforms/editor/<?php   echo $as->getAttributeSetID()?>/"  class="btn btn-mini"><i class="icon-edit"></i></a>
		&nbsp;
		<?php    echo $as->getAttributeSetName()?> <?php    if($internal){echo ' ('.t('Internal').')';}?>
	
		</h3>
			<div class="questions_<?php   echo $as->getAttributeSetID()?>" <?php    if($set != $as->getAttributeSetID()){?>style="display: none;"<?php    } ?>>
		<?php    
		
		$setattribs = $as->getAttributeKeys();
		if (count($setattribs) == 0) { ?>
		
			<div class="ccm-attribute-list-wrapper"><?php    echo t('No Questions defined.')?></div>
		
		<?php     } else { ?>
			
			<div class="ccm-attribute-sortable-set-list ccm-attribute-list-wrapper" attribute-set-id="<?php    echo $as->getAttributeSetID()?>" id="asID_<?php    echo $as->getAttributeSetID()?>">			
			
			<?php    
			
			foreach($setattribs as $ak) { ?>
			
			<div class="ccm-attribute" data-key="<?php    echo $ak->getAttributeKeyID()?>" id="akID_<?php    echo $as->getAttributeSetID()?>_<?php    echo $ak->getAttributeKeyID()?>">
				<img class="ccm-attribute-icon" src="<?php    echo $ak->getAttributeKeyIconSRC()?>" width="16" height="16" />
				<a href="<?php    echo $this->url($editURL, 'edit', $ak->getAttributeKeyID())?>">
					<?php    if($ak->isAttributeKeyConditioned()){ echo '<i>'.t('(conditioned)').'</i>';}?>
					<?php    echo $ak->getAttributeKeyName()?>
				</a>
			</div>
	

			<?php     } ?>
			
			</div>
			
			<?php     } ?>
			
			</div>
		<?php     } 
			
		$unsetattribs = $category->getUnassignedAttributeKeys();
		if (count($unsetattribs) > 0) { ?>
		
			<h3><?php    echo t('Other')?></h3>
			<div class="ccm-attribute-list-wrapper">
			<?php    
			foreach($unsetattribs as $ak) { ?>
	
			<div class="ccm-attribute" data-key="<?php    echo $ak->getAttributeKeyID()?>" id="akID_<?php    echo $as->getAttributeSetID()?>_<?php    echo $ak->getAttributeKeyID()?>">
				<img class="ccm-attribute-icon" src="<?php    echo $ak->getAttributeKeyIconSRC()?>" width="16" height="16" /><a href="<?php    echo $this->url($editURL, 'edit', $ak->getAttributeKeyID())?>"><?php    echo $ak->getAttributeKeyName()?></a>
			</div>
	

			<?php     } ?>
			</div>
		
		<?php    
		
		}
	
	} else { ?>
		
		<div class="ccm-attributes-list">
		
		<?php    
		foreach($attribs as $ak) { ?>
		<div class="ccm-attribute" id="akID_<?php    echo $ak->getAttributeKeyID()?>">
			<img class="ccm-attribute-icon" src="<?php    echo $ak->getAttributeKeyIconSRC()?>" width="16" height="16" /><a href="<?php    echo $this->url($editURL, 'edit', $ak->getAttributeKeyID())?>"><?php    echo $ak->getAttributeKeyName()?></a>
		</div>
		
		<?php     } ?>
	
		</div>
	
	<?php     } ?>

<?php     } else { ?>
	
	<p>
		<?php    
	 echo t('No Questions defined.');
		?>
	</p>
	
<?php     } ?>

</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.dialog-launch').dialog();

	$("div.ccm-attribute-sortable-set-list").sortable({
		handle: 'img.ccm-attribute-icon',
		cursor: 'move',
		opacity: 0.5,
		stop: function() {
			var ualist = $(this).sortable('serialize');
			$.post('<?php    echo REL_DIR_FILES_TOOLS_REQUIRED?>/dashboard/attribute_sets_update', ualist, function(r) {

			});
		}
	});
	
	$('.show_questions').bind('click tap',function(){
		var id = $(this).attr('rel');
		if($('.questions_'+id).css('display') == 'none'){
			$('.questions_'+id).slideDown('slow');
			$(this).html('<?php   echo t('<i class="icon-minus-sign"></i>')?>');
		}else{
			$('.questions_'+id).slideUp('slow');
			$(this).html('<?php   echo t('<i class="icon-plus-sign"></i>')?>');
		}
	});

	try {
	$("#atID").msDropDown();
	} catch(e) {
	alert(e.message);
	}

});
/*]]>*/
</script>

<style type="text/css">
div.ccm-attribute-sortable-set-list img.ccm-attribute-icon:hover {cursor: move}
</style>
