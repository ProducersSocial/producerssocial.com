<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
?>
<div class="clearfix" id="captcha_input">
	<div class="input">
		<div class="proform-attributes">
			<?php   
				$captcha = Loader::helper('validation/captcha');
				$captcha->label();
				$captcha->display();
				$captcha->showInput();
			?>
		</div>
	</div>
</div>