<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
if(!isset($review)){$review = 0;}
if(!isset($custom_class)){$custom_class = 'input';}
?>
<div class="clearfix proformgroup <?php   echo $ak->atHandle?>">
	<?php   
	if(!$ak->isAttributeKeyLabelHidden() && $ak->getReviewStatus() > 0){
		?>
		<strong><label for="<?php echo $ak->getAttributeKeyHandle()?>"><?php echo $ak->getAttributeKeyName()?> <?php    if($ak->isAttributeKeyRequired()){ echo '<span class="required"> *</span>';} ?></label></strong>
		<?php    
	} 
	?>
	<div class="<?php echo $custom_class?>">
		<div class="proform-attributes">
			<div class="<?php    if($ak->isAttributeKeyRequired()){ echo 'required_input';}?><?php    if($expand){echo ' optional_expand" rel="'.$i.'"';}else{echo '"';}?> data-handle="<?php   echo $ak->getAttributeKeyHandle()?>">
			<?php   
			if(($ak->getAttributeType()->getAttributeTypeHandle() != 'signature' || !$value) && $review < 1){
				ob_start();
				$ak->render('form',$value);
				$input = ob_get_contents();
				ob_end_clean();
				if($ak->getAttributeKeyTooltip()){
					$input = str_replace(' class="',' title="'.$ak->getAttributeKeyTooltip().'" class="form_tooltip ',$input);
				}
				if($ak->isAttributeKeyMonetary()){
					$input = str_replace(' class="',' class="price_option ',$input);
				}
				echo $input;
			}else{
				$cnt = $ak->getController();
				if(method_exists($cnt,'display')){
					$ak->render('display',$value);
				}else{
					if(is_object($value)){
						$akID = $ak->getAttributeKeyID();	
						$val = $value->getValue();
				        if(is_a($val,'File')){
    				        echo $val->getFileName();
				        }elseif(!is_array($val) && !$ak->getTypeMultipart()){
							echo $val;
						}
					}
				}
				echo '<div style="display: none;">';
					$ak->render('form',$value);
				echo '</div>';
				echo '<br/>';
			}
			?>
			</div>
		</div>
	</div>
</div>