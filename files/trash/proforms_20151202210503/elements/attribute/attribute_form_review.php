<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

$attribute_skip = array(
	'auto_responder',
	//'text_display',
	//'title_display',
	//'hr_display',
	'css_container',
	'fieldset_container',
	'optional_expand',
	'popup_form',
	'publish',
	'stack_display',
	'stepped_form'
);

if(!in_array($ak->getAttributeType()->getAttributeTypeHandle(),$attribute_skip)){
	
	if(!$ak->isAttributeKeyLabelHidden() && $ak->getReviewStatus() > 0){
		?>
		<strong><?php    echo $ak->render('label');?> <?php    if($ak->isAttributeKeyRequired()){ echo '<span class="required"> *</span>';} ?></strong>
		<?php    
	} 
	
	$cnt = $ak->getController();
	if(is_object($aValue)){
		?>
		<div class="input">
			<div class="proform-attributes">
				<?php   
				if(is_object($aValue)){
					$cnt = $ak->getController();
					if(method_exists($cnt,'display_review')){
						$ak->render('display_review',$aValue);
					}elseif(method_exists($cnt,'display')){
						$ak->render('display',$aValue);
					}else{
						$akID = $ak->getAttributeKeyID();
			
						$val = $aValue->getValue();
				
						if(!is_array($val) && !$ak->getTypeMultipart()){
							echo $val;
						}
					}
				}
				?>
			</div>
		</div>
	<?php  
	}  
}
?>