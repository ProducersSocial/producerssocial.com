<?php     defined('C5_EXECUTE') or die("Access Denied.");
$form = Loader::helper('form'); 
$ih = Loader::helper("concrete/interface");
$valt = Loader::helper('validation/token');
$akName = '';
$akIsSearchable = 1;
$asID = 0;

if (is_object($key)) {
	if (!isset($akHandle)) {
		$akHandle = $key->getAttributeKeyHandle();
	}
	$akName = $key->getAttributeKeyName();
	$is_header = $key->isAttributeKeyHeader();
	$akIsSearchable = $key->isAttributeKeySearchable();
	$akIsSearchableIndexed = $key->isAttributeKeyContentIndexed();
	$orakIsRequired = $key->isAttributeKeyRequired();
	$hide_label = $key->isAttributeKeyLabelHidden();
	$sets = $key->getAttributeSets();
	$no_duplicates = $key->isAttributeKeyNoDuplicates();
	$is_conditioned = $key->isAttributeKeyConditioned();
		if($is_conditioned){
			$condition_handle = $key->getAttributeConditionHandle();
			$condition_operator = $key->getAttributeConditionOpperator();
			$condition_value = $key->getAttributeConditionValue();
		}
	$reviewStatus = $key->getReviewStatus();
	$type_handle = $key->getAttributeKeyType()->atHandle;
	$tooltip = $key->getAttributeKeyTooltip();
	
	//var_dump($sets);

	foreach($sets as $set){
		$asIDs[] = $set->getAttributeSetID();
	}
}
?>

<div class="ccm-pane-body">
	<style type="text/css">
		.pane legend{display: none;}
		ul.tabs li:focus {background-color: white!important;}
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#is_conditioned').change(function(){
				if($(this).is(':checked')){
					$('#condition_handle').attr('disabled',false);
					$('#condition_operator').attr('disabled',false);
					$('#condition_value').attr('disabled',false);
				}else{
					$('#condition_handle').attr('disabled',true);
					$('#condition_operator').attr('disabled',true);
					$('#condition_value').attr('disabled',true);
				}
			});
			<?php   if(!is_object($key)){ ?>
			$('#akName').keyup(function(event) {
				var text = $(this).val();
				var numberOfWords = text.split(' ').length;
		        if(numberOfWords >= 4)
		        {
		            return false;
		        }
				text = text.replace(/\s/g,'_');
				text = text.replace('.','_');
				text = text.replace('?','');
				text = text.replace('!','');
				text = text.toLowerCase();
			    $('#akHandle').val(text);
			});
			<?php   } ?>
		});
	</script>
<?php     if (is_object($key) && !$ajax) { ?>
	<?php    
	$ih = Loader::helper('concrete/interface');
	$delConfirmJS = t('Are you sure you want to remove this attribute?');
	?>
	<script type="text/javascript">
	deleteAttribute = function(e) {
		if (confirm('<?php    echo $delConfirmJS?>')) { 
			location.href = "<?php    echo $this->action('delete', $key->getAttributeKeyID(), $valt->generate('delete_attribute'))?>";				
		}
	}
	</script>
	
	<a type="submit" class="btn danger ccm-button-v2-right" onClick="deleteAttribute()">
	  <i class="icon-trash icon-white"></i> <?php   echo t('Delete Question')?>
	</a>

	<span style="float: right; margin-right: 12px;height: 22px;display: block;"></span>
	<?php    
	$copyConfirmJS = t('Are you sure you want to duplicate this attribute?');
	?>
	<script type="text/javascript">
	copyAttribute = function() {
		if (confirm('<?php    echo $copyConfirmJS?>')) { 
			location.href = "<?php    echo $this->action('duplicate', $key->getAttributeKeyID(), $valt->generate('copy_attribute'))?>";				
		}
	}
	</script>
	<a type="submit" class="btn success ccm-button-v2-right" onClick="copyAttribute()">
	  <i class="icon-camera icon-white"></i> <?php   echo t('Duplicate Question')?>
	</a>
<?php     } ?>

	<ul class="tabs">
		<li class="active"><a href="javascript:void(0);" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.general').show();"><?php    echo t('General')?></a></li>
		<li><a href="javascript:void(0);" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.options').show();"><?php    echo t('Options')?></a></li>	
		<li><a href="javascript:void(0);" onclick="$('ul.tabs li').removeClass('active'); $(this).parent().addClass('active'); $('.pane').hide(); $('div.form').show();"><?php    echo t('Settings')?></a></li>
	</ul>
	
	<div class="pane general">
		
		<?php    if(is_object($key)){ echo $form->hidden('akID', $key->getAttributeKeyID()); } ?>
		<?php    echo $form->hidden('questionSetID',$questionSetID); ?>
		<?php    echo $form->hidden('order',$order); ?>
		<?php    echo $form->hidden('type',$atID); ?>
		
		<?php  if($help){ ?>
		<span style="display: none" id="ccm-page-help-content"><?php echo $help?></span>
		
		<a href="javascript:void(0);" class="ccm-icon-help" id="ccm-page-help" data-original-title="Help" style="float:right;"><?php echo t('Help')?> <i class="icon icon-question-sign"></i></a>
		<?php  } ?>
		
		<h3><?php    echo t('%s: Basic Details', $type->getAttributeTypeName())?></h3><br />

		<div class="clearfix">
		<?php    echo $form->label('akName', t('Question Text'))?>
		<div class="input">
			<?php    echo $form->text('akName', $akName,array('autofocus'=>'true'))?>
			<span class="help-inline"><?php    echo t('Required')?></span>
		</div>
		</div>

		<div class="clearfix">
		<?php    echo $form->label('akHandle', t('Question Handle'))?>
		<div class="input">
			<?php    echo $form->text('akHandle', $akHandle)?>
			<span class="help-inline"><?php    echo t('Required')?></span>
		</div>
		</div>

		<div class="clearfix">
		<?php    echo $form->label('tooltip', t('Tooltip Text'))?>
		<div class="input">
			<?php    echo $form->text('tooltip', $tooltip)?>
		</div>
		</div>

		<?php     if ($category->allowAttributeSets() == AttributeKeyCategory::ASET_ALLOW_SINGLE) { ?>
		<div class="clearfix">
		<?php    echo $form->label('asID', t('Forms to include in'))?>
		<div class="input">
			<?php    
				if($questionSetID){
					$asIDs[] = $questionSetID;
				}
				$sel = array('0' => t('** None'));
				$sets = $category->getAttributeSets();
				foreach($sets as $as) {
					$sel[$as->getAttributeSetID()] = $as->getAttributeSetName();
				}
				print $form->selectMultiple('asID', $sel, $asIDs);
				?>
		</div>
		</div>
		<?php     } ?>
	</div>
	<div class="pane options" style="display: none;">
		<h3><?php    echo t('%s: General Options', $type->getAttributeTypeName())?></h3><br />
		<div class="clearfix">
			<label><?php    echo t('Options')?></label>
			<div class="input">
				<?php  
				if(substr($type->getAttributeTypeHandle(), 0,5)=='price'){
					echo $form->hidden('is_monetary',1);
				}
				?>
				<ul class="inputs-list">
				<?php    
					$category_handle = $category->getAttributeKeyCategoryHandle();
					$keyword_label = t('Content included in "Keyword Search".');
					$header_label = t('Use as results header.');
					$advanced_label = t('Field is searchable.');
					$required_label = t('Field is required.');
					$hide_label_label = t('Field label is hidden.');
					$no_duplicates_label = t('Do not allow duplicate entries');
					$conditions = t('Only available under condition:');
					
					if($is_conditioned){
						$disabled = null;
					}else{
						$disabled = 'disabled';
					}
					
					if(!$hide_label){
						$hide_label = $type->getController()->hideLabel;
					}
					?>
					<li><label><?php    echo $form->checkbox('is_header', 1, $is_header)?> <span><?php    echo $header_label?></span></label></li>
					<li style="display: none;"><label><?php    echo $form->checkbox('akIsSearchableIndexed', 1, 1)?> <span><?php    echo $keyword_label?></span></label></li>
					<li><label><?php    echo $form->checkbox('akIsSearchable', 1, $akIsSearchable)?> <span><?php    echo $advanced_label?></span></label></li>
					<li><label><?php    echo $form->checkbox('orakIsRequired', 1, $orakIsRequired)?> <span><?php    echo $required_label?></span></label></li>
					<?php    if($reviewStatus || $hide_label){ ?>
					<li><label><?php    echo $form->checkbox('hide_label', 1, $hide_label)?> <span><?php    echo $hide_label_label?></span></label></li>
					<?php    }?>
					<?php    if($type->getAttributeTypeHandle() == 'text' || $type->getController()->restrictDuplicates){ ?>
					<li><label><?php    echo $form->checkbox('no_duplicates', 1, $no_duplicates)?> <span><?php    echo $no_duplicates_label?></span></label></li>
					<?php    } ?>
					<li><label><?php    echo $form->checkbox('is_conditioned', 1, $is_conditioned)?> <span><?php    echo $conditions?></span></label>
						<br/>
							<?php   echo t('If ')?> <?php    echo $form->text('condition_handle',$condition_handle,array('class'=>'input-medium','placeholder'=>t('attribute_handle'),$disabled=>$disabled))?>  
							<?php    echo $form->select('condition_operator',array('=='=>'==','!='=>'!=','>='=>'>=','<='=>'<=','>'=>'>','<'=>'<'),$condition_operator,array('class'=>'input-mini',$disabled=>$disabled))?> 
							<?php   echo t(' value ')?> 
							<?php    echo $form->text('condition_value',$condition_value,array('class'=>'input-medium',$disabled=>$disabled))?> 
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="pane form" style="display: none;">
		<h3><?php    echo t('%s: Setting Options', $type->getAttributeTypeName())?></h3><br />
		<?php    echo $form->hidden('atID', $type->getAttributeTypeID())?>
		<?php    echo $form->hidden('akCategoryID', $category->getAttributeKeyCategoryID()); ?>
		<?php    echo $valt->output('add_or_update_attribute')?>
		<?php     
		if ($category->getPackageID() > 0) { 
			@Loader::packageElement('attribute/categories/' . $category->getAttributeKeyCategoryHandle(), $category->getPackageHandle(), array('key' => $key));
		} else {
			@Loader::element('attribute/categories/' . $category->getAttributeKeyCategoryHandle(), array('key' => $key));
		}
		?>
		<?php     $type->render('type_form', $key); ?>
	</div>
</div>
<div class="ccm-pane-footer">

<?php     if (is_object($key)) { ?>
	<button type="submit" class="btn primary ccm-button-v2-right" name="ccm-attribute-key-form" id="ccm-attribute-key-form">
	  <i class="icon-ok-sign icon-white"></i> <?php   echo t('Save Question')?>
	</button>
<?php     } else { ?>
	<button type="submit" class="btn primary ccm-button-v2-right" name="ccm-attribute-key-form" id="ccm-attribute-key-form">
	  <i class="icon-plus-sign icon-white"></i> <?php   echo t('Add Question')?>
	</button>
<?php     } ?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#akName').focus();
		$('#akName').select();
	});
</script>