<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

$condition_handle = $ak->getAttributeConditionHandle();
$condition_operator = $ak->getAttributeConditionOpperator();
$condition_value = $ak->getAttributeConditionValue();
$condition_keyID = $ak->getAttributeConditionKeyID();

echo '
<script type="text/javascript"> /*<![CDATA[*/ 
	function inCondition_'.$ak->getAttributeKeyID().'(){ 
	
		var key_'.$condition_handle.' = $(\'input[name="akID\\\['.$condition_keyID.'\\\]\\\[value\\\]"]\'); 
		
		key_'.$condition_handle.'.keyup(function(){
			check_condition_'.$ak->getAttributeKeyID().'(key_'.$condition_handle.'.val());
		});
		
		function check_condition_'.$ak->getAttributeKeyID().'(change_val){ 	
			var condition = "'.$condition_value.'";
			if($.isNumeric(condition)){
				condition = condition * 1;
				change_val = change_val *1;
			}
			if(change_val '.$condition_operator.' condition){ 
				$.ajax({ 
					type: \'get\', 
					url: \''.Loader::helper('concrete/urls')->getBlockTypeToolsURL($bt).'/ajax_attribute.php\', 
					data:{ akID: '.$ak->getAttributeKeyID().'} 
				}).done(function(html){ 
					$(\'#conditional_'.$ak->getAttributeKeyID().'\').html(html); 
					$(document).trigger(\'rebind\', [\'Custom\', \'Event\']); 
					$(\'.proform_slider\').css(\'height\',$(\'#conditional_'.$ak->getAttributeKeyID().'\').parent().height());
				}); 
			}else{ 
				$(\'#conditional_'.$ak->getAttributeKeyID().'\').empty(); 
				$(\'.proform_slider\').css(\'height\',($(\'#conditional_'.$ak->getAttributeKeyID().'\').parent().height())	); 
			}
		};
	}
	
	inCondition_'.$ak->getAttributeKeyID().'(); 
	
	$(document).on(\'rebind\', function() { 
		inCondition_'.$ak->getAttributeKeyID().'(); 
	}); 
/*]]>*/ 
</script>
';