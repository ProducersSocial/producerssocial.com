<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardProformsSettingsController extends Controller {


	public function on_start(){
		$pkg = Package::getByHandle('proforms');
		$export_all = $pkg->config('PROFORMS_EXPORT_ALL');
		$this->set('export_all',$export_all);
	}
	
	public function view(){
		
	}

	public function set_settings(){
		if(!$this->post('export_all')){
			$export_all = 0;
		}else{
			$export_all = $this->post('export_all');
		}
		$pkg = Package::getByHandle('proforms');
		$pkg->saveConfig('PROFORMS_EXPORT_ALL', $export_all);
		$this->view();
	}
	
}

?>