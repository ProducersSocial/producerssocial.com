<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardProformsEditorController extends Controller {

	public function view($question_set=null){
		$this->set('question_set',$question_set);
		$this->set('otypes',AttributeType::getList('proforms_item'));
		
		$this->category = AttributeKeyCategory::getByHandle('proforms_item');
		if (is_object($this->category)) {
			$sets = $this->category->getAttributeSets();
			$this->set('sets', $sets);
		}
	}
	
	public function expanded_mode($question_set=null){
		$this->set('question_set',$question_set);
		$this->set('expanded_mode',true);
		$this->set('otypes',AttributeType::getList('proforms_item'));
		
		$this->category = AttributeKeyCategory::getByHandle('proforms_item');
		if (is_object($this->category)) {
			$sets = $this->category->getAttributeSets();
			$this->set('sets', $sets);
		}
	}
	
	public function on_start() {
		$this->set('category', AttributeKeyCategory::getByHandle('proforms_item'));
		
		$subnav = array(
			array(View::url('/dashboard/proforms/search/add/'), t('Add/Edit')),
			array(View::url('/dashboard/proforms/search'), t('Search')),
			array(View::url('/dashboard/proforms/attributes'), t('Attributes')),
			array(View::url('/dashboard/proforms/settings'), t('Settings')),
		);
		
		$this->set('subnav', $subnav);
		$html = Loader::helper('html');
		$this->addHeaderItem($html->javascript('jquery.dd.min.js','proforms'));
		$this->addHeaderItem($html->css('dd.css','proforms'));
		$this->addHeaderItem($html->css('form_editor.css','proforms'));
		$at = AttributeType::getByHandle('multiplex');
		$jspath = $at->getAttributeTypeFileURL('multiplex.js');
		$path = $at->getAttributeTypeFileURL('js/jQuery.geoselector.js');
		$json_path = $at->getAttributeTypeFileURL('js/divisions.json');
		$this->addFooterItem($html->javascript($path));
		$this->addHeaderItem($html->css('ccm.app.css'));
		$this->addHeaderItem($html->css('jquery.ui.css'));
		$this->addHeaderItem($html->css('bootstrap.css','proforms'));
		$this->addHeaderItem($html->javascript('jquery.js'));
		$this->addFooterItem($html->javascript('jquery.ui.js'));
		$this->addFooterItem($html->javascript('jquery.form.js'));
		$this->addFooterItem($html->javascript('ccm.app.js'));
		$this->addFooterItem($html->javascript('bootstrap.js'));		
		$this->addFooterItem($html->javascript('jquery.signaturepad.js','proforms'));	
		$this->addFooterItem($html->javascript($jspath));
		$this->addFooterItem($html->javascript('bootstrap.afix.js','proforms'));
		$h = Loader::helper('lists/states_provinces');
		$provinceJS = "<script type=\"text/javascript\"> var ccm_attributeTypeAddressStatesTextList = '\\\n";
		$all = $h->getAll();
		foreach($all as $country => $countries) {
			foreach($countries as $value => $text) {
				$provinceJS .= str_replace("'","\'", $country . ':' . $value . ':' . $text . "|\\\n");
			}
		}
		$provinceJS .= "'</script>";
		$this->addFooterItem($provinceJS);
		$this->addFooterItem($html->javascript('country_state.js','proforms'));
		$this->addFooterItem($html->javascript('form_editor.js','proforms'));
	}
}