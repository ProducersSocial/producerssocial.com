<?php     

defined('C5_EXECUTE') or die(_("Access Denied."));

/**
*
* An object that allows a filtered list of Form Entries to be returned.
* @package ProformsItems
*
*/
Loader::model('proforms_item', 'proforms');
Loader::Library('item_list_proforms_item','proforms');

class ProformsItemList extends ProformsItemItemList {
	
	protected $filter_reviewed = 1;
	protected $attributeFilters = array();
	protected $attributeClass = 'ProformsItemAttributeKey';
	protected $autoSortColumns = array('ProformsItemID');
	protected $itemsPerPage = 10;

	
	protected function createQuery() {
		if(!$this->queryCreated){
			$this->setBaseQuery($this->baseExtend);
			$this->setupAttributeFilters("LEFT JOIN ProformsItemSearchIndexAttributes ON (ProformsItemSearchIndexAttributes.ProformsItemID = ci.ProformsItemID) ".$this->afterSetup);
			$this->queryCreated=1;
			$this->baseExtend = null;
			$this->afterSetup = null;
			//if(!$_REQUEST['ccm_order_by']){
			//	$this->sortByString = 'ProformsItemDate DESC';
			//}
		}
	}	
	protected function setBaseQuery($additionalFields) {
		$this->setQuery('SELECT DISTINCT ci.ProformsItemID, ci.asID, ci.ProformsItemDate  ' . $additionalFields . ' FROM ProformsItem ci');
		if($this->filter_reviewed > 0){
			$this->filter(false, "(ci.reviewed < 1 || ci.reviewed IS NULL)");
		}else{
			//$this->filter(false, "(ci.reviewed = 0)");
		}
	}
	
	/* magic method for filtering by page attributes. */
	public function __call($nm, $a) {

		if (substr($nm, 0, 8) == 'filterBy') {
			$txt = Loader::helper('text');
			$attrib = $txt->uncamelcase(substr($nm, 8));

			if (count($a) == 2) {
				$this->filterByAttribute($attrib, $a[0], $a[1]);
			} else {
				$this->filterByAttribute($attrib, $a[0]);
			}
		}			
	}
	
	// Returns an array of userInfo objects based on current filter settings
	public function get($itemsToGet = 0, $offset = 0) {
		$proforms_entry = array(); 
		$this->createQuery();
		$r = parent::get($itemsToGet, intval($offset),true);
		foreach($r as $row) {
			$no = ProformsItem::getByID($row['ProformsItemID'], $row['distance']);	
			if($no){		
				$proforms_entry[] = $no;
			}
		}
		return $proforms_entry;
	}	
	
	public function getTotal(){ 
		$this->createQuery();
		return parent::getTotal();
	}
	
	public function sortByDate($order){
		$this->sortByString = 'ProformsItemDate '.$order;
	}	
	
	public function filterByReviewed($review){
		if($review == 0){
			$this->filter_reviewed = null;
		}
	}
	
	public function filterByID($ObjectID, $comparison = '=') {
		$this->filter('ProformsItemID', $ObjectID, $comparison);
	}

	public function filterByAttributeSet($asID){
		$this->filter(false, "ci.asID = $asID");
	}
	
	public function filterByKeywords($keywords) {
		$dbs = Loader::db();
		$keywordsExact = $dbs->quote($keywords);
		$qkeywords = $dbs->quote('%' . $keywords . '%');
		$keys = ProformsItemAttributeKey::getSearchableIndexedList();
		$attribsStr = '';
		foreach ($keys as $ak) {
			$cnt = $ak->getController();			
			$attribsStr.=' OR ' . $cnt->searchKeywords($keywords);
		}
		$this->filter(false, '(ci.ProformsItemID LIKE ' . $qkeywords . $attribsStr . ')');
	}
	
	public function filterByAttributeKeys($keys,$excluded=null){
	    Loader::model('attribute/categories/proforms_item','proforms');
	    if(is_array($keys)){
			foreach($keys as $akID=>$filter){
				if(is_integer($akID)){
					$ak = ProformsItemAttributeKey::getByID($akID);
					if(is_object($ak)){
						if($filter['atSelectOptionID']){
							if($fi){$category_filter .= ' AND ';}
							$category_filter .= '(';
							$fis = null;
							foreach($filter['atSelectOptionID'] as $option){
								if($fis){$category_filter .= ' OR ';}
								$akc = $ak->getController();
								if(is_numeric($option)){
									$val = SelectAttributeTypeOption::getByID($option);
									$string = $val->value;
								}else{
									$string = $option;
								}
								
								if($ak->getAttributeKeyType()->atHandle == 'select_values'){
								    $pair = explode(':^:',$string);
    								$string = $pair[0];
								}
								
								$handle = $ak->getAttributeKeyHandle();
								if($excluded && in_array($akID,$excluded)){
									$category_filter .= "ak_".$handle." NOT LIKE '%\n$string\n%'";
								}else{
									$category_filter .= "ak_".$handle." LIKE '%\n$string\n%'";
								}
								if($excluded && in_array($akID,$excluded)){
									$category_filter .= " OR ak_".$handle." IS NULL";
								}
								$fis++;
							}
							$fi++;
							$category_filter .= ')';
							
						}elseif($filter['atSelectNewOption']){
							if($fi){$category_filter .= ' AND ';}
							$category_filter .= '(';
							$fis = null;
							foreach($filter['atSelectNewOption'] as $option){
								if($fis){$category_filter .= ' OR ';}
								$akc = $ak->getController();
								
								if(is_numeric($option)){
									$val = SelectAttributeTypeOption::getByID($option);
									$string = $val->value;
								}else{
									$string = $option;
								}
								
								if($ak->getAttributeKeyType()->atHandle == 'select_values'){
								    $pair = explode(':^:',$string);
    								$string = $pair[0];
								}
								
								$handle = $ak->getAttributeKeyHandle();
								if($excluded && in_array($akID,$excluded)){
									$category_filter .= "ak_".$handle." NOT LIKE '%\n$string\n%'";
								}else{
									$category_filter .= "ak_".$handle." LIKE '%\n$string\n%'";
								}
								$fis++;
							}
							$fi++;
							$category_filter .= ')';
							
						}elseif($filter['atSelectMultiOptionID']){
							if($fi){$category_filter .= ' AND ';}
							$category_filter .= '(';
							$fis = null;
							foreach($filter['atSelectMultiOptionID'] as $option){
								if($fis){$category_filter .= ' OR ';}
								$akc = $ak->getController();
								$val = MultiSelectAttributeTypeOption::getByID($option);
								$string = $val->value;
								$handle = $ak->getAttributeKeyHandle();
								if($excluded && in_array($akID,$excluded)){
									$category_filter .= "ak_".$handle." NOT LIKE '%\n$string\n%'";
								}else{
									$category_filter .= "ak_".$handle." LIKE '%\n$string\n%'";
								}
								$fis++;
							}
							$fi++;
							$category_filter .= ')';
							
						}elseif($filter['atMultiSelectOptionOptionID']){
							if($fi){$category_filter .= ' AND ';}
							$category_filter .= '(';
							$fis = null;
							foreach($filter['atMultiSelectOptionOptionID'] as $option){
								if($fis){$category_filter .= ' AND ';}
								$akc = $ak->getController();
								$val = MultiSelectAttributeTypeOption::getByID($option);
								$string = $val->value;
								$handle = $ak->getAttributeKeyHandle();
								if($excluded && in_array($akID,$excluded)){
									$category_filter .= "ak_".$handle." NOT LIKE '%\n$string\n%'";
								}else{
									$category_filter .= "ak_".$handle." LIKE '%\n$string\n%'";
								}
								$fis++;
							}
							$fi++;
							$category_filter .= ')';
							
						}elseif($filter['value']){
							//var_dump($akID);
							$string = $filter['value'];
							if(is_array($string)){
								$vals_array = $string;
								foreach($vals_array as $string){
									if($fis){$category_filter .= ' AND ';}
									$category_filter .= '(';
									$handle = $ak->getAttributeKeyHandle();
									if($string == ''){
										$category_filter .= "ak_".$handle." LIKE '%$string%' OR ak_".$handle." IS NULL";
									}else{
										if($excluded && in_array($akID,$excluded)){
											$category_filter .= "ak_".$handle." NOT LIKE '%$string%'";
										}else{
											$category_filter .= "ak_".$handle." LIKE '%$string%'";
										}
									}
									$fis++;
									
									$category_filter .= ')';		
								}
							}else if($string != ''){
						
								if($fis){$category_filter .= ' AND ';}
								$category_filter .= '(';
								$handle = $ak->getAttributeKeyHandle();
								
								if($string == ''){
									$category_filter .= "ak_".$handle." LIKE '%$string%' OR ak_".$handle." IS NULL";
								}else{
									if($excluded && in_array($akID,$excluded)){
										$category_filter .= "ak_".$handle." NOT LIKE '%$string%'";
									}else{
										$category_filter .= "ak_".$handle." LIKE '%$string%'";
									}
								}
								
								$fis++;
								
								$category_filter .= ')';
							}
							$fi++;
						}
					}
				}
			}
			if($category_filter){
				$this->filter(false,$category_filter);
			}
		
		}

	}

}