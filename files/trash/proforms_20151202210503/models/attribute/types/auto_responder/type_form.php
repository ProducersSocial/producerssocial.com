<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');

?>
<style type="text/css">
.ccm-ui label input, .ccm-ui label textarea, .ccm-ui label select{display: inline-block!important}
.clearfix label{padding-right: 6px!important; width: 250px;}
.ccm-ui label{display: inline-block!important;}
div.ccm-summary-selected-item{display: inline-block!important; width: 220px;}
</style>
<fieldset>
	<div class="clearfix">
		<div class="input" style="color: #ff6034; font-weight: bold;">
			<?php   echo t('<p>Any Question Type ordered BEFORE this Question may be referenced within this email by using that question\'s respective Question Handle placed between double square brackets:  [[question_handle]].</p>')?>
		</div>
	</div>
</fieldset>
<div class="clearfix">
		<?php    echo $fm->label('notify', t('Notify Specific Emails Instead'))?>
		<div class="input">
			<div class="input-prepend">
				<label>
				<span class="add-on" style="z-index: 2000">
					<?php    echo $fm->checkbox('notifyEmailsOnSubmission', 1, $notifyEmailsOnSubmission == 1, array('onclick' => "$('input[name=notify]').focus()"))?>
				</span><?php    echo $fm->text('notify', $notify, array('style' => 'z-index:2000;' ))?>
				</label>
			</div>

			<span class="help-block"><?php    echo t('Seperate multiple emails with a comma.')?></span>
			<span class="help-block"><?php    echo t('If not set, responder will search for an')?></span>
			<span class="help-block"><?php    echo t('"email" QuestionType within the submitted Form.')?></span>
		</div>
	</div>
<fieldset>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('Email From Address')?></label>
		<div class="input">
			<?php   echo $fm->text('from_email',$from_email)?>
		</div>
	</div>
</fieldset>

<fieldset>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('Email Subject')?></label>
		<div class="input">
			<?php   echo $fm->text('response_subject',$response_subject)?>
		</div>
	</div>
</fieldset>

<fieldset>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('HTML Email Message')?></label>
		<div class="input">
			<div style="text-align: center" id="ccm-editor-pane">
			  <?php      Loader::element('editor_init'); ?>
			  <?php      Loader::element('editor_config'); ?>
			  <?php      Loader::element('editor_controls', array('mode'=>'full')); ?>
			  <?php     
			  echo $fm->textarea('response_message', $response_message, array('style' => 'width: 85%; font-family: sans-serif;', 'class' => 'ccm-advanced-editor'))?>
			</div>
		</div>
	</div>
	<br style="clear:both;"/>
	<div style="float: right;">
		<a href="<?php   echo Loader::helper('concrete/urls')->getToolsURL('proforms/handle_list.php','proforms')?>" class="dialog-launch" dialog-width="380" dialog-height="340" dialog-modal="true" dialog-title="<?php   echo t('View Available Handles')?>" dialog-on-close="" onClick="javascript:;">&raquo; <?php   echo t('View Available Attribute Handles')?></a>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.dialog-launch').dialog();
	});
	</script>
</fieldset>