<?php     defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php   
$fm = Loader::helper('form');
?>
<div class="clearfix">
	<label><?php    echo t("Association Type")?></label>
	<div class="input">
		<?php    print $fm->select('association_type',array('automatic'=>'Auto','manual'=>'Manual'),$association_type);?>
	</div>
</div>