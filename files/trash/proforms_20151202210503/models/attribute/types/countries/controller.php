<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/types/default/controller');

class CountriesAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public $restrictDuplicates = true;

	public function form() {
		$at = $this->attributeType;
		$jspath = $at->getAttributeTypeFileURL('type_form.js');
		$path = $at->getAttributeTypeFileURL('js/jQuery.geoselector.js');
		$json_path = $at->getAttributeTypeFileURL('js/divisions.json');
		$json_path = '/packages/proforms/models/attribute/types/multiplex/js/divisions.json';
		
		$this->set('json_path',$json_path);
		$this->addFooterItem(Loader::helper('html')->javascript($path));
		$this->addFooterItem(Loader::helper('html')->javascript($jspath));
		
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
			$this->set('value',$value);
		}

	}
	
	public function searchForm($list){
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}
	

}