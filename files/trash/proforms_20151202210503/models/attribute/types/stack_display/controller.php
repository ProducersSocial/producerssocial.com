<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class StackDisplayAttributeTypeController extends AttributeTypeController  {

	protected $searchIndexFieldDefinition = 'I 11 DEFAULT 0 NULL';
	public $reviewStatus = 0; // no review needed
	public $hideLabel = 1; // no lable needed

	public function getValue() {
		$db = Loader::db();
		$value = $db->GetOne("select value from atStackSelector where avID = ?", array($this->getAttributeValueID()));
		return $value;	
	}
	
	public function searchForm($list) {
		$PagecID = $this->request('value');
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $PagecID, '=');
		return $list;
	}
	
	public function search() {
		$form_selector = Loader::helper('form/stack_selector','stack_selector_attribute');
		print $form_selector->select_stack($this->field('value'), $this->request('value'));
	}
	
	public function form() {
		$this->load();
		$stack = Stack::getByID($this->stack);
		if(is_object($stack)){
			$blocks = $stack->getBlocks();
			foreach($blocks as $block){
				$block->display();
			}
		}
		//var_dump($stack->getBlocks());
		//$stack->display($c);
	}
	
	public function type_form() {
		$this->load();
	}
	
	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atStackSelectorSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->stack = $row['stack'];

		$this->set('stack', $this->stack);
	}
	
	/*
	public function validateForm($p) {
		return $p['value'] != 0;
	}
	*/

	public function saveValue($value) {
		$db = Loader::db();
	}
	
	public function saveKey($data){
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'stack'=>$data['stack']
		);
		
		$db=Loader::db();
		$db->Replace('atStackSelectorSettings', $vals, 'akID', true);
	}
	
	public function deleteKey() {
		$db = Loader::db();
		$arr = $this->attributeKey->getAttributeValueIDList();
		foreach($arr as $id) {
			$db->Execute('delete from atStackSelector where avID = ?', array($id));
		}
	}
	
	public function saveForm($data) {
		$db = Loader::db();
		$this->saveValue($data['value']);
	}
	
	public function deleteValue() {
		$db = Loader::db();
		$db->Execute('delete from atStackSelector where avID = ?', array($this->getAttributeValueID()));
	}
	
}