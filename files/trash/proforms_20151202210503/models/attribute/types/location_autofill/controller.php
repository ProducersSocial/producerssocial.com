<?php    
defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('attribute/types/default/controller');

class LocationAutofillAttributeTypeController extends DefaultAttributeTypeController  {

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public $restrictDuplicates = true;

	public function form() {
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		$this->addFooterItem('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>');
		$this->addFooterItem('
			<script type="text/javascript">
				autocomplete = new google.maps.places.Autocomplete((document.getElementById(\'autocomplete_location\')),{ types:[\'geocode\'] });
			</script>');
		print '<input id="autocomplete_location" name="'.$this->field('value').'" placeholder="Enter a city" type="text" autocomplete="on" value="'.$value.'">';
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Autofill Location QuestionType allows users to type location info to be prompted with automatic google geo results popup for selected.</p><p>This input is simple and great for consistent location info without all the form fields!</p>
		');
	}
	
	public function composer() {
		if (is_object($this->attributeValue)) {
			$value = Loader::helper('text')->entities($this->getAttributeValue()->getValue());
		}
		print Loader::helper('form')->text($this->field('value'), $value, array('class' => 'span5'));
	}
	
	public function searchForm($list){
		$this->load();
		$value = $this->request('value');
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), "%$value%", 'LIKE');
		return $list;
	}

	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	public function validateForm($phone) {
		/*
		if(is_array($phone)){
			$phone = $phone['value'];
		}
		$this->load();
		if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $phone)) {
			 // $phone is valid
			 $this->error = t('Your phone number is not in the correct format!');
			 return false;
		}
		*/
		return true;
	}

}