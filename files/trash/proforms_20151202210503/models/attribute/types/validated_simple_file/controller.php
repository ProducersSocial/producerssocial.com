<?php   
defined('C5_EXECUTE') or die("Access Denied.");

class ValidatedSimpleFileAttributeTypeController extends AttributeTypeController {

	protected $searchIndexFieldDefinition = 'I DEFAULT 0 NULL';
	public $is_multipart = 1;
	
	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atSimpleValidatedFileSettings where akID = ?', $ak->getAttributeKeyID());
		}
	
		
		$this->akID = $row['akID'];
		$this->asID = $row['asID'];
		$this->size_limit = $row['size_limit'];
		$this->allowed_file_types = $row['allowed_file_types'];

		$this->set('akID', $this->akID);
		$this->set('asID', $this->asID);
		$this->set('size_limit', $this->size_limit);
		$this->set('allowed_file_types', $this->allowed_file_types);
	}

	public function getValue() {
		$dbs = Loader::db();
		$value = $dbs->GetOne("select fID from atSimpleValidatedFile where avID = ?", array($this->getAttributeValueID()));
		if ($value > 0) {
			$f = File::getByID($value);
			return $f;
		}
	}
	
	public function getPath($avID=null) {
		if($avID != null){
			$avID = $avID;
		}else{
			$avID = $this->getAttributeValueID();
		}
		$dbs = Loader::db();
		$pathy = $dbs->GetOne("select path from atSimpleValidatedFile where avID = ?", array($avID));
		return $pathy;
	}

	public function getDisplayValue() {
		$f = $this->getValue();
		$path = $this->getPath();
		if (is_object($f)) {
			return BASE_URL.$path;
		}
	}
	
	public function display() {
		$f = $this->getValue();
		$path = $this->getPath();
		if (is_object($f)) {
			print '<a href="' . $path . '">' . $f->getFileName() . '</a>';
		}
	}

	public function searchForm($list) {
		$data = $this->request();
		$fileID = $data['value'];
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $fileID);
		return $list;
	}

	public function getSearchIndexValue() {
		$dbs = Loader::db();
		$value = $dbs->GetOne("select fID from atSimpleValidatedFile where avID = ?", array($this->getAttributeValueID()));
		return $value;
	}

	public function search() {
		// search by file causes too many problems
		//$al = Loader::helper('concrete/asset_library');
		//print $al->file('ccm-file-akID-' . $this->attributeKey->getAttributeKeyID(), $this->field('value'), t('Choose File'), $bf);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Validated Simple File Question Type allows the user to upload a file without the need to use the C5 filemanager. </p><p>This Question Type will also validate file type and size as needed.</p><p>Each file type should be listed in javascript formatting. ex - image/jpg,image/png,video/mpg</p>
		');
	}
	
	public function type_form(){
		$this->load();
		Loader::model('file_set');
		$this->set('fileSets', FileSet::getMySets());
	}

	public function form() {
		$this->load();
		$bf = false;
		if ($this->getAttributeValueID() > 0) {
			$bf = $this->getValue();
			$img = Loader::helper('image');
			$thumb = $img->getThumbnail($bf, 150, 50);
			$path = $this->getPath();
		}
		$form = Loader::helper('form');
		
		$allowed_file_types = explode("\n",$this->allowed_file_types);
		
		foreach($allowed_file_types as $ft){
			if($types_string){$types_string .= ',';}
			$types_string .= "'".trim($ft)."'";
		}

		print '<div class="ccm-ui">';
		print '	<div class="alert-info block-message alert">';
		print '	  <a class="close" href="javascript:(0);">×</a>';
		print '	  <p><strong>'.t('Logo Requirements').'</strong></p>';
		print '	  <p>'.t('Your Image must be larger than '.($this->size_limit / 1000).'mb, and be one of the following filetypes: ').$types_string.'</p>';

		if ($bf) {
			print '<img src="'.$bf->getDownloadURL().'" alt="company_logo" /><br/>';
			print '<a href="'.$bf->getDownloadURL().'" target="_blank">'.$bf->getFileName().'</a> &nbsp;&nbsp;';
			print '<br /><br />';
			print t('Or upload a new file');
			print '<br /><br />';
			print $form->file($this->field('new_file'));
			$key = $this->field('existing_value');
			print '<input type="hidden" name="' . $key . '" id="' . $key . '" value="' . $bf->getFileID() . '" />';
			
		} else {
			print t('Upload a file');
			print '<br />';
			print $form->file($this->field('new_file'));
			print $form->hidden($this->field('existing_value'), 0);
		}
		print $form->hidden($this->field('ajax_send'), 0);
		print '	</div>';
		print '</div>';
		print '
<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){
		$("#akID\\\['.$this->attributeKey->getAttributeKeyID().'\\\]\\\[new_file\\\]").bind( \'change\', function(){
		    var file = this.files[0];
		    name = file.name;
		    size = file.size;
		    type = file.type;
		    
		    //your validation
		    console.log(type);
		    var allowed_types = new Array('.$types_string.');
		    if($.inArray(type,allowed_types) == -1){
		    	alert(\'You have uploaded an invalid file type!\');
		    	$(\'#save\').hide();
		    	return false;
		    }else{
		    	$(\'#save\').show();
		    }
		    
		    //console.log(size);
		    //console.log('.($this->size_limit * 1000).');
		    if(size < '.($this->size_limit * 1000).'){
		    	alert(\'Your file must be greater than '.$this->size_limit.'kb in size!\');
		    	$(\'#save\').hide();
		    	return false;
		    }else{
		    	$(\'#save\').show();
		    }
		    
		    $("#akID\\\['.$this->attributeKey->getAttributeKeyID().'\\\]\\\[ajax_send\\\]").val(\'file_selected\');
		});
	});
	/*]]>*/
</script>';
	}

	// run when we call setAttribute(), instead of saving through the UI
	public function saveValue($obj) {
		$this->load();
		if(is_object($obj)){
			$path = BASE_URL.DIR_REL.$obj->getRelativePath();
			$fID = $obj->getFileID();
		}else{
			$fID = null;
			$path = null;
		}
		$path = $obj->getRelativePath();
		$dbs = Loader::db();
		$dbs->Replace('atSimpleValidatedFile', array('avID' => $this->getAttributeValueID(), 'fID' => $fID,'path'=>$path), 'avID', true);
	}

	public function saveForm($data) {
		
		if ($data['value'] > 0) {
			$f = File::getByID($data['value']);
			$this->saveValue($f);
		} else {
			$dbs = Loader::db();
			$dbs->Replace('atSimpleValidatedFile', array('avID' => $this->getAttributeValueID(), 'fID' => 0), 'avID', true);
		}
	}

	public function deleteValue() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atSimpleValidatedFile where avID = ?', array($this->getAttributeValueID()));
	}

	public function post($field = false) {
		// the only post that matters is the one for this attribute's name space
		$this->load();
		$files = $_FILES;
		$post = $_POST;
		if (is_object($this->attributeKey) && strlen($files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'])) {
			// we have uploaded a new file
			$existingFileID = $post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
			$f = File::getByID($existingFileID);
			$filename = $files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			$path = $files['akID']['tmp_name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			if ($existingFileID && (intval($existingFileID) > 0)) {
				// replace the file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename, $f);
			} else {
				// creating a new file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename);
			}
			
			if($this->asID){
				$fs = FileSet::getByID($this->asID);
				$fs->addFileToSet($newFile);
			}
			$data = array();
			$data['value'] = $newFile->getFileID();
			return $data;
		} else {
			if ($post['akID'][$this->attributeKey->getAttributeKeyID()]['clear_image'] ){
				// clear out any existing file in the file manager
				if ($post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']){
					$f = File::getByID($post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']);
					$f->delete();
				}
				// return an empty array to set the fID to 0
				$data = array();
				return $data;
			} else {
				// we aren't changing anything and have an existing file
				$data = array();
				$data['value'] = $post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
				return $data;
			}
		}
	}

	public function request($field = false) {
		// the only post that matters is the one for this attribute's name space
		$files = $_FILES;
		$request = $_REQUEST;
		if (is_object($this->attributeKey) && strlen($files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'])) {
			// we have uploaded a new file
			$existingFileID = $request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
			$f = File::getByID($existingFileID);
			$filename = $files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			$path = $files['akID']['tmp_name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			if ($existingFileID) {
				// replace the file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename, $f);
			} else {
				// creating a new file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename);
			}
			if($this->asID){
				$fs = FileSet::getByID($this->asID);
				$fs->addFileToSet($newFile);
			}
			$data = array();
			$data['value'] = $newFile->getFileID();
			return $data;
		} else {
			if ($request['akID'][$this->attributeKey->getAttributeKeyID()]['clear_image'] ){
				// clear out any existing file in the file manager
				if ($request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']){
					$f = File::getByID($request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']);
					$f->delete();
				}
				// return an empty array to set the fID to 0
				$data = array();
				return $data;
			} else {
				// we aren't changing anything and have an existing file
				$data = array();
				$data['value'] = $request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
				return $data;
			}
		}
	}

	public function validateForm($data){
		$newData = $this->request();
		if ($newData['value'] > 0){
			return true;
		} else {
			 $this->error = t('You are missing a required file!');
			return false;
		}
	}
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'asID'=>$data['fasID'],
			'size_limit'=>$data['size_limit'],
			'allowed_file_types'=> $data['allowed_file_types']
		);
		
		$db=Loader::db();
		$db->Replace('atSimpleValidatedFileSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atSimpleValidatedFileSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}