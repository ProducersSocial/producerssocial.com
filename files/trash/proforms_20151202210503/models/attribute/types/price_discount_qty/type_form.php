<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
function getAttributeOptionHTML($v){ 
	if ($v == 'TEMPLATE') {
		$akPriceDiscountQtyID = 'TEMPLATE_CLEAN';
		$akPriceDiscountQty = 'TEMPLATE';
		$akPriceDiscountQtyOption = 'TEMP_OPTION';
	} else {
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceDiscountQtyID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceDiscountQtyID = $v->getSelectAttributeOptionID();
		}
		$akPriceDiscountQty = $v->getSelectAttributeOptionValue();
		$akPriceDiscountQtyOption = $v->getSelectAttributeOptionValueOption();
	}
		?>
		<div id="akPriceDiscountQtyDisplay_<?php    echo $akPriceDiscountQtyID?>" >
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountQtyID)?>')" value="<?php    echo t('Edit')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.deleteValue('<?php    echo addslashes($akPriceDiscountQtyID)?>')" value="<?php    echo t('Delete')?>" />
			</div>			
			<span onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountQtyID)?>')" id="akPriceDiscountQtyStatic_<?php    echo $akPriceDiscountQtyID?>" class="leftCol"><?php    echo $akPriceDiscountQty ?> <i>(<?php    echo $akPriceDiscountQtyOption ?>)</i></span>
		</div>
		<div id="akPriceDiscountQtyEdit_<?php    echo $akPriceDiscountQtyID?>" style="display:none">
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceDiscountQtyID)?>')" value="<?php    echo t('Cancel')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.changeValue('<?php    echo addslashes($akPriceDiscountQtyID)?>')" value="<?php    echo t('Save')?>" />
			</div>		
			<span class="leftCol">
				<input name="akPriceDiscountQtyOriginal_<?php    echo $akPriceDiscountQtyID?>" type="hidden" value="<?php    echo $akPriceDiscountQty?>" />
				<?php     if (is_object($v) && $v->getSelectAttributeOptionTemporaryID() == false) { ?>
					<input id="akPriceDiscountQtyExistingOption_<?php    echo $akPriceDiscountQtyID?>" name="akPriceDiscountQtyExistingOption_<?php    echo $akPriceDiscountQtyID?>" type="hidden" value="<?php    echo $akPriceDiscountQtyID?>" />
				<?php     } else { ?>
					<input id="akPriceDiscountQtyNewOption_<?php    echo $akPriceDiscountQtyID?>" name="akPriceDiscountQtyNewOption_<?php    echo $akPriceDiscountQtyID?>" type="hidden" value="<?php    echo $akPriceDiscountQtyID?>" />
				<?php     } ?>
				<input id="akPriceDiscountQtyField_<?php    echo $akPriceDiscountQtyID?>" name="akPriceDiscountQty_<?php    echo $akPriceDiscountQtyID?>" type="text" value="<?php    echo $akPriceDiscountQty?>" size="20" />
				  
				<input id="akPriceDiscountQtyOptionField_<?php    echo $akPriceDiscountQtyID?>" name="akPriceDiscountQtyOption_<?php    echo $akPriceDiscountQtyID?>" type="text" value="<?php    echo $akPriceDiscountQtyOption?>" size="20" />
			</span>		
		</div>	
		<div class="ccm-spacer">&nbsp;</div>
<?php     } ?>

<fieldset>
<legend><?php    echo t('Select Options')?></legend>

<div class="clearfix">
<label><?php    echo t('Values')?></label>
<div class="input">
	<div id="attributeValuesInterface">
	<div id="attributeValuesWrap">
	<?php    
	Loader::helper('text');
	foreach($akPriceDiscountQty as $v) { 
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceDiscountQtyID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceDiscountQtyID = $v->getSelectAttributeOptionID();
		}
		?>
		<div id="akPriceDiscountQtyWrap_<?php    echo $akPriceDiscountQtyID?>" class="akPriceDiscountQtyWrap <?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?> akPriceDiscountQtyWrapSortable <?php     } ?>">
			<?php    echo getAttributeOptionHTML( $v )?>
		</div>
	<?php     } ?>
	</div>
	
	<div id="akPriceDiscountQtyWrapTemplate" class="akPriceDiscountQtyWrap" style="display:none">
		<?php    echo getAttributeOptionHTML('TEMPLATE') ?>
	</div>
	
	<div id="addAttributeValueWrap"> 
		<input id="akPriceDiscountQtyFieldNew" name="akPriceDiscountQtyNew" type="text" value="<?php    echo $defaultNewOptionNm ?>" size="40" class="faint input-medium" 
		onfocus="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',0)" 
		onblur="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',1)"
		onkeypress="ccmAttributesHelper.addEnterClick(event,function(){ccmAttributesHelper.saveNewOption()})"
		placeholder="<?php  echo t('% off (no % sign)')?>" /> 
		 <input id="akPriceDiscountQtyOptionFieldNew" name="akPriceDiscountQtyOptionNew" type="text" value="" placeholder="<?php  echo t('Qty')?>" class="input-small"/>
		<input class="btn" type="button" onClick="ccmAttributesHelper.saveNewOption(); $('#ccm-attribute-key-form').unbind()" value="<?php    echo t('Add') ?>" />
	</div>
	</div>

</div>
</div>


</fieldset>
<?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?>
<script type="text/javascript">
//<![CDATA[
$(function() {
	ccmAttributesHelper.makeSortable();
});
//]]>
</script>
<?php     } ?>