<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PaymentWepayAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
	}
	
	public function display_total(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="client_id" class="control-label">'.t('Wepay ID').'</label>';
		print '		<div class="input">';
		print $fm->text('client_id',$this->client_id,array('placeholder'=>'123456'));
		print '		</div>';
		print '	</div>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="client_secret" class="control-label">'.t('Wepay Secret').'</label>';
		print '		<div class="input">';
		print $fm->text('client_secret',$this->client_secret,array('placeholder'=>'secret key'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="wepay_price_handle" class="control-label">'.t('Wepay Token').'</label>';
		print '		<div class="input">';
		print $fm->text('access_token',$this->access_token,array('placeholder'=>'access token'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="account_id" class="control-label">'.t('Wepay Account ID#').'</label>';
		print '		<div class="input">';
		print $fm->text('account_id',$this->account_id,array('placeholder'=>'account ID'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="wepay_price_handle" class="control-label">'.t('Total Handle').'</label>';
		print '		<div class="input">';
		print $fm->text('wepay_price_handle',$this->wepay_price_handle,array('placeholder'=>'total_question_handle'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="wepay_return_page" class="control-label">'.t('Return Page').'</label>';
		print '		<div class="input">';
		print Loader::helper('form/page_selector')->selectPage('wepay_return_page',$this->wepay_return_page);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPaymentWepaySettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->client_id = $row['client_id'];
		$this->client_secret = $row['client_secret'];
		$this->access_token = $row['access_token'];
		$this->account_id = $row['account_id'];
		$this->wepay_price_handle = $row['wepay_price_handle'];
		$this->wepay_return_page = $row['wepay_return_page'];

		$this->set('akID', $this->akID);
		$this->set('client_id', $this->client_id);
		$this->set('client_secret', $this->client_secret);
		$this->set('access_token', $this->access_token);
		$this->set('account_id', $this->account_id);
		$this->set('wepay_price_handle', $this->wepay_price_handle);
		$this->set('wepay_return_page', $this->wepay_return_page);
	}
	
	public function getValue(){
		return $this->hidden_value;
	}

	
	public function saveForm($value) {
		$this->load();
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Wepay Payment QuestionType allows you to accept credit cards using a Wepay application API.</p>
		');
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'client_id'=>$data['client_id'],
			'client_secret'=>$data['client_secret'],
			'access_token'=>$data['access_token'],
			'account_id'=>$data['account_id'],
			'wepay_price_handle'=>$data['wepay_price_handle'],
			'wepay_return_page'=>$data['wepay_return_page']
		);
		
		$db=Loader::db();
		$db->Replace('atPaymentWepaySettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPaymentWepaySettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}