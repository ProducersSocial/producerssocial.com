function updatePricing(){
	var total = 0;
	
	$('select.price_option').each(function(){
		total = total + parseFloat($(this).find('option:selected').attr('data-value'));
	});
	
	$('input[type="radio"].price_option_time').each(function(){
		if($(this).is(':checked')){
			total = total + parseFloat($(this).attr('data-value'));
		}
	});
	
	$('.price_value input[type="hidden"]').each(function(){
		total = total + parseFloat($(this).val());
	});
	
	$('div[class*=price_shipping] input[type="hidden"]').each(function(){
		total = total + (total * parseFloat($(this).val()) / 100.0);
	});

	$('.price_value_enter input[type="text"]').each(function(){
		total = total + parseFloat($(this).val());
	});
	
	$('.price_event input[type="text"]').each(function(){
		if($.isNumeric($(this).val())){
			total = total + parseFloat($(this).val());
		}
		// check if datePicker value
		if(typeof $(this).attr('date-data-format') != 'undefined'){
			total = total + parseFloat($(this).attr('data-price'));
		}
	});
	
	$('.price_event input[type="hidden"]').each(function(){
		if($.isNumeric($(this).attr('data-price'))){
			total = total + parseFloat($(this).attr('data-price'));
		}
	});
	
	var break_dicount = 0;
	
	$('.price_discount_break').each(function(){
		var type = $(this).attr('data-break-type');
		var dbreak = $(this).attr('data-break');
		if(type == 'qty'){
			var qty = parseFloat($('.price_qty input').val());
			if(qty >= dbreak){
				break_dicount = (total * parseFloat($(this).val()) / 100.0);
			}
		}else if(type == 'event'){
			var d1 = new Date($('#dpd1').val()).getTime();
			var d2 = new Date($('#dpd2').val()).getTime();
			var eQty = ((d2 - d1) / 86400000);
			if(eQty >= dbreak){
				break_dicount = (total * parseFloat($(this).val()) / 100.0);
			}
		}
		
		if(break_dicount > 0){
			total = total - break_dicount;
		}
		
		if(total < 0){
			total = 0;
		}
	});
	
	$('.price_discount_event_code').each(function(){
		var discount_type = $(this).attr('data-type');
		if(parseFloat($(this).val()) <= total){
			if(discount_type == 'percent'){
				var event_discount = (total * parseFloat($(this).val()) / 100.0);
			}else{
				var event_discount = parseFloat($(this).val());
			}
			if(event_discount > 0){
				total = total - event_discount;
			}
			if(total < 0){
				total = 0;
			}
		}
	});
	
	$('.price_discount_event_value').each(function(){
		var discount_type = $(this).attr('data-type');
		if(discount_type == 'percent'){
			var event_discount = (total * parseFloat($(this).val()) / 100.0);
		}else{
			var event_discount = parseFloat($(this).val());
		}
		if(event_discount > 0){
			total = total - event_discount;
		}
		if(total < 0){
			total = 0;
		}
	});
	
	if($('#price_discount').val()){
		var discount_check = CCM_BASE_URL + CCM_REL + '/tools/packages/proforms/proforms/attributes/price_discount/discount_check.php';
		var discount_val = $('#price_discount').val();
		var akID = $('#price_discount').attr('data-at');
		$.ajax({
			url: discount_check,
			type: "POST",
			data:{
				code: discount_val,
				akID: akID
			},
			success: function(discount){
				if(discount[discount.length - 1] === '%') {
					discount = (discount.replace('%','')) * 1;
					var percent = discount / 100.0;
					discount = (total * percent);
					total = total - discount;
				}else{
					discount = discount * 1;
					total = total - discount;
				}
				doMath(total);
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  //console.log(err.Message);
			}
		});
		
		if(total < 0){
			total = 0;
		}
	}
	
    if($('.payment_amount')){
        var due = $('#paid_total .value').text();
        var id = $('#ProformsItemID').val();
        if(due.indexOf(')') >= 0){
            $('.proforms_submit').replaceWith(
                '<a href="/index.php/tools/packages/proforms/proforms/request_refund.php?ProformsItemID='+id+'&rcID='+CCM_CID+'" id="refund" dialog-width="250" dialog-height="300" dialog-modal="true" dialog-title="Refund Request" dialog-on-close="" class="btn info pull-right dialog-launch">Request Refund</a>'
            );
            $('#refund').dialog();
            
        }
	}
	
	doMath(total);
	//#form_total span
}

function doMath(total){
	
	if($('#form_tax').attr('data-value') > 0){
		var percent = parseFloat($('#form_tax').attr('data-value')) / 100.0;
		var qty = 0;
		$('.price_qty input').each(function(){
			qty += parseFloat($(this).val());
		});
		if(qty==0){qty = 1;}
		$('#event_qty').val(qty);
		total = total * qty;
		var tax = (total * percent);
		total = total + tax;
		$('#form_tax span.value').text(tax.toFixed(2));
		$('#form_tax input[type="hidden"]').val(tax.toFixed(2));
	}else{
		var qty = 0;
		$('.price_qty input').each(function(){
			qty += parseFloat($(this).val());
		});
		if(qty==0){qty = 1;}
		$('#event_qty').val(qty);
		total = total * qty;
		//console.log(total);
	}
	
	$('#form_total .value').text(total.toFixed(2));
	$('#form_total input[type="hidden"]').val(total.toFixed(2));	
}

$(document).ready(function(){
	$('.currency_symbol').html($('#currency_symbol_master').html());
	
	$('.price_option').change(function(){
		updatePricing();
	});
	
	$('.apply_update').click(function(e){
		e.preventDefault();
		updatePricing();
	});
	
	
	updatePricing();
});