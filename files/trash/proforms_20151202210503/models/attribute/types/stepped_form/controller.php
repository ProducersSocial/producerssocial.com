<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class SteppedFormAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $show_submit;
	public $show_previous;
	public $type;
	public $state;
	public $show_progress;
	public $progress_style;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	
	public function form(){
		$this->load();
	}
	
	public function closer(){
		$this->load();
		$this->set('step_title',$this->attributeKey->getAttributeKeyName());
		$at = $this->attributeType;
		$formjs = $at->getAttributeTypeFileURL('js/stepped_form.js');
		$this->addFooterItem(Loader::helper('html')->javascript($formjs));
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print $fm->hidden('force_close',1);
		print $fm->hidden('state','begin');
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atSteppedFormSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->show_submit = $row['show_submit'];
		$this->show_previous = $row['show_previous'];
		$this->type = $row['type'];
		$this->state = $row['state'];
		$this->show_progress = $row['show_progress'];
		$this->progress_style = $row['progress_style'];

		$this->set('akID', $this->akID);
		$this->set('show_submit', $this->show_submit);
		$this->set('show_previous', $this->show_previous);
		$this->set('type', $this->type);
		$this->set('state', $this->state);
		$this->set('show_progress', $this->show_progress);
		$this->set('progress_style', $this->progress_style);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Stepped Form Question Type allows you to have a multi-step form.</p><p>The question will "wrap" a div around any group of questions. When added, this attribute will add <u>two</u> Questions.  An opening and a closing question.  Simply drag the two around any set of questions to wrap them.</p> 
		<br /> 
		<h4>Advanced Use</h4>
		<p>Because this Question Type is quite javascript heavy, it is <u>IMPERATIVE</u> that all questions within the form be contained within the stepped form containers once a stepped form has been added.</p><br/>
		<p>This Question Type uses the "$reviewStatus=0;" and will never be printed out for Form review.  It is merely a front-end esthetic container.</p>
		');
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atRegexMatch WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atRegexMatch where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atRegexMatch (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$value['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atRegexMatch where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {

		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'show_submit'=>$data['show_submit'],
			'show_previous'=>$data['show_previous'],
			'type'=> $data['type'],
			'state' => (substr($this->attributeKey->getAttributeKeyHandle(),-20) != 'close_step_container') ? 'begin' : 'end',
			'show_progress'=>$data['show_progress'],
			'progress_style'=>$data['progress_style']
		);
		
		$db=Loader::db();
		$db->Replace('atSteppedFormSettings', $vals, 'akID', true);
		
		if($data['force_close'] == 1 && substr($this->attributeKey->getAttributeKeyHandle(),-20) != 'close_step_container'){
			$sets = $this->attributeKey->getAttributeSets();
			foreach($sets as $set){
				$asIDs[] = $set->getAttributeSetID();
			}
			$at = AttributeType::getByHandle('stepped_form');
			$closer = ProformsItemAttributeKey::getByHandle($this->attributeKey->getAttributeKeyHandle().'_close_step_container'); 
			if(!is_object($closer) || !intval($closer->getAttributeKeyID())){
				$data['hide_label'] = 1;
				$data['akHandle'] = $this->attributeKey->getAttributeKeyHandle().'_close_step_container';
				$data['akName'] = $this->attributeKey->getAttributeKeyName().t(' (close step container)');
				$data['state'] = 'end';
				$data['force_close'] = 0;
				$closer = ProformsItemAttributeKey::add($at,$data);
				foreach($sets as $set){
					$closer->setAttributeSet($set); 
				}
			}
		}
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atRegexMatchSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}