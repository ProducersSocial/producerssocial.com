<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
?>
<!--
<div class="clearfix">
	<label><?php    echo t("Show Submit Button")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="show_submit" value="1" <?php    if($show_submit){echo 'checked';}?>> <?php   echo t(' Yes, show submit button on this slide')?><br/>
		  <i><?php   echo t('(button will otherwise read "Next Step")')?></i>
		  
		</label>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Show Previous Button")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="show_previous" value="1" <?php    if($show_previous){echo 'checked';}?>> <?php   echo t(' Yes, show previous button on this slide')?>
		  
		</label>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Step Type")?></label>
	<div class="input">
		<select name="type">
			<option value="slide_left" <?php    if($type=='slide_left'){echo 'selected';}?>><?php   echo t('Slide Left')?></option>
			<option value="slide_up" <?php    if($type=='slide_up'){echo 'selected';}?>><?php   echo t('Slide Up')?></option>
			<option value="fade" <?php    if($type=='fade'){echo 'selected';}?>><?php   echo t('Fade')?></option>
			<option value="random" <?php    if($type=='random'){echo 'selected';}?>><?php   echo t('Random')?></option>
		</select>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Show Progress Bar")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="show_progress" value="1" <?php    if($show_progress){echo 'checked';}?>> <?php   echo t(' Yes, show progress bar')?>
		</label>
	</div>
</div>

<div class="clearfix">
	<label><?php    echo t("Progress Type")?></label>
	<div class="input">
		<select name="progress_style">
			<option value="flag" <?php    if($type=='flag'){echo 'selected';}?>><?php   echo t('Flag')?></option>
			<option value="numbers" <?php    if($type=='numbers'){echo 'selected';}?>><?php   echo t('Numbers')?></option>
			<option value="bar" <?php    if($type=='bar'){echo 'selected';}?>><?php   echo t('Bar')?></option>
			<option value="arrow" <?php    if($type=='arrow'){echo 'selected';}?>><?php   echo t('Arrows')?></option>
		</select>
	</div>
</div>
-->