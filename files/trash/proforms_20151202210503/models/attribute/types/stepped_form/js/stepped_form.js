$(document).ready(function(){
	var t = 0;
	var i = 0;
	$('.form_step').each(function(){
		t +=1;
		if(!$('.bar_'+t).attr('rel')){
			var name = $(this).attr('data-name');
			$('#form_progress').append('<div class="bar_item bar_'+t+'" rel="'+t+'"><span class="number">'+t+'</span><div class="step_name">'+name+'</div></div>');
		}
		if(!$(this).attr('alt')){
			i+=1;
			$(this).attr('alt',i);
			$(this).addClass('stepform_'+i);
			if(i==1){
				$(this).show();
				$('.bar_1').addClass('current_slide');
				$('.proform_slider').css('height',($(this).height() + 35));
				$('.form_step').css('width',$('.proform_slider').css('width'));
			}
		}
	});

	$('.submitit').html( $('.proforms_submit_group').html() );
	if(t > 1){
		$('.proforms_submit_group').hide();
	}
	var bID = $('input[name="bID"]').val();

	innitializeForm(bID,data);
	
	$('.bar_item').bind('click tap',function(){
		var current = false;
		var goto_slide = $(this).attr('rel');
		$('.bar_item').each(function(){
			if($(this).hasClass('current_slide')){
				current = true;
			}
			if($(this).attr('rel') == goto_slide && !current){
				var prev_slide = goto_slide;
				var current_slide = $('.current_slide').attr('rel');
				var o_height = $('.stepform_'+current_slide).height();
				var n_height = $('.stepform_'+prev_slide).height();
				var prev = $('.stepform_'+prev_slide);
		
				$('.bar_item').removeClass('current_slide');
				$('.bar_'+prev_slide).addClass('current_slide');
				$('.submitit').hide();
		
				if(o_height > n_height){
					$('.stepform_'+current_slide).hide('slide', {direction: 'right'}, 400);
					prev.show('slide', {
						direction: 'left',
						complete: function(){
							$('.proform_slider').css('height',(n_height + 35));
							$('html, body').animate({
						         scrollTop: $("#form_title").offset().top
						     }, 500);
						}
					}, 400);
				}else{
					$('.stepform_'+current_slide).hide('slide', {direction: 'right'}, 400);
					$('.proform_slider').css('height',(n_height + 35));
					prev.show('slide', {
						direction: 'left',
						complete: function(){
							$('html, body').animate({
						         scrollTop: $("#form_title").offset().top
						     }, 500);
						}
					}, 400);
				}
		
				if(prev_slide == 1){
					$('.prev_step').hide();
					$('.next_step').show();
				}else{
					$('.next_step').show();
				}
			}else if($(this).attr('rel') == goto_slide){
				var next_slide = goto_slide;
		
				var current_slide = $('.current_slide').attr('rel');
				var o_height = $('.stepform_'+current_slide).height();
				var n_height = $('.stepform_'+next_slide).height();
				var next = $('.stepform_'+next_slide);
				
				$('.bar_item').removeClass('current_slide');
				$('.bar_'+next_slide).addClass('current_slide');
		
				if(o_height > n_height){
					$('.stepform_'+current_slide).hide('slide', {direction: 'left'}, 400);
					next.show('slide', {
						direction: 'right',
						complete: function(){
							$('.proform_slider').css('height',(n_height + 35));
							$('html, body').animate({
						         scrollTop: $("#form_title").offset().top
						     }, 500);
						}
					}, 400);
				}else{
					$('.stepform_'+current_slide).hide('slide', {direction: 'left'}, 400);
					$('.proform_slider').css('height',(n_height + 35));
					next.show('slide', {
						direction: 'right',
						complete: function(){
							$('html, body').animate({
						         scrollTop: $("#form_title").offset().top
						     }, 500);
						}
					}, 400);
				}
		
				if($('.form_step').length == next_slide){
					$('.next_step').hide();
					$('.submitit').show();
				}
				if($('.form_step').length > 1){
					$('.prev_step').show();
				}
			}
		});
	});

	$('a.next_step').bind('click tap',function(e){

		var next_slide = ($(this).parents('.form_step').attr('alt')*1) + 1;

		var o_height = $(this).parents('.form_step').height();
		var n_height = $('.stepform_'+next_slide).height();
		var next = $('.stepform_'+next_slide);
		
		$('.bar_item').removeClass('current_slide');
		$('.bar_'+next_slide).addClass('current_slide');

		if(o_height > n_height){
			$(this).parents('.form_step').hide('slide', {direction: 'left'}, 400);
			next.show('slide', {
				direction: 'right',
				complete: function(){
					$('.proform_slider').css('height',(n_height + 35));
					$('html, body').animate({
				         scrollTop: $("#form_title").offset().top
				     }, 500);
				}
			}, 400);
		}else{
			$(this).parents('.form_step').hide('slide', {direction: 'left'}, 400);
			$('.proform_slider').css('height',(n_height + 35));
			next.show('slide', {
				direction: 'right',
				complete: function(){
					$('html, body').animate({
				         scrollTop: $("#form_title").offset().top
				     }, 500);
				}
			}, 400);
		}

		if($('.form_step').length == next_slide){
			$('.next_step').hide();
			$('.submitit').show();
		}
		if($('.form_step').length > 1){
			$('.prev_step').show();
		}

	});

	$('a.prev_step').bind('click tap',function(e){
		var prev_slide = ($(this).parents('.form_step').attr('alt')*1) - 1;

		var o_height = $(this).parents('.form_step').height();
		var n_height = $('.stepform_'+prev_slide).height();
		var prev = $('.stepform_'+prev_slide);

		$('.bar_item').removeClass('current_slide');
		$('.bar_'+prev_slide).addClass('current_slide');
		$('.submitit').hide();

		if(o_height > n_height){
			$(this).parents('.form_step').hide('slide', {direction: 'right'}, 400);
			prev.show('slide', {
				direction: 'left',
				complete: function(){
					$('.proform_slider').css('height',(n_height + 35));
					$('html, body').animate({
				         scrollTop: $("#form_title").offset().top
				     }, 500);
				}
			}, 400);
		}else{
			$(this).parents('.form_step').hide('slide', {direction: 'right'}, 400);
			$('.proform_slider').css('height',(n_height + 35));
			prev.show('slide', {
				direction: 'left',
				complete: function(){
					$('html, body').animate({
				         scrollTop: $("#form_title").offset().top
				     }, 500);
				}
			}, 400);
		}

		if(prev_slide == 1){
			$('.prev_step').hide();
			$('.next_step').show();
		}else{
			$('.next_step').show();
		}

	});
});