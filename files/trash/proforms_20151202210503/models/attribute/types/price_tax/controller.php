<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PriceTaxAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$tax = $this->getValue();
		if(!$tax){$tax = 0.00;}
		$tax = money_format('%(#4n',$tax);
		$fm = Loader::helper('form');
		echo '<div id="form_tax" data-value="'.$this->percent.'"><span class="value">'.$tax.'</span>'.$fm->hidden($this->field('value'),$tax,array('class'=>'form_tax_value')).'</div>';
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Percent').'</label>';
		print '		<div class="input">';
		print $fm->text('percent',$this->percent,array('placeholder'=>t('(no % symbol)')));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $this->request('value'), '=');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->text($this->field('value'), $this->request('value'));
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPriceTaxSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->percent = $row['percent'];

		$this->set('akID', $this->akID);
		$this->set('percent', $this->percent);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT tax FROM atPriceTax WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atPriceTax where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atPriceTax (avID,tax) values (?,?)', array($this->getAttributeValueID(),$value['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atPriceTax where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'percent'=>$data['percent'],
		);
		
		$db=Loader::db();
		$db->Replace('atPriceTaxSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceTaxSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}