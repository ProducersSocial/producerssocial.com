<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class OptionalExpandAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $type;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	
	public function form(){
		$this->load();
	}
	
	public function closer(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		$options = array('optionset_begin'=>t('Optional Expand Open'),'optionset_end'=>t('Optional Expand End'));
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Container State').'</label>';
		print '		<div class="input">';
		print $fm->select('type',$options,$this->type);
		print $fm->hidden('force_close',1);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atOptionalExpandSettings where akID = ?', $ak->getAttributeKeyID());
		}
		
		$this->akID = $row['akID'];
		$this->type = $row['type'];

		$this->set('akID', $this->akID);
		$this->set('type', $this->type);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atOptionalExpand WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Optional Expand Question Type is intended to clean up or "shorten" forms that have a need for large groupings of questions as optional inputs.</p>
		<br/>
		<p>To accomplish this task, all select options are "Wrapped" in a hidden container.  When the user checks "show optional questions", a list of questions will display.</p>
		<br /> 
		<h4>Advanced Use</h4>
		<p>This Question Type uses the "$reviewStatus=0;" and will never be printed out for Form review.  It is merely a front-end esthetic container.</p>
		');
	}
	
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atOptionalExpand where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atOptionalExpand (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$value));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atOptionalExpand where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'type'=>$data['type']
		);
		$db=Loader::db();
		$db->Replace('atOptionalExpandSettings', $vals, 'akID', true);
		if($data['force_close'] == 1 && substr($this->attributeKey->getAttributeKeyHandle(),-6) != '_close'){
			$sets = $this->attributeKey->getAttributeSets();
			foreach($sets as $set){
				$asIDs[] = $set->getAttributeSetID();
			}
			$at = AttributeType::getByHandle('optional_expand');

			$closer = ProformsItemAttributeKey::getByHandle($this->attributeKey->getAttributeKeyHandle().'_close'); 
			if(!is_object($closer) || !intval($closer->getAttributeKeyID())){

				$data['akHandle'] = $this->attributeKey->getAttributeKeyHandle().'_close';
				$data['akName'] = $this->attributeKey->getAttributeKeyName().t(' (close container)');
				$data['type'] = 'optionset_end';
				$data['force_close'] = 0;

				$closer = ProformsItemAttributeKey::add($at,$data);
				foreach($sets as $set){
					$closer->setAttributeSet($set); 
				}
			}
		}
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atOptionalExpandSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}