<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
function getAttributeOptionHTML($v){ 
	if ($v == 'TEMPLATE') {
		$akPriceSelectID = 'TEMPLATE_CLEAN';
		$akPriceSelect = 'TEMPLATE';
		$akPriceSelectOption = 'TEMP_OPTION';
	} else {
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceSelectID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceSelectID = $v->getSelectAttributeOptionID();
		}
		$akPriceSelect = $v->getSelectAttributeOptionValue();
		$akPriceSelectOption = $v->getSelectAttributeOptionValueOption();
	}
		?>
		<div id="akPriceSelectDisplay_<?php    echo $akPriceSelectID?>" >
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceSelectID)?>')" value="<?php    echo t('Edit')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.deleteValue('<?php    echo addslashes($akPriceSelectID)?>')" value="<?php    echo t('Delete')?>" />
			</div>			
			<span onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceSelectID)?>')" id="akPriceSelectStatic_<?php    echo $akPriceSelectID?>" class="leftCol"><?php    echo $akPriceSelect ?> <i>(<?php    echo $akPriceSelectOption ?>)</i></span>
		</div>
		<div id="akPriceSelectEdit_<?php    echo $akPriceSelectID?>" style="display:none">
			<div class="rightCol">
				<input class="btn" type="button" onClick="ccmAttributesHelper.editValue('<?php    echo addslashes($akPriceSelectID)?>')" value="<?php    echo t('Cancel')?>" />
				<input class="btn" type="button" onClick="ccmAttributesHelper.changeValue('<?php    echo addslashes($akPriceSelectID)?>')" value="<?php    echo t('Save')?>" />
			</div>		
			<span class="leftCol">
				<input name="akPriceSelectOriginal_<?php    echo $akPriceSelectID?>" type="hidden" value="<?php    echo $akPriceSelect?>" />
				<?php     if (is_object($v) && $v->getSelectAttributeOptionTemporaryID() == false) { ?>
					<input id="akPriceSelectExistingOption_<?php    echo $akPriceSelectID?>" name="akPriceSelectExistingOption_<?php    echo $akPriceSelectID?>" type="hidden" value="<?php    echo $akPriceSelectID?>" />
				<?php     } else { ?>
					<input id="akPriceSelectNewOption_<?php    echo $akPriceSelectID?>" name="akPriceSelectNewOption_<?php    echo $akPriceSelectID?>" type="hidden" value="<?php    echo $akPriceSelectID?>" />
				<?php     } ?>
				<input id="akPriceSelectField_<?php    echo $akPriceSelectID?>" name="akPriceSelect_<?php    echo $akPriceSelectID?>" type="text" value="<?php    echo $akPriceSelect?>" size="20" />
				  
				<input id="akPriceSelectOptionField_<?php    echo $akPriceSelectID?>" name="akPriceSelectOption_<?php    echo $akPriceSelectID?>" type="text" value="<?php    echo $akPriceSelectOption?>" size="20" />
			</span>		
		</div>	
		<div class="ccm-spacer">&nbsp;</div>
<?php     } ?>

<fieldset>
<legend><?php    echo t('Price Options')?></legend>


<div class="clearfix">
<label><?php    echo t('Values')?></label>
<div class="input">
	<div id="attributeValuesInterface">
	<div id="attributeValuesWrap">
	<?php    
	Loader::helper('text');
	foreach($akPriceSelect as $v) { 
		if ($v->getSelectAttributeOptionTemporaryID() != false) {
			$akPriceSelectID = $v->getSelectAttributeOptionTemporaryID();
		} else {
			$akPriceSelectID = $v->getSelectAttributeOptionID();
		}
		?>
		<div id="akPriceSelectWrap_<?php    echo $akPriceSelectID?>" class="akPriceSelectWrap <?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?> akPriceSelectWrapSortable <?php     } ?>">
			<?php    echo getAttributeOptionHTML( $v )?>
		</div>
	<?php     } ?>
	</div>
	
	<div id="akPriceSelectWrapTemplate" class="akPriceSelectWrap" style="display:none">
		<?php    echo getAttributeOptionHTML('TEMPLATE') ?>
	</div>
	
	<div id="addAttributeValueWrap"> 
		<input id="akPriceSelectFieldNew" name="akPriceSelectNew" type="text" value="<?php    echo $defaultNewOptionNm ?>" size="40" class="faint" 
		onfocus="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',0)" 
		onblur="ccmAttributesHelper.clrInitTxt(this,'<?php    echo $defaultNewOptionNm ?>','faint',1)"
		onkeypress="ccmAttributesHelper.addEnterClick(event,function(){ccmAttributesHelper.saveNewOption()})"
		 placeholder="<?php  echo t('Option Name')?>" /> 
		 <input id="akPriceSelectOptionFieldNew" name="akPriceSelectOptionNew" type="text" value="" placeholder="<?php  echo t('Price (no currency symbols)')?>"/>
		<input class="btn" type="button" onClick="ccmAttributesHelper.saveNewOption(); $('#ccm-attribute-key-form').unbind()" value="<?php    echo t('Add') ?>"/>
	</div>
	</div>

</div>
</div>


</fieldset>
<?php     if ($akSelectOptionDisplayOrder == 'display_asc') { ?>
<script type="text/javascript">
//<![CDATA[
$(function() {
	ccmAttributesHelper.makeSortable();
});
//]]>
</script>
<?php     } ?>