<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class AgreementAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $agreement_text;
	public $agreement_link;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		$val = $this->getValue();
		$this->set('value',$val['accept']);
		$this->set('agreement',$val['agreement']);
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		return $list;
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atAgreementSettings where akID = ?',$this->attributeKey->getAttributeKeyID());
		}

		$this->akID = $row['akID'];
		$this->display_popup = $row['display_popup'];
		$this->agreement_text = $row['agreement_text'];
		$this->agreement_link = (!$row['agreement_link']) ? 'View Agreement Text' : $row['agreement_link'];

		$this->set('akID', $this->akID);
		$this->set('display_popup', $this->display_popup);
		$this->set('agreement_text', $this->agreement_text);
		$this->set('agreement_link', $this->agreement_link);
	}
	
	public function getAgreementText(){
		$this->load();
		$val = $this->getValue();
		if($val['agreement']){
			return $val['agreement'];
		}else{
			return $this->agreement_text;
		}
	}
	
	public function getDisplayValue(){
		$this->load();
		$val = $this->getValue();
		
		if($val['accept']){
			$document = str_replace("[STRIKETHROUGH]", '<div class="editable_text">', $val['agreement']);
			$document = str_replace("[/STRIKETHROUGH]", '</div>', $document);
			
			return 'user accepted! <br/>
			'.$document.'<br />';
		}
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getRow("SELECT * FROM atAgreement WHERE avID = ?",array($this->getAttributeValueID()));
		if(!$val['accept']){
			$val = null;
		}
		return $val;
	}
	
	public function display_review(){
		$this->load();
		$val = $this->getValue();
		
		if($val['accept']){
			print '<style type="text/css">.strikeout{text-decoration: line-through!important; color: #ff7171;}</style>';
			$document = str_replace("[STRIKETHROUGH]", '<div class="editable_text">', $val['agreement']);
			$document = str_replace("[/STRIKETHROUGH]", '</div>', $document);
			print $document.'<br />';
			print '<input type="checkbox" checked disabled/> &nbsp;&nbsp;'. t('Agreed');
			$fm = Loader::helper('form');
			print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',1);
			print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][agreement_text]',urlencode($val['agreement']));
		}else{
			print '<input type="checkbox" disabled/> &nbsp;&nbsp;'. t('Did NOT Agree');
		}
	}
	
	public function display_print(){
		$this->load();
		$val = $this->getValue();

		print '<style type="text/css">.strikeout{text-decoration: line-through!important; color: #ff7171;}</style>';
		$document = str_replace("[STRIKETHROUGH]", '<div class="editable_text">', $this->agreement_text);
		$document = str_replace("[/STRIKETHROUGH]", '</div>', $document);
		print $document.'<br />';
	}
	
	public function display(){
		$this->load();
		$val = $this->getValue();
		if($val['accept']){
			print '<input type="checkbox" checked /> &nbsp;&nbsp;'. t('Agreed').' '.'<b><a href="'.Loader::helper('concrete/urls')->getToolsURL('proforms/attribute_previewer.php','proforms').'?akID='.$this->attributeKey->getAttributeKeyID().'&avID='.$this->getAttributeValueID().'" id="agreement_'.$this->attributeKey->getAttributeKeyID().'" alt="file_path" class="dialog-launch" dialog-width="70%" dialog-height="340" dialog-modal="true" dialog-title="View Agreement" dialog-on-close="" onClick="javascript:;">'.t('View Saved Agreement').'</a></b>';
		}else{
			print '<input type="checkbox" disabled/> &nbsp;&nbsp;'. t('Did NOT Agree');
		}
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>A link will be presented to the end user useing the "agreement link" field. Clicking on the link will present a popup modal containing the "agreement text".</p> 
		');
	}
	
	public function saveForm($value) {

		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atAgreement where avID = ?', array($this->getAttributeValueID()));
		if($value['task'] != 'clear_extended_attribute'){
			$db->Execute('insert into atAgreement (avID,accept,agreement) values (?,?,?)', array($this->getAttributeValueID(),$value['value'],urldecode($value['agreement_text'])));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atAgreement where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'display_popup'=>($data['display_popup']) ? 1 : 0,
			'agreement_text'=>$data['agreement_text'],
			'agreement_link'=>$data['agreement_link']
		);
		
		$db=Loader::db();
		$db->Replace('atAgreementSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atAgreementSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}