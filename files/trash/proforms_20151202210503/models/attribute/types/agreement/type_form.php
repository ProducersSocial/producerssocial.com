<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
if(!$display_popup){
	$disabled = 'disabled';
	$agreement_link = null;
}else{
	$disabled = null;
}
?>
<fieldset>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('Modal Agreement?')?></label>
		<div class="input">
			<div id="ccm-editor-pane">
			  <?php     echo $fm->checkbox('display_popup', 1, $display_popup)?> &nbsp;<?php   echo t('Yes, display Agreement text in pop-up dialog.')?>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('Agreement Link')?></label>
		<div class="input">
			<div id="ccm-editor-pane">
			  <?php     echo $fm->text('agreement_link', $agreement_link,array($disabled=>$disabled))?>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<label for="akHandle" class="control-label"><?php   echo t('Agreement Text')?></label>
		<div class="input">
			<div style="text-align: center" id="ccm-editor-pane">
			  <?php      Loader::element('editor_init'); ?>
			  <?php      Loader::element('editor_config'); ?>
			  <?php      Loader::element('editor_controls', array('mode'=>'full')); ?>
			  <?php     
			  echo $fm->textarea('agreement_text', $agreement_text, array('style' => 'width: 85%; font-family: sans-serif;', 'class' => 'ccm-advanced-editor'))?>
			</div>
		</div>
	</div>
</fieldset>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#display_popup').change(function(){
				if($(this).is(':checked')){
					$('#agreement_link').attr('disabled',false);
				}else{
					$('#agreement_link').attr('disabled',true).val('');
				}
			});
		});
	</script>