<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
if($display_popup){
	//View Agreement Text
	print $fm->checkbox('akID['.$akID.'][value]', 1, $value).' '.t('Yes, I Agree - ').' '.'<b><a href="'.Loader::helper('concrete/urls')->getToolsURL('proforms/attribute_previewer.php','proforms').'?akID='.$akID.'" id="agreement_'.$akID.'" alt="file_path" class="dialog-launch" dialog-width="70%" dialog-height="340" dialog-modal="true" dialog-title="View Agreement" dialog-on-close="" onClick="javascript:;">'.$agreement_link.'</a></b>';

}else{
	?>
	<style type="text/css">
	.editable_text:hover,.strikeout:hover{border: 1px dotted #ff7171; cursor: pointer; margin: -1px -1px;}
	.strikeout{text-decoration: line-through!important; color: #ff7171;}
	</style>
	<?php   
	if($agreement){
		$agreement_text = $agreement;
	}
	$document = str_replace("[STRIKETHROUGH]", '<div class="editable_text">', $agreement_text);
	$document = str_replace("[/STRIKETHROUGH]", '</div>', $document);
	print '<div id="agreement_'.$akID.'">';
	print $document;
	print '</div>';
	print '<hr/>';

	print $fm->checkbox('akID['.$akID.'][value]', 1, $value).' '.t('Yes, I Agree');
	print $fm->hidden('akID['.$akID.'][agreement_text]',urlencode($document));
	?>
	  <script>
$(document).ready(function() {function strikeOut(el){el.removeClass('editable_text');el.addClass('strikeout');el.bind('click',function(l){strikeIn($(this));});   var text = $('#agreement_<?php   echo $akID?>').html();   $('#akID\\[<?php   echo $akID?>\\]\\[agreement_text\\]').val(encodeURIComponent(text));}function strikeIn(el){ el.removeClass('strikeout'); el.addClass('editable_text'); el.bind('click',function(l){strikeOut($(this));});var text = $('#agreement_<?php   echo $akID?>').html();$('#akID\\[<?php   echo $akID?>\\]\\[agreement_text\\]').val(encodeURIComponent(text));}$('.editable_text').click(function(e){strikeOut($(this));});$('.strikeout').click(function(e){strikeIn($(this));}); });
	  </script>
  <?php   
}