<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class AssociateObjectAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $formatting;
	public $include_specials;
	public $length;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		//$vals = Cache::get('json','vals_'.$this->attributeKey->getAttributeKeyID(), strtotime('- 1 month'),true);
		//if(!$vals){
			$akc = AttributeKeyCategory::getByHandle($this->object_type);
			switch ($this->object_type){
				
				case 'collection':
					Loader::model('page_list');
					$pl = new PageList();
					$objects = $pl->get();
					if($objects && is_array($objects)){
						foreach($objects as $obj){
						
							switch ($this->value_handle){
								case 'objectTypeID':
									$handle = $obj->getCollectionID();
									break;
									
								default:
									$handle = sprintf($obj->getAttribute($this->value_handle));
									break;
							}
							
							switch ($this->name_handle){
								case 'cvName':
									$value = $obj->getCollectionName();
									break;
								default:
									$value = sprintf($obj->getAttribute($this->name_handle));
									break;
							}
							
							$vals[$handle] = $value;
						}
					}
					break;
				
				case 'user':
					Loader::model('user_list');
					$pl = new UserList();
					$objects = $pl->get();
					if($objects && is_array($objects)){
						foreach($objects as $obj){
						
							switch ($this->value_handle){
								case 'objectTypeID':
									$handle = $obj->getUserID();
									break;
									
								default:
									$handle = sprintf($obj->getAttribute($this->value_handle));
									break;
							}
							
							switch ($this->name_handle){
								case 'uName':
									$value = $obj->getUserName();
									break;
								case 'uEmail':
									$value = $obj->getUserEmail();
									break;
								default:
									$value = sprintf($obj->getAttribute($this->name_handle));
									break;
							}
							
							$vals[$handle] = $value;
						}
					}
					break;
				
				default:
					$objectName = str_replace(' ','',ucfirst(str_replace('_',' ',$this->object_type)));
					$object_list_name = $objectName.'List';
					Loader::model($akc->getAttributeKeyCategoryHandle().'_list',$akc->getPackageHandle());
					$ol = new $object_list_name();
					$objects = $ol->get();
					$vals = array(''=>t(' -- Choose One -- '));
					if($objects && is_array($objects)){
						foreach($objects as $obj){
							if($this->value_handle == 'objectTypeID'){
								$id_method = 'get'.$objectName.'ID';
								$handle = $obj->$id_method();
							}else{
								$handle = sprintf($obj->getAttribute($this->value_handle));
							}
							$vals[$handle] = sprintf($obj->getAttribute($this->name_handle));
						}
					}
					break;
		}
			//$vals = json_encode($vals);
			//Cache::set('json','vals_'.$this->attributeKey->getAttributeKeyID(), $vals);
		//}
		//$vals = json_decode($vals);
		$fm = Loader::helper('form');
		print $fm->select('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$vals,$this->getValue());
		
		
	}
	
	public function display(){
		$this->load();
		echo $this->getValue();
	}
	
	public function getDisplayValue(){
		
		$this->load();
		if($this->object_type){
    		$akc = AttributeKeyCategory::getByHandle($this->object_type);
    		Loader::model($akc->getAttributeKeyCategoryHandle(),$akc->getPackageHandle());
    		$objectName = str_replace(' ','',ucfirst(str_replace('_',' ',$this->object_type)));
            $object = $objectName::getByID($this->getValue());
            
    		return $object->getAttribute($this->name_handle);
        }
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		
		$db = Loader::db();
		$cats = $db->getAll("SELECT akCategoryHandle FROM AttributeKeyCategories");
		$objects = array();
		foreach($cats as $cat){
			$objects[$cat['akCategoryHandle']] = $cat['akCategoryHandle'];
		}

		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Object Type').'</label>';
		print '		<div class="input">';
		print $fm->select('object_type',$objects,$this->object_type);
		print '		</div>';
		print '	</div>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Attribute Handle to use as Text').'</label>';
		print '		<div class="input">';
		print $fm->text('name_handle',$this->name_handle);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atAssociateObjectSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->object_type = $row['object_type'];
		$this->value_handle = 'objectTypeID'; //$row['value_handle'];
		$this->name_handle = $row['name_handle'];

		$this->set('akID', $this->akID);
		$this->set('object_type', $this->object_type);
		$this->set('value_handle', $this->value_handle);
		$this->set('name_handle', $this->name_handle);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>A pulldown option of Attribute values of all objects from a defined object type.</p><br /><p>For example:  the User object is selected, and the attribute handle last_name is set as the text, a select  input will be generated listing all users last names, with each user Object ID as the value.</p>
		');
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT name_handle FROM atAssociateObject WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function searchForm($list) {
		$db = Loader::db();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), '%' . $this->request('value') . '%', 'like');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print $f->text($this->field('value'), $this->request('value'));
	}

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atAssociateObject where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atAssociateObject (avID,name_handle) values (?,?)', array($this->getAttributeValueID(),$value['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atAssociateObject where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'object_type'=>$data['object_type'],
			'value_handle'=>$data['value_handle'],
			'name_handle'=>$data['name_handle']
		);
		
		$db=Loader::db();
		$db->Replace('atAssociateObjectSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atAssociateObjectSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}


}