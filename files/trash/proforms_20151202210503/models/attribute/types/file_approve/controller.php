<?php   
defined('C5_EXECUTE') or die("Access Denied.");

class FileApproveAttributeTypeController extends AttributeTypeController {

	protected $searchIndexFieldDefinition = 'I DEFAULT 0 NULL';
	public $is_multipart = 1;
	
	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atFileApproveSettings where akID = ?', $ak->getAttributeKeyID());
		}
	
		
		$this->akID = $row['akID'];
		$this->asID = $row['asID'];
		$this->approver = $row['approver'];

		$this->set('akID', $this->akID);
		$this->set('asID', $this->asID);
		$this->set('approver', $this->approver);
	}
	
	
	public function type_form(){
		$this->load();
		Loader::model('file_set');
		$this->set('fileSets', FileSet::getMySets());
	}

	public function getValue() {
		$dbs = Loader::db();
		$value = $dbs->GetOne("select fID from atFileApprove where avID = ?", array($this->getAttributeValueID()));
		if ($value > 0) {
			$f = File::getByID($value);
			return $f;
		}
	}
	
	public function getStatus(){
	    $dbs = Loader::db();
    	return $dbs->GetOne("select status from atFileApprove where avID = ?", array($this->getAttributeValueID()));
	}
	
	public function getPath($avID=null) {
		if($avID != null){
			$avID = $avID;
		}else{
			$avID = $this->getAttributeValueID();
		}
		$dbs = Loader::db();
		$pathy = $dbs->GetOne("select path from atFileApprove where avID = ?", array($avID));
		return $pathy;
	}

	public function getDisplayValue() {
		$f = $this->getValue();
		$path = $this->getPath();
		if (is_object($f)) {
			return BASE_URL.$path;
		}
	}
	
	public function display() {
		$f = $this->getValue();
		$path = $this->getPath();
		if (is_object($f)) {
			print '<a href="' . $path . '">' . $f->getFileName() . '</a>';
		}
	}

	public function searchForm($list) {
		$data = $this->request();
		$fileID = $data['value'];
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), $fileID);
		return $list;
	}

	public function getSearchIndexValue() {
		$dbs = Loader::db();
		$value = $dbs->GetOne("select fID from atFileApprove where avID = ?", array($this->getAttributeValueID()));
		return $value;
	}

	public function search() {
		// search by file causes too many problems
		//$al = Loader::helper('concrete/asset_library');
		//print $al->file('ccm-file-akID-' . $this->attributeKey->getAttributeKeyID(), $this->field('value'), t('Choose File'), $bf);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Simple File Question Type allows the user to upload a file without the need to use the C5 filemanager. </p>
		');
	}

	public function form() {
	    $this->load();
	    
	    $approve_path = BASE_URL.DIR_REL.'/tools/packages/proforms/proforms/attributes/file_approve/file_approve.php';
	    
		$bf = false;
		if ($this->getAttributeValueID() > 0) {
			$bf = $this->getValue();
			$img = Loader::helper('image');
			$thumb = $img->getThumbnail($bf, 150, 50);
			$path = $this->getPath();
		}
		$form = Loader::helper('form');
		if ($bf) {
		    $dashboard = strpos($_SERVER['REQUEST_URI'],'dashboard');
		    if($dashboard > 0 && $this->approver == 'user'){
    			print '<a href="'.$bf->getDownloadURL().'" target="_blank">'.$bf->getFileName().'</a> &nbsp;&nbsp;';
    			
    			$status = $this->getStatus();
                if($status == 'approved'){
                    print "<i class=\"icon-ok-sign\"></i> file approved";
                }else{
                    print '[not approved]';
                }
                
    			print '<br /><br />';
    			print t('Or upload a new file');
    			print '<br /><br />';
    			print $form->file($this->field('new_file'));
    			$key = $this->field('existing_value');
    			print '<input type="hidden" name="' . $key . '" id="' . $key . '" value="' . $bf->getFileID() . '" />';
            }elseif($dashboard < 1 && $this->approver == 'admin'){
                print '<a href="'.$bf->getDownloadURL().'" target="_blank">'.$bf->getFileName().'</a> &nbsp;&nbsp;';
                
                $status = $this->getStatus();
                if($status == 'approved'){
                    print "<i class=\"icon-ok-sign\"></i> file approved";
                }else{
                    print '[not approved]';
                }
                
    			print '<br /><br />';
    			print t('Or upload a new file');
    			print '<br /><br />';
    			print $form->file($this->field('new_file'));
    			$key = $this->field('existing_value');
    			print '<input type="hidden" name="' . $key . '" id="' . $key . '" value="' . $bf->getFileID() . '" />';
            }else{
                print '<a href="'.$bf->getDownloadURL().'" target="_blank">'.$bf->getFileName().'</a> &nbsp;&nbsp;';
                
                $status = $this->getStatus();
                if($status == 'approved'){
                    print "<i class=\"icon-ok-sign\"></i> file approved";
                }else{
                    print '<a href="javascript:;" class="btn btn-info approve_file" data-rel="'.$this->getAttributeValueID().'">Approve File</a>';
                    print '<script type="text/javascript">
        $(document).ready(function(){
            $(".approve_file").click(function(){
                var file = $(this);
                $.ajax({
                    url: "'.$approve_path.'",
                    type: "POST",
                    data: {
                        avID: file.attr("data-rel"),
                        proformsItemID: "'.$_REQUEST['ProformsItemID'].'"
                    },
                    success: function(response){
                        console.log(response);
                        if(response == "success"){
                            file.replaceWith("<i class=\"icon-ok-sign\"></i> file approved");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(textStatus);
                    }
                });
            });
        });
    </script>';
                }
            }
			
		} else {
		    $dashboard = strpos($_SERVER['REQUEST_URI'],'dashboard');
		    if($dashboard > 0 && $this->approver == 'user'){
    			print t('Upload a file');
    			print '<br />';
    			print $form->file($this->field('new_file'));
    			print $form->hidden($this->field('existing_value'), 0);
            }elseif($dashboard < 1 && $this->approver == 'admin'){
    			print t('Upload a file');
    			print '<br />';
    			print $form->file($this->field('new_file'));
    			print $form->hidden($this->field('existing_value'), 0);
            }else{
                print 'No files needing approval at this time.';
            }
		}
		print $form->hidden($this->field('ajax_send'), 0);
		print '
<script type="text/javascript">

		$("#akID\\\['.$this->attributeKey->getAttributeKeyID().'\\\]\\\[new_file\\\]").bind( \'change\', function () {
			$("#akID\\\['.$this->attributeKey->getAttributeKeyID().'\\\]\\\[ajax_send\\\]").val(\'file_selected\');
		});

</script>';
	}

	// run when we call setAttribute(), instead of saving through the UI
	public function saveValue($obj) {
		if(is_object($obj)){
			$path = BASE_URL.DIR_REL.$obj->getRelativePath();
			$fID = $obj->getFileID();
		}else{
			$fID = null;
			$path = null;
		}
		$path = $obj->getRelativePath();
		$dbs = Loader::db();
		$dbs->Replace('atFileApprove', array('avID' => $this->getAttributeValueID(), 'fID' => $fID,'status'=>'','path'=>$path), 'avID', true);
		
		Loader::model('proforms_item','proforms');
        $pf = ProformsItem::getByID($_REQUEST['proformsItemID']);
        Events::fire('proforms_file_approve_added', $pf);
	}
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'asID'=>$data['fasID'],
			'approver'=>$data['approver']
		);
		
		$db=Loader::db();
		$db->Replace('atFileApproveSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$dbs = Loader::db();
		$arr = $this->attributeKey->getAttributeValueIDList();
		foreach ($arr as $id) {
			$dbs->Execute('delete from atFileApprove where avID = ?', array($id));
		}
	}

	public function saveForm($data) {
		
		if ($data['value'] > 0) {
			$f = File::getByID($data['value']);
			$this->saveValue($f);
		} else {
			$dbs = Loader::db();
			$dbs->Replace('atFileApprove', array('avID' => $this->getAttributeValueID(), 'fID' => 0), 'avID', true);
		}
	}

	public function deleteValue() {
		$dbs = Loader::db();
		$dbs->Execute('delete from atFileApprove where avID = ?', array($this->getAttributeValueID()));
	}

	public function post($field = false) {
		// the only post that matters is the one for this attribute's name space

		$files = $_FILES;
		$post = $_POST;
		if (is_object($this->attributeKey) && strlen($files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'])) {
			// we have uploaded a new file
			$existingFileID = $post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
			$f = File::getByID($existingFileID);
			$filename = $files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			$path = $files['akID']['tmp_name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			if ($existingFileID && (intval($existingFileID) > 0)) {
				// replace the file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename, $f);
			} else {
				// creating a new file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename);
			}
			if($this->asID){
				$fs = FileSet::getByID($this->asID);
				$fs->addFileToSet($newFile);
			}
			$data = array();
			$data['value'] = $newFile->getFileID();
			return $data;
		} else {
			if ($post['akID'][$this->attributeKey->getAttributeKeyID()]['clear_image'] ){
				// clear out any existing file in the file manager
				if ($post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']){
					$f = File::getByID($post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']);
					$f->delete();
				}
				// return an empty array to set the fID to 0
				$data = array();
				return $data;
			} else {
				// we aren't changing anything and have an existing file
				$data = array();
				$data['value'] = $post['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
				return $data;
			}
		}
	}

	public function request($field = false) {
		// the only post that matters is the one for this attribute's name space
		$files = $_FILES;
		$request = $_REQUEST;
		if (is_object($this->attributeKey) && strlen($files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'])) {
			// we have uploaded a new file
			$existingFileID = $request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
			$f = File::getByID($existingFileID);
			$filename = $files['akID']['name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			$path = $files['akID']['tmp_name'][$this->attributeKey->getAttributeKeyID()]['new_file'];
			if ($existingFileID) {
				// replace the file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename, $f);
			} else {
				// creating a new file in the system
				Loader::library("file_importer","proforms");
				$fi = new FileImporter();
				$newFile = $fi->import($path, $filename);
			}
			if($this->asID){
				$fs = FileSet::getByID($this->asID);
				$fs->addFileToSet($newFile);
			}
			$data = array();
			$data['value'] = $newFile->getFileID();
			return $data;
		} else {
			if ($request['akID'][$this->attributeKey->getAttributeKeyID()]['clear_image'] ){
				// clear out any existing file in the file manager
				if ($request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']){
					$f = File::getByID($request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value']);
					$f->delete();
				}
				// return an empty array to set the fID to 0
				$data = array();
				return $data;
			} else {
				// we aren't changing anything and have an existing file
				$data = array();
				$data['value'] = $request['akID'][$this->attributeKey->getAttributeKeyID()]['existing_value'];
				return $data;
			}
		}
	}

	public function validateForm($data){
		$newData = $this->request();
		if ($newData['value'] > 0){
			return true;
		} else {
			 $this->error = t('You are missing a required file!');
			return false;
		}
	}

}