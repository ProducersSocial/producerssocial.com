<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class RandomCodeAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $formatting;
	public $include_specials;
	public $length;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',1);
	}
	
	public function display(){
		$this->load();
		echo $this->getValue();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');

		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Formatting').'</label>';
		print '		<div class="input">';
		print $fm->select('formatting',array('letters_numbers'=>t('Letters & Numbers'),'letters'=>t('Just Letters'),'numbers'=>t('Just Numbers')),$this->formatting);
		print '		</div>';
		print '	</div>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Include Special Characters').'</label>';
		print '		<div class="input">';
		print $fm->checkbox('include_specials',1,$this->include_specials);
		print '		</div>';
		print '	</div>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Generated Code Length').'</label>';
		print '		<div class="input">';
		print $fm->text('length',$this->length,array('class'=>'input-mini')).t(' characters');
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atRandomCodeSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->formatting = $row['formatting'];
		$this->include_specials = $row['include_specials'];
		$this->length = $row['length'];

		$this->set('akID', $this->akID);
		$this->set('formatting', $this->formatting);
		$this->set('include_specials', $this->include_specials);
		$this->set('length', $this->formatting);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT code_string FROM atRandomCode WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function searchForm($list) {
		$db = Loader::db();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), '%' . $this->request('value') . '%', 'like');
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print $f->text($this->field('value'), $this->request('value'));
	}

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atRandomCode where avID = ?', array($this->getAttributeValueID()));
		$code_string = $this->generateCode();
		$db->Execute('insert into atRandomCode (avID,code_string) values (?,?)', array($this->getAttributeValueID(),$code_string));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atRandomCode where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'formatting'=>$data['formatting'],
			'include_specials'=>$data['include_specials'],
			'length'=>$data['length']
		);
		
		$db=Loader::db();
		$db->Replace('atRandomCodeSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atRandomCodeSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}
	
	public function generateCode(){
	
		$length = $this->length;
		
		if($this->formatting == 'letters_numbers'){
	    	$characters = '123456789BCDFGHJKLMNPQRSTVWXZ';
	    }elseif($this->formatting == 'letters'){
	    	$characters = 'BCDFGHJKLMNPQRSTVWXZ';
	    }elseif($this->formatting == 'numbers'){
	    	$characters = '123456789';
	    }
	    
	    if($this->include_specials){
		    $characters .= '!@^*_';
	    }
	    
	    $string = '';    
	
	    for ($p = 0; $p < $length; $p++) {
	        $string .= substr($characters, rand() % strlen($characters), 1);
	    }
	
		return $string;
	
	}

}