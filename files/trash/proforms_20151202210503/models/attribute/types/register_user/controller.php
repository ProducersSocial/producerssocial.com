<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class RegisterUserAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $require_review;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
		print Loader::helper('form')->hidden($this->field('value'), 2);
		$vals = $this->getValue();
		$this->set('username_field',$vals['username_field']);
		$this->set('email_field',$vals['email_field']);
		$this->set('password_field',$vals['password_field']);
	}
	
	public function display(){
		$this->load();
		if($this->require_review > 0){
			print Loader::helper('form')->hidden($this->field('value'), 2);
		}
		$vals = $this->getValue();
		$this->set('username_field',$vals['username_field']);
		$this->set('email_field',$vals['email_field']);
		$this->set('password_field',$vals['password_field']);
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$db = Loader::db();
		$handle = 'ak_'.$this->attributeKey->getAttributeKeyHandle();
		if($this->request('value') != 1){
			$list->filter(false,"$handle <> 1 OR $handle IS NULL");
		}else{
			$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		}
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->checkbox($this->field('value'), 1, $this->request('value')).' '.t('show entries that have already been registered');
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atRegisterUserSettings where akID = ?', $ak->getAttributeKeyID());
		}
		
		$this->akID = $row['akID'];
		$this->require_review = $row['require_review'];
		$this->group = $row['gID'];

		$this->set('akID', $this->akID);
		$this->set('require_review', $this->require_review);
		$this->set('group', $this->group);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getRow("SELECT * FROM atRegisterUser WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function validateForm($data){
		$this->load();
		if($data > 0 && !$_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()]['user']){
			$valc = Loader::helper('concrete/validation');
			$vals = Loader::helper('validation/strings');
			$userHelper = Loader::helper('concrete/user');
			
			$uName = $_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()]['username_field'];
			$uEmail = $_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()]['email_field'];
			$uPass = $_REQUEST['akID'][$this->attributeKey->getAttributeKeyID()]['password_field'];
			
	
			if (!$vals->email($uEmail)) {
				$this->error .= t('Invalid email address provided.');
			} else if (!$valc->isUniqueEmail($uEmail)) {
				$this->error .= t("The email address %s is already in use. Please choose another email.", $uEmail);
			}
			
			if (strlen($uName) < USER_USERNAME_MINIMUM) {
				$this->error .= t('A username must be at least %s characters long.', USER_USERNAME_MINIMUM);
			}
	
			if (strlen($uName) > USER_USERNAME_MAXIMUM) {
				$this->error .= t('A username cannot be more than %s characters long.', USER_USERNAME_MAXIMUM);
			}
	
	
			if (strlen($uName) >= USER_USERNAME_MINIMUM && !$valc->username($uName)) {
				if(USER_USERNAME_ALLOW_SPACES) {
					$this->error .= t('A username may only contain letters, numbers and spaces.');
				} else {
					$this->error .= t('A username may only contain letters or numbers.');
				}
				
			}
			
			if (!$valc->isUniqueUsername($uName)) {
				$this->error .= t("The username %s already exists. Please choose another", $uName);
			}
			
			if ((strlen($uPass) < USER_PASSWORD_MINIMUM) || (strlen($uPass) > USER_PASSWORD_MAXIMUM)) {
				$this->error .= t('A password must be between %s and %s characters', USER_PASSWORD_MINIMUM, USER_PASSWORD_MAXIMUM);
			}
				
			if (strlen($uPass) >= USER_PASSWORD_MINIMUM && !$valc->password($uPass)) {
				$this->error .= t('A password may not contain ", \', >, <, or any spaces.');
			}
		}
	}
	

	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Register User Question Type will present normal username, password, and email inputs togeter, and can be immediately registered as a user or registered after review. </p>
		');
	}
	
	
	public function saveForm($data) {
		$this->load();
		$pfID = $_REQUEST['ProformsItemID'];
		if($_REQUEST['question_set']){
			$as = AttributeSet::getByID($_REQUEST['question_set']);
		}else{
			$as = AttributeSet::getByID($_REQUEST['asID']);
		}
		$setAttribs = $as->getAttributeKeys();
		$set_name = $as->getAttributeSetName();

		$uName = $data['username_field'];
		$uEmail = $data['email_field'];
		$uPass = $data['password_field'];
		$uID = $data['user'];
		
		if($data['value'] > 0){
			Loader::model('userinfo');
			
			if($uID){
				$ui = UserInfo::getByID($uID);
			}else{
				$data = $_POST;
				$data['uName'] = $uName;
				$data['uEmail'] = $uEmail;
				$data['uPassword'] = $uPass;
				$data['uPasswordConfirm'] = $uPass;
				$data['uIsValidated'] = 1;
	
				$ui = UserInfo::register($data);
			}
			
			if($this->group != ''){
				$ui->updateGroups(array(1,2,$this->group));
			}else{
				$ui->updateGroups(array(1,2));
			}
			
			$process = $ui;
			
			if (is_object($process)) {
				
				if (REGISTER_NOTIFICATION) { //do we notify someone if a new user is added?
					$mh = Loader::helper('mail');
					if(EMAIL_ADDRESS_REGISTER_NOTIFICATION) {
						$mh->to(EMAIL_ADDRESS_REGISTER_NOTIFICATION);
					} else {
						$adminUser = UserInfo::getByID(USER_SUPER_ID);
						if (is_object($adminUser)) {
							$mh->to($adminUser->getUserEmail());
						}
					}
					
					$mh->addParameter('uID',    $process->getUserID());
					$mh->addParameter('user',   $process);
					$mh->addParameter('uName',  $process->getUserName());
					$mh->addParameter('uEmail', $process->getUserEmail());
					$attribs = UserAttributeKey::getRegistrationList();
					$attribValues = array();
					foreach($attribs as $ak) {
						$attribValues[] = $ak->getAttributeKeyDisplayHandle() . ': ' . $process->getAttribute($ak->getAttributeKeyHandle(), 'display');		
					}						
					$mh->addParameter('attribs', $attribValues);
					
					if (defined('EMAIL_ADDRESS_REGISTER_NOTIFICATION_FROM')) {
						$mh->from(EMAIL_ADDRESS_REGISTER_NOTIFICATION_FROM,  t('Website Registration Notification'));
					} else {
						$adminUser = UserInfo::getByID(USER_SUPER_ID);
						if (is_object($adminUser)) {
							$mh->from($adminUser->getUserEmail(),  t('Website Registration Notification'));
						}
					}
					if(REGISTRATION_TYPE == 'manual_approve') {
						$mh->load('user_register_approval_required');
					} else {
						$mh->load('user_register');
					}
					$mh->sendMail();
				}
				
				// now we log the user in
				if (USER_REGISTRATION_WITH_EMAIL_ADDRESS) {
					$u = new User($uEmail,$uPass);
				} else {
					$u = new User($uName, $uPass);
				}
				// if this is successful, uID is loaded into session for this user
				
				$rcID = $this->post('rcID');
				$nh = Loader::helper('validation/numbers');
				if (!$nh->integer($rcID)) {
					$rcID = 0;
				}
				
				// now we check whether we need to validate this user's email address
				if (defined("USER_VALIDATE_EMAIL") && USER_VALIDATE_EMAIL) {
					if (USER_VALIDATE_EMAIL > 0) {
						$uHash = $process->setupValidation();
						
						$mh = Loader::helper('mail');
						if (defined('EMAIL_ADDRESS_VALIDATE')) {
							$mh->from(EMAIL_ADDRESS_VALIDATE,  t('Validate Email Address'));
						}
						$mh->addParameter('uEmail', $uEmail);
						$mh->addParameter('uHash', $uHash);
						$mh->to($uEmail);
						$mh->load('validate_user_email');
						$mh->sendMail();
						$u->logout();
					}
				} else if(defined('USER_REGISTRATION_APPROVAL_REQUIRED') && USER_REGISTRATION_APPROVAL_REQUIRED) {
					$ui = UserInfo::getByID($u->uID);
					$ui->deactivate();
					//$this->redirect('/register', 'register_pending', $rcID);
					$redirectMethod='register_pending';
					$registerData['msg']=$this->getRegisterPendingMsg();
					$u->logout();
				}
				
				if (!$u->isError()) {
					//$this->redirect('/register', 'register_success', $rcID);
					$registerData['uID']=intval($u->uID);		
				}

			}

			foreach($setAttribs as $at){
				$handle = $at->getAttributeKeyHandle();
				$cak = UserAttributeKey::getByHandle($handle);
				if(is_object($cak)){
					$pf = ProformsItem::getByID($pfID);
					$value = $pf->getAttribute($handle);
					$ui->setAttribute($cak,$value);
				}
			}
		}
		
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atRegisterUser where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atRegisterUser (avID,registered,username_field,email_field,password_field) values (?,?,?,?,?)', array($this->getAttributeValueID(),$data['value'],$uName,$uEmail,$uPass));

	}
	
	public function getRegisterSuccessMsg(){
		return t('Your account has been created, and you are now logged in.');
	}
	
	public function getRegisterSuccessValidateMsgs(){
		$msgs=array();
		$msgs[]= t('You are registered but you need to validate your email address. Some or all functionality on this site will be limited until you do so.');
		$msgs[]= t('An email has been sent to your email address. Click on the URL contained in the email to validate your email address.');
		return $msgs;
	}
	
	public function getRegisterPendingMsg(){
		return t('You are registered but a site administrator must review your account, you will not be able to login until your account has been approved.');
	}	
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atRegisterUser where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		if(!$data['require_review']){
			$data['require_review'] = 0;
		}
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'require_review'=>$data['require_review'],
			'gID'=>$data['group']
		);
		
		$db=Loader::db();
		$db->Replace('atRegisterUserSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atRegisterUserSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}