<?php    
defined('C5_EXECUTE') or die("Access Denied.");

$fm = Loader::helper('form');
$u = new User();
if(!$u->isLoggedIn()){
?>
	<div class="clearfix">
		<strong><?php    echo t('Username');?> <span class="required"> *</span></strong>
		<div class="input">
			<div class="proform-attributes">
				<?php   
				echo $fm->text($this->field('username_field'), $username_field);
				?>
			</div>
		</div>
	</div>
	
	<div class="clearfix">
		<strong><?php    echo t('Email');?> <span class="required"> *</span></strong>
		<div class="input">
			<div class="proform-attributes">
				<?php   
				echo $fm->email($this->field('email_field'), $email_field);
				?>
			</div>
		</div>
	</div>
	
	<div class="clearfix">
		<strong><?php    echo t('Password');?> <span class="required"> *</span></strong>
		<div class="input">
			<div class="proform-attributes">
				<?php   
				echo $fm->password($this->field('password_field'), $password_field);
				?>
			</div>
		</div>
	</div>
<?php 
}else{
	echo $fm->hidden($this->field('user'), $u->uID);
}
?>
	