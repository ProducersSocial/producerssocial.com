<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
$ps = Loader::helper('form/page_selector');

Loader::model("groups");
$groups = new GroupList($u,false,true);
$grps = $groups->getGroupList();
//var_dump($grps);
$groups_array['0'] = '-- none --';
foreach($grps as $grp){
	$groups_array[$grp->getGroupID()] = $grp->getGroupName();
	///echo $group->getGroupID().' - '.$group->getGroupName().'<br />';
}
//var_dump($groups_array);
?>
<div class="clearfix">
	<label><?php    echo t("Register to group?")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <?php   echo $fm->select('group',$groups_array,$group)?>
		</label>
	</div>
</div>
<!--
<div class="clearfix">
	<label><?php    echo t("Require Review?")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="require_review" value="1" <?php    if($require_review>0){echo 'checked';}?>> <?php    echo t('Yes. Form Entries can only be registered by checkbox via backend.');?>
		</label>
	</div>
</div>
-->