<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class HiddenPageAttributeAttributeTypeController extends AttributeTypeController  {
	

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		if($_REQUEST[$this->request_name]){
			$ccID = $_REQUEST[$this->request_name];
			$page = Page::getByID($ccID);
		}else{
			global $c;
			$page = $c;
		}
		
		if(is_object($page)){
			if($this->attribute_handle == 'getCollectionName'){
				$val = $page->getCollectionName();
			}else{
				$val = sprintf($page->getAttribute($this->attribute_handle));
			}
			print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$val);
		}
	}
	
	public function display(){
		$this->load();
		echo t('Page Attribute ').$this->attribute_handle.': '.$this->getValue();
	}
	
	public function getDisplayValue(){
		$this->load();
		return t('Page Attribute ').$this->attribute_handle.': '.$this->getValue();
	}
	
	public function getDisplayReviewValue(){
		$this->load();
		return t('Page Attribute ').$this->attribute_handle.': '.$this->getValue();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');

		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Name of variable to pass as collectionID (do not use cID)').'</label>';
		print '		<div class="input">';
		print $fm->text('request_name',$this->request_name);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Page Attribute Handle to grab').'</label>';
		print '		<div class="input">';
		print $fm->text('attribute_handle',$this->attribute_handle);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atHiddenPageAttributeSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->request_name = $row['request_name'];
		$this->attribute_handle = $row['attribute_handle'];

		$this->set('akID', $this->akID);
		$this->set('request_name', $this->request_name);
		$this->set('attribute_handle', $this->attribute_handle);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT value FROM atHiddenPageAttribute WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Hidden Page Attribute QuestionType listens for a designated $_REQUEST variable name that represents a page/collection ID. It will then pull a designated attribute value from that page and saves that value as a hidden question to the form.</p>
		<br /> 
		<h4>Advanced Use</h4>
		<p>You can have several Hidden Page Attribute QuestionTypes in one form all pointing the same $_REQUEST collectionID!</p>
		<p>This means you can set up as many Hidden Page Attribute Questions as you wish with various data populated from a source linked page.  And then use that data, for example, in the Autoresponder QuestionType as a form source value. Get Creative!!!</p>
		');
	}

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$value = $value['value'];
		$db->Execute('delete from atHiddenPageAttribute where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atHiddenPageAttribute (avID,value) values (?,?)', array($this->getAttributeValueID(),$value));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'request_name'=>$data['request_name'],
			'attribute_handle'=>$data['attribute_handle']
		);
		
		$db=Loader::db();
		$db->Replace('atHiddenPageAttributeSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atHiddenPageAttributeSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}