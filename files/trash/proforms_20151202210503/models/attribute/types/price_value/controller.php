<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PriceValueAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');

		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$this->price,array('class'=>'price_value'));

	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="price" class="control-label">'.t('Price Value').'</label>';
		print '		<div class="input">';
		print $fm->text('price',$this->price);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Item Label').'</label>';
		print '		<div class="input">';
		print $fm->text('value',$this->value);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPriceValueSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->price = $row['price'];
		$this->value = $row['value'];

		$this->set('akID', $this->akID);
		$this->set('price', $this->price);
		$this->set('value', $this->value);
	}
	
	public function getValue(){
		return $this->price;
	}

	
	public function saveForm($value) {
		$this->load();
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'price'=>$data['price'],
			'value'=>$data['value']
		);
		
		$db=Loader::db();
		$db->Replace('atPriceValueSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceValueSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}