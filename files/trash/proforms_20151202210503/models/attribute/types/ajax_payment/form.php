<?php 
defined('C5_EXECUTE') or die("Access Denied.");

$fm = Loader::helper('form');
?>
<div id="payment_form"></div>
<script type="text/javascript">
	/*<![CDATA[*/	
	$(document).ready(function(){
		$.ajax({
			url: '<?php echo Loader::helper('concrete/urls')->getToolsURL('proforms/attributes/ajax_payment/get_payment_type.php','proforms')?>',
			type: "GET",
			data: {
				payment_form: <?php echo $_REQUEST['entryID']?>,
				payment_handle: '<?php echo $payment_select_handle?>'
			},
			success: function(response){
				$('#payment_form').html(response);
				$('.ajax_payment label').remove();
			}
		});
	});
	/*]]>*/
</script>