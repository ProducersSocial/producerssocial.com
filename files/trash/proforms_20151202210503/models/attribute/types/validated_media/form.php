<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
?>
<style type="text/css">
	.add_file_<?php   echo $akID?> { 
		display: block;
		height: 24px;
		line-height: 24px!important;
		background-image: url('/packages/proforms/images/add_48.png');
		background-size: 24px;
		background-repeat: no-repeat;
		background-position-x: left;
		padding-left: 32px!important;
	}
	
	.file_wrapper{
		display: block;
		margin: 12px 0px;
	}
	
	.input_wrapper input{
		float: left;
		margin: 6px 0px;
	}
	.icon {
		display: block;
		float: left;
		clear: both;
		margin-top: 4px!important;
		margin-right: 3px!important;
		height:20px;
		width:20px;
		background-image:url('<?php     echo ASSETS_URL_IMAGES?>/icons_sprite.png'); /*your location of the image may differ*/
	}
	.delete_validated_media_item {
		background-position: -22px -635px;
	}
</style>
<i class="validated_media_instructions">
<?php    if($qty){echo t('Limit of '.$qty.' per entry. ');}?>
<?php    if($size_limit){echo t('Files must be smaller than '.($size_limit / 1000).'mb');}?>
<?php    if($size_limit && (count($allowed_types_image) > 0 || count($allowed_types_video) > 0)){ echo ' and ';}?>
<?php    
if(count($allowed_types_image) > 0 || count($allowed_types_video) > 0){
	echo 'Files must be ';
}
if(count($allowed_types_image) > 0 && ($type == 'video_image' || $type == 'image')){
	foreach($allowed_types_image as $ftype){
		if($ft){ echo ', ';}
		echo "'".$ftype."'";
		$ft++;
	}
}

if(count($allowed_types_video) > 0 && ($type == 'video_image' || $type == 'video')){
	foreach($allowed_types_video as $ftype){
		if($ft){ echo ', ';}
		echo "'".$ftype."'";
		$ft++;
	}
}

?>
</i><br/>
<br/>
<a href="javascript:void(0);" class="add_file_<?php   echo $akID?>"><?php   echo t('Add File')?></a> 
<div class="input_option_<?php   echo $akID?> sample_file_<?php   echo $akID?>" style="display: none;">
	<a href="javascript:;" onClick="removeMediaItem($(this));" class="icon delete_validated_media_item"></a><?php    echo $fm->file('akID['.$akID.'][]'); ?>
</div>

<div id="file_input_wrapper_<?php   echo $akID?>" rel="<?php   echo count($fIDs)?>" class="input_wrapper">
	<?php   
	foreach($fIDs as $key=>$f){
		$fID = $f->getFileID();
		$name = $f->getFileName();
		$ext = strtolower($f->getExtension());
		?>
		<div class="input_option_<?php   echo $akID?> file_wrapper">
			<?php   
			if(in_array($ext,$allowed_types_video)){
				?>
				<a href="javascript:;" class="icon delete"></a> <a href="<?php    echo Loader::helper('concrete/urls')->getToolsURL('proforms/show_preview_image.php','proforms').'?vID='.$fID.'&ext='.$ext; ?>" id="image_<?php   echo $key?>_<?php   echo $akID?>" alt="file_path" class="dialog-launch" dialog-width="620" dialog-height="340" dialog-modal="true" dialog-title="View Image" dialog-on-close="" onClick="javascript:;"><?php    echo $name; ?></a>
				<?php   
			}else{
				$width = $f->getAttributeList()->getAttribute('width');
				if($width > 800){
					$width = 800;
				}
				?>
				<a href="javascript:;" class="icon delete"></a> <a href="<?php    echo Loader::helper('concrete/urls')->getToolsURL('proforms/show_preview_image.php','proforms').'?fID='.$fID; ?>" id="image_<?php   echo $key?>_<?php   echo $akID?>" alt="file_path" class="dialog-launch" dialog-width="<?php   echo $width?>" dialog-height="440" dialog-modal="true" dialog-title="View Image" dialog-on-close="" onClick="javascript:;"><?php    echo $name; ?></a>
			<?php   
			}
			?>
			<?php   echo $fm->hidden('akID['.$akID.'][existing_values][]',$fID)?>
		</div>
		<?php   
	}
	
	if(!$qty){$qty = 10;}
	?>
</div>
<script type="text/javascript">
/*<![CDATA[*/
$('.add_file_<?php   echo $akID?>').click(function(e){
	
	e.preventDefault();

	// get the element who trigged the event
	var qty = <?php   echo $qty?>;
	
    var jqEl = $('.sample_file_<?php   echo $akID?>');
    // get his parent div
    var tag = $('#file_input_wrapper_<?php   echo $akID?>');
    //get count
    var cnt = tag.attr('rel');
    // check which action we should perform
    if(qty && (cnt < qty)){
    	cnt = ( cnt * 1 ) + 1;
        tag.attr('rel',cnt);
        // clone the tag (div), reset his input text field
        // and place the new tag after the current one
        tag.append(jqEl.clone().find("input").val("").end().removeClass('sample_file_<?php   echo $akID?>').addClass('new_file_<?php   echo $akID?>').css('display','block'));
        
        $('a.delete').click(function(){
        	var cntr = $('#file_input_wrapper_<?php   echo $akID?>').attr('rel');
        	if(cntr > 0){
        		cntr = ( cntr * 1 ) - 1;
        		$('#file_input_wrapper_<?php   echo $akID?>').attr('rel',cntr);
				$(this).parent().remove();
			}else{
				$(this).parent().find("input").val("");
			}
		});
    }else{
	    alert('<?php   echo t('You have already added the maximum number of files allowed for this field.')?>');
    }
});


function removeMediaItem(item) {
	var count = $('#file_input_wrapper_<?php   echo $akID?> .input_option_<?php   echo $akID?>').length;
	if(count > 0){
		item.parent().remove();
		count = $('#file_input_wrapper_<?php   echo $akID?> .input_option_<?php   echo $akID?>').length;
		$('#file_input_wrapper_<?php   echo $akID?>').attr('rel',count);
	}else{
		item.parent().find("input").val("");
	}
};


//form validate, check file extensions, then submit

$('.new_file_<?php   echo $akID?> input').bind( 'change', function () {
		
		var validated = false;
		var message = '';
		<?php   
		if($size_limit){
		?>
		if((this.files[0].size * 1) > <?php   echo ($size_limit*1000)?> ){
			message += '<?php   echo t('Files must be smaller than '.($size_limit / 1000).'mb')?>';
		}
		<?php   
		}
		?>
       <?php   
       if(count($allowed_types_image) > 0 && ($type == 'video_image' || $type == 'image')){
       ?>
       if(isImage_<?php   echo $akID?>($(this).val())){
	       validated = true;
       }
       <?php   
       }
       if(count($allowed_types_video) > 0 && ($type == 'video_image' || $type == 'video')){
       ?>
       if(isVideo_<?php   echo $akID?>($(this).val())){
	       validated = true;
       }
       <?php   
       }
       ?>

       
	    if($('.new_file_<?php   echo $akID?> input').length < 1){
		    validated = true;
	    }
	    
	    if(message == '' && validated){
	    	return true;
	    }else{
	    	if(message.length > 0){
				alert(message);
			}
			if(!validated){
				alert('<?php   echo t('Your file is not the correct file type.  Please try again.')?>');
			}
			$(this).val('');
	    	return false;
		}
});


function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

<?php    if(count($allowed_types_image)>0){ ?>
function isImage_<?php   echo $akID?>(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
	    <?php   
	    foreach($allowed_types_image as $ftype){
			echo "case '".$ftype."':";
		}
		?>
        //etc
        return true;
    }
    return false;
}
<?php    } ?>
<?php    if(count($allowed_types_video)>0){ ?>
function isVideo_<?php   echo $akID?>(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
	    <?php   
	    foreach($allowed_types_video as $ftype){
			echo "case '".$ftype."':";
		}
		?>
        // etc
        return true;
    }
    return false;
}
<?php    } ?>	
$(document).ready(function(){
	$('.dialog-launch').dialog();
});
/*]]>*/
</script>