<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
$pkg = Package::getByHandle('proevents');
$dt = Loader::helper('form/date_time'); 
if($pkg){
			?>
			<div class="clearfix">
				<label><?php     echo t('Event Selection Type');?></label>
				<div class="input">
					<select name="type" id="date_type">
						<option value="single_date" <?php   if($type == 'single_date'){echo 'selected';}?>><?php   echo t('Single Date Picker');?></option>
						<option value="from_to" <?php   if($type == 'from_to'){echo 'selected';}?>><?php   echo t('From-To Date Picker');?></option>
						<option value="specific_date" <?php   if($type == 'specific_date'){echo 'selected';}?>><?php   echo t('Specific Date');?></option>
						<option value="listen" <?php   if($type == 'listen'){echo 'selected';}?>><?php   echo t('Listen for passed eID#');?></option>
					</select>
				</div>
			</div>
			<div class="clearfix chose_date">
				<label><?php     echo t('Choose Event Category');?></label>
				<div class="input">
		    	<?php    
		    		function getCat($category) {
		    		
		    		$selected_cat = explode(', ',$category);
					
					if(in_array('All Categories', $selected_cat) || empty($selected_cat)){
						echo '<input type="checkbox" name="category[]" value="All Categories" checked/> All Categories</br>';
					}else{
						echo '<input type="checkbox" name="category[]" value="All Categories"/> All Categories</br>';
					}
					$eventify = Loader::helper('eventify','proevents');
					$options = $eventify->getEventCats();
					
					foreach($options as $option){
						echo '<input type="checkbox" name="category[]" value="'.$option['value'].'"';
						if(in_array($option['value'], $selected_cat)){
							echo ' checked';
						}
						echo '/> '.$option['value'].' </br>';
					}
					
				}
				echo getCat($category);
				?>
				</div>
			</div>
			<div class="clearfix chose_date">
				<label><?php     echo t('Choose Calendar');?></label>
				<div class="input">
			    	<select name="section">
			    	<?php    
			    	function getSec($sctID) {
						$db = Loader::db();
						Loader::model('page_list');
						$eventSectionList = new PageList();
						$f = "ak_event_section = 1";
						$ubp = Page::getByPath('/homegroups');
						if($ubp->cID > 0){
							$f .= " OR ak_homegroup_section = 1";
						}
						$ubp = Page::getByPath('/meetings');
						if($ubp->cID > 0){
							$f .= " OR ak_meeting_section = 1";
						}
						$eventSectionList->filter(false,$f);
						$eventSectionList->sortBy('cvName', 'asc');
						//$eventSectionList->debug();
						$tmpSections = $eventSectionList->get();
						$sections = array();
						foreach($tmpSections as $_c) {
							$section = $_c->getCollectionName();
			
							if ($_c->getCollectionID() == $sctID && $section != 'All Sections'){
						  		echo '<option  value="'.$_c->getCollectionID().'"  selected>'.$section.'</option>';
							} elseif($section != 'All Sections') {
								echo  '<option value="'.$_c->getCollectionID().'">'.$section.'</option>';
							}
						}
						if ($sctID == 'All Sections' || $sctID == '' || !isset($sctID)){
							echo '<option value="All Sections" selected>'.t('All Calendars').'</option>';
						} elseif($sctID != 'All Sections') {
							echo '<option value="All Sections">'.t('All Calendars').'</option>';
						}
					}
					echo getSec($sctID);
					?>	
					</select>
				</div>
			</div>
			<div class="clearfix specific_date" style="display: <?php   if($type == 'specific_date' || !$type){ echo 'block'; }else{echo 'none';}?>;">
				<label><?php     echo t('Choose Date');?></label>
				<div class="input">
				<?php   
				if(!$search_date || date('Y',strtotime($search_date)) == '1969'){$search_date = date(DATE_APP_GENERIC_MDY);}
				echo $dt->date('search_date',$search_date);
				?>
				</div>
			</div>
			<div class="clearfix">
				<label for="event_require_payment" class="control-label"><?php echo t('Does Not Require Payment?')?></label>
				<div class="input">
					<?php echo Loader::helper('form')->checkbox('event_require_payment',1,$event_require_payment)?> <?php echo t('No, event processing does not requires payment notification.')?>
				</div>
			</div>
			<?php  

}else{
	echo t('You do not have ProEvents installed.  You must purchase ProEvents to use this Attribute/Question Type.');
}