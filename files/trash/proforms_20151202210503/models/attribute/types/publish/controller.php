<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PublishAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $publish_type;
	public $location;
	public $ctID;
	public $page_named;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$dpage = Page::getByID($this->location);
		$path = Loader::helper('navigation')->getLinkToCollection($dpage);
		$fm = Loader::helper('form');
		if($this->page_named > 0){
			print $fm->checkbox($this->field('value'), 1,$this->getValue()).t(' Yes, publish this to a page under ').$path;
			print $fm->hidden($this->field('reset'), 1);
		}else{
			print $fm->hidden($this->field('value'), 1 );
		}
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$db = Loader::db();
		$handle = 'ak_'.$this->attributeKey->getAttributeKeyHandle();
		if($this->request('value') != 1){
			$list->filter(false,"$handle <> 1 OR $handle IS NULL");
		}else{
			$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		}
		return $list;
	}
	
	public function search() {
		$f = Loader::helper('form');
		print ' &nbsp;&nbsp;'.$f->checkbox($this->field('value'), 1, $this->request('value')).' '.t('show entries that have already been published');
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPublishSettings where akID = ?', $ak->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->from_att = $row['from_att'];
		$this->to_att = $row['to_att'];
		$this->location = $row['location'];
		$this->ctID = $row['ctID'];
		$this->page_named = $row['page_named'];

		$this->set('akID', $this->akID);
		$this->set('from_att', $this->from_att);
		$this->set('to_att', $this->to_att);
		$this->set('location', $this->location);
		$this->set('ctID', $this->ctID);
		$this->set('page_named', $this->page_named);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atPublish WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Publish Question Type allows you to take any form and publish it as a page in your site. </p>
		<br /> 
		<h4>Advanced Use</h4>
		<p>The Publish Question Type is better left as an internal review question only. Checking publish and saving will match attribute handles on a one for one basis.  i.e. - Proforms Question Type "first_name" will search for a page attribute of the same handle "first_name" and save it.</p><br /><p>*this Question Type is still in a beta phase.</p>
		<p>Use cName & cDescription for Page Title and Descriptions respectively. Use block[content][template_name] for posting a question as a content block to the page.</p>
		');
	}
	
	
	public function saveForm($data) {
		$this->load();
		Loader::model('attribute/categories/collection');
		Loader::model('attribute/categories/proforms_item','proforms');
		$db = Loader::db();
		$pfID = $_REQUEST['ProformsItemID'];
		$pf = ProformsItem::getByID($pfID);
		$as = AttributeSet::getByID($_POST['asID']);
		if(!$as){
			$as = AttributeSet::getByID($_POST['question_set']);
		}
		if($as){
			$setAttribs = $as->getAttributeKeys();
			$set_name = $as->getAttributeSetName();
		}
		
		if($pfID > 0){
			$dpage = Page::getByID($this->location);
			$dpath = $dpage->getCollectionPath();
			$ct = CollectionType::getByID($this->ctID);
			
			$from_atts = explode("\n",$this->from_att);
			
			if($this->to_att){
				$to_atts = explode("\n",$this->to_att);
			}
			
			$pname = $pfID;
			
	
			if(is_array($to_atts)){
				foreach($to_atts as $key=>$at_handle){
					$at_handle = trim($at_handle);
					if($at_handle == 'cName'){
						$pname = sprintf($pf->getAttribute(trim($from_atts[$key])));
					}elseif($at_handle == 'cDescription'){
						$set_name = sprintf($pf->getAttribute(trim($from_atts[$key])));
					}
				}
			}
			
			$vars = array('ctID' =>$ct->getCollectionTypeID(), 'cDescription' => $set_name, 'cName' => $pname, 'cDatePublic' => date('Y-m-d'));
			
			$pname = strtolower(str_replace(' ','-',$pname));
			
			if($data['value'] > 0){
				$p = Page::getByPath($dpath.'/'.$pname.'/');
				if($p->getCollectionID() > 0){
					$p->update($vars);
				}else{
					$p = $dpage->add($ct, $vars);	
				}
			}else{
				$data['value'] = 0;
				$p = Page::getByPath($dpath.'/'.$pname.'/');
				if($p->getCollectionID() > 0 && $pfID > 0){
					$p->delete();
				}
	
				if(Package::getByHandle('proevents')->pkgID){
					/* ProEvents Integration - Delete Events */
					Loader::PackageElement('proforms_event','proforms',array('pf'=>$pf,'p'=>$p,'dpath'=>$dpath));
				}
				$setAttribs = null;
			}
	
			$atts_string = '';
			if($p->cID){
				if(is_array($to_atts)){
					foreach($to_atts as $key=>$at_handle){
					    $at_handle = trim($at_handle);
						if($at_handle != 'cName' && $at_handle != 'cDescription'){
							if(substr($at_handle,0,5) == 'block'){
								preg_match_all("/\[([^\]]+)\]/", $at_handle , $matches);
								$btHandle = $matches[1][0];
								$template = $matches[1][1];

								$content = $pf->getAttribute(trim($from_atts[$key]));
								
								//first wipe out any existing blocks of this type in this area
								$blocks = $p->getBlocks('Main');
								foreach($blocks as $b) {
									if($b->getBlockTypeHandle() == trim($btHandle)){
										$b->deleteBlock();
									}
								}
								//now add the new one
								$bt = BlockType::getByHandle(trim($btHandle));
								$datas = array('content' => $content);
								$b = $p->addBlock($bt, 'Main', $datas);
								//if there is a template, set it
								if($template){
									$b->setCustomTemplate($template);
								}
							}else{
								$cak = CollectionAttributeKey::getByHandle(trim($at_handle));
								
								if($cak && 
									(
									$cak->getAttributeType()->getAttributeTypeHandle() == 'image_file' ||
									$cak->getAttributeType()->getAttributeTypeHandle() == 'simple_file' ||
									$cak->getAttributeType()->getAttributeTypeHandle() == 'validated_simple_file' ||
									$cak->getAttributeType()->getAttributeTypeHandle() == 'validated_media'
									)
									
									){
									
									$value = $pf->getAttribute(trim($from_atts[$key]));
									
								}else{
									$value = sprintf($pf->getAttribute(trim($from_atts[$key])));
								}

								if($cak && $cak->getAttributeType()->getAttributeTypeHandle() == 'select'){
									$values = explode("\n",$value);
									foreach($values as $val){
										if($val){
											$id = $db->getOne("SELECT ID FROM atSelectOptions WHERE value LIKE '%$val%' AND akID=?",array($cak->getAttributeKeyID()));
											if(!$id){
												SelectAttributeTypeOption::add($cak,$val);
											}
											$values_array[] = $val;
										}
									}
									$value = $values_array;
								}
				
								$p->setAttribute($cak,$value);
							}
						}
					}
				}elseif($setAttribs){
					foreach($setAttribs as $at){
						$handle = $at->getAttributeKeyHandle();
						$cak = CollectionAttributeKey::getByHandle($handle);
						if(is_object($cak)){
							$pf = ProformsItem::getByID($pfID);
							$value = $pf->getAttribute($handle);
							$p->setAttribute($cak,$value);
						}
						if($handle=='event_multidate' && Package::getByHandle('proevents')->getPackageID() > 0){
							/* if proevents is installed and we have a multidate, will spool events */
							$spool_events = true;
						}
					}

					if($spool_events){
						/* ProEvents Integration - Add Events */
						Loader::PackageElement('proforms_event','proforms',array('pf'=>$pf,'p'=>$p));
					}
				}
			}
		}

		$db->Execute('delete from atPublish where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atPublish (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$data['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atPublish where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {

		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'from_att'=>$data['from_att'],
			'to_att'=>$data['to_att'],
			'location'=>$data['location'],
			'ctID'=>$data['ctID'],
			'page_named'=> ($data['page_named'] > 0) ? 1 : 0
		);
		
		$db=Loader::db();
		$db->Replace('atPublishSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPublishSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}