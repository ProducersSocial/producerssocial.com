<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class HiddenUserAttributeAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $response_message;
	public $from_email;
	public $response_subject;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		
		$u = new User();
		
		if($u->isLoggedIn()){
			
			$ui = UserInfo::getByID($u->uID);

			if($this->akHandle == 'uName'){
				print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$u->uName);
			}elseif($this->akHandle == 'uEmail'){
				print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$ui->getUserEmail());
			}else{
				$usercall = 'getUser'.Loader::helper('text')->camelcase($this->akHandle);
				print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$ui->$usercall());
			}
		}
	}
	
	public function type_form() {
		$this->load();
	}
	
	public function searchForm($list){
		$this->load();
		$list->filterByAttribute($this->attributeKey->getAttributeKeyHandle(), 1, '=');
		return $list;
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (is_object($ak)) {
			//return false;
			$db = Loader::db();
			$row = $db->GetRow('select * from atHiddenUserAttributeSettings where akID = ?', $ak->getAttributeKeyID());
		}
		
		$this->akID = $row['akID'];
		$this->akHandle = $row['akHandle'];

		$this->set('akID', $this->akID);
		$this->set('akHandle', $this->akHandle);
	}
	
	
	public function display(){
		$this->load();
		$val = $this->getValue();
		print $val;
	}
	
	
	public function getDisplayReviewValue(){
		$this->load();
		$val = $this->getValue();
		return $val;
	}
	

	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT value FROM atHiddenUserAttribute WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function help_text(){
		return t('<h4>What is it?</h4><p>Add hidden user attributes into your form for display use or to use with the autoresponder. Only valid for logged in users.</p>');
	}
	
	
	public function saveForm($value) {

		$this->load();

		if($value){
			$db = Loader::db();
			$db->Execute('delete from atHiddenUserAttribute where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atHiddenUserAttribute (avID,value) values (?,?)', array($this->getAttributeValueID(),$value['value']));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atHiddenUserAttribute where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'akHandle'=>$data['akHandle']
		);
		
		$db=Loader::db();
		$db->Replace('atHiddenUserAttributeSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atHiddenUserAttributeSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}