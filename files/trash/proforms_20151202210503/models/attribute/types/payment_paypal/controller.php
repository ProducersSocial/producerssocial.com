<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PaymentPaypalAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
	}
	
	public function display_total(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="paypal_id" class="control-label">'.t('Paypal ID').'</label>';
		print '		<div class="input">';
		print $fm->text('paypal_id',$this->paypal_id,array('placeholder'=>'paypalemail@yoursite.com'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="paypal_sandbox" class="control-label">'.t('Sandbox Mode?').'</label>';
		print '		<div class="input">';
		print $fm->checkbox('paypal_sandbox',1,$this->paypal_sandbox);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="paypal_price_handle" class="control-label">'.t('Total Handle').'</label>';
		print '		<div class="input">';
		print $fm->text('paypal_price_handle',$this->paypal_price_handle,array('placeholder'=>'total_question_handle'));
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="paypal_return_page" class="control-label">'.t('Return Page').'</label>';
		print '		<div class="input">';
		print Loader::helper('form/page_selector')->selectPage('paypal_return_page',$this->paypal_return_page);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPaymentPaypalSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->paypal_id = $row['paypal_id'];
		$this->paypal_price_handle = $row['paypal_price_handle'];
		$this->paypal_return_page = $row['paypal_return_page'];
		$this->paypal_sandbox = $row['paypal_sandbox'];

		$this->set('akID', $this->akID);
		$this->set('paypal_id', $this->paypal_id);
		$this->set('paypal_price_handle', $this->paypal_price_handle);
		$this->set('paypal_return_page', $this->paypal_return_page);
		$this->set('paypal_sandbox', $this->paypal_sandbox);
	}
	
	public function getValue(){
		return $this->hidden_value;
	}

	
	public function saveForm($value) {
		$this->load();
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'paypal_id'=>$data['paypal_id'],
			'paypal_price_handle'=>$data['paypal_price_handle'],
			'paypal_return_page'=>$data['paypal_return_page'],
			'paypal_sandbox'=>$data['paypal_sandbox']
		);
		
		$pkg = Package::getByHandle('proforms');
		$pkg->saveConfig('PAYPAL_TESTMODE', $data['paypal_sandbox']);
		
		$db=Loader::db();
		$db->Replace('atPaymentPaypalSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPaymentPaypalSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}