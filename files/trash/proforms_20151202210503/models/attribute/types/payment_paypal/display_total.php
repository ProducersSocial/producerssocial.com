<?php    
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('proforms_item','proforms');
$fm = Loader::helper('form');
$bt = BlockType::getByHandle('proforms_display');
$ProformsItemID = $_REQUEST['entryID'];
$salt = preg_replace('/[^A-Za-z0-9\-]/', '1', Config::get('SECURITY_TOKEN_ENCRYPTION') );
$ProformsItemID = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($ProformsItemID), MCRYPT_MODE_CBC, md5(md5($salt))), "\0");
$IPN = BASE_URL.DIR_REL.'/tools/packages/proforms/proforms/payment/ipn.php';

if($paypal_sandbox==1){
	$paypal = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
}else{
	$paypal = 'https://www.paypal.com/cgi-bin/webscr';
}
echo '<div class="payment_form">';
if($ProformsItemID){
	$pfo = ProFormsItem::getByID($ProformsItemID);
	if($pfo->asID > 0){
		$question_set = AttributeSet::getByID($pfo->asID);
		$setAttribs = $question_set->getAttributeKeys();
		foreach ($setAttribs as $ak) {
			$value = $pfo->getAttributeValueObject($ak);			
			if($ak->getAttributeKeyHandle() == $paypal_price_handle){
				$ctl = $ak->getController();
				$curency = $ctl->getCurrency();
				$amount = $ak->getController()->getRawValue($value->getAttributeValueID());
				Loader::packageElement('attribute/attribute_form_review','proforms',array('ak'=>$ak,'bt'=>$bt,'aValue'=>$value,'review'=>1));
			}
		}
		echo $fm->hidden('paypal_address',$paypal);
		echo $fm->hidden($this->field('value'),$ProformsItemID);
		echo $fm->hidden('cmd','_xclick');
		echo $fm->hidden('currency_code',strtoupper($curency));
		echo $fm->hidden('amount',$amount);
		echo $fm->hidden('business',$paypal_id);
		echo $fm->hidden('item_name',$question_set->getAttributeSetName());
		echo $fm->hidden('notify_url',$IPN.'?entryID='.$ProformsItemID.'&amount='.$amount);
		echo $fm->hidden('entryID',$ProformsItemID);
		echo $fm->hidden('return',BASE_URL.Loader::helper('navigation')->getLinkToCollection(Page::getByID($paypal_return_page)));
	}
}
echo '</div>';
?>