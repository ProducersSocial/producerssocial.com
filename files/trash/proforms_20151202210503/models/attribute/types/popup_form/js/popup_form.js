var innitializePopupForm = function(data){
		$('#form_title').remove();
		$('.proform_slider').hide();
		if(data.linkType == 'text'){
			$('.proform_slider').before('<a href="javascript:;" class="popup_form">'+data.linkText+'</a>');
		}else{
			$('.proform_slider').before('<div class="ccm-ui"><a href="javascript:;" class="popup_form btn info">'+data.linkText+'</a></div>');
		}
		
		$(document).ready(function(){
			$('.popup_form').bind('click', function(e) {
				e.preventDefault();
				$('.proform_slider').dialog({
					width: 'auto',
					height: 'auto',
					minHeight: 400,
					title: data.linkText,
					dragable: true,
					autoResize: true,
					dialogClass: "popupform_dialog",
					open: function(){
						$('.ui-dialog').find('.popup_form').remove();
						$('.ui-dialog').find('.form_step').width('85%');
						var response_divs = $('.response').html();
						$('.response').remove();
						$('.popupform_dialog').find('.proform_slider').before('<div id="form_title" style="display: none;"></div>');
						$('.popupform_dialog').find('.proform_slider').before('<div class="response">'+response_divs+'</div>');
						$('.ajax_error .alert').bind('close', function (e) {
						  e.preventDefault();
						  $('.ajax_error').hide();
						});
					},
					close: function(){
						$('.popupform_dialog').find('#form_title').remove();
						var response_divs = $('.popupform_dialog').find('.response').html().remove();
						$('.popup_form').after('<div class="response">'+response_divs+'</div>');
					}
				});
				return false;
			});
		});
}