<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PopupFormAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $type;
	public $css_class;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $reviewStatus = 0; // no review needed
	
	public function form(){
		$this->load();
	}
	
	public function closer(){
		$this->load();
		$at = $this->attributeType;
		$jspath = $at->getAttributeTypeFileURL('js/popup_form.js');
		$this->addFooterItem(Loader::helper('html')->javascript($jspath));
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
		$options = array('button'=>t('Button Link'),'text'=>t('Text Link'));
		print '<fieldset>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Link Type').'</label>';
		print '		<div class="input">';
		print $fm->select('type',$options,$this->type);
		print $fm->hidden('force_close',1);
		print '		</div>';
		print '	</div>';
		print '	<div class="clearfix">';
		print '		<label for="akHandle" class="control-label">'.t('Link Text').'</label>';
		print '		<div class="input">';
		print $fm->text('link_text',$this->link_text);
		print '		</div>';
		print '	</div>';
		print '</fieldset>';
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPopupFormSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->type = $row['type'];
		$this->link_text = $row['link_text'];
		$this->css_class = $row['css_class'];

		$this->set('akID', $this->akID);
		$this->set('type', $this->type);
		$this->set('link_text', $this->link_text);
		$this->set('css_class', $this->css_class);
	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Popup Form Question Type is designed to turn any form into a popup dialog form</p><p>The question will hide the form, and a link or button, and then pull the form into a modal window.</p> 
		<br /> 
		<h4>Advanced Use</h4>
		<p>This Question Type uses the "$reviewStatus=0;" and will never be printed out for Form review.  It is merely a front-end esthetic method.</p>
		');
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT dummy FROM atPopupForm WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		$db->Execute('delete from atPopupForm where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atPopupForm (avID,dummy) values (?,?)', array($this->getAttributeValueID(),$value));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){
		$db = Loader::db();
		$db->Execute('delete from atPopupForm where avID = ?', array($this->getAttributeValueID()));
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'type'=>$data['type'],
			'link_text'=>$data['link_text'],
			'css_class'=>$data['css_class']
		);
		
		$db=Loader::db();
		$db->Replace('atPopupFormSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPopupFormSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}