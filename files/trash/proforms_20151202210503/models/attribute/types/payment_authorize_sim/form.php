<?php   defined('C5_EXECUTE') or die("Access Denied.");
// Get the Authorize SDK
Loader::library('anet_php_sdk/AuthorizeNet', 'proforms_extend_authorize');

//$api_login_id = 'YOUR_API_LOGIN_ID';
//$transaction_key = 'YOUR_TRANSACTION_KEY';
$amount = "5.99";//Testing Only
$fp_timestamp = time();
$fp_sequence = "123" . time(); // Enter an invoice or other unique number.
$fingerprint = AuthorizeNetSIM_Form::getFingerprint($authorize_sim_api_login_id,
$authorize_sim_transaction_key, $amount, $fp_sequence, $fp_timestamp);


Loader::model('proforms_item','proforms');
$fm = Loader::helper('form');
$bt = BlockType::getByHandle('proforms_display');
//$ProformsItemID = $_REQUEST['entryID'];
//$IPN = BASE_URL.DIR_REL.'/tools/packages/proforms/proforms/payment/authorize_ipn.php';

if($authorize_sim_sandbox==1){
	$authorize_sim = 'https://secure.authorize.net/gateway/transact.dll';
}else{
	$authorize_sim = 'https://test.authorize.net/gateway/transact.dll';
	//$authorize_sim = 'https://developer.authorize.net/tools/paramdump/index.php';
}

echo '<div class="payment_form">';
echo $amount;
echo $fm->hidden('x_amount',$amount);
echo $fm->hidden('x_login',$authorize_sim_api_login_id);
echo $fm->hidden('x_fp_hash', $fingerprint);
echo $fm->hidden('x_fp_timestamp', $fp_timestamp);
echo $fm->hidden('x_fp_sequence', $fp_sequence);
echo $fm->hidden('x_version', '3.1');
echo $fm->hidden('x_show_form', 'payment_form');
echo $fm->hidden('x_test_request', 'false');
echo $fm->hidden('x_method', 'cc');
echo $fm->hidden('x_relay_response','true');
/*if($ProformsItemID){
	$pfo = ProFormsItem::getByID($ProformsItemID);
	if($pfo->asID > 0){
		$question_set = AttributeSet::getByID($pfo->asID);
		$setAttribs = $question_set->getAttributeKeys();
		echo '<h3>'.t('Payment Confirmation: ').$question_set->getAttributeSetName().'</h3><br/>';
		foreach ($setAttribs as $ak) {
			$value = $pfo->getAttributeValueObject($ak);
			Loader::packageElement('attribute/attribute_form_review','proforms',array('ak'=>$ak,'bt'=>$bt,'aValue'=>$value,'review'=>1));
			
			if($ak->getAttributeKeyHandle() == $paypal_price_handle){
				$ctl = $ak->getController();
				$curency = $ctl->getCurrency();
				$amount = $ak->getController()->getRawValue($value->getAttributeValueID());
			}
		}
		echo $fm->hidden($this->field('value'),$ProformsItemID);
		echo $fm->hidden('cmd','_xclick');
		echo $fm->hidden('currency_code',strtoupper($curency));
		echo $fm->hidden('amount',$amount);
		echo $fm->hidden('business',$paypal_id);
		echo $fm->hidden('item_name',$question_set->getAttributeSetName());
		echo $fm->hidden('notify_url',$IPN.'?entryID='.$ProformsItemID);
		echo $fm->hidden('entryID',$ProformsItemID);
		echo $fm->hidden('return',BASE_URL.Loader::helper('navigation')->getLinkToCollection(Page::getByID($paypal_return_page)));
	}
}*/
echo '</div>';
?>
<script type="text/javascript">
$(document).ready(function(){
	$('form.proform_slider').attr('action','<?php   echo $authorize_sim?>');
	//$('form.proform_slider').attr('method','get');
});
</script>