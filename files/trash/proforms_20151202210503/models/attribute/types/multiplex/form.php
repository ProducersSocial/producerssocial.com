<?php    
defined('C5_EXECUTE') or die("Access Denied.");

$i = 1;
$fm = Loader::helper('form');
$qTypes = array(
	'1'=>'Text',
	'2'=>'Textarea',
	'3'=>'Select',
	'4'=>'Checkbox',
	'5'=>'Radio',
	'6'=>'Address'
);

if(!is_array($answers)){
	$answers = array(1);
}
?>
<style type="text/css">
.address_form {margin-left: 90px;}
.address_form label select,.address_form label input{float: left;display: inline!important;margin-left: 55px;margin-right: 5px;}
.multiplex_set{border: 1px solid gainsboro;padding-top: 20px;}
.add_more_<?php    echo $akID?>{float: right;margin-top: -35px!important;color: white!important;z-index: 300;}
.multiplex_set .ccm-ui label.control-label{width: 80%!important;}
.event-attributes{width: 400px;}
</style>

<?php     if($allow_multiple){ ?>
<br style="clear:both;"/>
<a href="javascript:;" class="add_more_<?php    echo $akID?> btn info"><?php    echo t('Add Another')?></a>
<?php     } ?>
<div id="mpqs_<?php    echo $akID?>" class="multiplex_set">
		<?php    
		foreach($answers as $answer){
			$a = null;
			//var_dump($answer);
			if($i > 1){
				echo '</div>';
				echo '<hr/>';
				echo '<a href="javascript:;" onClick="removeMultiplex($(this));" style="float: right;">[X]</a>';
				echo '<div id="set_'.$akID.'_'.$i.'"" >';
			}else{
				echo '<div id="set_'.$akID.'_'.$i.'"" >';
			}
			if(is_array($questions)){
				foreach($questions as $key=>$question){
					//var_dump($question);
					if($question['mpqtID']==1){
						?>
					  	<div class="clearfix">
							<label style="margin-right: 5px;"><?php     echo $question['mpqTitle']?></label>
							<div class="input" data-set="<?php    echo $i?>">
							<?php    
								echo $fm->text('akID['.$question['akID'].']['.$i.']['.$question['mpqID'].']',$answer[$key]['mpqValue']);
							?>
							</div>
					  	</div>
					  	<?php    
					}elseif($question['mpqtID']==2){
						?>
					  	<div class="clearfix">
							<label style="margin-right: 5px;"><?php     echo $question['mpqTitle']?></label>
							<div class="input" data-set="<?php    echo $i?>">
							<?php    
								echo $fm->textarea('akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]',$answer[$key]['mpqValue']);
							?>
							</div>
					  	</div>
					  	<?php    
					}elseif($question['mpqtID']==3){
						?>
					  	<div class="clearfix">
							<label style="margin-right: 5px;"><?php     echo $question['mpqTitle']?></label>
							<div class="input" data-set="<?php    echo $i?>">
							<?php    
								echo $fm->select('akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]',explode("\r\n",$question['mpqOptions']),$answer[$key]['mpqValue']);
							?>
							</div>
					  	</div>
					  	<?php    
					}elseif($question['mpqtID']==4){
						  if(strpos($question['mpqOptions'], ',') > 0){
						  	$opts = explode(",",$question['mpqOptions']);
						  }else{
						  	$opts = explode("\r\n",$question['mpqOptions']);
						  }
						  $cb = 0;
						  $vals = array();
						  if(is_array($answer)){
						  	foreach($answer as $an){
						  		$vals[] = $an['mpqValue'];
						  	}
						  }
						  foreach($opts as $option){
						  	if($vals && in_array($option,$vals)){
					  	  		$selected = true;
						  	}else{
					  	  		$selected = false;
						  	}
						  	?>
						  	<div class="clearfix">
								<label style="margin-right: 5px;"><?php     if($cb<1){ echo $question['mpqTitle']; }?></label>
								<div class="input" data-set="<?php    echo $i?>">
								<?php    
									echo $fm->checkbox('akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]',$option,$selected);
									echo ' '.$option.' ';
								?>
								</div>
						  	</div>
						  	<?php    
					  		$cb++;
					  }
					}elseif($question['mpqtID']==5){
					  $opts = explode("\r\n",$question['mpqOptions']);
					  ?>
					  <div class="clearfix">
							<label style="margin-right: 5px;"><?php     echo $question['mpqTitle']?></label>
							<div class="input" data-set="<?php    echo $i?>">
								<?php    
								foreach($opts as $option){
									echo $fm->radio('akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]',$option,$answer[$key]['mpqValue']);
									echo ' '.$option.' ';
								}
								?>
					  		</div>
				  		</div>
				  		<?php    
					}elseif($question['mpqtID']==6){
						
						if(!$a){
							$street = $answer[$key]['mpqValue'];
							$city = $answer[($key+1)]['mpqValue'];
							$country = $answer[($key+2)]['mpqValue'];
							$states = $answer[($key+3)]['mpqValue'];
							$postal = $answer[($key+4)]['mpqValue'];
					  	?>
					  	<div class="clearfix">
							<label style="margin-right: 5px;"><?php     echo $question['mpqTitle']?></label>
							<div class="input date_<?php    echo $i?> dateform" data-set="<?php    echo $i?>">
								  	<div class="address_form"  data-id="<?php    echo $question['mpqID']?>">
								  		<input name="<?php    echo 'akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]'?>" value="<?php    echo $street?>" placeholder="<?php    echo t('Street')?>"/>
								  		<input name="<?php    echo 'akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]'?>" value="<?php    echo $city?>" placeholder="<?php    echo t('City')?>"/>
										<input name="akID[<?php    echo $question['akID']?>][<?php    echo $i?>][<?php    echo $question['mpqID']?>][]" class="country_<?php    echo $akID?>_<?php    echo $i?> country" value="<?php    echo $country?>" placeholder="<?php    echo t('Country')?>"/>
										<input name="akID[<?php    echo $question['akID']?>][<?php    echo $i?>][<?php    echo $question['mpqID']?>][]" class="state_<?php    echo $akID?>_<?php    echo $i?> state" value="<?php    echo $states?>"  placeholder="<?php    echo t('State/Province')?>"/>
										<input name="<?php    echo 'akID['.$question['akID'].']['.$i.']['.$question['mpqID'].'][]'?>" value="<?php    echo $postal?>" placeholder="<?php    echo t('Zip/Postal')?>"/>
								  	</div>
								<script type="text/javascript">
									/*<![CDATA[*/
									$(document).ready(function(){
										var dc = $('.country_<?php    echo $akID?>_<?php    echo $i?>').val();
										var ds = $('.state_<?php    echo $akID?>_<?php    echo $i?>').val();
										
										if(!dc){
											dc = 'United States';
											ds = 'Alabama';
										}
										$(".date_<?php    echo $i?>").geoSelector({ 
												data: "<?php    echo $json_path?>" ,
												countrySelector: '.country_<?php    echo $akID?>_<?php    echo $i?>',
												stateSelector: '.state_<?php    echo $akID?>_<?php    echo $i?>',
												defaultCountry: dc,
												defaultState: ds
										});
										$('.date_<?php    echo $i?>').find('script').remove();
									});
									/*]]>*/
								</script>

							</div>
					  	</div>
						<?php    
						$a++;
						}
					}
				}
			}
			$i++;
		}
		echo '</div>';
		echo '<input type="hidden" name="count" id="count" value="'.($i-1).'"/>';
		?>
</div>
<?php     if($allow_multiple){ ?>
<script type="text/javascript">
	/*<![CDATA[*/	
	$(document).ready(function(){innitializeMultiPlex(<?php    echo $akID?>,'<?php    echo $json_path?>');});
	/*]]>*/
</script>
<?php     } ?>