	function removeMultiplex(e){
		if($('.proform_slider').length > 0){
			$('.proform_slider').css('height',($('.proform_slider').height() - e.next('div').height() ));
		}
		e.prev('hr').remove();
		e.next('div').remove();
		e.remove();
	}


	var innitializeMultiPlex = function(APP_ID,JSON){
		$('.add_more_' + APP_ID ).unbind('click tap');
		$('.add_more_' + APP_ID ).bind('click tap',function(e){
			e.preventDefault();
			var template = {};
			template.APP_ID = $('#set_' + APP_ID + '_1').clone();
			var set = $('#count').val();
			var id = template.APP_ID.find('.address_form').attr('data-id');
			var nset = (set * 1)+1;
			$('#count').val(nset);
			template.APP_ID.find('input').each(function(){
				$(this).attr('name',$(this).attr('name').replace('[1]','['+nset+']'));
				if($(this).attr('type')!='checkbox'){
					$(this).val('');
				}
			});
			template.APP_ID.find('textarea').each(function(){
				$(this).attr('name',$(this).attr('name').replace('[1]','['+nset+']')).val('');
			});
			template.APP_ID.find('select').each(function(){
				$(this).attr('name',$(this).attr('name').replace('[1]','['+nset+']')).val('');
			});
			template.APP_ID.find("script").remove();
			template.APP_ID.find('.country').replaceWith('<input name="akID[' + APP_ID + ']['+nset+']['+id+'][]" class="country_' + APP_ID + '_'+nset+'" placeholder="Country"/>');
			template.APP_ID.find('.state').replaceWith('<input name="akID[' + APP_ID + ']['+nset+']['+id+'][]" class="state_' + APP_ID + '_'+nset+'" placeholder="State/Province"/>');
			
			template.APP_ID.text().replace('[1]','['+nset+']');
			$('#mpqs_' + APP_ID ).append('<hr/>');
			$('#mpqs_' + APP_ID ).append('<a href="javascript:;" onClick="removeMultiplex($(this));" style="float: right;">[X]</a>');

			if(template.APP_ID.find(".address_form").length > 0){
				template.APP_ID.find(".address_form").geoSelector({ 
						data: JSON ,
						countrySelector: '.country_' + APP_ID + '_'+nset,
						stateSelector: '.state_' + APP_ID + '_'+nset,
						defaultCountry: 'United States'
				}); 
			}
			$('#mpqs_' + APP_ID ).append(template.APP_ID);
			
			if($('.proform_slider').length > 0){
				$('.proform_slider').css('height',$(this).parent().parent().parent().parent().parent().height());
			}
			return false;
		});
	}
