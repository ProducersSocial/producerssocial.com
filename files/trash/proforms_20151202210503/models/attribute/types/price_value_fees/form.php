<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = loader::helper('form');
$i = 0;
$ak = ProformsItemAttributeKey::getByHandle($modify_question_handle);
?>
<fieldset>
	<div class="clearfix">
		<div class="input">
			<div class="alert-success block-message alert">
			<?php 
			if($fees_optional > 0){
				echo '<span style="float: right;">'.t('Processing Fees').' <i class="icon-white icon-question-sign" id="fees_tooltip" data-toggle="tooltip" data-placement="top" title="';
				if(is_array($fees)){
					foreach($fees as $fee){
						echo $fee['title'].' ';
					}
				}
				echo '"></i></span>';
			}else{
				if(is_array($fees)){
					foreach($fees as $fee){
						echo $fee['title'].' ';
					}
				}
			}
			?>
			<?php  if($fees_optional > 0){ ?>
			<input type="checkbox" name="fees_optional" id="fees_optional" value="1" > <?php    echo t('Yes. Please add processing fees to total.');?><br/>
			<?php  } ?>
			</div>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){

	<?php  if($fees_optional > 0){ ?>
	$('#fees_tooltip').tooltip();
	$('#fees_optional').change(function(){
		doneTyping();
	});
	<?php  } ?>
	
	var typingTimer;                //timer identifier
	var doneTypingInterval = 250;  //time in ms, 5 second for example
	
	//on keyup, start the countdown
	$('#akID\\[<?php echo $ak->getAttributeKeyID()?>\\]\\[value\\]').keyup(function(){
	    clearTimeout(typingTimer);
	    typingTimer = setTimeout(doneTyping, doneTypingInterval);
	});
	
	//on keydown, clear the countdown 
	$('#akID\\[<?php echo $ak->getAttributeKeyID()?>\\]\\[value\\]').keydown(function(){
	    clearTimeout(typingTimer);
	});
	
	//user is "finished typing," do something
	function doneTyping () {
	    //do something
	    <?php  if($fees_optional > 0){ ?>
	    if($('#fees_optional').is(':checked')){
	    <?php  } ?>
	    var tat = $('#akID\\[<?php echo $ak->getAttributeKeyID()?>\\]\\[value\\]').val();
	    <?php 
	    if(is_array($fees)){
			foreach($fees as $fee){
				if($fee['type']=='percent'){
					echo 'tat = parseFloat(tat) + (parseFloat(tat) * ('.$fee['value'].' / 100 ));';
				}else{
					echo 'tat = parseFloat(tat) + parseFloat('.$fee['value'].');';
				}
			}
		}
	    ?>

	    $('#akID\\[<?php echo $ak->getAttributeKeyID()?>\\]\\[value\\]').val(tat.toFixed(2));
	    
	    updatePricing();
	    
	    <?php  if($fees_optional > 0){ ?>
	    }
	    <?php  } ?>
	}
	
	doneTyping();
});
/*]]>*/
</script>