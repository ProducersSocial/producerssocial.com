<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class PriceValueFeesAttributeTypeController extends AttributeTypeController  {
	
	private $akID;

	protected $searchIndexFieldDefinition = 'X NULL';
	
	public function form(){
		$this->load();
	}
	
	public function type_form() {
		$this->load();
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atPriceValueFeesSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$fees = $this->getFees();
		
		$this->akID = $row['akID'];
		$this->fees = $fees;
		$this->modify_question_handle = $row['modify_question_handle'];
		$this->fees_optional = $row['fees_optional'];


		$this->set('akID', $this->akID);
		$this->set('fees',$fees);
		$this->set('modify_question_handle', $this->modify_question_handle);
		$this->set('fees_optional', $this->fees_optional);

	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT value FROM atPriceValueFees WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}

	
	public function saveForm($value) {
		$this->load();
		$db = Loader::db();
		if($value){
			$db = Loader::db();
			$db->Execute('delete from atPriceValueFees where avID = ?', array($this->getAttributeValueID()));
			$db->Execute('insert into atPriceValueFees (avID,value) values (?,?)', array($this->getAttributeValueID(),$value['value']));
		}
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	public function help_text(){
		return t('
		<h4>What is it?</h4>
		<p>The Price Value Fee QuestionType allows you to designate multiple fees by amount and or percent and dynamically modify a designated price value input field.</p>
		');
	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'modify_question_handle'=>$data['modify_question_handle'],
			'fees_optional'=>$data['fees_optional']
		);
		
		$db=Loader::db();
		$db->Replace('atPriceValueFeesSettings', $vals, 'akID', true);
		
		$this->saveFees();
	}
	
	public function saveFees(){
		$db = Loader::db();
		$db->Execute('delete from atPriceValueFeesItems where akID = ?', array($this->attributeKey->getAttributeKeyID()));
		if(is_array($this->request('fee_type'))){
			foreach($this->request('fee_type') as $key=>$fee_type){
				$fee_title = $_REQUEST['fee_title'][$key];
				$fee_amount = $_REQUEST['fee_amount'][$key];
				$db->Execute('insert into atPriceValueFeesItems (akID, title, value, type) values (?, ?, ?, ?)', array($this->attributeKey->getAttributeKeyID(),$fee_title,$fee_amount,$fee_type));
			}
		}
	}
	
	public function getFees(){
		$db = Loader::db();
		$values = $db->getAll("SELECT * FROM atPriceValueFeesItems WHERE akID = ?",array($this->attributeKey->getAttributeKeyID()));
		return $values;
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atPriceValueFeesSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}

}