<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = loader::helper('form');
$i = 0;
?>
<div class="clearfix">
	<label><?php    echo t("Make Fees Optional?")?></label>
	<div class="input">
		<label class="checkbox inline">
		  <input type="checkbox" name="fees_optional" value="1" <?php    if($fees_optional>0){echo 'checked';}?>> <?php    echo t('Yes. Fees are optional and will not be added unless checked.');?><br/>
		</label>
	</div>
</div>
<fieldset>
	<div class="clearfix">
		<label for="client_id" class="control-label"><?php echo t('Price Value to Modify')?></label>
		<div class="input">
				<?php echo $fm->text('modify_question_handle',$modify_question_handle,array('placeholder'=>'price_value question handle'));?>
		</div>
	</div>
<fieldset>
<fieldset>
	<div class="clearfix ">
		<a href="javascript:void(0);" class="btn success ccm-button-v2-right add_fee"><i class="icon-white icon-plus-sign"></i> <?php echo t('Add Fee')?></a>
		<h2><?php echo t('Fees & Charges')?></h2>
		<div id="fees">
			<?php  if($fees){ ?>
			<?php  foreach ($fees as $fee){ $i++; ?>
			<fieldset>
				<div class="clearfix">
					<label for="client_id" class="control-label"><?php echo t('Fee')?></label>
					<div class="input">
							<?php echo $fm->text('fee_title[]',$fee['title'],array('placeholder'=>'Title','class'=>'input-medium'));?>
							<?php echo $fm->text('fee_amount[]',$fee['value'],array('placeholder'=>'value','class'=>'input-small'));?>
							<select name="fee_type[]" class="input-small">
								<option value="percent" <?php  if($fee['type']=='percent'){echo 'selected';}?>><?php echo t('Percent')?></option>
								<option value="value" <?php  if($fee['type']=='value'){echo 'selected';}?>><?php echo t('Amount')?></option>
							</select>
							<a href="javascript:void(0);" onClick="$(this).parent().parent().parent().remove();"><i class="icon-trash"></i></a>
					</div>
				</div>
			<fieldset>
			<?php  } ?>
			<?php   }else{ ?>
			<?php echo t('No fees added yet.')?>
			<?php  } ?>
		</div>
	</div>
<fieldset>
<div style="display: none;" id="fee_template" data-count="<?php echo $i?>">
	<fieldset>
		<div class="clearfix">
			<label for="client_id" class="control-label"><?php echo t('Fee')?></label>
			<div class="input">
					<?php echo $fm->text('title_template',$fee_title,array('placeholder'=>'Title','class'=>'input-medium'));?>
					<?php echo $fm->text('fee_template',$fee_template,array('placeholder'=>'value','class'=>'input-small'));?>
					<select name="type_template" id="type_template" class="input-small">
						<option value="percent"><?php echo t('Percent')?></option>
						<option value="value"><?php echo t('Amount')?></option>
					</select>
					<a href="javascript:void(0);" onClick="$(this).parent().parent().parent().remove();"><i class="icon-trash"></i></a>
			</div>
		</div>
	<fieldset>
</div>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
	$('.add_fee').click(function(){
		var temp = $('#fee_template fieldset').clone();
		temp.find('#fee_template').attr('name','fee_amount[]');
		temp.find('#type_template').attr('name','fee_type[]');
		temp.find('#title_template').attr('name','fee_title[]');
		$('#fees').append(temp);
	});
});
/*]]>*/
</script>