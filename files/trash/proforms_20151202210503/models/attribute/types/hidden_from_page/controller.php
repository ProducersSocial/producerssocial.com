<?php    
defined('C5_EXECUTE') or die("Access Denied.");

class HiddenFromPageAttributeTypeController extends AttributeTypeController  {
	
	private $akID;
	public $hidden_value;

	protected $searchIndexFieldDefinition = 'X NULL';
	public $hideLabel = 1; // no lable needed
	
	public function form(){
		$this->load();
		$fm = Loader::helper('form');
		print $fm->hidden('akID['.$this->attributeKey->getAttributeKeyID().'][value]',$_SERVER['REQUEST_URI']);
	}
	
	public function type_form() {
		$this->load();
		$fm = Loader::helper('form');
	}

	private function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select * from atHiddenFromPageSettings where akID = ?', $this->attributeKey->getAttributeKeyID());
		
		$this->akID = $row['akID'];
		$this->hidden_value = $row['hidden_value'];

		$this->set('akID', $this->akID);
		$this->set('hidden_value', $this->hidden_value);
	}
	
	public function getValue(){
		$db = Loader::db();
		$val = $db->getOne("SELECT page_path FROM atHiddenFromPage WHERE avID = ?",array($this->getAttributeValueID()));
		return $val;
	}
	
	public function display(){
		$this->load();
		$val = $this->getValue();
		if($val){
			print '<a href="'.$val.'">'. $val .'</a>';
		}
	}
	
	public function getDisplayValue(){
		$this->load();
		$val = $this->getValue();
		if($val){
			return '<a href="'.$val.'">'. $val .'</a>';
		}
	}
	
	public function getDisplayReviewValue(){
		$this->load();
		$val = $this->getValue();
		if($val){
			return '<a href="'.$val.'">'. $val .'</a>';
		}
	}

	
	public function saveForm($value) {
		$this->load();
		$db=Loader::db();
		$db->Execute('delete from atHiddenFromPage where avID = ?', array($this->getAttributeValueID()));
		$db->Execute('insert into atHiddenFromPage (avID,page_path) values (?,?)', array($this->getAttributeValueID(),$value['value']));
	}
	
	public function saveValue($value) {
		$this->saveForm($value);
	}
	
	public function deleteValue(){

	}
	
	
	public function saveKey($data) {
		//var_dump($data);exit;
		$vals = array(
			'akID'=>$this->attributeKey->getAttributeKeyID(),
			'hidden_value'=>$data['hidden_value']
		);
		
		$db=Loader::db();
		$db->Replace('atHiddenFromPageSettings', $vals, 'akID', true);
	}

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atHiddenFromPageSettings where akID = ?', array($this->attributeKey->getAttributeKeyID()));
	}
	
	public function help_text(){
		return t('<h4>What is it?</h4><p>Add hidden field to collect the submitted forms page url. Very useful for single page applications that may have multiple ID\'s.</p>');
	}

}