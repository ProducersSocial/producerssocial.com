<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
/**
*
* Returns a ProformsItem Object.
* @package ProformsItems
*
*/
class ProformsItem extends Object {

	public function __construct($id=null){
		if($id){
			return $this->load($id);
		}
	}

	public function setItemReview() {
		$dbs = Loader::db();
		$dbs->Execute('UPDATE ProformsItem SET reviewed = 1 WHERE ProformsItemID = ?', array($this->ProformsItemID));
				
		return ProformsItem::getByID($this->ProformsItemID);
	}

	public function setItemReviewed() {
		$dbs = Loader::db();
		$dbs->Execute('UPDATE ProformsItem SET reviewed = 0 WHERE ProformsItemID = ?', array($this->ProformsItemID));
				
		return ProformsItem::getByID($this->ProformsItemID);
	}
	
	public function update($data) {
		extract($data);
		
		if(empty($ObjectDate)) {
			$dt = Loader::helper('date');
			$ObjectDate = $dt->getLocalDateTime();
		}
		$path = BASE_URL.DIR_REL ;
		$dbs = Loader::db();
		$dbs->Execute('UPDATE ProformsItem SET ProformsItemDate = ? WHERE ProformsItemID = ?', array($ObjectName, $this->ProformsItemID));
				
		return ProformsItem::getByID($this->ProformsItemID);
	}
	
	public function add($asID=null) {
		
		$dt = Loader::helper('date');
		$ObjectDate = $dt->getLocalDateTime();

		
		$v = array('ProformsItemDate' => $ObjectDate,'asID'=>$asID);
		
		$dbs = Loader::db();
		$dbs->Execute('INSERT INTO ProformsItem (ProformsItemDate,asID) values (?,?)', $v);
		$id = $dbs->Insert_ID();
			
		return ProformsItem::getByID($id);
	}
	
	public function duplicate($data) {
		$dt = Loader::helper('date');
		
		$ObjectDate = $dt->getLocalDateTime();
		
		$path = BASE_URL.DIR_REL ;
		$v = array('ProformsItemDate' => $ObjectDate,'siteOwner'=>$path);

		$dbs = Loader::db();
		$dbs->Execute('INSERT INTO ProformsItem (ProformsItemDate) values (?)', $v);
		$id = $dbs->Insert_ID();
		
		$v = array($this->getProformsItemID());
		$q = "select akID, avID from ProformsItemAttributeValues where ProformsItemID = ?";
		$r = $dbs->query($q, $v);
		while ($row = $r->fetchRow()) {
			$v2 = array($row['akID'], $row['avID'], $id);
			$dbs->query("insert into ProformsItemAttributeValues (akID, avID, ProformsItemID) values (?, ?, ?)", $v2);
		}
		return ProformsItem::getByID($id);
	}
	
	public function delete() {
		$dbs = Loader::db();
		$r = $dbs->Execute('SELECT avID, akID FROM ProformsItemAttributeValues WHERE ProformsItemID = ?', array($this->ProformsItemID));
		while ($row = $r->FetchRow()) {
			$noak = ProformsItemAttributeKey::getByID($row['akID']);
			$noav = $this->getAttributeValueObject($noak);
			if (is_object($noav)) {
				$noav->delete();
			}
		}
		$dbs->Execute('DELETE FROM ProformsItem WHERE ProformsItemID = ?', array($this->ProformsItemID));
		$dbs->Execute('DELETE FROM ProformsItemSearchIndexAttributes WHERE ProformsItemID = ?', array($this->ProformsItemID));
	}
	
	public function load($id) {
		$dbs = Loader::db();

		$row = $dbs->GetRow('SELECT * FROM ProformsItem WHERE ProformsItemID = ?', array($id));

		if ($row['ProformsItemID']) { 
			$this->setPropertiesFromArray($row);
			return true;
		} else {
			return false;
		}
	}
	
	public static function getByID($id) {
		$no = new ProformsItem($id);
		if ($no) {
			return $no;
		}
	}
	
	
	public function getProformsItemID() { return $this->ProformsItemID; }
	
	public function getProformsItemDate() { return $this->ProformsItemDate; }
	
	public function getAttributeSetID() { return $this->asID; }
		
	public function setAttribute($ak, $value) {
		Loader::model('attribute/categories/proforms_item', 'proforms');
		if (!is_object($ak)) {
			$ak = ProformsItemAttributeKey::getByHandle($ak);
		}
		$ak->setAttribute($this, $value);
		$this->reindex();
	}
	
	public function clearAttribute($ak){
		$this->setAttribute($ak,null);
	}

	public function reindex() {	
		Cache::disableLocalCache();
		
		$searchableAttributes = array('ProformsItemID' => $this->getProformsItemID());
		
		Loader::model('attribute/categories/proforms_item', 'proforms');			
		$attribs = ProformsItemAttributeKey::getAttributes($this->getProformsItemID(), 'getSearchIndexValue');

		$dbs = Loader::db();
		$dbs->Execute('DELETE FROM ProformsItemSearchIndexAttributes WHERE ProformsItemID = ?', array($this->getProformsItemID()));
		$rs = $dbs->Execute('SELECT * FROM ProformsItemSearchIndexAttributes WHERE ProformsItemID = -1');
		
		AttributeKey::reindex('ProformsItemSearchIndexAttributes', $searchableAttributes, $attribs, $rs);
		
		Cache::enableLocalCache();
	}
	

	public function getAttribute($ak, $displayMode = false) {
		Loader::model('attribute/categories/proforms_item', 'proforms');
		if (!is_object($ak)) {
			$ak = ProformsItemAttributeKey::getByHandle($ak);
		}
		if (is_object($ak)) {
			$av = $this->getAttributeValueObject($ak);
			if (is_object($av)) {
				return $av->getValue($displayMode);
			}
		}
	}
	
	public function getAttributeValueObject($ak, $createIfNotFound = false) {
		$dbs = Loader::db();
		$av = false;
		if(is_object($ak)){
			$v = array($this->getProformsItemID(), $ak->getAttributeKeyID());
			$avID = $dbs->GetOne("SELECT avID FROM ProformsItemAttributeValues WHERE ProformsItemID = ? and akID = ?", $v);
			if ($avID > 0) {
				$av = ProformsItemAttributeValue::getByID($avID);
				if (is_object($av)) {
					$av->setProformsItem($this);
					$av->setAttributeKey($ak);
				}
			}
			
			if ($createIfNotFound) {
				if (!is_object($av)) {
					$this->load($_REQUEST['ProformsItemID']);
					$av = $ak->addAttributeValue();
					$dbs = Loader::db();
					$v = array($this->getProformsItemID(), $ak->getAttributeKeyID(), $av->getAttributeValueID());
					$dbs->Replace('ProformsItemAttributeValues', array(
						'ProformsItemID' => $this->getProformsItemID(), 
						'akID' => $ak->getAttributeKeyID(), 
						'avID' => $av->getAttributeValueID()
					), array('ProformsItemID', 'akID'));
				}
			}
			
			return $av;
		}
	}
	
}