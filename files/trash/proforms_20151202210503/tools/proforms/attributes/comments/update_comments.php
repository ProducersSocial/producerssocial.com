<?php   
defined('C5_EXECUTE') or die("Access Denied.");

$db = Loader::db();
$posts = array();
//var_dump($_REQUEST['avID']);
$val = $db->execute("SELECT * FROM atCommentsPost WHERE avID=?",array($_REQUEST['avID']));
while($row = $val->fetchrow()){
	$posts[] = $row;
}

$u = new User();

if(is_array($posts)){
	foreach($posts as $post){
		$ui = UserInfo::getByID($post['uID']);
		$name = $ui->getUserFirstName().' '.$ui->getUserLastName();

		print '<tr>';
		print '	<td>';
		print '     <h6>'.$name.'</h6>';
		if($u->uID == $post['uID']){
		print '     <a href="javascript:removeComment('.$post['uID'].',\''.$post['date'].'\');" style="float:right" class="icon delete"></a>';
		}
		print '		<i style="font-size: 9px;">'.date('M d, Y @ g:ia',strtotime($post['date'])).'</i><br/>';
		print '		'.$post['comment'];
		print '	</td>';
		print '</tr>';

	}
}
