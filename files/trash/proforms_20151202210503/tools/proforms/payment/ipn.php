<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
		// STEP 1: Read POST data
		 
		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
		     $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		} 
		foreach ($myPost as $key => $value) {        
		   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
		        $value = urlencode(stripslashes($value)); 
		   } else {
		        $value = urlencode($value);
		   }
		   if($key=='entryID'){
			   $ProformsItemID = $value;
		   }
		   if($key=='txn_id'){
			   $txn_id = $value;
		   }
		   $req .= "&$key=$value";
		}
		
		$pkg = Package::getByHandle('proforms');
		$test_mode = $pkg->config('PAYPAL_TESTMODE');
		
		if($test_mode){
			$pp_address = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		}else{
			$pp_address = 'https://www.paypal.com/cgi-bin/webscr';
		}
		 
		 
		// STEP 2: Post IPN data back to paypal to validate
		 
		$ch = curl_init($pp_address);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		
		
		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		if( !($res = curl_exec($ch)) ) {
		    // error_log("Got " . curl_error($ch) . " when processing IPN data");
		    curl_close($ch);
		    exit;
		}
		curl_close($ch);

		if (strcmp ($res, "VERIFIED") == 0) {
			// check the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
			if(!$ProformsItemID){
				$ProformsItemID = $_REQUEST['entryID'];
			}
			Loader::model('proforms_item','proforms');
			$pfo = ProFormsItem::getByID($ProformsItemID);
			$question_set = $pfo->asID;
			$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
			foreach ($setAttribs as $ak) {
				if($ak->getAttributeType()->getAttributeTypeHandle() == 'price_total'){
					$value = $pfo->getAttributeValueObject($ak);
					$ctl = $ak->getController();
                    $amount = $ak->getController()->getRawValue($value->getAttributeValueID());
					$amount = money_format('%(#4n',$amount);
					
					if($_REQUEST['due_now']){
    					$amount = money_format('%(#4n',$_REQUEST['due_now']);
					}
					
					//Log::addEntry($amount);
                    
                    $ctl->savePayment($value->getAttributeValueID(),$amount,null,$txn_id);
                    
					Events::fire('proforms_item_payment',$pfo);
				}
			}
			/*
			$payer_email = $_POST['confirm_email'];
			$from_mail = 'sales@'.substr(BASE_URL,7);
			$ml = Loader::helper('mail');
			$ml->from($from_mail);
			$ml->to($payer_email);
			$ml->setSubject('Your Paypal Payment to '.substr(BASE_URL,7));
			$ml->setBody('Your Order has been processed!');
			$ml->sendMail();
			*/
		}
		else if (strcmp ($res, "INVALID") == 0) {
		// log for manual investigation

		}