<?php    
defined('C5_EXECUTE') or die("Access Denied.");

//Log::addEntry(json_encode($_REQUEST));

Loader::library('gateways/wepay','proforms');
 
if (!empty($_REQUEST['checkout_id'])) {$thecheckoutid = $_REQUEST['checkout_id'];}

if(!empty($_REQUEST['reference_id'])) {
	$refs = explode('.',$_REQUEST['reference_id']);
	$orderID = $refs[0];
	$akID = $refs[1];
}else{
	//echo 'ERROR: no reference ID passed!';
	Log::addEntry('ERROR: no reference ID passed!');
	exit;
}

Loader::model('proforms_item','proforms');
Loader::model('attribute/categories/proforms_item','proforms');

$ak = ProformsItemAttributeKey::getByID($akID);
$ctl = $ak->getController();
$ctl->form();
 
$client_id = $ctl->client_id;
$client_secret = $ctl->client_secret;
$access_token = $ctl->access_token;
$account_id = $ctl->account_id;
$handle = $ctl->wepay_price_handle;

$proformsItem = ProformsItem::getByID($orderID);

/*
Loader::model('campaigns','campaigns');
$campaign = Campaign::getByID($proformsItem->getAttribute('associated_campaign'));
$ui = UserInfo::getByID($campaign->getAttribute('associated_user'));
$user_account_token = $ui->getUserWepayToken();
*/
/**
* Initialize the WePay SDK object
*/
Wepay::useStaging($client_id, $client_secret);
$wepay = new WePay($user_account_token);
 
/**
* Make the API request to get the checkout_uri
*
*/
try {
$checkout = $wepay->request('checkout', array(
'checkout_id' => $thecheckoutid, // ID of the account that you want the money to go to
)
);
} catch (WePayException $e) { // if the API call returns an error, get the error message for display later
$error = $e->getMessage();
//echo $error;
Log::addEntry($error);
}
///some things you might want to use. Delete this stuff otherwise///
/*
print '<br /><br />';
print $checkout->short_description;
print '<br /><br />';
print $checkout->checkout_id;
print '<br /><br />';
print $checkout->reference_id;
print '<br /><br />';
print $checkout->gross;
print '<br /><br />';
print $checkout->payer_name;
*/
////stop deleteing here///
 
	if ($checkout->state == "captured") {
	///do something here
			$ak = ProformsItemAttributeKey::getByHandle($handle);
			$value = $proformsItem->getAttributeValueObject($ak);
			$db = Loader::db();
			
			$amount = money_format('%(#4n',$checkout->gross);
			
			Log::addEntry('SUCCESS: Logged captured ammount: '.$amount);
	
			//$db->Execute("DELETE FROM atPriceTotalPayments WHERE avID = ?",array($value->getAttributeValueID()));
			$db->Execute("INSERT INTO atPriceTotalPayments (avID,akID,amount,payment_date,transactionID) VALUES(?,?,?,?,?)",array($value->getAttributeValueID(),$akID,$amount,date('Y-m-d'),$checkout->checkout_id));
			Events::fire('proforms_item_payment',$proformsItem);
			
        } elseif ($checkout->state == "authorized") {
	///do something here
			/*
			$ak = ProformsItemAttributeKey::getByHandle($handle);
			$value = $proformsItem->getAttributeValueObject($ak);
			$db = Loader::db();
			
			$amount = money_format('%(#4n',$checkout->gross);
			
			Log::addEntry($amount);
	
			//$db->Execute("DELETE FROM atPriceTotalPayments WHERE avID = ?",array($value->getAttributeValueID()));
			$db->Execute("INSERT INTO atPriceTotalPayments (avID,akID,amount,payment_date,transactionID) VALUES(?,,??,?,?)",array($value->getAttributeValueID(),$akID,$amount,date('Y-m-d'),$checkout->checkout_id));
			Events::fire('proforms_item_payment',$proformsItem);
			*/
			
        } elseif ($checkout->state == "cancelled") {
	///do something here
        } elseif ($checkout->state == "refunded") {
	///do something here
        } elseif ($checkout->state == "expired") {
	///do something here
        }