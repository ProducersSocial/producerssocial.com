<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$question_set = $_REQUEST['questionSetID'];
$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
?>
<ul class="sortable droppable">
<?php   
foreach ($setAttribs as $ak) {
	$cnt = $ak->getController();
	$count += 1;
	if((!method_exists($cnt,'closer') 
	|| strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'ultiplex') > 0
	&& strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'idden_') < 1
	&& strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'sociate_') < 1
	)
	&& $ak->getReviewStatus() > 0
	){
		$class = '';
	}elseif(strpos($ak->getAttributeType()->getAttributeTypeHandle(), '_display') > 0 || strpos($ak->getAttributeType()->getAttributeTypeHandle(), 'idden_') > 0){
		$class = 'hidden_method';
	}else{
		$class = 'closer_method';
	}
	?>
	<li data-key="<?php   echo $ak->getAttributeKeyID()?>" data-type="<?php   echo $ak->getAttributeType()->getAttributeTypeID()?>">
    	<div class="clearfix">
    		<div class="input simple">
    			<div class="proform-attributes <?php    echo $class;?>">
    				<i class="icon-trash delete pull-right"></i>
    				<i class="icon-edit edit pull-right"></i>
    				<i class="icon-move move pull-right"></i>
    				<img class="ccm-attribute-icon" src="<?php    echo $ak->getAttributeKeyIconSRC()?>" width="16" height="16" /><?php    echo $ak->getAttributeKeyName()?>
    			</div>
    		</div>
    	</div>
	</li>
	<?php   
}
?>
</ul>