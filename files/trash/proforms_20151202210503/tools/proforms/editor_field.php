<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$fm = Loader::helper('form');
Loader::model('proforms_item','proforms');
Loader::model('attribute/categories/proforms_item','proforms');
$mode = 'add';
$atID = $_REQUEST['atID'];
$questionSetID = $_REQUEST['questionSetID'];
$order = $_REQUEST['order'];
$category = AttributeKeyCategory::getByHandle('proforms_item');
$type = AttributeType::getByID($atID);
$data = array('category' => $category, 'type' => $type,'questionSetID'=>$questionSetID);
$akID = $_REQUEST['akID'];
if($type){
	$cnt = $type->getController();
	if(method_exists($cnt,'help_text')){
		$help = $cnt->help_text();
	}else{
		$help = false;
	}
}
if($akID){
	$key = ProformsItemAttributeKey::getByID($akID);
	$data['key'] = $key;
	$data['ajax'] = true;
	$data['help'] = $help;
	$data['questionSetID'] = $questionSetID;
	$data['order'] = $order;
	$data['atID'] = $atID;
	$mode = 'edit';
}else{
	$data['help'] = $help;
}
?>
<style type="text/css">
#diolog_content{margin: 0!important;padding: 0!important;}
#diolog_content .ccm-pane-body{max-height: 500px;overflow-y: scroll;}
#diolog_content .ccm-ui form{margin: 0!important;}
#diolog_content .popover.bottom {left: 390px!important;width: 250px;}
#diolog_content .ccm-ui .popover.bottom .arrow {top: -6px;left: 95%;}
#diolog_content #ccm-page-help {margin-top: 4px;margin-right: 12px;}
</style>
<?php   
//load up type_form.js if exists
$contents1 = Loader::helper('file')->getContents($type->getAttributeTypeFileURL('type_form.js'));
if(!$contents1 && $type->getAttributeTypeFileURL('type_form.js')){
	$contents1 = Loader::helper('file')->getContents(BASE_URL.$type->getAttributeTypeFileURL('type_form.js'));
}
if($contents1 && strpos($contents1,'$(') > 0 && strpos($contents,'CCM_DISPATCHER_FILENAME') < 1){
	echo '<script type="text/javascript">'.$contents1.'</script>';
}
//load up type_form.css if exists
$contents2 = Loader::helper('file')->getContents($type->getAttributeTypeFileURL('type_form.css'));
if(!$contents2 && $type->getAttributeTypeFileURL('type_form.css')){
	$contents2 = Loader::helper('file')->getContents(BASE_URL.$type->getAttributeTypeFileURL('type_form.css'));
}
if($contents2 && strpos($contents2,'{') > 0 && strpos($contents,'CCM_DISPATCHER_FILENAME') < 1){
	echo '<style type="text/css">'.$contents2.'</style>';
}
?>
<?php    if (isset($type)) { ?>
	<div class="ccm-ui">
		<div class="ajax_error" style="display: none; clear:both;">
			<div class="alert alert-danger">
			  	<button type="button" class="close" data-dismiss="alert">&times;</button>
			  	<h4><?php    echo t('There was a problem with your form submission:'); ?></h4>
				<div class="error_message"></div>
			</div>
		</div>

		<form method="post" action="<?php    echo BASE_URL.DIR_REL;?>/index.php/dashboard/proforms/attributes/<?php   echo $mode?>/" id="ccm-attribute-type-form">
		
			<?php    Loader::packageElement("attribute/type_form_required", "proforms",$data)	; ?>
	
		</form>	
	</div>
<?php    } ?>
<script type="text/javascript">
	//$(document).ready(function(){
		$('.ccm-icon-help').popover({html: true,content: $('#ccm-page-help-content').html(),placement: 'bottom'});
		
		$('#ccm-attribute-type-form').submit(function(e){	
			e.preventDefault();
			$('.ajax_error').hide();
			var url = '<?php  echo BASE_URL.DIR_REL?>/index.php/dashboard/proforms/attributes/<?php   echo $mode?>/';
			if(typeof tinyMCE != 'undefined'){
				tinyMCE.triggerSave();
			}
			var form = $('#ccm-attribute-type-form').serialize();
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: form,
				success: function(response){
    				if(!response.success){
    					var message = '<ul>';
    					$.each(response,function(key,r){
    						message += '<li class="ajax_response">'+r+'</li>';
    					});
    					message += '</ul>';
    					$('.error_message').html(message);
    					$('.ajax_error').show();
    				}else{
    					$( ".ui-dialog-content" ).dialog( "close" );
    				}
				},
				error: function(ts) { console.log(ts.responseText) }
			});
		});
		$('.ajax_error .alert').bind('close', function (e) {
		  e.preventDefault();
		  $('.ajax_error').hide();
		});

		ccm_attributeTypeAddressCountries = function(obj) {
			if (!obj) {
				var obj = $("input[name=akHasCustomCountries][checked=checked]");
			}
			if (obj.attr('value') == 1) {
				$("#akCustomCountries").attr('disabled' , false);
			} else {
				$("#akCustomCountries").attr('disabled' , true);
				$("#akCustomCountries option").attr('selected', true);
			}
		}
	//});
</script>