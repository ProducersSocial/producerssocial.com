<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

$fID = $_REQUEST['fID'];
$vID = $_REQUEST['vID'];
$ext = $_REQUEST['ext'];
if($vID){
	$fID = $vID;
}
$f = File::getByID($fID);
$path = $f->getDownloadURL();

if($vID){
	if($ext == 'mov'){
		echo '<embed src="'.$path.'" width="620" height="380" autoplay="true">';
	}else{
	?>
	<video width="620" height="380" controls>
	  <source src="<?php   echo $path?>" type="video/<?php   echo $ext?>">
	  <?php   echo t('Your browser does not support the video tag.')?>
	</video>
	<?php   
	}
}else{
	$width = $f->getAttributeList()->getAttribute('width');
	if($width > 800){
		$width = 800;
	}
	echo '<img src="'.$path.'" alt="preview" width="'.$width.'" />';
}
