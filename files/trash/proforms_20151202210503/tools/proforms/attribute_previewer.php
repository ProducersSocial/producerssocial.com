<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
$form = Loader::helper('form');
Loader::model('proforms_item','proforms');
Loader::model('attribute/categories/proforms_item','proforms');
$ak = AttributeKey::getInstanceByID($_REQUEST['akID']);
if($_REQUEST['avID'] != ''){
	$db = Loader::db();
	$pfID = $db->getOne("SELECT ProformsItemID FROM ProformsItemAttributeValues WHERE avID = ?",array($_REQUEST['avID']));
	if($pfID){
		$proformsItem = ProformsItem::getByID($pfID);
		$val = $proformsItem->getAttribute($ak);
	}
}else{
	$val = null;
}
?>
<style type="text/css">
.strikeout{text-decoration: line-through!important; color: #ff7171;}
</style>
<?php   
if($ak->getAttributeType()->getAttributeTypeHandle() == 'agreement'){
	if($val['agreement']){
		$document = $val['agreement'];
	}else{
		$cnt = $ak->getController();
		$cnt->type_form();
		$document = $cnt->getAgreementText();
		
	}
	$document = str_replace("[STRIKETHROUGH]", '<div class="editable_text">', $document);
	$document = str_replace("[/STRIKETHROUGH]", '</div>', $document);
	echo $document;
}