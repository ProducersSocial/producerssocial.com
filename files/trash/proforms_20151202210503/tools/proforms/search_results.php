<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

$cnt = Loader::controller('/dashboard/proforms/search');
$ProformsItemList = $cnt->getRequestedSearchResults();

$proforms_entry = $ProformsItemList->getPage();
$pagination = $ProformsItemList->getPagination();


Loader::packageElement('dashboard/proforms/search/search_results', 'proforms', array('proforms_entry' => $proforms_entry, 'ProformsItemList' => $ProformsItemList, 'pagination' => $pagination));