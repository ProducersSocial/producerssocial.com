<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model("attribute/set");		
Loader::model('attribute/categories/proforms_item','proforms');
$searchFieldAttributes = ProformsItemAttributeKey::getList();
echo '<ul>';
foreach ($searchFieldAttributes as $ak) { 
	echo '"'.$ak->getAttributeKeyName() .'" = ' . $ak->getAttributeKeyHandle() .'<br/>';
}
echo '</ul>';