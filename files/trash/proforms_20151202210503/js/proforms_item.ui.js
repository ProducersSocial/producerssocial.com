ccm_setupProformsItemSearch = function() {
	$("#ccm-proforms-item-list-cb-all").click(function() {
		if ($(this).is(':checked')) {
			$('.ccm-list-record td.ccm-proforms-item-list-cb input[type=checkbox]').attr('checked', true);
			$("#ccm-proforms-item-list-multiple-operations").attr('disabled', false);
		} else {
			$('.ccm-list-record td.ccm-proforms-item-list-cb input[type=checkbox]').attr('checked', false);
			$("#ccm-proforms-item-list-multiple-operations").attr('disabled', true);
		}
	});
	$(".ccm-list-record td.ccm-proforms-item-list-cb input[type=checkbox]").click(function(e) {
		if ($(".ccm-list-record td.ccm-proforms-item-list-cb input[type=checkbox]:checked").length > 0) {
			$("#ccm-proforms-item-list-multiple-operations").attr('disabled', false);
		} else {
			$("#ccm-proforms-item-list-multiple-operations").attr('disabled', true);
		}
	});
	
	// if we're not in the dashboard, add to the multiple operations select menu

	$("#ccm-proforms-item-list-multiple-operations").change(function() {
		var action = $(this).val();
		switch(action) {
			case 'choose':
				var idstr = '';
				$("td.ccm-proforms-item-list-cb input[type=checkbox]:checked").each(function() {
					ccm_triggerSelectProformsItem($(this).val(), $(this).attr('proforms-item-name'), $(this).attr('proforms-item-email'));
				});
				jQuery.fn.dialog.closeTop();
				break;
			case "proforms_item": 
				oIDstring = '';
				$("td.ccm-proforms-item-list-cb input[type=checkbox]:checked").each(function() {
					oIDstring=oIDstring+'&ProformsItemID[]='+$(this).val();
				});
				jQuery.fn.dialog.open({
					width: 630,
					height: 450,
					modal: false,
					href: '/index.php/tools/packages/proforms/proforms/bulk_properties?' + oIDstring,
					title: ccmi18n.proforms_item				
				});
				break;	
			case "remove": 
				oIDstring = '';
				$("td.ccm-proforms-item-list-cb input[type=checkbox]:checked").each(function() {
					oIDstring=oIDstring+'&ProformsItemID[]='+$(this).val();
				});
				var dir = $("#url").attr('url');
				jQuery.fn.dialog.open({
					width: 340,
					height: 70,
					modal: false,
					href: dir+'/index.php/tools/packages/proforms/proforms/delete_items?' + oIDstring+'&url='+dir,
					title: 'Remove ProForms Entry?'				
				});
				break;		
			case "duplicate": 
				oIDstring = '';
				$("td.ccm-proforms-item-list-cb input[type=checkbox]:checked").each(function() {
					oIDstring=oIDstring+'ProformsItemID[]='+$(this).val()+'&';
				});
				var dir = $("#url").attr('url');
				$(window.location).attr('href', dir+'/index.php/dashboard/proforms/search/duplicate/?'+oIDstring);
				//jQuery.fn.dialog.open({
				//	width: 630,
				//	height: 450,
				//	modal: false,
				//	href: dir+'/index.php/tools/packages/service_planner/service_planner/duplicate_item?' + //oIDstring,
				//	title: 'Duplicate Service'				
				//});
				break;	
			case "gotoProformsItem": 
				oIDstring = '';
				$("td.ccm-proforms-item-list-cb input[type=checkbox]:checked").each(function() {
					oIDstring= $(this).val();
				});
				var dir = $("#url").attr('url');
				$(window.location).attr('href', dir+'/index.php/ProformsItems/ProformsItem/'+oIDstring+'/');
				break;				
		}
		
		$(this).get(0).selectedIndex = 0;
	});

	$("div.ccm-proforms-item-search-advanced-groups-cb input[type=checkbox]").unbind();
	$("div.ccm-proforms-item-search-advanced-groups-cb input[type=checkbox]").click(function() {
		$("#ccm-proforms-item-advanced-search").submit();
	});

}