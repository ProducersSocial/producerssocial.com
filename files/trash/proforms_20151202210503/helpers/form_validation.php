<?php   
defined('C5_EXECUTE') or die("Access Denied.");
class FormValidationHelper {

	public function validate_post($error) {
			$error = Loader::helper('validation/error');
			$jn = Loader::helper('json');
			$vt = Loader::helper('validation/strings');
			$vn = Loader::Helper('validation/numbers');
			$dt = Loader::helper("form/date_time");
			//$er = Loader::helper('validation/error');
			Loader::model('attribute/categories/proforms_item','proforms');
			
			if(!$_REQUEST['no_captcha']){
				$captcha = Loader::helper('validation/captcha');
				$e = $captcha->check();
				if(!$e){
					$error->add(t('Your Captcha Code did not match! - '.$_REQUEST['ccmCaptchaCode']));
				}
			}	
		
			Loader::model('attribute/set');
			$setAttribs = AttributeSet::getByID($_REQUEST['question_set'])->getAttributeKeys();
		
			if(is_array($setAttribs)){
				foreach ($setAttribs as $ak) {
					$cnt = $ak->getController();
					$type = $ak->getAttributeType();
					if($ak->isAttributeKeyRequired()){
						if(
							$type->getAttributeTypeHandle() != 'select' &&
							!$ak->getTypeMultipart() && 
							$type->getAttributeTypeHandle() != 'address'
						){
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['value']){
								$error->add("'".$ak->getAttributeKeyName()."'".t(' is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[value\\]';
							}
						}
						
						if($type->getAttributeTypeHandle() == 'select'){
							foreach($_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID'] as $optval){
								if($optval < 1){
									$error->add("'".$ak->getAttributeKeyName()."'".$optval.t(' is required!'));
									$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[atSelectOptionID\\]1';
								}
							}
						}
						
						if($type->getAttributeTypeHandle() == 'address'){
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['address1']){
								$error->add("'".$ak->getAttributeKeyName()."'".t(' Address1 is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[address1\\]';
							}
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['city']){
								$error->add("'".$ak->getAttributeKeyName()."'".t(' City is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[city\\]';
							}
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['state_province']){
								$error->add("'".$ak->getAttributeKeyName()."'".t(' State/Province is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[state_province\\]';
							}
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['postal_code']){
								$error->add("'".$ak->getAttributeKeyName()."'".t(' Postal Code is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[postal_code\\]';
							}
						}
					
					}
					
					if($ak->isAttributeKeyNoDuplicates()){
						if($_REQUEST['akID'][$ak->getAttributeKeyID()]['value']){
							Loader::model('proforms_item_list','proforms');
							$sl = new ProformsItemList();
							$sl->filterByAttribute($ak->getAttributeKeyHandle(), $_REQUEST['akID'][$ak->getAttributeKeyID()]['value'], '=');
							$sweepslist = $sl->get();
							if(count($sweepslist)>0){
								$error->add(t('Looks like an entry has been submitted for '.$ak->getAttributeKeyName().'!'));
							}
						}
					}
					
					if(method_exists($cnt,'validateForm')){
						if($_REQUEST['akID'][$ak->getAttributeKeyID()]['value']){
							$cnt->validateForm(array("value"=>$_REQUEST['akID'][$ak->getAttributeKeyID()]['value']));
						}elseif($_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID']){
							$cnt->validateForm($_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID']);
						}
		
						if($cnt->error != ''){
							$error->add($cnt->error);
						}
					}
	
				}
			}
			if($errored_fields){
				$this->errored_fields = $errored_fields;
			}
		return $error;
	}

}
?>