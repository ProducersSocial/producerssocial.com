<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
?>
    <?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('ProForms'), t('Search Proforms Item\'s on your site and perform bulk actions on them.'), false, false);?>
	<div class="ccm-pane-body">
	    <?php    Loader::packageElement('dashboard/proforms/search/dashboard', 'proforms')  ?>
	</div>
		
    <div class="ccm-pane-footer">

	</div>