<?php       defined('C5_EXECUTE') or die("Access Denied.");  

	class ProformsDisplayBlockController extends BlockController {
		
		var $pobj;
		var $pl;
		
		protected $btTable = 'btProformsDisplay';
		protected $btInterfaceWidth = "500";
		protected $btInterfaceHeight = "440";
		
		
		public function getBlockTypeDescription() {
			return t("ProForms Display Block.");
		}
		
		public function getBlockTypeName() {
			return t("ProForms Display Block");
		}
	
		
		function getbID() {return $this->bID;}
		
		
		function view() {
			if($this->request('proforms_entry') == 'success'){
				$this->set('message','success here');
				$this->set('success',true);
			}
			if($this->request('entryID') && $this->request('entryID') != ''){
			     /*
                 * for security reasons we decrypt our form ID#
                 */
			    $salt = preg_replace('/[^A-Za-z0-9\-]/', '1', Config::get('SECURITY_TOKEN_ENCRYPTION') );
                $ProformsItemID = str_replace(' ','+',$_REQUEST['entryID']);
                $ProformsItemID = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($ProformsItemID), MCRYPT_MODE_CBC, md5(md5($salt))), "\0");
                
				$this->set('ProformsItemID',$ProformsItemID);
				$this->set('entryID',$_REQUEST['entryID']);
			}
			
			if($this->request('review')){
				$this->set('review',$this->request('review'));
			}
			$this->set('message',$this->get('message'));
			
			if(!$this->question_set){
				$this->question_set = 1;
			}
			
		}	
		

		function save($data) { 
			$data['enable_review'] = ($data['enable_review']==1) ? 1 : 0;
			parent::save($data);
		}	

		
		public function on_page_view() {
		
			if($this->request('amp;method') == 'entry_save'){
				$this->action_entry_save();
				exit;
			}
			
			if($this->post('question_set') && !$this->post('multipart')){
				$this->action_entry_form();
				exit;
			}
			
			if($this->post('multipart')){
				$this->action_entry_multipart_form_validate();
				exit;
			}
			$html = Loader::helper('html');
			$this->addHeaderItem($html->css('ccm.app.css'));
			$this->addHeaderItem($html->css('jquery.ui.css'));
			$this->addHeaderItem($html->javascript('jquery.js'));
			$this->addHeaderItem($html->javascript('jquery.ui.js'));
			$this->addHeaderItem($html->javascript('jquery.form.js'));
			$this->addFooterItem($html->javascript('ccm.app.js'));
			$this->addFooterItem($html->javascript('bootstrap.js'));		
			$this->addFooterItem($html->javascript('jquery.signaturepad.js','proforms'));	
			
			/* uncomment to preload multiplex js */
			
			/*
            $at = AttributeType::getByHandle('multiplex');
			
    		$multiplex_js = $at->getAttributeTypeFileURL('multiplex.js');
    		$path = $at->getAttributeTypeFileURL('js/jQuery.geoselector.js');

    		$this->addFooterItem($html->javascript($multiplex_js));
    		$this->addFooterItem($html->javascript($path));
            */
			
			$h = Loader::helper('lists/states_provinces');
			$provinceJS = "<script type=\"text/javascript\"> var ccm_attributeTypeAddressStatesTextList = '\\\n";
			$all = $h->getAll();
			foreach($all as $country => $countries) {
				foreach($countries as $value => $text) {
					$provinceJS .= str_replace("'","\'", $country . ':' . $value . ':' . $text . "|\\\n");
				}
			}
			$provinceJS .= "'</script>";
			$this->addHeaderItem($provinceJS);
			$this->addHeaderItem($html->javascript('country_state.js','proforms'));
			
		}
		
		
		public function action_entry_form_multipart(){
			global $c;
			$_REQUEST['no_captcha'] = 1;
			$this->validate_post();
			if (!$this->error_post->has()) {
				$nh = Loader::helper('navigation');
				
				Loader::model('proforms_item', 'proforms');
				
				$asID = $_REQUEST['question_set'];
				
				if($this->post('ProformsItemID') && $this->post('ProformsItemID') != 'undefined'){
					$Object = ProformsItem::getByID($this->post('ProformsItemID'));
				}else{
					$Object = ProformsItem::add($asID);
					$_REQUEST['ProformsItemID'] = $Object->getProformsItemID();
				}
                
                /*
                 * for security reasons we encrypt our form ID#
                 */
                $salt = preg_replace('/[^A-Za-z0-9\-]/', '1', Config::get('SECURITY_TOKEN_ENCRYPTION') );
                $encryptedID = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($salt), $Object->getProformsItemID(), MCRYPT_MODE_CBC, md5(md5($salt))));
	
				Loader::model('attribute/set');
				Loader::model('attribute/categories/proforms_item', 'proforms');
				$set = AttributeSet::getByID($asID);
				$setAttribs = $set->getAttributeKeys();
				foreach ($setAttribs as $ak) {
					$ak->saveAttributeForm($Object);
					if($ak->getAttributeType()->getAttributeTypeHandle() == 'email'){
						$user_email = $Object->getAttribute($ak->getAttributeKeyHandle());
					}
				}
		
				$Object->reindex();

				if($this->enable_review > 0 && $this->request('review')){
					$Object->setItemReview();
					$this->externalRedirect($nh->getLinkToCollection($c).'?entryID='.$encryptedID);
				}else{
					$Object->setItemReviewed();

					$this->sendNotice($Object,$set);
					
					if($this->copy_to_user > 0 && $user_email){
						$this->sendUserCopy($Object,$set,$user_email);
					}
					
					Events::fire('proforms_item_entry', $Object);
				}
				
				if($this->page_forward > 0){
					$pf = Page::getByID($this->page_forward);
					if($this->pass_form){
						$pass = '?entryID='.$encryptedID;
					}
					$this->externalRedirect($nh->getLinkToCollection($pf).$pass);
				}elseif($this->url_forward){
					if($this->pass_form){
						$pass = '?entryID='.$encryptedID;
					}
					$this->externalRedirect($this->url_forward.$pass);
				}else{
					$this->externalRedirect($nh->getLinkToCollection($c).'?proforms_entry=success');
				}
			}else{
				$this->set('error',$this->error_post->getList());
			}
		}
		
		public function action_entry_multipart_form_validate(){
			$jh = Loader::helper('json');
			$this->validate_post();
			if ($this->error_post->has()) {
				if($this->errored_fields){
					print $jh->encode(array('error'=>$this->error_post->getList(),'fields'=>$this->errored_fields));
				}else{
					print $jh->encode(array('error'=>$this->error_post->getList()));
				}
			}else{
				print $jh->encode(array('success'=>1));
			}
			exit;
		}
		
		public function action_entry_save(){
			global $c;
			$nh = Loader::helper('navigation');
			
			Loader::model('proforms_item', 'proforms');
			
			$asID = $_REQUEST['question_set'];
			
			if($this->post('ProformsItemID') && $this->post('ProformsItemID') != 'undefined'){
				$Object = ProformsItem::getByID($this->post('ProformsItemID'));
			}else{
				$Object = ProformsItem::add($asID);
			}

			Loader::model('attribute/set');
			Loader::model('attribute/categories/proforms_item', 'proforms');
			$set = AttributeSet::getByID($asID);
			$setAttribs = $set->getAttributeKeys();
			foreach ($setAttribs as $ak) {
				@$ak->saveAttributeForm($Object);
			}
	
			$Object->reindex();
			
			exit;
		}
		
		public function validate_post() {
			$this->error_post = Loader::helper('validation/error');
			$jn = Loader::helper('json');
			$vt = Loader::helper('validation/strings');
			$vn = Loader::Helper('validation/numbers');
			$dt = Loader::helper("form/date_time");
			//$er = Loader::helper('validation/error');
			Loader::model('attribute/categories/proforms_item','proforms');
			
			if(!$_REQUEST['no_captcha']){
				$captcha = Loader::helper('validation/captcha');
				$e = $captcha->check();
				if(!$e){
					$this->error_post->add(t('Your Captcha Code did not match! - '.$_REQUEST['ccmCaptchaCode']));
				}
			}	
		
			Loader::model('attribute/set');
			$setAttribs = AttributeSet::getByID($_REQUEST['question_set'])->getAttributeKeys();
		
			if(is_array($setAttribs)){
				foreach ($setAttribs as $ak) {
					$cnt = $ak->getController();
					$type = $ak->getAttributeType();
					if($ak->isAttributeKeyRequired()){
						if(
							$type->getAttributeTypeHandle() != 'select' &&
							$type->getAttributeTypeHandle() != 'select_values' &&
							$type->getAttributeTypeHandle() != 'price_select' &&
							!$ak->getTypeMultipart() && 
							$type->getAttributeTypeHandle() != 'address' &&
							$type->getAttributeTypeHandle() != 'radio' &&
							$type->getAttributeTypeHandle() != 'simple_file'
						){
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['value']){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".t(' is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[value\\]';
							}
						}
						
						if($type->getAttributeTypeHandle() == 'simple_file'){
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['ajax_send']){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".$optval.t(' is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[new_file\\]';
							}
						}
						
						if($type->getAttributeTypeHandle() == 'select' || $type->getAttributeTypeHandle() == 'select_values' || $type->getAttributeTypeHandle() == 'price_select'){
							$args = $_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID'];
							for($s = 0; $s < count($args); $s++){
								$optval = $args[$s];
								if($optval < 1){
									$this->error_post->add("'".$ak->getAttributeKeyName()."'".$optval.t(' is required!'));
									$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[atSelectOptionID\\]1';
								}
							}
						}
						
						if($type->getAttributeTypeHandle() == 'radio'){
							$args = $_REQUEST['akID'][$ak->getAttributeKeyID()]['atRadioOptionID'];
							for($s = 0; $s < count($args); $s++){
								$optval = $args[$s];
							}
							if($optval < 1){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".$optval.t(' is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[atRadioOptionID\\]1';
							}
						}
						
						if($type->getAttributeTypeHandle() == 'address'){
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['address1']){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".t(' Address1 is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[address1\\]';
							}
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['city']){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".t(' City is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[city\\]';
							}
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['state_province']){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".t(' State/Province is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[state_province\\]';
							}
							if(!$_REQUEST['akID'][$ak->getAttributeKeyID()]['postal_code']){
								$this->error_post->add("'".$ak->getAttributeKeyName()."'".t(' Postal Code is required!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[postal_code\\]';
							}
						}
					
					}
					
					if($ak->isAttributeKeyNoDuplicates()){
						if($_REQUEST['akID'][$ak->getAttributeKeyID()]['value']){
							Loader::model('proforms_item_list','proforms');
							$sl = new ProformsItemList();
							$sl->filterByAttribute($ak->getAttributeKeyHandle(), $_REQUEST['akID'][$ak->getAttributeKeyID()]['value'], '=');
							$sweepslist = $sl->get();
							if(count($sweepslist)>0){
								$this->error_post->add(t('Looks like an entry has been submitted for '.$ak->getAttributeKeyName().'!'));
								$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[value\\]';
							}
						}
					}
					
					if(method_exists($cnt,'validateForm')){


						switch($type->getAttributeTypeHandle()){
							case 'email':
							case 'phone':
							case 'address':
							case 'regex_match':
								if($_REQUEST['akID'][$ak->getAttributeKeyID()]['value'] != ''){
									@$cnt->validateForm($_REQUEST['akID'][$ak->getAttributeKeyID()]['value']);
								}
								break;
								
							case 'select':
							case 'select_values':
							case 'price_select':
								@$cnt->validateForm($_REQUEST['akID'][$ak->getAttributeKeyID()]['atSelectOptionID']);
								break;
								
							default:
								if($_REQUEST['akID'][$ak->getAttributeKeyID()]['value'] != ''){
									@$cnt->validateForm($_REQUEST['akID'][$ak->getAttributeKeyID()]);
								}
								break;
						}

		
						if($cnt->error != ''){
							$this->error_post->add($cnt->error);
							$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[value\\]';
							$errored_fields[] = 'akID\\['.$ak->getAttributeKeyID().'\\]\\[atSelectOptionID\\]';
						}
					}
	
				}
			}
			if($errored_fields){
				$this->errored_fields = $errored_fields;
			}
		}
		

				
		public function sendNotice($Object,$set){
			$mh = Loader::helper('mail');
			
			if($this->notify){
				if($this->from_mail){
					$mh->from($this->from_mail);
				}else{
					$mh->from('proforms@'.substr(BASE_URL,7));
				}
				
				$tos = explode(',',$this->notify);
				foreach($tos as $to){
					$mh->to($to);
				}
				
				if($this->show_answers > 0){
					$mh->addParameter('set', $set);
					$mh->addParameter('response', $Object);
					$mh->load('advanced_notification', 'proforms');
				}else{
					$mh->addParameter('setID', $set->getAttributeSetID());
					$mh->load('basic_notification', 'proforms');
				}
				$mh->setSubject('New Form Entry');
				$mh->sendMail();
			}
		}
		
		public function sendUserCopy($Object,$set,$user_email){
			$mh = Loader::helper('mail');
			if($this->from_mail){
				$mh->from($this->from_mail);
			}else{
				$mh->from('proforms@'.substr(BASE_URL,7));
			}
			$mh->to($user_email);
			
			if($this->show_answers){
				$mh->addParameter('set', $set);
				$mh->addParameter('response', $Object);
				$mh->load('advanced_notification_user', 'proforms');
			}else{
				$mh->addParameter('setID', $set->getAttributeSetID());
				$mh->load('basic_notification_user', 'proforms');
			}
			$mh->setSubject('Your Form Submission');
			$mh->sendMail();
		}

}
?>