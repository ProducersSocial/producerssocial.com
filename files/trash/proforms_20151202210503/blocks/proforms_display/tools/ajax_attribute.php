<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('attribute/categories/proforms_item','proforms');
$akID = $_REQUEST['akID'];
$ak = ProformsItemAttributeKey::getByID($akID);

if(!$ak->isAttributeKeyLabelHidden() && $ak->getReviewStatus() > 0){
?>
<strong><?php    echo $ak->render('label');?> <?php    if($ak->isAttributeKeyRequired()){ echo '<span class="required"> *</span>';} ?></strong>
<?php   
}
?>
<div class="input">
	<div class="proform-attributes">
		<div <?php    if($expand){echo 'class="optional_expand" rel="'.$i.'"';}?> data-handle="<?php   echo $ak->getAttributeKeyHandle()?>">
		<?php   
		echo $ak->render('form');
		?>
		</div>
	</div>
</div>