<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('proforms_item','proforms');
$th = Loader::helper('concrete/urls'); 
$nh = Loader::helper('navigation');
$fm = Loader::helper('form');
$bt = $b->getBlockTypeObject();

global $c;
?>

<h2 id="form_title"><?php   echo $title?></h2>
<?php    if(!$success){ ?>
<form name="proforms_form_<?php   echo $bID?>" class="ccm-ui proform_slider" id="proforms_form_<?php   echo $bID?>" method="post" action="<?php   echo $this->action('entry_form_multipart')?>" enctype="multipart/form-data">
	<div id="form_progress"></div>
	<div class="clearfix"></div>
	<input type="hidden" name="bID" value="<?php   echo $bID?>"/>
	<input type="hidden" name="question_set" value="<?php   echo $question_set?>"/>
	<?php    echo $fm->hidden('multipart',1);?>
	<?php    if($ProformsItemID){ ?>
		<input type="hidden" name="ProformsItemID" id="ProformsItemID" value="<?php   echo $ProformsItemID?>"/>
		<?php    $pfo = ProFormsItem::getByID($ProformsItemID);?>
	<?php    } ?>
	<?php   
	$setAttribs = AttributeSet::getByID($question_set)->getAttributeKeys();
	foreach ($setAttribs as $ak) {
		if($pfo){$value = $pfo->getAttributeValueObject($ak);}
		$count += 1;
		if(($ak->getReviewStatus() > 0 || strpos($ak->getAttributeType()->getAttributeTypeHandle(), '_display') > 0 )){
	
			//load up conditions code
			Loader::packageElement('attribute/conditions','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
			
			if(!$ak->isAttributeKeyConditioned()){ 
				//load up form input
				if($pfo && $pfo->reviewed > 0){$review = 1;}
				if($_REQUEST['edit_entry']){$review = null;}
				Loader::packageElement('attribute/attribute_form','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value,'review'=>$review));
			}else{ 
				//or load up blank for conditional
				echo '<div id="conditional_'.$ak->getAttributeKeyID().'"></div>';
			} 

		}
		if($count == count($setAttribs)){
			if($displayCaptcha){
				Loader::packageElement('attribute/captcha','proforms');
			}else{
				echo $fm->hidden('no_captcha',true);
			}
		}
		echo $ak->render('closer');
	}	
	
	if(!$button_text){
		$button_text = t('Submit');
	}
	?>

	<div class="clearfix"></div>
	<?php  if($enable_review && $review){ ?>
		<div class="ajax_warning_<?php   echo $bID?> ccm-ui">
			<div class="alert alert-warning">
			  	<button type="button" class="close" data-dismiss="alert">&times;</button>
			  	<h4><?php    echo t('Save for later?'); ?></h4>
				<p><?php    echo t('If you would like to save your form and review & submit later, simply copy and save the URL from your web browser\'s address bar above.');?></p>
			</div>
		</div>
	<?php    } ?>
	<div class="proforms_submit_group">
		<?php    if($enable_review && !$review){ ?>
			<?php    echo $fm->hidden('multipart',1);?>
			<?php    echo $fm->hidden('review',1); ?>
			<input type="button" class="btn btn-large primary proforms_submit proforms_submit_<?php   echo $bID?>" name="proforms_submit_<?php   echo $bID?>" value="<?php   echo t('Review Entry')?>" style="float: right;clear: none;"/>
		<?php    }else{ ?>
			<input type="button" class="btn btn-large primary proforms_submit proforms_submit_<?php   echo $bID?>" name="proforms_submit_<?php   echo $bID?>" value="<?php   echo $button_text?>" style="float: right;clear: none;"/>
			<?php    if($review){ ?>
				<a href="<?php   echo $nh->getLinkToCollection($c).'?entryID='.$entryID.'&edit_entry=true'?>" class="btn btn-large info" name="proforms_review_<?php   echo $bID?>" style="float: right;margin-right: 22px;padding: 6px 22px!important;"><?php   echo t('Edit Entry')?></a>
			<?php    } ?>
		<?php    } ?>
	</div>
</form>
<?php   
}
?>
<div class="clearfix"></div>
<br/><br/>
<div class="response">
	<div class="ajax_loader_<?php   echo $bID?> loader_ajax" style="display: none;"><img src="<?php    echo Loader::helper('concrete/urls')->getBlockTypeAssetsURL($bt, 'images/ajax-loader.gif')?>" alt="loading"/> </div>
	
	<div class="ajax_error_<?php   echo $bID?> ajax_error ccm-ui" style="display: <?php    if($error){ echo 'block';}else{echo 'none';}?>; clear:both;">
		<div class="alert alert-danger">
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		  	<h4><?php    echo t('There was a problem with your form submission:'); ?></h4>
			<ul>
			<?php    
			if($error){
				echo '<ul>';
				foreach($error as $item){
					echo '<li>'.$item.'</li>';
				}
				echo '</ul>';
			}
			?>
			</ul>
		</div>
	</div>
	<div class="ajax_success_<?php   echo $bID?> ccm-ui"<?php    if(!$success){ echo 'style="display: none;"';}?>>
		<div class="alert alert-success">
		  	<button type="button" class="close" data-dismiss="alert">&times;</button>
		  	<h4><?php    echo t('Success!'); ?></h4>
		<?php   
		if($thankyouMsg){
			echo $thankyouMsg;
		}else{
			echo t('Your entry has been submitted.  Our team will review your submission!  Thanks Again!');
		}
		?>
		</div>
	</div>
</div>

<script type="text/javascript">
	/*<![CDATA[*/
	var data = {
		ajax: '<?php   echo $this->action('entry_form')?>',
		toolsAjax: "<?php   echo $this->action('entry_multipart_form_validate')?>",
		page_forward: <?php   echo $page_forward?>
		<?php  if($displayCaptcha){ ?>,
		captcha: '<?php echo Loader::helper('concrete/urls')->getToolsURL('proforms/reload_captcha','proforms')?>'
		<?php  } ?>
		<?php  if($page_forward > 0){ $pf = Page::getByID($page_forward); ?>,
		Link: '<?php echo $nh->getLinkToCollection($pf)?>'
		<?php  } ?>
	};

	$(document).ready(function(){
		innitializeForm(<?php   echo $bID?>,data);
		
		$('.ajax_error .alert').bind('close', function (e) {
		  e.preventDefault();
		  $('.ajax_error').hide();
		});
	});
	/*]]>*/
</script>