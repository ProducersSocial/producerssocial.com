<?php    
defined('C5_EXECUTE') or die(_("Access Denied.")); 
	
$fm = Loader::helper('form');	
?> 
<style type="text/css">
.ccm-ui label input, .ccm-ui label textarea, .ccm-ui label select{display: inline-block!important}
.clearfix label{padding-right: 6px!important; width: 250px;}
.ccm-ui label{display: inline-block!important;}
div.ccm-summary-selected-item{display: inline-block!important; width: 220px;}
</style>
<div class="ccm-ui">
	<div id="ccm-formBlockPane-options" class="ccm-formBlockPane">
		<fieldset>
			<div class="clearfix">
				<?php    echo $form->label('title', t('Form Title'))?>
				<div class="input">
					</span><?php    echo $form->text('title', $title, array('style' => 'z-index:2000;' ))?>
				</div>
			</div>
			<div class="clearfix">
				<label for="ccm-form-redirect"><?php    echo t('Choose a Form')?></label>
				<div class="input">
					<div id="ccm-form-redirect-page">
						<select name="question_set" id="question_set">
							<option value=""><?php   echo t('-- choose one --')?></option>
							<?php   
								$category = AttributeKeyCategory::getByHandle('proforms_item');
								$sets = $category->getAttributeSets();
								if(is_array($sets)){
									foreach($sets as $set){
										Loader::model('proforms_question_set','proforms');
										$qs = new ProformsQuestionSet();
										$internal = $qs->QuestionSetIsInternal($set->getAttributeSetID());
										if(!$internal){
											echo '<option value="'.$set->asID.'"';
											if($question_set ==  $set->asID){ echo ' selected';}
											echo '>'.$set->asName.'</option>';
										}
									}
								}
							?>
						</select>
					</div>
					<a href="javascript:;" id="manage_questions"><?php   echo t('Manage Questions')?></a>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('button_text', t('Submit Button Text'))?>
				<div class="input">
					<?php    echo $form->text('button_text', $button_text, array('style' => 'z-index:2000;' ))?>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('enable_review', t('Enable Review'))?>
				<div class="input">
					<?php    echo $form->checkbox('enable_review', 1, $enable_review == 1)?> <?php   echo t(' yes')?><br/>
					<i><?php   echo t('(Form will not be submitted until reviewed.)')?></i>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('thankyouMsg', t('Message to display when completed'))?>
				<div class="input">
					<?php    echo $form->textarea('thankyouMsg', $this->controller->thankyouMsg, array('rows' => 3))?>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('page_forward', t('Or Chose A Page to Forward To.'))?>
				<div class="input">
					<?php    echo Loader::helper('form/page_selector')->selectPage('page_forward', $page_forward)?>
				</div>
            </div>
			<div class="clearfix">
				<?php    echo $form->label('url_forward', t('Or Enter A URL to Forward To.'))?>
				<div class="input">
					<?php    echo $form->text('url_forward', $url_forward, array('style' => 'z-index:2000;' ))?>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('page_forward', t('Pass Form ID?'))?>
				<div class="input">
					<?php    echo $form->checkbox('pass_form', 1, $pass_form == 1)?> <?php   echo t(' yes')?><br/>
					<i><?php   echo t('(Form ID will be passed to the next page)')?></i>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('from_mail', t('\'From\' email address'))?>
				<div class="input">
					<?php    echo $form->text('from_mail', $from_mail, array('style' => 'z-index:2000;' ))?>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('notify', t('Notify me by email when people submit this form'))?>
				<div class="input">
					<div class="input-prepend">
						<label>
						<span class="add-on" style="z-index: 2000">
							<?php    echo $form->checkbox('notifyMeOnSubmission', 1, $notifyMeOnSubmission == 1, array('onclick' => "$('input[name=notify]').focus()"))?>
						</span><?php    echo $form->text('notify', $notify, array('style' => 'z-index:2000;' ))?>
						</label>
					</div>
		
					<span class="help-block"><?php    echo t('(Seperate multiple emails with a comma)')?></span>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('show_answers', t('Send Copy to User'))?>
				<div class="input">
					<?php    echo $form->checkbox('copy_to_user', 1, $copy_to_user == 1)?> <?php   echo t(' yes')?><br/>
					<i><?php   echo t('(send copy of form to user if email question is present)')?></i>
				</div>
			</div>
			<div class="clearfix">
				<?php    echo $form->label('show_answers', t('Advanced Notification'))?>
				<div class="input">
					<?php    echo $form->checkbox('show_answers', 1, $show_answers == 1)?> <?php   echo t(' yes')?>
				</div>
				
				<span class="help-block"><?php    echo t('(Show Questions/Answers in Notification)')?></span>
			</div>
			<div class="clearfix">
				<label><?php    echo t('Solving a <a href="%s" target="_blank">CAPTCHA</a> Required to Post?', 'http://en.wikipedia.org/wiki/Captcha')?></label>
				<div class="input">
					<ul class="inputs-list" id="displayCaptcha">
						<li>
							<label>
								<?php    echo $form->radio('displayCaptcha', 1, (int) $displayCaptcha)?>
								<span><?php    echo t('Yes')?></span>
							</label>
						</li>
						<li>
							<label>
								<?php    echo $form->radio('displayCaptcha', 0, (int) $displayCaptcha)?>
								<span><?php    echo t('No')?></span>
							</label>
						</li>
					</ul>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<script type="text/javascript">
	//$(document).ready(function(){
		$('#manage_questions').bind('click tap',function(){
			var url = '<?php  echo BASE_URL.DIR_REL?>/index.php/dashboard/proforms/attributes/'+ $('#question_set option:selected').val() + '/';
			window.location = url;
		});
	//});
</script>
