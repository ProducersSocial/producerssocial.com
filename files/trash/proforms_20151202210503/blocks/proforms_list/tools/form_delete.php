<?php     defined('C5_EXECUTE') or die("Access Denied."); 
$fm = Loader::helper('form');
$update_url = urldecode($_REQUEST['update_url']);
$ProformsItemID = $_REQUEST['ProformsItemID'];
?>
<div class="ccm-ui">
<p>
<?php   echo t('Are you SURE you want to remove this Form Entry?</p>  <p>This action may not be undone!')?>
</p>
<button href="javascript:;" class="ccm-input-submit btn danger" onClick="location.href='<?php   echo $update_url.'&ProformsItemID='.$ProformsItemID.'&=delete_item=true'?>';"><?php   echo t('Yes, remove this Form Entry')?></button>
</div>