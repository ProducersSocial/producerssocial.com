<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('proforms_item','proforms');
Loader::model('attribute/categories/proforms_item', 'proforms');
$update_url = urldecode($_REQUEST['update_url']);
$ProformsItemID = $_REQUEST['ProformsItemID'];
$review = ($_REQUEST['allow_edit'] > 0) ? 1 : 0;
$bID = $_REQUEST['bID'];
$b = Block::getByID($bID);
$bt = $b->getBlockTypeObject();
$cnt = $b->getController();
$selected_options = explode(',',$cnt->edits);
$pfo = ProFormsItem::getByID($ProformsItemID);

$view = View::getInstance();
?>
<style type="text/css">
.entry_review label {min-width: 80px;float:left;font-weight: bold;}
.entry_review .required{float: left;}
.entry_review .input {margin-left: 22px; float:left; clear: both;}
</style>
<form class="ccm-ui entry_review proform_slider" name="proforms_form_<?php   echo $bID?>" id="proforms_form_<?php   echo $bID?>" method="post" action="<?php   echo $update_url?>" enctype="multipart/form-data">
<input type="hidden" class="ccm-ui" name="ProformsItemID" id="ProformsItemID" value="<?php   echo $ProformsItemID?>"/>
<?php   
if($selected_options[0] != ''){
	foreach($selected_options as $akID){
	
		$ak = ProformsItemAttributeKey::getByID($akID);
		if($ak->akID > 0){
			if($pfo){$value = $pfo->getAttributeValueObject($ak);}
		
			if(!$ak->isAttributeKeyConditioned()){ 
				//load up form input
				Loader::packageElement('attribute/attribute_form','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value,'review'=>0));
			}else{ 
				//or load up blank for conditional
				//load up conditions code
				Loader::packageElement('attribute/conditions','proforms',array('ak'=>$ak,'bt'=>$bt,'value'=>$value));
				echo '<div id="conditional_'.$ak->getAttributeKeyID().'"></div>';
			} 
			
			echo $ak->render('closer');
			echo '<br/>';
		}
	}
}
?>
<input type="submit" name="submit" value="<?php   echo t('Update Entry')?>" class="btn primary"/>
</form>