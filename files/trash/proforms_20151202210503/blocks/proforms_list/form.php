<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<?php    
$c = Page::getCurrentPage(); 
$bt = BlockType::getByHandle('proforms_list');
Loader::model('attribute/categories/proforms_item','proforms');
?>
<style type="text/css">
#ccm-search-field-base { display: block!important; }
.ccm-selected-field-content{padding: 3px 8px;}
</style>
<input type="hidden" name="formsListToolsDir" value="<?php    echo $uh->getBlockTypeToolsURL($bt)?>/" />
<ul id="ccm-blockEditPane-tabs" class="ccm-dialog-tabs">
	<li class="ccm-nav-active"><a id="ccm-blockEditPane-tab-options" href="javascript:void(0);"><?php    echo t('Layout Options') ?></a></li>
	<li class=""><a id="ccm-blockEditPane-tab-filters"  href="javascript:void(0);"><?php    echo t('Additional Filters')?></a></li>
	<li class=""><a id="ccm-blockEditPane-tab-headers"  href="javascript:void(0);"><?php    echo t('Headings')?></a></li>
	<li class=""><a id="ccm-blockEditPane-tab-review"  href="javascript:void(0);"><?php    echo t('Reviewable')?></a></li>
	<li class=""><a id="ccm-blockEditPane-tab-edit"  href="javascript:void(0);"><?php    echo t('Editable')?></a></li>
</ul>
<div id="ccm-blockEditPane-options" class="ccm-blockEditPane">
	<div class="ccm-block-field-group">
	  <h4><?php    echo t('Number of Entries from Form')?></h4>
	  <?php    echo t('Display')?>
	  <input type="text" name="num" value="<?php    echo $num?>" style="width: 30px">
	  <?php    echo t('Entries from Form')?>
	  <?php    
		$category = AttributeKeyCategory::getByHandle('proforms_item');
		$sets = $category->getAttributeSets();
		foreach($sets as $as) {
			$sel[$as->getAttributeSetID()] = $as->getAttributeSetName();
		}
		print $form->select('asID', $sel, $asID);
		?>
	  <h4><?php    echo t('Filter')?></h4>
	  
	  <?php    
		  Loader::model('attribute/categories/collection');
		  $fuak = ProformsItemAttributeKey::getByHandle('associated_user');
		  ?>
	  <input <?php     if (!is_object($fuak)) { ?> disabled <?php     } ?> type="checkbox" name="filter_user" value="1" <?php     if ($filter_user == 1) { ?> checked <?php     } ?> style="vertical-align: middle" />
	  <?php    echo t('Filter by currently viewing User.')?>
	  <br/>
	  <?php    echo t('(<strong>Note</strong>: Form Entries must utilize the "Associated User" QuestionType with a handle of "associated_user".)');?></span>
		
		<br /><br />
		<input type="checkbox" name="filter_reviewed" value="1" <?php     if ($filter_reviewed == 1) { ?> checked <?php     } ?> style="vertical-align: middle" />
	  <?php    echo t('Show only reviewed applications. (for forms set to require review before submission)')?>
	  	<br/>
	</div>
	
	<div class="ccm-block-field-group">
		<h4><?php    echo t('Permissions')?></h4>
		<input type="checkbox" name="allow_edit" value="1" <?php     if ($allow_edit == 1) { ?> checked <?php     } ?> />
		<?php    echo t('Allow viewing User to edit Entries.')?>
		<br/>
		<input type="checkbox" name="allow_delete" value="1" <?php     if ($allow_delete == 1) { ?> checked <?php     } ?> />
		<?php    echo t('Allow viewing User to remove Entries.')?>
	</div>
	
	<div class="ccm-block-field-group">
		<h4><?php    echo t('Pagination')?></h4>
		<input type="checkbox" name="paginate" value="1" <?php     if ($paginate == 1) { ?> checked <?php     } ?> />
		<?php    echo t('Display pagination interface if more items are available than are displayed.')?>
	</div>
	
	<div class="ccm-block-field-group">
	  <h4><?php    echo t('Provide RSS Feed')?></h4>
	   <input id="ccm-pagelist-rssSelectorOn" type="radio" name="rss" class="rssSelector" value="1" <?php    echo ($rss?"checked=\"checked\"":"")?>/> <?php    echo t('Yes')?>   
	   &nbsp;&nbsp;
	   <input type="radio" name="rss" class="rssSelector" value="0" <?php    echo ($rss?"":"checked=\"checked\"")?>/> <?php    echo t('No')?>   
	   <br /><br />
	   <div id="ccm-pagelist-rssDetails" <?php    echo ($rss?"":"style=\"display:none;\"")?>>
		   <strong><?php    echo t('RSS Feed Title')?></strong><br />
		   <input id="ccm-pagelist-rssTitle" type="text" name="rssTitle" style="width:250px" value="<?php    echo $rssTitle?>" /><br /><br />
		   <strong><?php    echo t('RSS Feed Description')?></strong><br />
		   <textarea name="rssDescription" style="width:250px" ><?php    echo $rssDescription?></textarea>
	   </div>
	</div>
</div>
<div id="ccm-blockEditPane-filters" class="ccm-blockEditPane" style="display: none;">	
	<?php    
	$bt->inc('elements/form_setup_filter.php', array( 'c'=>$c, 'b'=>$b, 'controller'=>$controller,'stored_search'=>$stored_search, 'keywords' => $keywords ) );
	?>
</div>
<div id="ccm-blockEditPane-headers" class="ccm-blockEditPane" style="display: none;">	
	<?php    
	$bt->inc('elements/form_setup_headers.php', array( 'c'=>$c, 'b'=>$b, 'controller'=>$controller) );
	?>
</div>
<div id="ccm-blockEditPane-review" class="ccm-blockEditPane" style="display: none;">	
	<?php    
	$bt->inc('elements/form_setup_review.php', array( 'c'=>$c, 'b'=>$b, 'controller'=>$controller) );
	?>
</div>
<div id="ccm-blockEditPane-edit" class="ccm-blockEditPane" style="display: none;">	
	<?php    
	$bt->inc('elements/form_setup_edit.php', array( 'c'=>$c, 'b'=>$b, 'controller'=>$controller) );
	?>
</div>