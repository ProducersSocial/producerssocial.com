<?php      defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<?php      
$form = Loader::helper('form');

extract($controller->stored_search);

$excluded = $controller->excluded_list;


//data sources
$data_sources = array();
$data_sources['directly_under_this_page'] = t('Directly under this page');
$data_sources['under_parent_page'] = t('Directly under parent page');
//$data_sources['anywhere_under_this_page'] = t('Anywhere under this page');
$data_sources['stored_search_query'] = t('A stored search query');
$data_sources['top_purchased_products'] = t('Top purchased products');
$data_sources['top_visited_products'] = t('Top visited products');
$data_sources['products_on_sale'] = t('Products on Sale');
$data_sources['user_visited_products'] = t('Breadcrumb of products seen');


//anywhere under this page options
$levels = range(1,10);
$levels = array_combine($levels,$levels);

//search options
$search = $controller->stored_search;

if (!is_array($search['selectedSearchField'])) $search['selectedSearchField'] = array();
$searchFields = array(
	//'' => '** ' . t('Fields'),
	//'date_added' => t('Created Between')
	'' => t('Select Search Field.'),
);
$uh = Loader::helper('concrete/urls');

Loader::model("attribute/set");		
Loader::model('attribute/categories/proforms_item','proforms');
$searchFieldAttributes = ProformsItemAttributeKey::getList();

//var_dump($controller->stored_search);

$validAttributes = array();
//filter unsopported attributes untill we fix them
foreach($searchFieldAttributes as $ak) {
	if(is_object($ak) && 
		(
		$ak->getAttributeType()->getAttributeTypeHandle() == 'status' ||
		$ak->getAttributeType()->getAttributeTypeHandle() == 'select' ||
		$ak->getAttributeType()->getAttributeTypeHandle() == 'select_values' ||
		$ak->getAttributeType()->getAttributeTypeHandle() == 'email' ||
		$ak->getAttributeType()->getAttributeTypeHandle() == 'color' ||
		$ak->getAttributeType()->getAttributeTypeHandle() == 'publish' ||
		$ak->getAttributeType()->getAttributeTypeHandle() == 'hidden_value'
		)){
		if(!in_array($ak->getAttributeKeyID(),$validAttributes)){
			$validAttributes[$ak->getAttributeKeyID()] = $ak;
			$searchFields[$ak->getAttributeKeyID()] = $ak->getAttributeKeyName();
		}
	}
}
//var_dump($search);
$_POST = $search;
$_REQUEST  = $search;

?>
		<?php     // echo $form->hidden('mode', $mode); ?>
		<div id="radiantweb-search-advanced-fields">		
			<input type="hidden" name="search" value="1" />
			<div id="ccm-search-advanced-fields-inner">
				<div class="ccm-search-field">
					<?php     echo $form->label('keywords', t('Keywords'))?>
					<table border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td class="ccm-selected-field-content">
							<?php     echo $form->text('keywords', $keywords); ?>
							</td>
							<td>
						</tr>
					</table>
					
					<?php     echo $form->label('attributes', t('Attribute Filters'))?>
					<table>
						<tr>
							<td valign="top" width="100%" class="ccm-selected-field-content">
							<?php     echo $form->select('searchField', $searchFields, array('style' => 'width: 130px'));
							?>
							<input type="hidden" value="" class="radiantweb-selected-field" name="selectedSearchField[]" />
							</td>
						</tr>
					</table>
					<Br/>
				</div>
				<div id="ccm-search-saved-fields-wrapper">
					<?php 
					foreach ($validAttributes as $ak) { 
							if($ak->akID > 0){
								$i = $ak->getAttributeKeyID();
								if (in_array($i,$search['selectedSearchField'])) {

									$display = "block";
									$selected = $i;
								
									if(is_array($search[$selected]['value'])){
										$vals = $search[$selected]['value'];
									}elseif(is_array($search[$selected]['atSelectOptionID'])){				
										//var_dump($ak->getController()->getAllowMultipleValues());
										if(method_exists($ak->getController(),'getAllowMultipleValues') && $ak->getController()->getAllowMultipleValues() < 1){
											$vals = $search[$selected]['atSelectOptionID'];
										}else{
											$vals = $search[$selected];
										}
									}elseif(is_array($search[$selected])){
										$vals = $search[$selected];
									}else{
										$vals = array($search[$selected]);
									}
				
									foreach($vals as $value){
										
										//var_dump($value);
										
										$_POST['akID'][$i]['value'] = $value;
										$_POST['akID'][$i]['status_selected'] = $value;
										$_POST['akID'][$i]['atSelectOptionID'] = $value;
										if($search[$selected]['atSelectNewOption']){
											$_POST['akID'][$i]['atSelectNewOption'] = $search[$selected]['atSelectNewOption'];
										}
									?>
									<div id="radiantweb-search-field-set<?php     echo  $i ?>" class="ccm-search-field" style="display:<?php     echo  $display ?>">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td valign="top" class="control-label">
												<?php     echo  $ak->getAttributeKeyName(); ?>
												<input type="hidden" value="<?php     echo  $selected ?>" class="radiantweb-selected-field" name="selectedSearchField[]" />
												</td>
												<td valign="top" class="ccm-selected-field-content <?php echo $ak->getAttributeType()->getAttributeTypeHandle()?>">
												<?php     
													//var_dump($search[$selected]['value']);
													if($ak->getAttributeType()->getAttributeTypeHandle() == 'status'){
														$ak->render('basic_form');
													}else{
														$ak->render('form');
													}
												?>
												</td>
												<td valign="top">
												<a href="javascript:void(0)" class="ccm-search-remove-option"><img src="<?php     echo ASSETS_URL_IMAGES?>/icons/remove_minus.png" width="16" height="16" /></a>
												</td>
												<td>
													<?php    
													echo '<div style="float: right;">';
													if(in_array($i,$excluded)){$checked = true;}else{$checked = false;}
													echo $form->checkbox('excluded_list[]',$i,$checked);
													echo t(' Excluded Filter');
													echo '</div>';
													?>
												</td>
											</tr>
										</table>
										<br/>
									</div>
								<?php   } ?>
						<?php   } ?>
					<?php   } ?>
				<?php   } ?>
				</div>
				<div id="ccm-search-fields-wrapper">
					<?php       
						foreach ($validAttributes as $ak) { 
							if(is_object($ak)){
								$i = $ak->getAttributeKeyID();
								$selected = "";
								$display = "none";
								?>
								<div id="radiantweb-search-field-set<?php     echo  $i ?>" class="ccm-search-field" style="display:<?php     echo  $display ?>">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top" class="control-label">
											<?php     echo  $ak->getAttributeKeyName(); ?>
											<input type="hidden" value="<?php     echo  $ak->getAttributeKeyID() ?>" class="radiantweb-selected-field" name="selectedSearchField[]" />
											</td>
											<td valign="top" class="ccm-selected-field-content <?php echo $ak->getAttributeType()->getAttributeTypeHandle()?>">
											<?php     
												//var_dump($search[$selected]['value']);
												if($ak->getAttributeType()->getAttributeTypeHandle() == 'status'){
													$ak->render('basic_form');
												}else{
													$ak->render('form');
												}
											?>
											</td>
											<td valign="top">
											<a href="javascript:void(0)" class="ccm-search-remove-option"><img src="<?php     echo ASSETS_URL_IMAGES?>/icons/remove_minus.png" width="16" height="16" /></a>
											</td>
											<td>
												<?php    
												echo '<div style="float: right;">';
												if(in_array($i,$excluded)){$checked = true;}else{$checked = false;}
												echo $form->checkbox('excluded_list[]',$i,$checked);
												echo t(' Excluded Filter');
												echo '</div>';
												?>
											</td>
										</tr>
									</table>
									<br/>
								</div>
							<?php      } ?>
					<?php      } ?>
				</div>
			</div>
		</div>