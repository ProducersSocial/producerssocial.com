<?php    
defined('C5_EXECUTE') or die("Access Denied.");
$rssUrl = $showRss ? $controller->getRssUrl($b) : '';
$cu = Loader::helper('concrete/urls'); 
$th = Loader::helper('text');
$bt = $b->getBlockTypeObject();
?>
<style type="text/css">
.icon {background-image:url('<?php     echo ASSETS_URL_IMAGES?>/icons_sprite.png'); /*your location of the image may differ*/}
</style>
<?php    if($error){ ?>
<div class="ajax_error_<?php   echo $bID?> ajax_error ccm-ui" style="display: <?php    if($error){ echo 'block';}else{echo 'none';}?>; clear:both;">
	<div class="alert alert-danger">
	  	<button type="button" class="close" data-dismiss="alert">&times;</button>
	  	<h4><?php    echo t('There was a problem with your form submission:'); ?></h4>
		<ul>
		<?php    
		if($error){
			echo '<ul>';
			foreach($error as $item){
				echo '<li>'.$item.'</li>';
			}
			echo '</ul>';
		}
		?>
		</ul>
	</div>
</div>
<?php    } ?>
<?php    if($message){ ?>
<div class="ajax_success_<?php   echo $bID?> ccm-ui">
	<div class="alert alert-info">
	  	<button type="button" class="close" data-dismiss="alert">&times;</button>
	  	<h4><?php    echo t('Success!'); ?></h4>
	<?php    echo $message; ?>
	</div>
</div>
<?php    } ?>
<div id="ccm-list-wrapper" style="margin-bottom: 10px">
	<?php    
	$pkg = Package::getByHandle('proforms_item');
	$txt = Loader::helper('text');
	$keywords = $_REQUEST['keywords'];
	$bu = $this->action('view');
	if (count($entries) > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0" id="ccm-<?php   echo $searchType?>-list" class="ccm-results-list">
		<tr>
			<?php    if($allow_edit || $allow_delete){ ?>
			<th><?php   echo t('Action')?></th>
			<?php    } ?>
			<th class="<?php    echo $pl->getSearchResultsClass('ProformsItemID')?>">
				<a href="<?php    echo $pl->getSortByURL('ProformsItemID', 'asc', $bu)?>"><?php    echo t('ID#')?></a>
			</th>
		<?php    
		foreach($slist as $ak) { ?>
			<th class="<?php    echo $pl->getSearchResultsClass($ak)?>">
				<a href="<?php    echo $pl->getSortByURL($ak, 'asc', $bu)?>"><?php    echo $ak->getAttributeKeyName()?></a>
			</th>
		<?php    } ?>
			<th class="<?php    echo $pl->getSearchResultsClass('ProformsItemDate')?>"><a href="<?php    echo $pl->getSortByURL('ProformsItemDate', 'asc', $bu)?>"><?php echo t('Date')?></a></th>
		</tr>
		<?php    
		foreach($entries as $noO) { 
				
			$action = View::url('/dashboard/proforms/search/edit/' . $noO->getProformsItemID());

			?>
		<tr class="ccm-list-record <?php    echo $striped?>">
			<?php    if($allow_edit || $allow_delete){ ?>
			<td class="ccm-<?php   echo $searchType?>-list-cb" style="vertical-align: middle !important">
				<?php    if($allow_edit){ ?>
				<a href="<?php   echo $cu->getBlockTypeToolsURL($bt).'/form_edit.php?bID='.$bID.'&ProformsItemID='.$noO->getProformsItemID().'&update_url='.urlencode($this->action('update_entry'))?>" class="icon edit dialog-launch" dialog-modal="false" dialog-width="500" dialog-height="400" dialog-title="<?php   echo t('Edit')?>"></a> &nbsp;
				<?php    } ?>
				<?php    if($allow_delete){ ?>
				<a href="<?php   echo $cu->getBlockTypeToolsURL($bt).'/form_delete.php?bID='.$bID.'&ProformsItemID='.$noO->getProformsItemID().'&update_url='.urlencode($this->action('remove_entry'))?>" class="icon delete dialog-launch" dialog-modal="false" dialog-width="300" dialog-height="60" dialog-title="<?php   echo t('Delete')?>"></a>
				<?php    } ?>
			</td>
			<?php    } ?>
			<td><?php echo $noO->getProformsItemID()?></td>
			<?php    
			foreach($slist as $ak) { 
				if($ak->akID > 0){
					?>
					<td 
					<?php    if($review != ''){ ?>
						class="review_entry_<?php   echo $bID?>" data-item="<?php   echo $noO->getProformsItemID()?>"
					<?php    } ?>
					>
					<?php   
					$vo = $noO->getAttributeValueObject($ak);
					$at = $ak->getAttributeType();
					$cnt = $at->getController();
					$cnt->setAttributeKey($this);

					if(method_exists($cnt,'display_review')){
						$ak->render('display_review',$vo);
					}elseif(method_exists($cnt,'display')){
						$ak->render('display',$vo);
					}else{
						if(is_object($vo)){
							$akID = $ak->getAttributeKeyID();
			
							$val = $vo->getValue();
					
							if(!is_array($val) && !$ak->getTypeMultipart()){
								echo $val;
							}
						}
					}
					?>
					</td>
				<?php    } ?>
			<?php    } ?>
			<td><?php echo $noO->getProformsItemDate()?></td>
		</tr>
		<?php    
		}

	?>
	</table>
	<?php    } else { ?>
	<?php    echo t('No Entries found.')?>
	<?php    } 
	if($entries && $num > 0 && $paginate){
		$pl->displayPaging(); 
	}
	?>
</div>
<?php    
if($entries){
	echo $pl->displaySummary();
}
?>
<div id="entry_dialog_<?php   echo $bID?>"></div>
<?php     if ($rss): ?>
	<?php    $rssUrl = $controller->getRssUrl($b); ?>
	<div class="ccm-page-list-rss-icon">
		<a href="<?php     echo $rssUrl ?>" target="_blank"><img src="<?php     echo $uh->getBlockTypeAssetsURL($bt, 'rss.png')?>" width="14" height="14" alt="<?php     echo t('RSS Icon') ?>" title="<?php     echo t('RSS Feed') ?>" /> <?php     echo t('RSS Feed') ?></a>
	</div>
	<link href="<?php     echo BASE_URL.$rssUrl ?>" rel="alternate" type="application/rss+xml" title="<?php     echo $rssTitle; ?>" />
<?php     endif; ?>

<script type="text/javascript">
	/*<![CDATA[*/
	$(document).ready(function(){
		$('.dialog-launch').dialog();
		
		$('.review_entry_<?php   echo $bID?>').bind('click',function(){
			var id = $(this).attr('data-item');
			$('#entry_dialog_<?php   echo $bID?>').load('<?php   echo $cu->getBlockTypeToolsURL($bt).'/form_review.php?bID='.$bID?>&ProformsItemID='+id+'&update_url=<?php   echo urlencode($this->action('update_entry'))?>').dialog({
				width: 500,
				height: 450,
				title: '<?php   echo t('Review')?>',
				dialogClass: "chordchart_dialog",
				close: function(){
					$('#entry_dialog_<?php   echo $bID?>').empty();
				},
				buttons: { 
				    "Print": function() { $("#entry_dialog_<?php   echo $bID?>").printThis(); },
				    "Close": function() { $(this).dialog("close"); }
				  }
			});
		});
	});
	/*]]>*/
</script>