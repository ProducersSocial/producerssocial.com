<?php      defined('C5_EXECUTE') or die(_("Access Denied."));
class ProformsPackage extends Package {

	protected $pkgHandle = 'proforms';
	protected $appVersionRequired = '5.6.1.2';
	protected $pkgVersion = '7.5.8';
	
	public function getPackageDescription() {
		return t('Forms with power and extendability.');
	}
	
	public function getPackageName() {
		return t('Pro Forms');
	}
	
	public function install() {
		
		$this->load_required_models();
	
		$pkg = parent::install();
		
		//install blocks
	  	BlockType::installBlockTypeFromPackage('proforms_display', $pkg);
	  	BlockType::installBlockTypeFromPackage('proforms_list', $pkg);
	  	
	  	$iak = CollectionAttributeKey::getByHandle('icon_dashboard');
	  	
		$spa = SinglePage::add('dashboard/proforms', $pkg);
		$spa->update(array('cName' => t('Proforms'), 'cDescription'=>t('Proforms Manager.')));
		
		$spc = SinglePage::add('dashboard/proforms/overview', $pkg);
		$spc->update(array('cName' => t('Overview'), 'cDescription'=>t('Proforms Overview')));
		$spc->setAttribute($iak,'icon-th');
		
		$spb = SinglePage::add('dashboard/proforms/search', $pkg);
		$spb->update(array('cName' => t('Search Submissions'), 'cDescription'=>t('Manage Submissions.')));
		$spb->setAttribute($iak,'icon-search');
		
		$spc = SinglePage::add('dashboard/proforms/attributes', $pkg);
		$spc->update(array('cName' => t('Manage Questions'), 'cDescription'=>t('Manage Questions.')));
		$spc->setAttribute($iak,'icon-list');
		
		$spm = SinglePage::add('dashboard/proforms/manage_forms', $pkg);
		$spm->setAttribute($iak,'icon-list-alt');
		
		$spe = SinglePage::add('dashboard/proforms/editor', $pkg);
		$spe->setAttribute($iak,'icon-pencil');
		
		$sps = SinglePage::add('dashboard/proforms/settings', $pkg);
		$sps->setAttribute($iak,'icon-wrench');
		$sps->update(array('cName' => 'Settings', 'cDescription'=>t('Manage Settings.')));
		
		$this->install_form_atts($pkg);
		
	}
	
	public function install_form_atts($pkg){
		$allowSets = true;
		$akc = AttributeKeyCategory::add('proforms_item', $allowSets, $pkg);
		$akc->setAllowAttributeSets(AttributeKeyCategory::ASET_ALLOW_SINGLE);
		
		$form_fields = $akc->addSet('example_form', t('Example Form'),$pkg);
		
		$internal = $akc->addSet('internal', t('Internal'),$pkg);
		Loader::model('proforms_question_set','proforms');
		$qs = new ProformsQuestionSet();
		$qs->SetQuestionSetIsInternal($internal->getAttributeSetID(),1);
		
		
		$address = AttributeType::getByHandle('address');
		$text = AttributeType::getByHandle('text');
		$number = AttributeType::getByHandle('number');
		$select = AttributeType::getByHandle('select');
		$textarea = AttributeType::getByHandle('textarea');
		$boolean = AttributeType::getByHandle('boolean');
		$file = AttributeType::getByHandle('image_file');
		$date = AttributeType::getByHandle('date_time');
		

		$akc->associateAttributeKeyType($text);
		$akc->associateAttributeKeyType($textarea);
		$akc->associateAttributeKeyType($number);
		$akc->associateAttributeKeyType($boolean);
		
		$email = AttributeType::getByHandle('email');
		if(!is_object($email) || !intval($email->getAttributeTypeID()) ) { 
			$email = AttributeType::add('email', t('Email'), $pkg);
			$akc->associateAttributeKeyType($email);	  
		} 
		
		$phone = AttributeType::getByHandle('phone');
		if(!is_object($phone) || !intval($phone->getAttributeTypeID()) ) { 
			$phone = AttributeType::add('phone', t('Phone'), $pkg);
			$akc->associateAttributeKeyType($phone);	  
		} 
		
		$password = AttributeType::getByHandle('password');
		if(!is_object($password) || !intval($password->getAttributeTypeID()) ) { 
			$password = AttributeType::add('password', t('Password'), $pkg);
			$akc->associateAttributeKeyType($password);	  
		} 
		
		$regex_match = AttributeType::getByHandle('regex_match');
		if(!is_object($regex_match) || !intval($regex_match->getAttributeTypeID()) ) { 
			$regex_match = AttributeType::add('regex_match', t('Regex Match'), $pkg);
			$akc->associateAttributeKeyType($regex_match);	  
		} 
		
		$location_autofill = AttributeType::getByHandle('location_autofill');
		if(!is_object($location_autofill) || !intval($location_autofill->getAttributeTypeID()) ) { 
			$location_autofill = AttributeType::add('location_autofill', t('Location Autofill'), $pkg);
			$akc->associateAttributeKeyType($location_autofill);	  
		} 
		
		$multiplex = AttributeType::getByHandle('multiplex');
		if(!is_object($multiplex) || !intval($multiplex->getAttributeTypeID()) ) { 
			$multiplex = AttributeType::add('multiplex', t('MultiPlex'), $pkg);	  
		} 
		$akc->associateAttributeKeyType($multiplex);	
		
		$file_approve = AttributeType::getByHandle('file_approve');
		if(!is_object($file_approve) || !intval($file_approve->getAttributeTypeID()) ) { 
			$file_approve = AttributeType::add('file_approve', t('File Approve'), $pkg);	  
		} 
		$akc->associateAttributeKeyType($file_approve);
		
		$simplefile = AttributeType::getByHandle('simple_file');
		if(!is_object($simplefile) || !intval($simplefile->getAttributeTypeID()) ) { 
			$simplefile = AttributeType::add('simple_file', t('Simple File'), $pkg);
			$akc->associateAttributeKeyType($simplefile);	  
		} 
		
		$validatedsimplefile = AttributeType::getByHandle('validated_simple_file');
		if(!is_object($validatedsimplefile) || !intval($validatedsimplefile->getAttributeTypeID()) ) { 
			$validatedsimplefile = AttributeType::add('validated_simple_file', t('Validated Simple File'), $pkg);
			$akc->associateAttributeKeyType($validatedsimplefile);	  
		} 
		
		$akc->associateAttributeKeyType($file);
		
		$validated_media = AttributeType::getByHandle('validated_media');
		if(!is_object($validated_media) || !intval($validated_media->getAttributeTypeID()) ) { 
			$validated_media = AttributeType::add('validated_media', t('Validated Media'), $pkg);
			$akc->associateAttributeKeyType($validated_media);	  
		} 
		
		
		$akc->associateAttributeKeyType($date);
		$akc->associateAttributeKeyType($address);
		$akc->associateAttributeKeyType($select);
		
		$selectValues = AttributeType::getByHandle('select_values');
		if(!is_object($selectValues) || !intval($selectValues->getAttributeTypeID()) ) { 
			$selectValues = AttributeType::add('select_values', t('Select Values'), $pkg);
			$akc->associateAttributeKeyType($selectValues);	  
		} 
		
		$radio = AttributeType::getByHandle('radio');
		if(!is_object($radio) || !intval($radio->getAttributeTypeID()) ) { 
			$radio = AttributeType::add('radio', t('Radio'), $pkg);
			$akc->associateAttributeKeyType($radio);	  
		} 
		
		$hidden = AttributeType::getByHandle('hidden_value');
		if(!is_object($hidden) || !intval($hidden->getAttributeTypeID()) ) { 
			$hidden = AttributeType::add('hidden_value', t('Hidden Value'), $pkg);
			$akc->associateAttributeKeyType($hidden);	  
		} 
		
		$hidden_page_att = AttributeType::getByHandle('hidden_page_attribute');
		if(!is_object($hidden_page_att) || !intval($hidden_page_att->getAttributeTypeID()) ) { 
			$hidden_page_att = AttributeType::add('hidden_page_attribute', t('Hidden Page Attribute'), $pkg);
			$akc->associateAttributeKeyType($hidden_page_att);	  
		} 
		
		$hidden_user_att = AttributeType::getByHandle('hidden_user_attribute');
		if(!is_object($hidden_user_att) || !intval($hidden_user_att->getAttributeTypeID()) ) { 
			$hidden_user_att = AttributeType::add('hidden_user_attribute', t('Hidden User Attribute'), $pkg);
			$akc->associateAttributeKeyType($hidden_user_att);	  
		} 
		
		$hidden_from_page = AttributeType::getByHandle('hidden_from_page');
		if(!is_object($hidden_from_page) || !intval($hidden_from_page->getAttributeTypeID()) ) { 
			$hidden_from_page = AttributeType::add('hidden_from_page', t('Hidden From Page'), $pkg);
			$akc->associateAttributeKeyType($hidden_from_page);	  
		} 
		
		$random_code = AttributeType::getByHandle('random_code');
		if(!is_object($random_code) || !intval($random_code->getAttributeTypeID()) ) { 
			$random_code = AttributeType::add('random_code', t('Random Code'), $pkg);
			$akc->associateAttributeKeyType($random_code);	  
		} 
		
		$auto_responder = AttributeType::getByHandle('auto_responder');
		if(!is_object($auto_responder) || !intval($auto_responder->getAttributeTypeID()) ) { 
			$auto_responder = AttributeType::add('auto_responder', t('Auto Responder'), $pkg);
			$akc->associateAttributeKeyType($auto_responder);	  
		} 
		
		$website_urls = AttributeType::getByHandle('website_urls');
		if(!is_object($website_urls) || !intval($website_urls->getAttributeTypeID()) ) { 
			$website_urls = AttributeType::add('website_urls', t('Website Url\'s'), $pkg);
			$akc->associateAttributeKeyType($website_urls);	  
		} 
		
		$hr_display = AttributeType::getByHandle('hr_display');
		if(!is_object($hr_display) || !intval($hr_display->getAttributeTypeID()) ) { 
			$hr_display = AttributeType::add('hr_display', t('HR Tag'), $pkg);
			$akc->associateAttributeKeyType($hr_display);	  
		} 
		
		$text_display = AttributeType::getByHandle('text_display');
		if(!is_object($text_display) || !intval($text_display->getAttributeTypeID()) ) { 
			$text_display = AttributeType::add('text_display', t('Display Text'), $pkg);
			$akc->associateAttributeKeyType($text_display);	  
		} 
		
		$title_display = AttributeType::getByHandle('title_display');
		if(!is_object($title_display) || !intval($title_display->getAttributeTypeID()) ) { 
			$title_display = AttributeType::add('title_display', t('Display Title'), $pkg);
			$akc->associateAttributeKeyType($title_display);	  
		} 
		
		$stack = AttributeType::getByHandle('stack_display');
		if(!is_object($stack) || !intval($stack->getAttributeTypeID()) ) { 
			$stack = AttributeType::add('stack_display', t('Display Stack'), $pkg);
			$akc->associateAttributeKeyType($stack);	  
		} 
		
		$associate_object = AttributeType::getByHandle('associate_object');
		if(!is_object($associate_object) || !intval($associate_object->getAttributeTypeID()) ) { 
			$associate_object = AttributeType::add('associate_object', t('Associate Object'), $pkg);
			$akc->associateAttributeKeyType($associate_object);	  
		} 
		
		$associate_user = AttributeType::getByHandle('associate_user');
		if(!is_object($associate_user) || !intval($associate_user->getAttributeTypeID()) ) { 
			$associate_user = AttributeType::add('associate_user', t('Associate User'), $pkg);
			$akc->associateAttributeKeyType($associate_user);	  
		} 
		
		$register_user = AttributeType::getByHandle('register_user');
		if(!is_object($register_user) || !intval($register_user->getAttributeTypeID()) ) { 
			$register_user = AttributeType::add('register_user', t('Register User'), $pkg);
			$akc->associateAttributeKeyType($register_user);	  
		} 
		$publish = AttributeType::getByHandle('publish');
		if(!is_object($publish) || !intval($publish->getAttributeTypeID()) ) { 
			$publish = AttributeType::add('publish', t('Publish'), $pkg);
			$akc->associateAttributeKeyType($publish);	  
		} 
		
		$optional_expand = AttributeType::getByHandle('optional_expand');
		if(!is_object($optional_expand) || !intval($optional_expand->getAttributeTypeID()) ) { 
			$optional_expand = AttributeType::add('optional_expand', t('Optional Expand'), $pkg);
			$akc->associateAttributeKeyType($optional_expand);	  
		} 

		$fieldset_container = AttributeType::getByHandle('fieldset_container');
		if(!is_object($fieldset_container) || !intval($fieldset_container->getAttributeTypeID()) ) { 
			$fieldset_container = AttributeType::add('fieldset_container', t('Fieldset Container'), $pkg);
			$akc->associateAttributeKeyType($fieldset_container);	  
		} 
		
		$css_container = AttributeType::getByHandle('css_container');
		if(!is_object($css_container) || !intval($css_container->getAttributeTypeID()) ) { 
			$css_container = AttributeType::add('css_container', t('CSS Container'), $pkg);
			$akc->associateAttributeKeyType($css_container);	  
		} 
		
		$stepped_form = AttributeType::getByHandle('stepped_form');
		if(!is_object($stepped_form) || !intval($stepped_form->getAttributeTypeID()) ) { 
			$stepped_form = AttributeType::add('stepped_form', t('Stepped Form'), $pkg);
			$akc->associateAttributeKeyType($stepped_form);	  
		} 
		
		$popup_form= AttributeType::getByHandle('popup_form');
		if(!is_object($popup_form) || !intval($popup_form->getAttributeTypeID()) ) { 
			$popup_form = AttributeType::add('popup_form', t('PopUp Form'), $pkg);
			$akc->associateAttributeKeyType($popup_form);	  
		} 
	
		$color = AttributeType::getByHandle('color');
		if(!is_object($color) || !intval($color->getAttributeTypeID()) ) { 
			$color = AttributeType::add('color', t('Color'), $pkg);
			$akc->associateAttributeKeyType($color);	  
		} 
		
		$agreement = AttributeType::getByHandle('agreement');
		if(!is_object($agreement) || !intval($agreement->getAttributeTypeID()) ) { 
			$agreement = AttributeType::add('agreement', t('Agreement'), $pkg);
			$akc->associateAttributeKeyType($agreement);	  
		} 
		
		
		/*
		$signature = AttributeType::getByHandle('signature');
		if(!is_object($signature) || !intval($signature->getAttributeTypeID()) ) { 
			$signature = AttributeType::add('signature', t('Signature'), $pkg);
			$akc->associateAttributeKeyType($signature);	  
		} 
		*/
		
		$status = AttributeType::getByHandle('status');
		if(!is_object($status) || !intval($status->getAttributeTypeID()) ) { 
			$status = AttributeType::add('status', t('Status'), $pkg);
			$akc->associateAttributeKeyType($status);	  
		}
		
		$price_event = AttributeType::getByHandle('price_event');
		if(!is_object($price_event) || !intval($price_event->getAttributeTypeID()) ) { 
			$price_event = AttributeType::add('price_event', t('ProEvents Dates'), $pkg);
			$akc->associateAttributeKeyType($price_event);	  
		} 
		
		$price_value_enter = AttributeType::getByHandle('price_value_enter');
		if(!is_object($price_value_enter) || !intval($price_value_enter->getAttributeTypeID()) ) { 
			$price_value_enter = AttributeType::add('price_value_enter', t('Price Value Input'), $pkg);
			$akc->associateAttributeKeyType($price_value_enter);	  
		} 
		
		$price_value = AttributeType::getByHandle('price_value');
		if(!is_object($price_value) || !intval($price_value->getAttributeTypeID()) ) { 
			$price_value = AttributeType::add('price_value', t('Price Value'), $pkg);
			$akc->associateAttributeKeyType($price_value);	  
		} 
		
		$price_value_fees = AttributeType::getByHandle('price_value_fees');
		if(!is_object($price_value_fees) || !intval($price_value_fees->getAttributeTypeID()) ) { 
			$price_value_fees = AttributeType::add('price_value_fees',t('Price Value Fees'), $pkg);
			$akc->associateAttributeKeyType($price_value_fees);	  
		}
		
		$price_select = AttributeType::getByHandle('price_select');
		if(!is_object($price_select) || !intval($price_select->getAttributeTypeID()) ) { 
			$price_select = AttributeType::add('price_select', t('Price Select'), $pkg);
			$akc->associateAttributeKeyType($price_select);	  
		} 
		
		$price_qty = AttributeType::getByHandle('price_qty');
		if(!is_object($price_qty) || !intval($price_qty->getAttributeTypeID()) ) { 
			$price_qty = AttributeType::add('price_qty', t('Price Qty'), $pkg);
			$akc->associateAttributeKeyType($price_qty);	  
		} 
		
		$price_discount = AttributeType::getByHandle('price_discount');
		if(!is_object($price_discount) || !intval($price_discount->getAttributeTypeID()) ) { 
			$price_discount = AttributeType::add('price_discount', t('Price Discount'), $pkg);
			$akc->associateAttributeKeyType($price_discount);	  
		} 
		
		$price_discount_qty = AttributeType::getByHandle('price_discount_qty');
		if(!is_object($price_discount_qty) || !intval($price_discount_qty->getAttributeTypeID()) ) { 
			$price_discount_qty = AttributeType::add('price_discount_qty', t('Price Discount by Qty'), $pkg);
			$akc->associateAttributeKeyType($price_discount_qty);	  
		} 
		
		$price_discount_span = AttributeType::getByHandle('price_discount_span');
		if(!is_object($price_discount_span) || !intval($price_discount_span->getAttributeTypeID()) ) { 
			$price_discount_span = AttributeType::add('price_discount_span', t('Price Discount by Span'), $pkg);
			$akc->associateAttributeKeyType($price_discount_span);	  
		} 
		
		$price_tax = AttributeType::getByHandle('price_tax');
		if(!is_object($price_tax) || !intval($price_tax->getAttributeTypeID()) ) { 
			$price_tax = AttributeType::add('price_tax', t('Price Tax'), $pkg);
			$akc->associateAttributeKeyType($price_tax);	  
		} 
		
		$price_tax_address = AttributeType::getByHandle('price_tax_address');
		if(!is_object($price_tax_address) || !intval($price_tax_address->getAttributeTypeID()) ) { 
			$price_tax_address = AttributeType::add('price_tax_address', t('Price Tax Address'), $pkg);
			$akc->associateAttributeKeyType($price_tax_address);	  
		}
		
		$price_total = AttributeType::getByHandle('price_total');
		if(!is_object($price_total) || !intval($price_total->getAttributeTypeID()) ) { 
			$price_total = AttributeType::add('price_total', t('Price Total'), $pkg);
			$akc->associateAttributeKeyType($price_total);	  
		} 
		
		$price_shipping_address = AttributeType::getByHandle('price_shipping_address');
		if(!is_object($price_shipping_address) || !intval($price_shipping_address->getAttributeTypeID()) ) { 
			$price_shipping_address = AttributeType::add('price_shipping_address', t('Price Shipping Address'), $pkg);
			$akc->associateAttributeKeyType($price_shipping_address);	  
		} 
		
		$price_shipping_fixed = AttributeType::getByHandle('price_shipping_fixed');
		if(!is_object($price_shipping_fixed) || !intval($price_shipping_fixed->getAttributeTypeID()) ) { 
			$price_shipping_fixed = AttributeType::add('price_shipping_fixed', t('Price Shipping Fixed'), $pkg);
			$akc->associateAttributeKeyType($price_shipping_fixed);	  
		}
		
		$payment_paypal = AttributeType::getByHandle('payment_paypal');
		if(!is_object($payment_paypal) || !intval($payment_paypal->getAttributeTypeID()) ) { 
			$payment_paypal = AttributeType::add('payment_paypal', t('Paypal Payment'), $pkg);
			$akc->associateAttributeKeyType($payment_paypal);	  
		} 
		
		$payment_wepay = AttributeType::getByHandle('payment_wepay');
		if(!is_object($payment_wepay) || !intval($payment_wepay->getAttributeTypeID()) ) { 
			$payment_wepay = AttributeType::add('payment_wepay', t('Wepay Payment'), $pkg);
			$akc->associateAttributeKeyType($payment_wepay);	  
		} 
		
		$payment_authorize = AttributeType::getByHandle('payment_authorize');
		if(!is_object($payment_authorize) || !intval($payment_authorize->getAttributeTypeID()) ) { 
			$payment_authorize = AttributeType::add('payment_authorize', t('Authorize Payment'), $pkg);
			$akc->associateAttributeKeyType($payment_authorize);	  
		} 
		
		$ajax_payment = AttributeType::getByHandle('ajax_payment');
		if(!is_object($ajax_payment) || !intval($ajax_payment->getAttributeTypeID()) ) { 
			$ajax_payment = AttributeType::add('ajax_payment', t('Ajax Payment'), $pkg);
			$akc->associateAttributeKeyType($ajax_payment);	  
		} 
		
		
		#######################################################
	  	##general tab 
	  	## title, description
	  	#######################################################
		$first_name = ProformsItemAttributeKey::getByHandle('proforms_first_name'); 
		if(!is_object($first_name) || !intval($first_name->getAttributeKeyID())){
			$first_name = array(
							'akHandle' => 'proforms_first_name',
							'akName' => t('First Name'),
							'akIsSearchable' => 1,
							'akIsSearchableIndexed' => 1,				
							'akIsAutoCreated' => 1,
							'akIsEditable' => 1
						);
			$first_name = ProformsItemAttributeKey::add($text, $first_name, $pkg);
			$first_name->setAttributeKeyColumnHeader(1);
			$first_name->setAttributeSet($form_fields); 
		}
		
		$last_name = ProformsItemAttributeKey::getByHandle('proforms_last_name'); 
		if(!is_object($last_name) || !intval($last_name->getAttributeKeyID())){
			$last_name = array(
							'akHandle' => 'proforms_last_name',
							'akName' => t('Last Name'),
							'akIsSearchable' => 1,
							'akIsSearchableIndexed' => 1,				
							'akIsAutoCreated' => 1,
							'akIsEditable' => 1
						);
			$last_name = ProformsItemAttributeKey::add($text, $last_name, $pkg);
			$last_name->setAttributeKeyColumnHeader(1);
			$last_name->setAttributeSet($form_fields); 
		}
		
		$entry_address = ProformsItemAttributeKey::getByHandle('proforms_address'); 
		if(!is_object($entry_address) || !intval($entry_address->getAttributeKeyID())){
			$entry_address = array(
							'akHandle' => 'proforms_address',
							'akName' => t('Your Address'),
							'akIsSearchable' => 1,
							'akIsSearchableIndexed' => 1
						);
			$entry_address = ProformsItemAttributeKey::add($address, $entry_address, $pkg);
			$entry_address->setAttributeKeyColumnHeader(1);
			$entry_address->setAttributeSet($form_fields); 
		}
		
		$interest=ProformsItemAttributeKey::getByHandle('proforms_interest'); 
		if( !is_object($interest) || !intval($interest->getAttributeKeyID())) {
			ProformsItemAttributeKey::add($select, 
			array('akHandle' => 'proforms_interest', 
			'akName' => t('I am primarily a'), 
			'akIsSearchable' => '1', 
			'akIsSearchableIndexed' => '1'
			),$pkg)->setAttributeSet($form_fields); 
			$ak=ProformsItemAttributeKey::getByHandle('proforms_interest');
			SelectAttributeTypeOption::add($ak,t('Musician'));
			SelectAttributeTypeOption::add($ak,t('Mobile DJ / Entertainer'));
			SelectAttributeTypeOption::add($ak,t('Rental Production Company'));
			SelectAttributeTypeOption::add($ak,t('System Engineer'));
			SelectAttributeTypeOption::add($ak,t('Contractor'));
			SelectAttributeTypeOption::add($ak,t('Designer / Consultant'));
			SelectAttributeTypeOption::add($ak,t('Corporate / Institutional User'));
			SelectAttributeTypeOption::add($ak,t('House of Worship'));
			SelectAttributeTypeOption::add($ak,t('Other'));
			//SelectAttributeTypeOption::add($ak,'twice per month');
		}
		
		$comments = AttributeType::getByHandle('comments');
		if(!is_object($comments) || !intval($comments->getAttributeTypeID()) ) { 
			$comments = AttributeType::add('comments', t('Comments'), $pkg);
			$akc->associateAttributeKeyType($comments);	  
		} 
	
	}
	
	public function upgrade(){	
	
		$db = Loader::db();
	
		$pkg = Package::getByHandle('proforms');
		$akc = AttributeKeyCategory::getByHandle('proforms_item');
		$iak = CollectionAttributeKey::getByHandle('icon_dashboard');
		
		$spe = Page::getByPath('/dashboard/proforms/editor/');
		if(!$spe || $spe->getCollectionID() < 1){
			$spe = SinglePage::add('dashboard/proforms/editor', $pkg);
			$spe->setAttribute($iak,'icon-pencil');
		}
		
		$sps = Page::getByPath('/dashboard/proforms/settings');
		if(!$sps || $sps->getCollectionID() < 1){
			$sps = SinglePage::add('dashboard/proforms/settings', $pkg);
			$sps->setAttribute($iak,'icon-wrench');
			$sps->update(array('cName' => 'Settings', 'cDescription'=>t('Manage Settings.')));
		}
		
		$sps = Page::getByPath('/dashboard/proforms/overview');
		if(!$sps || $sps->getCollectionID() < 1){
        	$spc = SinglePage::add('dashboard/proforms/overview', $pkg);
        	$spc->update(array('cName' => t('Overview'), 'cDescription'=>t('Proforms Overview')));
        	$spc->setAttribute($iak,'icon-th');
        }
		
		$bt = BlockType::getByHandle('proforms_list');
		if(!$bt){
			BlockType::installBlockTypeFromPackage('proforms_list', $pkg);
		}
		
		$location_autofill = AttributeType::getByHandle('location_autofill');
		if(!is_object($location_autofill) || !intval($location_autofill->getAttributeTypeID()) ) { 
			$location_autofill = AttributeType::add('location_autofill', t('Location Autofill'), $pkg);
			$akc->associateAttributeKeyType($location_autofill);	  
		} 
		
		$file_approve = AttributeType::getByHandle('file_approve');
		if(!is_object($file_approve) || !intval($file_approve->getAttributeTypeID()) ) { 
			$file_approve = AttributeType::add('file_approve', t('File Approve'), $pkg);	  
			$akc->associateAttributeKeyType($file_approve);
		} 

		$simplefile = AttributeType::getByHandle('simple_file');
		if(!is_object($simplefile) || !intval($simplefile->getAttributeTypeID()) ) { 
			$simplefile = AttributeType::add('simple_file', t('Simple File'), $pkg);
			$akc->associateAttributeKeyType($simplefile);	  
		} 
		
		$validatedsimplefile = AttributeType::getByHandle('validated_simple_file');
		if(!is_object($validatedsimplefile) || !intval($validatedsimplefile->getAttributeTypeID()) ) { 
			$validatedsimplefile = AttributeType::add('validated_simple_file', t('Validated Simple File'), $pkg);
			$akc->associateAttributeKeyType($validatedsimplefile);	  
		} 
		
		$signature = AttributeType::getByHandle('signature');
		if(!is_object($signature) || !intval($signature->getAttributeTypeID()) ) { 
			$signature = AttributeType::add('signature', t('Signature'), $pkg);
			$akc->associateAttributeKeyType($signature);	  
		} 
		
		$selectValues = AttributeType::getByHandle('select_values');
		if(!is_object($selectValues) || !intval($selectValues->getAttributeTypeID()) ) { 
			$selectValues = AttributeType::add('select_values', t('Select Values'), $pkg);
			$akc->associateAttributeKeyType($selectValues);	  
		} 
		
		$radio = AttributeType::getByHandle('radio');
		if(!is_object($radio) || !intval($radio->getAttributeTypeID()) ) { 
			$radio = AttributeType::add('radio', t('Radio'), $pkg);
			$akc->associateAttributeKeyType($radio);	  
		} 

		$color = AttributeType::getByHandle('color');
		if(!is_object($color) || !intval($color->getAttributeTypeID()) ) { 
			$color = AttributeType::add('color', t('Color'), $pkg);
			$akc->associateAttributeKeyType($color);	  
		} 
		
		$comments = AttributeType::getByHandle('comments');
		if(!is_object($comments) || !intval($comments->getAttributeTypeID()) ) { 
			$comments = AttributeType::add('comments', t('Comments'), $pkg);
			$akc->associateAttributeKeyType($comments);	  
		} 
		
		$stack = AttributeType::getByHandle('stack_display');
		if(!is_object($stack) || !intval($stack->getAttributeTypeID()) ) { 
			$stack = AttributeType::add('stack_display', t('Display Stack'), $pkg);
			$akc->associateAttributeKeyType($stack);	  
		} 
		
		$associate_object = AttributeType::getByHandle('associate_object');
		if(!is_object($associate_object) || !intval($associate_object->getAttributeTypeID()) ) { 
			$associate_object = AttributeType::add('associate_object', t('Associate Object'), $pkg);
			$akc->associateAttributeKeyType($associate_object);	  
		} 
		
		$associate_user = AttributeType::getByHandle('associate_user');
		if(!is_object($associate_user) || !intval($associate_user->getAttributeTypeID()) ) { 
			$associate_user = AttributeType::add('associate_user', t('Associate User'), $pkg);
			$akc->associateAttributeKeyType($associate_user);	  
		} 
		
		$website_urls = AttributeType::getByHandle('website_urls');
		if(!is_object($website_urls) || !intval($website_urls->getAttributeTypeID()) ) { 
			$website_urls = AttributeType::add('website_urls', t('Website Url\'s'), $pkg);
			$akc->associateAttributeKeyType($website_urls);	  
		}
		
		$popup_form= AttributeType::getByHandle('popup_form');
		if(!is_object($popup_form) || !intval($popup_form->getAttributeTypeID()) ) { 
			$popup_form = AttributeType::add('popup_form', t('PopUp Form'), $pkg);
			$akc->associateAttributeKeyType($popup_form);	  
		}  
		
		$hidden_page_att = AttributeType::getByHandle('hidden_page_attribute');
		if(!is_object($hidden_page_att) || !intval($hidden_page_att->getAttributeTypeID()) ) { 
			$hidden_page_att = AttributeType::add('hidden_page_attribute', t('Hidden Page Attribute'), $pkg);
			$akc->associateAttributeKeyType($hidden_page_att);	  
		} 
		
		$hidden_user_att = AttributeType::getByHandle('hidden_user_attribute');
		if(!is_object($hidden_user_att) || !intval($hidden_user_att->getAttributeTypeID()) ) { 
			$hidden_user_att = AttributeType::add('hidden_user_attribute', t('Hidden User Attribute'), $pkg);
			$akc->associateAttributeKeyType($hidden_user_att);	  
		} 
		
		$hidden_from_page = AttributeType::getByHandle('hidden_from_page');
		if(!is_object($hidden_from_page) || !intval($hidden_from_page->getAttributeTypeID()) ) { 
			$hidden_from_page = AttributeType::add('hidden_from_page', t('Hidden From Page'), $pkg);
			$akc->associateAttributeKeyType($hidden_from_page);	  
		} 

		$fieldset_container = AttributeType::getByHandle('fieldset_container');
		if(!is_object($fieldset_container) || !intval($fieldset_container->getAttributeTypeID()) ) { 
			$fieldset_container = AttributeType::add('fieldset_container', t('Fieldset Container'), $pkg);
			$akc->associateAttributeKeyType($fieldset_container);	  
		} 
		
		$price_event = AttributeType::getByHandle('price_event');
		if(!is_object($price_event) || !intval($price_event->getAttributeTypeID()) ) { 
			$price_event = AttributeType::add('price_event', t('ProEvents Dates'), $pkg);
			$akc->associateAttributeKeyType($price_event);	  
		} 
		
		$price_select = AttributeType::getByHandle('price_select');
		if(!is_object($price_select) || !intval($price_select->getAttributeTypeID()) ) { 
			$price_select = AttributeType::add('price_select', t('Price Select'), $pkg);
			$akc->associateAttributeKeyType($price_select);	  
		} 
		
		$price_value = AttributeType::getByHandle('price_value');
		if(!is_object($price_value) || !intval($price_value->getAttributeTypeID()) ) { 
			$price_value = AttributeType::add('price_value', t('Price Value'), $pkg);
			$akc->associateAttributeKeyType($price_value);	  
		} 
		
		$price_value_fees = AttributeType::getByHandle('price_value_fees');
		if(!is_object($price_value_fees) || !intval($price_value_fees->getAttributeTypeID()) ) { 
			$price_value_fees = AttributeType::add('price_value_fees', t('Price Value Fees'), $pkg);
			$akc->associateAttributeKeyType($price_value_fees);	  
		}
		
		$price_qty = AttributeType::getByHandle('price_qty');
		if(!is_object($price_qty) || !intval($price_qty->getAttributeTypeID()) ) { 
			$price_qty = AttributeType::add('price_qty', t('Price Qty'), $pkg);
			$akc->associateAttributeKeyType($price_qty);	  
		} 
		
		$price_discount = AttributeType::getByHandle('price_discount');
		if(!is_object($price_discount) || !intval($price_discount->getAttributeTypeID()) ) { 
			$price_discount = AttributeType::add('price_discount', t('Price Discount'), $pkg);
			$akc->associateAttributeKeyType($price_discount);	  
		} 
		
		$price_tax = AttributeType::getByHandle('price_tax');
		if(!is_object($price_tax) || !intval($price_tax->getAttributeTypeID()) ) { 
			$price_tax = AttributeType::add('price_tax', t('Price Tax'), $pkg);
			$akc->associateAttributeKeyType($price_tax);	  
		} 
		
		$price_tax_address = AttributeType::getByHandle('price_tax_address');
		if(!is_object($price_tax_address) || !intval($price_tax_address->getAttributeTypeID()) ) { 
			$price_tax_address = AttributeType::add('price_tax_address', t('Price Tax Address'), $pkg);
			$akc->associateAttributeKeyType($price_tax_address);	  
		}
		
		$price_total = AttributeType::getByHandle('price_total');
		if(!is_object($price_total) || !intval($price_total->getAttributeTypeID()) ) { 
			$price_total = AttributeType::add('price_total', t('Price Total'), $pkg);
			$akc->associateAttributeKeyType($price_total);	  
		} 
		
		$price_shipping_address = AttributeType::getByHandle('price_shipping_address');
		if(!is_object($price_shipping_address) || !intval($price_shipping_address->getAttributeTypeID()) ) { 
			$price_shipping_address = AttributeType::add('price_shipping_address', t('Price Shipping Address'), $pkg);
			$akc->associateAttributeKeyType($price_shipping_address);	  
		}
		
		$price_shipping_fixed = AttributeType::getByHandle('price_shipping_fixed');
		if(!is_object($price_shipping_fixed) || !intval($price_shipping_fixed->getAttributeTypeID()) ) { 
			$price_shipping_fixed = AttributeType::add('price_shipping_fixed', t('Price Shipping Fixed'), $pkg);
			$akc->associateAttributeKeyType($price_shipping_fixed);	  
		}
		
		$payment_paypal = AttributeType::getByHandle('payment_paypal');
		if(!is_object($payment_paypal) || !intval($payment_paypal->getAttributeTypeID()) ) { 
			$payment_paypal = AttributeType::add('payment_paypal', t('Paypal Payment'), $pkg);
			$akc->associateAttributeKeyType($payment_paypal);	  
		}
		
		$payment_wepay = AttributeType::getByHandle('payment_wepay');
		if(!is_object($payment_wepay) || !intval($payment_wepay->getAttributeTypeID()) ) { 
			$payment_wepay = AttributeType::add('payment_wepay', t('Wepay Payment'), $pkg);
			$akc->associateAttributeKeyType($payment_wepay);
			$db->execute("ALTER TABLE atPriceTotalPayments ADD akID INT(10)");	  
		}  	
		
		$payment_authorize = AttributeType::getByHandle('payment_authorize');
		if(!is_object($payment_authorize) || !intval($payment_authorize->getAttributeTypeID()) ) { 
			$payment_authorize = AttributeType::add('payment_authorize', t('Authorize Payment'), $pkg);
			$akc->associateAttributeKeyType($payment_authorize);	  
		} 
		
		$ajax_payment = AttributeType::getByHandle('ajax_payment');
		if(!is_object($ajax_payment) || !intval($ajax_payment->getAttributeTypeID()) ) { 
			$ajax_payment = AttributeType::add('ajax_payment', t('Ajax Payment'), $pkg);
			$akc->associateAttributeKeyType($ajax_payment);	  
		}
		
		$price_discount_qty = AttributeType::getByHandle('price_discount_qty');
		if(!is_object($price_discount_qty) || !intval($price_discount_qty->getAttributeTypeID()) ) { 
			$price_discount_qty = AttributeType::add('price_discount_qty', t('Price Discount by Qty'), $pkg);
			$akc->associateAttributeKeyType($price_discount_qty);	  
		} 
		
		$price_discount_span = AttributeType::getByHandle('price_discount_span');
		if(!is_object($price_discount_span) || !intval($price_discount_span->getAttributeTypeID()) ) { 
			$price_discount_span = AttributeType::add('price_discount_span', t('Price Discount by Span'), $pkg);
			$akc->associateAttributeKeyType($price_discount_span);	  
		} 
		
		$price_value_enter = AttributeType::getByHandle('price_value_enter');
		if(!is_object($price_value_enter) || !intval($price_value_enter->getAttributeTypeID()) ) { 
			$price_value_enter = AttributeType::add('price_value_enter', t('Price Value Input'), $pkg);
			$akc->associateAttributeKeyType($price_value_enter);	  
		} 
	
		$is_header = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'is_header'");
		if(!$is_header){
			$db->execute("ALTER TABLE ProformsItemAttributeKeys ADD is_header INT(1) NOT NULL default '0'");
			$db->execute("ALTER TABLE ProformsItemAttributeKeys ADD is_conditioned INT(1) NOT NULL default '0'");
			$db->execute("ALTER TABLE ProformsItemAttributeKeys ADD condition_handle CHARACTER(255)");
			$db->execute("ALTER TABLE ProformsItemAttributeKeys ADD condition_operator CHARACTER(255)");
			$db->execute("ALTER TABLE ProformsItemAttributeKeys ADD condition_value CHARACTER(255)");
		}
		
		$display_popup = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'display_popup'");
		if(!$display_popup){
			$db->execute("ALTER TABLE atAgreementSettings ADD display_popup INT(1) NOT NULL default '0'");
			$db->execute("ALTER TABLE atAgreement ADD agreement LONGTEXT");
		}
		
		$attribute_compare = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'from_att'");
		if(!$attribute_compare){
			$db->execute("ALTER TABLE atPublishSettings ADD from_att LONGTEXT");
			$db->execute("ALTER TABLE atPublishSettings ADD to_att LONGTEXT");
		}
		
		$code_string = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'code_string'");
		if(!$code_string){
			$db->execute("ALTER TABLE atRandomCode ADD code_string CHARACTER(128)");
		}
		
		$autoresponder_update = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'notifyEmailsOnSubmission'");
		if(!$autoresponder_update){
			$db->execute("ALTER TABLE atatAutoResponderSettings ADD notifyEmailsOnSubmission INT(1) NOT NULL default '0'");
			$db->execute("ALTER TABLE atatAutoResponderSettings ADD notify LONGTEXT");
		}

		$tooltip = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'tooltip'");
		if(!$tooltip){
			$db->execute("ALTER TABLE ProformsItemAttributeKeys ADD tooltip CHARACTER(255)");
		}

		$reviewed = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'reviewed'");
		if(!$reviewed){
			$db->execute("ALTER TABLE ProformsItem ADD reviewed INT(1)");
		}
		
		
		$totalpayments = $db->getOne("SHOW TABLES LIKE 'atPriceTotalPayments'");
		if(!$totalpayments){
			$db->execute("CREATE TABLE atPriceTotalPayments(ptpID INT(10) KEY AUTO_INCREMENT, avID INT(10),amount DECIMAL(6,2),payment_date DATE,credit INT(10),transactionID CHARACTER(255))");
		}else{
			$credit = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'credit'");
			if(!$credit){
				$db->execute("ALTER TABLE atPriceTotalPayments ADD credit INT(10)");
			}
		}
		
		$db->execute("ALTER TABLE atPriceTotalPayments change amount amount DECIMAL(6,2)");
		
		$event_require_payment = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'event_require_payment'");
		if(!$event_require_payment){
			$db->execute("ALTER TABLE atPriceEventSettings ADD event_require_payment INT(1) NOT NULL default '0'");
		}
		
		$atSimpleFileSettings = $db->getOne("SHOW TABLES LIKE 'atSimpleFileSettings'");
		if(!$atSimpleFileSettings){
			$db->execute("CREATE TABLE atSimpleFileSettings(akID INT(10), asID INT(10) NULL)");
		}
		
		$fees_optional = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'fees_optional'");
		if(!$fees_optional){
			$db->execute("ALTER TABLE atPriceValueFeesSettings ADD fees_optional INT(10)");
		}
		
		$total_deferred = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'total_deferred'");
		if(!$total_deferred){
			$db->execute("ALTER TABLE atPriceTotal ADD total_deferred DECIMAL(6,2)");
		}
		
		$show_payments = $db->getOne("SELECT column_name from information_schema.columns WHERE column_name = 'show_payments'");
		if(!$show_payments){
			$db->execute("ALTER TABLE atPriceTotalSettings ADD show_payments INT(10) NULL");
		}
		
		$db->execute("ALTER TABLE atPriceTotal MODIFY paid DECIMAL(6,2)");
		
		parent::upgrade();

	}
	
	public function uninstall() {
		parent::uninstall();
		$dbs = Loader::db();
		$dbs->Execute('drop table if exists ProformsItemSearchIndexAttributes;');
	}
	
	function load_required_models() {
		Loader::model('single_page');
		Loader::model('collection');
		Loader::model('page');
		loader::model('block');
		Loader::model('collection_types');
		Loader::model('/attribute/categories/collection');
		Loader::model('/attribute/categories/user');
	}	
	
	public function on_start(){
	  	$html = Loader::helper('html');
		$v = View::getInstance();
		define('ENABLE_APPLICATION_EVENTS', true);
		
		Events::extend('proforms_item_entry', 'ProformsFormSubmit', 'onSubmit', DIRNAME_PACKAGES . '/' . $this->pkgHandle . '/models/events/proforms_submit.php');
		Events::extend('proforms_item_approve', 'ProformsFormStatus', 'onFormApprove', DIRNAME_PACKAGES . '/' . $this->pkgHandle . '/models/events/proforms_status.php');
		Events::extend('proforms_item_deny', 'ProformsFormStatus', 'onFormDeny', DIRNAME_PACKAGES . '/' . $this->pkgHandle . '/models/events/proforms_status.php');
		Events::extend('proforms_item_pending', 'ProformsFormStatus', 'onFormPending', DIRNAME_PACKAGES . '/' . $this->pkgHandle . '/models/events/proforms_status.php');
		
		Events::extend('proforms_item_payment', 'ProformsPayments', 'onPaymentSuccess', DIRNAME_PACKAGES . '/' . $this->pkgHandle . '/models/events/proforms_payments.php');
		
		
	}
  
}

?>