��          �   %   �      P     Q     p     �     �     �     �     �     �     �     �          2  #   G     k     t     �     �  1   �  -   �  .     )   L     v  K   |  A   �  %   
    0      4     U     g  	   x  2   �     �     �     �     �     �     �     �  )      	   *     4     B     a  2   v  +   �  -   �  *   	     .	  R   5	  M   �	  &   �	                                            
                             	                                                    Direct access to IRC channels. Height (in pixels) IRC Chat IRC channel IRC chat disabled in edit mode. IRC network KiwiIRC server KiwiIrcThemeBasic KiwiIrcThemeCLI (Dark) KiwiIrcThemeMini (Small) KiwiIrcThemeNone/default KiwiIrcThemeRelaxed Leave empty to use the default (%s) Nickname Please select Please specify the IRC network. Please specify the height. Suggest nickname from username of logged-in users The channel name contains invalid characters. The height must be a number greater than zero. The nickname contains invalid characters. Theme This block uses <a href="https://kiwiirc.com/" target="_blank">KiwiIRC</a>. To open the IRC chat <a href="%s" target="_blank">click here</a>. Your browser does not support frames. Project-Id-Version: concrete5 packages
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-09-09 09:28+0000
PO-Revision-Date: 2013-09-09 09:31+0000
Last-Translator: Michele Locati <michele@locati.it>
Language-Team: German (Germany) (http://www.transifex.com/projects/p/concrete5-mlocati-packages/language/de_DE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: ../../..
X-Poedit-SourceCharset: UTF-8
 Direkter Zugriff auf IRC Kanäle Höhe (in Pixeln) IRC Unterhaltung IRC Kanal IRC Unterhaltung im Bearbeitung-Modus deaktiviert. IRC Netzwerk KiwiIRC Server Basic CLI (Dunkel) Mini Keine/Standard Relaxed Leer lassen um Standard (%s) zu verwenden Spitzname Bitte wählen Bitte IRC Netzwerk definieren. Bitte Höhe angeben. Spitzname anhand eingeloggter Benutzer vorschlagen Der Kanal-Name enthält ungültige Zeichen. Die Höhe muss eine Zahl grösser als 0 sein. Der Spitzname enthält ungültige Zeichen. Layout Dieser Block verwendet <a href="https://kiwiirc.com/" target="_blank">KiwiIRC</a>. <a href="%s" target="_blank">Hier klicken</a> um IRC Unterhaltung zu öffnen. Ihr Browser unterstützt keine Frames. 