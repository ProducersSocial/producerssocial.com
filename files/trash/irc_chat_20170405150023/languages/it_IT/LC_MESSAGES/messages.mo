��    (      \  5   �      p     q     w  D   �  
   �     �     �                    $     0     P     \     t     �     �     �     �     �  #   �       <   $     a     y     �     �     �  1   �  -     .   0  )   _     �  K   �  A   �  %        C     c  "   q     �    �     �	     �	  F   �	     
     $
  "   8
     [
     n
     r
  
   {
  ,   �
     �
     �
     �
     �
     �
     �
            0   !     R  I   [  "   �     �     �     �     �  :     1   O  3   �  *   �     �  M   �  F   3      z  %   �     �  &   �      �                          #                              (   $         '                      &          
       	         "                                           !   %              About Advanced options Allow inserting a block in your website to partecipate to IRC chats. Appearance Device types Direct access to IRC channels. Height (in pixels) IRC IRC Chat IRC channel IRC chat disabled in edit mode. IRC network Key for the IRC channel KiwiIRC server KiwiIrcThemeBasic KiwiIrcThemeCLI (Dark) KiwiIrcThemeMini (Small) KiwiIrcThemeNone/default KiwiIrcThemeRelaxed Leave empty to use the default (%s) Nickname Or enter a custom IRC network (in the form <i>host:port</i>) Or enter a custom theme Other options Please select Please specify the IRC network. Please specify the height. Suggest nickname from username of logged-in users The channel name contains invalid characters. The height must be a number greater than zero. The nickname contains invalid characters. Theme This block uses <a href="https://kiwiirc.com/" target="_blank">KiwiIRC</a>. To open the IRC chat <a href="%s" target="_blank">click here</a>. Your browser does not support frames. connect to IRC network with SSL debug enabled show only a link for mobile phones show only a link for tablets Project-Id-Version: concrete5 packages
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-09-09 09:28+0000
PO-Revision-Date: 2013-09-09 09:33+0000
Last-Translator: Michele Locati <michele@locati.it>
Language-Team: Italian (Italy) (http://www.transifex.com/projects/p/concrete5-mlocati-packages/language/it_IT/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it_IT
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: ../../..
X-Poedit-SourceCharset: UTF-8
 Informazioni su Opzioni avanzate Permette di inserire un blocco nel sito per partecipare alle chat IRC. Aspetto Tipi di dispositivi Accedi direttamente ai canali IRC. Altezza (in pixel) IRC Chat IRC Canale IRC Chat IRC disabilitata in modalità modifica. Rete IRC Chiave per il canale IRC Server KiwiIRC Basic CLI (scuro) Mini (piccolo) Nessuno/predefinito Relaxed Lasciare vuoto per usare quello predefinito (%s) Nickname O inserisci una rete IRC personalizzata (nella forma <i>server:porta</i>) O inserisci un tema personalizzato Altre opzioni Selezionare Specificare la rete IRC. Specificare l’altezza. Suggerisci nickname dal nome utente degli utenti collegati Il nome del canale contiene caratteri non validi. L’altezza deve essere un numero maggiore di zero. Il nickname contiene caratteri non validi. Tema Questo blocco usa <a href="https://kiwiirc.com/" target="_blank">KiwiIRC</a>. Per aprire la chat IRC <a href="%s" target="_blank">fare clic qui</a>. Il browser non supporta i frame. connessione alla rete IRC tramite SSL abilita debug mostra solo un link per gli smartphone mostra solo un link per i tablet 