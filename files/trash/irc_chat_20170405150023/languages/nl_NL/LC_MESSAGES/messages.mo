��    (      \  5   �      p     q     w  D   �  
   �     �     �                    $     0     P     \     t     �     �     �     �     �  #   �       <   $     a     y     �     �     �  1   �  -     .   0  )   _     �  K   �  A   �  %        C     c  "   q     �  �  �     �	     �	  D   �	     �	     
      #
     D
     W
     [
  
   d
  *   o
     �
     �
     �
     �
     �
     �
     �
       +        :  G   C     �     �     �     �     �  A   �  "   :  .   ]  #   �     �  V   �  D     #   R  (   v     �  +   �  $   �                          #                              (   $         '                      &          
       	         "                                           !   %              About Advanced options Allow inserting a block in your website to partecipate to IRC chats. Appearance Device types Direct access to IRC channels. Height (in pixels) IRC IRC Chat IRC channel IRC chat disabled in edit mode. IRC network Key for the IRC channel KiwiIRC server KiwiIrcThemeBasic KiwiIrcThemeCLI (Dark) KiwiIrcThemeMini (Small) KiwiIrcThemeNone/default KiwiIrcThemeRelaxed Leave empty to use the default (%s) Nickname Or enter a custom IRC network (in the form <i>host:port</i>) Or enter a custom theme Other options Please select Please specify the IRC network. Please specify the height. Suggest nickname from username of logged-in users The channel name contains invalid characters. The height must be a number greater than zero. The nickname contains invalid characters. Theme This block uses <a href="https://kiwiirc.com/" target="_blank">KiwiIRC</a>. To open the IRC chat <a href="%s" target="_blank">click here</a>. Your browser does not support frames. connect to IRC network with SSL debug enabled show only a link for mobile phones show only a link for tablets Project-Id-Version: concrete5 packages
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-09-09 09:28+0000
PO-Revision-Date: 2013-09-12 23:10+0000
Last-Translator: Pam Banis
Language-Team: Dutch (Netherlands) (http://www.transifex.com/projects/p/concrete5-mlocati-packages/language/nl_NL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_NL
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: ../../..
X-Poedit-SourceCharset: UTF-8
 Over Geavanceerde opties Een blok invoegen in uw website te meehelpen aan IRC chats toestaan. Uiterlijke verschijning Apparaattypen Directe toegang tot IRC-kanalen. Hoogte (in pixels) IRC IRC Chat IRC kanaal IRC chat uitgeschakeld de bewerkingsmodus. Internet Relay Chat Toets voor het IRC kanaal KiwiIRC server Basis CLI (donker) Mini (klein) Geen/standaard Relaxed Laat leeg om de standaard te gebruiken (%s) Nickname Of type hier een aangepaste IRC-netwerk (in de vorm <i>host: poort</i>) Of geef een aangepaste thema op Andere opties Maak een keuze Specificeer het IRC-netwerk. Geef de hoogte op. Bijnaam van gebruikersnaam van de ingelogde gebruikers suggereren De naam bevat ongeldige karakters. De hoogte moet een getal zijn, groter dan nul. De nickname bevat ongeldige tekens. Thema Dit blok maakt gebruik van <a href="https://kiwiirc.com/" target="_blank">KiwiIRC</a>. Om de IRC chat te openen <a href="%s" target="_blank">klik hier</a>. Uw browser ondersteunt geen frames. verbinding maken met IRC netwerk met SSL foutopsporing ingeschakeld Toon alleen een link voor mobiele telefoons Toon alleen een link voor een tablet 